#include <stdio.h>
#include <time.h>
#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <TMath.h>
#include <vector>

#include <TROOT.h> 

#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TMinuit.h>
#include <TLatex.h>
#include <TRandom3.h> 
#include <thread>
#include <algorithm>
#include <THStack.h>

#include <complex>
//#include "AtlasStyle.C"

#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TF1.h>

#include "TLorentzVector.h"
#include "TVector3.h"
#include "TVectorD.h"

#include "TMath.h"
#include "Math/Vector3D.h"
#include "Math/Vector4D.h"

#define MASS_JPSI   3.096900
#define MASS_MUON   0.1056583745
#define MASS_PION   0.13957061
#define MASS_KAON   0.493677
#define MASS_PROTON 0.938272081
#define MASS_LB     5.61960
#define MASS_BD     5.27963
#define MASS_BS     5.36689
#define MEV         1000.0

#define includeFNRsimple 1

#define ppi_pip_bins 74
#define ppi_pip_low 5.05
#define ppi_pip_high 6.9
#define ppi_pip_bins2 84
#define ppi_pip_low2 4.9
#define ppi_pip_high2 7.

#define pK_Kp_bins 64
#define pK_Kp_low 5.200
#define pK_Kp_high 6.800
#define pK_Kp_bins2 66
#define pK_Kp_low2 5.150
#define pK_Kp_high2 6.800

#define pK_Kp_bins3 50
#define pK_Kp_low3 5.200
#define pK_Kp_high3 7.000
#define pK_Kp_bins4 51
#define pK_Kp_low4 5.150
#define pK_Kp_high4 7.000

#define Kpi_piK_bins 40
#define Kpi_piK_low 4.900
#define Kpi_piK_high 5.700
#define Kpi_piK_bins2 62
#define Kpi_piK_low2 4.650
#define Kpi_piK_high2 5.900

#define cos_bins 10
#define cos_low 0.0
#define cos_high 1.0

#define phi_bins 10
#define phi_low -3.1416
#define phi_high 3.1416

#define Jpsipi_bins 50
#define Jpsipi_low 3.300
#define Jpsipi_high 5.000

#define JpsiK_bins 50
#define JpsiK_low 3.500
#define JpsiK_high 5.200

#define Kpi_bins 50
#define Kpi_low 1.550
#define Kpi_high 2.400

#define KK_bins 62
#define KK_low 1.680
#define KK_high 2.300

#define JpsiK_bins2 100
#define JpsiK_low2 3.575
#define JpsiK_high2 4.850

#define JpsiK_bins3 50
#define JpsiK_low3 3.575
#define JpsiK_high3 4.850

#define KK_pipi_bins 40
#define KK_pipi_low 4.970
#define KK_pipi_high 6.000

#define KK_pipi_bins2 40
#define KK_pipi_low2 4.550
#define KK_pipi_high2 5.650

#define JpsiK_bins4 50
#define JpsiK_low4 3.600
#define JpsiK_high4 4.850

#define Jpsip_bins 50
#define Jpsip_low 4.000
#define Jpsip_high 5.250

#define pK_bins 50
#define pK_low 2.000
#define pK_high 2.800

#define pK_Kp_bins5 50
#define pK_Kp_low5 7.400
#define pK_Kp_high5 9.500

#define pK_Kp_bins6 50
#define pK_Kp_low6 -1.000
#define pK_Kp_high6 0.900

#define KK_bins2 100
#define KK_low2 4.970
#define KK_high2 6.000

#define pipi_bins 100
#define pipi_low 4.550
#define pipi_high 5.650

#define pipi_bins2 440
#define pipi_low2 4.550
#define pipi_high2 5.650

//pipi_bins2, pipi_low2, pipi_high2


using namespace std;

// angle_variables for Bd->JpsiKpi decayes
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//

Double_t theta_B0(TLorentzVector vBd, TLorentzVector vKstar)
{
    TVector3 vBd3 = vBd.BoostVector();
	TLorentzVector vKstar_b = vKstar;
	vKstar_b.Boost(-vBd3);
return vBd.Vect().Angle( vKstar_b.Vect() );
}

Double_t phi_psi(TLorentzVector vB0, TLorentzVector vZc, TLorentzVector vpsi)
{
	TLorentzVector vZc_b = vZc;
	TLorentzVector vpsi_b = vpsi;
	TVector3 vZc3 = vZc.BoostVector();
    TVector3 vB03 = vB0.BoostVector();
	vZc_b.Boost(-vB03);
	vpsi_b.Boost(-vZc3);

	TVector3 x0 = -vB0.Vect().Unit() + ( vB0.Vect().Unit() * vZc_b.Vect().Unit() ) * vZc_b.Vect().Unit();
	
	double argy = ( vZc_b.Vect().Unit().Cross(x0.Unit()) ) * vpsi_b.Vect().Unit();
	double argx = vpsi_b.Vect().Unit() * x0.Unit();

	return atan2( argy, argx );
}

Double_t phi_K(TLorentzVector vBd, TLorentzVector vKstar, TLorentzVector vK, TLorentzVector vpsi)
{
	TLorentzVector vK_b = vK;
	TLorentzVector vKstarb = vKstar;
	TLorentzVector vpsib = vpsi;
	TLorentzVector vKb = vK;
	TVector3 vBd3 = vBd.BoostVector();
	TVector3 vKstar3 = vKstar.BoostVector();
	

	vKstarb.Boost(-vBd3);
	vpsib.Boost(-vKstar3);
	vKb.Boost(-vKstar3);

	TVector3 x0 = - vBd.Vect().Unit() + ( vBd.Vect().Unit() * vKstarb.Vect().Unit() ) * vKstarb.Vect().Unit();
	
	
	double argy = ( -vpsib.Vect().Unit().Cross(x0.Unit()) ) * vKb.Vect().Unit();
	double argx = x0.Unit() * vKb.Vect().Unit();

	return atan2( argy, argx );
}

Double_t alpha_mu(TLorentzVector vmu_2, TLorentzVector vpsi, TLorentzVector vZc, TLorentzVector vBd)
{
	TLorentzVector vmu2_b = vmu_2;	
	TLorentzVector vpsi_b = vpsi;
	TLorentzVector vpsi_b2 = vpsi;
	//TLorentzVector vLambda_b = vLambda;
	TVector3 vBd3 = vBd.BoostVector();
	TVector3 vZc3 = vZc.BoostVector();
	TVector3 vpsi3 = vpsi.BoostVector();
	

	vmu2_b.Boost(-vpsi3);
	vpsi_b.Boost(-vBd3);
	vpsi_b2.Boost(-vZc3);


	TVector3 x1 = - vpsi_b2.Vect().Unit() + ( vpsi_b2.Vect().Unit() * vmu2_b.Vect().Unit() ) * vmu2_b.Vect().Unit();
	TVector3 x2 = - vpsi_b.Vect().Unit() + ( vpsi_b.Vect().Unit() * vmu2_b.Vect().Unit() ) * vmu2_b.Vect().Unit();
	
	
	double argy = ( vmu2_b.Vect().Unit().Cross(x1.Unit()) ) * x2.Unit();
	double argx = x1.Unit() * x2.Unit();

	return atan2( argy, argx );
}

Double_t phi_mu_Kstar(TLorentzVector vBd, TLorentzVector vKstar, TLorentzVector vpsi, TLorentzVector v_mu2)
{
	TLorentzVector vKstar_b = vKstar;
	TLorentzVector vpsi_b = vpsi;
	TVector3 vBd3 = vBd.BoostVector();
	
	vpsi_b.Boost(-vBd3);
	vKstar_b.Boost(-vBd3);

	TVector3 x0 = -vBd.Vect().Unit() + ( vBd.Vect().Unit() * vKstar_b.Vect().Unit() ) * vKstar_b.Vect().Unit();
	
	TLorentzVector v_mu2_b = v_mu2;
	TVector3 vpsi3 = vpsi.BoostVector();
	
	v_mu2_b.Boost(-vpsi3);
	
	double argy = ( vpsi_b.Vect().Unit().Cross(x0.Unit()) ) * v_mu2_b.Vect().Unit();
	double argx = v_mu2_b.Vect().Unit() * x0.Unit();

	return atan2( argy, argx );
}

// Only for B0 -> Jpsi K pi
Double_t phi_mu_X(TLorentzVector vZc, TLorentzVector vB0, TLorentzVector vpsi, TLorentzVector v_mu2)
{
	TLorentzVector vZc_b = vZc;
	TLorentzVector vpsi_b = vpsi;
	TVector3 vZc3 = vZc.BoostVector();
    TVector3 vB03 = vB0.BoostVector();
	vZc_b.Boost(-vB03);
	vpsi_b.Boost(-vZc3);

	TVector3 x0 = -vZc_b.Vect().Unit() + ( vZc_b.Vect().Unit() * vpsi_b.Vect().Unit() ) * vpsi_b.Vect().Unit();
	
	
	TLorentzVector v_mu2_b = v_mu2;
	TVector3 vpsi3 = vpsi.BoostVector();
	v_mu2_b.Boost(-vpsi3);
	
	double argy = ( vpsi_b.Vect().Unit().Cross(x0.Unit()) ) * v_mu2_b.Vect().Unit();
	double argx = v_mu2_b.Vect().Unit() * x0.Unit();

	return atan2( argy, argx );
}

///// angle between Jpsi and muon in Jpsi rest frame, Jpsi momentum is taken in Pc rest frame;
Double_t theta_psi_X(TLorentzVector vZc, TLorentzVector vpsi, TLorentzVector v_mu2)
{

	TVector3 vZc3 = vZc.BoostVector();
	TVector3 vpsi3 = vpsi.BoostVector();
	TLorentzVector v_mu2_b = v_mu2; 
	v_mu2_b.Boost(-vpsi3);
	TLorentzVector vpsi_b = vpsi;
	vpsi_b.Boost(-vZc3);

return v_mu2_b.Vect().Angle(vpsi_b.Vect());
}

//////////////////////////////////////////////////////////////////////////////////////////////
///// angle between Jpsi and Pc in Pc rest frame, Pc momentum is taken in Lambda_b rest frame;
Double_t theta_X(TLorentzVector vBd, TLorentzVector vZc, TLorentzVector vpsi)
{
	TVector3 vBd3 = vBd.BoostVector();
	TVector3 vZc3 = vZc.BoostVector();
	TLorentzVector vpsi_b = vpsi;
	vpsi_b.Boost(-vZc3);
	TLorentzVector vZc_b2 = vZc;
	vZc_b2.Boost(-vBd3);

return vpsi_b.Vect().Angle( vZc_b2.Vect() );
}

//////////////////////////////////////////////////////////////////////////////////////////////
///// angle between Kaon and Lstar candidate in Lstar rest frame, Lstar momentum is taken in Lambda_b rest frame;
Double_t theta_Kstar( TLorentzVector vBd, TLorentzVector vKstar, TLorentzVector vK )
{
	
	TVector3 vBd3 = vBd.BoostVector();
	TVector3 vKstar3 = vKstar.BoostVector();

	TLorentzVector vK_b = vK;
	vK_b.Boost(-vKstar3);
	
	TLorentzVector vKstarb = vKstar;
	vKstarb.Boost(-vBd3);
	return vK_b.Vect().Angle(vKstarb.Vect()) ;

}

///////////////////////////////////////////////////////////////////////////////////////////////
///// angle between between Jpsi and muon in Jpsi rest frame, Jpsi momentum is taken in Bd rest frame;
Double_t theta_psi_Kstar(TLorentzVector vBd, TLorentzVector vpsi, TLorentzVector v_mu2)
{

	TVector3 vBd3 = vBd.BoostVector();
	TVector3 vpsi3 = vpsi.BoostVector();
	TLorentzVector v_mu2_b = v_mu2; 
	v_mu2_b.Boost(-vpsi3);
	TLorentzVector vpsi_b = vpsi;
	vpsi_b.Boost(-vBd3);

return v_mu2_b.Vect().Angle(vpsi_b.Vect());
}

//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//

TH1D *B_pt_data =  new TH1D("B_pt_data", "B_pt_data", 50, 0., 100.);
TH1D *B_pt_BD_to_JPSI_K_PI =  new TH1D("B_pt_BD_to_JPSI_K_PI", "B_pt_BD_to_JPSI_K_PI", 50, 0., 100.);
TH1D *B_pt_BD_to_JPSI_PI_PI =  new TH1D("B_pt_BD_to_JPSI_PI_PI", "B_pt_BD_to_JPSI_PI_PI", 50, 0., 100.);
TH1D *B_pt_BS_to_JPSI_PI_PI =  new TH1D("B_pt_BS_to_JPSI_PI_PI", "B_pt_BS_to_JPSI_PI_PI", 50, 0., 100.);
TH1D *B_pt_BS_to_JPSI_K_K =  new TH1D("B_pt_BS_to_JPSI_K_K", "B_pt_BS_to_JPSI_K_K", 50, 0., 100.);
TH1D *B_pt_LAMBDA0B_to_JPSI_P_K =  new TH1D("B_pt_LAMBDA0B_to_JPSI_P_K", "B_pt_LAMBDA0B_to_JPSI_P_K", 50, 0., 100.);

TH1D *B_pt_BD_to_JPSI_K_PI_reweighted =  new TH1D("B_pt_BD_to_JPSI_K_PI_reweighted", "B_pt_BD_to_JPSI_K_PI_reweighted", 50, 0., 100.);
TH1D *B_pt_BD_to_JPSI_PI_PI_reweighted =  new TH1D("B_pt_BD_to_JPSI_PI_PI_reweighted", "B_pt_BD_to_JPSI_PI_PI_reweighted", 50, 0., 100.);
TH1D *B_pt_BS_to_JPSI_PI_PI_reweighted =  new TH1D("B_pt_BS_to_JPSI_PI_PI_reweighted", "B_pt_BS_to_JPSI_PI_PI_reweighted", 50, 0., 100.);
TH1D *B_pt_BS_to_JPSI_K_K_reweighted =  new TH1D("B_pt_BS_to_JPSI_K_K_reweighted", "B_pt_BS_to_JPSI_K_K_reweighted", 50, 0., 100.);
TH1D *B_pt_LAMBDA0B_to_JPSI_P_K_reweighted =  new TH1D("B_pt_LAMBDA0B_to_JPSI_P_K_reweighted", "B_pt_LAMBDA0B_to_JPSI_P_K_reweighted", 50, 0., 100.);

int main()
{    
    
    float GEV = 0.001;
    TH1* h_mu1_pt_data = new TH1D("h_mu1_pt_data", "h_mu1_pt_data", 100, 0., 80000.);
    TH1* h_mu2_pt_data = new TH1D("h_mu2_pt_data", "h_mu2_pt_data", 100, 0., 80000.);
    TH1* h_mu1_pt_data_w = new TH1D("h_mu1_pt_data_w", "h_mu1_pt_data_w", 100, 0., 80000.);
    TH1* h_mu2_pt_data_w = new TH1D("h_mu2_pt_data_w", "h_mu2_pt_data_w", 100, 0., 80000.);
    
    TH1* h_m_jpsi_data = new TH1D("h_m_jpsi_data", "h_m_jpsi_data", 50., 2950., 3250.);
    TH1* h_m_jpsi_data_sc = new TH1D("h_m_jpsi_data_sc", "h_m_jpsi_data_sc", 50., 2950., 3250.);
    TH1* h_m_jpsi_data_w = new TH1D("h_m_jpsi_data_w", "h_m_jpsi_data_w", 50., 2950., 3250.);

//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__||  Histograms  ||__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//

    TH2 *cos_theta_Zc_data = new TH2D("cos_theta_Zc_data", "cos_theta_Zc_data", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_Zc_BD_to_JPSI_K_PI = new TH2D("cos_theta_Zc_BD_to_JPSI_K_PI", "cos_theta_Zc_BD_to_JPSI_K_PI", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_Zc_BD_to_JPSI_PI_PI = new TH2D("cos_theta_Zc_BD_to_JPSI_PI_PI", "cos_theta_Zc_BD_to_JPSI_PI_PI", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_Zc_BS_to_JPSI_PI_PI = new TH2D("cos_theta_Zc_BS_to_JPSI_PI_PI", "cos_theta_Zc_BS_to_JPSI_PI_PI", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_Zc_BS_to_JPSI_K_K = new TH2D("cos_theta_Zc_BS_to_JPSI_K_K", "cos_theta_Zc_BS_to_JPSI_K_K", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_Zc_LAMBDA0B_to_JPSI_P_K = new TH2D("cos_theta_Zc_LAMBDA0B_to_JPSI_P_K", "cos_theta_Zc_LAMBDA0B_to_JPSI_P_K", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_Zc_BS_to_JPSI_K_PI = new TH2D("cos_theta_Zc_BS_to_JPSI_K_PI", "cos_theta_Zc_BS_to_JPSI_K_PI", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_Zc_BD_to_JPSI_K_K = new TH2D("cos_theta_Zc_BD_to_JPSI_K_K", "cos_theta_Zc_BD_to_JPSI_K_K", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_Zc_LAMBDA0B_to_JPSI_P_PI = new TH2D("cos_theta_Zc_LAMBDA0B_to_JPSI_P_PI", "cos_theta_Zc_LAMBDA0B_to_JPSI_P_PI", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    
    TH2 *cos_theta_psi_X_data = new TH2D("cos_theta_psi_X_data", "cos_theta_psi_X_data", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_psi_X_BD_to_JPSI_K_PI = new TH2D("cos_theta_psi_X_BD_to_JPSI_K_PI", "cos_theta_psi_X_BD_to_JPSI_K_PI", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_psi_X_BD_to_JPSI_PI_PI = new TH2D("cos_theta_psi_X_BD_to_JPSI_PI_PI", "cos_theta_psi_X_BD_to_JPSI_PI_PI", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_psi_X_BS_to_JPSI_PI_PI = new TH2D("cos_theta_psi_X_BS_to_JPSI_PI_PI", "cos_theta_psi_X_BS_to_JPSI_PI_PI", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_psi_X_BS_to_JPSI_K_K = new TH2D("cos_theta_psi_X_BS_to_JPSI_K_K", "cos_theta_psi_X_BS_to_JPSI_K_K", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K = new TH2D("cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K", "cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_psi_X_BS_to_JPSI_K_PI = new TH2D("cos_theta_psi_X_BS_to_JPSI_K_PI", "cos_theta_psi_X_BS_to_JPSI_K_PI", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_psi_X_BD_to_JPSI_K_K = new TH2D("cos_theta_psi_X_BD_to_JPSI_K_K", "cos_theta_psi_X_BD_to_JPSI_K_K", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_psi_X_LAMBDA0B_to_JPSI_P_PI = new TH2D("cos_theta_psi_X_LAMBDA0B_to_JPSI_P_PI", "cos_theta_psi_X_LAMBDA0B_to_JPSI_P_PI", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    
    TH2 *cos_theta_Kstar_data = new TH2D("cos_theta_Kstar_data", "cos_theta_Kstar_data", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_Kstar_BD_to_JPSI_K_PI = new TH2D("cos_theta_Kstar_BD_to_JPSI_K_PI", "cos_theta_Kstar_BD_to_JPSI_K_PI", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_Kstar_BD_to_JPSI_PI_PI = new TH2D("cos_theta_Kstar_BD_to_JPSI_PI_PI", "cos_theta_Kstar_BD_to_JPSI_PI_PI", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_Kstar_BS_to_JPSI_PI_PI = new TH2D("cos_theta_Kstar_BS_to_JPSI_PI_PI", "cos_theta_Kstar_BS_to_JPSI_PI_PI", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_Kstar_BS_to_JPSI_K_K = new TH2D("cos_theta_Kstar_BS_to_JPSI_K_K", "cos_theta_Kstar_BS_to_JPSI_K_K", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K = new TH2D("cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K", "cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_Kstar_BS_to_JPSI_K_PI = new TH2D("cos_theta_Kstar_BS_to_JPSI_K_PI", "cos_theta_Kstar_BS_to_JPSI_K_PI", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_Kstar_BD_to_JPSI_K_K = new TH2D("cos_theta_Kstar_BD_to_JPSI_K_K", "cos_theta_Kstar_BD_to_JPSI_K_K", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_Kstar_LAMBDA0B_to_JPSI_P_PI = new TH2D("cos_theta_Kstar_LAMBDA0B_to_JPSI_P_PI", "cos_theta_Kstar_LAMBDA0B_to_JPSI_P_PI", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    
    TH2 *cos_theta_psi_Kstar_data = new TH2D("cos_theta_psi_Kstar_data", "cos_theta_psi_Kstar_data", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_psi_Kstar_BD_to_JPSI_K_PI = new TH2D("cos_theta_psi_Kstar_BD_to_JPSI_K_PI", "cos_theta_psi_Kstar_BD_to_JPSI_K_PI", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_psi_Kstar_BD_to_JPSI_PI_PI = new TH2D("cos_theta_psi_Kstar_BD_to_JPSI_PI_PI", "cos_theta_psi_Kstar_BD_to_JPSI_PI_PI", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_psi_Kstar_BS_to_JPSI_PI_PI = new TH2D("cos_theta_psi_Kstar_BS_to_JPSI_PI_PI", "cos_theta_psi_Kstar_BS_to_JPSI_PI_PI", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_psi_Kstar_BS_to_JPSI_K_K = new TH2D("cos_theta_psi_Kstar_BS_to_JPSI_K_K", "cos_theta_psi_Kstar_BS_to_JPSI_K_K", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K = new TH2D("cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K", "cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_psi_Kstar_BS_to_JPSI_K_PI = new TH2D("cos_theta_psi_Kstar_BS_to_JPSI_K_PI", "cos_theta_psi_Kstar_BS_to_JPSI_K_PI", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_psi_Kstar_BD_to_JPSI_K_K = new TH2D("cos_theta_psi_Kstar_BD_to_JPSI_K_K", "cos_theta_psi_Kstar_BD_to_JPSI_K_K", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_PI = new TH2D("cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_PI", "cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_PI", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    
    TH2 *phi_K_data = new TH2D("phi_K_data", "phi_K_data", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_K_BD_to_JPSI_K_PI = new TH2D("phi_K_BD_to_JPSI_K_PI", "phi_K_BD_to_JPSI_K_PI", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_K_BD_to_JPSI_PI_PI = new TH2D("phi_K_BD_to_JPSI_PI_PI", "phi_K_BD_to_JPSI_PI_PI", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_K_BS_to_JPSI_PI_PI = new TH2D("phi_K_BS_to_JPSI_PI_PI", "phi_K_BS_to_JPSI_PI_PI", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_K_BS_to_JPSI_K_K = new TH2D("phi_K_BS_to_JPSI_K_K", "phi_K_BS_to_JPSI_K_K", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_K_LAMBDA0B_to_JPSI_P_K = new TH2D("phi_K_LAMBDA0B_to_JPSI_P_K", "phi_K_LAMBDA0B_to_JPSI_P_K", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_K_BS_to_JPSI_K_PI = new TH2D("phi_K_BS_to_JPSI_K_PI", "phi_K_BS_to_JPSI_K_PI", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_K_BD_to_JPSI_K_K = new TH2D("phi_K_BD_to_JPSI_K_K", "phi_K_BD_to_JPSI_K_K", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_K_LAMBDA0B_to_JPSI_P_PI = new TH2D("phi_K_LAMBDA0B_to_JPSI_P_PI", "phi_K_LAMBDA0B_to_JPSI_P_PI", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    
    TH2 *alpha_mu_data = new TH2D("alpha_mu_data", "alpha_mu_data", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *alpha_mu_BD_to_JPSI_K_PI = new TH2D("alpha_mu_BD_to_JPSI_K_PI", "alpha_mu_BD_to_JPSI_K_PI", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *alpha_mu_BD_to_JPSI_PI_PI = new TH2D("alpha_mu_BD_to_JPSI_PI_PI", "alpha_mu_BD_to_JPSI_PI_PI", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *alpha_mu_BS_to_JPSI_PI_PI = new TH2D("alpha_mu_BS_to_JPSI_PI_PI", "alpha_mu_BS_to_JPSI_PI_PI", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *alpha_mu_BS_to_JPSI_K_K = new TH2D("alpha_mu_BS_to_JPSI_K_K", "alpha_mu_BS_to_JPSI_K_K", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *alpha_mu_LAMBDA0B_to_JPSI_P_K = new TH2D("alpha_mu_LAMBDA0B_to_JPSI_P_K", "alpha_mu_LAMBDA0B_to_JPSI_P_K", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *alpha_mu_BS_to_JPSI_K_PI = new TH2D("alpha_mu_BS_to_JPSI_K_PI", "alpha_mu_BS_to_JPSI_K_PI", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *alpha_mu_BD_to_JPSI_K_K = new TH2D("alpha_mu_BD_to_JPSI_K_K", "alpha_mu_BD_to_JPSI_K_K", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *alpha_mu_LAMBDA0B_to_JPSI_P_PI = new TH2D("alpha_mu_LAMBDA0B_to_JPSI_P_PI", "alpha_mu_LAMBDA0B_to_JPSI_P_PI", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    
    TH2 *phi_mu_Kstar_data = new TH2D("phi_mu_Kstar_data", "phi_mu_Kstar_data", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_mu_Kstar_BD_to_JPSI_K_PI = new TH2D("phi_mu_Kstar_BD_to_JPSI_K_PI", "phi_mu_Kstar_BD_to_JPSI_K_PI", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_mu_Kstar_BD_to_JPSI_PI_PI = new TH2D("phi_mu_Kstar_BD_to_JPSI_PI_PI", "phi_mu_Kstar_BD_to_JPSI_PI_PI", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_mu_Kstar_BS_to_JPSI_PI_PI = new TH2D("phi_mu_Kstar_BS_to_JPSI_PI_PI", "phi_mu_Kstar_BS_to_JPSI_PI_PI", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_mu_Kstar_BS_to_JPSI_K_K = new TH2D("phi_mu_Kstar_BS_to_JPSI_K_K", "phi_mu_Kstar_BS_to_JPSI_K_K", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K = new TH2D("phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K", "phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_mu_Kstar_BS_to_JPSI_K_PI = new TH2D("phi_mu_Kstar_BS_to_JPSI_K_PI", "phi_mu_Kstar_BS_to_JPSI_K_PI", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_mu_Kstar_BD_to_JPSI_K_K = new TH2D("phi_mu_Kstar_BD_to_JPSI_K_K", "phi_mu_Kstar_BD_to_JPSI_K_K", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_mu_Kstar_LAMBDA0B_to_JPSI_P_PI = new TH2D("phi_mu_Kstar_LAMBDA0B_to_JPSI_P_PI", "phi_mu_Kstar_LAMBDA0B_to_JPSI_P_PI", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    
    TH2 *phi_mu_X_data = new TH2D("phi_mu_X_data", "phi_mu_X_data", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_mu_X_BD_to_JPSI_K_PI = new TH2D("phi_mu_X_BD_to_JPSI_K_PI", "phi_mu_X_BD_to_JPSI_K_PI", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_mu_X_BD_to_JPSI_PI_PI = new TH2D("phi_mu_X_BD_to_JPSI_PI_PI", "phi_mu_X_BD_to_JPSI_PI_PI", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_mu_X_BS_to_JPSI_PI_PI = new TH2D("phi_mu_X_BS_to_JPSI_PI_PI", "phi_mu_X_BS_to_JPSI_PI_PI", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_mu_X_BS_to_JPSI_K_K = new TH2D("phi_mu_X_BS_to_JPSI_K_K", "phi_mu_X_BS_to_JPSI_K_K", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_mu_X_LAMBDA0B_to_JPSI_P_K = new TH2D("phi_mu_X_LAMBDA0B_to_JPSI_P_K", "phi_mu_X_LAMBDA0B_to_JPSI_P_K", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_mu_X_BS_to_JPSI_K_PI = new TH2D("phi_mu_X_BS_to_JPSI_K_PI", "phi_mu_X_BS_to_JPSI_K_PI", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_mu_X_BD_to_JPSI_K_K = new TH2D("phi_mu_X_BD_to_JPSI_K_K", "phi_mu_X_BD_to_JPSI_K_K", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_mu_X_LAMBDA0B_to_JPSI_P_PI = new TH2D("phi_mu_X_LAMBDA0B_to_JPSI_P_PI", "phi_mu_X_LAMBDA0B_to_JPSI_P_PI", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    
    TH2 *phi_psi_data = new TH2D("phi_psi_data", "phi_psi_data", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_psi_BD_to_JPSI_K_PI = new TH2D("phi_psi_BD_to_JPSI_K_PI", "phi_psi_BD_to_JPSI_K_PI", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_psi_BD_to_JPSI_PI_PI = new TH2D("phi_psi_BD_to_JPSI_PI_PI", "phi_psi_BD_to_JPSI_PI_PI", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_psi_BS_to_JPSI_PI_PI = new TH2D("phi_psi_BS_to_JPSI_PI_PI", "phi_psi_BS_to_JPSI_PI_PI", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_psi_BS_to_JPSI_K_K = new TH2D("phi_psi_BS_to_JPSI_K_K", "phi_psi_BS_to_JPSI_K_K", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_psi_LAMBDA0B_to_JPSI_P_K = new TH2D("phi_psi_LAMBDA0B_to_JPSI_P_K", "phi_psi_LAMBDA0B_to_JPSI_P_K", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_psi_BS_to_JPSI_K_PI = new TH2D("phi_psi_BS_to_JPSI_K_PI", "phi_psi_BS_to_JPSI_K_PI", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_psi_BD_to_JPSI_K_K = new TH2D("phi_psi_BD_to_JPSI_K_K", "phi_psi_BD_to_JPSI_K_K", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_psi_LAMBDA0B_to_JPSI_P_PI = new TH2D("phi_psi_LAMBDA0B_to_JPSI_P_PI", "phi_psi_LAMBDA0B_to_JPSI_P_PI", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    
    TH2 *cos_theta_B0_data = new TH2D("cos_theta_B0_data", "cos_theta_B0_data", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_B0_BD_to_JPSI_K_PI = new TH2D("cos_theta_B0_BD_to_JPSI_K_PI", "cos_theta_B0_BD_to_JPSI_K_PI", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_B0_BD_to_JPSI_PI_PI = new TH2D("cos_theta_B0_BD_to_JPSI_PI_PI", "cos_theta_B0_BD_to_JPSI_PI_PI", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_B0_BS_to_JPSI_PI_PI = new TH2D("cos_theta_B0_BS_to_JPSI_PI_PI", "cos_theta_B0_BS_to_JPSI_PI_PI", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_B0_BS_to_JPSI_K_K = new TH2D("cos_theta_B0_BS_to_JPSI_K_K", "cos_theta_B0_BS_to_JPSI_K_K", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_B0_LAMBDA0B_to_JPSI_P_K = new TH2D("cos_theta_B0_LAMBDA0B_to_JPSI_P_K", "cos_theta_B0_LAMBDA0B_to_JPSI_P_K", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_B0_BS_to_JPSI_K_PI = new TH2D("cos_theta_B0_BS_to_JPSI_K_PI", "cos_theta_B0_BS_to_JPSI_K_PI", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_B0_BD_to_JPSI_K_K = new TH2D("cos_theta_B0_BD_to_JPSI_K_K", "cos_theta_B0_BD_to_JPSI_K_K", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_B0_LAMBDA0B_to_JPSI_P_PI = new TH2D("cos_theta_B0_LAMBDA0B_to_JPSI_P_PI", "cos_theta_B0_LAMBDA0B_to_JPSI_P_PI", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    
    TH2 *Kpi_piK_2d_data = new TH2D("Kpi_piK_2d_data", "Kpi_piK_2d_data", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);
    TH2 *Kpi_piK_2d_full = new TH2D("Kpi_piK_2d_full", "Kpi_piK_2d_full", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);
    TH2 *Kpi_piK_2d_bg = new TH2D("Kpi_piK_2d_bg", "Kpi_piK_2d_bg", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);
    TH2 *Kpi_piK_2d_BD_to_JPSI_K_PI = new TH2D("Kpi_piK_2d_BD_to_JPSI_K_PI", "Kpi_piK_2d_BD_to_JPSI_K_PI", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2); 
    TH2 *Kpi_piK_2d_BD_to_JPSI_PI_PI = new TH2D("Kpi_piK_2d_BD_to_JPSI_PI_PI", "Kpi_piK_2d_BD_to_JPSI_PI_PI", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);
    TH2 *Kpi_piK_2d_BS_to_JPSI_PI_PI = new TH2D("Kpi_piK_2d_BS_to_JPSI_PI_PI", "Kpi_piK_2d_BS_to_JPSI_PI_PI", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);
    TH2 *Kpi_piK_2d_BS_to_JPSI_K_K = new TH2D("Kpi_piK_2d_BS_to_JPSI_K_K", "Kpi_piK_2d_BS_to_JPSI_K_K", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2); 
    TH2 *Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K = new TH2D("Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K", "Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);  
    TH1 *Kpi_1d_bg = new TH1D("Kpi_1d_bg", "Kpi_1d_bg", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high);
    TH1 *Kpi_1d_full = new TH1D("Kpi_1d_full", "Kpi_1d_full", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high);
    TH2 *Kpi_piK_2d_BS_to_JPSI_K_PI = new TH2D("Kpi_piK_2d_BS_to_JPSI_K_PI", "Kpi_piK_2d_BS_to_JPSI_K_PI", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);
    TH2 *Kpi_piK_2d_BD_to_JPSI_K_K = new TH2D("Kpi_piK_2d_BD_to_JPSI_K_K", "Kpi_piK_2d_BD_to_JPSI_K_K", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2); 
    TH2 *Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI = new TH2D("Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI", "Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);  
    
    TH2 *ppi_pip_2d_data = new TH2D("ppi_pip_2d_data", "ppi_pip_2d_data", ppi_pip_bins, ppi_pip_low, ppi_pip_high, ppi_pip_bins2, ppi_pip_low2, ppi_pip_high2);
    TH2 *ppi_pip_2d_BD_to_JPSI_K_PI = new TH2D("ppi_pip_2d_BD_to_JPSI_K_PI", "ppi_pip_2d_BD_to_JPSI_K_PI", ppi_pip_bins, ppi_pip_low, ppi_pip_high, ppi_pip_bins2, ppi_pip_low2, ppi_pip_high2); 
    TH2 *ppi_pip_2d_BD_to_JPSI_PI_PI = new TH2D("ppi_pip_2d_BD_to_JPSI_PI_PI", "ppi_pip_2d_BD_to_JPSI_PI_PI", ppi_pip_bins, ppi_pip_low, ppi_pip_high, ppi_pip_bins2, ppi_pip_low2, ppi_pip_high2);
    TH2 *ppi_pip_2d_BS_to_JPSI_PI_PI = new TH2D("ppi_pip_2d_BS_to_JPSI_PI_PI", "ppi_pip_2d_BS_to_JPSI_PI_PI", ppi_pip_bins, ppi_pip_low, ppi_pip_high, ppi_pip_bins2, ppi_pip_low2, ppi_pip_high2);
    TH2 *ppi_pip_2d_BS_to_JPSI_K_K = new TH2D("ppi_pip_2d_BS_to_JPSI_K_K", "ppi_pip_2d_BS_to_JPSI_K_K", ppi_pip_bins, ppi_pip_low, ppi_pip_high, ppi_pip_bins2, ppi_pip_low2, ppi_pip_high2); 
    TH2 *ppi_pip_2d_LAMBDA0B_to_JPSI_P_K = new TH2D("ppi_pip_2d_LAMBDA0B_to_JPSI_P_K", "ppi_pip_2d_LAMBDA0B_to_JPSI_P_K", ppi_pip_bins, ppi_pip_low, ppi_pip_high, ppi_pip_bins2, ppi_pip_low2, ppi_pip_high2);  
    TH2 *ppi_pip_2d_BS_to_JPSI_K_PI = new TH2D("ppi_pip_2d_BS_to_JPSI_K_PI", "ppi_pip_2d_BS_to_JPSI_K_PI", ppi_pip_bins, ppi_pip_low, ppi_pip_high, ppi_pip_bins2, ppi_pip_low2, ppi_pip_high2);
    TH2 *ppi_pip_2d_BD_to_JPSI_K_K = new TH2D("ppi_pip_2d_BD_to_JPSI_K_K", "ppi_pip_2d_BD_to_JPSI_K_K", ppi_pip_bins, ppi_pip_low, ppi_pip_high, ppi_pip_bins2, ppi_pip_low2, ppi_pip_high2); 
    TH2 *ppi_pip_2d_LAMBDA0B_to_JPSI_P_PI = new TH2D("ppi_pip_2d_LAMBDA0B_to_JPSI_P_PI", "ppi_pip_2d_LAMBDA0B_to_JPSI_P_PI", ppi_pip_bins, ppi_pip_low, ppi_pip_high, ppi_pip_bins2, ppi_pip_low2, ppi_pip_high2); 

    TH1 *Jpsipi_1d = new TH1D("Jpsipi_1d", "Jpsipi_1d", 100, 3300. * GEV, 5000. * GEV);
    TH1 *JpsiK_1d = new TH1D("JpsiK_1d", "JpsiK_1d", 100, 3500. * GEV, 5200. * GEV);
    TH1 *Kpi_1d = new TH1D("Kpi_1d", "Kpi_1d", 100, 1550. * GEV, 2250. * GEV);
    TH1 *Jpsipi_1d_nsa = new TH1D("Jpsipi_1d_nsa", "Jpsipi_1d_nsa", 100, 3300. * GEV, 5000. * GEV);
    TH1 *Jpsipi_1d_BD_to_JPSI_K_PI_nsa = new TH1D("Jpsipi_1d_BD_to_JPSI_K_PI_nsa", "Jpsipi_1d_BD_to_JPSI_K_PI_nsa", 100, 3300. * GEV, 5000. * GEV);
    TH1 *JpsiK_1d_nsa = new TH1D("JpsiK_1d_nsa", "JpsiK_1d_nsa", 100, 3500. * GEV, 5200. * GEV);
    TH1 *Kpi_1d_nsa = new TH1D("Kpi_1d_nsa", "Kpi_1d_nsa", 100, 1550. * GEV, 2250. * GEV);
    

    TH2 *Jpsipi_2d_data = new TH2D("Jpsipi_2d_data", "Jpsipi_2d_data", Jpsipi_bins, Jpsipi_low, Jpsipi_high, Jpsipi_bins, Jpsipi_low, Jpsipi_high);
    TH2 *JpsiK_2d_data = new TH2D("JpsiK_2d_data", "JpsiK_2d_data", JpsiK_bins, JpsiK_low, JpsiK_high, JpsiK_bins, JpsiK_low, JpsiK_high);
    TH2 *Kpi_2d_data = new TH2D("Kpi_2d_data", "Kpi_2d_data", Kpi_bins, Kpi_low, Kpi_high, Kpi_bins, Kpi_low, Kpi_high);

    TH2 *Jpsipi_2d_BD_to_JPSI_K_PI = new TH2D("Jpsipi_2d_BD_to_JPSI_K_PI", "Jpsipi_2d_BD_to_JPSI_K_PI", Jpsipi_bins, Jpsipi_low, Jpsipi_high, Jpsipi_bins, Jpsipi_low, Jpsipi_high);
    TH2 *JpsiK_2d_BD_to_JPSI_K_PI = new TH2D("JpsiK_2d_BD_to_JPSI_K_PI", "JpsiK_2d_BD_to_JPSI_K_PI", JpsiK_bins, JpsiK_low, JpsiK_high, JpsiK_bins, JpsiK_low, JpsiK_high);
    TH2 *Kpi_2d_BD_to_JPSI_K_PI = new TH2D("Kpi_2d_BD_to_JPSI_K_PI", "Kpi_2d_BD_to_JPSI_K_PI", Kpi_bins, Kpi_low, Kpi_high, Kpi_bins, Kpi_low, Kpi_high);

    TH2 *Jpsipi_2d_BD_to_JPSI_PI_PI = new TH2D("Jpsipi_2d_BD_to_JPSI_PI_PI", "Jpsipi_2d_BD_to_JPSI_PI_PI", Jpsipi_bins, Jpsipi_low, Jpsipi_high, Jpsipi_bins, Jpsipi_low, Jpsipi_high);
    TH2 *JpsiK_2d_BD_to_JPSI_PI_PI = new TH2D("JpsiK_2d_BD_to_JPSI_PI_PI", "JpsiK_2d_BD_to_JPSI_PI_PI", JpsiK_bins, JpsiK_low, JpsiK_high, JpsiK_bins, JpsiK_low, JpsiK_high);
    TH2 *Kpi_2d_BD_to_JPSI_PI_PI = new TH2D("Kpi_2d_BD_to_JPSI_PI_PI", "Kpi_2d_BD_to_JPSI_PI_PI", Kpi_bins, Kpi_low, Kpi_high, Kpi_bins, Kpi_low, Kpi_high);

    TH2 *Jpsipi_2d_BS_to_JPSI_PI_PI = new TH2D("Jpsipi_2d_BS_to_JPSI_PI_PI", "Jpsipi_2d_BS_to_JPSI_PI_PI", Jpsipi_bins, Jpsipi_low, Jpsipi_high, Jpsipi_bins, Jpsipi_low, Jpsipi_high);
    TH2 *JpsiK_2d_BS_to_JPSI_PI_PI = new TH2D("JpsiK_2d_BS_to_JPSI_PI_PI", "JpsiK_2d_BS_to_JPSI_PI_PI", JpsiK_bins, JpsiK_low, JpsiK_high, JpsiK_bins, JpsiK_low, JpsiK_high);
    TH2 *Kpi_2d_BS_to_JPSI_PI_PI = new TH2D("Kpi_2d_BS_to_JPSI_PI_PI", "Kpi_2d_BS_to_JPSI_PI_PI", Kpi_bins, Kpi_low, Kpi_high, Kpi_bins, Kpi_low, Kpi_high);

    TH2 *Jpsipi_2d_BS_to_JPSI_K_K = new TH2D("Jpsipi_2d_BS_to_JPSI_K_K", "Jpsipi_2d_BS_to_JPSI_K_K", Jpsipi_bins, Jpsipi_low, Jpsipi_high, Jpsipi_bins, Jpsipi_low, Jpsipi_high);
    TH2 *JpsiK_2d_BS_to_JPSI_K_K = new TH2D("JpsiK_2d_BS_to_JPSI_K_K", "JpsiK_2d_BS_to_JPSI_K_K", JpsiK_bins, JpsiK_low, JpsiK_high, JpsiK_bins, JpsiK_low, JpsiK_high);
    TH2 *Kpi_2d_BS_to_JPSI_K_K = new TH2D("Kpi_2d_BS_to_JPSI_K_K", "Kpi_2d_BS_to_JPSI_K_K", Kpi_bins, Kpi_low, Kpi_high, Kpi_bins, Kpi_low, Kpi_high);
    
    TH2 *Jpsipi_2d_BS_to_JPSI_K_PI = new TH2D("Jpsipi_2d_BS_to_JPSI_K_PI", "Jpsipi_2d_BS_to_JPSI_K_PI", Jpsipi_bins, Jpsipi_low, Jpsipi_high, Jpsipi_bins, Jpsipi_low, Jpsipi_high);
    TH2 *JpsiK_2d_BS_to_JPSI_K_PI = new TH2D("JpsiK_2d_BS_to_JPSI_K_PI", "JpsiK_2d_BS_to_JPSI_K_PI", JpsiK_bins, JpsiK_low, JpsiK_high, JpsiK_bins, JpsiK_low, JpsiK_high);
    TH2 *Kpi_2d_BS_to_JPSI_K_PI = new TH2D("Kpi_2d_BS_to_JPSI_K_PI", "Kpi_2d_BS_to_JPSI_K_PI", Kpi_bins, Kpi_low, Kpi_high, Kpi_bins, Kpi_low, Kpi_high);
    
    TH2 *Jpsipi_2d_BD_to_JPSI_K_K = new TH2D("Jpsipi_2d_BD_to_JPSI_K_K", "Jpsipi_2d_BD_to_JPSI_K_K", Jpsipi_bins, Jpsipi_low, Jpsipi_high, Jpsipi_bins, Jpsipi_low, Jpsipi_high);
    TH2 *JpsiK_2d_BD_to_JPSI_K_K = new TH2D("JpsiK_2d_BD_to_JPSI_K_K", "JpsiK_2d_BD_to_JPSI_K_K", JpsiK_bins, JpsiK_low, JpsiK_high, JpsiK_bins, JpsiK_low, JpsiK_high);
    TH2 *Kpi_2d_BD_to_JPSI_K_K = new TH2D("Kpi_2d_BD_to_JPSI_K_K", "Kpi_2d_BD_to_JPSI_K_K", Kpi_bins, Kpi_low, Kpi_high, Kpi_bins, Kpi_low, Kpi_high);
    
    TH2 *Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI = new TH2D("Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI", "Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI", Jpsipi_bins, Jpsipi_low, Jpsipi_high, Jpsipi_bins, Jpsipi_low, Jpsipi_high);
    TH2 *JpsiK_2d_LAMBDA0B_to_JPSI_P_PI = new TH2D("JpsiK_2d_LAMBDA0B_to_JPSI_P_PI", "JpsiK_2d_LAMBDA0B_to_JPSI_P_PI", JpsiK_bins, JpsiK_low, JpsiK_high, JpsiK_bins, JpsiK_low, JpsiK_high);
    TH2 *Kpi_2d_LAMBDA0B_to_JPSI_P_PI = new TH2D("Kpi_2d_LAMBDA0B_to_JPSI_P_PI", "Kpi_2d_LAMBDA0B_to_JPSI_P_PI", Kpi_bins, Kpi_low, Kpi_high, Kpi_bins, Kpi_low, Kpi_high);
    
    TH2 *Jpsipi_2d_bg_comb = new TH2D("Jpsipi_2d_bg_comb", "Jpsipi_2d_bg_comb", Jpsipi_bins, Jpsipi_low, Jpsipi_high, Jpsipi_bins, Jpsipi_low, Jpsipi_high);
    TH2 *JpsiK_2d_bg_comb = new TH2D("JpsiK_2d_bg_comb", "JpsiK_2d_bg_comb", JpsiK_bins, JpsiK_low, JpsiK_high, JpsiK_bins, JpsiK_low, JpsiK_high);
    TH2 *Kpi_2d_bg_comb = new TH2D("Kpi_2d_bg_comb", "Kpi_2d_bg_comb", Kpi_bins, Kpi_low, Kpi_high, Kpi_bins, Kpi_low, Kpi_high);
    
    TH2 *trk_pt_bg_comb = new TH2D("trk_pt_bg_comb", "trk_pt_bg_comb", 50, 2000. * GEV, 10000. * GEV, 50, 2000. * GEV, 10000. * GEV);

    TH2 *Jpsipi_2d_LAMBDA0B_to_JPSI_P_K = new TH2D("Jpsipi_2d_LAMBDA0B_to_JPSI_P_K", "Jpsipi_2d_LAMBDA0B_to_JPSI_P_K", Jpsipi_bins, Jpsipi_low, Jpsipi_high, Jpsipi_bins, Jpsipi_low, Jpsipi_high);
    TH2 *JpsiK_2d_LAMBDA0B_to_JPSI_P_K = new TH2D("JpsiK_2d_LAMBDA0B_to_JPSI_P_K", "JpsiK_2d_LAMBDA0B_to_JPSI_P_K", JpsiK_bins, JpsiK_low, JpsiK_high, JpsiK_bins, JpsiK_low, JpsiK_high);
    TH2 *Kpi_2d_LAMBDA0B_to_JPSI_P_K = new TH2D("Kpi_2d_LAMBDA0B_to_JPSI_P_K", "Kpi_2d_LAMBDA0B_to_JPSI_P_K", Kpi_bins, Kpi_low, Kpi_high, Kpi_bins, Kpi_low, Kpi_high);
    
    TH1 *KK_1d_control_data = new TH1D("KK_1d_control_data", "KK_1d_control_data", KK_bins, KK_low, KK_high);
    TH1 *KK_1d_control_BD_to_JPSI_K_PI = new TH1D("KK_1d_control_BD_to_JPSI_K_PI", "KK_1d_control_BD_to_JPSI_K_PI", KK_bins, KK_low, KK_high);
    TH1 *KK_1d_control_BD_to_JPSI_PI_PI = new TH1D("KK_1d_control_BD_to_JPSI_PI_PI", "KK_1d_control_BD_to_JPSI_PI_PI", KK_bins, KK_low, KK_high);
    TH1 *KK_1d_control_BS_to_JPSI_PI_PI = new TH1D("KK_1d_control_BS_to_JPSI_PI_PI", "KK_1d_control_BS_to_JPSI_PI_PI", KK_bins, KK_low, KK_high);
    TH1 *KK_1d_control_BS_to_JPSI_K_K = new TH1D("KK_1d_control_BS_to_JPSI_K_K", "KK_1d_control_BS_to_JPSI_K_K", KK_bins, KK_low, KK_high);
    TH1 *KK_1d_control_LAMBDA0B_to_JPSI_P_K = new TH1D("KK_1d_control_LAMBDA0B_to_JPSI_P_K", "KK_1d_control_LAMBDA0B_to_JPSI_P_K", KK_bins, KK_low, KK_high);
    TH1 *KK_1d_control_BS_to_JPSI_K_PI = new TH1D("KK_1d_control_BS_to_JPSI_K_PI", "KK_1d_control_BS_to_JPSI_K_PI", KK_bins, KK_low, KK_high);
    TH1 *KK_1d_control_BD_to_JPSI_K_K = new TH1D("KK_1d_control_BD_to_JPSI_K_K", "KK_1d_control_BD_to_JPSI_K_K", KK_bins, KK_low, KK_high);
    TH1 *KK_1d_control_LAMBDA0B_to_JPSI_P_PI = new TH1D("KK_1d_control_LAMBDA0B_to_JPSI_P_PI", "KK_1d_control_LAMBDA0B_to_JPSI_P_PI", KK_bins, KK_low, KK_high);
    
    TH1 *JpsiK1_1d_control_data = new TH1D("JpsiK1_1d_control_data", "JpsiK1_1d_control_data", JpsiK_bins2, JpsiK_low2, JpsiK_high2);
    TH1 *JpsiK1_1d_control_BD_to_JPSI_K_PI = new TH1D("JpsiK1_1d_control_BD_to_JPSI_K_PI", "JpsiK1_1d_control_BD_to_JPSI_K_PI", JpsiK_bins2, JpsiK_low2, JpsiK_high2);
    TH1 *JpsiK1_1d_control_BD_to_JPSI_PI_PI = new TH1D("JpsiK1_1d_control_BD_to_JPSI_PI_PI", "JpsiK1_1d_control_BD_to_JPSI_PI_PI", JpsiK_bins2, JpsiK_low2, JpsiK_high2);
    TH1 *JpsiK1_1d_control_BS_to_JPSI_PI_PI = new TH1D("JpsiK1_1d_control_BS_to_JPSI_PI_PI", "JpsiK1_1d_control_BS_to_JPSI_PI_PI", JpsiK_bins2, JpsiK_low2, JpsiK_high2);
    TH1 *JpsiK1_1d_control_BS_to_JPSI_K_K = new TH1D("JpsiK1_1d_control_BS_to_JPSI_K_K", "JpsiK1_1d_control_BS_to_JPSI_K_K", JpsiK_bins2, JpsiK_low2, JpsiK_high2);
    TH1 *JpsiK1_1d_control_LAMBDA0B_to_JPSI_P_K = new TH1D("JpsiK1_1d_control_LAMBDA0B_to_JPSI_P_K", "JpsiK1_1d_control_LAMBDA0B_to_JPSI_P_K", JpsiK_bins2, JpsiK_low2, JpsiK_high2);
    TH1 *JpsiK1_1d_control_BS_to_JPSI_K_PI = new TH1D("JpsiK1_1d_control_BS_to_JPSI_K_PI", "JpsiK1_1d_control_BS_to_JPSI_K_PI", JpsiK_bins2, JpsiK_low2, JpsiK_high2);
    TH1 *JpsiK1_1d_control_BD_to_JPSI_K_K = new TH1D("JpsiK1_1d_control_BD_to_JPSI_K_K", "JpsiK1_1d_control_BD_to_JPSI_K_K", JpsiK_bins2, JpsiK_low2, JpsiK_high2);
    TH1 *JpsiK1_1d_control_LAMBDA0B_to_JPSI_P_PI = new TH1D("JpsiK1_1d_control_LAMBDA0B_to_JPSI_P_PI", "JpsiK1_1d_control_LAMBDA0B_to_JPSI_P_PI", JpsiK_bins2, JpsiK_low2, JpsiK_high2);
    
    TH1 *JpsiK2_1d_control_data = new TH1D("JpsiK2_1d_control_data", "JpsiK2_1d_control_data", JpsiK_bins2, JpsiK_low2, JpsiK_high2);
    TH1 *JpsiK2_1d_control_BD_to_JPSI_K_PI = new TH1D("JpsiK2_1d_control_BD_to_JPSI_K_PI", "JpsiK2_1d_control_BD_to_JPSI_K_PI", JpsiK_bins2, JpsiK_low2, JpsiK_high2);
    TH1 *JpsiK2_1d_control_BD_to_JPSI_PI_PI = new TH1D("JpsiK2_1d_control_BD_to_JPSI_PI_PI", "JpsiK2_1d_control_BD_to_JPSI_PI_PI", JpsiK_bins2, JpsiK_low2, JpsiK_high2);
    TH1 *JpsiK2_1d_control_BS_to_JPSI_PI_PI = new TH1D("JpsiK2_1d_control_BS_to_JPSI_PI_PI", "JpsiK2_1d_control_BS_to_JPSI_PI_PI", JpsiK_bins2, JpsiK_low2, JpsiK_high2);
    TH1 *JpsiK2_1d_control_BS_to_JPSI_K_K = new TH1D("JpsiK2_1d_control_BS_to_JPSI_K_K", "JpsiK2_1d_control_BS_to_JPSI_K_K", JpsiK_bins2, JpsiK_low2, JpsiK_high2);
    TH1 *JpsiK2_1d_control_LAMBDA0B_to_JPSI_P_K = new TH1D("JpsiK2_1d_control_LAMBDA0B_to_JPSI_P_K", "JpsiK2_1d_control_LAMBDA0B_to_JPSI_P_K", JpsiK_bins2, JpsiK_low2, JpsiK_high2);
    TH1 *JpsiK2_1d_control_BS_to_JPSI_K_PI = new TH1D("JpsiK2_1d_control_BS_to_JPSI_K_PI", "JpsiK2_1d_control_BS_to_JPSI_K_PI", JpsiK_bins2, JpsiK_low2, JpsiK_high2);
    TH1 *JpsiK2_1d_control_BD_to_JPSI_K_K = new TH1D("JpsiK2_1d_control_BD_to_JPSI_K_K", "JpsiK2_1d_control_BD_to_JPSI_K_K", JpsiK_bins2, JpsiK_low2, JpsiK_high2);
    TH1 *JpsiK2_1d_control_LAMBDA0B_to_JPSI_P_PI = new TH1D("JpsiK2_1d_control_LAMBDA0B_to_JPSI_P_PI", "JpsiK2_1d_control_LAMBDA0B_to_JPSI_P_PI", JpsiK_bins2, JpsiK_low2, JpsiK_high2);
    
    TH2 *JpsiK_2d_control_data = new TH2D("JpsiK_2d_control_data", "JpsiK_2d_control_data", JpsiK_bins3, JpsiK_low3, JpsiK_high3, JpsiK_bins3, JpsiK_low3, JpsiK_high3);
    TH2 *JpsiK_2d_control_BD_to_JPSI_K_PI = new TH2D("JpsiK_2d_control_BD_to_JPSI_K_PI", "JpsiK_2d_control_BD_to_JPSI_K_PI", JpsiK_bins3, JpsiK_low3, JpsiK_high3, JpsiK_bins3, JpsiK_low3, JpsiK_high3);
    TH2 *JpsiK_2d_control_BD_to_JPSI_PI_PI = new TH2D("JpsiK_2d_control_BD_to_JPSI_PI_PI", "JpsiK_2d_control_BD_to_JPSI_PI_PI", JpsiK_bins3, JpsiK_low3, JpsiK_high3, JpsiK_bins3, JpsiK_low3, JpsiK_high3);
    TH2 *JpsiK_2d_control_BS_to_JPSI_PI_PI = new TH2D("JpsiK_2d_control_BS_to_JPSI_PI_PI", "JpsiK_2d_control_BS_to_JPSI_PI_PI", JpsiK_bins3, JpsiK_low3, JpsiK_high3, JpsiK_bins3, JpsiK_low3, JpsiK_high3);
    TH2 *JpsiK_2d_control_BS_to_JPSI_K_K = new TH2D("JpsiK_2d_control_BS_to_JPSI_K_K", "JpsiK_2d_control_BS_to_JPSI_K_K", JpsiK_bins3, JpsiK_low3, JpsiK_high3, JpsiK_bins3, JpsiK_low3, JpsiK_high3);
    TH2 *JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K = new TH2D("JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K", "JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K", JpsiK_bins3, JpsiK_low3, JpsiK_high3, JpsiK_bins3, JpsiK_low3, JpsiK_high3);
    TH2 *JpsiK_2d_control_BS_to_JPSI_K_PI = new TH2D("JpsiK_2d_control_BS_to_JPSI_K_PI", "JpsiK_2d_control_BS_to_JPSI_K_PI", JpsiK_bins3, JpsiK_low3, JpsiK_high3, JpsiK_bins3, JpsiK_low3, JpsiK_high3);
    TH2 *JpsiK_2d_control_BD_to_JPSI_K_K = new TH2D("JpsiK_2d_control_BD_to_JPSI_K_K", "JpsiK_2d_control_BD_to_JPSI_K_K", JpsiK_bins3, JpsiK_low3, JpsiK_high3, JpsiK_bins3, JpsiK_low3, JpsiK_high3);
    TH2 *JpsiK_2d_control_LAMBDA0B_to_JPSI_P_PI = new TH2D("JpsiK_2d_control_LAMBDA0B_to_JPSI_P_PI", "JpsiK_2d_control_LAMBDA0B_to_JPSI_P_PI", JpsiK_bins3, JpsiK_low3, JpsiK_high3, JpsiK_bins3, JpsiK_low3, JpsiK_high3);
    
    TH2 *JpsiK_2d_controlLb_data = new TH2D("JpsiK_2d_controlLb_data", "JpsiK_2d_controlLb_data", JpsiK_bins4, JpsiK_low4, JpsiK_high4, JpsiK_bins4, JpsiK_low4, JpsiK_high4);
    TH2 *JpsiK_2d_controlLb_BD_to_JPSI_K_PI = new TH2D("JpsiK_2d_controlLb_BD_to_JPSI_K_PI", "JpsiK_2d_controlLb_BD_to_JPSI_K_PI", JpsiK_bins4, JpsiK_low4, JpsiK_high4, JpsiK_bins4, JpsiK_low4, JpsiK_high4);
    TH2 *JpsiK_2d_controlLb_BD_to_JPSI_PI_PI = new TH2D("JpsiK_2d_controlLb_BD_to_JPSI_PI_PI", "JpsiK_2d_controlLb_BD_to_JPSI_PI_PI", JpsiK_bins4, JpsiK_low4, JpsiK_high4, JpsiK_bins4, JpsiK_low4, JpsiK_high4);
    TH2 *JpsiK_2d_controlLb_BS_to_JPSI_PI_PI = new TH2D("JpsiK_2d_controlLb_BS_to_JPSI_PI_PI", "JpsiK_2d_controlLb_BS_to_JPSI_PI_PI", JpsiK_bins4, JpsiK_low4, JpsiK_high4, JpsiK_bins4, JpsiK_low4, JpsiK_high4);
    TH2 *JpsiK_2d_controlLb_BS_to_JPSI_K_K = new TH2D("JpsiK_2d_controlLb_BS_to_JPSI_K_K", "JpsiK_2d_controlLb_BS_to_JPSI_K_K", JpsiK_bins4, JpsiK_low4, JpsiK_high4, JpsiK_bins4, JpsiK_low4, JpsiK_high4);
    
	TH2 *JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K = new TH2D("JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K", "JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K", JpsiK_bins4, JpsiK_low4, JpsiK_high4, JpsiK_bins4, JpsiK_low4, JpsiK_high4);
    TH2 *JpsiK_2d_controlLb_BS_to_JPSI_K_PI = new TH2D("JpsiK_2d_controlLb_BS_to_JPSI_K_PI", "JpsiK_2d_controlLb_BS_to_JPSI_K_PI", JpsiK_bins4, JpsiK_low4, JpsiK_high4, JpsiK_bins4, JpsiK_low4, JpsiK_high4);
    TH2 *JpsiK_2d_controlLb_BD_to_JPSI_K_K = new TH2D("JpsiK_2d_controlLb_BD_to_JPSI_K_K", "JpsiK_2d_controlLb_BD_to_JPSI_K_K", JpsiK_bins4, JpsiK_low4, JpsiK_high4, JpsiK_bins4, JpsiK_low4, JpsiK_high4);
    TH2 *JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI = new TH2D("JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI", "JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI", JpsiK_bins4, JpsiK_low4, JpsiK_high4, JpsiK_bins4, JpsiK_low4, JpsiK_high4);
    
    TH2 *Jpsip_2d_controlLb_data = new TH2D("Jpsip_2d_controlLb_data", "Jpsip_2d_controlLb_data", Jpsip_bins, Jpsip_low, Jpsip_high, Jpsip_bins, Jpsip_low, Jpsip_high);
    TH2 *Jpsip_2d_controlLb_BD_to_JPSI_K_PI = new TH2D("Jpsip_2d_controlLb_BD_to_JPSI_K_PI", "Jpsip_2d_controlLb_BD_to_JPSI_K_PI", Jpsip_bins, Jpsip_low, Jpsip_high, Jpsip_bins, Jpsip_low, Jpsip_high);
    TH2 *Jpsip_2d_controlLb_BD_to_JPSI_PI_PI = new TH2D("Jpsip_2d_controlLb_BD_to_JPSI_PI_PI", "Jpsip_2d_controlLb_BD_to_JPSI_PI_PI", Jpsip_bins, Jpsip_low, Jpsip_high, Jpsip_bins, Jpsip_low, Jpsip_high);
    TH2 *Jpsip_2d_controlLb_BS_to_JPSI_PI_PI = new TH2D("Jpsip_2d_controlLb_BS_to_JPSI_PI_PI", "Jpsip_2d_controlLb_BS_to_JPSI_PI_PI", Jpsip_bins, Jpsip_low, Jpsip_high, Jpsip_bins, Jpsip_low, Jpsip_high);
    TH2 *Jpsip_2d_controlLb_BS_to_JPSI_K_K = new TH2D("Jpsip_2d_controlLb_BS_to_JPSI_K_K", "Jpsip_2d_controlLb_BS_to_JPSI_K_K", Jpsip_bins, Jpsip_low, Jpsip_high, Jpsip_bins, Jpsip_low, Jpsip_high);
    TH2 *Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K = new TH2D("Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K", "Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K", Jpsip_bins, Jpsip_low, Jpsip_high, Jpsip_bins, Jpsip_low, Jpsip_high);
    TH2 *Jpsip_2d_controlLb_BS_to_JPSI_K_PI = new TH2D("Jpsip_2d_controlLb_BS_to_JPSI_K_PI", "Jpsip_2d_controlLb_BS_to_JPSI_K_PI", Jpsip_bins, Jpsip_low, Jpsip_high, Jpsip_bins, Jpsip_low, Jpsip_high);
    TH2 *Jpsip_2d_controlLb_BD_to_JPSI_K_K = new TH2D("Jpsip_2d_controlLb_BD_to_JPSI_K_K", "Jpsip_2d_controlLb_BD_to_JPSI_K_K", Jpsip_bins, Jpsip_low, Jpsip_high, Jpsip_bins, Jpsip_low, Jpsip_high);
    TH2 *Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_PI = new TH2D("Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_PI", "Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_PI", Jpsip_bins, Jpsip_low, Jpsip_high, Jpsip_bins, Jpsip_low, Jpsip_high);
    
    TH2 *pK_2d_controlLb_data = new TH2D("pK_2d_controlLb_data", "pK_2d_controlLb_data", pK_bins, pK_low, pK_high, pK_bins, pK_low, pK_high);
    TH2 *pK_2d_controlLb_BD_to_JPSI_K_PI = new TH2D("pK_2d_controlLb_BD_to_JPSI_K_PI", "pK_2d_controlLb_BD_to_JPSI_K_PI", pK_bins, pK_low, pK_high, pK_bins, pK_low, pK_high);
    TH2 *pK_2d_controlLb_BD_to_JPSI_PI_PI = new TH2D("pK_2d_controlLb_BD_to_JPSI_PI_PI", "pK_2d_controlLb_BD_to_JPSI_PI_PI", pK_bins, pK_low, pK_high, pK_bins, pK_low, pK_high);
    TH2 *pK_2d_controlLb_BS_to_JPSI_PI_PI = new TH2D("pK_2d_controlLb_BS_to_JPSI_PI_PI", "pK_2d_controlLb_BS_to_JPSI_PI_PI", pK_bins, pK_low, pK_high, pK_bins, pK_low, pK_high);
    TH2 *pK_2d_controlLb_BS_to_JPSI_K_K = new TH2D("pK_2d_controlLb_BS_to_JPSI_K_K", "pK_2d_controlLb_BS_to_JPSI_K_K", pK_bins, pK_low, pK_high, pK_bins, pK_low, pK_high);
    TH2 *pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K = new TH2D("pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K", "pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K", pK_bins, pK_low, pK_high, pK_bins, pK_low, pK_high);
    TH2 *pK_2d_controlLb_BS_to_JPSI_K_PI = new TH2D("pK_2d_controlLb_BS_to_JPSI_K_PI", "pK_2d_controlLb_BS_to_JPSI_K_PI", pK_bins, pK_low, pK_high, pK_bins, pK_low, pK_high);
    TH2 *pK_2d_controlLb_BD_to_JPSI_K_K = new TH2D("pK_2d_controlLb_BD_to_JPSI_K_K", "pK_2d_controlLb_BD_to_JPSI_K_K", pK_bins, pK_low, pK_high, pK_bins, pK_low, pK_high);
    TH2 *pK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI = new TH2D("pK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI", "pK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI", pK_bins, pK_low, pK_high, pK_bins, pK_low, pK_high);

    TH2 *Kpi_piK_2d_data_for_ea = new TH2D("Kpi_piK_2d_data_for_ea", "Kpi_piK_2d_data_for_ea", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);
    TH2 *Kpi_piK_2d_full_for_ea = new TH2D("Kpi_piK_2d_full_for_ea", "Kpi_piK_2d_full_for_ea", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);
    TH2 *Kpi_piK_2d_bg_for_ea = new TH2D("Kpi_piK_2d_bg_for_ea", "Kpi_piK_2d_bg_for_ea", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);
    TH2 *Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea = new TH2D("Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea", "Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2); 
    TH2 *Kpi_piK_2d_BD_to_JPSI_PI_PI_for_ea = new TH2D("Kpi_piK_2d_BD_to_JPSI_PI_PI_for_ea", "Kpi_piK_2d_BD_to_JPSI_PI_PI_for_ea", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);
    TH2 *Kpi_piK_2d_BS_to_JPSI_PI_PI_for_ea = new TH2D("Kpi_piK_2d_BS_to_JPSI_PI_PI_for_ea", "Kpi_piK_2d_BS_to_JPSI_PI_PI_for_ea", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);
    TH2 *Kpi_piK_2d_BS_to_JPSI_K_K_for_ea = new TH2D("Kpi_piK_2d_BS_to_JPSI_K_K_for_ea", "Kpi_piK_2d_BS_to_JPSI_K_K_for_ea", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2); 
    TH2 *Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K_for_ea = new TH2D("Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K_for_ea", "Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K_for_ea", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);  
    TH2 *Kpi_piK_2d_BS_to_JPSI_K_PI_for_ea = new TH2D("Kpi_piK_2d_BS_to_JPSI_K_PI_for_ea", "Kpi_piK_2d_BS_to_JPSI_K_PI_for_ea", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);
    TH2 *Kpi_piK_2d_BD_to_JPSI_K_K_for_ea = new TH2D("Kpi_piK_2d_BD_to_JPSI_K_K_for_ea", "Kpi_piK_2d_BD_to_JPSI_K_K_for_ea", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2); 
    TH2 *Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI_for_ea = new TH2D("Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI_for_ea", "Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI_for_ea", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);

    TH2 *pK_Kp_2d_data = new TH2D("pK_Kp_2d_data", "pK_Kp_2d_data", pK_Kp_bins3, pK_Kp_low3, pK_Kp_high3, pK_Kp_bins4, pK_Kp_low4, pK_Kp_high4);
    TH2 *pK_Kp_2d_full = new TH2D("pK_Kp_2d_full", "pK_Kp_2d_full", pK_Kp_bins3, pK_Kp_low3, pK_Kp_high3, pK_Kp_bins4, pK_Kp_low4, pK_Kp_high4);
    TH2 *pK_Kp_2d_bg = new TH2D("pK_Kp_2d_bg", "pK_Kp_2d_bg", pK_Kp_bins3, pK_Kp_low3, pK_Kp_high3, pK_Kp_bins4, pK_Kp_low4, pK_Kp_high4);
    TH2 *pK_Kp_2d_BD_to_JPSI_K_PI = new TH2D("pK_Kp_2d_BD_to_JPSI_K_PI", "Kpi_piK_2d_BD_to_JPSI_K_PI", pK_Kp_bins3, pK_Kp_low3, pK_Kp_high3, pK_Kp_bins4, pK_Kp_low4, pK_Kp_high4); 
    TH2 *pK_Kp_2d_BD_to_JPSI_PI_PI = new TH2D("pK_Kp_2d_BD_to_JPSI_PI_PI", "pK_Kp_2d_BD_to_JPSI_PI_PI", pK_Kp_bins3, pK_Kp_low3, pK_Kp_high3, pK_Kp_bins4, pK_Kp_low4, pK_Kp_high4);
    TH2 *pK_Kp_2d_BS_to_JPSI_PI_PI = new TH2D("pK_Kp_2d_BS_to_JPSI_PI_PI", "pK_Kp_2d_BS_to_JPSI_PI_PI",  pK_Kp_bins3, pK_Kp_low3, pK_Kp_high3, pK_Kp_bins4, pK_Kp_low4, pK_Kp_high4);
    TH2 *pK_Kp_2d_BS_to_JPSI_K_K = new TH2D("pK_Kp_2d_BS_to_JPSI_K_K", "pK_Kp_2d_BS_to_JPSI_K_K", pK_Kp_bins3, pK_Kp_low3, pK_Kp_high3, pK_Kp_bins4, pK_Kp_low4, pK_Kp_high4); 
    TH2 *pK_Kp_2d_LAMBDA0B_to_JPSI_P_K = new TH2D("pK_Kp_2d_LAMBDA0B_to_JPSI_P_K", "pK_Kp_2d_LAMBDA0B_to_JPSI_P_K", pK_Kp_bins3, pK_Kp_low3, pK_Kp_high3, pK_Kp_bins4, pK_Kp_low4, pK_Kp_high4);  
    TH1 *pK_1d_bg = new TH1D("pK_1d_bg", "pK_1d_bg", 40, 5150. * GEV, 7500. * GEV);
    TH1 *pK_1d_full = new TH1D("pK_1d_full", "pK_1d_full", 40, 5150. * GEV, 7500. * GEV);
    TH2 *pK_Kp_2d_BS_to_JPSI_K_PI = new TH2D("pK_Kp_2d_BS_to_JPSI_K_PI", "pK_Kp_2d_BS_to_JPSI_K_PI",  pK_Kp_bins, pK_Kp_low, pK_Kp_high, pK_Kp_bins2, pK_Kp_low2, pK_Kp_high2);
    TH2 *pK_Kp_2d_BD_to_JPSI_K_K = new TH2D("pK_Kp_2d_BD_to_JPSI_K_K", "pK_Kp_2d_BD_to_JPSI_K_K", pK_Kp_bins, pK_Kp_low, pK_Kp_high, pK_Kp_bins2, pK_Kp_low2, pK_Kp_high2); 
    TH2 *pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI = new TH2D("pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI", "pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI", pK_Kp_bins, pK_Kp_low, pK_Kp_high, pK_Kp_bins2, pK_Kp_low2, pK_Kp_high2);
    
    TH2 *pK_Kp_2d_test_data = new TH2D("pK_Kp_2d_test_data", "pK_Kp_2d_test_data", pK_Kp_bins, pK_Kp_low, pK_Kp_high, pK_Kp_bins2, pK_Kp_low2, pK_Kp_high2);
    TH2 *pK_Kp_2d_test_full = new TH2D("pK_Kp_2d_test_full", "pK_Kp_2d_test_full", pK_Kp_bins, pK_Kp_low, pK_Kp_high, pK_Kp_bins2, pK_Kp_low2, pK_Kp_high2);
    TH2 *pK_Kp_2d_test_bg = new TH2D("pK_Kp_2d_test_bg", "pK_Kp_2d_test_bg", pK_Kp_bins, pK_Kp_low, pK_Kp_high, pK_Kp_bins2, pK_Kp_low2, pK_Kp_high2);
    TH2 *pK_Kp_2d_test_BD_to_JPSI_K_PI = new TH2D("pK_Kp_2d_test_BD_to_JPSI_K_PI", "Kpi_piK_2d_test_BD_to_JPSI_K_PI", pK_Kp_bins, pK_Kp_low, pK_Kp_high, pK_Kp_bins2, pK_Kp_low2, pK_Kp_high2); 
    TH2 *pK_Kp_2d_test_BD_to_JPSI_PI_PI = new TH2D("pK_Kp_2d_test_BD_to_JPSI_PI_PI", "pK_Kp_2d_test_BD_to_JPSI_PI_PI", pK_Kp_bins, pK_Kp_low, pK_Kp_high, pK_Kp_bins2, pK_Kp_low2, pK_Kp_high2);
    TH2 *pK_Kp_2d_test_BS_to_JPSI_PI_PI = new TH2D("pK_Kp_2d_test_BS_to_JPSI_PI_PI", "pK_Kp_2d_test_BS_to_JPSI_PI_PI",  pK_Kp_bins, pK_Kp_low, pK_Kp_high, pK_Kp_bins2, pK_Kp_low2, pK_Kp_high2);
    TH2 *pK_Kp_2d_test_BS_to_JPSI_K_K = new TH2D("pK_Kp_2d_test_BS_to_JPSI_K_K", "pK_Kp_2d_test_BS_to_JPSI_K_K", pK_Kp_bins, pK_Kp_low, pK_Kp_high, pK_Kp_bins2, pK_Kp_low2, pK_Kp_high2); 
    TH2 *pK_Kp_2d_test_LAMBDA0B_to_JPSI_P_K = new TH2D("pK_Kp_2d_test_LAMBDA0B_to_JPSI_P_K", "pK_Kp_2d_test_LAMBDA0B_to_JPSI_P_K", pK_Kp_bins, pK_Kp_low, pK_Kp_high, pK_Kp_bins2, pK_Kp_low2, pK_Kp_high2); 
    
    TH2 *pK_Kp_2d_rotate_data = new TH2D("pK_Kp_2d_rotate_data", "pK_Kp_2d_rotate_data", pK_Kp_bins5, pK_Kp_low5, pK_Kp_high5, pK_Kp_bins6, pK_Kp_low6, pK_Kp_high6);
    TH2 *pK_Kp_2d_rotate_BD_to_JPSI_K_PI = new TH2D("pK_Kp_2d_rotate_BD_to_JPSI_K_PI", "pK_Kp_2d_rotate_BD_to_JPSI_K_PI", pK_Kp_bins5, pK_Kp_low5, pK_Kp_high5, pK_Kp_bins6, pK_Kp_low6, pK_Kp_high6);
    TH2 *pK_Kp_2d_rotate_BD_to_JPSI_PI_PI = new TH2D("pK_Kp_2d_rotate_BD_to_JPSI_PI_PI", "pK_Kp_2d_rotate_BD_to_JPSI_PI_PI", pK_Kp_bins5, pK_Kp_low5, pK_Kp_high5, pK_Kp_bins6, pK_Kp_low6, pK_Kp_high6);
    TH2 *pK_Kp_2d_rotate_BS_to_JPSI_PI_PI = new TH2D("pK_Kp_2d_rotate_BS_to_JPSI_PI_PI", "pK_Kp_2d_rotate_BS_to_JPSI_PI_PI", pK_Kp_bins5, pK_Kp_low5, pK_Kp_high5, pK_Kp_bins6, pK_Kp_low6, pK_Kp_high6);
    TH2 *pK_Kp_2d_rotate_BS_to_JPSI_K_K = new TH2D("pK_Kp_2d_rotate_BS_to_JPSI_K_K", "pK_Kp_2d_rotate_BS_to_JPSI_K_K", pK_Kp_bins5, pK_Kp_low5, pK_Kp_high5, pK_Kp_bins6, pK_Kp_low6, pK_Kp_high6);
    TH2 *pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_K = new TH2D("pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_K", "pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_K", pK_Kp_bins5, pK_Kp_low5, pK_Kp_high5, pK_Kp_bins6, pK_Kp_low6, pK_Kp_high6);
    TH2 *pK_Kp_2d_rotate_BS_to_JPSI_K_PI = new TH2D("pK_Kp_2d_rotate_BS_to_JPSI_K_PI", "pK_Kp_2d_rotate_BS_to_JPSI_K_PI", pK_Kp_bins5, pK_Kp_low5, pK_Kp_high5, pK_Kp_bins6, pK_Kp_low6, pK_Kp_high6);
    TH2 *pK_Kp_2d_rotate_BD_to_JPSI_K_K = new TH2D("pK_Kp_2d_rotate_BD_to_JPSI_K_K", "pK_Kp_2d_rotate_BD_to_JPSI_K_K", pK_Kp_bins5, pK_Kp_low5, pK_Kp_high5, pK_Kp_bins6, pK_Kp_low6, pK_Kp_high6);
    TH2 *pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_PI = new TH2D("pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_PI", "pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_PI", pK_Kp_bins5, pK_Kp_low5, pK_Kp_high5, pK_Kp_bins6, pK_Kp_low6, pK_Kp_high6);
    
    TH2 *KK_pipi_2d_data = new TH2D("KK_pipi_2d_data", "KK_pipi_2d_data", KK_pipi_bins, KK_pipi_low, KK_pipi_high, KK_pipi_bins2, KK_pipi_low2, KK_pipi_high2);
    TH2 *KK_pipi_2d_BD_to_JPSI_K_PI = new TH2D("KK_pipi_2d_BD_to_JPSI_K_PI", "KK_pipi_2d_BD_to_JPSI_K_PI", KK_pipi_bins, KK_pipi_low, KK_pipi_high, KK_pipi_bins2, KK_pipi_low2, KK_pipi_high2);
    TH2 *KK_pipi_2d_BD_to_JPSI_PI_PI = new TH2D("KK_pipi_2d_BD_to_JPSI_PI_PI", "KK_pipi_2d_BD_to_JPSI_PI_PI", KK_pipi_bins, KK_pipi_low, KK_pipi_high, KK_pipi_bins2, KK_pipi_low2, KK_pipi_high2);
    TH2 *KK_pipi_2d_BS_to_JPSI_PI_PI = new TH2D("KK_pipi_2d_BS_to_JPSI_PI_PI", "KK_pipi_2d_BS_to_JPSI_PI_PI", KK_pipi_bins, KK_pipi_low, KK_pipi_high, KK_pipi_bins2, KK_pipi_low2, KK_pipi_high2);
    TH2 *KK_pipi_2d_BS_to_JPSI_K_K = new TH2D("KK_pipi_2d_BS_to_JPSI_K_K", "KK_pipi_2d_BS_to_JPSI_K_K", KK_pipi_bins, KK_pipi_low, KK_pipi_high, KK_pipi_bins2, KK_pipi_low2, KK_pipi_high2);
    TH2 *KK_pipi_2d_LAMBDA0B_to_JPSI_P_K = new TH2D("KK_pipi_2d_LAMBDA0B_to_JPSI_P_K", "KK_pipi_2d_LAMBDA0B_to_JPSI_P_K", KK_pipi_bins, KK_pipi_low, KK_pipi_high, KK_pipi_bins2, KK_pipi_low2, KK_pipi_high2);
    TH2 *KK_pipi_2d_BS_to_JPSI_K_PI = new TH2D("KK_pipi_2d_BS_to_JPSI_K_PI", "KK_pipi_2d_BS_to_JPSI_K_PI", KK_pipi_bins, KK_pipi_low, KK_pipi_high, KK_pipi_bins2, KK_pipi_low2, KK_pipi_high2);
    TH2 *KK_pipi_2d_BD_to_JPSI_K_K = new TH2D("KK_pipi_2d_BD_to_JPSI_K_K", "KK_pipi_2d_BD_to_JPSI_K_K", KK_pipi_bins, KK_pipi_low, KK_pipi_high, KK_pipi_bins2, KK_pipi_low2, KK_pipi_high2);
    TH2 *KK_pipi_2d_LAMBDA0B_to_JPSI_P_PI = new TH2D("KK_pipi_2d_LAMBDA0B_to_JPSI_P_PI", "KK_pipi_2d_LAMBDA0B_to_JPSI_P_PI", KK_pipi_bins, KK_pipi_low, KK_pipi_high, KK_pipi_bins2, KK_pipi_low2, KK_pipi_high2);
    
    TH1 *KK_1d_data = new TH1D("KK_1d_data", "KK_1d_data", KK_bins2, KK_low2, KK_high2);
    TH1 *KK_1d_BD_to_JPSI_K_PI = new TH1D("KK_1d_BD_to_JPSI_K_PI", "KK_1d_BD_to_JPSI_K_PI", KK_bins2, KK_low2, KK_high2);
    TH1 *KK_1d_BD_to_JPSI_PI_PI = new TH1D("KK_1d_BD_to_JPSI_PI_PI", "KK_1d_BD_to_JPSI_PI_PI", KK_bins2, KK_low2, KK_high2);
    TH1 *KK_1d_BS_to_JPSI_PI_PI = new TH1D("KK_1d_BS_to_JPSI_PI_PI", "KK_1d_BS_to_JPSI_PI_PI", KK_bins2, KK_low2, KK_high2);
    TH1 *KK_1d_BS_to_JPSI_K_K = new TH1D("KK_1d_BS_to_JPSI_K_K", "KK_1d_BS_to_JPSI_K_K", KK_bins2, KK_low2, KK_high2);
    TH1 *KK_1d_LAMBDA0B_to_JPSI_P_K = new TH1D("KK_1d_LAMBDA0B_to_JPSI_P_K", "KK_1d_LAMBDA0B_to_JPSI_P_K", KK_bins2, KK_low2, KK_high2);
    TH1 *KK_1d_bg = new TH1D("KK_1d_bg", "KK_1d_bg", KK_bins2, KK_low2, KK_high2);
    TH1 *KK_1d_BS_to_JPSI_K_PI = new TH1D("KK_1d_BS_to_JPSI_K_PI", "KK_1d_BS_to_JPSI_K_PI", KK_bins2, KK_low2, KK_high2);
    TH1 *KK_1d_BD_to_JPSI_K_K = new TH1D("KK_1d_BD_to_JPSI_K_K", "KK_1d_BD_to_JPSI_K_K", KK_bins2, KK_low2, KK_high2);
    TH1 *KK_1d_LAMBDA0B_to_JPSI_P_PI = new TH1D("KK_1d_LAMBDA0B_to_JPSI_P_PI", "KK_1d_LAMBDA0B_to_JPSI_P_PI", KK_bins2, KK_low2, KK_high2);
    
    TH1 *pipi_1d_data = new TH1D("pipi_1d_data", "pipi_1d_data", pipi_bins, pipi_low, pipi_high);
    TH1 *pipi_1d_BD_to_JPSI_K_PI = new TH1D("pipi_1d_BD_to_JPSI_K_PI", "pipi_1d_BD_to_JPSI_K_PI", pipi_bins, pipi_low, pipi_high);
    TH1 *pipi_1d_BD_to_JPSI_PI_PI = new TH1D("pipi_1d_BD_to_JPSI_PI_PI", "pipi_1d_BD_to_JPSI_PI_PI", pipi_bins, pipi_low, pipi_high);
    TH1 *pipi_1d_BS_to_JPSI_PI_PI = new TH1D("pipi_1d_BS_to_JPSI_PI_PI", "pipi_1d_BS_to_JPSI_PI_PI", pipi_bins, pipi_low, pipi_high);
    TH1 *pipi_1d_BS_to_JPSI_K_K = new TH1D("pipi_1d_BS_to_JPSI_K_K", "pipi_1d_BS_to_JPSI_K_K", pipi_bins, pipi_low, pipi_high);
    TH1 *pipi_1d_LAMBDA0B_to_JPSI_P_K = new TH1D("pipi_1d_LAMBDA0B_to_JPSI_P_K", "pipi_1d_LAMBDA0B_to_JPSI_P_K", pipi_bins, pipi_low, pipi_high);
    TH1 *pipi_1d_BS_to_JPSI_K_PI = new TH1D("pipi_1d_BS_to_JPSI_K_PI", "pipi_1d_BS_to_JPSI_K_PI", pipi_bins, pipi_low, pipi_high);
    TH1 *pipi_1d_BD_to_JPSI_K_K = new TH1D("pipi_1d_BD_to_JPSI_K_K", "pipi_1d_BD_to_JPSI_K_K", pipi_bins, pipi_low, pipi_high);
    TH1 *pipi_1d_LAMBDA0B_to_JPSI_P_PI = new TH1D("pipi_1d_LAMBDA0B_to_JPSI_P_PI", "pipi_1d_LAMBDA0B_to_JPSI_P_PI", pipi_bins, pipi_low, pipi_high);
    
    TH1 *pipi_1d_data_sb = new TH1D("pipi_1d_data_sb", "pipi_1d_data_sb", pipi_bins2, pipi_low2, pipi_high2);
    TH1 *pipi_1d_BD_to_JPSI_K_PI_sb = new TH1D("pipi_1d_BD_to_JPSI_K_PI_sb", "pipi_1d_BD_to_JPSI_K_PI_sb", pipi_bins2, pipi_low2, pipi_high2);
    TH1 *pipi_1d_BD_to_JPSI_PI_PI_sb = new TH1D("pipi_1d_BD_to_JPSI_PI_PI_sb", "pipi_1d_BD_to_JPSI_PI_PI_sb", pipi_bins2, pipi_low2, pipi_high2);
    TH1 *pipi_1d_BS_to_JPSI_PI_PI_sb = new TH1D("pipi_1d_BS_to_JPSI_PI_PI_sb", "pipi_1d_BS_to_JPSI_PI_PI_sb", pipi_bins2, pipi_low2, pipi_high2);
    TH1 *pipi_1d_BS_to_JPSI_K_K_sb = new TH1D("pipi_1d_BS_to_JPSI_K_K_sb", "pipi_1d_BS_to_JPSI_K_K_sb", pipi_bins2, pipi_low2, pipi_high2);
    TH1 *pipi_1d_LAMBDA0B_to_JPSI_P_K_sb = new TH1D("pipi_1d_LAMBDA0B_to_JPSI_P_K_sb", "pipi_1d_LAMBDA0B_to_JPSI_P_K_sb", pipi_bins2, pipi_low2, pipi_high2);
    TH1 *pipi_1d_BS_to_JPSI_K_PI_sb = new TH1D("pipi_1d_BS_to_JPSI_K_PI_sb", "pipi_1d_BS_to_JPSI_K_PI_sb", pipi_bins2, pipi_low2, pipi_high2);
    TH1 *pipi_1d_BD_to_JPSI_K_K_sb = new TH1D("pipi_1d_BD_to_JPSI_K_K_sb", "pipi_1d_BD_to_JPSI_K_K_sb", pipi_bins2, pipi_low2, pipi_high2);
    TH1 *pipi_1d_LAMBDA0B_to_JPSI_P_PI_sb = new TH1D("pipi_1d_LAMBDA0B_to_JPSI_P_PI_sb", "pipi_1d_LAMBDA0B_to_JPSI_P_PI_sb", pipi_bins2, pipi_low2, pipi_high2);
    
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__|| File creating||__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
    
    TFile *f_sa =new TFile("Zc1plus_fit/input/ROOT_files/signal_area_Zc1plus.root", "recreate");
    TTree *for_me = new TTree("for_me", "for_me");
    TTree *for_me_BS_to_JPSI_K_K = new TTree("for_me_BS_to_JPSI_K_K", "for_me_BS_to_JPSI_K_K");
    
    TFile *f_ca =new TFile("Zc1plus_fit/input/ROOT_files/control_area_BsKK_Zc1plus.root", "recreate");
    TTree *for_BsJpsiKK_control_BD_to_JPSI_K_PI = new TTree("for_BsJpsiKK_control_BD_to_JPSI_K_PI", "for_BsJpsiKK_control_BD_to_JPSI_K_PI");
    TTree *for_BsJpsiKK_control_BS_to_JPSI_K_K = new TTree("for_BsJpsiKK_control_BS_to_JPSI_K_K", "for_BsJpsiKK_control_BS_to_JPSI_K_K");
    
    TFile *f_ca_Lb =new TFile("Zc1plus_fit/input/ROOT_files/control_area_Lb.root", "recreate");
    
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__|| Phase space  ||__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//

    TFile *f_phs = new TFile("Zc1plus_fit/input/ROOT_files/phase_space.root", "recreate");

    float mu1_px, mu1_py, mu1_pz, mu2_px, mu2_py, mu2_pz, K_px, K_py, K_pz, p_px, p_py, p_pz, pi_px, pi_py, pi_pz, K1_px, K1_py, K1_pz, K2_px, K2_py, K2_pz,
            jpsipi1, jpsipi2, jpsiK1, jpsiK2, K1pi2, K2pi1, K1K2, w_dimuon, jpsiKpi, jpsipiK, jpsiKK, jpsipipi, jpsipK, jpsiKp, p1K2, p2K1, jpsip1, jpsip2, jpsippi, jpsipip;
    float mu1_BsKK_px, mu1_BsKK_py, mu1_BsKK_pz, mu2_BsKK_px, mu2_BsKK_py, mu2_BsKK_pz, K1_BsKK_px, K1_BsKK_py, K1_BsKK_pz, K2_BsKK_px, K2_BsKK_py, K2_BsKK_pz,
            jpsipi1_BsKK, jpsipi2_BsKK, jpsiK1_BsKK, jpsiK2_BsKK, K1pi2_BsKK, K2pi1_BsKK, K1K2_BsKK, cos_theta_Zc, cos_theta_Zc_piK, cos_theta_Zc_Kpi,
            phi_K_piK, phi_K_Kpi,
            alpha_mu_piK, alpha_mu_Kpi,
            phi_mu_Kstar_piK, phi_mu_Kstar_Kpi,
            phi_mu_X_piK, phi_mu_X_Kpi,
            cos_theta_psi_X_piK, cos_theta_psi_X_Kpi,
            cos_theta_Kstar_piK, cos_theta_Kstar_Kpi,
            cos_theta_psi_Kstar_piK, cos_theta_psi_Kstar_Kpi,
            cos_theta_B0_piK, cos_theta_B0_Kpi,
            phi_psi_piK, phi_psi_Kpi;
            
    Int_t signal, control, controlLb;
            
    float n_BD_to_JPSI_K_PI_counter = 0.,
            n_BD_to_JPSI_PI_PI_counter = 0.,
            n_BD_to_JPSI_K_K_counter = 0.,
            n_BS_to_JPSI_PI_PI_counter = 0.,
            n_BS_to_JPSI_K_K_counter = 0.,
            n_BS_to_JPSI_K_PI_counter = 0.,
            n_LAMBDA0B_to_JPSI_P_K_counter = 0.,
            n_LAMBDA0B_to_JPSI_P_PI_counter = 0.,
            n_data_counter = 0.;

    TTree *phs_BD_to_JPSI_K_PI = new TTree("phs_BD_to_JPSI_K_PI", "phs_BD_to_JPSI_K_PI");
    TTree *phs_BS_to_JPSI_K_K = new TTree("phs_BS_to_JPSI_K_K", "phs_BS_to_JPSI_K_K");
    TTree *phs_LAMBDA0B_to_JPSI_P_K = new TTree("phs_LAMBDA0B_to_JPSI_P_K", "phs_LAMBDA0B_to_JPSI_P_K");
    
    TBranch *phs_mu1_px = phs_BD_to_JPSI_K_PI->Branch("mu1_px", &mu1_px);
    TBranch *phs_mu1_py = phs_BD_to_JPSI_K_PI->Branch("mu1_py", &mu1_py);
    TBranch *phs_mu1_pz = phs_BD_to_JPSI_K_PI->Branch("mu1_pz", &mu1_pz);
    TBranch *phs_mu2_px = phs_BD_to_JPSI_K_PI->Branch("mu2_px", &mu2_px);
    TBranch *phs_mu2_py = phs_BD_to_JPSI_K_PI->Branch("mu2_py", &mu2_py);
    TBranch *phs_mu2_pz = phs_BD_to_JPSI_K_PI->Branch("mu2_pz", &mu2_pz);
    TBranch *phs_K_px = phs_BD_to_JPSI_K_PI->Branch("K_px", &K_px);
    TBranch *phs_K_py = phs_BD_to_JPSI_K_PI->Branch("K_py", &K_py);
    TBranch *phs_K_pz = phs_BD_to_JPSI_K_PI->Branch("K_pz", &K_pz);
    TBranch *phs_pi_px = phs_BD_to_JPSI_K_PI->Branch("pi_px", &pi_px);
    TBranch *phs_pi_py = phs_BD_to_JPSI_K_PI->Branch("pi_py", &pi_py);
    TBranch *phs_pi_pz = phs_BD_to_JPSI_K_PI->Branch("pi_pz", &pi_pz);
    TBranch *phs_jpsipi1 = phs_BD_to_JPSI_K_PI->Branch("jpsipi1", &jpsipi1);
    TBranch *phs_jpsipi2 = phs_BD_to_JPSI_K_PI->Branch("jpsipi2", &jpsipi2);
    TBranch *phs_jpsiK1 = phs_BD_to_JPSI_K_PI->Branch("jpsiK1", &jpsiK1);
    TBranch *phs_jpsiK2 = phs_BD_to_JPSI_K_PI->Branch("jpsiK2", &jpsiK2);
    TBranch *phs_K1pi2 = phs_BD_to_JPSI_K_PI->Branch("K1pi2", &K1pi2);
    TBranch *phs_K2pi1 = phs_BD_to_JPSI_K_PI->Branch("K2pi1", &K2pi1);
    TBranch *phs_K1K2 = phs_BD_to_JPSI_K_PI->Branch("K1K2", &K1K2);
    TBranch *phs_p1K2 = phs_BD_to_JPSI_K_PI->Branch("p1K2", &p1K2);
    TBranch *phs_p2K1 = phs_BD_to_JPSI_K_PI->Branch("p2K1", &p2K1);
    TBranch *phs_jpsip1 = phs_BD_to_JPSI_K_PI->Branch("jpsip1", &jpsip1);
    TBranch *phs_jpsip2 = phs_BD_to_JPSI_K_PI->Branch("jpsip2", &jpsip2);
    TBranch *phs_jpsiKpi = phs_BD_to_JPSI_K_PI->Branch("jpsiKpi", &jpsiKpi);
    TBranch *phs_jpsipiK = phs_BD_to_JPSI_K_PI->Branch("jpsipiK", &jpsipiK);
    TBranch *phs_jpsiKK = phs_BD_to_JPSI_K_PI->Branch("jpsiKK", &jpsiKK);
    TBranch *phs_jpsipipi = phs_BD_to_JPSI_K_PI->Branch("jpsipipi", &jpsipipi);
    TBranch *phs_jpsipK = phs_BD_to_JPSI_K_PI->Branch("jpsipK", &jpsipK);
    TBranch *phs_jpsiKp = phs_BD_to_JPSI_K_PI->Branch("jpsiKp", &jpsiKp);
    TBranch *phs_signal = phs_BD_to_JPSI_K_PI->Branch("signal", &signal);
    TBranch *phs_control = phs_BD_to_JPSI_K_PI->Branch("control", &control);
    TBranch *phs_controlLb = phs_BD_to_JPSI_K_PI->Branch("controlLb", &controlLb);
    TBranch *phs_w_dimuon = phs_BD_to_JPSI_K_PI->Branch("w_dimuon", &w_dimuon);
    TBranch *phs_cos_theta_Zc_Kpi = phs_BD_to_JPSI_K_PI->Branch("cos_theta_Zc_Kpi", &cos_theta_Zc_Kpi);
    TBranch *phs_cos_theta_Zc_piK = phs_BD_to_JPSI_K_PI->Branch("cos_theta_Zc_piK", &cos_theta_Zc_piK);
    TBranch *phs_phi_K_Kpi = phs_BD_to_JPSI_K_PI->Branch("phi_K_Kpi", &phi_K_Kpi);
    TBranch *phs_phi_K_piK = phs_BD_to_JPSI_K_PI->Branch("phi_K_piK", &phi_K_piK);
    TBranch *phs_alpha_mu_Kpi = phs_BD_to_JPSI_K_PI->Branch("alpha_mu_Kpi", &alpha_mu_Kpi);
    TBranch *phs_alpha_mu_piK = phs_BD_to_JPSI_K_PI->Branch("alpha_mu_piK", &alpha_mu_piK);
    TBranch *phs_phi_mu_Kstar_Kpi = phs_BD_to_JPSI_K_PI->Branch("phi_mu_Kstar_Kpi", &phi_mu_Kstar_Kpi);
    TBranch *phs_phi_mu_Kstar_piK = phs_BD_to_JPSI_K_PI->Branch("phi_mu_Kstar_piK", &phi_mu_Kstar_piK);
    TBranch *phs_phi_mu_X_Kpi = phs_BD_to_JPSI_K_PI->Branch("phi_mu_X_Kpi", &phi_mu_X_Kpi);
    TBranch *phs_phi_mu_X_piK = phs_BD_to_JPSI_K_PI->Branch("phi_mu_X_piK", &phi_mu_X_piK);
    TBranch *phs_cos_theta_psi_X_Kpi = phs_BD_to_JPSI_K_PI->Branch("cos_theta_psi_X_Kpi", &cos_theta_psi_X_Kpi);
    TBranch *phs_cos_theta_psi_X_piK = phs_BD_to_JPSI_K_PI->Branch("cos_theta_psi_X_piK", &cos_theta_psi_X_piK);
    TBranch *phs_cos_theta_Kstar_Kpi = phs_BD_to_JPSI_K_PI->Branch("cos_theta_Kstar_Kpi", &cos_theta_Kstar_Kpi);
    TBranch *phs_cos_theta_Kstar_piK = phs_BD_to_JPSI_K_PI->Branch("cos_theta_Kstar_piK", &cos_theta_Kstar_piK);
    TBranch *phs_cos_theta_psi_Kstar_Kpi = phs_BD_to_JPSI_K_PI->Branch("cos_theta_psi_Kstar_Kpi", &cos_theta_psi_Kstar_Kpi);
    TBranch *phs_cos_theta_psi_Kstar_piK = phs_BD_to_JPSI_K_PI->Branch("cos_theta_psi_Kstar_piK", &cos_theta_psi_Kstar_piK);
    TBranch *phs_cos_theta_B0_Kpi = phs_BD_to_JPSI_K_PI->Branch("cos_theta_B0_Kpi", &cos_theta_B0_Kpi);
    TBranch *phs_cos_theta_B0_piK = phs_BD_to_JPSI_K_PI->Branch("cos_theta_B0_piK", &cos_theta_B0_piK);
    TBranch *phs_phi_psi_Kpi = phs_BD_to_JPSI_K_PI->Branch("phi_psi_Kpi", &phi_psi_Kpi);
    TBranch *phs_phi_psi_piK = phs_BD_to_JPSI_K_PI->Branch("phi_psi_piK", &phi_psi_piK);
    TBranch *phs_jpsippi = phs_BD_to_JPSI_K_PI->Branch("jpsippi", &jpsippi);
    TBranch *phs_jpsipip = phs_BD_to_JPSI_K_PI->Branch("jpsipip", &jpsipip);
    
    TBranch *phs1_mu1_px = phs_BS_to_JPSI_K_K->Branch("mu1_px", &mu1_px);
    TBranch *phs1_mu1_py = phs_BS_to_JPSI_K_K->Branch("mu1_py", &mu1_py);
    TBranch *phs1_mu1_pz = phs_BS_to_JPSI_K_K->Branch("mu1_pz", &mu1_pz);
    TBranch *phs1_mu2_px = phs_BS_to_JPSI_K_K->Branch("mu2_px", &mu2_px);
    TBranch *phs1_mu2_py = phs_BS_to_JPSI_K_K->Branch("mu2_py", &mu2_py);
    TBranch *phs1_mu2_pz = phs_BS_to_JPSI_K_K->Branch("mu2_pz", &mu2_pz);
    TBranch *phs1_K1_px = phs_BS_to_JPSI_K_K->Branch("K1_px", &K1_px);
    TBranch *phs1_K1_py = phs_BS_to_JPSI_K_K->Branch("K1_py", &K1_py);
    TBranch *phs1_K1_pz = phs_BS_to_JPSI_K_K->Branch("K1_pz", &K1_pz);
    TBranch *phs1_K2_px = phs_BS_to_JPSI_K_K->Branch("K2_px", &K2_px);
    TBranch *phs1_K2_py = phs_BS_to_JPSI_K_K->Branch("K2_py", &K2_py);
    TBranch *phs1_K2_pz = phs_BS_to_JPSI_K_K->Branch("K2_pz", &K2_pz);
    TBranch *phs1_jpsipi1 = phs_BS_to_JPSI_K_K->Branch("jpsipi1", &jpsipi1);
    TBranch *phs1_jpsipi2 = phs_BS_to_JPSI_K_K->Branch("jpsipi2", &jpsipi2);
    TBranch *phs1_jpsiK1 = phs_BS_to_JPSI_K_K->Branch("jpsiK1", &jpsiK1);
    TBranch *phs1_jpsiK2 = phs_BS_to_JPSI_K_K->Branch("jpsiK2", &jpsiK2);
    TBranch *phs1_K1pi2 = phs_BS_to_JPSI_K_K->Branch("K1pi2", &K1pi2);
    TBranch *phs1_K2pi1 = phs_BS_to_JPSI_K_K->Branch("K2pi1", &K2pi1);
    TBranch *phs1_K1K2 = phs_BS_to_JPSI_K_K->Branch("K1K2", &K1K2);
    TBranch *phs1_p1K2 = phs_BS_to_JPSI_K_K->Branch("p1K2", &p1K2);
    TBranch *phs1_p2K1 = phs_BS_to_JPSI_K_K->Branch("p2K1", &p2K1);
    TBranch *phs1_jpsip1 = phs_BS_to_JPSI_K_K->Branch("jpsip1", &jpsip1);
    TBranch *phs1_jpsip2 = phs_BS_to_JPSI_K_K->Branch("jpsip2", &jpsip2);
    TBranch *phs1_jpsiKpi = phs_BS_to_JPSI_K_K->Branch("jpsiKpi", &jpsiKpi);
    TBranch *phs1_jpsipiK = phs_BS_to_JPSI_K_K->Branch("jpsipiK", &jpsipiK);
    TBranch *phs1_jpsiKK = phs_BS_to_JPSI_K_K->Branch("jpsiKK", &jpsiKK);
    TBranch *phs1_jpsipipi = phs_BS_to_JPSI_K_K->Branch("jpsipipi", &jpsipipi);
    TBranch *phs1_jpsipK = phs_BS_to_JPSI_K_K->Branch("jpsipK", &jpsipK);
    TBranch *phs1_jpsiKp = phs_BS_to_JPSI_K_K->Branch("jpsiKp", &jpsiKp);
    TBranch *phs1_signal = phs_BS_to_JPSI_K_K->Branch("signal", &signal);
    TBranch *phs1_control = phs_BS_to_JPSI_K_K->Branch("control", &control);
    TBranch *phs1_controlLb = phs_BS_to_JPSI_K_K->Branch("controlLb", &controlLb);
    TBranch *phs1_w_dimuon = phs_BS_to_JPSI_K_K->Branch("w_dimuon", &w_dimuon);
    TBranch *phs1_cos_theta_Zc_Kpi = phs_BS_to_JPSI_K_K->Branch("cos_theta_Zc_Kpi", &cos_theta_Zc_Kpi);
    TBranch *phs1_cos_theta_Zc_piK = phs_BS_to_JPSI_K_K->Branch("cos_theta_Zc_piK", &cos_theta_Zc_piK);
    TBranch *phs1_phi_K_Kpi = phs_BS_to_JPSI_K_K->Branch("phi_K_Kpi", &phi_K_Kpi);
    TBranch *phs1_phi_K_piK = phs_BS_to_JPSI_K_K->Branch("phi_K_piK", &phi_K_piK);
    TBranch *phs1_alpha_mu_Kpi = phs_BS_to_JPSI_K_K->Branch("alpha_mu_Kpi", &alpha_mu_Kpi);
    TBranch *phs1_alpha_mu_piK = phs_BS_to_JPSI_K_K->Branch("alpha_mu_piK", &alpha_mu_piK);
    TBranch *phs1_phi_mu_Kstar_Kpi = phs_BS_to_JPSI_K_K->Branch("phi_mu_Kstar_Kpi", &phi_mu_Kstar_Kpi);
    TBranch *phs1_phi_mu_Kstar_piK = phs_BS_to_JPSI_K_K->Branch("phi_mu_Kstar_piK", &phi_mu_Kstar_piK);
    TBranch *phs1_phi_mu_X_Kpi = phs_BS_to_JPSI_K_K->Branch("phi_mu_X_Kpi", &phi_mu_X_Kpi);
    TBranch *phs1_phi_mu_X_piK = phs_BS_to_JPSI_K_K->Branch("phi_mu_X_piK", &phi_mu_X_piK);
    TBranch *phs1_cos_theta_psi_X_Kpi = phs_BS_to_JPSI_K_K->Branch("cos_theta_psi_X_Kpi", &cos_theta_psi_X_Kpi);
    TBranch *phs1_cos_theta_psi_X_piK = phs_BS_to_JPSI_K_K->Branch("cos_theta_psi_X_piK", &cos_theta_psi_X_piK);
    TBranch *phs1_cos_theta_Kstar_Kpi = phs_BS_to_JPSI_K_K->Branch("cos_theta_Kstar_Kpi", &cos_theta_Kstar_Kpi);
    TBranch *phs1_cos_theta_Kstar_piK = phs_BS_to_JPSI_K_K->Branch("cos_theta_Kstar_piK", &cos_theta_Kstar_piK);
    TBranch *phs1_cos_theta_psi_Kstar_Kpi = phs_BS_to_JPSI_K_K->Branch("cos_theta_psi_Kstar_Kpi", &cos_theta_psi_Kstar_Kpi);
    TBranch *phs1_cos_theta_psi_Kstar_piK = phs_BS_to_JPSI_K_K->Branch("cos_theta_psi_Kstar_piK", &cos_theta_psi_Kstar_piK);
    TBranch *phs1_cos_theta_B0_Kpi = phs_BS_to_JPSI_K_K->Branch("cos_theta_B0_Kpi", &cos_theta_B0_Kpi);
    TBranch *phs1_cos_theta_B0_piK = phs_BS_to_JPSI_K_K->Branch("cos_theta_B0_piK", &cos_theta_B0_piK);
    TBranch *phs1_phi_psi_Kpi = phs_BS_to_JPSI_K_K->Branch("phi_psi_Kpi", &phi_psi_Kpi);
    TBranch *phs1_phi_psi_piK = phs_BS_to_JPSI_K_K->Branch("phi_psi_piK", &phi_psi_piK);
    TBranch *phs1_jpsippi = phs_BS_to_JPSI_K_K->Branch("jpsippi", &jpsippi);
    TBranch *phs1_jpsipip = phs_BS_to_JPSI_K_K->Branch("jpsipip", &jpsipip);
    
    TBranch *phs2_mu1_px = phs_LAMBDA0B_to_JPSI_P_K->Branch("mu1_px", &mu1_px);
    TBranch *phs2_mu1_py = phs_LAMBDA0B_to_JPSI_P_K->Branch("mu1_py", &mu1_py);
    TBranch *phs2_mu1_pz = phs_LAMBDA0B_to_JPSI_P_K->Branch("mu1_pz", &mu1_pz);
    TBranch *phs2_mu2_px = phs_LAMBDA0B_to_JPSI_P_K->Branch("mu2_px", &mu2_px);
    TBranch *phs2_mu2_py = phs_LAMBDA0B_to_JPSI_P_K->Branch("mu2_py", &mu2_py);
    TBranch *phs2_mu2_pz = phs_LAMBDA0B_to_JPSI_P_K->Branch("mu2_pz", &mu2_pz);
    TBranch *phs2_p_px = phs_LAMBDA0B_to_JPSI_P_K->Branch("p_px", &p_px);
    TBranch *phs2_p_py = phs_LAMBDA0B_to_JPSI_P_K->Branch("p_py", &p_py);
    TBranch *phs2_p_pz = phs_LAMBDA0B_to_JPSI_P_K->Branch("p_pz", &p_pz);
    TBranch *phs2_K_px = phs_LAMBDA0B_to_JPSI_P_K->Branch("K_px", &K_px);
    TBranch *phs2_K_py = phs_LAMBDA0B_to_JPSI_P_K->Branch("K_py", &K_py);
    TBranch *phs2_K_pz = phs_LAMBDA0B_to_JPSI_P_K->Branch("K_pz", &K_pz);
    TBranch *phs2_jpsipi1 = phs_LAMBDA0B_to_JPSI_P_K->Branch("jpsipi1", &jpsipi1);
    TBranch *phs2_jpsipi2 = phs_LAMBDA0B_to_JPSI_P_K->Branch("jpsipi2", &jpsipi2);
    TBranch *phs2_jpsiK1 = phs_LAMBDA0B_to_JPSI_P_K->Branch("jpsiK1", &jpsiK1);
    TBranch *phs2_jpsiK2 = phs_LAMBDA0B_to_JPSI_P_K->Branch("jpsiK2", &jpsiK2);
    TBranch *phs2_K1pi2 = phs_LAMBDA0B_to_JPSI_P_K->Branch("K1pi2", &K1pi2);
    TBranch *phs2_K2pi1 = phs_LAMBDA0B_to_JPSI_P_K->Branch("K2pi1", &K2pi1);
    TBranch *phs2_K1K2 = phs_LAMBDA0B_to_JPSI_P_K->Branch("K1K2", &K1K2);
    TBranch *phs2_p1K2 = phs_LAMBDA0B_to_JPSI_P_K->Branch("p1K2", &p1K2);
    TBranch *phs2_p2K1 = phs_LAMBDA0B_to_JPSI_P_K->Branch("p2K1", &p2K1);
    TBranch *phs2_jpsip1 = phs_LAMBDA0B_to_JPSI_P_K->Branch("jpsip1", &jpsip1);
    TBranch *phs2_jpsip2 = phs_LAMBDA0B_to_JPSI_P_K->Branch("jpsip2", &jpsip2);
    TBranch *phs2_jpsiKpi = phs_LAMBDA0B_to_JPSI_P_K->Branch("jpsiKpi", &jpsiKpi);
    TBranch *phs2_jpsipiK = phs_LAMBDA0B_to_JPSI_P_K->Branch("jpsipiK", &jpsipiK);
    TBranch *phs2_jpsiKK = phs_LAMBDA0B_to_JPSI_P_K->Branch("jpsiKK", &jpsiKK);
    TBranch *phs2_jpsipipi = phs_LAMBDA0B_to_JPSI_P_K->Branch("jpsipipi", &jpsipipi);
    TBranch *phs2_jpsipK = phs_LAMBDA0B_to_JPSI_P_K->Branch("jpsipK", &jpsipK);
    TBranch *phs2_jpsiKp = phs_LAMBDA0B_to_JPSI_P_K->Branch("jpsiKp", &jpsiKp);
    TBranch *phs2_signal = phs_LAMBDA0B_to_JPSI_P_K->Branch("signal", &signal);
    TBranch *phs2_control = phs_LAMBDA0B_to_JPSI_P_K->Branch("control", &control);
    TBranch *phs2_controlLb = phs_LAMBDA0B_to_JPSI_P_K->Branch("controlLb", &controlLb);
    TBranch *phs2_w_dimuon = phs_LAMBDA0B_to_JPSI_P_K->Branch("w_dimuon", &w_dimuon);
    TBranch *phs2_cos_theta_Zc_Kpi = phs_LAMBDA0B_to_JPSI_P_K->Branch("cos_theta_Zc_Kpi", &cos_theta_Zc_Kpi);
    TBranch *phs2_cos_theta_Zc_piK = phs_LAMBDA0B_to_JPSI_P_K->Branch("cos_theta_Zc_piK", &cos_theta_Zc_piK);
    TBranch *phs2_phi_K_Kpi = phs_LAMBDA0B_to_JPSI_P_K->Branch("phi_K_Kpi", &phi_K_Kpi);
    TBranch *phs2_phi_K_piK = phs_LAMBDA0B_to_JPSI_P_K->Branch("phi_K_piK", &phi_K_piK);
    TBranch *phs2_alpha_mu_Kpi = phs_LAMBDA0B_to_JPSI_P_K->Branch("alpha_mu_Kpi", &alpha_mu_Kpi);
    TBranch *phs2_alpha_mu_piK = phs_LAMBDA0B_to_JPSI_P_K->Branch("alpha_mu_piK", &alpha_mu_piK);
    TBranch *phs2_phi_mu_Kstar_Kpi = phs_LAMBDA0B_to_JPSI_P_K->Branch("phi_mu_Kstar_Kpi", &phi_mu_Kstar_Kpi);
    TBranch *phs2_phi_mu_Kstar_piK = phs_LAMBDA0B_to_JPSI_P_K->Branch("phi_mu_Kstar_piK", &phi_mu_Kstar_piK);
    TBranch *phs2_phi_mu_X_Kpi = phs_LAMBDA0B_to_JPSI_P_K->Branch("phi_mu_X_Kpi", &phi_mu_X_Kpi);
    TBranch *phs2_phi_mu_X_piK = phs_LAMBDA0B_to_JPSI_P_K->Branch("phi_mu_X_piK", &phi_mu_X_piK);
    TBranch *phs2_cos_theta_psi_X_Kpi = phs_LAMBDA0B_to_JPSI_P_K->Branch("cos_theta_psi_X_Kpi", &cos_theta_psi_X_Kpi);
    TBranch *phs2_cos_theta_psi_X_piK = phs_LAMBDA0B_to_JPSI_P_K->Branch("cos_theta_psi_X_piK", &cos_theta_psi_X_piK);
    TBranch *phs2_cos_theta_Kstar_Kpi = phs_LAMBDA0B_to_JPSI_P_K->Branch("cos_theta_Kstar_Kpi", &cos_theta_Kstar_Kpi);
    TBranch *phs2_cos_theta_Kstar_piK = phs_LAMBDA0B_to_JPSI_P_K->Branch("cos_theta_Kstar_piK", &cos_theta_Kstar_piK);
    TBranch *phs2_cos_theta_psi_Kstar_Kpi = phs_LAMBDA0B_to_JPSI_P_K->Branch("cos_theta_psi_Kstar_Kpi", &cos_theta_psi_Kstar_Kpi);
    TBranch *phs2_cos_theta_psi_Kstar_piK = phs_LAMBDA0B_to_JPSI_P_K->Branch("cos_theta_psi_Kstar_piK", &cos_theta_psi_Kstar_piK);
    TBranch *phs2_cos_theta_B0_Kpi = phs_LAMBDA0B_to_JPSI_P_K->Branch("cos_theta_B0_Kpi", &cos_theta_B0_Kpi);
    TBranch *phs2_cos_theta_B0_piK = phs_LAMBDA0B_to_JPSI_P_K->Branch("cos_theta_B0_piK", &cos_theta_B0_piK);
    TBranch *phs2_phi_psi_Kpi = phs_LAMBDA0B_to_JPSI_P_K->Branch("phi_psi_Kpi", &phi_psi_Kpi);
    TBranch *phs2_phi_psi_piK = phs_LAMBDA0B_to_JPSI_P_K->Branch("phi_psi_piK", &phi_psi_piK);
    TBranch *phs2_jpsippi = phs_LAMBDA0B_to_JPSI_P_K->Branch("jpsippi", &jpsippi);
    TBranch *phs2_jpsipip = phs_LAMBDA0B_to_JPSI_P_K->Branch("jpsipip", &jpsipip);
    
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__|| File creating||__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
    
    TFile *f_sas = new TFile("Zc1plus_fit/input/ROOT_files/signal_area_shape.root", "open");
    TH2 *area_hist = (TH2D*)f_sas->Get("area_hist");
    
    TFile *f_sas_Lb = new TFile("Zc1plus_fit/input/ROOT_files/signal_area_shape_Lb.root", "open");
    TH2 *area_hist_Lb = (TH2D*)f_sas_Lb->Get("area_hist");
    
    TChain stree("stree");
    stree.AddFile("datasets/data15.root");
    stree.AddFile("datasets/data16Main.root");
    stree.AddFile("datasets/data16delayed.root");
    stree.AddFile("datasets/data17.root");
    stree.AddFile("datasets/data18.root");
    
    Long64_t nentries = stree.GetEntries();
    cout << "Number of runs data 15 + 16 + 17 + 18\t" << nentries << endl;
    
    float sB_mu1_px, sB_mu1_py, sB_mu1_pz, sB_mu2_px, sB_mu2_py, sB_mu2_pz, sB_trk1_px, sB_trk1_py, sB_trk1_pz, sB_trk2_px, sB_trk2_py, sB_trk2_pz, sB_trk1_charge, sB_trk2_charge, sB_Lxy_MaxSumPt, sB_Bs_pt, sB_mu1_pt, sB_mu2_pt, sB_trk1_pt, sB_trk2_pt, sB_Bs_chi2_ndof, sB_MaxSumPt, sB_mu1_eta, sB_mu2_eta, sB_trk1_eta, sB_trk2_eta, sB_Jpsi_mass, sB_Jpsi_chi2, sB_mu1_charge, sB_mu2_charge, w_trigger, w_SF_mu1, w_SF_mu2, Bs_B_pt;
    int sB_year;
    
    float w;
    float w_4tr;
    
    stree.SetBranchAddress("sB_mu1_px", &sB_mu1_px);
    stree.SetBranchAddress("sB_mu1_py", &sB_mu1_py);
    stree.SetBranchAddress("sB_mu1_pz", &sB_mu1_pz);
    stree.SetBranchAddress("sB_mu2_px", &sB_mu2_px);
    stree.SetBranchAddress("sB_mu2_py", &sB_mu2_py);
    stree.SetBranchAddress("sB_mu2_pz", &sB_mu2_pz);
    stree.SetBranchAddress("sB_trk1_px", &sB_trk1_px);
    stree.SetBranchAddress("sB_trk1_py", &sB_trk1_py);
    stree.SetBranchAddress("sB_trk1_pz", &sB_trk1_pz);
    stree.SetBranchAddress("sB_trk2_px", &sB_trk2_px);
    stree.SetBranchAddress("sB_trk2_py", &sB_trk2_py);
    stree.SetBranchAddress("sB_trk2_pz", &sB_trk2_pz);
    stree.SetBranchAddress("sB_trk1_charge", &sB_trk1_charge);
    stree.SetBranchAddress("sB_trk2_charge", &sB_trk2_charge);
    stree.SetBranchAddress("sB_Lxy_MaxSumPt", &sB_Lxy_MaxSumPt);
    stree.SetBranchAddress("sB_Bs_pt", &sB_Bs_pt);
    stree.SetBranchAddress("sB_mu1_pt", &sB_mu1_pt);
    stree.SetBranchAddress("sB_mu2_pt", &sB_mu2_pt);
    stree.SetBranchAddress("sB_trk1_pt", &sB_trk1_pt);
    stree.SetBranchAddress("sB_trk2_pt", &sB_trk2_pt);
    stree.SetBranchAddress("sB_Bs_chi2_ndof", &sB_Bs_chi2_ndof);
    stree.SetBranchAddress("sB_MaxSumPt", &sB_MaxSumPt);
    stree.SetBranchAddress("sB_mu1_eta", &sB_mu1_eta);
    stree.SetBranchAddress("sB_mu2_eta", &sB_mu2_eta);
    stree.SetBranchAddress("sB_trk1_eta", &sB_trk1_eta);
    stree.SetBranchAddress("sB_trk2_eta", &sB_trk2_eta);
    stree.SetBranchAddress("sB_Jpsi_mass", &sB_Jpsi_mass);
    stree.SetBranchAddress("sB_Jpsi_chi2", &sB_Jpsi_chi2);
    stree.SetBranchAddress("sB_mu1_charge", &sB_mu1_charge);
    stree.SetBranchAddress("sB_mu2_charge", &sB_mu2_charge);
    stree.SetBranchAddress("sB_year", &sB_year);
    
    float w_sc;
    
    for(int i = 1; i <= nentries; i++)
    {
        stree.GetEntry(i);
        if (i % 100000 == 0) cout << i << endl;
        TLorentzVector mu1, mu2, pi1, pi2, K1, K2, p1, p2, jpsi;
        w = 1.;
        w_sc  = 0.;
        mu1.SetXYZM(sB_mu1_px, sB_mu1_py, sB_mu1_pz, 105.65837);
        mu2.SetXYZM(sB_mu2_px, sB_mu2_py, sB_mu2_pz, 105.65837);
        pi1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 139.57);
        pi2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 139.57);
        K1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 493.677);
        K2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 493.677);
        p1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 938.272);
        p2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 938.272);
        jpsi.SetXYZM(sB_mu1_px + sB_mu2_px, sB_mu1_py + sB_mu2_py, sB_mu1_pz + sB_mu2_pz, 3096.);
        TLorentzVector bs = jpsi + pi1 + pi2;
        if ((K1 + pi2).M() > 1550. && (pi1 + K2).M() > 1550. && sB_Lxy_MaxSumPt > 1. && (jpsi + pi1).M() < 4900.  && (jpsi + pi2).M() < 4900. && ((sB_mu1_pt > 4000. && sB_mu2_pt > 4000. && sB_year == 15) || (sB_mu1_pt > 4000. && sB_mu2_pt > 4000. && sB_year != 15)) && sB_trk1_pt > 2000. && sB_trk2_pt > 2000.  && sB_Bs_chi2_ndof < 1.7 && sB_Bs_pt > 0.25 * sB_MaxSumPt && 4900. < (jpsi + K1 + pi2).M() && (jpsi + K1 + pi2).M() < 5700. && (jpsi + pi1 + K2).M() < 5900. && 2950. < sB_Jpsi_mass && sB_Jpsi_mass < 3250.) 
        {
            //if ((sB_trk1_charge * sB_trk2_charge > 0) || (sB_mu1_charge * sB_mu2_charge > 0)) {w = -1;} // mu same charge weight
            if (sB_trk1_charge * sB_trk2_charge > 0) {w = -1;} // mu same charge not weight + ignored
            if (sB_mu1_charge * sB_mu2_charge > 0) {w = 0; w_sc = 1.;} // mu same charge ignored
            //i_temp += w;
            
            h_mu1_pt_data->Fill(sB_mu1_pt, 1. - w_sc);
            h_mu2_pt_data->Fill(sB_mu1_pt, 1. - w_sc);
            h_mu1_pt_data->Fill(sB_mu1_pt, w);
            h_mu2_pt_data->Fill(sB_mu1_pt, w);
            
            h_m_jpsi_data->Fill(sB_Jpsi_mass, 1. - w_sc);
            h_m_jpsi_data_w->Fill(sB_Jpsi_mass, w);
            h_m_jpsi_data_sc->Fill(sB_Jpsi_mass, w_sc);
            
            if ((jpsi + pi1 + K2).M() > 4650.) {Kpi_piK_2d_data->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, w);
                Kpi_piK_2d_data_for_ea->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, w);}
            else {Kpi_piK_2d_data->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, 0.);
                Kpi_piK_2d_data_for_ea->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, 0.);} 
             
            if ((jpsi + p1 + K2).M() * GEV > 5.2 && (jpsi + p1 + K2).M() * GEV < 7.0 && (jpsi + K1 + p2).M() * GEV > 5.15 && (jpsi + K1 + p2).M() * GEV < 7.0) 
            {
                pK_Kp_2d_data->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, w);
                //pK_Kp_2d_test_data->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, w);
                pK_Kp_2d_rotate_data->Fill((1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, (-1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, w);
            }
            else 
            {
                pK_Kp_2d_data->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, 0.);
                //pK_Kp_2d_test_data->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, 0.);
                pK_Kp_2d_rotate_data->Fill((1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, (-1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, 0.);
                
            } 
            
            if ((jpsi + p1 + pi2).M() * GEV > ppi_pip_low && (jpsi + p1 + pi2).M() * GEV < ppi_pip_high && (jpsi + pi1 + p2).M() * GEV > ppi_pip_low2 && (jpsi + pi1 + p2).M() * GEV < ppi_pip_high2) 
            {
                ppi_pip_2d_data->Fill((jpsi + p1 + pi2).M() * GEV, (jpsi + p2 + pi1).M() * GEV, w);
            }
            else 
            {
                ppi_pip_2d_data->Fill((jpsi + p1 + pi2).M() * GEV, (jpsi + p2 + pi1).M() * GEV, 0.);                
            }
            
            if ((jpsi + p1 + K2).M() * GEV > 5.2 && (jpsi + p1 + K2).M() * GEV < 6.8 && (jpsi + K1 + p2).M() * GEV > 5.15 && (jpsi + K1 + p2).M() * GEV < 6.8)
            {
                pK_Kp_2d_test_data->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, w);
            }
            else 
            {
                pK_Kp_2d_test_data->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, 0.);                
            }
            
            KK_1d_data->Fill((jpsi + K1 + K2).M() * GEV, w);
            pipi_1d_data->Fill((jpsi + pi1 + pi2).M() * GEV, w);
            pipi_1d_data_sb->Fill((jpsi + pi1 + pi2).M() * GEV, w);
            
            int xsa = area_hist->ProjectionX()->FindBin((jpsi + K1 + pi2).M() * GEV); 
            int ysa = area_hist->ProjectionY()->FindBin((jpsi + K2 + pi1).M() * GEV);
            if (area_hist->GetBinContent(xsa, ysa) == 1)
            {
                Jpsipi_2d_data->Fill((jpsi + pi1).M() * GEV, (jpsi + pi2).M() * GEV, w);
                JpsiK_2d_data->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV, w);
                Kpi_2d_data->Fill((K1 + pi2).M() * GEV, (K2 + pi1).M() * GEV, w); 
                
                cos_theta_Zc_data->Fill(abs(cos(theta_X(jpsi + K1 + pi2, jpsi + pi2, jpsi))), abs(cos(theta_X(jpsi + K2 + pi1, jpsi + pi1, jpsi))), w);
                
                phi_K_data->Fill(phi_K(jpsi + K1 + pi2, K1 + pi2, K1, jpsi), phi_K(jpsi + K2 + pi1, K2 + pi1, K2, jpsi), w);
                alpha_mu_data->Fill(alpha_mu(mu2, jpsi, jpsi + pi2, jpsi + K1 + pi2), alpha_mu(mu2, jpsi, jpsi + pi1, jpsi + K2 + pi1), w);
                phi_mu_Kstar_data->Fill(phi_mu_Kstar(jpsi + K1 + pi2, K1 + pi2, jpsi, mu2), phi_mu_Kstar(jpsi + K2 + pi1, K2 + pi1, jpsi, mu2), w);
                phi_mu_X_data->Fill(phi_mu_X(jpsi + pi2, jpsi + pi2 + K1, jpsi, mu2), phi_mu_X(jpsi + pi1, jpsi + pi1 + K2, jpsi, mu2), w);
                cos_theta_psi_X_data->Fill(abs(cos(theta_psi_X(jpsi + pi2, jpsi, mu2))), abs(cos(theta_psi_X(jpsi + pi1, jpsi, mu2))), w);
                cos_theta_Kstar_data->Fill(abs(cos(theta_Kstar(jpsi + K1 + pi2, K1 + pi2, K1))), abs(cos(theta_Kstar(jpsi + K2 + pi1, K2 + pi1, K2))), w);
                cos_theta_psi_Kstar_data->Fill(abs(cos(theta_psi_Kstar(jpsi + K1 + pi2, jpsi, mu2))), abs(cos(theta_psi_Kstar(jpsi + K2 + pi1, jpsi, mu2))), w);
                cos_theta_B0_data->Fill(abs(cos(theta_B0(jpsi + K1 + pi2, K1 + pi2))), abs(cos(theta_B0(jpsi + K2 + pi1, K2 + pi1))), w);
                phi_psi_data->Fill(phi_psi(jpsi + K1 + pi2, jpsi + pi2, jpsi), phi_psi(jpsi + K2 + pi1, jpsi + pi1, jpsi), w);
                
                B_pt_data->Fill(sB_Bs_pt * GEV, w);
                
                if (sB_trk1_charge * sB_trk2_charge > 0)
                {
                    Jpsipi_2d_bg_comb->Fill((jpsi + pi1).M() * GEV, (jpsi + pi2).M() * GEV);
                    JpsiK_2d_bg_comb->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV);
                    Kpi_2d_bg_comb->Fill((K1 + pi2).M() * GEV, (K2 + pi1).M() * GEV);
                    trk_pt_bg_comb->Fill(sB_trk1_pt * GEV, sB_trk1_pt * GEV);
                }               
            }
            
            xsa = area_hist_Lb->ProjectionX()->FindBin((jpsi + p1 + K2).M() * GEV); 
            ysa = area_hist_Lb->ProjectionY()->FindBin((jpsi + p2 + K1).M() * GEV);
            
            if (area_hist_Lb->GetBinContent(xsa, ysa) == 1)
            {
                Jpsip_2d_controlLb_data->Fill((jpsi + p1).M() * GEV, (jpsi + p2).M() * GEV, w);
                JpsiK_2d_controlLb_data->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV, w);
                pK_2d_controlLb_data->Fill((p1 + K2).M() * GEV, (p2 + K1).M() * GEV, w);                
            }
            
            if (5336. < (jpsi + K1 + K2).M() && (jpsi + K1 + K2).M() < 5396.)
            {
                KK_1d_control_data->Fill((K1 + K2).M() * GEV, w);
                JpsiK1_1d_control_data->Fill((jpsi + K1).M() * GEV, w);
                JpsiK2_1d_control_data->Fill((jpsi + K2).M() * GEV, w);
                JpsiK_2d_control_data->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV, w);
                n_data_counter += w;
            }
        }
    } 
    
    TFile *f_in_func = new TFile("B_pt_studies/parameters_signal_Zc1plus_v16_it1_n0.root");
    TVectorD *vpar_B_pt = (TVectorD*)f_in_func->Get("par_B_pt_func");
    Double_t w_B_pt = 1., w1 = 1.; 
    
    TChain mc_BD_to_JPSI_K_PI("BsAllCandidates");
    mc_BD_to_JPSI_K_PI.AddFile("datasets/mc_BD_to_JPSI_K_PI_Bhad.root"); 
    mc_BD_to_JPSI_K_PI.AddFile("datasets/mc_BD_to_JPSI_K_PI_antiBhad.root");
    nentries = mc_BD_to_JPSI_K_PI.GetEntries();
    cout << "Number of runs BD_to_JPSI_K_PI\t" << nentries << endl;

    float muon1_px, muon1_py, muon1_pz, muon2_px, muon2_py, muon2_pz, kaon_px, kaon_py, kaon_pz, pion_px, pion_py, pion_pz;
    
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_mu1_px", &sB_mu1_px);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_mu1_py", &sB_mu1_py);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_mu1_pz", &sB_mu1_pz);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_mu2_px", &sB_mu2_px);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_mu2_py", &sB_mu2_py);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_mu2_pz", &sB_mu2_pz);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_trk1_px", &sB_trk1_px);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_trk1_py", &sB_trk1_py);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_trk1_pz", &sB_trk1_pz);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_trk2_px", &sB_trk2_px);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_trk2_py", &sB_trk2_py);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_trk2_pz", &sB_trk2_pz);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_trk1_charge", &sB_trk1_charge);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_trk2_charge", &sB_trk2_charge);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_Lxy_MaxSumPt", &sB_Lxy_MaxSumPt);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_Bs_pt", &sB_Bs_pt);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_mu1_pt", &sB_mu1_pt);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_mu2_pt", &sB_mu2_pt);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_trk1_pt", &sB_trk1_pt);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_trk2_pt", &sB_trk2_pt);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_Bs_chi2_ndof", &sB_Bs_chi2_ndof);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_MaxSumPt", &sB_MaxSumPt);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_mu1_eta", &sB_mu1_eta);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_mu2_eta", &sB_mu2_eta);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_trk1_eta", &sB_trk1_eta);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_trk2_eta", &sB_trk2_eta);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_Jpsi_mass", &sB_Jpsi_mass);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_Jpsi_chi2", &sB_Jpsi_chi2);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_muon1_px", &muon1_px);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_muon1_py", &muon1_py);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_muon1_pz", &muon1_pz);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_muon2_px", &muon2_px);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_muon2_py", &muon2_py);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_muon2_pz", &muon2_pz);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_kaon_px", &kaon_px);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_kaon_py", &kaon_py);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_kaon_pz", &kaon_pz);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_pion_px", &pion_px);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_pion_py", &pion_py);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_pion_pz", &pion_pz);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_w_trigger", &w_trigger);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_w_SF_mu1", &w_SF_mu1);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_w_SF_mu2", &w_SF_mu2);
    mc_BD_to_JPSI_K_PI.SetBranchAddress("sB_B_pt", &Bs_B_pt);
    for(int i = 1; i <= nentries; i++)
    {
        mc_BD_to_JPSI_K_PI.GetEntry(i);
        if (i % 10000 == 0) {cout << i << endl;}
        TLorentzVector mu1, mu2, pi1, pi2, K1, K2, p1, p2, jpsi, muon1, muon2, kaon, pion;
        w = 1.; w1 = 1.;
        mu1.SetXYZM(sB_mu1_px, sB_mu1_py, sB_mu1_pz, 105.65837);
        mu2.SetXYZM(sB_mu2_px, sB_mu2_py, sB_mu2_pz, 105.65837);
        pi1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 139.57);
        pi2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 139.57);
        K1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 493.677);
        K2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 493.677);
        p1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 938.272);
        p2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 938.272);
        jpsi.SetXYZM(sB_mu1_px + sB_mu2_px, sB_mu1_py + sB_mu2_py, sB_mu1_pz + sB_mu2_pz, 3096.);
        muon1.SetXYZM(muon1_px, muon1_py, muon1_pz, 105.65837);
        muon2.SetXYZM(muon2_px, muon2_py, muon2_pz, 105.65837);
        kaon.SetXYZM(kaon_px, kaon_py, kaon_pz, 493.677);
        pion.SetXYZM(pion_px, pion_py, pion_pz, 139.57); 
        TLorentzVector bs = jpsi + pi1 + pi2;
        if ((K1 + pi2).M() > 1550. && (pi1 + K2).M() > 1550. && sB_Lxy_MaxSumPt > 1. && (jpsi + pi1).M() < 4900.  && (jpsi + pi2).M() < 4900. && sB_mu1_pt > 4000. && sB_mu2_pt > 4000. && sB_trk1_pt > 2000. && sB_trk2_pt > 2000.  && sB_Bs_chi2_ndof < 1.7 && sB_Bs_pt > 0.25 * sB_MaxSumPt && 4900. < (jpsi + K1 + pi2).M() && (jpsi + K1 + pi2).M() < 5700. && (jpsi + pi1 + K2).M() < 5900. && 2950. < sB_Jpsi_mass && sB_Jpsi_mass < 3250.) 
        {
            if (sB_trk1_charge * sB_trk2_charge > 0) {w = -1;}
            w_dimuon = w_trigger * w_SF_mu1 * w_SF_mu2;
            if (Bs_B_pt * GEV <= 50.)
            {
                w_B_pt = (*vpar_B_pt)(0) + (*vpar_B_pt)(1) * (Bs_B_pt * GEV);
            }
            else
            {
                w_B_pt = (*vpar_B_pt)(0) + (*vpar_B_pt)(1) * 50.;
            }
            w1 = w * w_dimuon;
            w_dimuon = w_dimuon * w_B_pt;
            w = w * w_dimuon;
            w_4tr = w;
            
            //cout << w << endl;
            if ((jpsi + pi1 + K2).M() > 4650.) {Kpi_piK_2d_BD_to_JPSI_K_PI->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, w_4tr);
                Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, w_4tr);}
            else {Kpi_piK_2d_BD_to_JPSI_K_PI->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, 0.);
                Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, 0.);} 
             
            if ((jpsi + p1 + K2).M() * GEV > 5.2 && (jpsi + p1 + K2).M() * GEV < 7.0 && (jpsi + K1 + p2).M() * GEV > 5.15 && (jpsi + K1 + p2).M() * GEV < 7.0) 
            {
                pK_Kp_2d_BD_to_JPSI_K_PI->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, w_4tr);
                //pK_Kp_2d_test_BD_to_JPSI_K_PI->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, w_4tr);
                pK_Kp_2d_rotate_BD_to_JPSI_K_PI->Fill((1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, (-1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, w_4tr);
            }
            else 
            {
                pK_Kp_2d_BD_to_JPSI_K_PI->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, 0.);
                //pK_Kp_2d_test_BD_to_JPSI_K_PI->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, 0.);
                pK_Kp_2d_rotate_BD_to_JPSI_K_PI->Fill((1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, (-1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, 0.);
                
            } 
            
            if ((jpsi + p1 + K2).M() * GEV > 5.2 && (jpsi + p1 + K2).M() * GEV < 6.8 && (jpsi + K1 + p2).M() * GEV > 5.15 && (jpsi + K1 + p2).M() * GEV < 6.8)
            {
                pK_Kp_2d_test_BD_to_JPSI_K_PI->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, w);
            }
            else 
            {
                pK_Kp_2d_test_BD_to_JPSI_K_PI->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, 0.);                
            }
            
            if ((jpsi + p1 + pi2).M() * GEV > ppi_pip_low && (jpsi + p1 + pi2).M() * GEV < ppi_pip_high && (jpsi + pi1 + p2).M() * GEV > ppi_pip_low2 && (jpsi + pi1 + p2).M() * GEV < ppi_pip_high2) 
            {
                ppi_pip_2d_BD_to_JPSI_K_PI->Fill((jpsi + p1 + pi2).M() * GEV, (jpsi + p2 + pi1).M() * GEV, w);
            }
            else 
            {
                ppi_pip_2d_BD_to_JPSI_K_PI->Fill((jpsi + p1 + pi2).M() * GEV, (jpsi + p2 + pi1).M() * GEV, 0.);                
            }
 
            if ((jpsi + K1 + K2).M() * GEV > 4.97 && (jpsi + K1 + K2).M() * GEV < 6.0 && (jpsi + pi1 + pi2).M() * GEV > 4.55 && (jpsi + pi1 + pi2).M() * GEV < 5.65) {KK_pipi_2d_BD_to_JPSI_K_PI->Fill((jpsi + K1 + K2).M() * GEV, (jpsi + pi1 + pi2).M() * GEV, w_4tr);}
            else {KK_pipi_2d_BD_to_JPSI_K_PI->Fill((jpsi + K1 + K2).M() * GEV, (jpsi + pi1 + pi2).M() * GEV, 0.);}  
            
            KK_1d_BD_to_JPSI_K_PI->Fill((jpsi + K1 + K2).M() * GEV, w_4tr);
            pipi_1d_BD_to_JPSI_K_PI->Fill((jpsi + pi1 + pi2).M() * GEV, w_4tr);
            
            mu1_px = muon1_px;
            mu1_py = muon1_py;
            mu1_pz = muon1_pz;
            mu2_px = muon2_px;
            mu2_py = muon2_py;
            mu2_pz = muon2_pz;
            K_px = kaon_px;
            K_py = kaon_py;
            K_pz = kaon_pz;
            pi_px = pion_px;
            pi_py = pion_py;
            pi_pz = pion_pz;
            jpsipi1 = (jpsi + pi1).M() * GEV;
            jpsipi2 = (jpsi + pi2).M() * GEV;
            jpsiK1 = (jpsi + K1).M() * GEV;
            jpsiK2 = (jpsi + K2).M() * GEV;
            K1pi2 = (K1 + pi2).M() * GEV;
            K2pi1 = (K2 + pi1).M() * GEV;
            K1K2 = (K1 + K2).M() * GEV;
            p1K2 = (p1 + K2).M() * GEV;
            p2K1 = (p2 + K1).M() * GEV;
            jpsip1 = (jpsi + p1).M() * GEV;
            jpsip2 = (jpsi + p2).M() * GEV;
            jpsiKpi = (jpsi + K1 + pi2).M() * GEV;
            jpsipiK = (jpsi + K2 + pi1).M() * GEV;
            jpsiKK = (jpsi + K1 + K2).M() * GEV;
            jpsipipi = (jpsi + pi1 + pi2).M() * GEV;
            jpsipK = (jpsi + p1 + K2).M() * GEV;
            jpsiKp = (jpsi + p2 + K1).M() * GEV;
            jpsippi = (jpsi + p1 + pi2).M() * GEV;
            jpsipip = (jpsi + pi1 + p2).M() * GEV;
            signal = 0;
            control = 0;
            controlLb = 0;
            cos_theta_Zc_Kpi = abs(cos(theta_X(jpsi + K1 + pi2, jpsi + pi2, jpsi)));
            cos_theta_Zc_piK = abs(cos(theta_X(jpsi + K2 + pi1, jpsi + pi1, jpsi)));
            phi_K_Kpi = phi_K(jpsi + K1 + pi2, K1 + pi2, K1, jpsi);
            phi_K_piK = phi_K(jpsi + K2 + pi1, K2 + pi1, K2, jpsi);
            alpha_mu_Kpi = alpha_mu(mu2, jpsi, jpsi + pi2, jpsi + K1 + pi2);
            alpha_mu_piK = alpha_mu(mu2, jpsi, jpsi + pi2, jpsi + K1 + pi2);
            phi_mu_Kstar_Kpi = phi_mu_Kstar(jpsi + K1 + pi2, K1 + pi2, jpsi, mu2);
            phi_mu_Kstar_piK = phi_mu_Kstar(jpsi + K2 + pi1, K2 + pi1, jpsi, mu2);
            phi_mu_X_Kpi = phi_mu_X(jpsi + pi2, jpsi + pi2 + K1, jpsi, mu2);
            phi_mu_X_piK = phi_mu_X(jpsi + pi1, jpsi + pi1 + K2, jpsi, mu2);
            cos_theta_psi_X_Kpi = abs(cos(theta_psi_X(jpsi + pi2, jpsi, mu2)));
            cos_theta_psi_X_piK = abs(cos(theta_psi_X(jpsi + pi1, jpsi, mu2)));
            cos_theta_Kstar_Kpi = abs(cos(theta_Kstar(jpsi + K1 + pi2, K1 + pi2, K1)));
            cos_theta_Kstar_piK = abs(cos(theta_Kstar(jpsi + K2 + pi1, K2 + pi1, K2)));
            cos_theta_psi_Kstar_Kpi = abs(cos(theta_psi_Kstar(jpsi + K1 + pi2, jpsi, mu2)));
            cos_theta_psi_Kstar_piK = abs(cos(theta_psi_Kstar(jpsi + K2 + pi1, jpsi, mu2)));
            cos_theta_B0_Kpi = abs(cos(theta_B0(jpsi + K1 + pi2, K1 + pi2)));
            cos_theta_B0_piK = abs(cos(theta_B0(jpsi + K2 + pi1, K2 + pi1)));
            phi_psi_Kpi = phi_psi(jpsi + K1 + pi2, jpsi + pi2, jpsi);
            phi_psi_piK = phi_psi(jpsi + K2 + pi1, jpsi + pi1, jpsi);
            
            int xsa = area_hist->ProjectionX()->FindBin((jpsi + K1 + pi2).M() * GEV); 
            int ysa = area_hist->ProjectionY()->FindBin((jpsi + K2 + pi1).M() * GEV);
            if (area_hist->GetBinContent(xsa, ysa) == 1)
            {
                Jpsipi_2d_BD_to_JPSI_K_PI->Fill((jpsi + pi1).M() * GEV, (jpsi + pi2).M() * GEV, w);
                JpsiK_2d_BD_to_JPSI_K_PI->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV, w);
                Kpi_2d_BD_to_JPSI_K_PI->Fill((K1 + pi2).M() * GEV, (K2 + pi1).M() * GEV, w);
                cos_theta_Zc_BD_to_JPSI_K_PI->Fill(abs(cos(theta_X(jpsi + K1 + pi2, jpsi + pi2, jpsi))), abs(cos(theta_X(jpsi + K2 + pi1, jpsi + pi1, jpsi))), w);
                
                phi_K_BD_to_JPSI_K_PI->Fill(phi_K(jpsi + K1 + pi2, K1 + pi2, K1, jpsi), phi_K(jpsi + K2 + pi1, K2 + pi1, K2, jpsi), w);
                alpha_mu_BD_to_JPSI_K_PI->Fill(alpha_mu(mu2, jpsi, jpsi + pi2, jpsi + K1 + pi2), alpha_mu(mu2, jpsi, jpsi + pi1, jpsi + K2 + pi1), w);
                phi_mu_Kstar_BD_to_JPSI_K_PI->Fill(phi_mu_Kstar(jpsi + K1 + pi2, K1 + pi2, jpsi, mu2), phi_mu_Kstar(jpsi + K2 + pi1, K2 + pi1, jpsi, mu2), w);
                phi_mu_X_BD_to_JPSI_K_PI->Fill(phi_mu_X(jpsi + pi2, jpsi + pi2 + K1, jpsi, mu2), phi_mu_X(jpsi + pi1, jpsi + pi1 + K2, jpsi, mu2), w);
                cos_theta_psi_X_BD_to_JPSI_K_PI->Fill(abs(cos(theta_psi_X(jpsi + pi2, jpsi, mu2))), abs(cos(theta_psi_X(jpsi + pi1, jpsi, mu2))), w);
                cos_theta_Kstar_BD_to_JPSI_K_PI->Fill(abs(cos(theta_Kstar(jpsi + K1 + pi2, K1 + pi2, K1))), abs(cos(theta_Kstar(jpsi + K2 + pi1, K2 + pi1, K2))), w);
                cos_theta_psi_Kstar_BD_to_JPSI_K_PI->Fill(abs(cos(theta_psi_Kstar(jpsi + K1 + pi2, jpsi, mu2))), abs(cos(theta_psi_Kstar(jpsi + K2 + pi1, jpsi, mu2))), w);
                cos_theta_B0_BD_to_JPSI_K_PI->Fill(abs(cos(theta_B0(jpsi + K1 + pi2, K1 + pi2))), abs(cos(theta_B0(jpsi + K2 + pi1, K2 + pi1))), w);
                phi_psi_BD_to_JPSI_K_PI->Fill(phi_psi(jpsi + K1 + pi2, jpsi + pi2, jpsi), phi_psi(jpsi + K2 + pi1, jpsi + pi1, jpsi), w);
                
                B_pt_BD_to_JPSI_K_PI->Fill(sB_Bs_pt * GEV, w1);
                B_pt_BD_to_JPSI_K_PI_reweighted->Fill(sB_Bs_pt * GEV, w);
                
                mu1_px = muon1_px;
                mu1_py = muon1_py;
                mu1_pz = muon1_pz;
                mu2_px = muon2_px;
                mu2_py = muon2_py;
                mu2_pz = muon2_pz;
                K_px = kaon_px;
                K_py = kaon_py;
                K_pz = kaon_pz;
                pi_px = pion_px;
                pi_py = pion_py;
                pi_pz = pion_pz;
                jpsipi1 = (jpsi + pi1).M() * GEV;
                jpsipi2 = (jpsi + pi2).M() * GEV;
                jpsiK1 = (jpsi + K1).M() * GEV;
                jpsiK2 = (jpsi + K2).M() * GEV;
                K1pi2 = (K1 + pi2).M() * GEV;
                K2pi1 = (K2 + pi1).M() * GEV;
                
                for_me->Fill();
                signal = 1;
            }
            
            xsa = area_hist_Lb->ProjectionX()->FindBin((jpsi + p1 + K2).M() * GEV); 
            ysa = area_hist_Lb->ProjectionY()->FindBin((jpsi + p2 + K1).M() * GEV);
            
            if (area_hist_Lb->GetBinContent(xsa, ysa) == 1)
            {
                Jpsip_2d_controlLb_BD_to_JPSI_K_PI->Fill((jpsi + p1).M() * GEV, (jpsi + p2).M() * GEV, w);
                JpsiK_2d_controlLb_BD_to_JPSI_K_PI->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV, w);
                pK_2d_controlLb_BD_to_JPSI_K_PI->Fill((p1 + K2).M() * GEV, (p2 + K1).M() * GEV, w); 
                controlLb = 1;
            }
            
            if (5336. < (jpsi + K1 + K2).M() && (jpsi + K1 + K2).M() < 5396.)
            {
                KK_1d_control_BD_to_JPSI_K_PI->Fill((K1 + K2).M() * GEV, w);
                JpsiK1_1d_control_BD_to_JPSI_K_PI->Fill((jpsi + K1).M() * GEV, w);
                JpsiK2_1d_control_BD_to_JPSI_K_PI->Fill((jpsi + K2).M() * GEV, w);
                JpsiK_2d_control_BD_to_JPSI_K_PI->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV, w);
                mu1_px = muon1_px;
                mu1_py = muon1_py;
                mu1_pz = muon1_pz;
                mu2_px = muon2_px;
                mu2_py = muon2_py;
                mu2_pz = muon2_pz;
                K_px = kaon_px;
                K_py = kaon_py;
                K_pz = kaon_pz;
                pi_px = pion_px;
                pi_py = pion_py;
                pi_pz = pion_pz;
                jpsiK1 = (jpsi + K1).M() * GEV;
                jpsiK2 = (jpsi + K2).M() * GEV;
                K1K2 = (K1 + K2).M() * GEV;
                for_BsJpsiKK_control_BD_to_JPSI_K_PI->Fill();
                n_BD_to_JPSI_K_PI_counter += w;
                
                control = 1;
            }
            phs_BD_to_JPSI_K_PI->Fill();
        }
    }
    
    TChain mc_BD_to_JPSI_PI_PI("BsAllCandidates");
    mc_BD_to_JPSI_PI_PI.AddFile("datasets/mc_BD_to_JPSI_PI_PI_Bhad.root"); 
    mc_BD_to_JPSI_PI_PI.AddFile("datasets/mc_BD_to_JPSI_PI_PI_antiBhad.root");
    nentries = mc_BD_to_JPSI_PI_PI.GetEntries();
    cout << "Number of runs BD_to_JPSI_PI_PI\t" << nentries << endl;
    
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_mu1_px", &sB_mu1_px);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_mu1_py", &sB_mu1_py);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_mu1_pz", &sB_mu1_pz);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_mu2_px", &sB_mu2_px);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_mu2_py", &sB_mu2_py);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_mu2_pz", &sB_mu2_pz);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_trk1_px", &sB_trk1_px);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_trk1_py", &sB_trk1_py);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_trk1_pz", &sB_trk1_pz);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_trk2_px", &sB_trk2_px);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_trk2_py", &sB_trk2_py);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_trk2_pz", &sB_trk2_pz);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_trk1_charge", &sB_trk1_charge);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_trk2_charge", &sB_trk2_charge);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_Lxy_MaxSumPt", &sB_Lxy_MaxSumPt);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_Bs_pt", &sB_Bs_pt);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_mu1_pt", &sB_mu1_pt);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_mu2_pt", &sB_mu2_pt);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_trk1_pt", &sB_trk1_pt);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_trk2_pt", &sB_trk2_pt);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_Bs_chi2_ndof", &sB_Bs_chi2_ndof);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_MaxSumPt", &sB_MaxSumPt);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_mu1_eta", &sB_mu1_eta);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_mu2_eta", &sB_mu2_eta);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_trk1_eta", &sB_trk1_eta);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_trk2_eta", &sB_trk2_eta);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_Jpsi_mass", &sB_Jpsi_mass);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_Jpsi_chi2", &sB_Jpsi_chi2);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_w_trigger", &w_trigger);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_w_SF_mu1", &w_SF_mu1);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_w_SF_mu2", &w_SF_mu2);
    mc_BD_to_JPSI_PI_PI.SetBranchAddress("sB_B_pt", &Bs_B_pt);
    for(int i = 1; i <= nentries; i++)
    {
        mc_BD_to_JPSI_PI_PI.GetEntry(i);
        TLorentzVector mu1, mu2, pi1, pi2, K1, K2, p1, p2, jpsi;
        w = 1.;
        mu1.SetXYZM(sB_mu1_px, sB_mu1_py, sB_mu1_pz, 105.65837);
        mu2.SetXYZM(sB_mu2_px, sB_mu2_py, sB_mu2_pz, 105.65837);
        pi1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 139.57);
        pi2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 139.57);
        K1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 493.677);
        K2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 493.677);
        p1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 938.272);
        p2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 938.272);
        jpsi.SetXYZM(sB_mu1_px + sB_mu2_px, sB_mu1_py + sB_mu2_py, sB_mu1_pz + sB_mu2_pz, 3096.);
        TLorentzVector bs = jpsi + pi1 + pi2;
        if ((K1 + pi2).M() > 1550. && (pi1 + K2).M() > 1550. && sB_Lxy_MaxSumPt > 1. && (jpsi + pi1).M() < 4900.  && (jpsi + pi2).M() < 4900. && sB_mu1_pt > 4000. && sB_mu2_pt > 4000. && sB_trk1_pt > 2000. && sB_trk2_pt > 2000.  && sB_Bs_chi2_ndof < 1.7 && sB_Bs_pt > 0.25 * sB_MaxSumPt && 4900. < (jpsi + K1 + pi2).M() && (jpsi + K1 + pi2).M() < 5700. && (jpsi + pi1 + K2).M() < 5900. && 2950. < sB_Jpsi_mass && sB_Jpsi_mass < 3250.) 
        {
            if (sB_trk1_charge * sB_trk2_charge > 0) {w = -1;}
            //i_temp += w;
            w_dimuon = w_trigger * w_SF_mu1 * w_SF_mu2;
            if (Bs_B_pt * GEV <= 50.)
            {
                w_B_pt = (*vpar_B_pt)(0) + (*vpar_B_pt)(1) * (Bs_B_pt * GEV);
            }
            else
            {
                w_B_pt = (*vpar_B_pt)(0) + (*vpar_B_pt)(1) * 50.;
            }
            w1 = w * w_dimuon;
            w_dimuon = w_dimuon * w_B_pt;
            w = w * w_dimuon;
            w_4tr = w;
            
            if ((jpsi + pi1 + K2).M() > 4650.) {Kpi_piK_2d_BD_to_JPSI_PI_PI->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, w_4tr);
                Kpi_piK_2d_BD_to_JPSI_PI_PI_for_ea->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, w_4tr);}
            else {Kpi_piK_2d_BD_to_JPSI_PI_PI->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, 0.);
                Kpi_piK_2d_BD_to_JPSI_PI_PI_for_ea->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, 0.);} 
             
            if ((jpsi + p1 + K2).M() * GEV > 5.2 && (jpsi + p1 + K2).M() * GEV < 7.0 && (jpsi + K1 + p2).M() * GEV > 5.15 && (jpsi + K1 + p2).M() * GEV < 7.0) 
            {
                pK_Kp_2d_BD_to_JPSI_PI_PI->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, w_4tr);
                //pK_Kp_2d_test_BD_to_JPSI_PI_PI->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, w_4tr);
                pK_Kp_2d_rotate_BD_to_JPSI_PI_PI->Fill((1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, (-1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, w_4tr);
            }
            else 
            {
                pK_Kp_2d_BD_to_JPSI_PI_PI->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, 0.);
                //pK_Kp_2d_test_BD_to_JPSI_PI_PI->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, 0.);
                pK_Kp_2d_rotate_BD_to_JPSI_PI_PI->Fill((1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, (-1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, 0.);
                
            }
            
            if ((jpsi + p1 + K2).M() * GEV > 5.2 && (jpsi + p1 + K2).M() * GEV < 6.8 && (jpsi + K1 + p2).M() * GEV > 5.15 && (jpsi + K1 + p2).M() * GEV < 6.8)
            {
                pK_Kp_2d_test_BD_to_JPSI_PI_PI->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, w);
            }
            else 
            {
                pK_Kp_2d_test_BD_to_JPSI_PI_PI->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, 0.);                
            }
            
            if ((jpsi + p1 + pi2).M() * GEV > ppi_pip_low && (jpsi + p1 + pi2).M() * GEV < ppi_pip_high && (jpsi + pi1 + p2).M() * GEV > ppi_pip_low2 && (jpsi + pi1 + p2).M() * GEV < ppi_pip_high2) 
            {
                ppi_pip_2d_BD_to_JPSI_PI_PI->Fill((jpsi + p1 + pi2).M() * GEV, (jpsi + p2 + pi1).M() * GEV, w);
            }
            else 
            {
                ppi_pip_2d_BD_to_JPSI_PI_PI->Fill((jpsi + p1 + pi2).M() * GEV, (jpsi + p2 + pi1).M() * GEV, 0.);                
            }
 
            if ((jpsi + K1 + K2).M() * GEV > 4.97 && (jpsi + K1 + K2).M() * GEV < 6.0 && (jpsi + pi1 + pi2).M() * GEV > 4.55 && (jpsi + pi1 + pi2).M() * GEV < 5.65) {KK_pipi_2d_BD_to_JPSI_PI_PI->Fill((jpsi + K1 + K2).M() * GEV, (jpsi + pi1 + pi2).M() * GEV, w_4tr);}
            else {KK_pipi_2d_BD_to_JPSI_PI_PI->Fill((jpsi + K1 + K2).M() * GEV, (jpsi + pi1 + pi2).M() * GEV, 0.);}
            
            KK_1d_BD_to_JPSI_PI_PI->Fill((jpsi + K1 + K2).M() * GEV, w_4tr);
            pipi_1d_BD_to_JPSI_PI_PI->Fill((jpsi + pi1 + pi2).M() * GEV, w_4tr);
            
            int xsa = area_hist->ProjectionX()->FindBin((jpsi + K1 + pi2).M() * GEV); 
            int ysa = area_hist->ProjectionY()->FindBin((jpsi + K2 + pi1).M() * GEV);
            if (area_hist->GetBinContent(xsa, ysa) == 1)
            {
                Jpsipi_2d_BD_to_JPSI_PI_PI->Fill((jpsi + pi1).M() * GEV, (jpsi + pi2).M() * GEV, w);
                JpsiK_2d_BD_to_JPSI_PI_PI->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV, w);
                Kpi_2d_BD_to_JPSI_PI_PI->Fill((K1 + pi2).M() * GEV, (K2 + pi1).M() * GEV, w);
                cos_theta_Zc_BD_to_JPSI_PI_PI->Fill(abs(cos(theta_X(jpsi + K1 + pi2, jpsi + pi2, jpsi))), abs(cos(theta_X(jpsi + K2 + pi1, jpsi + pi1, jpsi))), w);
                phi_K_BD_to_JPSI_PI_PI->Fill(phi_K(jpsi + K1 + pi2, K1 + pi2, K1, jpsi), phi_K(jpsi + K2 + pi1, K2 + pi1, K2, jpsi), w);
                alpha_mu_BD_to_JPSI_PI_PI->Fill(alpha_mu(mu2, jpsi, jpsi + pi2, jpsi + K1 + pi2), alpha_mu(mu2, jpsi, jpsi + pi1, jpsi + K2 + pi1), w);
                phi_mu_Kstar_BD_to_JPSI_PI_PI->Fill(phi_mu_Kstar(jpsi + K1 + pi2, K1 + pi2, jpsi, mu2), phi_mu_Kstar(jpsi + K2 + pi1, K2 + pi1, jpsi, mu2), w);
                phi_mu_X_BD_to_JPSI_PI_PI->Fill(phi_mu_X(jpsi + pi2, jpsi + pi2 + K1, jpsi, mu2), phi_mu_X(jpsi + pi1, jpsi + pi1 + K2, jpsi, mu2), w);
                cos_theta_psi_X_BD_to_JPSI_PI_PI->Fill(abs(cos(theta_psi_X(jpsi + pi2, jpsi, mu2))), abs(cos(theta_psi_X(jpsi + pi1, jpsi, mu2))), w);
                cos_theta_Kstar_BD_to_JPSI_PI_PI->Fill(abs(cos(theta_Kstar(jpsi + K1 + pi2, K1 + pi2, K1))), abs(cos(theta_Kstar(jpsi + K2 + pi1, K2 + pi1, K2))), w);
                cos_theta_psi_Kstar_BD_to_JPSI_PI_PI->Fill(abs(cos(theta_psi_Kstar(jpsi + K1 + pi2, jpsi, mu2))), abs(cos(theta_psi_Kstar(jpsi + K2 + pi1, jpsi, mu2))), w);
                cos_theta_B0_BD_to_JPSI_PI_PI->Fill(abs(cos(theta_B0(jpsi + K1 + pi2, K1 + pi2))), abs(cos(theta_B0(jpsi + K2 + pi1, K2 + pi1))), w);
                phi_psi_BD_to_JPSI_PI_PI->Fill(phi_psi(jpsi + K1 + pi2, jpsi + pi2, jpsi), phi_psi(jpsi + K2 + pi1, jpsi + pi1, jpsi), w);
                
                B_pt_BD_to_JPSI_PI_PI->Fill(sB_Bs_pt * GEV, w1);
                B_pt_BD_to_JPSI_PI_PI_reweighted->Fill(sB_Bs_pt * GEV, w);
            }  
            
            xsa = area_hist_Lb->ProjectionX()->FindBin((jpsi + p1 + K2).M() * GEV); 
            ysa = area_hist_Lb->ProjectionY()->FindBin((jpsi + p2 + K1).M() * GEV);
            
            if (area_hist_Lb->GetBinContent(xsa, ysa) == 1)
            {
                Jpsip_2d_controlLb_BD_to_JPSI_PI_PI->Fill((jpsi + p1).M() * GEV, (jpsi + p2).M() * GEV, w);
                JpsiK_2d_controlLb_BD_to_JPSI_PI_PI->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV, w);
                pK_2d_controlLb_BD_to_JPSI_PI_PI->Fill((p1 + K2).M() * GEV, (p2 + K1).M() * GEV, w);                
            }
            
            if (5336. < (jpsi + K1 + K2).M() && (jpsi + K1 + K2).M() < 5396.)
            {
                KK_1d_control_BD_to_JPSI_PI_PI->Fill((K1 + K2).M() * GEV, w);
                JpsiK1_1d_control_BD_to_JPSI_PI_PI->Fill((jpsi + K1).M() * GEV, w);
                JpsiK2_1d_control_BD_to_JPSI_PI_PI->Fill((jpsi + K2).M() * GEV, w);
                JpsiK_2d_control_BD_to_JPSI_PI_PI->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV, w);
                n_BD_to_JPSI_PI_PI_counter += w;
            }
        }
    }
       
    TChain mc_LAMBDA0B_to_JPSI_P_PI("BsAllCandidates");
    mc_LAMBDA0B_to_JPSI_P_PI.AddFile("datasets/mc_LAMBDA0B_to_JPSI_P_PI_Bhad.root");
    mc_LAMBDA0B_to_JPSI_P_PI.AddFile("datasets/mc_LAMBDA0B_to_JPSI_P_PI_antiBhad.root");
    nentries = mc_LAMBDA0B_to_JPSI_P_PI.GetEntries();
    cout << "Number of runs LAMBDA0B_to_JPSI_P_PI\t" << nentries << endl;
    
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_mu1_px", &sB_mu1_px);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_mu1_py", &sB_mu1_py);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_mu1_pz", &sB_mu1_pz);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_mu2_px", &sB_mu2_px);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_mu2_py", &sB_mu2_py);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_mu2_pz", &sB_mu2_pz);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_trk1_px", &sB_trk1_px);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_trk1_py", &sB_trk1_py);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_trk1_pz", &sB_trk1_pz);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_trk2_px", &sB_trk2_px);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_trk2_py", &sB_trk2_py);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_trk2_pz", &sB_trk2_pz);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_trk1_charge", &sB_trk1_charge);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_trk2_charge", &sB_trk2_charge);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_Lxy_MaxSumPt", &sB_Lxy_MaxSumPt);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_Bs_pt", &sB_Bs_pt);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_mu1_pt", &sB_mu1_pt);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_mu2_pt", &sB_mu2_pt);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_trk1_pt", &sB_trk1_pt);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_trk2_pt", &sB_trk2_pt);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_Bs_chi2_ndof", &sB_Bs_chi2_ndof);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_MaxSumPt", &sB_MaxSumPt);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_mu1_eta", &sB_mu1_eta);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_mu2_eta", &sB_mu2_eta);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_trk1_eta", &sB_trk1_eta);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_trk2_eta", &sB_trk2_eta);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_Jpsi_mass", &sB_Jpsi_mass);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_Jpsi_chi2", &sB_Jpsi_chi2);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_w_trigger", &w_trigger);
    mc_LAMBDA0B_to_JPSI_P_PI.SetBranchAddress("sB_B_pt", &Bs_B_pt);
    for(int i = 1; i <= nentries; i++)
    {
        mc_LAMBDA0B_to_JPSI_P_PI.GetEntry(i);
        TLorentzVector mu1, mu2, pi1, pi2, K1, K2, p1, p2, jpsi;
        w = 1.;
        mu1.SetXYZM(sB_mu1_px, sB_mu1_py, sB_mu1_pz, 105.65837);
        mu2.SetXYZM(sB_mu2_px, sB_mu2_py, sB_mu2_pz, 105.65837);
        pi1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 139.57);
        pi2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 139.57);
        K1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 493.677);
        K2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 493.677);
        p1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 938.272);
        p2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 938.272);
        jpsi.SetXYZM(sB_mu1_px + sB_mu2_px, sB_mu1_py + sB_mu2_py, sB_mu1_pz + sB_mu2_pz, 3096.);
        TLorentzVector bs = jpsi + pi1 + pi2;
        if ((K1 + pi2).M() > 1550. && (pi1 + K2).M() > 1550. && sB_Lxy_MaxSumPt > 1. && (jpsi + pi1).M() < 4900.  && (jpsi + pi2).M() < 4900. && sB_mu1_pt > 4000. && sB_mu2_pt > 4000. && sB_trk1_pt > 2000. && sB_trk2_pt > 2000.  && sB_Bs_chi2_ndof < 1.7 && sB_Bs_pt > 0.25 * sB_MaxSumPt && 4900. < (jpsi + K1 + pi2).M() && (jpsi + K1 + pi2).M() < 5700. && (jpsi + pi1 + K2).M() < 5900. && 2950. < sB_Jpsi_mass && sB_Jpsi_mass < 3250.) 
        {
            if (sB_trk1_charge * sB_trk2_charge > 0) {w = -1;}
            //i_temp += w;
            w_dimuon = w_trigger;
            if (Bs_B_pt * GEV <= 50.)
            {
                w_B_pt = (*vpar_B_pt)(0) + (*vpar_B_pt)(1) * (Bs_B_pt * GEV);
            }
            else
            {
                w_B_pt = (*vpar_B_pt)(0) + (*vpar_B_pt)(1) * 50.;
            }
            w1 = w * w_dimuon;
            w_dimuon = w_dimuon * w_B_pt;
            w = w * w_dimuon;
            w_4tr = w;
            
            if ((jpsi + pi1 + K2).M() > 4650.) {Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, w_4tr);
                Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI_for_ea->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, w_4tr);}
            else {Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, 0.);
                Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI_for_ea->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, 0.);} 
             
            if ((jpsi + p1 + K2).M() * GEV > 5.2 && (jpsi + p1 + K2).M() * GEV < 7.0 && (jpsi + K1 + p2).M() * GEV > 5.15 && (jpsi + K1 + p2).M() * GEV < 7.0) 
            {
                pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, w_4tr);
                //pK_Kp_2d_test_LAMBDA0B_to_JPSI_P_PI->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, w_4tr);
                pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_PI->Fill((1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, (-1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, w_4tr);
            }
            else 
            {
                pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, 0.);
                //pK_Kp_2d_test_LAMBDA0B_to_JPSI_P_PI->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, 0.);
                pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_PI->Fill((1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, (-1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, 0.);
                
            }
            
            if ((jpsi + p1 + pi2).M() * GEV > ppi_pip_low && (jpsi + p1 + pi2).M() * GEV < ppi_pip_high && (jpsi + pi1 + p2).M() * GEV > ppi_pip_low2 && (jpsi + pi1 + p2).M() * GEV < ppi_pip_high2) 
            {
                ppi_pip_2d_LAMBDA0B_to_JPSI_P_PI->Fill((jpsi + p1 + pi2).M() * GEV, (jpsi + p2 + pi1).M() * GEV, w);
            }
            else 
            {
                ppi_pip_2d_LAMBDA0B_to_JPSI_P_PI->Fill((jpsi + p1 + pi2).M() * GEV, (jpsi + p2 + pi1).M() * GEV, 0.);                
            }
            
            if ((jpsi + K1 + K2).M() * GEV > 4.97 && (jpsi + K1 + K2).M() * GEV < 6.0 && (jpsi + pi1 + pi2).M() * GEV > 4.55 && (jpsi + pi1 + pi2).M() * GEV < 5.65) {KK_pipi_2d_LAMBDA0B_to_JPSI_P_PI->Fill((jpsi + K1 + K2).M() * GEV, (jpsi + pi1 + pi2).M() * GEV, w_4tr);}
            else {KK_pipi_2d_LAMBDA0B_to_JPSI_P_PI->Fill((jpsi + K1 + K2).M() * GEV, (jpsi + pi1 + pi2).M() * GEV, 0.);}
            
            KK_1d_LAMBDA0B_to_JPSI_P_PI->Fill((jpsi + K1 + K2).M() * GEV, w_4tr);
            pipi_1d_LAMBDA0B_to_JPSI_P_PI->Fill((jpsi + pi1 + pi2).M() * GEV, w_4tr);
            
            int xsa = area_hist->ProjectionX()->FindBin((jpsi + K1 + pi2).M() * GEV); 
            int ysa = area_hist->ProjectionY()->FindBin((jpsi + K2 + pi1).M() * GEV);
            if (area_hist->GetBinContent(xsa, ysa) == 1)
            {
                Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->Fill((jpsi + pi1).M() * GEV, (jpsi + pi2).M() * GEV, w);
                JpsiK_2d_LAMBDA0B_to_JPSI_P_PI->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV, w);
                Kpi_2d_LAMBDA0B_to_JPSI_P_PI->Fill((K1 + pi2).M() * GEV, (K2 + pi1).M() * GEV, w);
                cos_theta_Zc_LAMBDA0B_to_JPSI_P_PI->Fill(abs(cos(theta_X(jpsi + K1 + pi2, jpsi + pi2, jpsi))), abs(cos(theta_X(jpsi + K2 + pi1, jpsi + pi1, jpsi))), w);
                phi_K_LAMBDA0B_to_JPSI_P_PI->Fill(phi_K(jpsi + K1 + pi2, K1 + pi2, K1, jpsi), phi_K(jpsi + K2 + pi1, K2 + pi1, K2, jpsi), w);
                alpha_mu_LAMBDA0B_to_JPSI_P_PI->Fill(alpha_mu(mu2, jpsi, jpsi + pi2, jpsi + K1 + pi2), alpha_mu(mu2, jpsi, jpsi + pi1, jpsi + K2 + pi1), w);
                phi_mu_Kstar_LAMBDA0B_to_JPSI_P_PI->Fill(phi_mu_Kstar(jpsi + K1 + pi2, K1 + pi2, jpsi, mu2), phi_mu_Kstar(jpsi + K2 + pi1, K2 + pi1, jpsi, mu2), w);
                phi_mu_X_LAMBDA0B_to_JPSI_P_PI->Fill(phi_mu_X(jpsi + pi2, jpsi + pi2 + K1, jpsi, mu2), phi_mu_X(jpsi + pi1, jpsi + pi1 + K2, jpsi, mu2), w);
                cos_theta_psi_X_LAMBDA0B_to_JPSI_P_PI->Fill(abs(cos(theta_psi_X(jpsi + pi2, jpsi, mu2))), abs(cos(theta_psi_X(jpsi + pi1, jpsi, mu2))), w);
                cos_theta_Kstar_LAMBDA0B_to_JPSI_P_PI->Fill(abs(cos(theta_Kstar(jpsi + K1 + pi2, K1 + pi2, K1))), abs(cos(theta_Kstar(jpsi + K2 + pi1, K2 + pi1, K2))), w);
                cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_PI->Fill(abs(cos(theta_psi_Kstar(jpsi + K1 + pi2, jpsi, mu2))), abs(cos(theta_psi_Kstar(jpsi + K2 + pi1, jpsi, mu2))), w);
                cos_theta_B0_LAMBDA0B_to_JPSI_P_PI->Fill(abs(cos(theta_B0(jpsi + K1 + pi2, K1 + pi2))), abs(cos(theta_B0(jpsi + K2 + pi1, K2 + pi1))), w);
                phi_psi_LAMBDA0B_to_JPSI_P_PI->Fill(phi_psi(jpsi + K1 + pi2, jpsi + pi2, jpsi), phi_psi(jpsi + K2 + pi1, jpsi + pi1, jpsi), w);
                
                //B_pt_LAMBDA0B_to_JPSI_P_PI->Fill(sB_Bs_pt * GEV, w1);
                //B_pt_LAMBDA0B_to_JPSI_P_PI_reweighted->Fill(sB_Bs_pt * GEV, w);
            }  
            
            xsa = area_hist_Lb->ProjectionX()->FindBin((jpsi + p1 + K2).M() * GEV); 
            ysa = area_hist_Lb->ProjectionY()->FindBin((jpsi + p2 + K1).M() * GEV);
            
            if (area_hist_Lb->GetBinContent(xsa, ysa) == 1)
            {
                Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->Fill((jpsi + p1).M() * GEV, (jpsi + p2).M() * GEV, w);
                JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV, w);
                pK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->Fill((p1 + K2).M() * GEV, (p2 + K1).M() * GEV, w);                
            }
            
            if (5336. < (jpsi + K1 + K2).M() && (jpsi + K1 + K2).M() < 5396.)
            {
                KK_1d_control_LAMBDA0B_to_JPSI_P_PI->Fill((K1 + K2).M() * GEV, w);
                JpsiK1_1d_control_LAMBDA0B_to_JPSI_P_PI->Fill((jpsi + K1).M() * GEV, w);
                JpsiK2_1d_control_LAMBDA0B_to_JPSI_P_PI->Fill((jpsi + K2).M() * GEV, w);
                JpsiK_2d_control_LAMBDA0B_to_JPSI_P_PI->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV, w);
                n_LAMBDA0B_to_JPSI_P_PI_counter += w;
            }
        }
    }
    
    TChain mc_BS_to_JPSI_K_PI("BsAllCandidates");
    mc_BS_to_JPSI_K_PI.AddFile("datasets/mc_BS_to_JPSI_K_PI_Bhad.root"); 
    mc_BS_to_JPSI_K_PI.AddFile("datasets/mc_BS_to_JPSI_K_PI_antiBhad.root");
    nentries = mc_BS_to_JPSI_K_PI.GetEntries();
    cout << "Number of runs BS_to_JPSI_K_PI\t" << nentries << endl;
    
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_mu1_px", &sB_mu1_px);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_mu1_py", &sB_mu1_py);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_mu1_pz", &sB_mu1_pz);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_mu2_px", &sB_mu2_px);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_mu2_py", &sB_mu2_py);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_mu2_pz", &sB_mu2_pz);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_trk1_px", &sB_trk1_px);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_trk1_py", &sB_trk1_py);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_trk1_pz", &sB_trk1_pz);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_trk2_px", &sB_trk2_px);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_trk2_py", &sB_trk2_py);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_trk2_pz", &sB_trk2_pz);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_trk1_charge", &sB_trk1_charge);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_trk2_charge", &sB_trk2_charge);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_Lxy_MaxSumPt", &sB_Lxy_MaxSumPt);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_Bs_pt", &sB_Bs_pt);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_mu1_pt", &sB_mu1_pt);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_mu2_pt", &sB_mu2_pt);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_trk1_pt", &sB_trk1_pt);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_trk2_pt", &sB_trk2_pt);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_Bs_chi2_ndof", &sB_Bs_chi2_ndof);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_MaxSumPt", &sB_MaxSumPt);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_mu1_eta", &sB_mu1_eta);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_mu2_eta", &sB_mu2_eta);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_trk1_eta", &sB_trk1_eta);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_trk2_eta", &sB_trk2_eta);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_Jpsi_mass", &sB_Jpsi_mass);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_Jpsi_chi2", &sB_Jpsi_chi2);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_w_trigger", &w_trigger);
    mc_BS_to_JPSI_K_PI.SetBranchAddress("sB_B_pt", &Bs_B_pt);
    for(int i = 1; i <= nentries; i++)
    {
        mc_BS_to_JPSI_K_PI.GetEntry(i);
        TLorentzVector mu1, mu2, pi1, pi2, K1, K2, p1, p2, jpsi;
        w = 1.;
        mu1.SetXYZM(sB_mu1_px, sB_mu1_py, sB_mu1_pz, 105.65837);
        mu2.SetXYZM(sB_mu2_px, sB_mu2_py, sB_mu2_pz, 105.65837);
        pi1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 139.57);
        pi2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 139.57);
        K1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 493.677);
        K2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 493.677);
        p1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 938.272);
        p2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 938.272);
        jpsi.SetXYZM(sB_mu1_px + sB_mu2_px, sB_mu1_py + sB_mu2_py, sB_mu1_pz + sB_mu2_pz, 3096.);
        TLorentzVector bs = jpsi + pi1 + pi2;
        if ((K1 + pi2).M() > 1550. && (pi1 + K2).M() > 1550. && sB_Lxy_MaxSumPt > 1. && (jpsi + pi1).M() < 4900.  && (jpsi + pi2).M() < 4900. && sB_mu1_pt > 4000. && sB_mu2_pt > 4000. && sB_trk1_pt > 2000. && sB_trk2_pt > 2000.  && sB_Bs_chi2_ndof < 1.7 && sB_Bs_pt > 0.25 * sB_MaxSumPt && 4900. < (jpsi + K1 + pi2).M() && (jpsi + K1 + pi2).M() < 5700. && (jpsi + pi1 + K2).M() < 5900. && 2950. < sB_Jpsi_mass && sB_Jpsi_mass < 3250.) 
        {
            if (sB_trk1_charge * sB_trk2_charge > 0) {w = -1;}
            //i_temp += w;
            w_dimuon = w_trigger;
            if (Bs_B_pt * GEV <= 50.)
            {
                w_B_pt = (*vpar_B_pt)(0) + (*vpar_B_pt)(1) * (Bs_B_pt * GEV);
            }
            else
            {
                w_B_pt = (*vpar_B_pt)(0) + (*vpar_B_pt)(1) * 50.;
            }
            w1 = w * w_dimuon;
            w_dimuon = w_dimuon * w_B_pt;
            w = w * w_dimuon;
            w_4tr = w;
            
            if ((jpsi + pi1 + K2).M() > 4650.) {Kpi_piK_2d_BS_to_JPSI_K_PI->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, w_4tr);
                Kpi_piK_2d_BS_to_JPSI_K_PI_for_ea->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, w_4tr);}
            else {Kpi_piK_2d_BS_to_JPSI_K_PI->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, 0.);
                Kpi_piK_2d_BS_to_JPSI_K_PI_for_ea->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, 0.);} 
             
            if ((jpsi + p1 + K2).M() * GEV > 5.2 && (jpsi + p1 + K2).M() * GEV < 7.0 && (jpsi + K1 + p2).M() * GEV > 5.15 && (jpsi + K1 + p2).M() * GEV < 7.0) 
            {
                pK_Kp_2d_BS_to_JPSI_K_PI->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, w_4tr);
                //pK_Kp_2d_test_BS_to_JPSI_K_PI->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, w_4tr);
                pK_Kp_2d_rotate_BS_to_JPSI_K_PI->Fill((1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, (-1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, w_4tr);
            }
            else 
            {
                pK_Kp_2d_BS_to_JPSI_K_PI->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, 0.);
                //pK_Kp_2d_test_BS_to_JPSI_K_PI->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, 0.);
                pK_Kp_2d_rotate_BS_to_JPSI_K_PI->Fill((1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, (-1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, 0.);
                
            }
            
            if ((jpsi + p1 + pi2).M() * GEV > ppi_pip_low && (jpsi + p1 + pi2).M() * GEV < ppi_pip_high && (jpsi + pi1 + p2).M() * GEV > ppi_pip_low2 && (jpsi + pi1 + p2).M() * GEV < ppi_pip_high2) 
            {
                ppi_pip_2d_BS_to_JPSI_K_PI->Fill((jpsi + p1 + pi2).M() * GEV, (jpsi + p2 + pi1).M() * GEV, w);
            }
            else 
            {
                ppi_pip_2d_BS_to_JPSI_K_PI->Fill((jpsi + p1 + pi2).M() * GEV, (jpsi + p2 + pi1).M() * GEV, 0.);                
            }
            
            if ((jpsi + K1 + K2).M() * GEV > 4.97 && (jpsi + K1 + K2).M() * GEV < 6.0 && (jpsi + pi1 + pi2).M() * GEV > 4.55 && (jpsi + pi1 + pi2).M() * GEV < 5.65) {KK_pipi_2d_BS_to_JPSI_K_PI->Fill((jpsi + K1 + K2).M() * GEV, (jpsi + pi1 + pi2).M() * GEV, w_4tr);}
            else {KK_pipi_2d_BS_to_JPSI_K_PI->Fill((jpsi + K1 + K2).M() * GEV, (jpsi + pi1 + pi2).M() * GEV, 0.);}
            
            KK_1d_BS_to_JPSI_K_PI->Fill((jpsi + K1 + K2).M() * GEV, w_4tr);
            pipi_1d_BS_to_JPSI_K_PI->Fill((jpsi + pi1 + pi2).M() * GEV, w_4tr);
            
            int xsa = area_hist->ProjectionX()->FindBin((jpsi + K1 + pi2).M() * GEV); 
            int ysa = area_hist->ProjectionY()->FindBin((jpsi + K2 + pi1).M() * GEV);
            if (area_hist->GetBinContent(xsa, ysa) == 1)
            {
                Jpsipi_2d_BS_to_JPSI_K_PI->Fill((jpsi + pi1).M() * GEV, (jpsi + pi2).M() * GEV, w);
                JpsiK_2d_BS_to_JPSI_K_PI->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV, w);
                Kpi_2d_BS_to_JPSI_K_PI->Fill((K1 + pi2).M() * GEV, (K2 + pi1).M() * GEV, w);
                cos_theta_Zc_BS_to_JPSI_K_PI->Fill(abs(cos(theta_X(jpsi + K1 + pi2, jpsi + pi2, jpsi))), abs(cos(theta_X(jpsi + K2 + pi1, jpsi + pi1, jpsi))), w);
                phi_K_BS_to_JPSI_K_PI->Fill(phi_K(jpsi + K1 + pi2, K1 + pi2, K1, jpsi), phi_K(jpsi + K2 + pi1, K2 + pi1, K2, jpsi), w);
                alpha_mu_BS_to_JPSI_K_PI->Fill(alpha_mu(mu2, jpsi, jpsi + pi2, jpsi + K1 + pi2), alpha_mu(mu2, jpsi, jpsi + pi1, jpsi + K2 + pi1), w);
                phi_mu_Kstar_BS_to_JPSI_K_PI->Fill(phi_mu_Kstar(jpsi + K1 + pi2, K1 + pi2, jpsi, mu2), phi_mu_Kstar(jpsi + K2 + pi1, K2 + pi1, jpsi, mu2), w);
                phi_mu_X_BS_to_JPSI_K_PI->Fill(phi_mu_X(jpsi + pi2, jpsi + pi2 + K1, jpsi, mu2), phi_mu_X(jpsi + pi1, jpsi + pi1 + K2, jpsi, mu2), w);
                cos_theta_psi_X_BS_to_JPSI_K_PI->Fill(abs(cos(theta_psi_X(jpsi + pi2, jpsi, mu2))), abs(cos(theta_psi_X(jpsi + pi1, jpsi, mu2))), w);
                cos_theta_Kstar_BS_to_JPSI_K_PI->Fill(abs(cos(theta_Kstar(jpsi + K1 + pi2, K1 + pi2, K1))), abs(cos(theta_Kstar(jpsi + K2 + pi1, K2 + pi1, K2))), w);
                cos_theta_psi_Kstar_BS_to_JPSI_K_PI->Fill(abs(cos(theta_psi_Kstar(jpsi + K1 + pi2, jpsi, mu2))), abs(cos(theta_psi_Kstar(jpsi + K2 + pi1, jpsi, mu2))), w);
                cos_theta_B0_BS_to_JPSI_K_PI->Fill(abs(cos(theta_B0(jpsi + K1 + pi2, K1 + pi2))), abs(cos(theta_B0(jpsi + K2 + pi1, K2 + pi1))), w);
                phi_psi_BS_to_JPSI_K_PI->Fill(phi_psi(jpsi + K1 + pi2, jpsi + pi2, jpsi), phi_psi(jpsi + K2 + pi1, jpsi + pi1, jpsi), w);
                
                //B_pt_BS_to_JPSI_K_PI->Fill(sB_Bs_pt * GEV, w1);
                //B_pt_BS_to_JPSI_K_PI_reweighted->Fill(sB_Bs_pt * GEV, w);
            }  
            
            xsa = area_hist_Lb->ProjectionX()->FindBin((jpsi + p1 + K2).M() * GEV); 
            ysa = area_hist_Lb->ProjectionY()->FindBin((jpsi + p2 + K1).M() * GEV);
            
            if (area_hist_Lb->GetBinContent(xsa, ysa) == 1)
            {
                Jpsip_2d_controlLb_BS_to_JPSI_K_PI->Fill((jpsi + p1).M() * GEV, (jpsi + p2).M() * GEV, w);
                JpsiK_2d_controlLb_BS_to_JPSI_K_PI->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV, w);
                pK_2d_controlLb_BS_to_JPSI_K_PI->Fill((p1 + K2).M() * GEV, (p2 + K1).M() * GEV, w);                
            }
            
            if (5336. < (jpsi + K1 + K2).M() && (jpsi + K1 + K2).M() < 5396.)
            {
                KK_1d_control_BS_to_JPSI_K_PI->Fill((K1 + K2).M() * GEV, w);
                JpsiK1_1d_control_BS_to_JPSI_K_PI->Fill((jpsi + K1).M() * GEV, w);
                JpsiK2_1d_control_BS_to_JPSI_K_PI->Fill((jpsi + K2).M() * GEV, w);
                JpsiK_2d_control_BS_to_JPSI_K_PI->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV, w);
                n_BS_to_JPSI_K_PI_counter += w;
            }
        }
    }
    
    TChain mc_BD_to_JPSI_K_K("BsAllCandidates");
    mc_BD_to_JPSI_K_K.AddFile("datasets/mc_BD_to_JPSI_K_K_Bhad.root"); 
    mc_BD_to_JPSI_K_K.AddFile("datasets/mc_BD_to_JPSI_K_K_antiBhad.root");
    nentries = mc_BD_to_JPSI_K_K.GetEntries();
    cout << "Number of runs BD_to_JPSI_K_K\t" << nentries << endl;
    
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_mu1_px", &sB_mu1_px);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_mu1_py", &sB_mu1_py);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_mu1_pz", &sB_mu1_pz);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_mu2_px", &sB_mu2_px);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_mu2_py", &sB_mu2_py);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_mu2_pz", &sB_mu2_pz);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_trk1_px", &sB_trk1_px);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_trk1_py", &sB_trk1_py);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_trk1_pz", &sB_trk1_pz);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_trk2_px", &sB_trk2_px);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_trk2_py", &sB_trk2_py);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_trk2_pz", &sB_trk2_pz);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_trk1_charge", &sB_trk1_charge);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_trk2_charge", &sB_trk2_charge);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_Lxy_MaxSumPt", &sB_Lxy_MaxSumPt);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_Bs_pt", &sB_Bs_pt);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_mu1_pt", &sB_mu1_pt);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_mu2_pt", &sB_mu2_pt);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_trk1_pt", &sB_trk1_pt);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_trk2_pt", &sB_trk2_pt);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_Bs_chi2_ndof", &sB_Bs_chi2_ndof);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_MaxSumPt", &sB_MaxSumPt);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_mu1_eta", &sB_mu1_eta);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_mu2_eta", &sB_mu2_eta);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_trk1_eta", &sB_trk1_eta);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_trk2_eta", &sB_trk2_eta);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_Jpsi_mass", &sB_Jpsi_mass);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_Jpsi_chi2", &sB_Jpsi_chi2);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_w_trigger", &w_trigger);
    mc_BD_to_JPSI_K_K.SetBranchAddress("sB_B_pt", &Bs_B_pt);
    for(int i = 1; i <= nentries; i++)
    {
        mc_BD_to_JPSI_K_K.GetEntry(i);
        TLorentzVector mu1, mu2, pi1, pi2, K1, K2, p1, p2, jpsi;
        w = 1.;
        mu1.SetXYZM(sB_mu1_px, sB_mu1_py, sB_mu1_pz, 105.65837);
        mu2.SetXYZM(sB_mu2_px, sB_mu2_py, sB_mu2_pz, 105.65837);
        pi1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 139.57);
        pi2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 139.57);
        K1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 493.677);
        K2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 493.677);
        p1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 938.272);
        p2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 938.272);
        jpsi.SetXYZM(sB_mu1_px + sB_mu2_px, sB_mu1_py + sB_mu2_py, sB_mu1_pz + sB_mu2_pz, 3096.);
        TLorentzVector bs = jpsi + pi1 + pi2;
        if ((K1 + pi2).M() > 1550. && (pi1 + K2).M() > 1550. && sB_Lxy_MaxSumPt > 1. && (jpsi + pi1).M() < 4900.  && (jpsi + pi2).M() < 4900. && sB_mu1_pt > 4000. && sB_mu2_pt > 4000. && sB_trk1_pt > 2000. && sB_trk2_pt > 2000.  && sB_Bs_chi2_ndof < 1.7 && sB_Bs_pt > 0.25 * sB_MaxSumPt && 4900. < (jpsi + K1 + pi2).M() && (jpsi + K1 + pi2).M() < 5700. && (jpsi + pi1 + K2).M() < 5900. && 2950. < sB_Jpsi_mass && sB_Jpsi_mass < 3250.) 
        {
            if (sB_trk1_charge * sB_trk2_charge > 0) {w = -1;}
            //i_temp += w;
            w_dimuon = w_trigger;
            if (Bs_B_pt * GEV <= 50.)
            {
                w_B_pt = (*vpar_B_pt)(0) + (*vpar_B_pt)(1) * (Bs_B_pt * GEV);
            }
            else
            {
                w_B_pt = (*vpar_B_pt)(0) + (*vpar_B_pt)(1) * 50.;
            }
            w1 = w * w_dimuon;
            w_dimuon = w_dimuon * w_B_pt;
            w = w * w_dimuon;
            w_4tr = w;
            
            if ((jpsi + pi1 + K2).M() > 4650.) {Kpi_piK_2d_BD_to_JPSI_K_K->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, w_4tr);
                Kpi_piK_2d_BD_to_JPSI_K_K_for_ea->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, w_4tr);}
            else {Kpi_piK_2d_BD_to_JPSI_K_K->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, 0.);
                Kpi_piK_2d_BD_to_JPSI_K_K_for_ea->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, 0.);} 
             
            if ((jpsi + p1 + K2).M() * GEV > 5.2 && (jpsi + p1 + K2).M() * GEV < 7.0 && (jpsi + K1 + p2).M() * GEV > 5.15 && (jpsi + K1 + p2).M() * GEV < 7.0) 
            {
                pK_Kp_2d_BD_to_JPSI_K_K->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, w_4tr);
                //pK_Kp_2d_test_BD_to_JPSI_K_K->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, w_4tr);
                pK_Kp_2d_rotate_BD_to_JPSI_K_K->Fill((1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, (-1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, w_4tr);
            }
            else 
            {
                pK_Kp_2d_BD_to_JPSI_K_K->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, 0.);
                //pK_Kp_2d_test_BD_to_JPSI_K_K->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, 0.);
                pK_Kp_2d_rotate_BD_to_JPSI_K_K->Fill((1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, (-1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, 0.);
                
            }
            
            if ((jpsi + p1 + pi2).M() * GEV > ppi_pip_low && (jpsi + p1 + pi2).M() * GEV < ppi_pip_high && (jpsi + pi1 + p2).M() * GEV > ppi_pip_low2 && (jpsi + pi1 + p2).M() * GEV < ppi_pip_high2) 
            {
                ppi_pip_2d_BD_to_JPSI_K_K->Fill((jpsi + p1 + pi2).M() * GEV, (jpsi + p2 + pi1).M() * GEV, w);
            }
            else 
            {
                ppi_pip_2d_BD_to_JPSI_K_K->Fill((jpsi + p1 + pi2).M() * GEV, (jpsi + p2 + pi1).M() * GEV, 0.);                
            }
 
            if ((jpsi + K1 + K2).M() * GEV > 4.97 && (jpsi + K1 + K2).M() * GEV < 6.0 && (jpsi + pi1 + pi2).M() * GEV > 4.55 && (jpsi + pi1 + pi2).M() * GEV < 5.65) {KK_pipi_2d_BD_to_JPSI_K_K->Fill((jpsi + K1 + K2).M() * GEV, (jpsi + pi1 + pi2).M() * GEV, w_4tr);}
            else {KK_pipi_2d_BD_to_JPSI_K_K->Fill((jpsi + K1 + K2).M() * GEV, (jpsi + pi1 + pi2).M() * GEV, 0.);}
            
            KK_1d_BD_to_JPSI_K_K->Fill((jpsi + K1 + K2).M() * GEV, w_4tr);
            pipi_1d_BD_to_JPSI_K_K->Fill((jpsi + pi1 + pi2).M() * GEV, w_4tr);
            
            int xsa = area_hist->ProjectionX()->FindBin((jpsi + K1 + pi2).M() * GEV); 
            int ysa = area_hist->ProjectionY()->FindBin((jpsi + K2 + pi1).M() * GEV);
            if (area_hist->GetBinContent(xsa, ysa) == 1)
            {
                Jpsipi_2d_BD_to_JPSI_K_K->Fill((jpsi + pi1).M() * GEV, (jpsi + pi2).M() * GEV, w);
                JpsiK_2d_BD_to_JPSI_K_K->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV, w);
                Kpi_2d_BD_to_JPSI_K_K->Fill((K1 + pi2).M() * GEV, (K2 + pi1).M() * GEV, w);
                cos_theta_Zc_BD_to_JPSI_K_K->Fill(abs(cos(theta_X(jpsi + K1 + pi2, jpsi + pi2, jpsi))), abs(cos(theta_X(jpsi + K2 + pi1, jpsi + pi1, jpsi))), w);
                phi_K_BD_to_JPSI_K_K->Fill(phi_K(jpsi + K1 + pi2, K1 + pi2, K1, jpsi), phi_K(jpsi + K2 + pi1, K2 + pi1, K2, jpsi), w);
                alpha_mu_BD_to_JPSI_K_K->Fill(alpha_mu(mu2, jpsi, jpsi + pi2, jpsi + K1 + pi2), alpha_mu(mu2, jpsi, jpsi + pi1, jpsi + K2 + pi1), w);
                phi_mu_Kstar_BD_to_JPSI_K_K->Fill(phi_mu_Kstar(jpsi + K1 + pi2, K1 + pi2, jpsi, mu2), phi_mu_Kstar(jpsi + K2 + pi1, K2 + pi1, jpsi, mu2), w);
                phi_mu_X_BD_to_JPSI_K_K->Fill(phi_mu_X(jpsi + pi2, jpsi + pi2 + K1, jpsi, mu2), phi_mu_X(jpsi + pi1, jpsi + pi1 + K2, jpsi, mu2), w);
                cos_theta_psi_X_BD_to_JPSI_K_K->Fill(abs(cos(theta_psi_X(jpsi + pi2, jpsi, mu2))), abs(cos(theta_psi_X(jpsi + pi1, jpsi, mu2))), w);
                cos_theta_Kstar_BD_to_JPSI_K_K->Fill(abs(cos(theta_Kstar(jpsi + K1 + pi2, K1 + pi2, K1))), abs(cos(theta_Kstar(jpsi + K2 + pi1, K2 + pi1, K2))), w);
                cos_theta_psi_Kstar_BD_to_JPSI_K_K->Fill(abs(cos(theta_psi_Kstar(jpsi + K1 + pi2, jpsi, mu2))), abs(cos(theta_psi_Kstar(jpsi + K2 + pi1, jpsi, mu2))), w);
                cos_theta_B0_BD_to_JPSI_K_K->Fill(abs(cos(theta_B0(jpsi + K1 + pi2, K1 + pi2))), abs(cos(theta_B0(jpsi + K2 + pi1, K2 + pi1))), w);
                phi_psi_BD_to_JPSI_K_K->Fill(phi_psi(jpsi + K1 + pi2, jpsi + pi2, jpsi), phi_psi(jpsi + K2 + pi1, jpsi + pi1, jpsi), w);
                
                //B_pt_BD_to_JPSI_K_K->Fill(sB_Bs_pt * GEV, w1);
                //B_pt_BD_to_JPSI_K_K_reweighted->Fill(sB_Bs_pt * GEV, w);
            }  
            
            xsa = area_hist_Lb->ProjectionX()->FindBin((jpsi + p1 + K2).M() * GEV); 
            ysa = area_hist_Lb->ProjectionY()->FindBin((jpsi + p2 + K1).M() * GEV);
            
            if (area_hist_Lb->GetBinContent(xsa, ysa) == 1)
            {
                Jpsip_2d_controlLb_BD_to_JPSI_K_K->Fill((jpsi + p1).M() * GEV, (jpsi + p2).M() * GEV, w);
                JpsiK_2d_controlLb_BD_to_JPSI_K_K->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV, w);
                pK_2d_controlLb_BD_to_JPSI_K_K->Fill((p1 + K2).M() * GEV, (p2 + K1).M() * GEV, w);                
            }
            
            if (5336. < (jpsi + K1 + K2).M() && (jpsi + K1 + K2).M() < 5396.)
            {
                KK_1d_control_BD_to_JPSI_K_K->Fill((K1 + K2).M() * GEV, w);
                JpsiK1_1d_control_BD_to_JPSI_K_K->Fill((jpsi + K1).M() * GEV, w);
                JpsiK2_1d_control_BD_to_JPSI_K_K->Fill((jpsi + K2).M() * GEV, w);
                JpsiK_2d_control_BD_to_JPSI_K_K->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV, w);
                n_BD_to_JPSI_K_K_counter += w;
            }
        }
    }

    TChain mc_BS_to_JPSI_PI_PI("BsAllCandidates");
    mc_BS_to_JPSI_PI_PI.AddFile("datasets/mc_BS_to_JPSI_PI_PI_Bhad.root");
    mc_BS_to_JPSI_PI_PI.AddFile("datasets/mc_BS_to_JPSI_PI_PI_antiBhad.root");
    nentries = mc_BS_to_JPSI_PI_PI.GetEntries();
    cout << "Number of runs BS_to_JPSI_PI_PI\t" << nentries << endl;
    
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_mu1_px", &sB_mu1_px);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_mu1_py", &sB_mu1_py);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_mu1_pz", &sB_mu1_pz);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_mu2_px", &sB_mu2_px);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_mu2_py", &sB_mu2_py);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_mu2_pz", &sB_mu2_pz);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_trk1_px", &sB_trk1_px);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_trk1_py", &sB_trk1_py);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_trk1_pz", &sB_trk1_pz);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_trk2_px", &sB_trk2_px);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_trk2_py", &sB_trk2_py);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_trk2_pz", &sB_trk2_pz);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_trk1_charge", &sB_trk1_charge);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_trk2_charge", &sB_trk2_charge);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_Lxy_MaxSumPt", &sB_Lxy_MaxSumPt);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_Bs_pt", &sB_Bs_pt);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_mu1_pt", &sB_mu1_pt);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_mu2_pt", &sB_mu2_pt);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_trk1_pt", &sB_trk1_pt);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_trk2_pt", &sB_trk2_pt);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_Bs_chi2_ndof", &sB_Bs_chi2_ndof);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_MaxSumPt", &sB_MaxSumPt);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_mu1_eta", &sB_mu1_eta);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_mu2_eta", &sB_mu2_eta);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_trk1_eta", &sB_trk1_eta);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_trk2_eta", &sB_trk2_eta);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_Jpsi_mass", &sB_Jpsi_mass);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_Jpsi_chi2", &sB_Jpsi_chi2);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_w_trigger", &w_trigger);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_w_SF_mu1", &w_SF_mu1);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_w_SF_mu2", &w_SF_mu2);
    mc_BS_to_JPSI_PI_PI.SetBranchAddress("sB_B_pt", &Bs_B_pt);
    for(int i = 1; i <= nentries; i++)
    {
        mc_BS_to_JPSI_PI_PI.GetEntry(i);
        TLorentzVector mu1, mu2, pi1, pi2, K1, K2, p1, p2, jpsi;
        w = 1.;
        mu1.SetXYZM(sB_mu1_px, sB_mu1_py, sB_mu1_pz, 105.65837);
        mu2.SetXYZM(sB_mu2_px, sB_mu2_py, sB_mu2_pz, 105.65837);
        pi1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 139.57);
        pi2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 139.57);
        K1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 493.677);
        K2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 493.677);
        p1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 938.272);
        p2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 938.272);
        jpsi.SetXYZM(sB_mu1_px + sB_mu2_px, sB_mu1_py + sB_mu2_py, sB_mu1_pz + sB_mu2_pz, 3096.);
        TLorentzVector bs = jpsi + pi1 + pi2;
        if ((K1 + pi2).M() > 1550. && (pi1 + K2).M() > 1550. && sB_Lxy_MaxSumPt > 1. && (jpsi + pi1).M() < 4900.  && (jpsi + pi2).M() < 4900. && sB_mu1_pt > 4000. && sB_mu2_pt > 4000. && sB_trk1_pt > 2000. && sB_trk2_pt > 2000.  && sB_Bs_chi2_ndof < 1.7 && sB_Bs_pt > 0.25 * sB_MaxSumPt && 4900. < (jpsi + K1 + pi2).M() && (jpsi + K1 + pi2).M() < 5700. && (jpsi + pi1 + K2).M() < 5900. && 2950. < sB_Jpsi_mass && sB_Jpsi_mass < 3250.) 
        {
            if (sB_trk1_charge * sB_trk2_charge > 0) {w = -1;}
            //i_temp += w;
            w_dimuon = w_trigger * w_SF_mu1 * w_SF_mu2;
            if (Bs_B_pt * GEV <= 50.)
            {
                w_B_pt = (*vpar_B_pt)(0) + (*vpar_B_pt)(1) * (Bs_B_pt * GEV);
            }
            else
            {
                w_B_pt = (*vpar_B_pt)(0) + (*vpar_B_pt)(1) * 50.;
            }
            w1 = w * w_dimuon;
            w_dimuon = w_dimuon * w_B_pt;
            w = w * w_dimuon;
            w_4tr = w;
            
            if ((jpsi + pi1 + K2).M() > 4650.) {Kpi_piK_2d_BS_to_JPSI_PI_PI->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, w_4tr);
                Kpi_piK_2d_BS_to_JPSI_PI_PI_for_ea->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, w_4tr);}
            else {Kpi_piK_2d_BS_to_JPSI_PI_PI->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, 0.);
                Kpi_piK_2d_BS_to_JPSI_PI_PI_for_ea->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, 0.);} 
             
            if ((jpsi + p1 + K2).M() * GEV > 5.2 && (jpsi + p1 + K2).M() * GEV < 7.0 && (jpsi + K1 + p2).M() * GEV > 5.15 && (jpsi + K1 + p2).M() * GEV < 7.0) 
            {
                pK_Kp_2d_BS_to_JPSI_PI_PI->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, w_4tr);
                //pK_Kp_2d_test_BS_to_JPSI_PI_PI->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, w_4tr);
                pK_Kp_2d_rotate_BS_to_JPSI_PI_PI->Fill((1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, (-1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, w_4tr);
            }
            else 
            {
                pK_Kp_2d_BS_to_JPSI_PI_PI->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, 0.);
                //pK_Kp_2d_test_BS_to_JPSI_PI_PI->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, 0.);
                pK_Kp_2d_rotate_BS_to_JPSI_PI_PI->Fill((1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, (-1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, 0.);
            } 
            
            if ((jpsi + p1 + K2).M() * GEV > 5.2 && (jpsi + p1 + K2).M() * GEV < 6.8 && (jpsi + K1 + p2).M() * GEV > 5.15 && (jpsi + K1 + p2).M() * GEV < 6.8)
            {
                pK_Kp_2d_test_BS_to_JPSI_PI_PI->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, w);
            }
            else 
            {
                pK_Kp_2d_test_BS_to_JPSI_PI_PI->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, 0.);                
            }
            
            if ((jpsi + p1 + pi2).M() * GEV > ppi_pip_low && (jpsi + p1 + pi2).M() * GEV < ppi_pip_high && (jpsi + pi1 + p2).M() * GEV > ppi_pip_low2 && (jpsi + pi1 + p2).M() * GEV < ppi_pip_high2) 
            {
                ppi_pip_2d_BS_to_JPSI_PI_PI->Fill((jpsi + p1 + pi2).M() * GEV, (jpsi + p2 + pi1).M() * GEV, w);
            }
            else 
            {
                ppi_pip_2d_BS_to_JPSI_PI_PI->Fill((jpsi + p1 + pi2).M() * GEV, (jpsi + p2 + pi1).M() * GEV, 0.);                
            }         
            
            if ((jpsi + K1 + K2).M() * GEV > 4.97 && (jpsi + K1 + K2).M() * GEV < 6.0 && (jpsi + pi1 + pi2).M() * GEV > 4.55 && (jpsi + pi1 + pi2).M() * GEV < 5.65) {KK_pipi_2d_BS_to_JPSI_PI_PI->Fill((jpsi + K1 + K2).M() * GEV, (jpsi + pi1 + pi2).M() * GEV, w_4tr);}
            else {KK_pipi_2d_BS_to_JPSI_PI_PI->Fill((jpsi + K1 + K2).M() * GEV, (jpsi + pi1 + pi2).M() * GEV, 0.);} 
            
            KK_1d_BS_to_JPSI_PI_PI->Fill((jpsi + K1 + K2).M() * GEV, w_4tr);
            pipi_1d_BS_to_JPSI_PI_PI->Fill((jpsi + pi1 + pi2).M() * GEV, w_4tr);

            int xsa = area_hist->ProjectionX()->FindBin((jpsi + K1 + pi2).M() * GEV); 
            int ysa = area_hist->ProjectionY()->FindBin((jpsi + K2 + pi1).M() * GEV);
            if (area_hist->GetBinContent(xsa, ysa) == 1)
            {
                Jpsipi_2d_BS_to_JPSI_PI_PI->Fill((jpsi + pi1).M() * GEV, (jpsi + pi2).M() * GEV, w);
                JpsiK_2d_BS_to_JPSI_PI_PI->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV, w);
                Kpi_2d_BS_to_JPSI_PI_PI->Fill((K1 + pi2).M() * GEV, (K2 + pi1).M() * GEV, w);
                cos_theta_Zc_BS_to_JPSI_PI_PI->Fill(abs(cos(theta_X(jpsi + K1 + pi2, jpsi + pi2, jpsi))), w);
                phi_K_BS_to_JPSI_PI_PI->Fill(phi_K(jpsi + K1 + pi2, K1 + pi2, K1, jpsi), phi_K(jpsi + K2 + pi1, K2 + pi1, K2, jpsi), w);
                alpha_mu_BS_to_JPSI_PI_PI->Fill(alpha_mu(mu2, jpsi, jpsi + pi2, jpsi + K1 + pi2), alpha_mu(mu2, jpsi, jpsi + pi1, jpsi + K2 + pi1), w);
                phi_mu_Kstar_BS_to_JPSI_PI_PI->Fill(phi_mu_Kstar(jpsi + K1 + pi2, K1 + pi2, jpsi, mu2), phi_mu_Kstar(jpsi + K2 + pi1, K2 + pi1, jpsi, mu2), w);
                phi_mu_X_BS_to_JPSI_PI_PI->Fill(phi_mu_X(jpsi + pi2, jpsi + pi2 + K1, jpsi, mu2), phi_mu_X(jpsi + pi1, jpsi + pi1 + K2, jpsi, mu2), w);
                cos_theta_psi_X_BS_to_JPSI_PI_PI->Fill(abs(cos(theta_psi_X(jpsi + pi2, jpsi, mu2))), abs(cos(theta_psi_X(jpsi + pi1, jpsi, mu2))), w);
                cos_theta_Kstar_BS_to_JPSI_PI_PI->Fill(abs(cos(theta_Kstar(jpsi + K1 + pi2, K1 + pi2, K1))), abs(cos(theta_Kstar(jpsi + K2 + pi1, K2 + pi1, K2))), w);
                cos_theta_psi_Kstar_BS_to_JPSI_PI_PI->Fill(abs(cos(theta_psi_Kstar(jpsi + K1 + pi2, jpsi, mu2))), abs(cos(theta_psi_Kstar(jpsi + K2 + pi1, jpsi, mu2))), w);
                cos_theta_B0_BS_to_JPSI_PI_PI->Fill(abs(cos(theta_B0(jpsi + K1 + pi2, K1 + pi2))), abs(cos(theta_B0(jpsi + K2 + pi1, K2 + pi1))), w);
                phi_psi_BS_to_JPSI_PI_PI->Fill(phi_psi(jpsi + K1 + pi2, jpsi + pi2, jpsi), phi_psi(jpsi + K2 + pi1, jpsi + pi1, jpsi), w);
                
                B_pt_BS_to_JPSI_PI_PI->Fill(sB_Bs_pt * GEV, w1);
                B_pt_BS_to_JPSI_PI_PI_reweighted->Fill(sB_Bs_pt * GEV, w);
            }  
            
            xsa = area_hist_Lb->ProjectionX()->FindBin((jpsi + p1 + K2).M() * GEV); 
            ysa = area_hist_Lb->ProjectionY()->FindBin((jpsi + p2 + K1).M() * GEV);
            
            if (area_hist_Lb->GetBinContent(xsa, ysa) == 1)
            {
                Jpsip_2d_controlLb_BS_to_JPSI_PI_PI->Fill((jpsi + p1).M() * GEV, (jpsi + p2).M() * GEV, w);
                JpsiK_2d_controlLb_BS_to_JPSI_PI_PI->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV, w);
                pK_2d_controlLb_BS_to_JPSI_PI_PI->Fill((p1 + K2).M() * GEV, (p2 + K1).M() * GEV, w);                
            }
            
            if (5336. < (jpsi + K1 + K2).M() && (jpsi + K1 + K2).M() < 5396.)
            {
                KK_1d_control_BS_to_JPSI_PI_PI->Fill((K1 + K2).M() * GEV, w);
                JpsiK1_1d_control_BS_to_JPSI_PI_PI->Fill((jpsi + K1).M() * GEV, w);
                JpsiK2_1d_control_BS_to_JPSI_PI_PI->Fill((jpsi + K2).M() * GEV, w);
                JpsiK_2d_control_BS_to_JPSI_PI_PI->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV, w);
                n_BS_to_JPSI_PI_PI_counter += w;
            }
        }
    }
    
    TChain mc_BS_to_JPSI_K_K("BsAllCandidates");
    mc_BS_to_JPSI_K_K.AddFile("datasets/mc_BS_to_JPSI_K_K_Bhad.root"); 
    mc_BS_to_JPSI_K_K.AddFile("datasets/mc_BS_to_JPSI_K_K_antiBhad.root"); 
    nentries = mc_BS_to_JPSI_K_K.GetEntries();
    cout << "Number of runs BS_to_JPSI_K_K\t" << nentries << endl;
    
    float muon1_BsKK_px, muon1_BsKK_py, muon1_BsKK_pz, muon2_BsKK_px, muon2_BsKK_py, muon2_BsKK_pz, Kplus_BsKK_px, Kplus_BsKK_py, Kplus_BsKK_pz, Kminus_BsKK_px, Kminus_BsKK_py, Kminus_BsKK_pz;
    
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_mu1_px", &sB_mu1_px);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_mu1_py", &sB_mu1_py);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_mu1_pz", &sB_mu1_pz);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_mu2_px", &sB_mu2_px);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_mu2_py", &sB_mu2_py);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_mu2_pz", &sB_mu2_pz);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_trk1_px", &sB_trk1_px);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_trk1_py", &sB_trk1_py);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_trk1_pz", &sB_trk1_pz);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_trk2_px", &sB_trk2_px);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_trk2_py", &sB_trk2_py);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_trk2_pz", &sB_trk2_pz);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_trk1_charge", &sB_trk1_charge);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_trk2_charge", &sB_trk2_charge);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_Lxy_MaxSumPt", &sB_Lxy_MaxSumPt);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_Bs_pt", &sB_Bs_pt);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_mu1_pt", &sB_mu1_pt);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_mu2_pt", &sB_mu2_pt);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_trk1_pt", &sB_trk1_pt);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_trk2_pt", &sB_trk2_pt);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_Bs_chi2_ndof", &sB_Bs_chi2_ndof);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_MaxSumPt", &sB_MaxSumPt);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_mu1_eta", &sB_mu1_eta);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_mu2_eta", &sB_mu2_eta);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_trk1_eta", &sB_trk1_eta);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_trk2_eta", &sB_trk2_eta);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_Jpsi_mass", &sB_Jpsi_mass);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_Jpsi_chi2", &sB_Jpsi_chi2);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_muon1_px", &muon1_BsKK_px);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_muon1_py", &muon1_BsKK_py);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_muon1_pz", &muon1_BsKK_pz);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_muon2_px", &muon2_BsKK_px);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_muon2_py", &muon2_BsKK_py);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_muon2_pz", &muon2_BsKK_pz);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_Kplus_px", &Kplus_BsKK_px);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_Kplus_py", &Kplus_BsKK_py);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_Kplus_pz", &Kplus_BsKK_pz);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_Kminus_px", &Kminus_BsKK_px);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_Kminus_py", &Kminus_BsKK_py);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_Kminus_pz", &Kminus_BsKK_pz);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_w_trigger", &w_trigger);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_w_SF_mu1", &w_SF_mu1);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_w_SF_mu2", &w_SF_mu2);
    mc_BS_to_JPSI_K_K.SetBranchAddress("sB_B_pt", &Bs_B_pt);
    for(int i = 1; i <= nentries; i++)
    {
        mc_BS_to_JPSI_K_K.GetEntry(i);
        TLorentzVector mu1, mu2, pi1, pi2, K1, K2, p1, p2, jpsi,
                        muon1, muon2, kaon1, kaon2;
        w = 1.;
        mu1.SetXYZM(sB_mu1_px, sB_mu1_py, sB_mu1_pz, 105.65837);
        mu2.SetXYZM(sB_mu2_px, sB_mu2_py, sB_mu2_pz, 105.65837);
        pi1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 139.57);
        pi2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 139.57);
        K1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 493.677);
        K2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 493.677);
        p1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 938.272);
        p2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 938.272);
        jpsi.SetXYZM(sB_mu1_px + sB_mu2_px, sB_mu1_py + sB_mu2_py, sB_mu1_pz + sB_mu2_pz, 3096.);
        TLorentzVector bs = jpsi + pi1 + pi2;
        if ((K1 + pi2).M() > 1550. && (pi1 + K2).M() > 1550. && sB_Lxy_MaxSumPt > 1. && (jpsi + pi1).M() < 4900.  && (jpsi + pi2).M() < 4900. && sB_mu1_pt > 4000. && sB_mu2_pt > 4000. && sB_trk1_pt > 2000. && sB_trk2_pt > 2000.  && sB_Bs_chi2_ndof < 1.7 && sB_Bs_pt > 0.25 * sB_MaxSumPt && 4900. < (jpsi + K1 + pi2).M() && (jpsi + K1 + pi2).M() < 5700. && (jpsi + pi1 + K2).M() < 5900. && 2950. < sB_Jpsi_mass && sB_Jpsi_mass < 3250.) 
        {
            if (sB_trk1_charge * sB_trk2_charge > 0) {w = -1;}
            //i_temp += w;
            
            muon1.SetXYZM(muon1_BsKK_px, muon1_BsKK_py, muon1_BsKK_pz, 105.65837);
            muon2.SetXYZM(muon2_BsKK_px, muon2_BsKK_py, muon2_BsKK_pz, 105.65837);
            kaon1.SetXYZM(Kplus_BsKK_px, Kplus_BsKK_py, Kplus_BsKK_pz, 493.677);
            kaon2.SetXYZM(Kminus_BsKK_px, Kminus_BsKK_py, Kminus_BsKK_pz, 493.677);
            w_dimuon = w_trigger * w_SF_mu1 * w_SF_mu2;
            if (Bs_B_pt * GEV <= 50.)
            {
                w_B_pt = (*vpar_B_pt)(0) + (*vpar_B_pt)(1) * (Bs_B_pt * GEV);
            }
            else
            {
                w_B_pt = (*vpar_B_pt)(0) + (*vpar_B_pt)(1) * 50.;
            }
            w1 = w * w_dimuon;
            w_dimuon = w_dimuon * w_B_pt;
            w = w * w_dimuon;
            w_4tr = w;
            
            if ((jpsi + pi1 + K2).M() > 4650.) {Kpi_piK_2d_BS_to_JPSI_K_K->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, w_4tr);
                Kpi_piK_2d_BS_to_JPSI_K_K_for_ea->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, w_4tr);}
            else {Kpi_piK_2d_BS_to_JPSI_K_K->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, 0.);
                Kpi_piK_2d_BS_to_JPSI_K_K_for_ea->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, 0.);} 
             
            if ((jpsi + p1 + K2).M() * GEV > 5.2 && (jpsi + p1 + K2).M() * GEV < 7.0 && (jpsi + K1 + p2).M() * GEV > 5.15 && (jpsi + K1 + p2).M() * GEV < 7.0) 
            {
                pK_Kp_2d_BS_to_JPSI_K_K->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, w_4tr);
                //pK_Kp_2d_test_BS_to_JPSI_K_K->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, w_4tr);
                pK_Kp_2d_rotate_BS_to_JPSI_K_K->Fill((1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, (-1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, w_4tr);
            }
            else 
            {
                pK_Kp_2d_BS_to_JPSI_K_K->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, 0.);
                //pK_Kp_2d_test_BS_to_JPSI_K_K->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, 0.);
                pK_Kp_2d_rotate_BS_to_JPSI_K_K->Fill((1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, (-1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, 0.);                
            } 
            
            if ((jpsi + p1 + K2).M() * GEV > 5.2 && (jpsi + p1 + K2).M() * GEV < 6.8 && (jpsi + K1 + p2).M() * GEV > 5.15 && (jpsi + K1 + p2).M() * GEV < 6.8)
            {
                pK_Kp_2d_test_BS_to_JPSI_K_K->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, w);
            }
            else 
            {
                pK_Kp_2d_test_BS_to_JPSI_K_K->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, 0.);                
            }
            
            if ((jpsi + p1 + pi2).M() * GEV > ppi_pip_low && (jpsi + p1 + pi2).M() * GEV < ppi_pip_high && (jpsi + pi1 + p2).M() * GEV > ppi_pip_low2 && (jpsi + pi1 + p2).M() * GEV < ppi_pip_high2) 
            {
                ppi_pip_2d_BS_to_JPSI_K_K->Fill((jpsi + p1 + pi2).M() * GEV, (jpsi + p2 + pi1).M() * GEV, w);
            }
            else 
            {
                ppi_pip_2d_BS_to_JPSI_K_K->Fill((jpsi + p1 + pi2).M() * GEV, (jpsi + p2 + pi1).M() * GEV, 0.);                
            } 
 
            if ((jpsi + K1 + K2).M() * GEV > 4.97 && (jpsi + K1 + K2).M() * GEV < 6.0 && (jpsi + pi1 + pi2).M() * GEV > 4.55 && (jpsi + pi1 + pi2).M() * GEV < 5.65) {KK_pipi_2d_BS_to_JPSI_K_K->Fill((jpsi + K1 + K2).M() * GEV, (jpsi + pi1 + pi2).M() * GEV, w_4tr);}
            else {KK_pipi_2d_BS_to_JPSI_K_K->Fill((jpsi + K1 + K2).M() * GEV, (jpsi + pi1 + pi2).M() * GEV, 0.);} 
            
            KK_1d_BS_to_JPSI_K_K->Fill((jpsi + K1 + K2).M() * GEV, w_4tr);
            pipi_1d_BS_to_JPSI_K_K->Fill((jpsi + pi1 + pi2).M() * GEV, w_4tr);
            
            mu1_px = muon1_BsKK_px;
            mu1_py = muon1_BsKK_py;
            mu1_pz = muon1_BsKK_pz;
            mu2_px = muon2_BsKK_px;
            mu2_py = muon2_BsKK_py;
            mu2_pz = muon2_BsKK_pz;
            K1_px = Kplus_BsKK_px;
            K1_py = Kplus_BsKK_py;
            K1_pz = Kplus_BsKK_pz;
            K2_px = Kminus_BsKK_px;
            K2_py = Kminus_BsKK_py;
            K2_pz = Kminus_BsKK_pz;
            jpsipi1 = (jpsi + pi1).M() * GEV;
            jpsipi2 = (jpsi + pi2).M() * GEV;
            jpsiK1 = (jpsi + K1).M() * GEV;
            jpsiK2 = (jpsi + K2).M() * GEV;
            K1pi2 = (K1 + pi2).M() * GEV;
            K2pi1 = (K2 + pi1).M() * GEV;
            K1K2 = (K1 + K2).M() * GEV;
            p1K2 = (p1 + K2).M() * GEV;
            p2K1 = (p2 + K1).M() * GEV;
            jpsip1 = (jpsi + p1).M() * GEV;
            jpsip2 = (jpsi + p2).M() * GEV;
            jpsiKpi = (jpsi + K1 + pi2).M() * GEV;
            jpsipiK = (jpsi + K2 + pi1).M() * GEV;
            jpsiKK = (jpsi + K1 + K2).M() * GEV;
            jpsipipi = (jpsi + pi1 + pi2).M() * GEV;
            jpsipK = (jpsi + p1 + K2).M() * GEV;
            jpsiKp = (jpsi + p2 + K1).M() * GEV;
            signal = 0;
            control = 0;
            controlLb = 0;
            cos_theta_Zc_Kpi = abs(cos(theta_X(jpsi + K1 + pi2, jpsi + pi2, jpsi)));
            cos_theta_Zc_piK = abs(cos(theta_X(jpsi + K2 + pi1, jpsi + pi1, jpsi)));
            phi_K_Kpi = phi_K(jpsi + K1 + pi2, K1 + pi2, K1, jpsi);
            phi_K_piK = phi_K(jpsi + K2 + pi1, K2 + pi1, K2, jpsi);
            alpha_mu_Kpi = alpha_mu(mu2, jpsi, jpsi + pi2, jpsi + K1 + pi2);
            alpha_mu_piK = alpha_mu(mu2, jpsi, jpsi + pi2, jpsi + K1 + pi2);
            phi_mu_Kstar_Kpi = phi_mu_Kstar(jpsi + K1 + pi2, K1 + pi2, jpsi, mu2);
            phi_mu_Kstar_piK = phi_mu_Kstar(jpsi + K2 + pi1, K2 + pi1, jpsi, mu2);
            phi_mu_X_Kpi = phi_mu_X(jpsi + pi2, jpsi + pi2 + K1, jpsi, mu2);
            phi_mu_X_piK = phi_mu_X(jpsi + pi1, jpsi + pi1 + K2, jpsi, mu2);
            cos_theta_psi_X_Kpi = abs(cos(theta_psi_X(jpsi + pi2, jpsi, mu2)));
            cos_theta_psi_X_piK = abs(cos(theta_psi_X(jpsi + pi1, jpsi, mu2)));
            cos_theta_Kstar_Kpi = abs(cos(theta_Kstar(jpsi + K1 + pi2, K1 + pi2, K1)));
            cos_theta_Kstar_piK = abs(cos(theta_Kstar(jpsi + K2 + pi1, K2 + pi1, K2)));
            cos_theta_psi_Kstar_Kpi = abs(cos(theta_psi_Kstar(jpsi + K1 + pi2, jpsi, mu2)));
            cos_theta_psi_Kstar_piK = abs(cos(theta_psi_Kstar(jpsi + K2 + pi1, jpsi, mu2)));
            cos_theta_B0_Kpi = abs(cos(theta_B0(jpsi + K1 + pi2, K1 + pi2)));
            cos_theta_B0_piK = abs(cos(theta_B0(jpsi + K2 + pi1, K2 + pi1)));
            phi_psi_Kpi = phi_psi(jpsi + K1 + pi2, jpsi + pi2, jpsi);
            phi_psi_piK = phi_psi(jpsi + K2 + pi1, jpsi + pi1, jpsi);
            jpsippi = (jpsi + p1 + pi2).M() * GEV;
            jpsipip = (jpsi + pi1 + p2).M() * GEV;
            
            int xsa = area_hist->ProjectionX()->FindBin((jpsi + K1 + pi2).M() * GEV); 
            int ysa = area_hist->ProjectionY()->FindBin((jpsi + K2 + pi1).M() * GEV);
            if (area_hist->GetBinContent(xsa, ysa) == 1)
            {
                Jpsipi_2d_BS_to_JPSI_K_K->Fill((jpsi + pi1).M() * GEV, (jpsi + pi2).M() * GEV, w);
                JpsiK_2d_BS_to_JPSI_K_K->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV, w);
                Kpi_2d_BS_to_JPSI_K_K->Fill((K1 + pi2).M() * GEV, (K2 + pi1).M() * GEV, w);
                cos_theta_Zc_BS_to_JPSI_K_K->Fill(abs(cos(theta_X(jpsi + K1 + pi2, jpsi + pi2, jpsi))), abs(cos(theta_X(jpsi + K2 + pi1, jpsi + pi1, jpsi))), w);
                phi_K_BS_to_JPSI_K_K->Fill(phi_K(jpsi + K1 + pi2, K1 + pi2, K1, jpsi), phi_K(jpsi + K2 + pi1, K2 + pi1, K2, jpsi), w);
                alpha_mu_BS_to_JPSI_K_K->Fill(alpha_mu(mu2, jpsi, jpsi + pi2, jpsi + K1 + pi2), alpha_mu(mu2, jpsi, jpsi + pi1, jpsi + K2 + pi1), w);
                phi_mu_Kstar_BS_to_JPSI_K_K->Fill(phi_mu_Kstar(jpsi + K1 + pi2, K1 + pi2, jpsi, mu2), phi_mu_Kstar(jpsi + K2 + pi1, K2 + pi1, jpsi, mu2), w);
                phi_mu_X_BS_to_JPSI_K_K->Fill(phi_mu_X(jpsi + pi2, jpsi + pi2 + K1, jpsi, mu2), phi_mu_X(jpsi + pi1, jpsi + pi1 + K2, jpsi, mu2), w);
                cos_theta_psi_X_BS_to_JPSI_K_K->Fill(abs(cos(theta_psi_X(jpsi + pi2, jpsi, mu2))), abs(cos(theta_psi_X(jpsi + pi1, jpsi, mu2))), w);
                cos_theta_Kstar_BS_to_JPSI_K_K->Fill(abs(cos(theta_Kstar(jpsi + K1 + pi2, K1 + pi2, K1))), abs(cos(theta_Kstar(jpsi + K2 + pi1, K2 + pi1, K2))), w);
                cos_theta_psi_Kstar_BS_to_JPSI_K_K->Fill(abs(cos(theta_psi_Kstar(jpsi + K1 + pi2, jpsi, mu2))), abs(cos(theta_psi_Kstar(jpsi + K2 + pi1, jpsi, mu2))), w);
                cos_theta_B0_BS_to_JPSI_K_K->Fill(abs(cos(theta_B0(jpsi + K1 + pi2, K1 + pi2))), abs(cos(theta_B0(jpsi + K2 + pi1, K2 + pi1))), w);
                phi_psi_BS_to_JPSI_K_K->Fill(phi_psi(jpsi + K1 + pi2, jpsi + pi2, jpsi), phi_psi(jpsi + K2 + pi1, jpsi + pi1, jpsi), w);
                
                B_pt_BS_to_JPSI_K_K->Fill(sB_Bs_pt * GEV, w1);
                B_pt_BS_to_JPSI_K_K_reweighted->Fill(sB_Bs_pt * GEV, w);
                
                mu1_BsKK_px = muon1_BsKK_px;
                mu1_BsKK_py = muon1_BsKK_py;
                mu1_BsKK_pz = muon1_BsKK_pz;
                mu2_BsKK_px = muon2_BsKK_px;
                mu2_BsKK_py = muon2_BsKK_py;
                mu2_BsKK_pz = muon2_BsKK_pz;
                K1_BsKK_px = Kplus_BsKK_px;
                K1_BsKK_py = Kplus_BsKK_py;
                K1_BsKK_pz = Kplus_BsKK_pz;
                K2_BsKK_px = Kminus_BsKK_px;
                K2_BsKK_py = Kminus_BsKK_py;
                K2_BsKK_pz = Kminus_BsKK_pz;
                jpsipi1_BsKK = (jpsi + pi1).M() * GEV;
                jpsipi2_BsKK = (jpsi + pi2).M() * GEV;
                jpsiK1_BsKK = (jpsi + K1).M() * GEV;
                jpsiK2_BsKK = (jpsi + K2).M() * GEV;
                K1pi2_BsKK = (K1 + pi2).M() * GEV;
                K2pi1_BsKK = (K2 + pi1).M() * GEV;
                for_me_BS_to_JPSI_K_K->Fill();
                
                signal = 1;
            }     
            
            xsa = area_hist_Lb->ProjectionX()->FindBin((jpsi + p1 + K2).M() * GEV); 
            ysa = area_hist_Lb->ProjectionY()->FindBin((jpsi + p2 + K1).M() * GEV);
            
            if (area_hist_Lb->GetBinContent(xsa, ysa) == 1)
            {
                Jpsip_2d_controlLb_BS_to_JPSI_K_K->Fill((jpsi + p1).M() * GEV, (jpsi + p2).M() * GEV, w);
                JpsiK_2d_controlLb_BS_to_JPSI_K_K->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV, w);
                pK_2d_controlLb_BS_to_JPSI_K_K->Fill((p1 + K2).M() * GEV, (p2 + K1).M() * GEV, w);
                controlLb = 1;
            }
            
            if (5336. < (jpsi + K1 + K2).M() && (jpsi + K1 + K2).M() < 5396.)
            {
                KK_1d_control_BS_to_JPSI_K_K->Fill((K1 + K2).M() * GEV, w);
                JpsiK1_1d_control_BS_to_JPSI_K_K->Fill((jpsi + K1).M() * GEV, w);
                JpsiK2_1d_control_BS_to_JPSI_K_K->Fill((jpsi + K2).M() * GEV, w);
                JpsiK_2d_control_BS_to_JPSI_K_K->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV, w);
                mu1_BsKK_px = muon1_BsKK_px;
                mu1_BsKK_py = muon1_BsKK_py;
                mu1_BsKK_pz = muon1_BsKK_pz;
                mu2_BsKK_px = muon2_BsKK_px;
                mu2_BsKK_py = muon2_BsKK_py;
                mu2_BsKK_pz = muon2_BsKK_pz;
                K1_BsKK_px = Kplus_BsKK_px;
                K1_BsKK_py = Kplus_BsKK_py;
                K1_BsKK_pz = Kplus_BsKK_pz;
                K2_BsKK_px = Kminus_BsKK_px;
                K2_BsKK_py = Kminus_BsKK_py;
                K2_BsKK_pz = Kminus_BsKK_pz;
                jpsiK1_BsKK = (jpsi + K1).M() * GEV;
                jpsiK2_BsKK = (jpsi + K2).M() * GEV;
                K1K2_BsKK = (K1 + K2).M() * GEV;
                for_BsJpsiKK_control_BS_to_JPSI_K_K->Fill();
                n_BS_to_JPSI_K_K_counter += w;
                
                control = 1;
            }
            phs_BS_to_JPSI_K_K->Fill();
        }
    }
    
    TChain mc_LAMBDA0B_to_JPSI_P_K("BsAllCandidates");
    mc_LAMBDA0B_to_JPSI_P_K.AddFile("datasets/mc_LAMBDA0B_to_JPSI_P_K_Bhad.root");
    mc_LAMBDA0B_to_JPSI_P_K.AddFile("datasets/mc_LAMBDA0B_to_JPSI_P_K_antiBhad.root");
    nentries = mc_LAMBDA0B_to_JPSI_P_K.GetEntries();
    cout << "Number of runs LAMBDA0B_to_JPSI_P_K\t" << nentries << endl;
    
    float muon1_Lb_px, muon1_Lb_py, muon1_Lb_pz, muon2_Lb_px, muon2_Lb_py, muon2_Lb_pz, proton_Lb_px, proton_Lb_py, proton_Lb_pz, Kminus_Lb_px, Kminus_Lb_py, Kminus_Lb_pz;
    
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_mu1_px", &sB_mu1_px);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_mu1_py", &sB_mu1_py);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_mu1_pz", &sB_mu1_pz);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_mu2_px", &sB_mu2_px);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_mu2_py", &sB_mu2_py);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_mu2_pz", &sB_mu2_pz);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_trk1_px", &sB_trk1_px);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_trk1_py", &sB_trk1_py);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_trk1_pz", &sB_trk1_pz);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_trk2_px", &sB_trk2_px);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_trk2_py", &sB_trk2_py);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_trk2_pz", &sB_trk2_pz);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_trk1_charge", &sB_trk1_charge);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_trk2_charge", &sB_trk2_charge);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_Lxy_MaxSumPt", &sB_Lxy_MaxSumPt);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_Bs_pt", &sB_Bs_pt);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_mu1_pt", &sB_mu1_pt);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_mu2_pt", &sB_mu2_pt);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_trk1_pt", &sB_trk1_pt);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_trk2_pt", &sB_trk2_pt);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_Bs_chi2_ndof", &sB_Bs_chi2_ndof);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_MaxSumPt", &sB_MaxSumPt);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_mu1_eta", &sB_mu1_eta);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_mu2_eta", &sB_mu2_eta);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_trk1_eta", &sB_trk1_eta);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_trk2_eta", &sB_trk2_eta);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_Jpsi_mass", &sB_Jpsi_mass);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_Jpsi_chi2", &sB_Jpsi_chi2);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_muon1_px", &muon1_Lb_px);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_muon1_py", &muon1_Lb_py);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_muon1_pz", &muon1_Lb_pz);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_muon2_px", &muon2_Lb_px);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_muon2_py", &muon2_Lb_py);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_muon2_pz", &muon2_Lb_pz);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_proton_px", &proton_Lb_px);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_proton_py", &proton_Lb_py);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_proton_pz", &proton_Lb_pz);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_Kminus_px", &Kminus_Lb_px);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_Kminus_py", &Kminus_Lb_py);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_Kminus_pz", &Kminus_Lb_pz);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_w_trigger", &w_trigger);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_w_SF_mu1", &w_SF_mu1);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_w_SF_mu2", &w_SF_mu2);
    mc_LAMBDA0B_to_JPSI_P_K.SetBranchAddress("sB_B_pt", &Bs_B_pt);
    for(int i = 1; i <= nentries; i++)
    {
        mc_LAMBDA0B_to_JPSI_P_K.GetEntry(i);
        
        TLorentzVector mu1, mu2, pi1, pi2, K1, K2, p1, p2, jpsi;
        w = 1.;
        mu1.SetXYZM(sB_mu1_px, sB_mu1_py, sB_mu1_pz, 105.65837);
        mu2.SetXYZM(sB_mu2_px, sB_mu2_py, sB_mu2_pz, 105.65837);
        pi1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 139.57);
        pi2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 139.57);
        K1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 493.677);
        K2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 493.677);
        p1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 938.272);
        p2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 938.272);
        jpsi.SetXYZM(sB_mu1_px + sB_mu2_px, sB_mu1_py + sB_mu2_py, sB_mu1_pz + sB_mu2_pz, 3096.);
        TLorentzVector bs = jpsi + pi1 + pi2;
        if ((K1 + pi2).M() > 1550. && (pi1 + K2).M() > 1550. && sB_Lxy_MaxSumPt > 1. && (jpsi + pi1).M() < 4900.  && (jpsi + pi2).M() < 4900. && sB_mu1_pt > 4000. && sB_mu2_pt > 4000. && sB_trk1_pt > 2000. && sB_trk2_pt > 2000.  && sB_Bs_chi2_ndof < 1.7 && sB_Bs_pt > 0.25 * sB_MaxSumPt && 4900. < (jpsi + K1 + pi2).M() && (jpsi + K1 + pi2).M() < 5700. && (jpsi + pi1 + K2).M() < 5900. && 2950. < sB_Jpsi_mass && sB_Jpsi_mass < 3250.) 
        {
            if (sB_trk1_charge * sB_trk2_charge > 0) {w = -1;}
            //i_temp += w;
            w_dimuon = w_trigger * w_SF_mu1 * w_SF_mu2;
            if (Bs_B_pt * GEV <= 50.)
            {
                w_B_pt = (*vpar_B_pt)(0) + (*vpar_B_pt)(1) * (Bs_B_pt * GEV);
            }
            else
            {
                w_B_pt = (*vpar_B_pt)(0) + (*vpar_B_pt)(1) * 50.;
            }
            w1 = w * w_dimuon;
            w_dimuon = w_dimuon * w_B_pt;
            w = w * w_dimuon;
            w_4tr = w;
            
            if ((jpsi + pi1 + K2).M() > 4650.) {Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, w_4tr);
                Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K_for_ea->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, w_4tr);}
            else {Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, 0.);
                Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K_for_ea->Fill((jpsi + K1 + pi2).M() * GEV, (jpsi + K2 + pi1).M() * GEV, 0.);} 
             
            if ((jpsi + p1 + K2).M() * GEV > 5.2 && (jpsi + p1 + K2).M() * GEV < 7.0 && (jpsi + K1 + p2).M() * GEV > 5.15 && (jpsi + K1 + p2).M() * GEV < 7.0) 
            {
                pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, w_4tr);
                //pK_Kp_2d_test_LAMBDA0B_to_JPSI_P_K->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, w_4tr);
                pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_K->Fill((1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, (-1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, w_4tr);
            }
            else 
            {
                pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, 0.);
                //pK_Kp_2d_test_LAMBDA0B_to_JPSI_P_K->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, 0.);
                pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_K->Fill((1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, (-1. / sqrt(2.)) * (jpsi + p1 + K2).M() * GEV + (1. / sqrt(2.)) * (jpsi + p2 + K1).M() * GEV, 0.);
            }
            
            if ((jpsi + p1 + K2).M() * GEV > 5.2 && (jpsi + p1 + K2).M() * GEV < 6.8 && (jpsi + K1 + p2).M() * GEV > 5.15 && (jpsi + K1 + p2).M() * GEV < 6.8)
            {
                pK_Kp_2d_test_LAMBDA0B_to_JPSI_P_K->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, w);
            }
            else 
            {
                pK_Kp_2d_test_LAMBDA0B_to_JPSI_P_K->Fill((jpsi + p1 + K2).M() * GEV, (jpsi + p2 + K1).M() * GEV, 0.);                
            }
            
            if ((jpsi + p1 + pi2).M() * GEV > ppi_pip_low && (jpsi + p1 + pi2).M() * GEV < ppi_pip_high && (jpsi + pi1 + p2).M() * GEV > ppi_pip_low2 && (jpsi + pi1 + p2).M() * GEV < ppi_pip_high2) 
            {
                ppi_pip_2d_LAMBDA0B_to_JPSI_P_K->Fill((jpsi + p1 + pi2).M() * GEV, (jpsi + p2 + pi1).M() * GEV, w);
            }
            else 
            {
                ppi_pip_2d_LAMBDA0B_to_JPSI_P_K->Fill((jpsi + p1 + pi2).M() * GEV, (jpsi + p2 + pi1).M() * GEV, 0.);                
            } 
 
            if ((jpsi + K1 + K2).M() * GEV > 4.97 && (jpsi + K1 + K2).M() * GEV < 6.0 && (jpsi + pi1 + pi2).M() * GEV > 4.55 && (jpsi + pi1 + pi2).M() * GEV < 5.65) {KK_pipi_2d_LAMBDA0B_to_JPSI_P_K->Fill((jpsi + K1 + K2).M() * GEV, (jpsi + pi1 + pi2).M() * GEV, w_4tr);}
            else {KK_pipi_2d_LAMBDA0B_to_JPSI_P_K->Fill((jpsi + K1 + K2).M() * GEV, (jpsi + pi1 + pi2).M() * GEV, 0.);}
            
            KK_1d_LAMBDA0B_to_JPSI_P_K->Fill((jpsi + K1 + K2).M() * GEV, w_4tr);
            pipi_1d_LAMBDA0B_to_JPSI_P_K->Fill((jpsi + pi1 + pi2).M() * GEV, w_4tr);
            
            mu1_px = muon1_Lb_px;
            mu1_py = muon1_Lb_py;
            mu1_pz = muon1_Lb_pz;
            mu2_px = muon2_Lb_px;
            mu2_py = muon2_Lb_py;
            mu2_pz = muon2_Lb_pz;
            p_px = proton_Lb_px;
            p_py = proton_Lb_py;
            p_pz = proton_Lb_pz;
            K_px = Kminus_Lb_px;
            K_py = Kminus_Lb_py;
            K_pz = Kminus_Lb_pz;
            jpsipi1 = (jpsi + pi1).M() * GEV;
            jpsipi2 = (jpsi + pi2).M() * GEV;
            jpsiK1 = (jpsi + K1).M() * GEV;
            jpsiK2 = (jpsi + K2).M() * GEV;
            K1pi2 = (K1 + pi2).M() * GEV;
            K2pi1 = (K2 + pi1).M() * GEV;
            K1K2 = (K1 + K2).M() * GEV;
            p1K2 = (p1 + K2).M() * GEV;
            p2K1 = (p2 + K1).M() * GEV;
            jpsip1 = (jpsi + p1).M() * GEV;
            jpsip2 = (jpsi + p2).M() * GEV;
            jpsiKpi = (jpsi + K1 + pi2).M() * GEV;
            jpsipiK = (jpsi + K2 + pi1).M() * GEV;
            jpsiKK = (jpsi + K1 + K2).M() * GEV;
            jpsipipi = (jpsi + pi1 + pi2).M() * GEV;
            jpsipK = (jpsi + p1 + K2).M() * GEV;
            jpsiKp = (jpsi + p2 + K1).M() * GEV;
            signal = 0;
            control = 0;
            controlLb = 0;
            cos_theta_Zc_Kpi = abs(cos(theta_X(jpsi + K1 + pi2, jpsi + pi2, jpsi)));
            cos_theta_Zc_piK = abs(cos(theta_X(jpsi + K2 + pi1, jpsi + pi1, jpsi)));
            phi_K_Kpi = phi_K(jpsi + K1 + pi2, K1 + pi2, K1, jpsi);
            phi_K_piK = phi_K(jpsi + K2 + pi1, K2 + pi1, K2, jpsi);
            alpha_mu_Kpi = alpha_mu(mu2, jpsi, jpsi + pi2, jpsi + K1 + pi2);
            alpha_mu_piK = alpha_mu(mu2, jpsi, jpsi + pi2, jpsi + K1 + pi2);
            phi_mu_Kstar_Kpi = phi_mu_Kstar(jpsi + K1 + pi2, K1 + pi2, jpsi, mu2);
            phi_mu_Kstar_piK = phi_mu_Kstar(jpsi + K2 + pi1, K2 + pi1, jpsi, mu2);
            phi_mu_X_Kpi = phi_mu_X(jpsi + pi2, jpsi + pi2 + K1, jpsi, mu2);
            phi_mu_X_piK = phi_mu_X(jpsi + pi1, jpsi + pi1 + K2, jpsi, mu2);
            cos_theta_psi_X_Kpi = abs(cos(theta_psi_X(jpsi + pi2, jpsi, mu2)));
            cos_theta_psi_X_piK = abs(cos(theta_psi_X(jpsi + pi1, jpsi, mu2)));
            cos_theta_Kstar_Kpi = abs(cos(theta_Kstar(jpsi + K1 + pi2, K1 + pi2, K1)));
            cos_theta_Kstar_piK = abs(cos(theta_Kstar(jpsi + K2 + pi1, K2 + pi1, K2)));
            cos_theta_psi_Kstar_Kpi = abs(cos(theta_psi_Kstar(jpsi + K1 + pi2, jpsi, mu2)));
            cos_theta_psi_Kstar_piK = abs(cos(theta_psi_Kstar(jpsi + K2 + pi1, jpsi, mu2)));
            cos_theta_B0_Kpi = abs(cos(theta_B0(jpsi + K1 + pi2, K1 + pi2)));
            cos_theta_B0_piK = abs(cos(theta_B0(jpsi + K2 + pi1, K2 + pi1)));
            phi_psi_Kpi = phi_psi(jpsi + K1 + pi2, jpsi + pi2, jpsi);
            phi_psi_piK = phi_psi(jpsi + K2 + pi1, jpsi + pi1, jpsi);
            jpsippi = (jpsi + p1 + pi2).M() * GEV;
            jpsipip = (jpsi + pi1 + p2).M() * GEV;

            int xsa = area_hist->ProjectionX()->FindBin((jpsi + K1 + pi2).M() * GEV); 
            int ysa = area_hist->ProjectionY()->FindBin((jpsi + K2 + pi1).M() * GEV);
            if (area_hist->GetBinContent(xsa, ysa) == 1)
            {
                Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->Fill((jpsi + pi1).M() * GEV, (jpsi + pi2).M() * GEV, w);
                JpsiK_2d_LAMBDA0B_to_JPSI_P_K->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV, w);
                Kpi_2d_LAMBDA0B_to_JPSI_P_K->Fill((K1 + pi2).M() * GEV, (K2 + pi1).M() * GEV, w);
                cos_theta_Zc_LAMBDA0B_to_JPSI_P_K->Fill(abs(cos(theta_X(jpsi + K1 + pi2, jpsi + pi2, jpsi))), abs(cos(theta_X(jpsi + K2 + pi1, jpsi + pi1, jpsi))), w);
                phi_K_LAMBDA0B_to_JPSI_P_K->Fill(phi_K(jpsi + K1 + pi2, K1 + pi2, K1, jpsi), phi_K(jpsi + K2 + pi1, K2 + pi1, K2, jpsi), w);
                alpha_mu_LAMBDA0B_to_JPSI_P_K->Fill(alpha_mu(mu2, jpsi, jpsi + pi2, jpsi + K1 + pi2), alpha_mu(mu2, jpsi, jpsi + pi1, jpsi + K2 + pi1), w);
                phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K->Fill(phi_mu_Kstar(jpsi + K1 + pi2, K1 + pi2, jpsi, mu2), phi_mu_Kstar(jpsi + K2 + pi1, K2 + pi1, jpsi, mu2), w);
                phi_mu_X_LAMBDA0B_to_JPSI_P_K->Fill(phi_mu_X(jpsi + pi2, jpsi + pi2 + K1, jpsi, mu2), phi_mu_X(jpsi + pi1, jpsi + pi1 + K2, jpsi, mu2), w);
                cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K->Fill(abs(cos(theta_psi_X(jpsi + pi2, jpsi, mu2))), abs(cos(theta_psi_X(jpsi + pi1, jpsi, mu2))), w);
                cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->Fill(abs(cos(theta_Kstar(jpsi + K1 + pi2, K1 + pi2, K1))), abs(cos(theta_Kstar(jpsi + K2 + pi1, K2 + pi1, K2))), w);
                cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K->Fill(abs(cos(theta_psi_Kstar(jpsi + K1 + pi2, jpsi, mu2))), abs(cos(theta_psi_Kstar(jpsi + K2 + pi1, jpsi, mu2))), w);
                cos_theta_B0_LAMBDA0B_to_JPSI_P_K->Fill(abs(cos(theta_B0(jpsi + K1 + pi2, K1 + pi2))), abs(cos(theta_B0(jpsi + K2 + pi1, K2 + pi1))), w);
                phi_psi_LAMBDA0B_to_JPSI_P_K->Fill(phi_psi(jpsi + K1 + pi2, jpsi + pi2, jpsi), phi_psi(jpsi + K2 + pi1, jpsi + pi1, jpsi), w);
                
                B_pt_LAMBDA0B_to_JPSI_P_K->Fill(sB_Bs_pt * GEV, w1);
                B_pt_LAMBDA0B_to_JPSI_P_K_reweighted->Fill(sB_Bs_pt * GEV, w);
                
                signal = 1;
            }   
            
            xsa = area_hist_Lb->ProjectionX()->FindBin((jpsi + p1 + K2).M() * GEV); 
            ysa = area_hist_Lb->ProjectionY()->FindBin((jpsi + p2 + K1).M() * GEV);
            
            if (area_hist_Lb->GetBinContent(xsa, ysa) == 1)
            {
                Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Fill((jpsi + p1).M() * GEV, (jpsi + p2).M() * GEV, w);
                JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV, w);
                pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Fill((p1 + K2).M() * GEV, (p2 + K1).M() * GEV, w);  
                controlLb = 1;
            }
            
            if (5336. < (jpsi + K1 + K2).M() && (jpsi + K1 + K2).M() < 5396.)
            {
                KK_1d_control_LAMBDA0B_to_JPSI_P_K->Fill((K1 + K2).M() * GEV, w);
                JpsiK1_1d_control_LAMBDA0B_to_JPSI_P_K->Fill((jpsi + K1).M() * GEV, w);
                JpsiK2_1d_control_LAMBDA0B_to_JPSI_P_K->Fill((jpsi + K2).M() * GEV, w);
                JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K->Fill((jpsi + K1).M() * GEV, (jpsi + K2).M() * GEV, w);
                n_LAMBDA0B_to_JPSI_P_K_counter += w;
                control = 1;
            }
            phs_LAMBDA0B_to_JPSI_P_K->Fill();
        }
    }
    
    n_BD_to_JPSI_K_PI_counter = n_BD_to_JPSI_K_PI_counter / KK_1d_BD_to_JPSI_K_PI->Integral();
    n_BD_to_JPSI_PI_PI_counter = n_BD_to_JPSI_PI_PI_counter / KK_1d_BD_to_JPSI_PI_PI->Integral();
    n_BS_to_JPSI_PI_PI_counter = n_BS_to_JPSI_PI_PI_counter / KK_1d_BS_to_JPSI_PI_PI->Integral();
    n_BS_to_JPSI_K_K_counter = n_BS_to_JPSI_K_K_counter / KK_1d_BS_to_JPSI_K_K->Integral();
    n_LAMBDA0B_to_JPSI_P_K_counter = n_LAMBDA0B_to_JPSI_P_K_counter / KK_1d_LAMBDA0B_to_JPSI_P_K->Integral();
    n_LAMBDA0B_to_JPSI_P_PI_counter = n_LAMBDA0B_to_JPSI_P_PI_counter / KK_1d_LAMBDA0B_to_JPSI_P_PI->Integral();
    n_BD_to_JPSI_K_K_counter = n_BD_to_JPSI_K_K_counter / KK_1d_BD_to_JPSI_K_K->Integral();
    n_BS_to_JPSI_K_PI_counter = n_BS_to_JPSI_K_PI_counter / KK_1d_BS_to_JPSI_K_PI->Integral();
    
    Float_t n2_new_c = 0., n3_new_c = 0., n5_new_c = 0., n6_new_c = 0., n7_new_c = 0., n8_new_c = 0.;
    
    for(int i = 1; i <= Kpi_piK_2d_BD_to_JPSI_K_PI->ProjectionX()->GetNbinsX(); i++)
    {
        for(int j = 1; j <= Kpi_piK_2d_BD_to_JPSI_K_PI->ProjectionY()->GetNbinsX(); j++)
        {
            if (area_hist->GetBinContent(i, j) > 0)
            {
                n2_new_c += Kpi_piK_2d_BD_to_JPSI_PI_PI->GetBinContent(i, j);
                n3_new_c += Kpi_piK_2d_BS_to_JPSI_PI_PI->GetBinContent(i, j);
                n5_new_c += Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, j);
                n6_new_c += Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, j);
                n7_new_c += Kpi_piK_2d_BD_to_JPSI_K_K->GetBinContent(i, j);
                n8_new_c += Kpi_piK_2d_BS_to_JPSI_K_PI->GetBinContent(i, j);
            }
        }
    }
    
    Float_t n2_new_lb = 0., n3_new_lb = 0., n5_new_lb = 0., n6_new_lb = 0., n7_new_lb = 0., n8_new_lb = 0.;
    
    for(int i = 1; i <= pK_Kp_2d_BD_to_JPSI_K_PI->ProjectionX()->GetNbinsX(); i++)
    {
        for(int j = 1; j <= pK_Kp_2d_BD_to_JPSI_K_PI->ProjectionY()->GetNbinsX(); j++)
        {
            if (area_hist_Lb->GetBinContent(i, j) > 0)
            {
                n2_new_lb += pK_Kp_2d_BD_to_JPSI_PI_PI->GetBinContent(i, j);
                n3_new_lb += pK_Kp_2d_BS_to_JPSI_PI_PI->GetBinContent(i, j);
                n5_new_lb += pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, j);
                n6_new_lb += pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, j);
                n7_new_lb += pK_Kp_2d_BD_to_JPSI_K_K->GetBinContent(i, j);
                n8_new_lb += pK_Kp_2d_BS_to_JPSI_K_PI->GetBinContent(i, j);
            }
        }
    }
    
    n2_new_c = n2_new_c / Kpi_piK_2d_BD_to_JPSI_PI_PI->Integral();
    n3_new_c = n3_new_c / Kpi_piK_2d_BS_to_JPSI_PI_PI->Integral();
    n5_new_c = n5_new_c / Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Integral();
    n6_new_c = n6_new_c / Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->Integral();
    n7_new_c = n7_new_c / Kpi_piK_2d_BD_to_JPSI_K_K->Integral();
    n8_new_c = n8_new_c / Kpi_piK_2d_BS_to_JPSI_K_PI->Integral();
    
    n2_new_lb = n2_new_lb / pK_Kp_2d_BD_to_JPSI_PI_PI->Integral();
    n3_new_lb = n3_new_lb / pK_Kp_2d_BS_to_JPSI_PI_PI->Integral();
    n5_new_lb = n5_new_lb / pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->Integral();
    n6_new_lb = n6_new_lb / pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI->Integral();
    n7_new_lb = n7_new_lb / pK_Kp_2d_BD_to_JPSI_K_K->Integral();
    n8_new_lb = n8_new_lb / pK_Kp_2d_BS_to_JPSI_K_PI->Integral();
    
    TVectorD parameters(18);
    parameters[0] = n5_new_c;
    parameters[1] = n2_new_c;
    parameters[2] = n3_new_c;
    parameters[3] = n6_new_c;
    parameters[4] = n7_new_c;
    parameters[5] = n8_new_c;
    parameters[6] = n_LAMBDA0B_to_JPSI_P_K_counter;
    parameters[7] = n_BD_to_JPSI_PI_PI_counter;
    parameters[8] = n_BS_to_JPSI_PI_PI_counter;
    parameters[9] = n_LAMBDA0B_to_JPSI_P_PI_counter;
    parameters[10] = n_BD_to_JPSI_K_K_counter;
    parameters[11] = n_BS_to_JPSI_K_PI_counter;
    parameters[12] = n5_new_lb;
    parameters[13] = n2_new_lb;
    parameters[14] = n3_new_lb;
    parameters[15] = n6_new_lb;
    parameters[16] = n7_new_lb;
    parameters[17] = n8_new_lb;
    
    cout << "\n\nCoefficients for global fit:" << endl;
    cout << "n5_new_c\t" << n5_new_c << endl; 
    cout << "n2_new_c\t" << n2_new_c << endl;
    cout << "n3_new_c\t" << n3_new_c << endl;
    cout << "n6_new_c\t" << n6_new_c << endl; 
    cout << "n7_new_c\t" << n7_new_c << endl;
    cout << "n8_new_c\t" << n8_new_c << endl;
    cout << "n_LAMBDA0B_to_JPSI_P_K_counter\t" << n_LAMBDA0B_to_JPSI_P_K_counter << endl;
    cout << "n_BD_to_JPSI_PI_PI_counter\t" << n_BD_to_JPSI_PI_PI_counter << endl;
    cout << "n_BS_to_JPSI_PI_PI_counter\t" << n_BS_to_JPSI_PI_PI_counter << "\n" << endl;
    cout << "n_LAMBDA0B_to_JPSI_P_PI_counter\t" << n_LAMBDA0B_to_JPSI_P_PI_counter << endl;
    cout << "n_BD_to_JPSI_K_K_counter\t" << n_BD_to_JPSI_K_K_counter << endl;
    cout << "n_BS_to_JPSI_K_PI_counter\t" << n_BS_to_JPSI_K_PI_counter << "\n" << endl;
    cout << "n5_new_lb\t" << n5_new_lb << endl; 
    cout << "n2_new_lb\t" << n2_new_lb << endl;
    cout << "n3_new_lb\t" << n3_new_lb << endl;
    cout << "n6_new_lb\t" << n6_new_lb << endl; 
    cout << "n7_new_lb\t" << n7_new_lb << endl;
    cout << "n8_new_lb\t" << n8_new_lb << endl;
    
    Kpi_piK_2d_BD_to_JPSI_K_PI->Scale(1. / Kpi_piK_2d_BD_to_JPSI_K_PI->Integral());
    Kpi_piK_2d_BD_to_JPSI_PI_PI->Scale(1. / Kpi_piK_2d_BD_to_JPSI_PI_PI->Integral());
    Kpi_piK_2d_BS_to_JPSI_PI_PI->Scale(1. / Kpi_piK_2d_BS_to_JPSI_PI_PI->Integral());
    Kpi_piK_2d_BS_to_JPSI_K_K->Scale(1. / Kpi_piK_2d_BS_to_JPSI_K_K->Integral());
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Scale(1. / Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Integral());
    Kpi_piK_2d_BS_to_JPSI_K_PI->Scale(1. / Kpi_piK_2d_BS_to_JPSI_K_PI->Integral());
    Kpi_piK_2d_BD_to_JPSI_K_K->Scale(1. / Kpi_piK_2d_BD_to_JPSI_K_K->Integral());
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->Scale(1. / Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->Integral());

    pK_Kp_2d_BD_to_JPSI_K_PI->Scale(1. /  pK_Kp_2d_BD_to_JPSI_K_PI->Integral());
    pK_Kp_2d_BD_to_JPSI_PI_PI->Scale(1. / pK_Kp_2d_BD_to_JPSI_PI_PI->Integral());
    pK_Kp_2d_BS_to_JPSI_PI_PI->Scale(1. / pK_Kp_2d_BS_to_JPSI_PI_PI->Integral());
    pK_Kp_2d_BS_to_JPSI_K_K->Scale(1. / pK_Kp_2d_BS_to_JPSI_K_K->Integral());
    pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->Scale(1. / pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->Integral());
    pK_Kp_2d_BS_to_JPSI_K_PI->Scale(1. /  pK_Kp_2d_BS_to_JPSI_K_PI->Integral());
    pK_Kp_2d_BD_to_JPSI_K_K->Scale(1. / pK_Kp_2d_BD_to_JPSI_K_K->Integral());
    pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI->Scale(1. / pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI->Integral());
    
    pK_Kp_2d_test_BD_to_JPSI_K_PI->Scale(1. /  pK_Kp_2d_test_BD_to_JPSI_K_PI->Integral());
    pK_Kp_2d_test_BD_to_JPSI_PI_PI->Scale(1. / pK_Kp_2d_test_BD_to_JPSI_PI_PI->Integral());
    pK_Kp_2d_test_BS_to_JPSI_PI_PI->Scale(1. / pK_Kp_2d_test_BS_to_JPSI_PI_PI->Integral());
    pK_Kp_2d_test_BS_to_JPSI_K_K->Scale(1. / pK_Kp_2d_test_BS_to_JPSI_K_K->Integral());
    pK_Kp_2d_test_LAMBDA0B_to_JPSI_P_K->Scale(1. / pK_Kp_2d_test_LAMBDA0B_to_JPSI_P_K->Integral());
    //pK_Kp_2d_test_BS_to_JPSI_K_PI->Scale(1. /  pK_Kp_2d_test_BS_to_JPSI_K_PI->Integral());
    //pK_Kp_2d_test_BD_to_JPSI_K_K->Scale(1. / pK_Kp_2d_test_BD_to_JPSI_K_K->Integral());
    //pK_Kp_2d_test_LAMBDA0B_to_JPSI_P_PI->Scale(1. / pK_Kp_2d_test_LAMBDA0B_to_JPSI_P_PI->Integral());
    
    pK_Kp_2d_rotate_BD_to_JPSI_K_PI->Scale(1. /  pK_Kp_2d_rotate_BD_to_JPSI_K_PI->Integral());
    pK_Kp_2d_rotate_BD_to_JPSI_PI_PI->Scale(1. / pK_Kp_2d_rotate_BD_to_JPSI_PI_PI->Integral());
    pK_Kp_2d_rotate_BS_to_JPSI_PI_PI->Scale(1. / pK_Kp_2d_rotate_BS_to_JPSI_PI_PI->Integral());
    pK_Kp_2d_rotate_BS_to_JPSI_K_K->Scale(1. / pK_Kp_2d_rotate_BS_to_JPSI_K_K->Integral());
    pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_K->Scale(1. / pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_K->Integral());
    pK_Kp_2d_rotate_BS_to_JPSI_K_PI->Scale(1. /  pK_Kp_2d_rotate_BS_to_JPSI_K_PI->Integral());
    pK_Kp_2d_rotate_BD_to_JPSI_K_K->Scale(1. / pK_Kp_2d_rotate_BD_to_JPSI_K_K->Integral());
    pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_PI->Scale(1. / pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_PI->Integral());
    
    Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea->Scale(1. / Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea->Integral());
    Kpi_piK_2d_BD_to_JPSI_PI_PI_for_ea->Scale(1. / Kpi_piK_2d_BD_to_JPSI_PI_PI_for_ea->Integral());
    Kpi_piK_2d_BS_to_JPSI_PI_PI_for_ea->Scale(1. / Kpi_piK_2d_BS_to_JPSI_PI_PI_for_ea->Integral());
    Kpi_piK_2d_BS_to_JPSI_K_K_for_ea->Scale(1. / Kpi_piK_2d_BS_to_JPSI_K_K_for_ea->Integral());
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K_for_ea->Scale(1. / Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K_for_ea->Integral());
    Kpi_piK_2d_BS_to_JPSI_K_PI_for_ea->Scale(1. / Kpi_piK_2d_BS_to_JPSI_K_PI_for_ea->Integral());
    Kpi_piK_2d_BD_to_JPSI_K_K_for_ea->Scale(1. / Kpi_piK_2d_BD_to_JPSI_K_K_for_ea->Integral());
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI_for_ea->Scale(1. / Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI_for_ea->Integral());
    
    Jpsipi_2d_BD_to_JPSI_K_PI->Scale(1. / Jpsipi_2d_BD_to_JPSI_K_PI->Integral());
    Jpsipi_2d_BD_to_JPSI_PI_PI->Scale(1. / Jpsipi_2d_BD_to_JPSI_PI_PI->Integral());
    Jpsipi_2d_BS_to_JPSI_PI_PI->Scale(1. / Jpsipi_2d_BS_to_JPSI_PI_PI->Integral());
    Jpsipi_2d_BS_to_JPSI_K_K->Scale(1. / Jpsipi_2d_BS_to_JPSI_K_K->Integral());
    Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->Scale(1. / Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->Integral());
    Jpsipi_2d_BS_to_JPSI_K_PI->Scale(1. / Jpsipi_2d_BS_to_JPSI_K_PI->Integral());
    Jpsipi_2d_BD_to_JPSI_K_K->Scale(1. / Jpsipi_2d_BD_to_JPSI_K_K->Integral());
    Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->Scale(1. / Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->Integral());
    
    JpsiK_2d_BD_to_JPSI_K_PI->Scale(1. / JpsiK_2d_BD_to_JPSI_K_PI->Integral());
    JpsiK_2d_BD_to_JPSI_PI_PI->Scale(1. / JpsiK_2d_BD_to_JPSI_PI_PI->Integral());
    JpsiK_2d_BS_to_JPSI_PI_PI->Scale(1. / JpsiK_2d_BS_to_JPSI_PI_PI->Integral());
    JpsiK_2d_BS_to_JPSI_K_K->Scale(1. / JpsiK_2d_BS_to_JPSI_K_K->Integral());
    JpsiK_2d_LAMBDA0B_to_JPSI_P_K->Scale(1. / JpsiK_2d_LAMBDA0B_to_JPSI_P_K->Integral());
    JpsiK_2d_BS_to_JPSI_K_PI->Scale(1. / JpsiK_2d_BS_to_JPSI_K_PI->Integral());
    JpsiK_2d_BD_to_JPSI_K_K->Scale(1. / JpsiK_2d_BD_to_JPSI_K_K->Integral());
    JpsiK_2d_LAMBDA0B_to_JPSI_P_PI->Scale(1. / JpsiK_2d_LAMBDA0B_to_JPSI_P_PI->Integral());
    
    Kpi_2d_BD_to_JPSI_K_PI->Scale(1. / Kpi_2d_BD_to_JPSI_K_PI->Integral());
    Kpi_2d_BD_to_JPSI_PI_PI->Scale(1. / Kpi_2d_BD_to_JPSI_PI_PI->Integral());
    Kpi_2d_BS_to_JPSI_PI_PI->Scale(1. / Kpi_2d_BS_to_JPSI_PI_PI->Integral());
    Kpi_2d_BS_to_JPSI_K_K->Scale(1. / Kpi_2d_BS_to_JPSI_K_K->Integral());
    Kpi_2d_LAMBDA0B_to_JPSI_P_K->Scale(1. / Kpi_2d_LAMBDA0B_to_JPSI_P_K->Integral());
    Kpi_2d_BS_to_JPSI_K_PI->Scale(1. / Kpi_2d_BS_to_JPSI_K_PI->Integral());
    Kpi_2d_BD_to_JPSI_K_K->Scale(1. / Kpi_2d_BD_to_JPSI_K_K->Integral());
    Kpi_2d_LAMBDA0B_to_JPSI_P_PI->Scale(1. / Kpi_2d_LAMBDA0B_to_JPSI_P_PI->Integral());
    
    KK_1d_BD_to_JPSI_K_PI->Scale(1. / KK_1d_BD_to_JPSI_K_PI->Integral());
    KK_1d_BD_to_JPSI_PI_PI->Scale(1. / KK_1d_BD_to_JPSI_PI_PI->Integral());
    KK_1d_BS_to_JPSI_PI_PI->Scale(1. / KK_1d_BS_to_JPSI_PI_PI->Integral());
    KK_1d_BS_to_JPSI_K_K->Scale(1. / KK_1d_BS_to_JPSI_K_K->Integral());
    KK_1d_LAMBDA0B_to_JPSI_P_K->Scale(1. / KK_1d_LAMBDA0B_to_JPSI_P_K->Integral());
    KK_1d_BS_to_JPSI_K_PI->Scale(1. / KK_1d_BS_to_JPSI_K_PI->Integral());
    KK_1d_BD_to_JPSI_K_K->Scale(1. / KK_1d_BD_to_JPSI_K_K->Integral());
    KK_1d_LAMBDA0B_to_JPSI_P_PI->Scale(1. / KK_1d_LAMBDA0B_to_JPSI_P_PI->Integral());
    
    pipi_1d_BD_to_JPSI_K_PI->Scale(1. / pipi_1d_BD_to_JPSI_K_PI->Integral());
    pipi_1d_BD_to_JPSI_PI_PI->Scale(1. / pipi_1d_BD_to_JPSI_PI_PI->Integral());
    pipi_1d_BS_to_JPSI_PI_PI->Scale(1. / pipi_1d_BS_to_JPSI_PI_PI->Integral());
    pipi_1d_BS_to_JPSI_K_K->Scale(1. / pipi_1d_BS_to_JPSI_K_K->Integral());
    pipi_1d_LAMBDA0B_to_JPSI_P_K->Scale(1. / pipi_1d_LAMBDA0B_to_JPSI_P_K->Integral());
    pipi_1d_BS_to_JPSI_K_PI->Scale(1. / pipi_1d_BS_to_JPSI_K_PI->Integral());
    pipi_1d_BD_to_JPSI_K_K->Scale(1. / pipi_1d_BD_to_JPSI_K_K->Integral());
    pipi_1d_LAMBDA0B_to_JPSI_P_PI->Scale(1. / pipi_1d_LAMBDA0B_to_JPSI_P_PI->Integral());
    
    JpsiK1_1d_control_BD_to_JPSI_K_PI->Scale(1. / JpsiK1_1d_control_BD_to_JPSI_K_PI->Integral());
    if (JpsiK1_1d_control_BD_to_JPSI_PI_PI->Integral() != 0.) {JpsiK1_1d_control_BD_to_JPSI_PI_PI->Scale(1. / JpsiK1_1d_control_BD_to_JPSI_PI_PI->Integral());}
    JpsiK1_1d_control_BS_to_JPSI_PI_PI->Scale(1. / JpsiK1_1d_control_BS_to_JPSI_PI_PI->Integral());
    JpsiK1_1d_control_BS_to_JPSI_K_K->Scale(1. / JpsiK1_1d_control_BS_to_JPSI_K_K->Integral());
    JpsiK1_1d_control_LAMBDA0B_to_JPSI_P_K->Scale(1. / JpsiK1_1d_control_LAMBDA0B_to_JPSI_P_K->Integral());
    JpsiK1_1d_control_BS_to_JPSI_K_PI->Scale(1. / JpsiK1_1d_control_BS_to_JPSI_K_PI->Integral());
    if (JpsiK1_1d_control_BD_to_JPSI_K_K->Integral() != 0.) {JpsiK1_1d_control_BD_to_JPSI_K_K->Scale(1. / JpsiK1_1d_control_BD_to_JPSI_K_K->Integral());}
    JpsiK1_1d_control_LAMBDA0B_to_JPSI_P_PI->Scale(1. / JpsiK1_1d_control_LAMBDA0B_to_JPSI_P_PI->Integral());
    
    JpsiK2_1d_control_BD_to_JPSI_K_PI->Scale(1. / JpsiK2_1d_control_BD_to_JPSI_K_PI->Integral());
    if (JpsiK2_1d_control_BD_to_JPSI_PI_PI->Integral() != 0.) {JpsiK2_1d_control_BD_to_JPSI_PI_PI->Scale(1. / JpsiK2_1d_control_BD_to_JPSI_PI_PI->Integral());}
    JpsiK2_1d_control_BS_to_JPSI_PI_PI->Scale(1. / JpsiK2_1d_control_BS_to_JPSI_PI_PI->Integral());
    JpsiK2_1d_control_BS_to_JPSI_K_K->Scale(1. / JpsiK2_1d_control_BS_to_JPSI_K_K->Integral());
    JpsiK2_1d_control_LAMBDA0B_to_JPSI_P_K->Scale(1. / JpsiK2_1d_control_LAMBDA0B_to_JPSI_P_K->Integral());
    JpsiK2_1d_control_BS_to_JPSI_K_PI->Scale(1. / JpsiK2_1d_control_BS_to_JPSI_K_PI->Integral());
    if (JpsiK2_1d_control_BD_to_JPSI_K_K->Integral() != 0.) {JpsiK2_1d_control_BD_to_JPSI_K_K->Scale(1. / JpsiK2_1d_control_BD_to_JPSI_K_K->Integral());}
    JpsiK2_1d_control_LAMBDA0B_to_JPSI_P_PI->Scale(1. / JpsiK2_1d_control_LAMBDA0B_to_JPSI_P_PI->Integral());
    
    JpsiK_2d_control_BD_to_JPSI_K_PI->Scale(1. / JpsiK_2d_control_BD_to_JPSI_K_PI->Integral());
    if (JpsiK_2d_control_BD_to_JPSI_PI_PI->Integral() != 0.) {JpsiK_2d_control_BD_to_JPSI_PI_PI->Scale(1. / JpsiK_2d_control_BD_to_JPSI_PI_PI->Integral());}
    JpsiK_2d_control_BS_to_JPSI_PI_PI->Scale(1. / JpsiK_2d_control_BS_to_JPSI_PI_PI->Integral());
    JpsiK_2d_control_BS_to_JPSI_K_K->Scale(1. / JpsiK_2d_control_BS_to_JPSI_K_K->Integral());
    JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K->Scale(1. / JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K->Integral());
    JpsiK_2d_control_BS_to_JPSI_K_PI->Scale(1. / JpsiK_2d_control_BS_to_JPSI_K_PI->Integral());
    if (JpsiK_2d_control_BD_to_JPSI_K_K->Integral() != 0.) {JpsiK_2d_control_BD_to_JPSI_K_K->Scale(1. / JpsiK_2d_control_BD_to_JPSI_K_K->Integral());}
    JpsiK_2d_control_LAMBDA0B_to_JPSI_P_PI->Scale(1. / JpsiK_2d_control_LAMBDA0B_to_JPSI_P_PI->Integral());
    
    KK_1d_control_BD_to_JPSI_K_PI->Scale(1. / KK_1d_control_BD_to_JPSI_K_PI->Integral());
    if (KK_1d_control_BD_to_JPSI_PI_PI->Integral() != 0.) {KK_1d_control_BD_to_JPSI_PI_PI->Scale(1. / KK_1d_control_BD_to_JPSI_PI_PI->Integral());}
    KK_1d_control_BS_to_JPSI_PI_PI->Scale(1. / KK_1d_control_BS_to_JPSI_PI_PI->Integral());
    KK_1d_control_BS_to_JPSI_K_K->Scale(1. / KK_1d_control_BS_to_JPSI_K_K->Integral());
    KK_1d_control_LAMBDA0B_to_JPSI_P_K->Scale(1. / KK_1d_control_LAMBDA0B_to_JPSI_P_K->Integral());
    KK_1d_control_BS_to_JPSI_K_PI->Scale(1. / KK_1d_control_BS_to_JPSI_K_PI->Integral());
    if (KK_1d_control_BD_to_JPSI_K_K->Integral() != 0.) {KK_1d_control_BD_to_JPSI_K_K->Scale(1. / KK_1d_control_BD_to_JPSI_K_K->Integral());}
    KK_1d_control_LAMBDA0B_to_JPSI_P_PI->Scale(1. / KK_1d_control_LAMBDA0B_to_JPSI_P_PI->Integral());
    
    //cout << "Expested Itegral of data hist\t" << i_temp << endl;
    cout << "Real Itegral of data hist Kpi_piK\t" << Kpi_piK_2d_data->Integral() << endl;
    cout << "Real Itegral of data hist pK_Kp\t\t" << pK_Kp_2d_data->Integral() << endl;
    cout << "Real Itegral of data hist KK_pipi\t" << KK_pipi_2d_data->Integral() << endl;
    cout << "Real Itegral of data hist pK_Kp rotate\t" << pK_Kp_2d_rotate_data->Integral() << endl;
    cout << "Real Itegral of data hist ppi_pip\t" << ppi_pip_2d_data->Integral() << endl;
    
    TFile *f_fit =new TFile("Zc1plus_fit/input/ROOT_files/fitting_Zc1plus.root", "recreate");
    Kpi_piK_2d_BD_to_JPSI_K_PI->Write();
    Kpi_piK_2d_BD_to_JPSI_PI_PI->Write();
    Kpi_piK_2d_BS_to_JPSI_PI_PI->Write();
    Kpi_piK_2d_BS_to_JPSI_K_K->Write();
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Write();
    Kpi_piK_2d_BD_to_JPSI_K_K->Write();
    Kpi_piK_2d_BS_to_JPSI_K_PI->Write();
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->Write();
    Kpi_piK_2d_data->Write();

    KK_pipi_2d_BD_to_JPSI_K_PI->Write();
    KK_pipi_2d_BD_to_JPSI_PI_PI->Write();
    KK_pipi_2d_BS_to_JPSI_PI_PI->Write();
    KK_pipi_2d_BS_to_JPSI_K_K->Write();
    KK_pipi_2d_LAMBDA0B_to_JPSI_P_K->Write();
    KK_pipi_2d_BD_to_JPSI_K_K->Write();
    KK_pipi_2d_BS_to_JPSI_K_PI->Write();
    KK_pipi_2d_LAMBDA0B_to_JPSI_P_PI->Write();
    KK_pipi_2d_data->Write();

    pK_Kp_2d_BD_to_JPSI_K_PI->Write();
    pK_Kp_2d_BD_to_JPSI_PI_PI->Write();
    pK_Kp_2d_BS_to_JPSI_PI_PI->Write();
    pK_Kp_2d_BS_to_JPSI_K_K->Write();
    pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->Write();
    pK_Kp_2d_BD_to_JPSI_K_K->Write();
    pK_Kp_2d_BS_to_JPSI_K_PI->Write();
    pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI->Write();
    pK_Kp_2d_data->Write();
    
    ppi_pip_2d_BD_to_JPSI_K_PI->Write();
    ppi_pip_2d_BD_to_JPSI_PI_PI->Write();
    ppi_pip_2d_BS_to_JPSI_PI_PI->Write();
    ppi_pip_2d_BS_to_JPSI_K_K->Write();
    ppi_pip_2d_LAMBDA0B_to_JPSI_P_K->Write();
    ppi_pip_2d_BD_to_JPSI_K_K->Write();
    ppi_pip_2d_BS_to_JPSI_K_PI->Write();
    ppi_pip_2d_LAMBDA0B_to_JPSI_P_PI->Write();
    ppi_pip_2d_data->Write();
    
    pK_Kp_2d_test_BD_to_JPSI_K_PI->Write();
    pK_Kp_2d_test_BD_to_JPSI_PI_PI->Write();
    pK_Kp_2d_test_BS_to_JPSI_PI_PI->Write();
    pK_Kp_2d_test_BS_to_JPSI_K_K->Write();
    pK_Kp_2d_test_LAMBDA0B_to_JPSI_P_K->Write();
    //pK_Kp_2d_test_BD_to_JPSI_K_K->Write();
    //pK_Kp_2d_test_BS_to_JPSI_K_PI->Write();
    //pK_Kp_2d_test_LAMBDA0B_to_JPSI_P_PI->Write();
    pK_Kp_2d_test_data->Write();
    
    cout << "\n_________________________________________\nTest of test pK_Kp:" << endl;
    cout << "X projection: " << pK_Kp_2d_test_data->ProjectionX()->Integral() << endl;
    cout << "Y projection: " << pK_Kp_2d_test_data->ProjectionY()->Integral() << endl;
    if (pK_Kp_2d_test_data->ProjectionX()->Integral() == pK_Kp_2d_test_data->ProjectionY()->Integral())
    {
        cout << "Everything is OK" << endl;
    }
    else
    {
        cout << "Something went wrong" << endl;
    }
    cout << "_________________________________________\n" << endl;
    
    pK_Kp_2d_rotate_BD_to_JPSI_K_PI->Write();
    pK_Kp_2d_rotate_BD_to_JPSI_PI_PI->Write();
    pK_Kp_2d_rotate_BS_to_JPSI_PI_PI->Write();
    pK_Kp_2d_rotate_BS_to_JPSI_K_K->Write();
    pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_K->Write();
    pK_Kp_2d_rotate_BD_to_JPSI_K_K->Write();
    pK_Kp_2d_rotate_BS_to_JPSI_K_PI->Write();
    pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_PI->Write();
    pK_Kp_2d_rotate_data->Write();
    
    KK_1d_BD_to_JPSI_K_PI->Write();
    KK_1d_BD_to_JPSI_PI_PI->Write();
    KK_1d_BS_to_JPSI_PI_PI->Write();
    KK_1d_BS_to_JPSI_K_K->Write();
    KK_1d_LAMBDA0B_to_JPSI_P_K->Write();
    KK_1d_BD_to_JPSI_K_K->Write();
    KK_1d_BS_to_JPSI_K_PI->Write();
    KK_1d_LAMBDA0B_to_JPSI_P_PI->Write();
    KK_1d_data->Write();
    
    pipi_1d_BD_to_JPSI_K_PI->Write();
    pipi_1d_BD_to_JPSI_PI_PI->Write();
    pipi_1d_BS_to_JPSI_PI_PI->Write();
    pipi_1d_BS_to_JPSI_K_K->Write();
    pipi_1d_LAMBDA0B_to_JPSI_P_K->Write();
    pipi_1d_BD_to_JPSI_K_K->Write();
    pipi_1d_BS_to_JPSI_K_PI->Write();
    pipi_1d_LAMBDA0B_to_JPSI_P_PI->Write();
    pipi_1d_data->Write();
    pipi_1d_data_sb->Write();
    
    f_fit->Close();
    
    f_sa->cd();
    Jpsipi_2d_BD_to_JPSI_K_PI->Write();
    Jpsipi_2d_BD_to_JPSI_PI_PI->Write();
    Jpsipi_2d_BS_to_JPSI_PI_PI->Write();
    Jpsipi_2d_BS_to_JPSI_K_K->Write();
    Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->Write();
    Jpsipi_2d_BD_to_JPSI_K_K->Write();
    Jpsipi_2d_BS_to_JPSI_K_PI->Write();
    Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->Write();
    Jpsipi_2d_data->Write();
    
    JpsiK_2d_BD_to_JPSI_K_PI->Write();
    JpsiK_2d_BD_to_JPSI_PI_PI->Write();
    JpsiK_2d_BS_to_JPSI_PI_PI->Write();
    JpsiK_2d_BS_to_JPSI_K_K->Write();
    JpsiK_2d_LAMBDA0B_to_JPSI_P_K->Write();
    JpsiK_2d_BD_to_JPSI_K_K->Write();
    JpsiK_2d_BS_to_JPSI_K_PI->Write();
    JpsiK_2d_LAMBDA0B_to_JPSI_P_PI->Write();
    JpsiK_2d_data->Write();
    
    Kpi_2d_BD_to_JPSI_K_PI->Write();
    Kpi_2d_BD_to_JPSI_PI_PI->Write();
    Kpi_2d_BS_to_JPSI_PI_PI->Write();
    Kpi_2d_BS_to_JPSI_K_K->Write();
    Kpi_2d_LAMBDA0B_to_JPSI_P_K->Write();
    Kpi_2d_BD_to_JPSI_K_K->Write();
    Kpi_2d_BS_to_JPSI_K_PI->Write();
    Kpi_2d_LAMBDA0B_to_JPSI_P_PI->Write();
    Kpi_2d_data->Write();
    
    f_sa->Close();
    
    f_ca->cd();
    
    JpsiK1_1d_control_BD_to_JPSI_K_PI->Write();
    JpsiK1_1d_control_BD_to_JPSI_PI_PI->Write();
    JpsiK1_1d_control_BS_to_JPSI_PI_PI->Write();
    JpsiK1_1d_control_BS_to_JPSI_K_K->Write();
    JpsiK1_1d_control_LAMBDA0B_to_JPSI_P_K->Write();
    JpsiK1_1d_control_BD_to_JPSI_K_K->Write();
    JpsiK1_1d_control_BS_to_JPSI_K_PI->Write();
    JpsiK1_1d_control_LAMBDA0B_to_JPSI_P_PI->Write();
    JpsiK1_1d_control_data->Write();
    
    JpsiK2_1d_control_BD_to_JPSI_K_PI->Write();
    JpsiK2_1d_control_BD_to_JPSI_PI_PI->Write();
    JpsiK2_1d_control_BS_to_JPSI_PI_PI->Write();
    JpsiK2_1d_control_BS_to_JPSI_K_K->Write();
    JpsiK2_1d_control_LAMBDA0B_to_JPSI_P_K->Write();
    JpsiK2_1d_control_BD_to_JPSI_K_K->Write();
    JpsiK2_1d_control_BS_to_JPSI_K_PI->Write();
    JpsiK2_1d_control_LAMBDA0B_to_JPSI_P_PI->Write();
    JpsiK2_1d_control_data->Write();
    
    KK_1d_control_BD_to_JPSI_K_PI->Write();
    KK_1d_control_BD_to_JPSI_PI_PI->Write();
    KK_1d_control_BS_to_JPSI_PI_PI->Write();
    KK_1d_control_BS_to_JPSI_K_K->Write();
    KK_1d_control_LAMBDA0B_to_JPSI_P_K->Write();
    KK_1d_control_BD_to_JPSI_K_K->Write();
    KK_1d_control_BS_to_JPSI_K_PI->Write();
    KK_1d_control_LAMBDA0B_to_JPSI_P_PI->Write();
    KK_1d_control_data->Write();
    
    JpsiK_2d_control_BD_to_JPSI_K_PI->Write();
    JpsiK_2d_control_BD_to_JPSI_PI_PI->Write();
    JpsiK_2d_control_BS_to_JPSI_PI_PI->Write();
    JpsiK_2d_control_BS_to_JPSI_K_K->Write();
    JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K->Write();
    JpsiK_2d_control_BD_to_JPSI_K_K->Write();
    JpsiK_2d_control_BS_to_JPSI_K_PI->Write();
    JpsiK_2d_control_LAMBDA0B_to_JPSI_P_PI->Write();
    JpsiK_2d_control_data->Write();
    
    f_ca->Close();
    
    f_ca_Lb->cd();
    
    JpsiK_2d_controlLb_BD_to_JPSI_K_PI->Write();
    JpsiK_2d_controlLb_BD_to_JPSI_PI_PI->Write();
    JpsiK_2d_controlLb_BS_to_JPSI_PI_PI->Write();
    JpsiK_2d_controlLb_BS_to_JPSI_K_K->Write();
    JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Write();
    JpsiK_2d_controlLb_BD_to_JPSI_K_K->Write();
    JpsiK_2d_controlLb_BS_to_JPSI_K_PI->Write();
    JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->Write();
    JpsiK_2d_controlLb_data->Write();
    
    Jpsip_2d_controlLb_BD_to_JPSI_K_PI->Write();
    Jpsip_2d_controlLb_BD_to_JPSI_PI_PI->Write();
    Jpsip_2d_controlLb_BS_to_JPSI_PI_PI->Write();
    Jpsip_2d_controlLb_BS_to_JPSI_K_K->Write();
    Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Write();
    Jpsip_2d_controlLb_BD_to_JPSI_K_K->Write();
    Jpsip_2d_controlLb_BS_to_JPSI_K_PI->Write();
    Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->Write();
    Jpsip_2d_controlLb_data->Write();
    
    pK_2d_controlLb_BD_to_JPSI_K_PI->Write();
    pK_2d_controlLb_BD_to_JPSI_PI_PI->Write();
    pK_2d_controlLb_BS_to_JPSI_PI_PI->Write();
    pK_2d_controlLb_BS_to_JPSI_K_K->Write();
    pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Write();
    pK_2d_controlLb_BD_to_JPSI_K_K->Write();
    pK_2d_controlLb_BS_to_JPSI_K_PI->Write();
    pK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->Write();
    pK_2d_controlLb_data->Write();
    
    f_ca_Lb->Close();
    
    f_phs->cd();
    
    phs_BD_to_JPSI_K_PI->Write();
    phs_BS_to_JPSI_K_K->Write();
    phs_LAMBDA0B_to_JPSI_P_K->Write();
    
    parameters.Write("counters");
    
    f_phs->Close();
    
    //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
    // that part of code is used for saving
    // same-charge muon events distribution 
    // from signal area
    //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
    
    TFile *f_out_bg =new TFile("Comb_bg/comb_bg_signal.root", "recreate");
    Jpsipi_2d_bg_comb->Write();
    JpsiK_2d_bg_comb->Write();
    Kpi_2d_bg_comb->Write();
    trk_pt_bg_comb->Write();
    f_out_bg->Close();
    
    //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
    // next part of code is used for drawing pictures
    // that show mc events from "global area" distributions 
    // for different decays
    // with overlapping signal area boundaries
    //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
    
    area_hist->SetStats(0);
    area_hist->SetLineColor(2);
    
    TCanvas *c_Bd = new TCanvas("c_Bd","signal area optimisation Bd");
    Kpi_piK_2d_BD_to_JPSI_K_PI->SetStats(0);
    Kpi_piK_2d_BD_to_JPSI_K_PI->Scale(10.);
    Kpi_piK_2d_BD_to_JPSI_K_PI->Draw("COLZ");
    area_hist->Draw("same BOX");
    c_Bd->SaveAs("SignalAreaShapeSA/BdvsSR.png");
    
    TCanvas *c_Lb = new TCanvas("c_Lb","signal area optimisation Lb");
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->SetStats(0);
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Scale(10.);
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Draw("COLZ");
    area_hist->Draw("same BOX");
    c_Lb->SaveAs("SignalAreaShapeSA/LbvsSR.png");
    
    TCanvas *c_Bs = new TCanvas("c_Bs","signal area optimisation Bs");
    Kpi_piK_2d_BS_to_JPSI_K_K->SetStats(0);
    Kpi_piK_2d_BS_to_JPSI_K_K->Scale(10.);
    Kpi_piK_2d_BS_to_JPSI_K_K->Draw("COLZ");    
    area_hist->Draw("same BOX");
    c_Bs->SaveAs("SignalAreaShapeSA/BsvsSR.png");
    
    //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
    
    TFile *f_out_cos =new TFile("Zc1plus_fit/input/ROOT_files/cos_signal.root", "recreate");
    cos_theta_Zc_data->Write();
    cos_theta_Zc_BD_to_JPSI_K_PI->Write();
    cos_theta_Zc_BD_to_JPSI_PI_PI->Write();
    cos_theta_Zc_BS_to_JPSI_PI_PI->Write();
    cos_theta_Zc_BS_to_JPSI_K_K->Write();
    cos_theta_Zc_LAMBDA0B_to_JPSI_P_K->Write();
    cos_theta_Zc_BS_to_JPSI_K_PI->Write();
    cos_theta_Zc_BD_to_JPSI_K_K->Write();
    cos_theta_Zc_LAMBDA0B_to_JPSI_P_PI->Write();
    
    phi_K_data->Write();
    phi_K_BD_to_JPSI_K_PI->Write();
    phi_K_BD_to_JPSI_PI_PI->Write();
    phi_K_BS_to_JPSI_PI_PI->Write();
    phi_K_BS_to_JPSI_K_K->Write();
    phi_K_LAMBDA0B_to_JPSI_P_K->Write();
    phi_K_BS_to_JPSI_K_PI->Write();
    phi_K_BD_to_JPSI_K_K->Write();
    phi_K_LAMBDA0B_to_JPSI_P_PI->Write();
    
    alpha_mu_data->Write();
    alpha_mu_BD_to_JPSI_K_PI->Write();
    alpha_mu_BD_to_JPSI_PI_PI->Write();
    alpha_mu_BS_to_JPSI_PI_PI->Write();
    alpha_mu_BS_to_JPSI_K_K->Write();
    alpha_mu_LAMBDA0B_to_JPSI_P_K->Write();
    alpha_mu_BS_to_JPSI_K_PI->Write();
    alpha_mu_BD_to_JPSI_K_K->Write();
    alpha_mu_LAMBDA0B_to_JPSI_P_PI->Write();
    
    phi_mu_Kstar_data->Write();
    phi_mu_Kstar_BD_to_JPSI_K_PI->Write();
    phi_mu_Kstar_BD_to_JPSI_PI_PI->Write();
    phi_mu_Kstar_BS_to_JPSI_PI_PI->Write();
    phi_mu_Kstar_BS_to_JPSI_K_K->Write();
    phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K->Write();
    phi_mu_Kstar_BS_to_JPSI_K_PI->Write();
    phi_mu_Kstar_BD_to_JPSI_K_K->Write();
    phi_mu_Kstar_LAMBDA0B_to_JPSI_P_PI->Write();
    
    phi_mu_X_data->Write();
    phi_mu_X_BD_to_JPSI_K_PI->Write();
    phi_mu_X_BD_to_JPSI_PI_PI->Write();
    phi_mu_X_BS_to_JPSI_PI_PI->Write();
    phi_mu_X_BS_to_JPSI_K_K->Write();
    phi_mu_X_LAMBDA0B_to_JPSI_P_K->Write();
    phi_mu_X_BS_to_JPSI_K_PI->Write();
    phi_mu_X_BD_to_JPSI_K_K->Write();
    phi_mu_X_LAMBDA0B_to_JPSI_P_PI->Write();
    
    cos_theta_psi_X_data->Write();
    cos_theta_psi_X_BD_to_JPSI_K_PI->Write();
    cos_theta_psi_X_BD_to_JPSI_PI_PI->Write();
    cos_theta_psi_X_BS_to_JPSI_PI_PI->Write();
    cos_theta_psi_X_BS_to_JPSI_K_K->Write();
    cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K->Write();
    cos_theta_psi_X_BS_to_JPSI_K_PI->Write();
    cos_theta_psi_X_BD_to_JPSI_K_K->Write();
    cos_theta_psi_X_LAMBDA0B_to_JPSI_P_PI->Write();
    
    cos_theta_Kstar_data->Write();
    cos_theta_Kstar_BD_to_JPSI_K_PI->Write();
    cos_theta_Kstar_BD_to_JPSI_PI_PI->Write();
    cos_theta_Kstar_BS_to_JPSI_PI_PI->Write();
    cos_theta_Kstar_BS_to_JPSI_K_K->Write();
    cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->Write();
    cos_theta_Kstar_BS_to_JPSI_K_PI->Write();
    cos_theta_Kstar_BD_to_JPSI_K_K->Write();
    cos_theta_Kstar_LAMBDA0B_to_JPSI_P_PI->Write();
    
    cos_theta_psi_Kstar_data->Write();
    cos_theta_psi_Kstar_BD_to_JPSI_K_PI->Write();
    cos_theta_psi_Kstar_BD_to_JPSI_PI_PI->Write();
    cos_theta_psi_Kstar_BS_to_JPSI_PI_PI->Write();
    cos_theta_psi_Kstar_BS_to_JPSI_K_K->Write();
    cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K->Write();
    cos_theta_psi_Kstar_BS_to_JPSI_K_PI->Write();
    cos_theta_psi_Kstar_BD_to_JPSI_K_K->Write();
    cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_PI->Write();
    
    cos_theta_B0_data->Write();
    cos_theta_B0_BD_to_JPSI_K_PI->Write();
    cos_theta_B0_BD_to_JPSI_PI_PI->Write();
    cos_theta_B0_BS_to_JPSI_PI_PI->Write();
    cos_theta_B0_BS_to_JPSI_K_K->Write();
    cos_theta_B0_LAMBDA0B_to_JPSI_P_K->Write();
    cos_theta_B0_BS_to_JPSI_K_PI->Write();
    cos_theta_B0_BD_to_JPSI_K_K->Write();
    cos_theta_B0_LAMBDA0B_to_JPSI_P_PI->Write();
    
    phi_psi_data->Write();
    phi_psi_BD_to_JPSI_K_PI->Write();
    phi_psi_BD_to_JPSI_PI_PI->Write();
    phi_psi_BS_to_JPSI_PI_PI->Write();
    phi_psi_BS_to_JPSI_K_K->Write();
    phi_psi_LAMBDA0B_to_JPSI_P_K->Write();
    phi_psi_BS_to_JPSI_K_PI->Write();
    phi_psi_BD_to_JPSI_K_K->Write();
    phi_psi_LAMBDA0B_to_JPSI_P_PI->Write();
    
    f_out_cos->Close();
    
    TH1D* B_pt_data_buf = (TH1D*)B_pt_data->Clone("B_pt_data_buf");
    
    B_pt_BS_to_JPSI_K_K->SetLineColor(30);
    B_pt_BD_to_JPSI_K_PI->SetLineColor(7);
    B_pt_BD_to_JPSI_PI_PI->SetLineColor(8);
    B_pt_BS_to_JPSI_PI_PI->SetLineColor(46);
    B_pt_LAMBDA0B_to_JPSI_P_K->SetLineColor(3);
    
    TFile *f_B_pt_comb = new TFile("B_pt_studies/B_pt_comb.root");
    TH1D *B_pt_comb = (TH1D*)f_B_pt_comb->Get("B_pt_comb");
    
    double n_lambda, n_bs, n_bd_pipi, n_bd_Kpi, n_bs_KK, p0, p1, p2, p3, a0, a1, a2, b0, b1, b2, d0_KKpipi, p0_KKpipi, p1_KKpipi, p2_KKpipi, p3_KKpipi, a0_KKpipi, a1_KKpipi, a2_KKpipi, b0_KKpipi, b1_KKpipi, b2_KKpipi, x0_KKpipi, comb_bg_norm;
    
    TFile *f_par = new TFile("Zc1plus_fit/input/parameters/parameters_signal_Zc1plus_v16_it0_n9.root", "open");
    TVectorD *vpar = (TVectorD*)f_par->Get("parameters");
    
    Double_t comb_bg_sa_integral = 0.;
    
    n_lambda = (*vpar)(80);
    n_bs = (*vpar)(81);
    n_bd_pipi = (*vpar)(82);
    n_bd_Kpi = (*vpar)(83);
    n_bs_KK = (*vpar)(84);
    p0 = (*vpar)(85);
    p1 = (*vpar)(86);
    p2 = (*vpar)(87);
    p3 = (*vpar)(88);
    a0 = (*vpar)(89);
    b0 = (*vpar)(90);
    b1 = (*vpar)(91);
    b2 = (*vpar)(92);
    comb_bg_norm = (*vpar)(213);

    float xx, yy, x, y, xmin_Kpi = 4.9, xmin_piK = 4.65;
    
    float st = 1 / sqrt(2.);

    for(int i = 1; i <= Kpi_piK_2d_data->ProjectionX()->GetNbinsX(); i++)
    {
        for(int j = 1; j <= Kpi_piK_2d_data->ProjectionY()->GetNbinsX(); j++)
        {
            xx = Kpi_piK_2d_data->ProjectionX()->GetBinCenter(i);
            yy = Kpi_piK_2d_data->ProjectionY()->GetBinCenter(j);
            x = st * xx + st * yy - 6.6;
            y = -st * xx + st * yy;
            if (area_hist->GetBinContent(i, j) == 1)
            {
                comb_bg_sa_integral += exp(p0 + x * p1 + pow(x, 2.) * p2 + pow(x, 3.) * p3) * exp(-1. * pow((y + a0) / (b0 + x * b1 + pow(x, 2.) * b2), 2.));
            }
        }
    }
    
    TH1D* B_pt_comb_buf = (TH1D*)B_pt_comb->Clone("B_pt_comb_buf");
    B_pt_comb_buf->Scale(comb_bg_sa_integral / B_pt_data->Integral() / B_pt_comb_buf->Integral());
    B_pt_comb_buf->SetFillStyle(3244);
    B_pt_comb_buf->SetFillColor(42);
    B_pt_comb->Scale(comb_bg_sa_integral / B_pt_comb->Integral());
    
    TH1D* B_pt_mc = (TH1D*)B_pt_BS_to_JPSI_K_K->Clone("B_pt_mc");
    B_pt_mc->Add(B_pt_BD_to_JPSI_K_PI);
    B_pt_mc->Add(B_pt_BD_to_JPSI_PI_PI);
    B_pt_mc->Add(B_pt_BS_to_JPSI_PI_PI);
    B_pt_mc->Add(B_pt_LAMBDA0B_to_JPSI_P_K);
    
    B_pt_mc->SetLineColor(1);
    B_pt_mc->SetLineWidth(2);
    
    TLegend *legend_1 = new TLegend(.60,.60,.90,.90);
    legend_1->AddEntry(B_pt_data,"data");
    legend_1->AddEntry(B_pt_comb_buf,"data comb");
    legend_1->AddEntry(B_pt_mc,"mc total");
	legend_1->AddEntry(B_pt_BS_to_JPSI_K_K,"BS_to_JPSI_K_K");
	legend_1->AddEntry(B_pt_BD_to_JPSI_K_PI,"BD_to_JPSI_K_PI");
	legend_1->AddEntry(B_pt_BD_to_JPSI_PI_PI,"BD_to_JPSI_PI_PI");
	legend_1->AddEntry(B_pt_BS_to_JPSI_PI_PI,"BS_to_JPSI_PI_PI");
	legend_1->AddEntry(B_pt_LAMBDA0B_to_JPSI_P_K,"LAMBDA0B_to_JPSI_P_K");
    
    B_pt_data->SetTitle("");
    B_pt_data->SetStats(0);
    
    TCanvas *c_B_pt = new TCanvas("c_B_pt","signal area optimisation Bd");
    B_pt_data->GetYaxis()->SetRangeUser(0., B_pt_data->GetMaximum() * 1.35);
    B_pt_data->DrawNormalized();
    B_pt_comb_buf->Draw("hist same");
    B_pt_mc->DrawNormalized("same hist");
    B_pt_BS_to_JPSI_K_K->DrawNormalized("same hist");
    B_pt_BD_to_JPSI_K_PI->DrawNormalized("same hist");
    B_pt_BD_to_JPSI_PI_PI->DrawNormalized("same hist");
    B_pt_BS_to_JPSI_PI_PI->DrawNormalized("same hist");
    B_pt_LAMBDA0B_to_JPSI_P_K->DrawNormalized("same hist");
    legend_1->Draw();
    c_B_pt->SaveAs("B_pt_studies/B_pt.png");
    
    TH1D *B_pt_BS_to_JPSI_K_K_relation = new TH1D("B_pt_BS_to_JPSI_K_K_relation", "B_pt_BS_to_JPSI_K_K_relation", 50, 0., 100.);
    TH1D *B_pt_BD_to_JPSI_K_PI_relation = new TH1D("B_pt_BD_to_JPSI_K_PI_relation", "B_pt_BD_to_JPSI_K_PI_relation", 50, 0., 100.);
    TH1D *B_pt_BD_to_JPSI_PI_PI_relation = new TH1D("B_pt_BD_to_JPSI_PI_PI_relation", "B_pt_BD_to_JPSI_PI_PI_relation", 50, 0., 100.);
    TH1D *B_pt_BS_to_JPSI_PI_PI_relation = new TH1D("B_pt_BS_to_JPSI_PI_PI_relation", "B_pt_BS_to_JPSI_PI_PI_relation", 50, 0., 100.);
    TH1D *B_pt_LAMBDA0B_to_JPSI_P_K_relation = new TH1D("B_pt_LAMBDA0B_to_JPSI_P_K_relation", "B_pt_LAMBDA0B_to_JPSI_P_K_relation", 50, 0., 100.);
    TH1D *B_pt_mc_relation = new TH1D("B_pt_mc_relation", "B_pt_mc_relation", 50, 0., 100.);
    
    cout << "\nB_pt_data integral before procedure:\t" << B_pt_data->Integral() << endl;
    B_pt_data->Add(B_pt_comb, -1.);
    cout << "\nB_pt_data integral after procedure:\t" << B_pt_data->Integral() << endl;
    B_pt_data->Scale(1. / B_pt_data->Integral());
    B_pt_BS_to_JPSI_K_K->Scale(1. / B_pt_BS_to_JPSI_K_K->Integral());
    B_pt_BD_to_JPSI_K_PI->Scale(1. / B_pt_BD_to_JPSI_K_PI->Integral());
    B_pt_BD_to_JPSI_PI_PI->Scale(1. / B_pt_BD_to_JPSI_PI_PI->Integral());
    B_pt_BS_to_JPSI_PI_PI->Scale(1. / B_pt_BS_to_JPSI_PI_PI->Integral());
    B_pt_LAMBDA0B_to_JPSI_P_K->Scale(1. / B_pt_LAMBDA0B_to_JPSI_P_K->Integral());
    B_pt_mc->Scale(1. / B_pt_mc->Integral());
    
    B_pt_BS_to_JPSI_K_K_relation->Divide(B_pt_data, B_pt_BS_to_JPSI_K_K);
    B_pt_BD_to_JPSI_K_PI_relation->Divide(B_pt_data, B_pt_BD_to_JPSI_K_PI);
    B_pt_BD_to_JPSI_PI_PI_relation->Divide(B_pt_data, B_pt_BD_to_JPSI_PI_PI);
    B_pt_BS_to_JPSI_PI_PI_relation->Divide(B_pt_data, B_pt_BS_to_JPSI_PI_PI);
    B_pt_LAMBDA0B_to_JPSI_P_K_relation->Divide(B_pt_data, B_pt_LAMBDA0B_to_JPSI_P_K);
    B_pt_mc_relation->Divide(B_pt_data, B_pt_mc);
    
    TF1 *f1 = new TF1("f1", "[0] + x * [1]", 14., 60.);
    f1->SetParameters(0.6, 0.01);
    Double_t par[2];
    Double_t par_err[2];
    TF1 *f2 = new TF1("f2", "[0]", 60., 100.);
    f2->SetParameters(1.2, 0.);
    TCanvas *c_B_pt_relation = new TCanvas("c_B_pt_relation","signal area optimisation Bd");
    B_pt_mc_relation->Draw();
    B_pt_mc_relation->Fit("f1", "", "", 14., 50.);
    B_pt_mc_relation->Fit("f2", "+", "", 50., 100.);
    f1->GetParameters(&par[0]);
    for(int i = 0; i < 2; i++)
    {
        par_err[i] = f1->GetParError(i);
    }
    c_B_pt_relation->SaveAs("B_pt_studies/B_pt_relation.png");
    
    cout << "\n\nResults of fit:\n" << endl;
    cout << "par0:\t" << par[0] << " +- " << par_err[0] << endl;
    cout << "par1:\t" << par[1] << " +- " << par_err[1] << endl;
    
    TFile *f_out_func = new TFile("B_pt_studies/parameters_signal_Zc1plus_v16_it1_n0.root", "recreate");
    TVectorD par_B_pt_func(2);
    par_B_pt_func[0] = par[0];
    par_B_pt_func[1] = par[1];
    TVectorD par_err_B_pt_func(2);
    par_err_B_pt_func[0] = par_err[0];
    par_err_B_pt_func[1] = par_err[1];
    par_B_pt_func.Write("par_B_pt_func");
    par_err_B_pt_func.Write("par_err_B_pt_func");
    f_out_func->Close();
    
    B_pt_BS_to_JPSI_K_K_reweighted->Add(B_pt_BD_to_JPSI_K_PI_reweighted);
    B_pt_BS_to_JPSI_K_K_reweighted->Add(B_pt_BD_to_JPSI_PI_PI_reweighted);
    B_pt_BS_to_JPSI_K_K_reweighted->Add(B_pt_BS_to_JPSI_PI_PI_reweighted);
    B_pt_BS_to_JPSI_K_K_reweighted->Add(B_pt_LAMBDA0B_to_JPSI_P_K_reweighted);
    
    B_pt_data_buf->Add(B_pt_comb, -1.);
    
    B_pt_BS_to_JPSI_K_K_reweighted->SetLineColor(2);
    B_pt_BS_to_JPSI_K_K_reweighted->SetLineWidth(2);
    
    TCanvas *c_B_pt_reweighted = new TCanvas("c_B_pt_reweighted","signal area optimisation Bd");
    B_pt_data_buf->GetYaxis()->SetRangeUser(0., B_pt_data_buf->GetMaximum() * 1.35);
    B_pt_data_buf->DrawNormalized();
    B_pt_BS_to_JPSI_K_K_reweighted->DrawNormalized("same hist");
    c_B_pt_reweighted->SaveAs("B_pt_studies/B_pt_reweighted.png");
    
    TCanvas *c_ppiX = new TCanvas("c_ppiX","c_ppiX");
    ppi_pip_2d_data->ProjectionX()->Draw();
    
    TCanvas *c_ppiY = new TCanvas("c_ppiY","c_ppiY");
    ppi_pip_2d_data->ProjectionY()->Draw();
}
