In this folder you can see code for running EvoAlg for optimization signal area shape.

For running algorithm:

[root 0] .L esab_run.c
[root 1] main()

Pictures with forms of the signal area for different generations and graphs for reference values are in folder "pictures".
Histograms with mc events of different decays and comb background Kpi_piK mass distributions for EvoAlg are loaded from file evo_alg.root. This file will be reloaded after every run of Zc1plus hypothesis fit code.

To change alpha and beta parameters of EvoAlg you should go to adapt_func_esab function in adapt_func_esab.h.

After every successful run of EvoAlg you should copy signal_area_shape.root to ROOT_files folder.
