//That is implementation of "Population" class
#define noi 100

#include <iostream>
#include <time.h>
#include <TH1.h>
#include <TH2.h>
#include "Individual_esab.h"
#include "adapt_func_esab.h"

using namespace std;

class Population
{
private:
    Individual individuals[noi];
    TH2 *area_hist = new TH2I("area_hist", "area_hist", 40, 4.9, 5.7, 62, 4.65, 5.9);
    TFile *f = new TFile("signal_area_shape.root", "recreate");
    //double elite;
public:

//konstructor

    Population(){}

//set methods

    void setInitial(int first, int last)
    {
        for(int i = first; i <= last; i++)
        {
            individuals[i].setInitial();
        }
        //elite = p_elite;
    }

    void setGeneNum(int x, int y, float value_BD_to_JPSI_K_PI, float value_bg)
    {
        for(int i = 0; i < noi; i++)
        {
            individuals[i].setGeneNum(x, y, value_BD_to_JPSI_K_PI, value_bg);
        }        
    }

    /*void setElite(double value)
    {
        elite = value;
    }*/

//get methods

    double getAdaptMin()
    {
        return individuals[0].getAdapt();
    }

    double getMin_b_s()
    {
        return adapt_func_esab_b_s(individuals[0]);
    }

    double getMin_b_splusb()
    {
        return adapt_func_esab_b_splusb(individuals[0]);
    }

    double getMin_s_splusb()
    {
        return adapt_func_esab_s_splusb(individuals[0]);
    }

    double getMin_comb_d()
    {
        return adapt_func_esab_comb_d(individuals[0]);
    }

    double getMin_BD_to_JPSI_K_PI_d()
    {
        return adapt_func_esab_BD_to_JPSI_K_PI_d(individuals[0]);
    }

    double getMin_BD_to_JPSI_K_PI_number()
    {
        return adapt_func_esab_BD_to_JPSI_K_PI_number(individuals[0]);
    }

    double getMin_BD_to_JPSI_PI_PI_d()
    {
        return adapt_func_esab_BD_to_JPSI_PI_PI_d(individuals[0]);
    }

    double getMin_BS_to_JPSI_PI_PI_d()
    {
        return adapt_func_esab_BS_to_JPSI_PI_PI_d(individuals[0]);
    }

    double getMin_BS_to_JPSI_K_K_d()
    {
        return adapt_func_esab_BS_to_JPSI_K_K_d(individuals[0]);
    }

    double getMin_LAMBDA0B_to_JPSI_P_K_d()
    {
        return adapt_func_esab_LAMBDA0B_to_JPSI_P_K_d(individuals[0]);
    }

    /*double getAdaptMean()
    {
        double value;        
        for(int i = 0; i < noi; i++)
        {
            value += individuals[i].getAdapt() / noi;
        }
        return value;
    }*/

    /*double getGeneMin(int i)
    {
        return individuals[0].getGene(i);
    }*/

//other methods 

    /*int elite_number() //(?) may be it is wrong decision 
    {
        return int(noi * elite);           
    }*/
   
    /*int Crossover() //it returns 1, when we have 1 or less elite individuals
    {
        int n1, n2, ne = elite_number();
        if (ne <= 1) {return 1;}        
        for(int i = ne; i < noi; i++)
        {
            srand(rand());
            n1 = rand() % ne;
            n2 = rand() % ne;
            while (n1 == n2)
            {
                n2 = rand() % ne;
            }
            individuals[i].Crossover(individuals[n1], individuals[n2]);
        }
        return 0;
    }*/

    void Estimation_b_splusb(int first, int last)
    {
        for(int i = first; i <= last; i++)
        {
            individuals[i].setAdapt(adapt_func_esab_b_splusb(individuals[i]));
        }   
        //area_hist->Reset();
    }

    void Estimation_s_splusb(int first, int last)
    {
        for(int i = first; i <= last; i++)
        {
            individuals[i].setAdapt(adapt_func_esab_s_splusb(individuals[i]));
        }
        //area_hist->Reset();
    }

    void Estimation_b_s(int first, int last)
    {
        for(int i = first; i <= last; i++)
        {
            individuals[i].setAdapt(adapt_func_esab_b_s(individuals[i]));
        }
        //area_hist->Reset();
    }

    void Estimation(int first, int last)
    {
        for(int i = first; i <= last; i++)
        {
            individuals[i].setAdapt(adapt_func_esab(individuals[i]));
        }
        //area_hist->Reset();
    }

    
    void Sort()
    {
        int i = 0;
        bool flag = 0;
        Individual b;
        while (i < noi - 1 && !flag)
        {
            flag = 1;
            for(int k = noi - 1; k > i; k-=1)
            {
                if (individuals[k].getAdapt() < individuals[k - 1].getAdapt())
                {
                    flag = 0;
                    b = individuals[k];
                    individuals[k] = individuals[k - 1];
                    individuals[k - 1] = b;
                }
            }
        }
    }

    void Mutation(int first, int last)
    {
        for(int i = first; i <= last; i++)
        {
            individuals[i].Mutation();
        }
    }

    void Copy(int last)
    {
        /*default_random_engine generator;
        generator.seed(rand());
        uniform_real_distribution<double> distribution(0., 1.);*/        
        for(int i = 1; i <= last; i++)
        {
            individuals[i] = individuals[0];
        }
    }


    void DrawBest()
    {
        area_hist->Reset();        
        individuals[0].DrawArea(area_hist);
    }

    void DrawBestBD()
    {
        area_hist->Reset();        
        individuals[0].DrawAreaBD(area_hist);
    }

    void SaveBest()
    {
        area_hist->Reset();        
        individuals[0].DrawArea(area_hist);
        f->cd();
        area_hist->Write();
        f->Close();
    }

    /*void Disaster(double lower[nog], double upper[noi], int first, int last)
    {
        for(int i = first; i <= last; i++)
        {
            individuals[i].setRandom(lower, upper);
        }
    }*/

    /*void Generation()
    {
        Estimation(0, noi - 1);
        Sort();
        Crossover();        
        Mutation(0, noi - 1);
    }*/ 

    /*void Run(int number_of_generation)   
    {
        for(int i = 1; i <= number_of_generation; i++)
        {
            cout << "|||||||||||||||||||||||||||||" << endl;
            cout << "Generation number " << i << endl; 
            Generation();
            cout << "min value:\t" << getAdaptMin() << endl;
            cout << "mean value:\t" << getAdaptMean() << endl;
            cout << "|||||||||||||||||||||||||||||" << endl;   
        }
    }*/

    /*void printAdaptAll()
    {
        cout << "____________________________\n" << endl;
        for(int i = 0; i < noi; i++)
        {
            cout << i << ": " << individuals[i].getAdapt() << endl;
        }
        cout << "____________________________\n" << endl;        
    }*/
};
