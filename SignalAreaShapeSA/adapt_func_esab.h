//#include "Individual_esab.h"
#include <TH1.h>

TH2D *Kpi_piK_2d_bg_for_ea = 0;
TH2D *Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea = 0;
TH2D *Kpi_piK_2d_BD_to_JPSI_PI_PI_for_ea = 0;
TH2D *Kpi_piK_2d_BS_to_JPSI_K_K_for_ea = 0;
TH2D *Kpi_piK_2d_BS_to_JPSI_PI_PI_for_ea = 0;
TH2D *Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K_for_ea = 0;
TH2D *Kpi_piK_2d_full_for_ea = 0;

double adapt_func_esab_b_splusb(Individual x)
{
    double value = 0., b = 0., d = 0.;
    int xmin = x.getXmin(),
        xmax = x.getXmax(),
        ymin = x.getYmin(),
        ymax = x.getYmax();
    for(int i = xmin; i <= xmax; i++)
    {
        for(int j = ymin; j <= ymax; j++)
        {
            b += (Kpi_piK_2d_full_for_ea->GetBinContent(i, j) - Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea->GetBinContent(i, j)) * x.getGene(i, j);
            d += Kpi_piK_2d_full_for_ea->GetBinContent(i, j) * x.getGene(i, j);
        }
    }
    value = b / sqrt(d);
    return value;
}

double adapt_func_esab_s_splusb(Individual x)
{
    double value = 0., b = 0., d = 0.;
    int xmin = x.getXmin(),
        xmax = x.getXmax(),
        ymin = x.getYmin(),
        ymax = x.getYmax();
    for(int i = xmin; i <= xmax; i++)
    {
        for(int j = ymin; j <= ymax; j++)
        {
            b += (Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea->GetBinContent(i, j)) * x.getGene(i, j);
            d += Kpi_piK_2d_full_for_ea->GetBinContent(i, j) * x.getGene(i, j);
        }
    }
    value = -b / sqrt(d);
    return value;
}

double adapt_func_esab(Individual x)
{
    double value = 0., b = 0., d = 0., b_phys = 0., b_comb = 0.;
    double alpha = 4.;
    int xmin = x.getXmin(),
        xmax = x.getXmax(),
        ymin = x.getYmin(),
        ymax = x.getYmax();
    for(int i = xmin; i <= xmax; i++)
    {
        for(int j = ymin; j <= ymax; j++)
        {
            b += (Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea->GetBinContent(i, j)) * x.getGene(i, j);
            b_phys += (Kpi_piK_2d_full_for_ea->GetBinContent(i, j) - Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea->GetBinContent(i, j) - Kpi_piK_2d_bg_for_ea->GetBinContent(i, j)) * x.getGene(i, j);
            b_comb += (Kpi_piK_2d_bg_for_ea->GetBinContent(i, j)) * x.getGene(i, j);
            d += (Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea->GetBinContent(i, j) + alpha * (Kpi_piK_2d_full_for_ea->GetBinContent(i, j) - Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea->GetBinContent(i, j))) * x.getGene(i, j);
        }
    }
    value = -b / sqrt(b + 4. * b_phys + 8. * b_comb);
    return value;
}

double adapt_func_esab_b_s(Individual x)
{
    double value = 0., b = 0., d = 0., s = 0.;
    int xmin = x.getXmin(),
        xmax = x.getXmax(),
        ymin = x.getYmin(),
        ymax = x.getYmax();
    for(int i = xmin; i <= xmax; i++)
    {
        for(int j = ymin; j <= ymax; j++)
        {
            b += (Kpi_piK_2d_full_for_ea->GetBinContent(i, j) - Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea->GetBinContent(i, j)) * x.getGene(i, j);
            //b_comb += (Kpi_piK_2d_bg_for_ea->GetBinContent(i, j)) * x.getGene(i, j);
            d += Kpi_piK_2d_full_for_ea->GetBinContent(i, j) * x.getGene(i, j);
            s += (Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea->GetBinContent(i, j)) * x.getGene(i, j);
        }
    }
    value = b / s;
    return value;
}

double adapt_func_esab_comb_d(Individual x)
{
    double value = 0., b = 0., d = 0., s = 0.;
    int xmin = x.getXmin(),
        xmax = x.getXmax(),
        ymin = x.getYmin(),
        ymax = x.getYmax();
    for(int i = xmin; i <= xmax; i++)
    {
        for(int j = ymin; j <= ymax; j++)
        {
            b += (Kpi_piK_2d_bg_for_ea->GetBinContent(i, j)) * x.getGene(i, j);
            d += Kpi_piK_2d_full_for_ea->GetBinContent(i, j) * x.getGene(i, j);
            //s += (Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea->GetBinContent(i, j)) * x.getGene(i, j);
        }
    }
    value = b / d;
    return value;
}

double adapt_func_esab_BD_to_JPSI_K_PI_d(Individual x)
{
    double value = 0., b = 0., d = 0., s = 0.;
    int xmin = x.getXmin(),
        xmax = x.getXmax(),
        ymin = x.getYmin(),
        ymax = x.getYmax();
    for(int i = xmin; i <= xmax; i++)
    {
        for(int j = ymin; j <= ymax; j++)
        {
            //b += (Kpi_piK_2d_full_for_ea->GetBinContent(i, j) - Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea->GetBinContent(i, j)) * x.getGene(i, j);
            d += Kpi_piK_2d_full_for_ea->GetBinContent(i, j) * x.getGene(i, j);
            s += (Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea->GetBinContent(i, j)) * x.getGene(i, j);
        }
    }
    value = s / d;
    return value;
}

double adapt_func_esab_BD_to_JPSI_PI_PI_d(Individual x)
{
    double value = 0., b = 0., d = 0., s = 0.;
    int xmin = x.getXmin(),
        xmax = x.getXmax(),
        ymin = x.getYmin(),
        ymax = x.getYmax();
    for(int i = xmin; i <= xmax; i++)
    {
        for(int j = ymin; j <= ymax; j++)
        {
            //b += (Kpi_piK_2d_full_for_ea->GetBinContent(i, j) - Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea->GetBinContent(i, j)) * x.getGene(i, j);
            d += Kpi_piK_2d_full_for_ea->GetBinContent(i, j) * x.getGene(i, j);
            s += (Kpi_piK_2d_BD_to_JPSI_PI_PI_for_ea->GetBinContent(i, j)) * x.getGene(i, j);
        }
    }
    value = s / d;
    return value;
}

double adapt_func_esab_BS_to_JPSI_PI_PI_d(Individual x)
{
    double value = 0., b = 0., d = 0., s = 0.;
    int xmin = x.getXmin(),
        xmax = x.getXmax(),
        ymin = x.getYmin(),
        ymax = x.getYmax();
    for(int i = xmin; i <= xmax; i++)
    {
        for(int j = ymin; j <= ymax; j++)
        {
            //b += (Kpi_piK_2d_full_for_ea->GetBinContent(i, j) - Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea->GetBinContent(i, j)) * x.getGene(i, j);
            d += Kpi_piK_2d_full_for_ea->GetBinContent(i, j) * x.getGene(i, j);
            s += (Kpi_piK_2d_BS_to_JPSI_PI_PI_for_ea->GetBinContent(i, j)) * x.getGene(i, j);
        }
    }
    value = s / d;
    return value;
}

double adapt_func_esab_BS_to_JPSI_K_K_d(Individual x)
{
    double value = 0., b = 0., d = 0., s = 0.;
    int xmin = x.getXmin(),
        xmax = x.getXmax(),
        ymin = x.getYmin(),
        ymax = x.getYmax();
    for(int i = xmin; i <= xmax; i++)
    {
        for(int j = ymin; j <= ymax; j++)
        {
            //b += (Kpi_piK_2d_full_for_ea->GetBinContent(i, j) - Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea->GetBinContent(i, j)) * x.getGene(i, j);
            d += Kpi_piK_2d_full_for_ea->GetBinContent(i, j) * x.getGene(i, j);
            s += (Kpi_piK_2d_BS_to_JPSI_K_K_for_ea->GetBinContent(i, j)) * x.getGene(i, j);
        }
    }
    value = s / d;
    return value;
}

double adapt_func_esab_LAMBDA0B_to_JPSI_P_K_d(Individual x)
{
    double value = 0., b = 0., d = 0., s = 0.;
    int xmin = x.getXmin(),
        xmax = x.getXmax(),
        ymin = x.getYmin(),
        ymax = x.getYmax();
    for(int i = xmin; i <= xmax; i++)
    {
        for(int j = ymin; j <= ymax; j++)
        {
            //b += (Kpi_piK_2d_full_for_ea->GetBinContent(i, j) - Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea->GetBinContent(i, j)) * x.getGene(i, j);
            d += Kpi_piK_2d_full_for_ea->GetBinContent(i, j) * x.getGene(i, j);
            s += (Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K_for_ea->GetBinContent(i, j)) * x.getGene(i, j);
        }
    }
    value = s / d;
    return value;
}

double adapt_func_esab_BD_to_JPSI_K_PI_number(Individual x)
{
    double value = 0., b = 0., d = 0., s = 0.;
    int xmin = x.getXmin(),
        xmax = x.getXmax(),
        ymin = x.getYmin(),
        ymax = x.getYmax();
    for(int i = xmin; i <= xmax; i++)
    {
        for(int j = ymin; j <= ymax; j++)
        {
            //b += (Kpi_piK_2d_full_for_ea->GetBinContent(i, j) - Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea->GetBinContent(i, j)) * x.getGene(i, j);
            d += Kpi_piK_2d_full_for_ea->GetBinContent(i, j) * x.getGene(i, j);
            s += (Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea->GetBinContent(i, j)) * x.getGene(i, j);
        }
    }
    value = s;
    return value;
}





