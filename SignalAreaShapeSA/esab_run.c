#include "Population_esab.h"
#include "string.h"

#define nog 100

int main()
{
    TFile *f = new TFile("evo_alg.root");
    
    Kpi_piK_2d_bg_for_ea = (TH2D*)f->Get("Kpi_piK_2d_bg_for_ea");
    Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea = (TH2D*)f->Get("Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea");
    Kpi_piK_2d_BD_to_JPSI_PI_PI_for_ea = (TH2D*)f->Get("Kpi_piK_2d_BD_to_JPSI_PI_PI_for_ea");
    Kpi_piK_2d_BS_to_JPSI_K_K_for_ea = (TH2D*)f->Get("Kpi_piK_2d_BS_to_JPSI_K_K_for_ea");
    Kpi_piK_2d_BS_to_JPSI_PI_PI_for_ea = (TH2D*)f->Get("Kpi_piK_2d_BS_to_JPSI_PI_PI_for_ea");
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K_for_ea = (TH2D*)f->Get("Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K_for_ea");
    Kpi_piK_2d_full_for_ea = (TH2D*)f->Get("Kpi_piK_2d_full_for_ea");
    
    int xmin = 5,
        xmax = 40,
        ymin = 5,
        ymax = 50;    
    Population p;
    for(int i = 0; i < sizex; i++)
    {
        for(int k = 0; k < sizey; k++)
        {
            p.setGeneNum(i, k, Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea->GetBinContent(xmin + i, ymin + k), Kpi_piK_2d_bg_for_ea->GetBinContent(xmin + i, ymin + k));
        }
    }
    p.setInitial(0, noi - 1);
    //p.DrawBest();
    p.Mutation(0, noi - 1);
    p.Estimation(0, noi - 1);
    p.Sort();
    TCanvas *c0 = new TCanvas("esab_area"); 
    float x[nog], y_b_s[nog], y_b_splusb[nog], y_s_splusb[nog],
          y_comb_d[nog], y_BD_to_JPSI_K_PI_d[nog], y_BD_to_JPSI_K_PI_number[nog], y_BD_to_JPSI_PI_PI_d[nog], y_BS_to_JPSI_PI_PI_d[nog], y_BS_to_JPSI_K_K_d[nog], y_LAMBDA0B_to_JPSI_P_K_d[nog];
    
    for(int i = 0; i < nog; i++)
    {
        cout << "Generation " << i << endl;
        //p.setInitial((int)(noi * 0.8) + 1, noi - 1);        
        p.Copy(noi - 1);
        p.Mutation(1, noi - 1);
        p.Estimation(1, noi - 1);
        p.Sort();
        p.DrawBest();
        string first = "pictures/esab_area/generation";
        string format = ".png";
        ostringstream ost;
        ost << i;
        string name;
        name += first;
        name += ost.str();
        name += format;
        c0->SaveAs(name.c_str());
        cout << p.getAdaptMin() << endl;
        cout << "b / s\t" << p.getMin_b_s() << endl;
        cout << "b / sqrt(s + b)\t" << p.getMin_b_splusb() << endl;
        cout << "s / sqrt(s + b)\t" << p.getMin_s_splusb() << endl;
        cout << "Bd_to_JPSI_K_PI number\t" << p.getMin_BD_to_JPSI_K_PI_number() << endl; 
        x[i] = (float)i;
        y_b_s[i] = p.getMin_b_s();
        y_b_splusb[i] = p.getMin_b_splusb();
        y_s_splusb[i] = -p.getMin_s_splusb();
        y_comb_d[i] = p.getMin_comb_d();
        y_BD_to_JPSI_K_PI_d[i] = p.getMin_BD_to_JPSI_K_PI_d();
        y_BD_to_JPSI_K_PI_number[i] = p.getMin_BD_to_JPSI_K_PI_number();
        y_BD_to_JPSI_PI_PI_d[i] = p.getMin_BD_to_JPSI_PI_PI_d();
        y_BS_to_JPSI_PI_PI_d[i] = p.getMin_BS_to_JPSI_PI_PI_d();
        y_BS_to_JPSI_K_K_d[i] = p.getMin_BS_to_JPSI_K_K_d();
        y_LAMBDA0B_to_JPSI_P_K_d[i] = p.getMin_LAMBDA0B_to_JPSI_P_K_d();
    }
    p.SaveBest();

    TCanvas *c1 = new TCanvas("b_s");
    TGraph *gr1 = new TGraph(nog, x, y_b_s);
    gr1->SetTitle("v1 || b / s || (min func: s / sqrt(s + b)");    
    gr1->Draw(); 
    c1->SaveAs("pictures/esab_graphs/graph_b_s.png");

    TCanvas *c2 = new TCanvas("b_splusb");
    TGraph *gr2 = new TGraph(nog, x, y_b_splusb);
    gr2->SetTitle("v1 || b / splusb || (min func: s / sqrt(s + b)");    
    gr2->Draw(); 
    c2->SaveAs("pictures/esab_graphs/graph_b_splusb.png");

    TCanvas *c3 = new TCanvas("s_splusb");
    TGraph *gr3 = new TGraph(nog, x, y_s_splusb);
    gr3->SetTitle("v1 || s / splusb || (min func: s / sqrt(s + b)");    
    gr3->Draw(); 
    c3->SaveAs("pictures/esab_graphs/graph_s_splusb.png");

    TCanvas *c_comb_d = new TCanvas("comb_d");
    TGraph *gr_comb_d = new TGraph(nog, x, y_comb_d);
    gr_comb_d->SetTitle("v1 || comb_d || (min func: s / sqrt(s + b)");    
    gr_comb_d->Draw(); 
    c_comb_d->SaveAs("pictures/esab_graphs/graph_comb_d.png");

    TCanvas *c_BD_to_JPSI_PI_PI_d = new TCanvas("BD_to_JPSI_PI_PI_d");
    TGraph *gr_BD_to_JPSI_PI_PI_d = new TGraph(nog, x, y_BD_to_JPSI_PI_PI_d);
    gr_BD_to_JPSI_PI_PI_d->SetTitle("v1 || BD_to_JPSI_PI_PI_d || (min func: s / sqrt(s + b)");    
    gr_BD_to_JPSI_PI_PI_d->Draw(); 
    c_BD_to_JPSI_PI_PI_d->SaveAs("pictures/esab_graphs/graph_BD_to_JPSI_PI_PI_d.png");

    TCanvas *c_BD_to_JPSI_K_PI_d = new TCanvas("BD_to_JPSI_K_PI_d");
    TGraph *gr_BD_to_JPSI_K_PI_d = new TGraph(nog, x, y_BD_to_JPSI_K_PI_d);
    gr_BD_to_JPSI_K_PI_d->SetTitle("v1 || BD_to_JPSI_K_PI_d || (min func: s / sqrt(s + b)");    
    gr_BD_to_JPSI_K_PI_d->Draw(); 
    c_BD_to_JPSI_K_PI_d->SaveAs("pictures/esab_graphs/graph_BD_to_JPSI_K_PI_d.png");

    TCanvas *c_BD_to_JPSI_K_PI_number = new TCanvas("BD_to_JPSI_K_PI_number");
    TGraph *gr_BD_to_JPSI_K_PI_number = new TGraph(nog, x, y_BD_to_JPSI_K_PI_number);
    gr_BD_to_JPSI_K_PI_number->SetTitle("v1 || BD_to_JPSI_K_PI_number || (min func: s / sqrt(s + b)");    
    gr_BD_to_JPSI_K_PI_number->Draw(); 
    c_BD_to_JPSI_K_PI_number->SaveAs("pictures/esab_graphs/graph_BD_to_JPSI_K_PI_number.png");

    TCanvas *c_BS_to_JPSI_PI_PI_d = new TCanvas("BS_to_JPSI_PI_PI_d");
    TGraph *gr_BS_to_JPSI_PI_PI_d = new TGraph(nog, x, y_BS_to_JPSI_PI_PI_d);
    gr_BS_to_JPSI_PI_PI_d->SetTitle("v1 || BS_to_JPSI_PI_PI_d || (min func: s / sqrt(s + b)");    
    gr_BS_to_JPSI_PI_PI_d->Draw(); 
    c_BS_to_JPSI_PI_PI_d->SaveAs("pictures/esab_graphs/graph_BS_to_JPSI_PI_PI_d.png");

    TCanvas *c_BS_to_JPSI_K_K_d = new TCanvas("BS_to_JPSI_K_K_d");
    TGraph *gr_BS_to_JPSI_K_K_d = new TGraph(nog, x, y_BS_to_JPSI_K_K_d);
    gr_BS_to_JPSI_K_K_d->SetTitle("v1 || BS_to_JPSI_K_K_d || (min func: s / sqrt(s + b)");    
    gr_BS_to_JPSI_K_K_d->Draw(); 
    c_BS_to_JPSI_K_K_d->SaveAs("pictures/esab_graphs/graph_BS_to_JPSI_K_K_d.png");

    TCanvas *c_LAMBDA0B_to_JPSI_P_K_d = new TCanvas("LAMBDA0B_to_JPSI_P_K_d");
    TGraph *gr_LAMBDA0B_to_JPSI_P_K_d = new TGraph(nog, x, y_LAMBDA0B_to_JPSI_P_K_d);
    gr_LAMBDA0B_to_JPSI_P_K_d->SetTitle("v1 || LAMBDA0B_to_JPSI_P_K_d || (min func: s / sqrt(s + b)");    
    gr_LAMBDA0B_to_JPSI_P_K_d->Draw(); 
    c_LAMBDA0B_to_JPSI_P_K_d->SaveAs("pictures/esab_graphs/graph_LAMBDA0B_to_JPSI_P_K_d.png"); 
}
