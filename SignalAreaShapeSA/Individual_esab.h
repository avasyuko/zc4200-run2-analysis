//That is implementation of "Individual" class
#define sizex 36 //size of area in bins
#define sizey 46 // After binning changing don`t forget to apply changes to defining constants
#define sizehistx 40 
#define sizehisty 62
#define GEV 0.001

#include <iostream>
#include <time.h>
#include <TH1.h>
#include <TH2.h>
#include <string.h>

using namespace std;

TH2 *area_hist1 = new TH2I("area_hist1", "area_hist1", 40, 4.9, 5.7, 62, 4.65, 5.9);

class Individual
{
private:
    // After binning changing don`t forget to apply changes to private part
    double adaptability;
    int area[sizex][sizey],
        area_buffer[sizex][sizey],
        areax[sizex][sizey],
        areay[sizex][sizey];
    int xmin = 5,
        xmax = 40,
        ymin = 5,
        ymax = 50;
    double xstep = (5.7 - 4.9) / 40.,
           ystep = (5.9 - 4.65) / 62.;
    double Xmin = 4.9 + xstep / 2.,
           Ymin = 4.65 + ystep / 2.;
    float area_BD_to_JPSI_K_PI[sizex][sizey],
            area_bg[sizex][sizey];
    
public:
//konstructor
    
    Individual()
    {
        //cout << "Go!" << endl;
        for(int i0 = 0; i0 < sizex; i0++)
        for(int k0 = 0; k0 < sizey; k0++)
        {
            int i = area_hist1->ProjectionX()->FindBin(area_hist1->ProjectionY()->GetBinCenter(k0 + ymin)) - xmin;
            int k = area_hist1->ProjectionY()->FindBin(area_hist1->ProjectionX()->GetBinCenter(i0 + xmin)) - ymin;
            areax[i0][k0] = i;
            areay[i0][k0] = k;
            
            //cout << i0 << " : " << k0 << "  ->  " << i << " : " << k << endl;
        }
    }

//set methods:

    void setGene(int x, int y, int value)
    {
        area[x][y] = value;
    }

    void setGeneNum(int x, int y, float value_BD_to_JPSI_K_PI, float value_bg)
    {
        area_BD_to_JPSI_K_PI[x][y] = value_BD_to_JPSI_K_PI;
        area_bg[x][y] = value_bg;
    }

    /*void setStep(int n, double value)
    {
        steps[n] = value;
    }*/

    /*void setLowerLimit(int n, double value)
    {
        lower_limits[n] = value;
    }*/

    /*void setUpperLimit(int n, double value)
    {
        upper_limits[n] = value;
    }*/

    void setAdapt(double value)
    {
        adaptability = value;
    }

    void setInitial()
    {           
        for(int i = 0; i < sizex; i++)
        {
            for(int k = 0; k < sizey; k++)
            {
                area[i][k] = 0;
            }
        }
        
        for(int i = 0; i < sizex; i++)
        {
            for(int k = 0; k < sizey; k++)
            {
                float x = Xmin + (xmin + i - 1) * xstep, 
                      y = Ymin + (ymin + k - 1) * ystep;               
                bool vertical = (5.25 < y && y < 5.31) && (5.25 < x && x < 5.31),
                     horizontal = (5.25 < y && y < 5.31) && (5.25 < x && x < 5.31);             
                if (vertical || horizontal)
                {
                    area[i][k] = 1;
                    area[areax[i][k]][areay[i][k]] = 1; // symmetry part
                }
            }
            //cout << "\n";
        }
    }

//get methods
 
    int getGene(int x, int y) 
    {
        if (xmin <= x && x <= xmax && ymin <= y && y <= ymax)
        {        
            return area[x - xmin][y - ymin];
        }
        else
        {
            return 0 ;              
        }
    }

    /*int getNbins()
    {
        return area->Integral();
    }*/

    int getXmin()
    {
        return xmin;
    }

    int getXmax()
    {
        return xmax;
    }

    int getYmin()
    {
        return ymin;
    }

    int getYmax()
    {
        return ymax;
    }
 
    /*double getLowerLimit(int n)
    {
        return lower_limits[n];
    }*/ 

    /*double getUpperLimit(int n)
    {
        return upper_limits[n];
    }*/
 
    double getAdapt()
    {
        return adaptability;
    } 

    double getSquare()
    {
        int s = 0;        
        for(int i = 1; i < sizex - 1; i++)
        {
            for(int j = 1; j < sizey - 1; j++)
            {
                s += area[i][j];
            }
        }
        return (double)s;
    }

//other methods

    void DrawArea(TH2 *hist)
    {
        for(int i = 0; i < sizehistx; i++)
        {
            for(int j = 0; j < sizehisty; j++)
            {
                if (xmin <= i && i <= xmax && ymin <= j && j <= ymax)
                {        
                    hist->SetBinContent(i, j, area[i - xmin][j - ymin]);
                }
                else
                {
                    hist->SetBinContent(i, j, 0);               
                }
                
            } 
        }       
        hist->Draw("COLZ");       
    }    

    void DrawAreaBD(TH2 *hist)
    {
        for(int i = 0; i < sizehistx; i++)
        {
            for(int j = 0; j < sizehisty; j++)
            {
                if (xmin <= i && i <= xmax && ymin <= j && j <= ymax)
                {        
                    hist->SetBinContent(i, j, area_BD_to_JPSI_K_PI[i - xmin][j - ymin]);
                }
                else
                {
                    hist->SetBinContent(i, j, 0);               
                }
                
            } 
        }       
        hist->Draw("COLZ");       
    }    

    /*void Crossover(Individual father, Individual mother)
    {
        srand (rand());        
        int split = rand() % nog;
        for(int i = 0; i < split; i++)
        {
            genes[i] = father.getGene(i);
            steps[i] = father.getStep(i);
        }
        for(int i = split; i < nog; i++)
        {
            genes[i] = mother.getGene(i);
            steps[i] = mother.getStep(i);
        }
    }

    void showDNA()
    {
        for (int i = 0; i < nog - 1; i++)
        {
            cout << genes[i] << " || ";
        }
        cout << genes[nog - 1] << endl;
    }*/

    void Mutation()
    {
        //cout << "Mutation:" << endl;
        default_random_engine generator;
        generator.seed(rand());
        uniform_real_distribution<double> distribution(0., 1.);
        double sq = sqrt(getSquare());
        for(int i = 1; i < sizex - 1; i++)
        {
            for(int j = 1; j < sizey - 1; j++)
            {
                bool enough = (area_BD_to_JPSI_K_PI[i][j] > 100.) && (area_bg[i][j] > 0.);                 
                bool entire = (area[i - 1][j] == 1) && (area[i + 1][j] == 1) && (area[i][j - 1] == 1) && (area[i][j + 1] == 1);
                bool nothing = (area[i - 1][j] == 0) && (area[i + 1][j] == 0) && (area[i][j - 1] == 0) && (area[i][j + 1] == 0);
                //float x = Xmin + i * xstep, 
                      //y = Ymin + j * ystep;
                if (enough) {                
                if (!entire && !nothing)
                {
                    if (distribution(generator) < 0.005 * exp(-0.01 * sq)) //* exp(-5. * abs(x - 5.28)) * exp(-5. * (y - 5.28)))
                    {
                        //int bc = area[i][j];
                        //area[i][j] = 1 - bc;
                        area_buffer[i][j] = 1;
                    }
                    else {area_buffer[i][j] = 0;}
                }
                else {area_buffer[i][j] = 0;} }
                else {area_buffer[i][j] = 0;}
            }
        }
        
        for(int i = 1; i < sizex - 1; i++)
        {
            for(int j = 1; j < sizey - 1; j++)
            {   
                if (area_buffer[i][j] == 1)
                {
                    int bc = area[i][j];
                    area[i][j] = 1 - bc;
                    area[areax[i][j]][areay[i][j]] = 1 - bc; // symmetry part
                    //cout << i << " : " << j << " ---> " << areax[i][j] << ":" << areay[i][j] << endl;
                }
            }
        }

        for(int i = 1; i < sizex - 1; i++)
        {
            for(int j = 1; j < sizey - 1; j++)
            {
                bool entire = (area[i - 1][j] == 1) && (area[i + 1][j] == 1) && (area[i][j - 1] == 1) && (area[i][j + 1] == 1);
                bool nothing = (area[i - 1][j] == 0) && (area[i + 1][j] == 0) && (area[i][j - 1] == 0) && (area[i][j + 1] == 0);
                //float x = Xmin + i * xstep, 
                      //y = Ymin + j * ystep;
                if (entire)
                {
                    area[i][j] = 1;
                }
                if (nothing)
                {
                    area[i][j] = 0;
                }
            }
        }

        /*bool exist;
        int jmin, jmax;
        int imin, imax;
        bool Problems = 1;

        while (Problems)
        {
        Problems = 0;
        for(int i = 1; i < sizex - 1; i++)
        {
            exist = 0;
            jmin = 0;
            jmax = 0; 
            for(int j = 1; j < sizey - 1; j++)
            {
                if (area[i][j] == 1)
                {
                    if (!exist) {exist = 1; jmin = j;}
                    jmax = j;
                }
            }
            if (exist)
            {
                for(int j = jmin; j <= jmax; j++) {if(area[i][j] == 0) {Problems = 1;} area[i][j] = 1;}
            }
        }

        for(int j = 1; j < sizey - 1; j++)
        {
            exist = 0;
            imin = 0;
            imax = 0; 
            for(int i = 1; i < sizex - 1; i++)
            {
                if (area[i][j] == 1)
                {
                    if (!exist) {exist = 1; imin = i;}
                    imax = i;
                }
            }
            if (exist)
            {
                for(int i = imin; i <= imax; i++) {if(area[i][j] == 0) {Problems = 1;} area[i][j] = 1;}
            }
        }
        }*/
    }
};
