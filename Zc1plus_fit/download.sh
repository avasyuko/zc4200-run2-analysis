#!/bin/bash
folder=iteration0
cd $folder

touch task_test.txt
touch task_test.sh
touch buf.txt

echo "pbook<<EOF >> buf.txt" >> task_test.sh

while read LINE
	do echo "show($LINE)" >> task_test.sh
 
done < taskid_${folder}.txt

echo "exit()" >> task_test.sh
echo "EOF" >> task_test.sh

echo "Start task test:"
. task_test.sh
rm task_test.sh

c=0
i=0
dn=()
while read LINE1
    do let "d = c % 3"
if [[ "$c" -eq "0" ]]
then
	let "c = c + 1"
	continue
fi
if [[ "$d" -eq "0" ]]
then
	cl=0
	bool=0
	while [[ "$bool" -eq "0" ]] 
	do
	if [[ "${LINE1:$cl:1}" == "r" ]]
	then
		echo "${LINE1:0:8}------->running" >> task_test.txt
	    dn+=(0)
		break
	fi
	if [[ "${LINE1:$cl:1}" == "d" ]]
	then
		echo "${LINE1:0:8}------->done" >> task_test.txt
		dn+=(1)
		let "i = i + 1"
		break
	fi
	if [[ "${LINE1:$cl:1}" == "f" ]]
	then
        echo "${LINE1:2:8}------->failed" >> task_test.txt
		dn+=(0)
		break
	fi
	let "cl = cl + 1"
	let "bool = cl / 30"
	done
fi
let "c = c + 1"
done < buf.txt

echo ""
cat task_test.txt

rm buf.txt
rm task_test.txt

echo ""

if [[ $i -eq "10" ]]
then
	echo "All tasks are done"
else
	echo "Only $i tasks are done. Run this script later."
fi

echo "If you want to download datasets press y:"

read choice

if [[ $choice == "y" ]]
then
	echo "Downloading datasets..."
touch estimation.c
echo "#include <stdio.h>" >> estimation.c
echo "#include <iostream>" >> estimation.c
echo "#include <TFile.h>" >> estimation.c
echo "#include <TVectorD.h>" >> estimation.c
echo "TVectorD vmin(214);" >> estimation.c
echo "TVectorD *vpar;" >> estimation.c
echo "void vcopy()" >> estimation.c
echo "{" >> estimation.c
echo "for(int i = 0; i < 214; i++)" >> estimation.c
echo "{" >> estimation.c
echo "vmin[i] = (*vpar)(i);" >> estimation.c
echo "}" >> estimation.c
echo "}" >> estimation.c
echo "using namespace std;" >> estimation.c 
echo "int main() {" >> estimation.c
echo "float min = 0;" >> estimation.c

i=0
while read LINE
	do if [[ ${dn[$i]} == "1" ]]
then
rucio download ${LINE}_outputs.tar
cd ${LINE}_outputs.tar
tar -xvf *
cp -r out* ../.
cd ..
mv outpu* number$i
echo "TFile *f_${i} = new TFile("'"'"number${i}/parameters_signal_Zc1plus_v10_n1.root"'");' >> estimation.c
echo "vpar = (TVectorD*)f_${i}->Get("'"parameters");' >> estimation.c
echo 'cout << "number'"$i"' likelyhood: " << (*vpar)(213) << "\t" << (*vpar)(44) << "\t" << (*vpar)(45) << endl;' >> estimation.c
echo 'if ((*vpar)(213) < min) {min = (*vpar)(213); vcopy();}' >> estimation.c
rm -r user.avasyuko*
fi
let "i = i + 1"
done < dataset_${folder}.txt
echo 'cout << "\n\nBest: " << min << "\n" << endl;' >> estimation.c
echo 'TFile *f_out = new TFile("parameters_in.root", "recreate");' >> estimation.c
echo 'vmin.Write("parameters");' >> estimation.c
echo 'f_out->Close();' >> estimation.c
echo "}" >> estimation.c

fi

root -b -l <<EOF
.L estimation.c++
main()
.q
EOF

rm ../input/parameters_in.root
cp parameters_in.root ../input/

#rm estimation.c	

echo "Goodbye =)"



