

		|| MODEL Zc1plus ||


Mode: only drawing pictures

 PARAMETER DEFINITIONS:
    NO.   NAME         VALUE      STEP SIZE      LIMITS
     1 K1410_1_Bw1_amp   1.00000e+00  1.00000e+01     no limits
     2 K1410_1_Bw1_phi  -1.07159e+01  1.00000e+00     no limits
     3 K1430_0_Bw1_amp   1.65058e+01  1.00000e+01     no limits
     4 K1430_0_Bw1_phi  -9.55440e-01  1.00000e+00     no limits
     5 K1430_2_Bw1_amp  -2.16735e+02  1.00000e+01     no limits
     6 K1430_2_Bw1_phi  -5.25446e+02  1.00000e+00     no limits
     7 K1680_1_Bw1_amp   7.32275e+00  1.00000e+01     no limits
     8 K1680_1_Bw1_phi  -6.04420e+02  1.00000e+00     no limits
     9 K1780_3_Bw1_amp  -2.89613e+01  1.00000e+01     no limits
    10 K1780_3_Bw1_phi  -9.28762e+01  1.00000e+00     no limits
    11 K1950_0_Bw1_amp  -2.49460e-01  1.00000e+01     no limits
    12 K1950_0_Bw1_phi   8.74607e+00  1.00000e+00     no limits
    13 K2045_4_Bw1_amp   1.34009e+05  1.00000e+01     no limits
    14 K2045_4_Bw1_phi  -4.12552e+03  1.00000e+00     no limits
    15 K1980_2_Bw1_amp   6.61320e+02  1.00000e+01     no limits
    16 K1980_2_Bw1_phi   2.25062e+02  1.00000e+00     no limits
    17 K1410_1_Bw2_amp   1.69846e+02  1.00000e+01     no limits
    18 K1410_1_Bw2_phi  -1.72477e+00  1.00000e+00     no limits
    19 K1410_1_Bw3_amp   9.58629e+01  1.00000e+01     no limits
    20 K1410_1_Bw3_phi   7.13864e+00  1.00000e+00     no limits
    21 K1430_2_Bw2_amp   3.75315e+01  1.00000e+01     no limits
    22 K1430_2_Bw2_phi   1.18042e+02  1.00000e+00     no limits
    23 K1430_2_Bw3_amp   5.44540e+02  1.00000e+01     no limits
    24 K1430_2_Bw3_phi   4.38953e+01  1.00000e+00     no limits
    25 K1680_1_Bw2_amp  -1.54276e+02  1.00000e+01     no limits
    26 K1680_1_Bw2_phi  -7.07569e+00  1.00000e+00     no limits
    27 K1680_1_Bw3_amp   2.44429e+01  1.00000e+01     no limits
    28 K1680_1_Bw3_phi   3.17893e+00  1.00000e+00     no limits
    29 K1780_3_Bw2_amp  -8.66595e+03  1.00000e+01     no limits
    30 K1780_3_Bw2_phi  -3.23401e+00  1.00000e+00     no limits
    31 K1780_3_Bw3_amp  -3.89483e+03  1.00000e+01     no limits
    32 K1780_3_Bw3_phi   3.57174e+02  1.00000e+00     no limits
    33 K1980_2_Bw2_amp  -1.98346e+03  1.00000e+01     no limits
    34 K1980_2_Bw2_phi  -1.42698e+01  1.00000e+00     no limits
    35 K1980_2_Bw3_amp   8.49113e+01  1.00000e+01     no limits
    36 K1980_2_Bw3_phi   2.73994e+02  1.00000e+00     no limits
    37 K2045_4_Bw2_amp   7.64600e+05  1.00000e+01     no limits
    38 K2045_4_Bw2_phi   4.53049e+00  1.00000e+00     no limits
    39 K2045_4_Bw3_amp  -9.52210e+05  1.00000e+01     no limits
    40 K2045_4_Bw3_phi  -3.44434e-01  1.00000e+00     no limits
    41 X1Bs1_amp    1.27865e+02  1.00000e+01     no limits
    42 X1Bs1_phi    1.41022e+01  1.00000e+00     no limits
    43 X1Bs2_amp    3.05737e+03  1.00000e+01     no limits
    44 X1Bs2_phi   -5.29200e+00  1.00000e+00     no limits
    45 gamma_Zc4200   3.66122e-01  1.00000e-02    0.00000e+00  5.00000e-01
    46 m_Zc4200     4.12742e+00  1.00000e-01    4.00000e+00  4.40000e+00
    47 n_BD_to_JPSI_K_PI_par   0.00000e+00  1.00000e+01     no limits
    48 phi1680_1_Bw1_amp   1.00000e+00  1.00000e+00     no limits
    49 phi1680_1_Bw1_phi  -9.05636e+04  1.00000e+01     no limits
    50 phi1680_1_Bw2_amp  -1.19704e+05  1.00000e+00     no limits
    51 phi1680_1_Bw2_phi   2.78672e+02  1.00000e+01     no limits
    52 phi1680_1_Bw3_amp   1.16252e+05  1.00000e+00     no limits
    53 phi1680_1_Bw3_phi   2.48112e+02  1.00000e+01     no limits
    54 f1525_2_Bw1_amp  -1.55855e+06  1.00000e+00     no limits
    55 f1525_2_Bw1_phi   3.58544e+02  1.00000e+01     no limits
    56 f1525_2_Bw2_amp   9.02084e+05  1.00000e+00     no limits
    57 f1525_2_Bw2_phi   1.05434e+01  1.00000e+01     no limits
    58 f1525_2_Bw3_amp   4.32373e+06  1.00000e+00     no limits
    59 f1525_2_Bw3_phi   8.58044e+02  1.00000e+01     no limits
    60 f1640_2_Bw1_amp   9.30495e+05  1.00000e+00     no limits
    61 f1640_2_Bw1_phi  -1.39287e+04  1.00000e+01     no limits
    62 f1640_2_Bw2_amp   9.42691e+05  1.00000e+00     no limits
    63 f1640_2_Bw2_phi   1.02407e+00  1.00000e+01     no limits
    64 f1640_2_Bw3_amp  -2.77895e+06  1.00000e+00     no limits
    65 f1640_2_Bw3_phi  -4.57691e+04  1.00000e+01     no limits
    66 f1750_2_Bw1_amp  -3.19530e+05  1.00000e+00     no limits
    67 f1750_2_Bw1_phi   2.06577e+04  1.00000e+00     no limits
    68 f1750_2_Bw2_amp  -6.31182e+05  1.00000e+01     no limits
    69 f1750_2_Bw2_phi  -3.99360e+00  1.00000e+00     no limits
    70 f1750_2_Bw3_amp  -2.75771e+06  1.00000e+01     no limits
    71 f1750_2_Bw3_phi   1.99648e+04  1.00000e+00     no limits
    72 f1950_2_Bw1_amp   1.65199e+06  1.00000e+01     no limits
    73 f1950_2_Bw1_phi  -6.09246e+02  1.00000e+00     no limits
    74 f1950_2_Bw2_amp   2.78342e+06  1.00000e+01     no limits
    75 f1950_2_Bw2_phi  -9.04063e+00  1.00000e+00     no limits
    76 f1950_2_Bw3_amp  -3.17711e+06  1.00000e+01     no limits
    77 f1950_2_Bw3_phi   7.45140e+04  1.00000e+00     no limits
    78 NR_Bw1_amp  -6.00234e+03  1.00000e+01     no limits
    79 NR_Bw1_phi   0.00000e+00  1.00000e+00     no limits
    80 n_BS_to_JPSI_K_K_par   0.00000e+00  1.00000e+01     no limits
    81 n_lambda     1.02843e+04  1.00000e+02     no limits
    82 n_BS_PIPI    5.88427e+03  1.00000e+02     no limits
    83 n_BD_PIPI    3.23785e+03  1.00000e+02     no limits
    84 n_BD_KPI     5.25428e+04  1.00000e+02     no limits
    85 n_BS_KK      1.30657e+04  1.00000e+02     no limits
    86 p0           6.97510e+00  1.00000e-02     no limits
    87 p1          -2.62652e+00  1.00000e-02     no limits
    88 p2          -3.43879e+00  1.00000e-02     no limits
    89 p3           2.05891e+00  1.00000e-02     no limits
    90 a0          -2.57666e-03  1.00000e-02     no limits
    91 b0           2.06412e-02  1.00000e-02     no limits
    92 b1           3.10727e-01  1.00000e-02     no limits
    93 b2          -1.40412e-01  1.00000e-02     no limits
    94 x0_KK        4.97820e+00  1.00000e-02     no limits
    95 d0_KK        1.73617e+00  1.00000e-02     no limits
    96 p0_KK        1.34847e+01  1.00000e-02     no limits
    97 p1_KK       -2.08538e+01  1.00000e-02     no limits
    98 p2_KK        2.39080e+01  1.00000e-02     no limits
    99 p3_KK       -1.36830e+01  1.00000e-02     no limits
   100 p4_KK        0.00000e+00  1.00000e-02     no limits
   101 p5_KK        0.00000e+00  1.00000e-02     no limits
   102 x0_pipi      4.58031e+00  1.00000e-02     no limits
   103 d0_pipi     -4.09883e-01  1.00000e-02     no limits
   104 p0_pipi     -1.86227e+00  1.00000e-02     no limits
   105 p1_pipi      7.22008e+01  1.00000e-02     no limits
   106 p2_pipi     -2.11225e+02  1.00000e-02     no limits
   107 p3_pipi      2.43135e+02  1.00000e-02     no limits
   108 p4_pipi     -9.87979e+01  1.00000e-02     no limits
   109 p5_pipi      0.00000e+00  1.00000e-02     no limits
   110 p6_pipi      0.00000e+00  1.00000e-02     no limits
   111 x0_pKKp     -2.60747e-01  1.00000e-02     no limits
   112 d0_pKKp      5.62156e+00  1.00000e-02     no limits
   113 p0_pKKp      1.26120e+01  1.00000e-02     no limits
   114 p1_pKKp     -1.96200e+01  1.00000e-02     no limits
   115 p2_pKKp      1.17711e+01  1.00000e-02     no limits
   116 p3_pKKp     -3.74706e+00  1.00000e-02     no limits
   117 a0_pKKp      4.79498e-02  1.00000e-02     no limits
   118 b0_pKKp      1.46887e-01  1.00000e-02     no limits
   119 b1_pKKp      4.53921e-01  1.00000e-02     no limits
   120 b2_pKKp     -7.56883e-02  1.00000e-02     no limits
   121 u0_pKKp      7.61864e+00  1.00000e-02     no limits
   122 Ls1800_12_Bw1_amp   4.08085e-06  1.00000e+01     no limits
   123 Ls1800_12_Bw1_phi   1.88496e+01  1.00000e+00     no limits
   124 Ls1800_12_Bw2_amp   0.00000e+00  1.00000e+01     no limits
   125 Ls1800_12_Bw2_phi   0.00000e+00  1.00000e+00     no limits
   126 Ls1800_12_Bw3_amp   0.00000e+00  1.00000e+01     no limits
   127 Ls1800_12_Bw3_phi   0.00000e+00  1.00000e+00     no limits
   128 Ls1800_12_Bw4_amp   0.00000e+00  1.00000e+01     no limits
   129 Ls1800_12_Bw4_phi   0.00000e+00  1.00000e+00     no limits
   130 Ls1810_12_Bw1_amp   7.06824e-06  1.00000e+01     no limits
   131 Ls1810_12_Bw1_phi  -1.24175e+01  1.00000e+00     no limits
   132 Ls1810_12_Bw2_amp   0.00000e+00  1.00000e+01     no limits
   133 Ls1810_12_Bw2_phi   0.00000e+00  1.00000e+00     no limits
   134 Ls1810_12_Bw3_amp   0.00000e+00  1.00000e+01     no limits
   135 Ls1810_12_Bw3_phi   0.00000e+00  1.00000e+00     no limits
   136 Ls1820_52_Bw1_amp   0.00000e+00  1.00000e+01     no limits
   137 Ls1820_52_Bw1_phi   0.00000e+00  1.00000e+00     no limits
   138 Ls1820_52_Bw2_amp   0.00000e+00  1.00000e+01     no limits
   139 Ls1820_52_Bw2_phi   0.00000e+00  1.00000e+00     no limits
   140 Ls1820_52_Bw3_amp   0.00000e+00  1.00000e+01     no limits
   141 Ls1820_52_Bw3_phi   0.00000e+00  1.00000e+00     no limits
   142 Ls1830_52_Bw1_amp   0.00000e+00  1.00000e+01     no limits
   143 Ls1830_52_Bw1_phi   0.00000e+00  1.00000e+00     no limits
   144 Ls1830_52_Bw2_amp   0.00000e+00  1.00000e+01     no limits
   145 Ls1830_52_Bw2_phi   0.00000e+00  1.00000e+00     no limits
   146 Ls1830_52_Bw3_amp   0.00000e+00  1.00000e+01     no limits
   147 Ls1830_52_Bw3_phi   0.00000e+00  1.00000e+00     no limits
   148 Ls1890_32_Bw1_amp   3.00990e+00  1.00000e+01     no limits
   149 Ls1890_32_Bw1_phi  -1.64289e+01  1.00000e+00     no limits
   150 Ls1890_32_Bw2_amp   0.00000e+00  1.00000e+01     no limits
   151 Ls1890_32_Bw2_phi   0.00000e+00  1.00000e+00     no limits
   152 Ls1890_32_Bw3_amp   0.00000e+00  1.00000e+01     no limits
   153 Ls1890_32_Bw3_phi   0.00000e+00  1.00000e+00     no limits
   154 Ls2020_72_Bw1_amp   0.00000e+00  1.00000e+01     no limits
   155 Ls2020_72_Bw1_phi   0.00000e+00  1.00000e+00     no limits
   156 Ls2020_72_Bw2_amp   0.00000e+00  1.00000e+01     no limits
   157 Ls2020_72_Bw2_phi   0.00000e+00  1.00000e+00     no limits
   158 Ls2020_72_Bw3_amp   0.00000e+00  1.00000e+01     no limits
   159 Ls2020_72_Bw3_phi   0.00000e+00  1.00000e+00     no limits
   160 Ls2050_32_Bw1_amp   0.00000e+00  1.00000e+01     no limits
   161 Ls2050_32_Bw1_phi   0.00000e+00  1.00000e+00     no limits
   162 Ls2050_32_Bw2_amp   0.00000e+00  1.00000e+01     no limits
   163 Ls2050_32_Bw2_phi   0.00000e+00  1.00000e+00     no limits
   164 Ls2050_32_Bw3_amp   0.00000e+00  1.00000e+01     no limits
   165 Ls2050_32_Bw3_phi   0.00000e+00  1.00000e+00     no limits
   166 Ls2100_72_Bw1_amp   2.31973e+02  1.00000e+01     no limits
   167 Ls2100_72_Bw1_phi   1.70533e+00  1.00000e+00     no limits
   168 Ls2100_72_Bw2_amp   0.00000e+00  1.00000e+01     no limits
   169 Ls2100_72_Bw2_phi   0.00000e+00  1.00000e+00     no limits
   170 Ls2100_72_Bw3_amp   0.00000e+00  1.00000e+01     no limits
   171 Ls2100_72_Bw3_phi   0.00000e+00  1.00000e+00     no limits
   172 Ls2110_52_Bw1_amp   2.53130e+01  1.00000e+01     no limits
   173 Ls2110_52_Bw1_phi  -1.32163e+00  1.00000e+00     no limits
   174 Ls2110_52_Bw2_amp   0.00000e+00  1.00000e+01     no limits
   175 Ls2110_52_Bw2_phi   0.00000e+00  1.00000e+00     no limits
   176 Ls2110_52_Bw3_amp   0.00000e+00  1.00000e+01     no limits
   177 Ls2110_52_Bw3_phi   0.00000e+00  1.00000e+00     no limits
   178 Ls2325_32_Bw1_amp   0.00000e+00  1.00000e+01     no limits
   179 Ls2325_32_Bw1_phi   0.00000e+00  1.00000e+00     no limits
   180 Ls2325_32_Bw2_amp   0.00000e+00  1.00000e+01     no limits
   181 Ls2325_32_Bw2_phi   0.00000e+00  1.00000e+00     no limits
   182 Ls2325_32_Bw3_amp   0.00000e+00  1.00000e+01     no limits
   183 Ls2325_32_Bw3_phi   0.00000e+00  1.00000e+00     no limits
   184 Ls2350_92_Bw1_amp   0.00000e+00  1.00000e+01     no limits
   185 Ls2350_92_Bw1_phi   0.00000e+00  1.00000e+00     no limits
   186 Ls2350_92_Bw2_amp   0.00000e+00  1.00000e+01     no limits
   187 Ls2350_92_Bw2_phi   0.00000e+00  1.00000e+00     no limits
   188 LsNR_12_Bw_amp   0.00000e+00  1.00000e+01     no limits
   189 LsNR_12_Bw_phi   0.00000e+00  1.00000e+00     no limits
   190 Pc1Bw1_amp   5.85192e-01  1.00000e+01     no limits
   191 Pc1Bw1_phi   0.00000e+00  1.00000e+00     no limits
   192 Pc1Bw2_amp   0.00000e+00  1.00000e+01     no limits
   193 Pc1Bw2_phi   0.00000e+00  1.00000e+00     no limits
   194 Pc1Bs1_amp   1.00000e+00  1.00000e+01     no limits
   195 Pc1Bs1_phi   0.00000e+00  1.00000e+00     no limits
   196 Pc1Bs2_amp   0.00000e+00  1.00000e+01     no limits
   197 Pc1Bs2_phi   0.00000e+00  1.00000e+00     no limits
   198 Pc1Bs3_amp   0.00000e+00  1.00000e+01     no limits
   199 Pc1Bs3_phi   0.00000e+00  1.00000e+00     no limits
   200 Pc2Bw1_amp   1.28457e+00  1.00000e+01     no limits
   201 Pc2Bw1_phi   2.83962e+00  1.00000e+00     no limits
   202 Pc2Bw2_amp   0.00000e+00  1.00000e+01     no limits
   203 Pc2Bw2_phi   0.00000e+00  1.00000e+00     no limits
   204 Pc2Bs1_amp   1.00000e+00  1.00000e+01     no limits
   205 Pc2Bs1_phi   0.00000e+00  1.00000e+00     no limits
   206 Pc2Bs2_amp   0.00000e+00  1.00000e+01     no limits
   207 Pc2Bs2_phi   0.00000e+00  1.00000e+00     no limits
   208 Pc2Bs3_amp   0.00000e+00  1.00000e+01     no limits
   209 Pc2Bs3_phi   0.00000e+00  1.00000e+00     no limits
   210 M01          4.28265e+00  1.00000e-02     no limits
   211 M02          4.45005e+00  1.00000e-02     no limits
   212 gamma1       1.41247e-01  1.00000e-02     no limits
   213 gamma2       4.88763e-02  1.00000e-02     no limits
   214 comb_bg_norm   5.76665e+04  1.00000e+02     no limits
 **********
 **    1 **FIX          86
 **********
 **********
 **    2 **FIX         104
 **********
 **********
 **    3 **FIX          96
 **********
 **********
 **    4 **FIX         113
 **********
 **********
 **    5 **SET ERRDEF         0.5
 **********

After fit statistics:

signal area:

n_BD_to_JPSI_K_PI	37421.1	0.693281
n_BS_to_JPSI_K_K	7591.96	0.140652
n_BS_to_JPSI_PI_PI	78.2097	0.00144895
n_BD_to_JPSI_PI_PI	587.337	0.0108813
n_LAMBDA0B_to_JPSI_P_K	4133.62	0.0765814
n_comb_bg	4164.56	0.0771548
describing:	1.00662

control area:

n_BD_to_JPSI_K_PI	14359.4
n_BS_to_JPSI_K_K	8285.67
n_BS_to_JPSI_PI_PI	3.65916
n_BD_to_JPSI_PI_PI	42.4697
n_LAMBDA0B_to_JPSI_P_K	2318.64
n_comb_bg	3730.67
describing:	1.01121

control area Lb:

n_BD_to_JPSI_K_PI	12440.2
n_BS_to_JPSI_K_K	4241.3
n_BS_to_JPSI_PI_PI	100.569
n_BD_to_JPSI_PI_PI	299.933
n_LAMBDA0B_to_JPSI_P_K	6392.61
n_comb_bg	5558.93

JpsiK_2d_K1410_1	1.28112e+06
JpsiK_2d_K1430_0	520462
JpsiK_2d_K1430_2	111348
JpsiK_2d_K1680_1	1.53133e+06
JpsiK_2d_K1780_3	261532
JpsiK_2d_K1950_0	451.826
JpsiK_2d_K1980_2	302209
JpsiK_2d_K2045_4	101276
JpsiK_2d_Zc4200		876649


JpsiK_2d_K1410_1	9614.35
JpsiK_2d_K1430_0	3905.89
JpsiK_2d_K1430_2	835.627
JpsiK_2d_K1680_1	11492.1
JpsiK_2d_K1780_3	1962.71
JpsiK_2d_K1950_0	3.39079
JpsiK_2d_K1980_2	2267.98
JpsiK_2d_K2045_4	760.04
JpsiK_2d_Zc4200		6578.94

nbins 	297

50657.2	50648

OLD:

OLD signal area:

n_BD_to_JPSI_K_PI	34599.4	0.68301
n_BS_to_JPSI_K_K	7256.3	0.143243
n_BS_to_JPSI_PI_PI	46.9232	0.000926289
n_BD_to_JPSI_PI_PI	265.736	0.00524576
n_LAMBDA0B_to_JPSI_P_K	3247.38	0.0641051
n_comb_bg	5241.5	0.10347
62
40

NEW:

NEW:

n_BD_to_JPSI_K_PI	37420.9	0.698867
n_BS_to_JPSI_K_K	7591.97	0.141786
n_BS_to_JPSI_PI_PI	78.2097	0.00146063
n_BD_to_JPSI_PI_PI	587.337	0.010969
n_LAMBDA0B_to_JPSI_P_K	4133.62	0.0771988
n_comb_bg	3733.05	0.0697179
pK_Kp_2d_bg	56558.5
pK_Kp_2d_BS_to_JPSI_PI_PI	5884.27
pK_Kp_2d_BD_to_JPSI_PI_PI	3237.85
pK_Kp_2d_BS_to_JPSI_K_K	13065.7
pK_Kp_2d_BD_to_JPSI_K_PI	52542.8
pK_Kp_2d_LAMBDA0B_to_JPSI_P_K	10284.3
Minimized function: 0


 Comb_bg Integral KpipiK:	57666.5


 Comb_bg Integral KK:	57666.5


 Comb_bg Integral pipi:	57666.5


 Comb_bg Integral pKKp:	56558.5


 Comb_bg Integral pKKp rotate:	56558.5


 Comb_bg_true : 	57661.1


_________________________________________________________

