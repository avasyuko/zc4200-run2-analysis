#include <stdio.h>
#include <time.h>
#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <TMath.h>
#include <vector>

#include <TROOT.h> 

#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TMinuit.h>
#include <TLatex.h>
#include <TRandom3.h> 
#include <thread>
#include <algorithm>
#include <THStack.h>
#include <TRandom.h>

#include <complex>
//#include "AtlasStyle.png"

#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TF1.h>

#include "TLorentzVector.h"
#include "TVector3.h"
#include "TVectorD.h"

#include "TMath.h"
//#include "Math/Vector3D.h"
//#include "Math/Vector4D.h"

#define MASS_JPSI   3.096900
#define MASS_MUON   0.1056583745
#define MASS_PION   0.13957061
#define MASS_KAON   0.493677
#define MASS_PROTON 0.938272081
#define MASS_LB     5.61960
#define MASS_BD     5.27963
#define MASS_BS     5.36689
#define MEV         1000.0
float GEV = 0.001;

Double_t PI = TMath::Pi();

int includeLs1810 = 1;   int extended1810 = 0;
	int includeLs1820 = 0;
	int includeLs1830 = 0;
	int includeLs1890 = 1;   int extended1890 = 0;
	int includeLs2020 = 0;
	int includeLs2050 = 0;
	int includeLs2100 = 1;
	int includeLs2110 = 1;
	int includeLs2325 = 0;
	int includeLs2350 = 0;
	int includeLs2385 = 0;

#define BW_nom_Lstar 0
#define BW_nom_Pc 0
#define Pc_32minus_52plus 1
#define Pc_32plus_52minus 0
#define Pc4_12minus_32minus_12minus_32minus 0
#define includeLsNRsimple 0
#define includeLsNR 0
#define Pc1_interference_off 0
#define Pc2_interference_off 0
#define Pc3_interference_off 0
#define Pc4_interference_off 0
#define includeKsNR 1

#define phs_Bd 148211
#define phs_Bs 35487
#define phs_Lb 12808
//#define phs_Lb 20

#define includeFNRsimple 1

//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__| fit modes START  |__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//

bool RARE_DECAYS = 1;
    
bool fit_mode = 1;
bool JpsipK_fit_mode = 0;
bool JpsipK_fit_mode_plus_JpsiKpi = 0;
bool JpsipK_fit_mode_comb_only = 0;
bool Jpsipipi_fit_mode = 0;
bool Jpsipipi_fit_mode_plus_JpsiKpi = 0;
bool Jpsipipi_fit_mode_plus_JpsiKpi_lbfix = 0;
bool Jpsipipi_fit_mode_comb_only = 0;
bool JpsiKK_fit_mode = 0;
bool JpsiKK_fit_mode_plus_JpsiKpi = 0;
bool JpsiKK_fit_mode_plus_JpsiKpi_lbpipi_fix = 0;
bool JpsiKK_fit_mode_no_control = 0;
bool JpsiKK_fit_mode_plus_JpsiKpi_no_control_lbpipi_fix = 0;
bool JpsiKK_fit_mode_comb_only = 0;
bool JpsiKpi_fit_mode = 0;
bool JpsiKpi_fit_mode_no_signal = 0;
bool JpsiKpi_fit_mode_comb_only = 0;
bool signal_area_fit_mode = 0;
bool signal_area_fit_mode_short = 0;
bool global_area_no_comb_form_4tr = 0;
bool signal_area_fit_mode_short_noZc = 0;
bool signal_area_fit_mode_noZc = 0;
bool signal_area_fit_mode_onlyZc = 0;
bool Jpsippi_fit_mode_comb_only = 0;
Int_t MODEL;

//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__| fit modes FINISH |__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//



//#define THREADS 4

//#define GEV 0.001

int THREADS;

using namespace std;

void ov_uf_bins_Clear(TH2 *hist)
{
    Int_t hist_nbinsX = hist->ProjectionX()->GetNbinsX();
    Int_t hist_nbinsY = hist->ProjectionY()->GetNbinsX();
    
    for(int i = 0; i <= hist_nbinsX + 1; i++)
    {
        hist->SetBinContent(i, 0, 0.);
        hist->SetBinContent(i, hist_nbinsY + 1, 0.);
    }
    
    for(int i = 0; i <= hist_nbinsY + 1; i++)
    {
        hist->SetBinContent(0, i, 0.);
        hist->SetBinContent(hist_nbinsX + 1, i, 0.);
    }
}

/*std::complex<float> breit_wigner(float m0, float m, float g0, int l, float d, float p, float p0) //counts relativistic Breit-Wigner amplitude
{
	std::complex<float> z1(m * sqrt(m0 * G_width(p, p0, d, l, m0, m, g0)), 0);
	std::complex<float> z2(m0 * m0 - m * m, -1. * G_width(p, p0, d, l, m0, m, g0) * m0);
	std::complex<float> r;
	r = z1 / z2;
	//cout << "z2 = " << z2 << endl;
	return r;
}*/

float Wigner_matrix2(float a, float  b, float c, double fi) //counts Wigner d-matrix(0.5; 1; 1.5; 2.5 (not full b = 1.5|0.5|-0.5|-1.5); 3.5(not full b = 1.5|0.5|-0.5|-1.5))
{
	int t;
	float f = 0.;
	switch (t = (int)nearbyint(a * 2))
	{
	case 0:
	{	
		switch (t = (int)nearbyint(2 * b))
			  {
			  case 0:
			  {
						switch (t = (int)nearbyint(c * 2))
						{
						case 0:
						{
							f = 1.;
								   break;
						}
						}
						break;
			  }
			  }
			break;	
	}
	case 1:
	{
			  switch (t = (int)nearbyint(2 * b))
			  {
			  case 1:
			  {
						switch (t = (int)nearbyint(c * 2))
						{
						case -1:
						{
							f = -sin(fi / 2.);
								   break;
						}
						case 1:
						{
							f = cos(fi / 2.);
								  break;
						}
						default:
						{
													
							if ((f == 0) && (b != c)) { f = (float)pow(-1, b - c) * Wigner_matrix2(a, c, b, fi); }
							if (f == 0) { f = (float)pow(-1, b - c) * Wigner_matrix2(a, -b, -c, fi); }
							if (f == 0) { f = Wigner_matrix2(a, -c, -b, fi); }	
						}
						}
						break;
			  }
			  default:
			  {					
					if ((f == 0) && (b != c)) { f = (float)pow(-1, b - c) * Wigner_matrix2(a, c, b, fi); }
					if (f == 0) { f = (float)pow(-1, b - c) * Wigner_matrix2(a, -b, -c, fi); }
					if (f == 0) { f = Wigner_matrix2(a, -c, -b, fi); }		
			  }
			  }
			  break;
	}
	case 2:
	{
			  switch (t = (int)nearbyint(b))
			  {
			  case -1:
			  {
						switch (t = (int)nearbyint(c * 2))
						{
						case -2:
						{
								  f = (1. + cos(fi)) / 2.;
								  break;
						}
						case 0:
						{
								  f = -sin(fi) / sqrt(2.);
								  break;
						}
						case 2:
						{
								  f = (1. - cos(fi)) / 2.;
								  break;
						}
						default:
						{
													
							if ((f == 0) && (b != c)) { f = (float)pow(-1, b - c) * Wigner_matrix2(a, c, b, fi); }
							if (f == 0) { f = (float)pow(-1, b - c) * Wigner_matrix2(a, -b, -c, fi); }
							if (f == 0) { f = Wigner_matrix2(a, -c, -b, fi); }	
						}
						}
						break;

			  }
			  case 0:
			  {
						switch (t = (int)nearbyint(c * 2))
						{
						case -2:
						{
								  f = -sin(fi) / sqrt(2.);
								  break;
						}
						case 0:
						{
								  f = cos(fi);
								  break;
						}
						case 2:
						{
								  f = sin(fi) / sqrt(2.);
								  break;
						}
						default:
						{
													
							if ((f == 0) && (b != c)) { f = (float)pow(-1, b - c) * Wigner_matrix2(a, c, b, fi); }
							if (f == 0) { f = (float)pow(-1, b - c) * Wigner_matrix2(a, -b, -c, fi); }
							if (f == 0) { f = Wigner_matrix2(a, -c, -b, fi); }	
						}
						}
						break;
			  }
			  case 1:
			  {
						switch (t = (int)nearbyint(c * 2))
						{
						case -2:
						{
								   f = (1. - cos(fi)) / 2.;
								   break;
						}
						case 0:
						{
								  f = -sin(fi) / sqrt(2.);
								  break;
						}
						case 2:
						{
								  f = (1. + cos(fi)) / 2.;
								  break;
						}
						default:
						{
													
							if ((f == 0) && (b != c)) { f = (float)pow(-1, b - c) * Wigner_matrix2(a, c, b, fi); }
							if (f == 0) { f = (float)pow(-1, b - c) * Wigner_matrix2(a, -b, -c, fi); }
							if (f == 0) { f = Wigner_matrix2(a, -c, -b, fi); }	
						}
						}
						break;
			  }
				default:
						{
													
							if ((f == 0) && (b != c)) { f = (float)pow(-1, b - c) * Wigner_matrix2(a, c, b, fi); }
							if (f == 0) { f = (float)pow(-1, b - c) * Wigner_matrix2(a, -b, -c, fi); }
							if (f == 0) { f = Wigner_matrix2(a, -c, -b, fi); }	
						}
			  }
			  break;
	}
	case 4:
	{
			switch (t = (int)nearbyint(b))
			  {
			  case 0:
			  {
						switch (t = (int)nearbyint(c * 2))
						{
						case -2:
						{
								  f = -sqrt(6.) / 3. * pow(cos(fi / 2.), 5.) * pow(sin(fi / 2.), 1.) + sqrt(6.) / 2. * pow(cos(fi / 2.), 3.) * pow(sin(fi / 2.), 3.);
								  break;
						}
						case 0:
						{
								  f = pow(cos(fi / 2.), 4.);
								  break;
						}
						case 2:
						{
								  f = sqrt(6.) / 3. * pow(cos(fi / 2.), 5.) * pow(sin(fi / 2.), 1.) - sqrt(6.) / 2. * pow(cos(fi / 2.), 3.) * pow(sin(fi / 2.), 3.);
								  break;
						}
						}
						break;
			  }
			}
			break;	
	}
	case 6:
	{
			switch (t = (int)nearbyint(b))
			  {
			  case 0:
			  {
						switch (t = (int)nearbyint(c * 2))
						{
						case -2:
						{
								  f = - 2. * sqrt(3.) * pow(cos(fi / 2.), 7.) * pow(sin(fi / 2.), 1.) + 6 * sqrt(3.) * pow(cos(fi / 2.), 5.) * pow(sin(fi / 2.), 3.) - 2. * sqrt(3.) * pow(cos(fi / 2.), 3.) * pow(sin(fi / 2.), 5.);
								  break;
						}
						case 0:
						{
								  f = pow(cos(fi / 2.), 6.);
								  break;
						}
						case 2:
						{
								  f = 2. * sqrt(3.) * pow(cos(fi / 2.), 7.) * pow(sin(fi / 2.), 1.) - 6 * sqrt(3.) * pow(cos(fi / 2.), 5.) * pow(sin(fi / 2.), 3.) + 2. * sqrt(3.) * pow(cos(fi / 2.), 3.) * pow(sin(fi / 2.), 5.);
								  break;
						}
						}
						break;
			  }
			}
			break;	
	}

	default:
	{
		//cout << "forbidden value a = " << a << endl;;
	}
	}
	//cout << "Wigner_matrix = " << f << endl;
	return f;
}

float Wigner_matrix(float J, float m1, float m2, float theta)
{

//// J is spin*2
//// m1 is m*2
//// m2 is m'*2

float costheta = cos(theta);
float sintheta = sin(theta);

float costheta_2 = cos(theta/2.0);
float sintheta_2 = sin(theta/2.0);

float cos2theta = cos(2.0*theta);
float sin2theta = sin(2.0*theta);


if(J == 0.0) ////
{
	if((m1 == 0.0 && m2 == 0.0))
		return costheta_2;
}
else
if(J == 0.5) ////
{
	if((m1 == 0.5 && m2 == 0.5) || (m1 == -0.5 && m2 == -0.5))
		return costheta_2;
	if( m1 == 0.5 && m2 == -0.5 )
		return -sintheta_2;
	if( m1 == -0.5 && m2 == 0.5 )
		return sintheta_2;
}
else
if(J == 1.0) ////
{
	if((m1 == 1.0 && m2 == 1.0) || (m1 == -1.0 && m2 == -1.0))
		return (1 + costheta)/2.0;
	if((m1 == 1.0 && m2 == -1.0) || (m1 == -1.0 && m2 == 1.0))
		return (1 - costheta)/2.0;
	if((m1 == 1.0 && m2 == 0.0) || (m1 == 0.0 && m2 == -1.0))
		return -sintheta/sqrt(2.0);
	if((m1 == -1.0 && m2 == 0.0) || (m1 == 0.0 && m2 == 1.0))
		return sintheta/sqrt(2.0);
	if((m1 == 0.0 && m2 == 0.0))
		return costheta;
}
else
if(J == 1.5) ////
{
	if((m1 == 0.5 && m2 == 0.5) || (m1 == -0.5 && m2 == -0.5))
		return (3.0*costheta - 1)*costheta_2/2.0;
	if((m1 == 0.5 && m2 == -0.5))
		return -(3.0*costheta + 1)*sintheta_2/2.0;
	if((m1 == -0.5 && m2 == 0.5))
		return (3.0*costheta + 1)*sintheta_2/2.0;
	if((m1 == 0.5 && m2 == 1.5) || (m1 == -1.5 && m2 == -0.5))
		return sqrt(3.0)*(costheta + 1)*sintheta_2/2.0;
	if((m1 == -0.5 && m2 == -1.5) )
		return -sqrt(3.0)*(costheta + 1)*sintheta_2/2.0;
	if((m1 == 0.5 && m2 == -1.5) || (m1 == 1.5 && m2 == -0.5) || (m1 == -0.5 && m2 == 1.5) )
		return sqrt(3.0)*(1 - costheta)*costheta_2/2.0;
}
else
if(J == 2.5) ////
{
	if((m1 == 0.5 && m2 == 1.5))
		return sqrt(2.0)*(-0.25 + 1.25*costheta*costheta + costheta)*sintheta_2;
	if((m1 == -0.5 && m2 == -1.5))
		return -sqrt(2.0)*(-0.25 + 1.25*costheta*costheta + costheta)*sintheta_2;
	if((m1 == 0.5 && m2 == -1.5) || (m1 == -0.5 && m2 == 1.5))
		return sqrt(2.0)*(0.25 - 1.25*costheta*costheta + costheta)*costheta_2;
	if((m1 == 0.5 && m2 == 0.5) || (m1 == -0.5 && m2 == -0.5))
		return pow(costheta_2, 5.0) - 6.0*pow(costheta_2, 3.0)*pow(sintheta_2, 2.0) + 3.0*pow(sintheta_2, 4.0)*costheta_2;
	if((m1 == 0.5 && m2 == -0.5))
		return -3.0*pow(costheta_2, 4.0)*sintheta_2 + 6.0*pow(costheta_2, 2.0)*pow(sintheta_2, 3.0) - pow(sintheta_2, 5.0);
	if((m1 == -0.5 && m2 == 0.5))
		return 3.0*pow(costheta_2, 4.0)*sintheta_2 - 6.0*pow(costheta_2, 2.0)*pow(sintheta_2, 3.0) + pow(sintheta_2, 5.0);
}
else
if(J == 2.0) //// 
{
	if((m1 == 0.0 && m2 == 0.0))
		return 1.0/2.0*(3.0*costheta*costheta - 1.0);
	if((m1 == 1.0 && m2 == 0.0) || (m1 == 0.0 && m2 == -1.0))
		return -sqrt(3.0/8.0)*sin2theta;
	if((m1 == 0.0 && m2 == 1.0) || (m1 == -1.0 && m2 == 0.0))
		return sqrt(3.0/8.0)*sin2theta;
	if((m1 == 1.0 && m2 == -1.0) || (m1 == -1.0 && m2 == 1.0))
		return 1.0/2.0*(-2.0*costheta*costheta - costheta + 1.0);


}
else
if(J == 3.0) ////
{
	if((m1 == 0.0 && m2 == 0.0))
		return -1.5*costheta + 2.5*costheta*costheta*costheta;
	if((m1 == 1.0 && m2 == 0.0) || (m1 == 0.0 && m2 == -1.0))
		return sqrt(3.0)/4.0*sintheta - 5.0/4.0*sqrt(3.0)*sintheta*costheta*costheta;
	if((m1 == 0.0 && m2 == 1.0) || (m1 == -1.0 && m2 == 0.0))
		return -sqrt(3.0)/4.0*sintheta + 5.0/4.0*sqrt(3.0)*sintheta*costheta*costheta;
}
else
if(J == 3.5) ////
{
	if((m1 == 0.5 && m2 == 0.5) || (m1 == -0.5 && m2 == -0.5) )
		return pow(costheta_2, 7.0) - 12.0*pow(costheta_2, 5.0)*pow(sintheta_2, 2.0) + 18.0*pow(costheta_2, 3.0)*pow(sintheta_2, 4.0) - 4.0*costheta_2*pow(sintheta_2, 6.0);
	if((m1 == 0.5 && m2 == -0.5) )
		return -pow(costheta_2, 6.0)*sintheta_2 + 6.0*pow(costheta_2, 4.0)*pow(sintheta_2, 3.0) - 6.0*pow(costheta_2, 2.0)*pow(sintheta_2, 5.0) + pow(sintheta_2, 7.0);
	if((m1 == -0.5 && m2 == 0.5) )
		return pow(costheta_2, 6.0)*sintheta_2 - 6.0*pow(costheta_2, 4.0)*pow(sintheta_2, 3.0) + 6.0*pow(costheta_2, 2.0)*pow(sintheta_2, 5.0) - pow(sintheta_2, 7.0);


	if((m1 == 1.5 && m2 == 0.5))
		return sqrt(15.0)*( -pow(costheta_2, 6.0)*sintheta_2 + 4.0*pow(costheta_2, 4.0)*pow(sintheta_2, 3.0) - 2.0*pow(costheta_2, 2.0)*pow(sintheta_2, 5.0) );
	if((m1 == -1.5 && m2 == -0.5))
		return -sqrt(15.0)*( -pow(costheta_2, 6.0)*sintheta_2 + 4.0*pow(costheta_2, 4.0)*pow(sintheta_2, 3.0) - 2.0*pow(costheta_2, 2.0)*pow(sintheta_2, 5.0) );

	if((m1 == 1.5 && m2 == -0.5) || (m1 == -1.5 && m2 == 0.5))
		return sqrt(15.0)*( 2.0*pow(costheta_2, 5.0)*pow(sintheta_2, 2.0) - 4.0*pow(costheta_2, 3.0)*pow(sintheta_2, 4.0) + costheta_2*pow(sintheta_2, 6.0) );
}
else
if(J == 4.0) ////
{
	if((m1 == 0.0 && m2 == 0.0))
		return pow((1.0+costheta)/2.0, 4.0) + pow((1.0-costheta)/2.0, 4.0) - 4.0*sintheta*sintheta*pow((1.0+costheta)/2.0, 2.0)
			+9.0/4.0*pow(sintheta, 4.0) - 4.0*sintheta*sintheta*pow((1.0-costheta)/2.0, 2.0);
	

	if((m1 == 1.0 && m2 == 0.0) || (m1 == 0.0 && m2 == -1.0))
		return -1.0/sqrt(5.0)*sintheta*pow((1.0+costheta)/2.0, 3.0) + 3.0*sqrt(5.0)/8.0*sintheta*sintheta*sintheta*(1.0+costheta)/2.0
			-sqrt(5.0)/2.0*sintheta*sintheta*sintheta*(1.0-costheta)/2.0 + sqrt(5.0)/2.0*sintheta*pow((1.0-costheta)/2.0, 3.0);
	if((m1 == 0.0 && m2 == 1.0) || (m1 == -1.0 && m2 == 0.0))
		return 1.0/sqrt(5.0)*sintheta*pow((1.0+costheta)/2.0, 3.0) - 3.0*sqrt(5.0)/8.0*sintheta*sintheta*sintheta*(1.0+costheta)/2.0
			+sqrt(5.0)/2.0*sintheta*sintheta*sintheta*(1.0-costheta)/2.0 - sqrt(5.0)/2.0*sintheta*pow((1.0-costheta)/2.0, 3.0);
	
}
if(J == 4.5) ////
{
	if((m1 == 0.5 && m2 == 0.5) || (m1 == -0.5 && m2 == -0.5) )
		return pow(costheta_2, 9.0) - 20.0*pow(costheta_2, 7.0)*pow(sintheta_2, 2.0) + 60.0*pow(costheta_2, 5.0)*pow(sintheta_2, 4.0) - 120.0*pow(costheta_2, 3.0)*pow(sintheta_2, 6.0) + 5.0*costheta_2*pow(sintheta_2, 8.0);
	if((m1 == 0.5 && m2 == -0.5) )
		return -5.0*pow(costheta_2, 8.0)*sintheta_2 + 120.0*pow(costheta_2, 6.0)*pow(sintheta_2, 3.0) - 60.0*pow(costheta_2, 4.0)*pow(sintheta_2, 5.0) + 20.0*pow(costheta_2, 2.0)*pow(sintheta_2, 7.0) - pow(sintheta_2, 9.0);
	if((m1 == -0.5 && m2 == 0.5) )
		return 5.0*pow(costheta_2, 8.0)*sintheta_2 - 120.0*pow(costheta_2, 6.0)*pow(sintheta_2, 3.0) + 60.0*pow(costheta_2, 4.0)*pow(sintheta_2, 5.0) - 20.0*pow(costheta_2, 2.0)*pow(sintheta_2, 7.0) + pow(sintheta_2, 9.0);


	if((m1 == 1.5 && m2 == 0.5) )
		return sqrt(6.0)*( -2.0*pow(costheta_2, 8.0)*sintheta_2 + 7.5*pow(costheta_2, 6.0)*pow(sintheta_2, 3.0) - 20.0*pow(costheta_2, 4.0)*pow(sintheta_2, 5.0) + 5.0*pow(costheta_2, 2.0)*pow(sintheta_2, 7.0)  );
	if((m1 == -1.5 && m2 == -0.5) )
		return sqrt(6.0)*( 2.0*pow(costheta_2, 8.0)*sintheta_2 - 7.5*pow(costheta_2, 6.0)*pow(sintheta_2, 3.0) + 20.0*pow(costheta_2, 4.0)*pow(sintheta_2, 5.0) - 5.0*pow(costheta_2, 2.0)*pow(sintheta_2, 7.0)  );

	if((m1 == 1.5 && m2 == -0.5) || (m1 == 1.5 && m2 == -0.5) )
		return sqrt(6.0)*( 5.0*pow(costheta_2, 7.0)*pow(sintheta_2, 2.0) -20*pow(costheta_2, 5.0)*pow(sintheta_2, 4.0) +7.5*pow(costheta_2, 3.0)*pow(sintheta_2, 6.0) - 2.0*costheta_2*pow(sintheta_2, 8.0)  );


}



return 0.0;

}

std::complex<float> make_complex(float a, float p) //this function construct complex type using amplitude and phase
{
    return std::complex<float>(a * cos(p), a * sin(p));
}

double BlattWeisskopf(double p0, double p1, double d, int L)
{

	if(L==0) return 1.0;

	float c1 = p0*d*p0*d;
	float c2 = p1*d*p1*d;
	
	if(L==1) return sqrt((1 + c1)/(1 + c2));
	
	if(L==2) return sqrt((9.0 + 3.0*c1 + c1*c1)/(9.0 + 3.0*c2 + c2*c2));
	
	if(L==3) return sqrt((225.0 + 45.0*c1 + 6.0*c1*c1 + c1*c1*c1)/(225.0 + 45.0*c2 + 6.0*c2*c2 + c2*c2*c2));
	
	float c14 = c1*c1*c1*c1;
	float c24 = c2*c2*c2*c2;
		
	if(L==4) return sqrt(   ( 11025.0 + 1575.0*c1 + 135.0*c1*c1 + 10.0*c1*c1*c1 + c14 )/
				( 11025.0 + 1575.0*c2 + 135.0*c2*c2 + 10.0*c1*c1*c1 + c24 ) );
	
	if(L==5) return sqrt(   (893025.0 + 99225.0*c1 + 6300.0*c1*c1 + 315.0*c1*c1*c1 + 15.0*c14 + c14*c1)/
			        (893025.0 + 99225.0*c2 + 6300.0*c2*c2 + 315.0*c2*c2*c2 + 15.0*c24 + c24*c2) );
	return 0.0;


}

std::complex<float> BreitWignerX( float MX, float massB, float M0X, float gamma0, int LX )
{
std::complex<float> ix(0,1);
const float massProton = 0.938272;
const float massJpsi = 3.096916;
const float massKaon = 0.493667;
const float massPion = 0.13957;
//const float massLambdaB = MASS_LB;
//massB = MASS_BD;
float mplusX = massPion + massJpsi; /// mass JPsi + mass proton
float spinX = 1.;
//if(M0X < mplusX) return 0.0;
//if(MX < mplusX)  return 0.0;
//float q0 = sqrt(M0X*M0X - mplusX*mplusX); 
//float q1 = sqrt(MX*MX - mplusX*mplusX);


float argq0 = pow( M0X*M0X - massJpsi*massJpsi - massPion*massPion, 2.0 ) - 4.0 * massJpsi*massJpsi*massPion*massPion;
float argq1 = pow( MX*MX - massJpsi*massJpsi - massPion*massPion, 2.0 ) - 4.0 * massJpsi*massJpsi*massPion*massPion;
if(argq0 < 0.0 || argq1 < 0.0) return 0.0;
float q0 = sqrt(argq0)/2.0/M0X;
float q1 = sqrt(argq1)/2.0/MX;


float argp0 = pow( massB*massB - M0X*M0X - massKaon*massKaon, 2.0 ) - 4.0 * M0X*M0X*massKaon*massKaon;
float argp1 = pow( massB*massB - MX*MX - massKaon*massKaon, 2.0 ) - 4.0 * MX*MX*massKaon*massKaon;
if(argp0 < 0.0 || argp1 < 0.0) return 0.0;
float p0 = sqrt(argp0)/2.0/massB;
float p1 = sqrt(argp1)/2.0/massB;

float gamma = gamma0 * pow( q1/q0, 2*LX + 1) * M0X / MX * powf(BlattWeisskopf(q0, q1, 3.0, LX), 2.0);

float nom = 1.0;
//if(BW_nom_X==1) nom = MX*sqrt(M0X*gamma); //commented
std::complex<float> denom = nom/( M0X*M0X - MX*MX - ix*M0X*gamma );
///std::complex<float> Rm = denom * BlattWeisskopf(q0, q1, 3.0, LX) * pow( q1/M0X, LX );

const int LB = spinX;
std::complex<float> Rm = (float)(BlattWeisskopf(p0, p1, 3.0, LB) * pow( p1/massB, LB )) * denom * (float)(BlattWeisskopf(q0, q1, 3.0, LX) * pow( q1/M0X, LX ));

return Rm;
}

std::complex<float> BreitWignerX1( float MX, float massB, float M0X, float gamma0, int LX )
{
std::complex<float> ix(0,1);
const float massProton = 0.938272;
const float massJpsi = 3.096916;
const float massKaon = 0.493667;
const float massPion = 0.13957;
//const float massLambdaB = MASS_LB;
//massB = MASS_BD;
float mplusX = massPion + massJpsi; /// mass JPsi + mass proton
float spinX = 1.;
//if(M0X < mplusX) return 0.0;
//if(MX < mplusX)  return 0.0;
//float q0 = sqrt(M0X*M0X - mplusX*mplusX); 
//float q1 = sqrt(MX*MX - mplusX*mplusX);


float argq0 = pow( M0X*M0X - massJpsi*massJpsi - massPion*massPion, 2.0 ) - 4.0 * massJpsi*massJpsi*massPion*massPion;
float argq1 = pow( MX*MX - massJpsi*massJpsi - massPion*massPion, 2.0 ) - 4.0 * massJpsi*massJpsi*massPion*massPion;
if(argq0 < 0.0 || argq1 < 0.0) return 0.0;
float q0 = sqrt(argq0)/2.0/M0X;
float q1 = sqrt(argq1)/2.0/MX;


float argp0 = pow( massB*massB - M0X*M0X - massKaon*massKaon, 2.0 ) - 4.0 * M0X*M0X*massKaon*massKaon;
float argp1 = pow( massB*massB - MX*MX - massKaon*massKaon, 2.0 ) - 4.0 * MX*MX*massKaon*massKaon;
if(argp0 < 0.0 || argp1 < 0.0) return 0.0;
float p0 = sqrt(argp0)/2.0/massB;
float p1 = sqrt(argp1)/2.0/massB;

float gamma = gamma0 * pow( q1/q0, 2*LX + 1) * M0X / MX * powf(BlattWeisskopf(q0, q1, 3.0, LX), 2.0);

float nom = 1.0;
//if(BW_nom_X==1) nom = MX*sqrt(M0X*gamma); //commented
std::complex<float> denom = nom/( M0X*M0X - MX*MX - ix*M0X*gamma );
///std::complex<float> Rm = denom * BlattWeisskopf(q0, q1, 3.0, LX) * pow( q1/M0X, LX );

const int LB = spinX;
std::complex<float> Rm = (float)(BlattWeisskopf(p0, p1, 3.0, LB) * pow( p1/massB, LB )) * denom * (float)(BlattWeisskopf(q0, q1, 3.0, LX) * pow( q1/M0X, LX ));

return Rm;
}

std::complex<float> BreitWignerX2( float MX, float massB, float M0X, float gamma0, int LX )
{
std::complex<float> ix(0,1);
const float massProton = 0.938272;
const float massJpsi = 3.096916;
const float massKaon = 0.493667;
const float massPion = 0.13957;
//const float massLambdaB = MASS_LB;
//massB = MASS_BD;
float mplusX = massPion + massJpsi; /// mass JPsi + mass proton
float spinX = 2.;
//if(M0X < mplusX) return 0.0;
//if(MX < mplusX)  return 0.0;
//float q0 = sqrt(M0X*M0X - mplusX*mplusX); 
//float q1 = sqrt(MX*MX - mplusX*mplusX);


float argq0 = pow( M0X*M0X - massJpsi*massJpsi - massPion*massPion, 2.0 ) - 4.0 * massJpsi*massJpsi*massPion*massPion;
float argq1 = pow( MX*MX - massJpsi*massJpsi - massPion*massPion, 2.0 ) - 4.0 * massJpsi*massJpsi*massPion*massPion;
if(argq0 < 0.0 || argq1 < 0.0) return 0.0;
float q0 = sqrt(argq0)/2.0/M0X;
float q1 = sqrt(argq1)/2.0/MX;


float argp0 = pow( massB*massB - M0X*M0X - massKaon*massKaon, 2.0 ) - 4.0 * M0X*M0X*massKaon*massKaon;
float argp1 = pow( massB*massB - MX*MX - massKaon*massKaon, 2.0 ) - 4.0 * MX*MX*massKaon*massKaon;
if(argp0 < 0.0 || argp1 < 0.0) return 0.0;
float p0 = sqrt(argp0)/2.0/massB;
float p1 = sqrt(argp1)/2.0/massB;

float gamma = gamma0 * pow( q1/q0, 2*LX + 1) * M0X / MX * powf(BlattWeisskopf(q0, q1, 3.0, LX), 2.0);

float nom = 1.0;
//if(BW_nom_X==1) nom = MX*sqrt(M0X*gamma); //commented
std::complex<float> denom = nom/( M0X*M0X - MX*MX - ix*M0X*gamma );
///std::complex<float> Rm = denom * BlattWeisskopf(q0, q1, 3.0, LX) * pow( q1/M0X, LX );

const int LB = spinX;
std::complex<float> Rm = (float)(BlattWeisskopf(p0, p1, 3.0, LB) * pow( p1/massB, LB )) * denom * (float)(BlattWeisskopf(q0, q1, 3.0, LX) * pow( q1/M0X, LX ));

return Rm;
}

std::complex<float> BreitWignerKstar( float MK, float M0K, float massB, float gamma0, int LK, int LB )
{
std::complex<float> ix(0,1);
const float massProton = 0.938272;
const float massJpsi = 3.096916;
const float massKaon = 0.493667;
const float massPion = 0.13957;
//const float massLambdaB = MASS_LB;
//const float massB = MASS_BD;
const float mplusK = massPion + massKaon; /// mass JPsi + mass proton

//massB = MASS_BD;

float argq0 = pow( M0K*M0K - massPion*massPion - massKaon*massKaon, 2.0 ) - 4.0 * massPion*massPion*massKaon*massKaon;
float argq1 = pow( MK*MK - massPion*massPion - massKaon*massKaon, 2.0 ) - 4.0 * massPion*massPion*massKaon*massKaon;
if(argq0 < 0.0 || argq1 < 0.0) return 0.0;
float q0 = sqrt(argq0)/2.0/M0K;
float q1 = sqrt(argq1)/2.0/MK;


float argp0 = pow( massB*massB - M0K*M0K - massJpsi*massJpsi, 2.0 ) - 4.0 * M0K*M0K*massJpsi*massJpsi;
float argp1 = pow( massB*massB - MK*MK - massJpsi*massJpsi, 2.0 ) - 4.0 * MK*MK*massJpsi*massJpsi;
if(argp0 < 0.0 || argp1 < 0.0) return 0.0;
float p0 = sqrt(argp0)/2.0/massB;
float p1 = sqrt(argp1)/2.0/massB;


float gamma = gamma0 * pow( q1/q0, 2*LK + 1) * M0K / MK * powf(BlattWeisskopf(q0, q1, 3.0, LK), 2.0);

if(gamma != gamma ){printf("in BWkstar gamma is nan"); getchar();}

float nom = 1.0;
//if(BW_nom_Kstar==1) nom = MK*sqrt(M0K*gamma); //commented
std::complex<float> denom = nom/( M0K*M0K - MK*MK - ix*M0K*gamma );

float BlWsLB = BlattWeisskopf(p0, p1, 3.0, LB);
float BlWsLK = BlattWeisskopf(q0, q1, 3.0, LK);

if(BlWsLB != BlWsLB) { printf("blatt LB is NaN, p0=%10.2e\tp1=%10.2e\tLB=%d", p0, p1, LB); getchar(); }
if(BlWsLK != BlWsLK) { printf("blatt LK is NaN, q0=%10.2e\tq1=%10.2e\tLK=%d", q0, q1, LK); getchar(); }

std::complex<float> Rm = (float)(BlWsLB*pow( p1/massB, LB ))*denom*(float)(BlWsLK*pow( q1/M0K, LK ));

/*
if(Rm != Rm){
printf("\nBW: %10.2e ", abs(denom));
printf("%10.1e ", BlattWeisskopf(p0, p1, 3.0, LB));
printf("%10.1e ", BlattWeisskopf(q0, q1, 3.0, LK));
printf("%10.1e ", pow( p1/massB, LB ) );
printf("%10.1e ", pow( q1/M0K, LK ) );
printf("%10.4e ", massB );
printf("%10.4e ", mplusB1 );
printf("%10.4e ", MK );
getchar();
}*/

return Rm;
}

std::complex<float> HamplitudeX0minus(float M, float massB, float M0, float gamma, std::complex<float> Bs1, std::complex<float> Bs2, float hmu, float thetaX, float thetaPsi, float phimu, float alpha_mu )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);

std::complex<float> ealpha = cosf(hmu*alpha_mu) + ix * sinf(hmu*alpha_mu);

std::complex<float> Hs1, Hs2, Hs3;

std::complex<float> Ampl1;
Ampl1 = 0.0;

float spinX = 0.;
int parityX = -1;

/////spinX 2
	if(spinX == 0 && parityX == -1){
	Hs1 = -Bs1*BreitWignerX( M, massB, M0, gamma, 1);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi);		

	}

	if(spinX == 1 && parityX == 1){
	Hs1 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerX1( M, massB, M0, gamma, 0) - (float)(sqrt(2.0/3.0))*Bs2*BreitWignerX1( M, massB, M0, gamma, 2);
	Hs2 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerX1( M, massB, M0, gamma, 0) + (float)(sqrt(1.0/6.0))*Bs2*BreitWignerX1( M, massB, M0, gamma, 2);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0,-1.0, thetaX) * emu2;

	}
	if(spinX == 2 && parityX == -1){
	Hs1 = (float)(sqrt(2.0/5.0))*Bs1*BreitWignerX2( M, massB, M0, gamma, 1) - (float)(sqrt(3.0/5.0))*Bs2*BreitWignerX2( M, massB, M0, gamma, 3);
	Hs2 = (float)(sqrt(3.0/10.0))*Bs1*BreitWignerX2( M, massB, M0, gamma, 1) + (float)(sqrt(1.0/5.0))*Bs2*BreitWignerX2( M, massB, M0, gamma, 3);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}
	
	if(spinX == 2 && parityX == 1){
	Hs2 = (float)(-sqrt(0.5))*Bs1*BreitWignerX2( M, massB, M0, gamma, 2);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 -= Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}

	////Ampl1 *= (float)B0Factor( M, M0, spinX );


return Ampl1*ealpha;
}

std::complex<float> HamplitudeX1minus(float M, float massB, float M0, float gamma, std::complex<float> Bs1, std::complex<float> Bs2, float hmu, float thetaX, float thetaPsi, float phimu, float alpha_mu )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);

std::complex<float> ealpha = cosf(hmu*alpha_mu) + ix * sinf(hmu*alpha_mu);

std::complex<float> Hs1, Hs2, Hs3;

std::complex<float> Ampl1;
Ampl1 = 0.0;

float spinX = 1.;
int parityX = -1;

/////spinX 2
	if(spinX == 0 && parityX == -1){
	Hs1 = -Bs1*BreitWignerX( M, massB, M0, gamma, 1);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi);		

	}

	if(spinX == 1 && parityX == 1){
	Hs1 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerX1( M, massB, M0, gamma, 0) - (float)(sqrt(2.0/3.0))*Bs2*BreitWignerX1( M, massB, M0, gamma, 2);
	Hs2 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerX1( M, massB, M0, gamma, 0) + (float)(sqrt(1.0/6.0))*Bs2*BreitWignerX1( M, massB, M0, gamma, 2);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0,-1.0, thetaX) * emu2;

	}

    if(spinX == 1 && parityX == -1){	
    Hs1 = Bs1*BreitWignerX1( M, massB, M0, gamma, 1);	    
        Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 0.0, thetaX); 	}
	if(spinX == 2 && parityX == -1){
	Hs1 = (float)(sqrt(2.0/5.0))*Bs1*BreitWignerX( M, massB, M0, gamma, 1) - (float)(sqrt(3.0/5.0))*Bs2*BreitWignerX( M, massB, M0, gamma, 3);
	Hs2 = (float)(sqrt(3.0/10.0))*Bs1*BreitWignerX( M, massB, M0, gamma, 1) + (float)(sqrt(1.0/5.0))*Bs2*BreitWignerX( M, massB, M0, gamma, 3);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}
	
	if(spinX == 2 && parityX == 1){
	Hs2 = (float)(-sqrt(0.5))*Bs1*BreitWignerX( M, massB, M0, gamma, 2);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 -= Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}

	////Ampl1 *= (float)B0Factor( M, M0, spinX );


return Ampl1*ealpha;
}

std::complex<float> HamplitudeX1plus(float M, float massB, float M0, float gamma, std::complex<float> Bs1, std::complex<float> Bs2, float hmu, float thetaX, float thetaPsi, float phimu, float alpha_mu )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);

std::complex<float> ealpha = cosf(hmu*alpha_mu) + ix * sinf(hmu*alpha_mu);

std::complex<float> Hs1, Hs2, Hs3;

std::complex<float> Ampl1;
Ampl1 = 0.0;

float spinX = 1.;
int parityX = 1;

/////spinX 2
	if(spinX == 0 && parityX == -1){
	Hs1 = -Bs1*BreitWignerX( M, massB, M0, gamma, 1);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi);		

	}

	if(spinX == 1 && parityX == 1){
	Hs1 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerX1( M, massB, M0, gamma, 0) - (float)(sqrt(2.0/3.0))*Bs2*BreitWignerX1( M, massB, M0, gamma, 2);
	Hs2 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerX1( M, massB, M0, gamma, 0) + (float)(sqrt(1.0/6.0))*Bs2*BreitWignerX1( M, massB, M0, gamma, 2);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0,-1.0, thetaX) * emu2;

	}
	if(spinX == 2 && parityX == -1){
	Hs1 = (float)(sqrt(2.0/5.0))*Bs1*BreitWignerX( M, massB, M0, gamma, 1) - (float)(sqrt(3.0/5.0))*Bs2*BreitWignerX( M, massB, M0, gamma, 3);
	Hs2 = (float)(sqrt(3.0/10.0))*Bs1*BreitWignerX( M, massB, M0, gamma, 1) + (float)(sqrt(1.0/5.0))*Bs2*BreitWignerX( M, massB, M0, gamma, 3);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}
	
	if(spinX == 2 && parityX == 1){
	Hs2 = (float)(-sqrt(0.5))*Bs1*BreitWignerX( M, massB, M0, gamma, 2);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 -= Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}

	////Ampl1 *= (float)B0Factor( M, M0, spinX );


return Ampl1*ealpha;
}

std::complex<float> HamplitudeX2minus(float M, float massB, float M0, float gamma, std::complex<float> Bs1, std::complex<float> Bs2, float hmu, float thetaX, float thetaPsi, float phimu, float alpha_mu )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);

std::complex<float> ealpha = cosf(hmu*alpha_mu) + ix * sinf(hmu*alpha_mu);

std::complex<float> Hs1, Hs2, Hs3;

std::complex<float> Ampl1;
Ampl1 = 0.0;

float spinX = 2;
int parityX = -1;

/////spinX 2
	if(spinX == 0 && parityX == -1){
	Hs1 = -Bs1*BreitWignerX( M, massB, M0, gamma, 1);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi);		

	}

	if(spinX == 1 && parityX == 1){
	Hs1 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerX1( M, massB, M0, gamma, 0) - (float)(sqrt(2.0/3.0))*Bs2*BreitWignerX1( M, massB, M0, gamma, 2);
	Hs2 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerX1( M, massB, M0, gamma, 0) + (float)(sqrt(1.0/6.0))*Bs2*BreitWignerX1( M, massB, M0, gamma, 2);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0,-1.0, thetaX) * emu2;

	}
	if(spinX == 2 && parityX == -1){
	Hs1 = (float)(sqrt(2.0/5.0))*Bs1*BreitWignerX2( M, massB, M0, gamma, 1) - (float)(sqrt(3.0/5.0))*Bs2*BreitWignerX2( M, massB, M0, gamma, 3);
	Hs2 = (float)(sqrt(3.0/10.0))*Bs1*BreitWignerX2( M, massB, M0, gamma, 1) + (float)(sqrt(1.0/5.0))*Bs2*BreitWignerX2( M, massB, M0, gamma, 3);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}
	
	if(spinX == 2 && parityX == 1){
	Hs2 = (float)(-sqrt(0.5))*Bs1*BreitWignerX2( M, massB, M0, gamma, 2);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 -= Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}

	////Ampl1 *= (float)B0Factor( M, M0, spinX );


return Ampl1*ealpha;
}

std::complex<float> HamplitudeX2plus(float M, float massB, float M0, float gamma, std::complex<float> Bs1, std::complex<float> Bs2, float hmu, float thetaX, float thetaPsi, float phimu, float alpha_mu )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);

std::complex<float> ealpha = cosf(hmu*alpha_mu) + ix * sinf(hmu*alpha_mu);

std::complex<float> Hs1, Hs2, Hs3;

std::complex<float> Ampl1;
Ampl1 = 0.0;

float spinX = 2.;
int parityX = 1;

/////spinX 2
	if(spinX == 0 && parityX == -1){
	Hs1 = -Bs1*BreitWignerX( M, massB, M0, gamma, 1);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi);		

	}

	if(spinX == 1 && parityX == 1){
	Hs1 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerX1( M, massB, M0, gamma, 0) - (float)(sqrt(2.0/3.0))*Bs2*BreitWignerX1( M, massB, M0, gamma, 2);
	Hs2 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerX1( M, massB, M0, gamma, 0) + (float)(sqrt(1.0/6.0))*Bs2*BreitWignerX1( M, massB, M0, gamma, 2);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0,-1.0, thetaX) * emu2;

	}
	if(spinX == 2 && parityX == -1){
	Hs1 = (float)(sqrt(2.0/5.0))*Bs1*BreitWignerX2( M, massB, M0, gamma, 1) - (float)(sqrt(3.0/5.0))*Bs2*BreitWignerX2( M, massB, M0, gamma, 3);
	Hs2 = (float)(sqrt(3.0/10.0))*Bs1*BreitWignerX2( M, massB, M0, gamma, 1) + (float)(sqrt(1.0/5.0))*Bs2*BreitWignerX2( M, massB, M0, gamma, 3);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}
	
	if(spinX == 2 && parityX == 1){
	Hs2 = (float)(-sqrt(0.5))*Bs1*BreitWignerX2( M, massB, M0, gamma, 2);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 -= Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}

	////Ampl1 *= (float)B0Factor( M, M0, spinX );


return Ampl1*ealpha;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::complex<float> HamplitudeKstar0(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);

std::complex<float> BrWig1 = BreitWignerKstar( M, M0, MB, gamma, 1, 0);

if(BrWig1 != BrWig1){
printf("BrWig1 Kstar0 is nan");
printf("%10.2e %10.2e %10.2e %10.2e", M, M0, MB, gamma);
getchar();
}

std::complex<float>	Hs1 = -Bw1*BrWig1;

std::complex<float> Ampl1 = 0.0;

Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi);

return Ampl1;
}

std::complex<float> BreitWignerKstarNR( float MK, float M0K, float massB, float gamma0, int LK, int LB )
{
std::complex<float> ix(0,1);
const float massProton = MASS_PROTON;
const float massJpsi = MASS_JPSI;
const float massKaon = MASS_KAON;
const float massPion = MASS_PION;
const float massLambdaB = MASS_LB;
//const float massB = MASS_BD;
const float mplusK = massPion + massKaon; /// mass JPsi + mass proton

massB = MASS_BD;

float argq0 = pow( M0K*M0K - massPion*massPion - massKaon*massKaon, 2.0 ) - 4.0 * massPion*massPion*massKaon*massKaon;
float argq1 = pow( MK*MK - massPion*massPion - massKaon*massKaon, 2.0 ) - 4.0 * massPion*massPion*massKaon*massKaon;
if(argq0 < 0.0 || argq1 < 0.0) return 0.0;
float q0 = sqrt(argq0)/2.0/M0K;
float q1 = sqrt(argq1)/2.0/MK;


float argp0 = pow( massB*massB - M0K*M0K - massJpsi*massJpsi, 2.0 ) - 4.0 * M0K*M0K*massJpsi*massJpsi;
float argp1 = pow( massB*massB - MK*MK - massJpsi*massJpsi, 2.0 ) - 4.0 * MK*MK*massJpsi*massJpsi;
if(argp0 < 0.0 || argp1 < 0.0) return 0.0;
float p0 = sqrt(argp0)/2.0/massB;
float p1 = sqrt(argp1)/2.0/massB;


float gamma = gamma0 * pow( q1/q0, 2*LK + 1) * M0K / MK * powf(BlattWeisskopf(q0, q1, 3.0, LK), 2.0);

if(gamma != gamma ){printf("in BWkstar gamma is nan"); getchar();}

float BlWsLB = BlattWeisskopf(p0, p1, 3.0, LB);
float BlWsLK = BlattWeisskopf(q0, q1, 3.0, LK);

if(BlWsLB != BlWsLB) { printf("blatt LB is NaN, p0=%10.2e\tp1=%10.2e\tLB=%d", p0, p1, LB); getchar(); }
if(BlWsLK != BlWsLK) { printf("blatt LK is NaN, q0=%10.2e\tq1=%10.2e\tLK=%d", q0, q1, LK); getchar(); }

std::complex<float> Rm = (float)(BlWsLB*pow( p1/massB, LB ))*(float)(BlWsLK*pow( q1/M0K, LK ));

/*
if(Rm != Rm){
printf("\nBW: %10.2e ", abs(denom));
printf("%10.1e ", BlattWeisskopf(p0, p1, 3.0, LB));
printf("%10.1e ", BlattWeisskopf(q0, q1, 3.0, LK));
printf("%10.1e ", pow( p1/massB, LB ) );
printf("%10.1e ", pow( q1/M0K, LK ) );
printf("%10.4e ", massB );
printf("%10.4e ", mplusB1 );
printf("%10.4e ", MK );
getchar();
}*/

return Rm;
}
/////////////==============================================================================


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
std::complex<float> HamplitudeKstarNR0(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);

std::complex<float> BrWig1 = BreitWignerKstarNR( M, M0, MB, gamma, 1, 0);

if(BrWig1 != BrWig1){
printf("BrWig1 Kstar0 is nan");
printf("%10.2e %10.2e %10.2e %10.2e", M, M0, MB, gamma);
getchar();
}

std::complex<float>	Hs1 = -Bw1*BrWig1;

std::complex<float> Ampl1 = 0.0;

Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi);

return Ampl1;
}



std::complex<float> HamplitudeKstar1(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK )
{


if(hmu != hmu || thetaKstar != thetaKstar || thetaPsi != thetaPsi || phimu != phimu || phiK != phiK)
{printf("input is nan"); getchar(); }

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);
std::complex<float> eK1 = cos(phiK) + ix*sin(phiK);//exp(ix*phiK);
std::complex<float> eK2 = cos(phiK) - ix*sin(phiK);

std::complex<float> BrWig1 = BreitWignerKstar( M, M0, MB, gamma, 0, 1);
std::complex<float> BrWig2 = BreitWignerKstar( M, M0, MB, gamma, 1, 1);
std::complex<float> BrWig3 = BreitWignerKstar( M, M0, MB, gamma, 2, 1);

if(BrWig1 != BrWig1 || BrWig2 != BrWig2 || BrWig3 != BrWig3){
printf("BrWig123 Kstar1 is nan");
printf("%10.2e %10.2e %10.2e %10.2e", M, M0, MB, gamma);
getchar();
}

std::complex<float>	Hs1 = -(float)(sqrt(1.0/3.0))*Bw1*BrWig1 + (float)(sqrt(2.0/3.0))*Bw3*BrWig3;
std::complex<float>	Hs2 = (float)(sqrt(1.0/3.0))*Bw1*BrWig1 - (float)(sqrt(1.0/2.0))*Bw2*BrWig2 + (float)(sqrt(1.0/6.0))*Bw3*BrWig3;
std::complex<float>	Hs3 = (float)(sqrt(1.0/3.0))*Bw1*BrWig1 + (float)(sqrt(1.0/2.0))*Bw2*BrWig2 + (float)(sqrt(1.0/6.0))*Bw3*BrWig3;


if(Bw1 != Bw1 || Bw2 != Bw2 || Bw3 != Bw3)
{
printf("Bw is nan"); getchar();
}

if(Hs1 != Hs1 || Hs2 != Hs2 || Hs3 != Hs3)
{
printf("Hs is nan"); getchar();
}

float wmpsi1 = (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi);
float wmpsi2 = (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi);
float wmpsi3 = (float)Wigner_matrix(1.0, -1.0, hmu, thetaPsi);
float wmkst1 = (float)Wigner_matrix(1.0, 0.0, 0.0, thetaKstar);
float wmkst2 = (float)Wigner_matrix(1.0, 1.0, 0.0, thetaKstar);
float wmkst3 = (float)Wigner_matrix(1.0, -1.0, 0.0, thetaKstar);

if(wmpsi1 != wmpsi1 || wmpsi2 != wmpsi2 || wmpsi3 != wmpsi3)
{printf("wmpsi is nan"); getchar(); }
if(wmkst1 != wmkst1 || wmkst2 != wmkst2 || wmkst3 != wmkst3)
{printf("wmkst is nan"); getchar(); }


std::complex<float> Ampl1 = 0.0;
Ampl1 += Hs1 * wmpsi1 * wmkst1;
Ampl1 += Hs2 * wmpsi2 * wmkst2 * emu1 * eK1;
Ampl1 += Hs3 * wmpsi3 * wmkst3 * emu2 * eK2;


return Ampl1;
}

std::complex<float> HamplitudeKstar2(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);
std::complex<float> eK1 = cos(phiK) + ix*sin(phiK);//exp(ix*phiK);
std::complex<float> eK2 = cos(phiK) - ix*sin(phiK);

std::complex<float> BrWig1 = BreitWignerKstar( M, M0, MB, gamma, 1, 2);
std::complex<float> BrWig2 = BreitWignerKstar( M, M0, MB, gamma, 2, 2);
std::complex<float> BrWig3 = BreitWignerKstar( M, M0, MB, gamma, 3, 2);

if(BrWig1 != BrWig1 || BrWig2 != BrWig2 || BrWig3 != BrWig3){
printf("BrWig123 Kstar2 is nan");
printf("%10.2e %10.2e %10.2e %10.2e", M, M0, MB, gamma);
getchar();
}

std::complex<float>	Hs1 = (float)(sqrt(2.0/5.0))*Bw1*BrWig1 - (float)(sqrt(3.0/5.0))*Bw3*BrWig3;
std::complex<float>	Hs2 = -(float)(sqrt(3.0/10.0))*Bw1*BrWig1 + (float)(sqrt(1.0/2.0))*Bw2*BrWig2 - (float)(sqrt(1.0/5.0))*Bw3*BrWig3;
std::complex<float>	Hs3 = -(float)(sqrt(3.0/10.0))*Bw1*BrWig1 - (float)(sqrt(1.0/2.0))*Bw2*BrWig2 - (float)(sqrt(1.0/5.0))*Bw3*BrWig3;


std::complex<float> Ampl1 = 0.0;
Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 0.0, thetaKstar);
Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 1.0, 0.0, thetaKstar) * emu1 * eK1;
Ampl1 += Hs3 * (float)Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, -1.0, 0.0, thetaKstar) * emu2 * eK2;

if(Ampl1 != Ampl1){ 
	printf("BWL1 %10.2e\n", abs(BreitWignerKstar( M, M0, MB, gamma, 1, 2)) ); 
	printf("BWL3 %10.2e\n", abs(BreitWignerKstar( M, M0, MB, gamma, 3, 2)) ); getchar(); }

return Ampl1;
}


std::complex<float> HamplitudeKstar3(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);
std::complex<float> eK1 = cos(phiK) + ix*sin(phiK);//exp(ix*phiK);
std::complex<float> eK2 = cos(phiK) - ix*sin(phiK);

std::complex<float> BrWig1 = BreitWignerKstar( M, M0, MB, gamma, 2, 3);
std::complex<float> BrWig2 = BreitWignerKstar( M, M0, MB, gamma, 3, 3);
std::complex<float> BrWig3 = BreitWignerKstar( M, M0, MB, gamma, 4, 3);

if(BrWig1 != BrWig1 || BrWig2 != BrWig2 || BrWig3 != BrWig3){
printf("BrWig123 Kstar3 is nan");
printf("%10.2e %10.2e %10.2e %10.2e", M, M0, MB, gamma);
getchar();
}

std::complex<float>	Hs1 = -(float)(sqrt(3.0/7.0))*Bw1*BrWig1 + (float)(sqrt(4.0/7.0))*Bw3*BrWig3;
std::complex<float>	Hs2 = (float)(sqrt(2.0/7.0))*Bw1*BrWig1 - (float)(sqrt(1.0/2.0))*Bw2*BrWig2 + (float)(sqrt(3.0/14.0))*Bw3*BrWig3;
std::complex<float>	Hs3 = (float)(sqrt(2.0/7.0))*Bw1*BrWig1 + (float)(sqrt(1.0/2.0))*Bw2*BrWig2 + (float)(sqrt(3.0/14.0))*Bw3*BrWig3;


std::complex<float> Ampl1 = 0.0;
Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(3.0, 0.0, 0.0, thetaKstar);
Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(3.0, 1.0, 0.0, thetaKstar) * emu1 * eK1;
Ampl1 += Hs3 * (float)Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * (float)Wigner_matrix(3.0, -1.0, 0.0, thetaKstar) * emu2 * eK2;


return Ampl1;
}




std::complex<float> HamplitudeKstar4(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);
std::complex<float> eK1 = cos(phiK) + ix*sin(phiK);//exp(ix*phiK);
std::complex<float> eK2 = cos(phiK) - ix*sin(phiK);


std::complex<float> BrWig1 = BreitWignerKstar( M, M0, MB, gamma, 3, 4);
std::complex<float> BrWig2 = BreitWignerKstar( M, M0, MB, gamma, 4, 4);
std::complex<float> BrWig3 = BreitWignerKstar( M, M0, MB, gamma, 5, 4);

if(BrWig1 != BrWig1 || BrWig2 != BrWig2 || BrWig3 != BrWig3){
printf("BrWig123 Kstar4 is nan");
printf("%10.2e %10.2e %10.2e %10.2e", M, M0, MB, gamma);
getchar();
}

std::complex<float>	Hs1 = (float)(sqrt(4.0/9.0))*Bw1*BrWig1 - (float)(sqrt(5.0/9.0))*Bw3*BrWig3;
std::complex<float>	Hs2 = -(float)(1.0/3.0*sqrt(5.0/2.0))*Bw1*BrWig1 + (float)(sqrt(1.0/2.0))*Bw2*BrWig2 - (float)(sqrt(2.0/9.0))*Bw3*BrWig3;
std::complex<float>	Hs3 = -(float)(1.0/3.0*sqrt(5.0/2.0))*Bw1*BrWig1 - (float)(sqrt(1.0/2.0))*Bw2*BrWig2 - (float)(sqrt(2.0/9.0))*Bw3*BrWig3;



std::complex<float> Ampl1 = 0.0;
Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(4.0, 0.0, 0.0, thetaKstar);
Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(4.0, 1.0, 0.0, thetaKstar) * emu1 * eK1;
Ampl1 += Hs3 * (float)Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * (float)Wigner_matrix(4.0, -1.0, 0.0, thetaKstar) * emu2 * eK2;


return Ampl1;
}





float HamplitudeXKstar_total(float MX, float MK, float MB, float M01, float M02, float gamma1, float gamma2,
						std::complex<float> X1Bs1, std::complex<float> X1Bs2,
						std::complex<float> X2Bs1, std::complex<float> X2Bs2,
						std::complex<float> K1410_1_Bw1, std::complex<float> K1410_1_Bw2, std::complex<float> K1410_1_Bw3,
						std::complex<float> K1430_0_Bw1, std::complex<float> K1430_0_Bw2, std::complex<float> K1430_0_Bw3,
						std::complex<float> K1430_2_Bw1, std::complex<float> K1430_2_Bw2, std::complex<float> K1430_2_Bw3,
						std::complex<float> K1680_1_Bw1, std::complex<float> K1680_1_Bw2, std::complex<float> K1680_1_Bw3,
						std::complex<float> K1780_3_Bw1, std::complex<float> K1780_3_Bw2, std::complex<float> K1780_3_Bw3,
						std::complex<float> K1950_0_Bw1, std::complex<float> K1950_0_Bw2, std::complex<float> K1950_0_Bw3,
						std::complex<float> K1980_2_Bw1, std::complex<float> K1980_2_Bw2, std::complex<float> K1980_2_Bw3,
						std::complex<float> K2045_4_Bw1, std::complex<float> K2045_4_Bw2, std::complex<float> K2045_4_Bw3,
						std::complex<float> KNR_0_Bw,
						float thetaX, float thetaKstar, float thetaPsiX, float thetaPsiKstar, float phimuX, float phimuKstar, 
						float phiK, float alphamu)
{
	
	float MeL = 0;
	
	std::complex<float> ix(0,1);

	for(int i3 = 0; i3 < 2; i3++)
	{

	float hmu = 2*i3 - 1.0;

	///std::complex<float> eamu = cos(hmu*phimuX) + ix*sin(hmu*phimuX);//exp(ix*phimu);
	std::complex<float> eamu = 1.0;
	std::complex<float> A1X = eamu*HamplitudeX1plus( MX, MB, M01, gamma1, X1Bs1, X1Bs2, hmu, thetaX, thetaPsiX, phimuX, alphamu );
	
	std::complex<float> A2X = eamu*HamplitudeX1plus( MX, MB, M02, gamma2, X2Bs1, X2Bs2, hmu, thetaX, thetaPsiX, phimuX, alphamu );
	
	
	
	std::complex<float> AK1410_1 = HamplitudeKstar1(MK, MB, 1.414, 0.232, K1410_1_Bw1, K1410_1_Bw2, K1410_1_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	std::complex<float> AK1430_0 = HamplitudeKstar0(MK, MB, 1.425, 0.270, K1430_0_Bw1, K1430_0_Bw2, K1430_0_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	//	std::complex<float> AK1430_2 = HamplitudeKstar2(MK, MB, 1.426, 0.099, K1430_2_Bw1, K1430_2_Bw2, K1430_2_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	// PDG 2018, neutral only
	std::complex<float> AK1430_2 = HamplitudeKstar2(MK, MB, 1.4324, 0.109, K1430_2_Bw1, K1430_2_Bw2, K1430_2_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	//	std::complex<float> AK1680_1 = HamplitudeKstar1(MK, MB, 1.717, 0.322, K1680_1_Bw1, K1680_1_Bw2, K1680_1_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	// PDG 2018
	std::complex<float> AK1680_1 = HamplitudeKstar1(MK, MB, 1.718, 0.322, K1680_1_Bw1, K1680_1_Bw2, K1680_1_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	std::complex<float> AK1780_3 = HamplitudeKstar3(MK, MB, 1.776, 0.159, K1780_3_Bw1, K1780_3_Bw2, K1780_3_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	std::complex<float> AK1950_0 = HamplitudeKstar0(MK, MB, 1.945, 0.201, K1950_0_Bw1, K1950_0_Bw2, K1950_0_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );

	std::complex<float> AK1980_2 = HamplitudeKstar2(MK, MB, 1.974, 0.376, K1980_2_Bw1, K1980_2_Bw2, K1980_2_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );

	std::complex<float> AK2045_4 = HamplitudeKstar4(MK, MB, 2.045, 0.198, K2045_4_Bw1, K2045_4_Bw2, K2045_4_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	///std::complex<float> AK1430_2 = HamplitudeKstar2(MK, MB, 1.4324, 0.109, K1430_2_Bw1, K1430_2_Bw2, K1430_2_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	std::complex<float> AKNR_0 = 0.0;
	if(includeKsNR == 1) AKNR_0 = HamplitudeKstarNR0(MK, MB, 1.425, 0.270, KNR_0_Bw, 0.0, 0.0, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK ); //commented


	
	//float ad = pow( abs(A1X + A2X + AK1410_1 + AK1430_0 + AK1430_2 + AK1680_1 + AK1780_3 + AK1950_0 + AK2045_4), 2.0 );
	float ad = pow( abs(A1X + A2X + AK1410_1 + AK1430_0 + AK1430_2 + AK1680_1 + AK1780_3 + AK1950_0 + AK1980_2 + AK2045_4 + AKNR_0), 2.0 );
	MeL += ad;
	if(ad != ad){
		printf("MX=%10.2e\n", MX);
		printf("MB=%10.2e\n", MB);
		printf("MK=%10.2e\n", MK);
		printf("A1X=%10.2e\n", abs(A1X));
		printf("A2X=%10.2e\n", abs(A2X));
		printf("AK1410_1=%10.2e\n", abs(AK1410_1));
		printf("AK1430_0=%10.2e\n", abs(AK1430_0));
		printf("AK1430_2=%10.2e\n", abs(AK1430_2));
		printf("AK1680_1=%10.2e\n", abs(AK1680_1));
		printf("AK1780_3=%10.2e\n", abs(AK1780_3));
		printf("AK1950_0=%10.2e\n", abs(AK1950_0));
		printf("AK1980_0=%10.2e\n", abs(AK1980_2));
		printf("AK2045_4=%10.2e\n", abs(AK2045_4));
		
		getchar();
	}


	}

	return MeL/3.0;


}

std::complex<float> HamplitudeKstar0_accelerated(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK, std::complex<float> BWK1_0, std::complex<float> BWK1_1)
{

std::complex<float> Ampl1;
if (hmu < 0.)
{
    Ampl1 = Bw1 * BWK1_0;
}
else 
{
    Ampl1 = Bw1 * BWK1_1;
}

return Ampl1;
}

// -BrWig1* (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi)


std::complex<float> HamplitudeKstar1_accelerated(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK, std::complex<float> BWK1_0, std::complex<float> BWK2_0, std::complex<float> BWK3_0, std::complex<float> BWK1_1, std::complex<float> BWK2_1, std::complex<float> BWK3_1)
{


std::complex<float> Ampl1;
if (hmu < 0.)
{
    Ampl1 = Bw1 * BWK1_0 + Bw2 * BWK2_0 + Bw3 * BWK3_0;
}
else 
{
    Ampl1 = Bw1 * BWK1_1 + Bw2 * BWK2_1 + Bw3 * BWK3_1;
}


return Ampl1;
}

std::complex<float> HamplitudeKstar2_accelerated(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK, std::complex<float> BWK1_0, std::complex<float> BWK2_0, std::complex<float> BWK3_0, std::complex<float> BWK1_1, std::complex<float> BWK2_1, std::complex<float> BWK3_1)
{

std::complex<float> Ampl1;
if (hmu < 0.)
{
    Ampl1 = Bw1 * BWK1_0 + Bw2 * BWK2_0 + Bw3 * BWK3_0;
}
else 
{
    Ampl1 = Bw1 * BWK1_1 + Bw2 * BWK2_1 + Bw3 * BWK3_1;
}

return Ampl1;
}

std::complex<float> HamplitudeKstar3_accelerated(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK, std::complex<float> BWK1_0, std::complex<float> BWK2_0, std::complex<float> BWK3_0, std::complex<float> BWK1_1, std::complex<float> BWK2_1, std::complex<float> BWK3_1)
{

std::complex<float> Ampl1;
if (hmu < 0.)
{
    Ampl1 = Bw1 * BWK1_0 + Bw2 * BWK2_0 + Bw3 * BWK3_0;
}
else 
{
    Ampl1 = Bw1 * BWK1_1 + Bw2 * BWK2_1 + Bw3 * BWK3_1;
}

return Ampl1;
}

std::complex<float> HamplitudeKstar4_accelerated(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK, std::complex<float> BWK1_0, std::complex<float> BWK2_0, std::complex<float> BWK3_0, std::complex<float> BWK1_1, std::complex<float> BWK2_1, std::complex<float> BWK3_1)
{

std::complex<float> Ampl1;
if (hmu < 0.)
{
    Ampl1 = Bw1 * BWK1_0 + Bw2 * BWK2_0 + Bw3 * BWK3_0;
}
else 
{
    Ampl1 = Bw1 * BWK1_1 + Bw2 * BWK2_1 + Bw3 * BWK3_1;
}

return Ampl1;
}





float HamplitudeXKstar_total_accelerated(float MX, float MK, float MB, float M01, float M02, float gamma1, float gamma2,
						std::complex<float> X1Bs1, std::complex<float> X1Bs2,
						std::complex<float> X2Bs1, std::complex<float> X2Bs2,
						std::complex<float> K1410_1_Bw1, std::complex<float> K1410_1_Bw2, std::complex<float> K1410_1_Bw3,
						std::complex<float> K1430_0_Bw1, std::complex<float> K1430_0_Bw2, std::complex<float> K1430_0_Bw3,
						std::complex<float> K1430_2_Bw1, std::complex<float> K1430_2_Bw2, std::complex<float> K1430_2_Bw3,
						std::complex<float> K1680_1_Bw1, std::complex<float> K1680_1_Bw2, std::complex<float> K1680_1_Bw3,
						std::complex<float> K1780_3_Bw1, std::complex<float> K1780_3_Bw2, std::complex<float> K1780_3_Bw3,
						std::complex<float> K1950_0_Bw1, std::complex<float> K1950_0_Bw2, std::complex<float> K1950_0_Bw3,
						std::complex<float> K1980_2_Bw1, std::complex<float> K1980_2_Bw2, std::complex<float> K1980_2_Bw3,
						std::complex<float> K2045_4_Bw1, std::complex<float> K2045_4_Bw2, std::complex<float> K2045_4_Bw3,
						std::complex<float> KNR_0_Bw,
						float thetaX, float thetaKstar, float thetaPsiX, float thetaPsiKstar, float phimuX, float phimuKstar, 
						float phiK, float alphamu,
                        std::complex<float> aBWK1410_1_1_minus, std::complex<float> aBWK1410_1_2_minus, std::complex<float> aBWK1410_1_3_minus, 
                        std::complex<float> aBWK1430_0_1_minus, 
                        std::complex<float> aBWK1430_2_1_minus, std::complex<float> aBWK1430_2_2_minus, std::complex<float> aBWK1430_2_3_minus, 
                        std::complex<float> aBWK1680_1_1_minus, std::complex<float> aBWK1680_1_2_minus, std::complex<float> aBWK1680_1_3_minus, 
                        std::complex<float> aBWK1780_3_1_minus, std::complex<float> aBWK1780_3_2_minus, std::complex<float> aBWK1780_3_3_minus, 
                        std::complex<float> aBWK1950_0_1_minus, 
                        std::complex<float> aBWK1980_2_1_minus, std::complex<float> aBWK1980_2_2_minus, std::complex<float> aBWK1980_2_3_minus, 
                        std::complex<float> aBWK2045_4_1_minus, std::complex<float> aBWK2045_4_2_minus, std::complex<float> aBWK2045_4_3_minus,
                        std::complex<float> aBWK1410_1_1_plus, std::complex<float> aBWK1410_1_2_plus, std::complex<float> aBWK1410_1_3_plus, 
                        std::complex<float> aBWK1430_0_1_plus, 
                        std::complex<float> aBWK1430_2_1_plus, std::complex<float> aBWK1430_2_2_plus, std::complex<float> aBWK1430_2_3_plus, 
                        std::complex<float> aBWK1680_1_1_plus, std::complex<float> aBWK1680_1_2_plus, std::complex<float> aBWK1680_1_3_plus, 
                        std::complex<float> aBWK1780_3_1_plus, std::complex<float> aBWK1780_3_2_plus, std::complex<float> aBWK1780_3_3_plus, 
                        std::complex<float> aBWK1950_0_1_plus, 
                        std::complex<float> aBWK1980_2_1_plus, std::complex<float> aBWK1980_2_2_plus, std::complex<float> aBWK1980_2_3_plus, 
                        std::complex<float> aBWK2045_4_1_plus, std::complex<float> aBWK2045_4_2_plus, std::complex<float> aBWK2045_4_3_plus)
{
	
	float MeL = 0;
	
	std::complex<float> ix(0,1);

	for(int i3 = 0; i3 < 2; i3++)
	{

	float hmu = 2*i3 - 1.0;

	///std::complex<float> eamu = cos(hmu*phimuX) + ix*sin(hmu*phimuX);//exp(ix*phimu);
	std::complex<float> eamu = 1.0;
    
    std::complex<float> A1X = std::complex<float>(0., 0.);
    std::complex<float> A2X = std::complex<float>(0., 0.);
    
    if (MODEL == 0)
    {
        A1X = eamu*HamplitudeX1plus( MX, MB, M01, gamma1, X1Bs1, X1Bs2, hmu, thetaX, thetaPsiX, phimuX, alphamu );
        A2X = eamu*HamplitudeX1plus( MX, MB, M02, gamma2, X2Bs1, X2Bs2, hmu, thetaX, thetaPsiX, phimuX, alphamu );
    }
    
    if (MODEL == 2)
    {
        A1X = eamu*HamplitudeX1minus( MX, MB, M01, gamma1, X1Bs1, X1Bs2, hmu, thetaX, thetaPsiX, phimuX, alphamu );
        A2X = eamu*HamplitudeX1minus( MX, MB, M02, gamma2, X2Bs1, X2Bs2, hmu, thetaX, thetaPsiX, phimuX, alphamu );
    }
    
    if (MODEL == 3)
    {
        A1X = eamu*HamplitudeX0minus( MX, MB, M01, gamma1, X1Bs1, X1Bs2, hmu, thetaX, thetaPsiX, phimuX, alphamu );
        A2X = eamu*HamplitudeX0minus( MX, MB, M02, gamma2, X2Bs1, X2Bs2, hmu, thetaX, thetaPsiX, phimuX, alphamu );
    }
    
    if (MODEL == 4)
    {
        A1X = eamu*HamplitudeX2minus( MX, MB, M01, gamma1, X1Bs1, X1Bs2, hmu, thetaX, thetaPsiX, phimuX, alphamu );
        A2X = eamu*HamplitudeX2minus( MX, MB, M02, gamma2, X2Bs1, X2Bs2, hmu, thetaX, thetaPsiX, phimuX, alphamu );
    }
    
    if (MODEL == 5)
    {
        A1X = eamu*HamplitudeX2plus( MX, MB, M01, gamma1, X1Bs1, X1Bs2, hmu, thetaX, thetaPsiX, phimuX, alphamu );
        A2X = eamu*HamplitudeX2plus( MX, MB, M02, gamma2, X2Bs1, X2Bs2, hmu, thetaX, thetaPsiX, phimuX, alphamu );
    }
	
	std::complex<float> AK1410_1 = HamplitudeKstar1_accelerated(MK, MB, 1.414, 0.232, K1410_1_Bw1, K1410_1_Bw2, K1410_1_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK, aBWK1410_1_1_minus, aBWK1410_1_2_minus, aBWK1410_1_3_minus, aBWK1410_1_1_plus, aBWK1410_1_2_plus, aBWK1410_1_3_plus);
	std::complex<float> AK1430_0 = HamplitudeKstar0_accelerated(MK, MB, 1.425, 0.270, K1430_0_Bw1, K1430_0_Bw2, K1430_0_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK, aBWK1430_0_1_minus, aBWK1430_0_1_plus);
	//	std::complex<float> AK1430_2 = HamplitudeKstar2(MK, MB, 1.426, 0.099, K1430_2_Bw1, K1430_2_Bw2, K1430_2_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	// PDG 2018, neutral only
	std::complex<float> AK1430_2 = HamplitudeKstar2_accelerated(MK, MB, 1.4324, 0.109, K1430_2_Bw1, K1430_2_Bw2, K1430_2_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK, aBWK1430_2_1_minus, aBWK1430_2_2_minus, aBWK1430_2_3_minus, aBWK1430_2_1_plus, aBWK1430_2_2_plus, aBWK1430_2_3_plus);
	//	std::complex<float> AK1680_1 = HamplitudeKstar1(MK, MB, 1.717, 0.322, K1680_1_Bw1, K1680_1_Bw2, K1680_1_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	// PDG 2018
	std::complex<float> AK1680_1 = HamplitudeKstar1_accelerated(MK, MB, 1.718, 0.322, K1680_1_Bw1, K1680_1_Bw2, K1680_1_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK, aBWK1680_1_1_minus, aBWK1680_1_2_minus, aBWK1680_1_3_minus, aBWK1680_1_1_plus, aBWK1680_1_2_plus, aBWK1680_1_3_plus);
	std::complex<float> AK1780_3 = HamplitudeKstar3_accelerated(MK, MB, 1.776, 0.159, K1780_3_Bw1, K1780_3_Bw2, K1780_3_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK, aBWK1780_3_1_minus, aBWK1780_3_2_minus, aBWK1780_3_3_minus, aBWK1780_3_1_plus, aBWK1780_3_2_plus, aBWK1780_3_3_plus);
	std::complex<float> AK1950_0 = HamplitudeKstar0_accelerated(MK, MB, 1.945, 0.201, K1950_0_Bw1, K1950_0_Bw2, K1950_0_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK, aBWK1950_0_1_minus, aBWK1950_0_1_plus);

	std::complex<float> AK1980_2 = HamplitudeKstar2_accelerated(MK, MB, 1.974, 0.376, K1980_2_Bw1, K1980_2_Bw2, K1980_2_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK, aBWK1980_2_1_minus, aBWK1980_2_2_minus, aBWK1980_2_3_minus, aBWK1980_2_1_plus, aBWK1980_2_2_plus, aBWK1980_2_3_plus);

	std::complex<float> AK2045_4 = HamplitudeKstar4_accelerated(MK, MB, 2.045, 0.198, K2045_4_Bw1, K2045_4_Bw2, K2045_4_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK, aBWK2045_4_1_minus, aBWK2045_4_2_minus, aBWK2045_4_3_minus, aBWK2045_4_1_plus, aBWK2045_4_2_plus, aBWK2045_4_3_plus);
	///std::complex<float> AK1430_2 = HamplitudeKstar2(MK, MB, 1.4324, 0.109, K1430_2_Bw1, K1430_2_Bw2, K1430_2_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	std::complex<float> AKNR_0 = 0.0;
	if(includeKsNR == 1) AKNR_0 = HamplitudeKstarNR0(MK, MB, 1.975, 0.270, KNR_0_Bw, 0.0, 0.0, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK ); //commented


	float ad = pow( abs(A1X + A2X + AK1410_1 + AK1430_0 + AK1430_2 + AK1680_1 + AK1780_3 + AK1950_0 + AK2045_4 + AK1980_2 + AKNR_0), 2.0 );
	//float ad = pow( abs(AK1410_1 + AK1430_0 + AK1430_2 + AK1680_1 + AK1780_3 + AK1950_0 + AK1980_2 + AK2045_4), 2.0 );
	MeL += ad;
	if(ad != ad){
		printf("MX=%10.2e\n", MX);
		printf("MB=%10.2e\n", MB);
		printf("MK=%10.2e\n", MK);
		//printf("A1X=%10.2e\n", abs(A1X));
		//printf("A2X=%10.2e\n", abs(A2X));
		printf("AK1410_1=%10.2e\n", abs(AK1410_1));
		printf("AK1430_0=%10.2e\n", abs(AK1430_0));
		printf("AK1430_2=%10.2e\n", abs(AK1430_2));
		printf("AK1680_1=%10.2e\n", abs(AK1680_1));
		printf("AK1780_3=%10.2e\n", abs(AK1780_3));
		printf("AK1950_0=%10.2e\n", abs(AK1950_0));
		printf("AK1980_0=%10.2e\n", abs(AK1980_2));
		printf("AK2045_4=%10.2e\n", abs(AK2045_4));
		
		getchar();
	}


	}

	return MeL/3.0;


}

//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
//                                                                  For Bs -> Jpsi K K                                                                              //
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//

std::complex<float> BreitWignerf( float MK, float M0K, float massB, float gamma0, int LK, int LB )
{
std::complex<float> ix(0,1);
const float massProton = MASS_PROTON;
const float massJpsi = MASS_JPSI;
const float massKaon = MASS_KAON;
const float massPion = MASS_PION;
const float massLambdaB = MASS_LB;
//const float massB = MASS_BD;
const float mplusK = massKaon + massKaon; /// mass JPsi + mass proton

massB = MASS_BS;


float argq0 = pow( M0K*M0K - massKaon*massKaon - massKaon*massKaon, 2.0 ) - 4.0 * massKaon*massKaon*massKaon*massKaon;
float argq1 = pow( MK*MK - massKaon*massKaon - massKaon*massKaon, 2.0 ) - 4.0 * massKaon*massKaon*massKaon*massKaon;
if(argq0 < 0.0 || argq1 < 0.0) return 0.0;
float q0 = sqrt(argq0)/2.0/M0K;
float q1 = sqrt(argq1)/2.0/MK;


float argp0 = pow( massB*massB - M0K*M0K - massJpsi*massJpsi, 2.0 ) - 4.0 * M0K*M0K*massJpsi*massJpsi;
float argp1 = pow( massB*massB - MK*MK - massJpsi*massJpsi, 2.0 ) - 4.0 * MK*MK*massJpsi*massJpsi;
if(argp0 < 0.0 || argp1 < 0.0) return 0.0;
float p0 = sqrt(argp0)/2.0/massB;
float p1 = sqrt(argp1)/2.0/massB;


float gamma = gamma0 * pow( q1/q0, 2*LK + 1) * M0K / MK * BlattWeisskopf(q0, q1, 3.0, LK);

if(gamma != gamma ){printf("in BWkstar gamma is nan"); getchar();}

float nom = 1.0;
//if(BW_nom_f==1) nom = MK*sqrt(M0K*gamma);
std::complex<float> denom = nom/( M0K*M0K - MK*MK - ix*M0K*gamma );

float BlWsLB = BlattWeisskopf(p0, p1, 3.0, LB);
float BlWsLK = BlattWeisskopf(q0, q1, 3.0, LK);

if(BlWsLB != BlWsLB) { printf("blatt LB is NaN, p0=%10.2e\tp1=%10.2e\tLB=%d", p0, p1, LB); getchar(); }
if(BlWsLK != BlWsLK) { printf("blatt LK is NaN, q0=%10.2e\tq1=%10.2e\tLK=%d", q0, q1, LK); getchar(); }

std::complex<float> Rm = (float)(BlWsLB*pow( p1/massB, LB ))*denom*(float)(BlWsLK*pow( q1/M0K, LK ));

/*
if(Rm != Rm){
printf("\nBW: %10.2e ", abs(denom));
printf("%10.1e ", BlattWeisskopf(p0, p1, 3.0, LB));
printf("%10.1e ", BlattWeisskopf(q0, q1, 3.0, LK));
printf("%10.1e ", pow( p1/massB, LB ) );
printf("%10.1e ", pow( q1/M0K, LK ) );
printf("%10.4e ", massB );
printf("%10.4e ", mplusB1 );
printf("%10.4e ", MK );
getchar();
}*/

return Rm;
}

std::complex<float> BreitWignerfNR( float MK, float M0K, float massB, float gamma0, int LK, int LB )
{
std::complex<float> ix(0,1);
const float massProton = MASS_PROTON;
const float massJpsi = MASS_JPSI;
const float massKaon = MASS_KAON;
const float massPion = MASS_PION;
const float massLambdaB = MASS_LB;
//const float massB = MASS_BD;
const float mplusK = massKaon + massKaon; /// mass JPsi + mass proton

massB = MASS_BS;


float argq0 = pow( M0K*M0K - massKaon*massKaon - massKaon*massKaon, 2.0 ) - 4.0 * massKaon*massKaon*massKaon*massKaon;
float argq1 = pow( MK*MK - massKaon*massKaon - massKaon*massKaon, 2.0 ) - 4.0 * massKaon*massKaon*massKaon*massKaon;
if(argq0 < 0.0 || argq1 < 0.0) return 0.0;
float q0 = sqrt(argq0)/2.0/M0K;
float q1 = sqrt(argq1)/2.0/MK;


float argp0 = pow( massB*massB - M0K*M0K - massJpsi*massJpsi, 2.0 ) - 4.0 * M0K*M0K*massJpsi*massJpsi;
float argp1 = pow( massB*massB - MK*MK - massJpsi*massJpsi, 2.0 ) - 4.0 * MK*MK*massJpsi*massJpsi;
if(argp0 < 0.0 || argp1 < 0.0) return 0.0;
float p0 = sqrt(argp0)/2.0/massB;
float p1 = sqrt(argp1)/2.0/massB;


float gamma = gamma0 * pow( q1/q0, 2*LK + 1) * M0K / MK * BlattWeisskopf(q0, q1, 3.0, LK);

if(gamma != gamma ){printf("in BWkstar gamma is nan"); getchar();}

float BlWsLB = BlattWeisskopf(p0, p1, 3.0, LB);
float BlWsLK = BlattWeisskopf(q0, q1, 3.0, LK);

if(BlWsLB != BlWsLB) { printf("blatt LB is NaN, p0=%10.2e\tp1=%10.2e\tLB=%d", p0, p1, LB); getchar(); }
if(BlWsLK != BlWsLK) { printf("blatt LK is NaN, q0=%10.2e\tq1=%10.2e\tLK=%d", q0, q1, LK); getchar(); }

std::complex<float> Rm = (float)(BlWsLB*pow( p1/massB, LB ))*(float)(BlWsLK*pow( q1/M0K, LK ));

return Rm;
}

int spinZ; 

std::complex<float> BreitWignerZ( float MX, float massB, float M0X, float gamma0, int LX )
{
std::complex<float> ix(0,1);
const float massProton = MASS_PROTON;
const float massJpsi = MASS_JPSI;
const float massKaon = MASS_KAON;
const float massPion = MASS_PION;
const float massLambdaB = MASS_LB;
massB = MASS_BS;


float argq0 = pow( M0X*M0X - massJpsi*massJpsi - massKaon*massKaon, 2.0 ) - 4.0 * massJpsi*massJpsi*massKaon*massKaon;
float argq1 = pow( MX*MX - massJpsi*massJpsi - massKaon*massKaon, 2.0 ) - 4.0 * massJpsi*massJpsi*massKaon*massKaon;
if(argq0 < 0.0 || argq1 < 0.0) return 0.0;
float q0 = sqrt(argq0)/2.0/M0X;
float q1 = sqrt(argq1)/2.0/MX;


float argp0 = pow( massB*massB - M0X*M0X - massKaon*massKaon, 2.0 ) - 4.0 * M0X*M0X*massKaon*massKaon;
float argp1 = pow( massB*massB - MX*MX - massKaon*massKaon, 2.0 ) - 4.0 * MX*MX*massKaon*massKaon;
if(argp0 < 0.0 || argp1 < 0.0) return 0.0;
float p0 = sqrt(argp0)/2.0/massB;
float p1 = sqrt(argp1)/2.0/massB;

float gamma = gamma0 * pow( q1/q0, 2*LX + 1) * M0X / MX * powf(BlattWeisskopf(q0, q1, 3.0, LX), 2.0);

float nom = MX*sqrt(M0X*gamma);
std::complex<float> denom = nom/( M0X*M0X - MX*MX - ix*M0X*gamma );
///std::complex<float> Rm = denom * BlattWeisskopf(q0, q1, 3.0, LX) * pow( q1/M0X, LX );

const int LB = spinZ;
std::complex<float> Rm = (float)(BlattWeisskopf(p0, p1, 3.0, LB) * pow( p1/massB, LB )) * denom * (float)(BlattWeisskopf(q0, q1, 3.0, LX) * pow( q1/M0X, LX ));

return Rm;
}

std::complex<float> HamplitudefNR0(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);

std::complex<float> BrWig1 = BreitWignerfNR( M, M0, MB, gamma, 1, 0);

std::complex<float>	Hs1 = -Bw1*BrWig1;

std::complex<float> Ampl1 = 0.0;

Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi);

return Ampl1;
}



std::complex<float> Hamplitudephi1(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);
std::complex<float> eK1 = cos(phiK) + ix*sin(phiK);//exp(ix*phiK);
std::complex<float> eK2 = cos(phiK) - ix*sin(phiK);

std::complex<float> BrWig1 = BreitWignerf( M, M0, MB, gamma, 0, 1);
std::complex<float> BrWig2 = BreitWignerf( M, M0, MB, gamma, 1, 1);
std::complex<float> BrWig3 = BreitWignerf( M, M0, MB, gamma, 2, 1);

std::complex<float>	Hs1 = -(float)(sqrt(1.0/3.0))*Bw1*BrWig1 + (float)(sqrt(2.0/3.0))*Bw3*BrWig3;
std::complex<float>	Hs2 = (float)(sqrt(1.0/3.0))*Bw1*BrWig1 - (float)(sqrt(1.0/2.0))*Bw2*BrWig2 + (float)(sqrt(1.0/6.0))*Bw3*BrWig3;
std::complex<float>	Hs3 = (float)(sqrt(1.0/3.0))*Bw1*BrWig1 + (float)(sqrt(1.0/2.0))*Bw2*BrWig2 + (float)(sqrt(1.0/6.0))*Bw3*BrWig3;


float wmpsi1 = (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi);
float wmpsi2 = (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi);
float wmpsi3 = (float)Wigner_matrix(1.0, -1.0, hmu, thetaPsi);
float wmkst1 = (float)Wigner_matrix(1.0, 0.0, 0.0, thetaKstar);
float wmkst2 = (float)Wigner_matrix(1.0, 1.0, 0.0, thetaKstar);
float wmkst3 = (float)Wigner_matrix(1.0, -1.0, 0.0, thetaKstar);

if(wmpsi1 != wmpsi1 || wmpsi2 != wmpsi2 || wmpsi3 != wmpsi3)
{printf("wmpsi is nan"); getchar(); }
if(wmkst1 != wmkst1 || wmkst2 != wmkst2 || wmkst3 != wmkst3)
{printf("wmkst is nan"); getchar(); }


std::complex<float> Ampl1 = 0.0;
Ampl1 += Hs1 * wmpsi1 * wmkst1;
Ampl1 += Hs2 * wmpsi2 * wmkst2 * emu1 * eK1;
Ampl1 += Hs3 * wmpsi3 * wmkst3 * emu2 * eK2;


return Ampl1;
}



std::complex<float> Hamplitudef2(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);
std::complex<float> eK1 = cos(phiK) + ix*sin(phiK);//exp(ix*phiK);
std::complex<float> eK2 = cos(phiK) - ix*sin(phiK);

std::complex<float> BrWig1 = BreitWignerf( M, M0, MB, gamma, 1, 2);
std::complex<float> BrWig2 = BreitWignerf( M, M0, MB, gamma, 2, 2);
std::complex<float> BrWig3 = BreitWignerf( M, M0, MB, gamma, 3, 2);

if(BrWig1 != BrWig1 || BrWig2 != BrWig2 || BrWig3 != BrWig3){
printf("BrWig123 Kstar2 is nan");
printf("%10.2e %10.2e %10.2e %10.2e", M, M0, MB, gamma);
getchar();
}

std::complex<float>	Hs1 = (float)(sqrt(2.0/5.0))*Bw1*BrWig1 - (float)(sqrt(3.0/5.0))*Bw3*BrWig3;
std::complex<float>	Hs2 = -(float)(sqrt(3.0/10.0))*Bw1*BrWig1 + (float)(sqrt(1.0/2.0))*Bw2*BrWig2 - (float)(sqrt(1.0/5.0))*Bw3*BrWig3;
std::complex<float>	Hs3 = -(float)(sqrt(3.0/10.0))*Bw1*BrWig1 - (float)(sqrt(1.0/2.0))*Bw2*BrWig2 - (float)(sqrt(1.0/5.0))*Bw3*BrWig3;


std::complex<float> Ampl1 = 0.0;
Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 0.0, thetaKstar);
Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 1.0, 0.0, thetaKstar) * emu1 * eK1;
Ampl1 += Hs3 * (float)Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, -1.0, 0.0, thetaKstar) * emu2 * eK2;

if(Ampl1 != Ampl1){ 
	printf("BWL1 %10.2e\n", abs(BreitWignerKstar( M, M0, MB, gamma, 1, 2)) ); 
	printf("BWL3 %10.2e\n", abs(BreitWignerKstar( M, M0, MB, gamma, 3, 2)) ); getchar(); }

return Ampl1;
}

std::complex<float> HamplitudeZ1plus(float M, float massB, float M0, float gamma, std::complex<float> Bs1, std::complex<float> Bs2, float hmu, float thetaX, float thetaPsi, float phimu )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);
std::complex<float> Hs1, Hs2, Hs3;

std::complex<float> Ampl1;
Ampl1 = 0.0;

int spinZ = 1;
int parityZ = 1;

/////spinX 2
	if(spinZ == 0 && parityZ == -1){
	Hs1 = -Bs1*BreitWignerZ( M, massB, M0, gamma, 1);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi);		

	}

	if(spinZ == 1 && parityZ == 1){
	Hs1 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerZ( M, massB, M0, gamma, 0) - (float)(sqrt(2.0/3.0))*Bs2*BreitWignerZ( M, massB, M0, gamma, 2);
	Hs2 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerZ( M, massB, M0, gamma, 0) + (float)(sqrt(1.0/6.0))*Bs2*BreitWignerZ( M, massB, M0, gamma, 2);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0,-1.0, thetaX) * emu2;

	}
	if(spinZ == 2 && parityZ == -1){
	Hs1 = (float)(sqrt(2.0/5.0))*Bs1*BreitWignerZ( M, massB, M0, gamma, 1) - (float)(sqrt(3.0/5.0))*Bs2*BreitWignerZ( M, massB, M0, gamma, 3);
	Hs2 = (float)(sqrt(3.0/10.0))*Bs1*BreitWignerZ( M, massB, M0, gamma, 1) + (float)(sqrt(1.0/5.0))*Bs2*BreitWignerZ( M, massB, M0, gamma, 3);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}
	
	if(spinZ == 2 && parityZ == 1){
	Hs2 = (float)(-sqrt(0.5))*Bs1*BreitWignerZ( M, massB, M0, gamma, 2);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 -= Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}

	/////Ampl1 *= (float)BsFactor( M, M0, spinZ );


return Ampl1;
}

float Hamplitudef_total(float MX, float MX1, float MK, float MB, float M01, float M02, float gamma1, float gamma2,
						std::complex<float> X1Bs1, std::complex<float> X1Bs2,
						std::complex<float> phi1680_1_Bw1, std::complex<float> phi1680_1_Bw2, std::complex<float> phi1680_1_Bw3,
						std::complex<float> f1525_2_Bw1, std::complex<float> f1525_2_Bw2, std::complex<float> f1525_2_Bw3,
						std::complex<float> f1640_2_Bw1, std::complex<float> f1640_2_Bw2, std::complex<float> f1640_2_Bw3,
						std::complex<float> f1750_2_Bw1, std::complex<float> f1750_2_Bw2, std::complex<float> f1750_2_Bw3,
						std::complex<float> f1950_2_Bw1, std::complex<float> f1950_2_Bw2, std::complex<float> f1950_2_Bw3,
						std::complex<float> NR_Bw1,
						float thetaXs, float thetaF, float thetaPsiXs, float thetaPsiF, float phimuXs, float phimuF, float phiKF, float thetaXs1, float thetaPsiF1, float phimuXs1)
{
	float MeL = 0;
	
	std::complex<float> ix(0,1);

	for(int i3 = 0; i3 < 2; i3++)
	{

	float hmu = 2*i3 - 1.0;

	//std::complex<float> A1X = HamplitudeX1plus( MX, MB, M01, gamma1, X1Bs1, X1Bs2, hmu, thetaXs, thetaPsiXs, phimuXs );
	//std::complex<float> A2X = HamplitudeX2plus( MX, MB, M02, gamma2, X2Bs1, X2Bs2, hmu, thetaXs, thetaPsiXs, phimuXs );
	
	
	//std::complex<float> A1X = HamplitudeZ1plus( MX, MB, M01, gamma1, X1Bs1, X1Bs2, hmu, thetaXs, thetaPsiF, phimuXs );
	//std::complex<float> A1X1 = HamplitudeZ1plus( MX1, MB, M01, gamma1, X1Bs1, X1Bs2, hmu, thetaXs1, thetaPsiF1, phimuXs1 );
	std::complex<float> Aphi1680_1 = Hamplitudephi1(MK, MB, 1.680, 0.150, phi1680_1_Bw1, phi1680_1_Bw2, phi1680_1_Bw3, hmu, thetaF, thetaPsiF, phimuF, phiKF );
	std::complex<float> Af1525_2 = Hamplitudef2(MK, MB, 1.525, 0.073, f1525_2_Bw1, f1525_2_Bw2, f1525_2_Bw3, hmu, thetaF, thetaPsiF, phimuF, phiKF );
	std::complex<float> Af1640_2 = Hamplitudef2(MK, MB, 1.639, 0.099, f1640_2_Bw1, f1640_2_Bw2, f1640_2_Bw3, hmu, thetaF, thetaPsiF, phimuF, phiKF );
	std::complex<float> Af1750_2 = Hamplitudef2(MK, MB, 1.750, 0.099, f1750_2_Bw1, f1750_2_Bw2, f1750_2_Bw3, hmu, thetaF, thetaPsiF, phimuF, phiKF );
	std::complex<float> Af1950_2 = Hamplitudef2(MK, MB, 1.944, 0.472, f1950_2_Bw1, f1950_2_Bw2, f1950_2_Bw3, hmu, thetaF, thetaPsiF, phimuF, phiKF );
	std::complex<float> ANR = 0.0;
	if(includeFNRsimple == 1) ANR = NR_Bw1;
	//if(includeFNR == 1) ANR = HamplitudefNR0(MK, MB, 1.9, 0.3, NR_Bw1, 0.0, 0.0, hmu, thetaF, thetaPsiF, phimuF, phiKF );
	//float ad = pow( abs(A1X + A2X + AK1410_1 + AK1430_0 + AK1430_2 + AK1680_1 + AK1780_3 + AK1950_0 + AK2045_4), 2.0 );
	float ad = pow( abs(Aphi1680_1 + Af1525_2 + Af1640_2 + Af1750_2 + Af1950_2 + ANR ), 2.0 ); // add "A1X + A1X1 + " for exotics
	MeL += ad;
	/*if(ad != ad){
		printf("MX=%10.2e\n", MX);
		printf("MB=%10.2e\n", MB);
		printf("MK=%10.2e\n", MK);
		printf("A1X=%10.2e\n", abs(A1X));
		printf("A2X=%10.2e\n", abs(A2X));
		printf("AK1410_1=%10.2e\n", abs(AK1410_1));
		printf("AK1430_0=%10.2e\n", abs(AK1430_0));
		printf("AK1430_2=%10.2e\n", abs(AK1430_2));
		printf("AK1680_1=%10.2e\n", abs(AK1680_1));
		printf("AK1780_3=%10.2e\n", abs(AK1780_3));
		printf("AK1950_0=%10.2e\n", abs(AK1950_0));
		printf("AK2045_4=%10.2e\n", abs(AK2045_4));
		
		getchar();
	}*/


	}

	return MeL/3.0;


}



/*

double hadronmass1[CHANNELS] = { MASS_PROTON * MEV, MASS_KAON * MEV, MASS_KAON * MEV, MASS_PION * MEV, MASS_KAON * MEV, MASS_PION * MEV, MASS_PROTON * MEV, MASS_KAON * MEV, MASS_PROTON * MEV, MASS_KAON * MEV, MASS_PROTON * MEV, MASS_PION * MEV };
double hadronmass2[CHANNELS] = { MASS_KAON * MEV, MASS_PROTON * MEV, MASS_KAON * MEV, MASS_PION * MEV, MASS_PION * MEV, MASS_KAON * MEV, MASS_KAON * MEV, MASS_PROTON * MEV, MASS_KAON * MEV, MASS_PROTON * MEV, MASS_PION * MEV, MASS_PROTON * MEV };


////// kinematic variables:
	if (j==4)
			{
			v_p1.SetXYZM( GEV*mc_momentum->at(1).Px(), GEV*mc_momentum->at(1).Py(), GEV*mc_momentum->at(1).Pz(), hadronmass1[j] );
			v_K2.SetXYZM( GEV*mc_momentum->at(2).Px(), GEV*mc_momentum->at(2).Py(), GEV*mc_momentum->at(2).Pz(), hadronmass2[j] );
			    vLambda_pK = v_Jpsi1 + v_p1 + v_K2;
				vJpsip = v_Jpsi1 + v_p1;
				vJpsiK = v_Jpsi1 + v_K2;
				v_pK = v_p1 + v_K2;

				BPhS_mc_mass_Kpi[evcounter] = GeV*v_pK.Mag();
				BPhS_mc_mass_X[evcounter] = GeV*vJpsiK.Mag();
				BPhS_mc_theta_X[evcounter] = theta_Pc(vLambda_pK, vJpsip, v_Jpsi1);
				BPhS_mc_theta_psi[evcounter] = theta_psi(vJpsip, v_Jpsi1, v_mu2);
				BPhS_mc_theta_psiKstar[evcounter] = theta_psi(vLambda_pK, v_Jpsi1, v_mu2);
				BPhS_mc_theta_Kstar[evcounter] = theta_Lstar( vLambda_pK, v_pK, v_K2 );
				BPhS_mc_phi_mu[evcounter] = phi_mu_Pc(vJpsip, v_K2, v_Jpsi1, v_mu2);
				BPhS_mc_phi_muKstar[evcounter] = phi_mu_Lstar(vLambda_pK, v_p1+v_K2, v_Jpsi1, v_mu2);
				BPhS_mc_phi_K[evcounter] = phi_K(vLambda_pK, v_pK, v_K2, v_Jpsi1);
				BPhS_mc_alpha_mu[evcounter] = alpha_mu(v_mu2, v_Jpsi1, vJpsip, vLambda_pK);
	}
*/



/////////////////////////////////////// Переделанные углы:

Double_t theta_B0(TLorentzVector vBd, TLorentzVector vKstar)
{
    TVector3 vBd3 = vBd.BoostVector();
	TLorentzVector vKstar_b = vKstar;
	vKstar_b.Boost(-vBd3);
return vBd.Vect().Angle( vKstar_b.Vect() );
}

Double_t phi_psi(TLorentzVector vB0, TLorentzVector vZc, TLorentzVector vpsi)
{
	TLorentzVector vZc_b = vZc;
	TLorentzVector vpsi_b = vpsi;
	TVector3 vZc3 = vZc.BoostVector();
    TVector3 vB03 = vB0.BoostVector();
	vZc_b.Boost(-vB03);
	vpsi_b.Boost(-vZc3);

	TVector3 x0 = -vB0.Vect().Unit() + ( vB0.Vect().Unit() * vZc_b.Vect().Unit() ) * vZc_b.Vect().Unit();
	
	double argy = ( vZc_b.Vect().Unit().Cross(x0.Unit()) ) * vpsi_b.Vect().Unit();
	double argx = vpsi_b.Vect().Unit() * x0.Unit();

	return atan2( argy, argx );
}

Double_t phi_K(TLorentzVector vBd, TLorentzVector vKstar, TLorentzVector vK, TLorentzVector vpsi)
{
	TLorentzVector vK_b = vK;
	TLorentzVector vKstarb = vKstar;
	TLorentzVector vpsib = vpsi;
	TLorentzVector vKb = vK;
	TVector3 vBd3 = vBd.BoostVector();
	TVector3 vKstar3 = vKstar.BoostVector();
	

	vKstarb.Boost(-vBd3);
	vpsib.Boost(-vKstar3);
	vKb.Boost(-vKstar3);

	TVector3 x0 = - vBd.Vect().Unit() + ( vBd.Vect().Unit() * vKstarb.Vect().Unit() ) * vKstarb.Vect().Unit();
	
	
	double argy = ( -vpsib.Vect().Unit().Cross(x0.Unit()) ) * vKb.Vect().Unit();
	double argx = x0.Unit() * vKb.Vect().Unit();

	return atan2( argy, argx );
}

Double_t alpha_mu(TLorentzVector vmu_2, TLorentzVector vpsi, TLorentzVector vZc, TLorentzVector vBd)
{
	TLorentzVector vmu2_b = vmu_2;	
	TLorentzVector vpsi_b = vpsi;
	TLorentzVector vpsi_b2 = vpsi;
	//TLorentzVector vLambda_b = vLambda;
	TVector3 vBd3 = vBd.BoostVector();
	TVector3 vZc3 = vZc.BoostVector();
	TVector3 vpsi3 = vpsi.BoostVector();
	

	vmu2_b.Boost(-vpsi3);
	vpsi_b.Boost(-vBd3);
	vpsi_b2.Boost(-vZc3);


	TVector3 x1 = - vpsi_b2.Vect().Unit() + ( vpsi_b2.Vect().Unit() * vmu2_b.Vect().Unit() ) * vmu2_b.Vect().Unit();
	TVector3 x2 = - vpsi_b.Vect().Unit() + ( vpsi_b.Vect().Unit() * vmu2_b.Vect().Unit() ) * vmu2_b.Vect().Unit();
	
	
	double argy = ( vmu2_b.Vect().Unit().Cross(x1.Unit()) ) * x2.Unit();
	double argx = x1.Unit() * x2.Unit();

	return atan2( argy, argx );
}

Double_t phi_mu_Kstar(TLorentzVector vBd, TLorentzVector vKstar, TLorentzVector vpsi, TLorentzVector v_mu2)
{
	TLorentzVector vKstar_b = vKstar;
	TLorentzVector vpsi_b = vpsi;
	TVector3 vBd3 = vBd.BoostVector();
	
	vpsi_b.Boost(-vBd3);
	vKstar_b.Boost(-vBd3);

	TVector3 x0 = -vBd.Vect().Unit() + ( vBd.Vect().Unit() * vKstar_b.Vect().Unit() ) * vKstar_b.Vect().Unit();
	
	TLorentzVector v_mu2_b = v_mu2;
	TVector3 vpsi3 = vpsi.BoostVector();
	
	v_mu2_b.Boost(-vpsi3);
	
	double argy = ( vpsi_b.Vect().Unit().Cross(x0.Unit()) ) * v_mu2_b.Vect().Unit();
	double argx = v_mu2_b.Vect().Unit() * x0.Unit();

	return atan2( argy, argx );
}

Double_t phi_mu_X(TLorentzVector vZc, TLorentzVector vB0, TLorentzVector vpsi, TLorentzVector v_mu2)
{
	TLorentzVector vZc_b = vZc;
	TLorentzVector vpsi_b = vpsi;
	TVector3 vZc3 = vZc.BoostVector();
    TVector3 vB03 = vB0.BoostVector();
	vZc_b.Boost(-vB03);
	vpsi_b.Boost(-vZc3);

	TVector3 x0 = -vZc_b.Vect().Unit() + ( vZc_b.Vect().Unit() * vpsi_b.Vect().Unit() ) * vpsi_b.Vect().Unit();
	
	
	TLorentzVector v_mu2_b = v_mu2;
	TVector3 vpsi3 = vpsi.BoostVector();
	v_mu2_b.Boost(-vpsi3);
	
	double argy = ( vpsi_b.Vect().Unit().Cross(x0.Unit()) ) * v_mu2_b.Vect().Unit();
	double argx = v_mu2_b.Vect().Unit() * x0.Unit();

	return atan2( argy, argx );
}

///// angle between Jpsi and muon in Jpsi rest frame, Jpsi momentum is taken in Pc rest frame;
Double_t theta_psi_X(TLorentzVector vZc, TLorentzVector vpsi, TLorentzVector v_mu2)
{

	TVector3 vZc3 = vZc.BoostVector();
	TVector3 vpsi3 = vpsi.BoostVector();
	TLorentzVector v_mu2_b = v_mu2; 
	v_mu2_b.Boost(-vpsi3);
	TLorentzVector vpsi_b = vpsi;
	vpsi_b.Boost(-vZc3);

return v_mu2_b.Vect().Angle(vpsi_b.Vect());
}

//////////////////////////////////////////////////////////////////////////////////////////////
///// angle between Jpsi and Pc in Pc rest frame, Pc momentum is taken in Lambda_b rest frame;
Double_t theta_X(TLorentzVector vBd, TLorentzVector vZc, TLorentzVector vpsi)
{
	TVector3 vBd3 = vBd.BoostVector();
	TVector3 vZc3 = vZc.BoostVector();
	TLorentzVector vpsi_b = vpsi;
	vpsi_b.Boost(-vZc3);
	TLorentzVector vZc_b2 = vZc;
	vZc_b2.Boost(-vBd3);

return vpsi_b.Vect().Angle( vZc_b2.Vect() );
}

//////////////////////////////////////////////////////////////////////////////////////////////
///// angle between Kaon and Lstar candidate in Lstar rest frame, Lstar momentum is taken in Lambda_b rest frame;
Double_t theta_Kstar( TLorentzVector vBd, TLorentzVector vKstar, TLorentzVector vK )
{
	
	TVector3 vBd3 = vBd.BoostVector();
	TVector3 vKstar3 = vKstar.BoostVector();

	TLorentzVector vK_b = vK;
	vK_b.Boost(-vKstar3);
	
	TLorentzVector vKstarb = vKstar;
	vKstarb.Boost(-vBd3);
	return vK_b.Vect().Angle(vKstarb.Vect()) ;

}

///////////////////////////////////////////////////////////////////////////////////////////////
///// angle between between Jpsi and muon in Jpsi rest frame, Jpsi momentum is taken in Bd rest frame;
Double_t theta_psi_Kstar(TLorentzVector vBd, TLorentzVector vpsi, TLorentzVector v_mu2)
{

	TVector3 vBd3 = vBd.BoostVector();
	TVector3 vpsi3 = vpsi.BoostVector();
	TLorentzVector v_mu2_b = v_mu2; 
	v_mu2_b.Boost(-vpsi3);
	TLorentzVector vpsi_b = vpsi;
	vpsi_b.Boost(-vBd3);

return v_mu2_b.Vect().Angle(vpsi_b.Vect());
}

Double_t s_calculation(TLorentzVector vjpsi, TLorentzVector vK, TLorentzVector vpi)
{
	float t = 0.61803, a, b, t1, t2;
    float m, M, M0 = 4800., M1 = 4900., m0 = 5250., m1 = 5310.;
    TLorentzVector vjpsi1, vK1, vpi1, vjpsi2, vK2, vpi2;
    M = (vjpsi + vK + vpi).M();
    m = m0 + (m1 - m0) / (M1 - M0) * (M - M0);
    a = 1.;
    b = 2.;
    t1 = a + (b - a) * (1 - t);
    t2 = a + (b - a) * t;
    while (b - a > 0.0001)
    {
        vjpsi1.SetXYZM(t1 * vjpsi.Px(), t1 * vjpsi.Py(), t1 * vjpsi.Pz(), 3096.916);
        vK1.SetXYZM(t1 * vK.Px(), t1 * vK.Py(), t1 * vK.Pz(), 493.677);
        vpi1.SetXYZM(t1 * vpi.Px(), t1 * vpi.Py(), t1 * vpi.Pz(), 139.57);
        vjpsi2.SetXYZM(t2 * vjpsi.Px(), t2 * vjpsi.Py(), t2 * vjpsi.Pz(), 3096.916);
        vK2.SetXYZM(t2 * vK.Px(), t2 * vK.Py(), t2 * vK.Pz(), 493.677);
        vpi2.SetXYZM(t2 * vpi.Px(), t2 * vpi.Py(), t2 * vpi.Pz(), 139.57);
        if (abs(m - (vjpsi1 + vK1 + vpi1).M()) > abs((vjpsi2 + vK2 + vpi2).M() - m))
        {
            a = t1;
            t1 = t2;
            t2 = a + (b - a) * t;
        }
        else
        {
            b = t2;
            t2 = t1;
            t1 = a + (b - a) * (1 - t);
        }
    }
    vjpsi1.SetXYZM(t1 * vjpsi.Px(), t1 * vjpsi.Py(), t1 * vjpsi.Pz(), 3096.916);
    vK1.SetXYZM(t1 * vK.Px(), t1 * vK.Py(), t1 * vK.Pz(), 493.677);
    vpi1.SetXYZM(t1 * vpi.Px(), t1 * vpi.Py(), t1 * vpi.Pz(), 139.57);
    vjpsi2.SetXYZM(t2 * vjpsi.Px(), t2 * vjpsi.Py(), t2 * vjpsi.Pz(), 3096.916);
    vK2.SetXYZM(t2 * vK.Px(), t2 * vK.Py(), t2 * vK.Pz(), 493.677);
    vpi2.SetXYZM(t2 * vpi.Px(), t2 * vpi.Py(), t2 * vpi.Pz(), 139.57);
    if (abs(m - (vjpsi1 + vK1 + vpi1).M()) > abs((vjpsi2 + vK2 + vpi2).M() - m)) 
    {
        //cout << (vjpsi2 + vK2 + vpi2).M()  << "    " << m << endl;
        return t2;
    }
    else
    {
        //cout << (vjpsi1 + vK1 + vpi1).M() << "    " << m << endl;
        return t1;
    }
}

Double_t s_mc_calculation(TLorentzVector vjpsi, TLorentzVector vK, TLorentzVector vpi)
{
	float t = 0.61803, a, b, t1, t2;
    float m, M, M0 = 4800., M1 = 4900., m0 = 5250., m1 = 5310.;
    TLorentzVector vjpsi1, vK1, vpi1, vjpsi2, vK2, vpi2;
    M = (vjpsi + vK + vpi).M();
    m = 5279.64;
    a = 1.;
    b = 2.;
    t1 = a + (b - a) * (1 - t);
    t2 = a + (b - a) * t;
    while (b - a > 0.0001)
    {
        vjpsi1.SetXYZM(t1 * vjpsi.Px(), t1 * vjpsi.Py(), t1 * vjpsi.Pz(), 3096.916);
        vK1.SetXYZM(t1 * vK.Px(), t1 * vK.Py(), t1 * vK.Pz(), 493.677);
        vpi1.SetXYZM(t1 * vpi.Px(), t1 * vpi.Py(), t1 * vpi.Pz(), 139.57);
        vjpsi2.SetXYZM(t2 * vjpsi.Px(), t2 * vjpsi.Py(), t2 * vjpsi.Pz(), 3096.916);
        vK2.SetXYZM(t2 * vK.Px(), t2 * vK.Py(), t2 * vK.Pz(), 493.677);
        vpi2.SetXYZM(t2 * vpi.Px(), t2 * vpi.Py(), t2 * vpi.Pz(), 139.57);
        if (abs(m - (vjpsi1 + vK1 + vpi1).M()) > abs((vjpsi2 + vK2 + vpi2).M() - m))
        {
            a = t1;
            t1 = t2;
            t2 = a + (b - a) * t;
        }
        else
        {
            b = t2;
            t2 = t1;
            t1 = a + (b - a) * (1 - t);
        }
    }
    vjpsi1.SetXYZM(t1 * vjpsi.Px(), t1 * vjpsi.Py(), t1 * vjpsi.Pz(), 3096.916);
    vK1.SetXYZM(t1 * vK.Px(), t1 * vK.Py(), t1 * vK.Pz(), 493.677);
    vpi1.SetXYZM(t1 * vpi.Px(), t1 * vpi.Py(), t1 * vpi.Pz(), 139.57);
    vjpsi2.SetXYZM(t2 * vjpsi.Px(), t2 * vjpsi.Py(), t2 * vjpsi.Pz(), 3096.916);
    vK2.SetXYZM(t2 * vK.Px(), t2 * vK.Py(), t2 * vK.Pz(), 493.677);
    vpi2.SetXYZM(t2 * vpi.Px(), t2 * vpi.Py(), t2 * vpi.Pz(), 139.57);
    if (abs(m - (vjpsi1 + vK1 + vpi1).M()) > abs((vjpsi2 + vK2 + vpi2).M() - m)) 
    {
        //cout << (vjpsi2 + vK2 + vpi2).M()  << "    " << m << endl;
        return t2;
    }
    else
    {
        //cout << (vjpsi1 + vK1 + vpi1).M() << "    " << m << endl;
        return t1;
    }
}

//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__||   Matrix element Lb   ||__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//

std::complex<float> BreitWignerLstar( float ML, float M0L, float massLb, float gamma0, int LLstar, int LLb )
{
std::complex<float> ix(0,1);
const float massProton = MASS_PROTON;
const float massJpsi = MASS_JPSI;
const float massKaon = MASS_KAON;
const float massPion = MASS_PION;
const float massLambdaB = MASS_LB;
//const float massB = MASS_BD;
const float mplusL = massProton + massKaon; /// mass JPsi + mass proton

massLb = MASS_LB;



float argq0 = pow( M0L*M0L - massProton*massProton - massKaon*massKaon, 2.0 ) - 4.0 * massProton*massProton*massKaon*massKaon;
float argq1 = pow( ML*ML - massProton*massProton - massKaon*massKaon, 2.0 ) - 4.0 * massProton*massProton*massKaon*massKaon;
if(argq0 < 0.0 || argq1 < 0.0) return 0.0;
float q0 = sqrt(argq0)/2.0/M0L;
float q1 = sqrt(argq1)/2.0/ML;


float argp0 = pow( massLb*massLb - M0L*M0L - massJpsi*massJpsi, 2.0 ) - 4.0 * M0L*M0L*massJpsi*massJpsi;
float argp1 = pow( massLb*massLb - ML*ML - massJpsi*massJpsi, 2.0 ) - 4.0 * ML*ML*massJpsi*massJpsi;
if(argp0 < 0.0 || argp1 < 0.0) return 0.0;
float p0 = sqrt(argp0)/2.0/massLb;
float p1 = sqrt(argp1)/2.0/massLb;

float gamma = gamma0 * pow( q1/q0, 2*LLstar + 1) * M0L / ML * powf(BlattWeisskopf(q0, q1, 3.0, LLstar), 2.0);

///float gamma = gamma0;

if(gamma != gamma ){printf("in BWkstar gamma is nan"); getchar();}



float nom = 1.0;
if( BW_nom_Lstar == 1) nom = ML*sqrt(M0L*gamma);
std::complex<float> denom = nom/( M0L*M0L - ML*ML - ix*M0L*gamma );

float BlWsLB = BlattWeisskopf(p0, p1, 3.0, LLb);
float BlWsLK = BlattWeisskopf(q0, q1, 3.0, LLstar);

if(BlWsLB != BlWsLB) { printf("blatt LLb is NaN, p0=%10.2e\tp1=%10.2e\tLB=%d", p0, p1, LLb); getchar(); }
if(BlWsLK != BlWsLK) { printf("blatt LLstar is NaN, q0=%10.2e\tq1=%10.2e\tLK=%d", q0, q1, LLstar); getchar(); }

std::complex<float> Rm = (float)(BlWsLB*pow( p1/massLb, LLb ))*denom*(float)(BlWsLK*pow( q1/M0L, LLstar ));

//std::complex<float> Rm = (float)(BlWsLB*pow( p1/massLb, LLb ))*denom;
//std::complex<float> Rm = denom*(float)(BlWsLK*pow( q1/M0L, LLstar ));
//std::complex<float> Rm = denom;

/*
if(Rm != Rm){
printf("\nBW: %10.2e ", abs(denom));
printf("%10.1e ", BlattWeisskopf(p0, p1, 3.0, LB));
printf("%10.1e ", BlattWeisskopf(q0, q1, 3.0, LK));
printf("%10.1e ", pow( p1/massB, LB ) );
printf("%10.1e ", pow( q1/M0K, LK ) );
printf("%10.4e ", massB );
printf("%10.4e ", mplusB1 );
printf("%10.4e ", MK );
getchar();
}*/

return Rm;
}




std::complex<float> BreitWignerLstarNR( float ML, float M0L, float massLb, float gamma0, int LLstar, int LLb )
{
std::complex<float> ix(0,1);
const float massProton = MASS_PROTON;
const float massJpsi = MASS_JPSI;
const float massKaon = MASS_KAON;
const float massPion = MASS_PION;
const float massLambdaB = MASS_LB;
//const float massB = MASS_BD;
const float mplusL = massProton + massKaon; /// mass JPsi + mass proton

massLb = MASS_LB;



float argq0 = pow( M0L*M0L - massProton*massProton - massKaon*massKaon, 2.0 ) - 4.0 * massProton*massProton*massKaon*massKaon;
float argq1 = pow( ML*ML - massProton*massProton - massKaon*massKaon, 2.0 ) - 4.0 * massProton*massProton*massKaon*massKaon;
if(argq0 < 0.0 || argq1 < 0.0) return 0.0;
float q0 = sqrt(argq0)/2.0/M0L;
float q1 = sqrt(argq1)/2.0/ML;


float argp0 = pow( massLb*massLb - M0L*M0L - massJpsi*massJpsi, 2.0 ) - 4.0 * M0L*M0L*massJpsi*massJpsi;
float argp1 = pow( massLb*massLb - ML*ML - massJpsi*massJpsi, 2.0 ) - 4.0 * ML*ML*massJpsi*massJpsi;
if(argp0 < 0.0 || argp1 < 0.0) return 0.0;
float p0 = sqrt(argp0)/2.0/massLb;
float p1 = sqrt(argp1)/2.0/massLb;

float gamma = gamma0 * pow( q1/q0, 2*LLstar + 1) * M0L / ML * powf(BlattWeisskopf(q0, q1, 3.0, LLstar), 2.0);

///float gamma = gamma0;

if(gamma != gamma ){printf("in BWkstar gamma is nan"); getchar();}


float nom = 1.0;
std::complex<float> denom = nom;

float BlWsLB = BlattWeisskopf(p0, p1, 3.0, LLb);
float BlWsLK = BlattWeisskopf(q0, q1, 3.0, LLstar);

if(BlWsLB != BlWsLB) { printf("blatt LLb is NaN, p0=%10.2e\tp1=%10.2e\tLB=%d", p0, p1, LLb); getchar(); }
if(BlWsLK != BlWsLK) { printf("blatt LLstar is NaN, q0=%10.2e\tq1=%10.2e\tLK=%d", q0, q1, LLstar); getchar(); }

std::complex<float> Rm = (float)(BlWsLB*pow( p1/massLb, LLb ))*denom*(float)(BlWsLK*pow( q1/M0L, LLstar ));

return Rm;
}

double LambdabFactor( double Mpc, double M0pc, int LL )
{
return 1.0;
//std::complex<double> ix(0,1);
const double massProton = MASS_PROTON;
const double massJpsi = MASS_JPSI;
const double massKaon = MASS_KAON;
const double massLambdaB = MASS_LB;
double mplusPc = massProton + massJpsi; /// mass JPsi + mass proton

double mplusLambda0 = M0pc + massKaon;
double mplusLambda1 = Mpc + massKaon;


double arg0 = pow( massLambdaB*massLambdaB -  M0pc*M0pc - massJpsi*massJpsi, 2.0) - 4.0*M0pc*M0pc*massJpsi*massJpsi;
double arg1 = pow( massLambdaB*massLambdaB -  Mpc*Mpc - massJpsi*massJpsi, 2.0) - 4.0*Mpc*Mpc*massJpsi*massJpsi;

if(arg0 <= 0.0 || arg1 <= 0.0) return 0.0;

//double p0 = sqrt(massLambdaB*massLambdaB - mplusLambda0*mplusLambda0 ); 
//double p1 = sqrt(massLambdaB*massLambdaB - mplusLambda1*mplusLambda1 );

double p0 = sqrt(arg0)/2.0/massLambdaB;
double p1  = sqrt(arg1)/2.0/massLambdaB;

double factor = BlattWeisskopf(p0, p1, 3.0, LL) * pow( p1/massLambdaB, LL );

return factor;
}

std::complex<float> BreitWignerPc( float Mpc, float M0pc, float gamma0, int Lpc )
{
std::complex<float> ix(0,1);
const float massProton = MASS_PROTON;
const float massJpsi = MASS_JPSI;
const float massKaon = MASS_KAON;
const float massLambdaB = MASS_LB;
float mplusPc = massProton + massJpsi; /// mass JPsi + mass proton

//float q0 = sqrt(M0pc*M0pc - mplusPc*mplusPc); 
//float q1 = sqrt(Mpc*Mpc - mplusPc*mplusPc);

float argq0 = pow( M0pc*M0pc - massJpsi*massJpsi - massProton*massProton, 2.0 ) - 4.0 * massJpsi*massJpsi*massProton*massProton;
float argq1 = pow( Mpc*Mpc - massJpsi*massJpsi - massProton*massProton, 2.0 ) - 4.0 * massJpsi*massJpsi*massProton*massProton;
if(argq0 < 0.0 || argq1 < 0.0) return 0.0;
float q0 = sqrt(argq0)/2.0/M0pc;
float q1 = sqrt(argq1)/2.0/Mpc;


//float argp0 = pow( massLambdaB*massLambdaB - M0pc*M0pc - massKaon*massKaon, 2.0 ) - 4.0 * M0pc*M0pc*massKaon*massKaon;
//float argp1 = pow( massLambdaB*massLambdaB - Mpc*Mpc - massKaon*massKaon, 2.0 ) - 4.0 * Mpc*Mpc*massKaon*massKaon;
//if(argp0 < 0.0 || argp1 < 0.0) return 0.0;
//float p0 = sqrt(argp0)/2.0/massLambdaB;
//float p1 = sqrt(argp1)/2.0/massLambdaB;


float gamma = gamma0 * pow( q1/q0, 2*Lpc + 1) * M0pc / Mpc * powf(BlattWeisskopf(q0, q1, 3.0, Lpc) , 2.0);
float nom = 1.0;
if( BW_nom_Pc == 1) nom = Mpc*sqrt(M0pc*gamma);

std::complex<float> denom = nom/( M0pc*M0pc - Mpc*Mpc - ix*M0pc*gamma );
std::complex<float> Rm = denom * (float)BlattWeisskopf(q0, q1, 3.0, Lpc) * (float)pow( q1/M0pc, Lpc );
//std::complex<float> Rm = BlattWeisskopf(p0, p1, 3.0, LL) * pow( p1/massLambdaB, LL ) * denom * BlattWeisskopf(q0, q1, 3.0, Lpc) * pow( q1/M0pc, Lpc );
return Rm;
}

std::complex<float> Hamplitude12minus(float M, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bs1, std::complex<float> Bs2, std::complex<float> Bs3,  float hL, float hp, float hmu, float thetaPc, float thetaL, float thetaPsi, float phimu, float phiPc, float phiPsi, float alpha_mu, float thetap )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = cosf(hL*phiPc) + ix*sinf(hL*phiPc); //exp(ix*hL*phiPc);

std::complex<float> ePsi1 = cosf(0.5*phiPsi) + ix*sinf(0.5*phiPsi); //exp(ix*phiPsi);
std::complex<float> ePsi2 = cosf(0.5*phiPsi) - ix*sinf(0.5*phiPsi); //exp(-ix*phiPsi);

std::complex<float> ealpha = cosf(hmu*alpha_mu) + ix*sinf(hmu*alpha_mu); //exp(-ix*phiPsi);

std::complex<float> Hs1, Hs2, Hs3, Hw1, Hw2;


Hw1 = sqrtf(0.5)*Bw1 * (float)LambdabFactor( M, M0, 0 ) - sqrtf(0.5)*Bw2 * (float)LambdabFactor( M, M0, 1 );  //lPc = 1/2
Hw2 = sqrtf(0.5)*Bw1 * (float)LambdabFactor( M, M0, 0 ) + sqrtf(0.5)*Bw2 * (float)LambdabFactor( M, M0, 1 );  //lPc = -1/2



std::complex<float> Ampl1, Ampl2, Ampl3, Ampl4;

float hppc = 0.5;
	Hs1 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerPc( M, M0, gamma, 0) + (float)(sqrt(1.0/6.0)) * Bs2 * BreitWignerPc( M, M0, gamma, 2);   //H_1_1/2
	Hs2 = (float)(1.0/sqrt(6.0))*Bs1*BreitWignerPc( M, M0, gamma, 0) - (float)(sqrt(1.0/3.0)) * Bs2 * BreitWignerPc( M, M0, gamma, 2);   //H_0_1/2
Ampl1 = 0.0;
Ampl1 += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(0.5, 0.5, 1.0 - hppc, thetaPc);
Ampl1 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(0.5, 0.5, - hppc, thetaPc);
////Ampl1 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, -1.0 - hppc, thetaPc);
Ampl1 *= Hw1 * ePsi1 * eL * Wigner_matrix(0.5, hL, 0.5, thetaL);

Ampl2 = 0.0;
Ampl2  += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(0.5, -0.5, 1.0 - hppc, thetaPc);
Ampl2 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(0.5, -0.5, - hppc, thetaPc);
///Ampl2 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, -1.0 - hppc, thetaPc);
Ampl2 *= Hw2 * ePsi2 * eL * Wigner_matrix(0.5, hL, -0.5, thetaL);

Ampl1 += Ampl2;
Ampl1 *= Wigner_matrix(0.5, hppc, hp, thetap);

hppc = -0.5;
	Hs3 = -(float)(1.0/sqrt(3.0))*Bs1*BreitWignerPc( M, M0, gamma, 0) - (float)(sqrt(1.0/6.0)) * Bs2 * BreitWignerPc( M, M0, gamma, 2);   //H_-1_-1/2
	Hs2 = -(float)(1.0/sqrt(6.0))*Bs1*BreitWignerPc( M, M0, gamma, 0) + (float)(sqrt(1.0/3.0)) * Bs2 * BreitWignerPc( M, M0, gamma, 2);   //H_0_-1/2
Ampl3 = 0.0;
//Ampl3 += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, 1.0 - hppc, thetaPc);
Ampl3 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(0.5, 0.5, - hppc, thetaPc);
Ampl3 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(0.5, 0.5, -1.0 - hppc, thetaPc);
Ampl3 *= Hw1 * ePsi1 * eL * Wigner_matrix(0.5, hL, 0.5, thetaL);

Ampl4 = 0.0;
//Ampl4  += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, 1.0 - hppc, thetaPc);
Ampl4 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(0.5, -0.5, - hppc, thetaPc);
Ampl4 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(0.5, -0.5, -1.0 - hppc, thetaPc);
Ampl4 *= Hw2 * ePsi2 * eL * Wigner_matrix(0.5, hL, -0.5, thetaL);

Ampl3 += Ampl4;
Ampl3 *= Wigner_matrix(0.5, hppc, hp, thetap);

Ampl1 += Ampl3;

Ampl1 *= ealpha; 

return Ampl1;
}



std::complex<float> Hamplitude12plus(float M, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bs1, std::complex<float> Bs2, std::complex<float> Bs3,  float hL, float hp, float hmu, float thetaPc, float thetaL, float thetaPsi, float phimu, float phiPc, float phiPsi, float alpha_mu, float thetap )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = cosf(hL*phiPc) + ix*sinf(hL*phiPc); //exp(ix*hL*phiPc);

std::complex<float> ePsi1 = cosf(0.5*phiPsi) + ix*sinf(0.5*phiPsi); //exp(ix*phiPsi);
std::complex<float> ePsi2 = cosf(0.5*phiPsi) - ix*sinf(0.5*phiPsi); //exp(-ix*phiPsi);

std::complex<float> ealpha = cosf(hmu*alpha_mu) + ix*sinf(hmu*alpha_mu); //exp(-ix*phiPsi);

std::complex<float> Hs1, Hs2, Hs3, Hw1, Hw2;


Hw1 = sqrtf(0.5)*Bw1 * (float)LambdabFactor( M, M0, 0 ) - sqrtf(0.5)*Bw2 * (float)LambdabFactor( M, M0, 1 );  //lPc = 1/2
Hw2 = sqrtf(0.5)*Bw1 * (float)LambdabFactor( M, M0, 0 ) + sqrtf(0.5)*Bw2 * (float)LambdabFactor( M, M0, 1 );  //lPc = -1/2



std::complex<float> Ampl1, Ampl2, Ampl3, Ampl4;

float hppc = 0.5;
	Hs1 = -(float)(1.0/sqrt(3.0))*Bs1*BreitWignerPc( M, M0, gamma, 1) - (float)(sqrt(1.0/6.0)) * Bs2 * BreitWignerPc( M, M0, gamma, 1);   //H_1_1/2
	Hs2 = (float)(1.0/sqrt(6.0))*Bs1*BreitWignerPc( M, M0, gamma, 1) - (float)(sqrt(1.0/3.0)) * Bs2 * BreitWignerPc( M, M0, gamma, 1);   //H_0_1/2
Ampl1 = 0.0;
Ampl1 += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(0.5, 0.5, 1.0 - hppc, thetaPc);
Ampl1 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(0.5, 0.5, - hppc, thetaPc);
////Ampl1 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, -1.0 - hppc, thetaPc);
Ampl1 *= Hw1 * ePsi1 * eL * Wigner_matrix(0.5, hL, 0.5, thetaL);

Ampl2 = 0.0;
Ampl2  += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(0.5, -0.5, 1.0 - hppc, thetaPc);
Ampl2 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(0.5, -0.5, - hppc, thetaPc);
///Ampl2 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, -1.0 - hppc, thetaPc);
Ampl2 *= Hw2 * ePsi2 * eL * Wigner_matrix(0.5, hL, -0.5, thetaL);

Ampl1 += Ampl2;
Ampl1 *= Wigner_matrix(0.5, hppc, hp, thetap);

hppc = -0.5;
	Hs3 = -(float)(1.0/sqrt(3.0))*Bs1*BreitWignerPc( M, M0, gamma, 1) - (float)(sqrt(1.0/6.0)) * Bs2 * BreitWignerPc( M, M0, gamma, 1);   //H_-1_-1/2
	Hs2 = (float)(1.0/sqrt(6.0))*Bs1*BreitWignerPc( M, M0, gamma, 1) - (float)(sqrt(1.0/3.0)) * Bs2 * BreitWignerPc( M, M0, gamma, 1);   //H_0_-1/2
Ampl3 = 0.0;
//Ampl3 += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, 1.0 - hppc, thetaPc);
Ampl3 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(0.5, 0.5, - hppc, thetaPc);
Ampl3 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(0.5, 0.5, -1.0 - hppc, thetaPc);
Ampl3 *= Hw1 * ePsi1 * eL * Wigner_matrix(0.5, hL, 0.5, thetaL);

Ampl4 = 0.0;
//Ampl4  += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, 1.0 - hppc, thetaPc);
Ampl4 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(0.5, -0.5, - hppc, thetaPc);
Ampl4 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(0.5, -0.5, -1.0 - hppc, thetaPc);
Ampl4 *= Hw2 * ePsi2 * eL * Wigner_matrix(0.5, hL, -0.5, thetaL);

Ampl3 += Ampl4;
Ampl3 *= Wigner_matrix(0.5, hppc, hp, thetap);

Ampl1 += Ampl3;

Ampl1 *= ealpha; 

return Ampl1;
}




std::complex<float> Hamplitude32minus(float M, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bs1, std::complex<float> Bs2, std::complex<float> Bs3,  float hL, float hp, float hmu, float thetaPc, float thetaL, float thetaPsi, float phimu, float phiPc, float phiPsi, float alpha_mu, float thetap )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = cosf(hL*phiPc) + ix*sinf(hL*phiPc); //exp(ix*hL*phiPc);

std::complex<float> ePsi1 = cosf(0.5*phiPsi) + ix*sinf(0.5*phiPsi); //exp(ix*phiPsi);
std::complex<float> ePsi2 = cosf(0.5*phiPsi) - ix*sinf(0.5*phiPsi); //exp(-ix*phiPsi);

std::complex<float> ealpha = cosf(hmu*alpha_mu) + ix*sinf(hmu*alpha_mu); //exp(-ix*phiPsi);

std::complex<float> Hs1, Hs2, Hs3, Hw1, Hw2;

if(hp == 0.5) {
	
}
if(hp == -0.5) {
	
}

Hw1 = -sqrtf(0.5)*Bw1 * (float)LambdabFactor( M, M0, 1 ) + sqrtf(0.5)*Bw2 * (float)LambdabFactor( M, M0, 2 );
Hw2 = -sqrtf(0.5)*Bw1 * (float)LambdabFactor( M, M0, 1 ) - sqrtf(0.5)*Bw2 * (float)LambdabFactor( M, M0, 2 );



std::complex<float> Ampl1, Ampl2, Ampl3, Ampl4;

float hppc = 0.5;
    Hs1 = (float)(1.0/2.0/sqrt(3.0))*Bs1*BreitWignerPc( M, M0, gamma, 0) - ( (float)(sqrt(1.0/3.0))*Bs2 + (float)(1.0/2.0/sqrt(3.0))*Bs3 )* BreitWignerPc( M, M0, gamma, 2);
	Hs2 = (float)(sqrt(1.0/6.0))*Bs1 * BreitWignerPc( M, M0, gamma, 0) + ( (float)(sqrt(1.0/6.0))*Bs2 - (float)(sqrt(1.0/6.0))*Bs3 ) * BreitWignerPc( M, M0, gamma, 2);
	Hs3 = (float)(1.0/2.0)*Bs1 * BreitWignerPc( M, M0, gamma, 0) + (float)(1.0/2.0)*Bs3 * BreitWignerPc( M, M0, gamma, 2);
Ampl1 = 0.0;
Ampl1 += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, 1.0 - hppc, thetaPc);
Ampl1 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, - hppc, thetaPc);
Ampl1 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, -1.0 - hppc, thetaPc);
Ampl1 *= Hw1 * ePsi1 * eL * Wigner_matrix(0.5, hL, 0.5, thetaL);

Ampl2 = 0.0;
Ampl2  += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, 1.0 - hppc, thetaPc);
Ampl2 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, - hppc, thetaPc);
Ampl2 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, -1.0 - hppc, thetaPc);
Ampl2 *= Hw2 * ePsi2 * eL * Wigner_matrix(0.5, hL, -0.5, thetaL);

Ampl1 += Ampl2;
Ampl1 *= Wigner_matrix(0.5, hppc, hp, thetap);

hppc = -0.5;
    Hs3 = (float)(1.0/2.0/sqrt(3.0))*Bs1*BreitWignerPc( M, M0, gamma, 0) - ( (float)(sqrt(1.0/3.0))*Bs2 + (float)(1.0/2.0/sqrt(3.0))*Bs3) * BreitWignerPc( M, M0, gamma, 2);
	Hs2 = (float)(sqrt(1.0/6.0))*Bs1 * BreitWignerPc( M, M0, gamma, 0) + ( (float)(sqrt(1.0/6.0))*Bs2 - (float)(sqrt(1.0/6.0))*Bs3 ) * BreitWignerPc( M, M0, gamma, 2);
	Hs1 = (float)(1.0/2.0)*Bs1 * BreitWignerPc( M, M0, gamma, 0) + (float)(1.0/2.0)*Bs3 * BreitWignerPc( M, M0, gamma, 2);
Ampl3 = 0.0;
Ampl3 += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, 1.0 - hppc, thetaPc);
Ampl3 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, - hppc, thetaPc);
Ampl3 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, -1.0 - hppc, thetaPc);
Ampl3 *= Hw1 * ePsi1 * eL * Wigner_matrix(0.5, hL, 0.5, thetaL);

Ampl4 = 0.0;
Ampl4  += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, 1.0 - hppc, thetaPc);
Ampl4 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, - hppc, thetaPc);
Ampl4 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, -1.0 - hppc, thetaPc);
Ampl4 *= Hw2 * ePsi2 * eL * Wigner_matrix(0.5, hL, -0.5, thetaL);

Ampl3 += Ampl4;
Ampl3 *= Wigner_matrix(0.5, hppc, hp, thetap);

Ampl1 += Ampl3;

Ampl1 *= ealpha; 

return Ampl1;
}



std::complex<float> Hamplitude32plus(float M, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bs1, std::complex<float> Bs2, std::complex<float> Bs3,  float hL, float hp, float hmu, float thetaPc, float thetaL, float thetaPsi, float phimu, float phiPc, float phiPsi, float alpha_mu, float thetap )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = cosf(hL*phiPc) + ix*sinf(hL*phiPc); //exp(ix*hL*phiPc);

std::complex<float> ePsi1 = cosf(0.5*phiPsi) + ix*sinf(0.5*phiPsi); //exp(ix*phiPsi);
std::complex<float> ePsi2 = cosf(0.5*phiPsi) - ix*sinf(0.5*phiPsi); //exp(-ix*phiPsi);

std::complex<float> ealpha = cosf(hmu*alpha_mu) + ix*sinf(hmu*alpha_mu); //exp(-ix*phiPsi);

std::complex<float> Hs1, Hs2, Hs3, Hw1, Hw2;

if(hp == 0.5) {
		
}
if(hp == -0.5) {
	
}

Hw1 = -sqrtf(0.5)*Bw1 * (float)LambdabFactor( M, M0, 1 ) + sqrtf(0.5)*Bw2 * (float)LambdabFactor( M, M0, 2 );
Hw2 = -sqrtf(0.5)*Bw1 * (float)LambdabFactor( M, M0, 1 ) - sqrtf(0.5)*Bw2 * (float)LambdabFactor( M, M0, 2 );



std::complex<float> Ampl1, Ampl2, Ampl3, Ampl4;

float hppc = 0.5;
	Hs1 = (-(float)(1.0/sqrt(3.0))*Bs1 + (float)(sqrt(1.0/60.0))*Bs2 )*BreitWignerPc( M, M0, gamma, 1) - (float)(3.0/sqrt(60.0))*Bs3*BreitWignerPc( M, M0, gamma, 3);
	Hs2 = (-(float)(sqrt(1.0/6.0))*Bs1 -(float)(sqrt(1.0/30.0))*Bs2 )* BreitWignerPc( M, M0, gamma, 1) + (float)(sqrt(3.0/10.0))*Bs3*BreitWignerPc( M, M0, gamma, 3);
	Hs3 = (float)(3.0/2.0/sqrt(5.0))*Bs1 * BreitWignerPc( M, M0, gamma, 1) + (float)(1.0/2.0/sqrt(5.0))*Bs3 * BreitWignerPc( M, M0, gamma, 3);

Ampl1 = 0.0;
Ampl1 += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, 1.0 - hppc, thetaPc);
Ampl1 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, - hppc, thetaPc);
Ampl1 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, -1.0 - hppc, thetaPc);
Ampl1 *= Hw1 * ePsi1 * eL * Wigner_matrix(0.5, hL, 0.5, thetaL);

Ampl2 = 0.0;
Ampl2  += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, 1.0 - hppc, thetaPc);
Ampl2 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, - hppc, thetaPc);
Ampl2 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, -1.0 - hppc, thetaPc);
Ampl2 *= Hw2 * ePsi2 * eL * Wigner_matrix(0.5, hL, -0.5, thetaL);

Ampl1 += Ampl2;
Ampl1 *= Wigner_matrix(0.5, hppc, hp, thetap);

hppc = -0.5;
	Hs3 = -(-(float)(1.0/sqrt(3.0))*Bs1 + (float)(sqrt(1.0/60.0))*Bs2 )*BreitWignerPc( M, M0, gamma, 1) + (float)(3.0/sqrt(60.0))*Bs3*BreitWignerPc( M, M0, gamma, 3);
	Hs2 = -(-(float)(sqrt(1.0/6.0))*Bs1 -(float)(sqrt(1.0/30.0))*Bs2 )* BreitWignerPc( M, M0, gamma, 1) - (float)(sqrt(3.0/10.0))*Bs3*BreitWignerPc( M, M0, gamma, 3);
	Hs1 = -(float)(3.0/2.0/sqrt(5.0))*Bs1 * BreitWignerPc( M, M0, gamma, 1) - (float)(1.0/2.0/sqrt(5.0))*Bs3 * BreitWignerPc( M, M0, gamma, 3);

Ampl3 = 0.0;
Ampl3 += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, 1.0 - hppc, thetaPc);
Ampl3 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, - hppc, thetaPc);
Ampl3 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, -1.0 - hppc, thetaPc);
Ampl3 *= Hw1 * ePsi1 * eL * Wigner_matrix(0.5, hL, 0.5, thetaL);

Ampl4 = 0.0;
Ampl4  += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, 1.0 - hppc, thetaPc);
Ampl4 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, - hppc, thetaPc);
Ampl4 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, -1.0 - hppc, thetaPc);
Ampl4 *= Hw2 * ePsi2 * eL * Wigner_matrix(0.5, hL, -0.5, thetaL);

Ampl3 += Ampl4;
Ampl3 *= Wigner_matrix(0.5, hppc, hp, thetap);

Ampl1 += Ampl3;

Ampl1 *= ealpha; 

return Ampl1;
}



std::complex<float> Hamplitude52plus(float M, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bs1, std::complex<float> Bs2, std::complex<float> Bs3,  float hL, float hp, float hmu, float thetaPc, float thetaL, float thetaPsi, float phimu, float phiPc, float phiPsi, float alpha_mu, float thetap  )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = cosf(hL*phiPc) + ix*sinf(hL*phiPc); //exp(ix*hL*phiPc);

std::complex<float> ePsi1 = cosf(0.5*phiPsi) + ix*sinf(0.5*phiPsi); //exp(ix*phiPsi);
std::complex<float> ePsi2 = cosf(0.5*phiPsi) - ix*sinf(0.5*phiPsi); //exp(-ix*phiPsi);

std::complex<float> ealpha = cosf(hmu*alpha_mu) + ix*sinf(hmu*alpha_mu); //exp(-ix*phiPsi);

std::complex<float> Hs1, Hs2, Hs3, Hw1, Hw2;

if(hp == 0.5) {
	
}
if(hp == -0.5) {
	
}

Hw1 = sqrtf(0.5)*Bw1 * (float)LambdabFactor( M, M0, 2 ) - sqrtf(0.5)*Bw2 * (float)LambdabFactor( M, M0, 3 );
Hw2 = sqrtf(0.5)*Bw1 * (float)LambdabFactor( M, M0, 2 ) + sqrtf(0.5)*Bw2 * (float)LambdabFactor( M, M0, 3 );


std::complex<float> Ampl1, Ampl2, Ampl3, Ampl4;

float hppc = 0.5;
	Hs1 = (float)(sqrt(1.0/10.0))*Bs1 * BreitWignerPc( M, M0, gamma, 1) - ((float)(sqrt(1.0/3.0))*Bs2 + (float)(sqrt(1.0/15.0))*Bs3) * BreitWignerPc( M, M0, gamma, 3);
	Hs2 = (float)(sqrt(1.0/5.0))*Bs1 * BreitWignerPc( M, M0, gamma, 1) + ( (float)(sqrt(1.0/6.0))*Bs2 - (float)(sqrt(2.0/15.0))*Bs3) * BreitWignerPc( M, M0, gamma, 3);
	Hs3 = (float)(sqrt(1.0/5.0))*Bs1 * BreitWignerPc( M, M0, gamma, 1) + (float)(sqrt(3.0/10.0))*Bs3 * BreitWignerPc( M, M0, gamma, 3);

Ampl1 = 0.0;
Ampl1 += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(2.5, 0.5, 1.0 - hppc, thetaPc);
Ampl1 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(2.5, 0.5, - hppc, thetaPc);
Ampl1 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(2.5, 0.5, -1.0 - hppc, thetaPc);
Ampl1 *= Hw1 * ePsi1 * eL * Wigner_matrix(0.5, hL, 0.5, thetaL);

Ampl2 = 0.0;
Ampl2  = Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(2.5, -0.5, 1.0 - hppc, thetaPc);
Ampl2 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(2.5, -0.5, - hppc, thetaPc);
Ampl2 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(2.5, -0.5, -1.0 - hppc, thetaPc);
Ampl2 *= Hw2 * ePsi2 * eL * Wigner_matrix(0.5, hL, -0.5, thetaL);

Ampl1 += Ampl2;
Ampl1 *= Wigner_matrix(0.5, hppc, hp, thetap);

hppc = -0.5;
	Hs3 = (float)(sqrt(1.0/10.0))*Bs1 * BreitWignerPc( M, M0, gamma, 1) - ((float)(sqrt(1.0/3.0))*Bs2 + (float)(sqrt(1.0/15.0))*Bs3) * BreitWignerPc( M, M0, gamma, 3);
	Hs2 = (float)(sqrt(1.0/5.0))*Bs1 * BreitWignerPc( M, M0, gamma, 1) + ( (float)(sqrt(1.0/6.0))*Bs2 - (float)(sqrt(2.0/15.0))*Bs3) * BreitWignerPc( M, M0, gamma, 3);
	Hs1 = (float)(sqrt(1.0/5.0))*Bs1 * BreitWignerPc( M, M0, gamma, 1) + (float)(sqrt(3.0/10.0))*Bs3 * BreitWignerPc( M, M0, gamma, 3);
Ampl3 = 0.0;
Ampl3 += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(2.5, 0.5, 1.0 - hppc, thetaPc);
Ampl3 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(2.5, 0.5, - hppc, thetaPc);
Ampl3 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(2.5, 0.5, -1.0 - hppc, thetaPc);
Ampl3 *= Hw1 * ePsi1 * eL * Wigner_matrix(0.5, hL, 0.5, thetaL);


Ampl4 = 0.0;
Ampl4  = Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(2.5, -0.5, 1.0 - hppc, thetaPc);
Ampl4 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(2.5, -0.5, - hppc, thetaPc);
Ampl4 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(2.5, -0.5, -1.0 - hppc, thetaPc);
Ampl4 *= Hw2 * ePsi2 * eL * Wigner_matrix(0.5, hL, -0.5, thetaL);

Ampl3 += Ampl4;
Ampl3 *= Wigner_matrix(0.5, hppc, hp, thetap);

Ampl1 += Ampl3;
Ampl1 *= ealpha;


//Ampl1 =  BreitWignerPc( M, M0, gamma, 1);
//std::complex<float> denom = nom/( M0pc*M0pc - Mpc*Mpc - ix*M0pc*gamma );
//Ampl1 = 0.03*abs( (float)(1.0) / ( M0*M0 - M*M - ix*M0*gamma ) );
return Ampl1;
}



std::complex<float> Hamplitude52minus(float M, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bs1, std::complex<float> Bs2, std::complex<float> Bs3,  float hL, float hp, float hmu, float thetaPc, float thetaL, float thetaPsi, float phimu, float phiPc, float phiPsi, float alpha_mu, float thetap  )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = cosf(hL*phiPc) + ix*sinf(hL*phiPc); //exp(ix*hL*phiPc);

std::complex<float> ePsi1 = cosf(0.5*phiPsi) + ix*sinf(0.5*phiPsi); //exp(ix*phiPsi);
std::complex<float> ePsi2 = cosf(0.5*phiPsi) - ix*sinf(0.5*phiPsi); //exp(-ix*phiPsi);

std::complex<float> ealpha = cosf(hmu*alpha_mu) + ix*sinf(hmu*alpha_mu); //exp(-ix*phiPsi);

std::complex<float> Hs1, Hs2, Hs3, Hw1, Hw2;

if(hp == 0.5) {
	
}
if(hp == -0.5) {
	
}

Hw1 = sqrtf(0.5)*Bw1 * (float)LambdabFactor( M, M0, 2 ) - sqrtf(0.5)*Bw2 * (float)LambdabFactor( M, M0, 3 );
Hw2 = sqrtf(0.5)*Bw1 * (float)LambdabFactor( M, M0, 2 ) + sqrtf(0.5)*Bw2 * (float)LambdabFactor( M, M0, 3 );


std::complex<float> Ampl1, Ampl2, Ampl3, Ampl4;

float hppc = 0.5;
	Hs1 = ( -(float)(sqrt(1.0/3.0))*Bs1 + (float)(sqrt(1.0/42.0))*Bs2 )*BreitWignerPc( M, M0, gamma, 2) - (float)(sqrt(1.0/7.0))*Bs3 * BreitWignerPc( M, M0, gamma, 4);
	Hs2 = (-(float)(sqrt(1.0/6.0))*Bs1 - (float)(sqrt(1.0/21.0))*Bs2 )* BreitWignerPc( M, M0, gamma, 2) + (float)(sqrt(2.0/14.0))*Bs3 * BreitWignerPc( M, M0, gamma, 4);
	Hs3 = (float)(sqrt(3.0/7.0))*Bs1 * BreitWignerPc( M, M0, gamma, 2) + (float)(sqrt(1.0/14.0))*Bs3 * BreitWignerPc( M, M0, gamma, 4);
Ampl1 = 0.0;
Ampl1 += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(2.5, 0.5, 1.0 - hppc, thetaPc);
Ampl1 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(2.5, 0.5, - hppc, thetaPc);
Ampl1 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(2.5, 0.5, -1.0 - hppc, thetaPc);
Ampl1 *= Hw1 * ePsi1 * eL * Wigner_matrix(0.5, hL, 0.5, thetaL);

Ampl2 = 0.0;
Ampl2  = Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(2.5, -0.5, 1.0 - hppc, thetaPc);
Ampl2 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(2.5, -0.5, - hppc, thetaPc);
Ampl2 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(2.5, -0.5, -1.0 - hppc, thetaPc);
Ampl2 *= Hw2 * ePsi2 * eL * Wigner_matrix(0.5, hL, -0.5, thetaL);

Ampl1 += Ampl2;
Ampl1 *= Wigner_matrix(0.5, hppc, hp, thetap);

hppc = -0.5;
	Hs3 = -( -(float)(sqrt(1.0/3.0))*Bs1 + (float)(sqrt(1.0/42.0))*Bs2 )*BreitWignerPc( M, M0, gamma, 2) + (float)(sqrt(1.0/7.0))*Bs3 * BreitWignerPc( M, M0, gamma, 4);
	Hs2 = -(-(float)(sqrt(1.0/6.0))*Bs1 - (float)(sqrt(1.0/21.0))*Bs2 )* BreitWignerPc( M, M0, gamma, 2) - (float)(sqrt(2.0/14.0))*Bs3 * BreitWignerPc( M, M0, gamma, 4);
	Hs1 = -(float)(sqrt(3.0/7.0))*Bs1 * BreitWignerPc( M, M0, gamma, 2) - (float)(sqrt(1.0/14.0))*Bs3 * BreitWignerPc( M, M0, gamma, 4);
Ampl3 = 0.0;
Ampl3 += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(2.5, 0.5, 1.0 - hppc, thetaPc);
Ampl3 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(2.5, 0.5, - hppc, thetaPc);
Ampl3 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(2.5, 0.5, -1.0 - hppc, thetaPc);
Ampl3 *= Hw1 * ePsi1 * eL * Wigner_matrix(0.5, hL, 0.5, thetaL);


Ampl4 = 0.0;
Ampl4  = Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(2.5, -0.5, 1.0 - hppc, thetaPc);
Ampl4 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(2.5, -0.5, - hppc, thetaPc);
Ampl4 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(2.5, -0.5, -1.0 - hppc, thetaPc);
Ampl4 *= Hw2 * ePsi2 * eL * Wigner_matrix(0.5, hL, -0.5, thetaL);

Ampl3 += Ampl4;
Ampl3 *= Wigner_matrix(0.5, hppc, hp, thetap);

Ampl1 += Ampl3;
Ampl1 *= ealpha;

return Ampl1;
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////
//minimal Lstar12 //////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
std::complex<float> HamplitudeLstar12(int Lstarparity, float M, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hL, float hp, float hmu, float thetaLstar, float thetaL, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = 1.0; //cos(hL*phiLstar) + ix*sin(hL*phiLstar); //exp(ix*hL*phiPc);

//std::complex<float> eK0 = cosf(1.5*phiK) + ix*sinf(1.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK1 = cosf(0.5*phiK) + ix*sinf(0.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK2 = cosf(0.5*phiK) - ix*sinf(0.5*phiK); //exp(-ix*phiPsi);
//std::complex<float> eK3 = cosf(1.5*phiK) - ix*sinf(1.5*phiK); //exp(-ix*phiPsi);


std::complex<float> Ampl1 = 0.0;
std::complex<float> Hs1, Hw1, Hw2, Hw3, Hw4;

Hs1 = 1.0;
if(Lstarparity > 0 && hp == -0.5) Hs1 = -1.0;   /// do not do it when Lstar parity is negative

//int LLb = 0;
//if(Lstarparity > 0) LLb = 1;

int LLstar = 0;
if(Lstarparity > 0) LLstar = 1;

Hw1 = sqrtf(1.0/6.0)*Bw1;
Hw2 = sqrtf(1.0/3.0)*Bw1;
Hw3 = -sqrtf(1.0/3.0)*Bw1;
Hw4 = -sqrtf(1.0/6.0)*Bw1;


//BreitWignerLstar( float ML, float M0L, float massLb, float gamma0, int LLstar, int LLb )

Ampl1 += Hs1*Hw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK1 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 0.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw2*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK1 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 0.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) * emu2 * eL * eK2 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 0.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw4*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK2 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 0.5, -0.5, hp, thetaLstar );


//printf( "BW=%10.5e\t%10.5e\n", BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ).real(), BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ).imag() );
//printf( "WM_thetapsi=%10.5e\n", Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) );
//printf( "WM_thetaL=%10.5e\n", Wigner_matrix( 0.5, hL, 0.5, thetaL ) );
//printf( "WM_thetaLstar=%10.5e\n", Wigner_matrix( 0.5, 0.5, hp, thetaLstar ) );
//printf( "eK1=%10.5e\t%10.5e\n", eK1.real(), eK1.imag() );
//printf( "Hw1=%10.5e\t%10.5e\n", Hw1.real(), Hw1.imag() );

//printf( "ampl1=%10.5e\t%10.5e\n", (Hs1*Hw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK1 
//	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 0.5, 0.5, hp, thetaLstar )).real(), 
//		(Hs1*Hw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK1 
//	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 0.5, 0.5, hp, thetaLstar )).imag() );



return Ampl1;
}



//////////////////////////////////////////////////////////////////////////////////////////
//reloaded full Lstar12 //////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
std::complex<float> HamplitudeLstar12(int Lstarparity, float M, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, std::complex<float> Bw4, float hL, float hp, float hmu, float thetaLstar, float thetaL, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = 1.0; //cos(hL*phiLstar) + ix*sin(hL*phiLstar); //exp(ix*hL*phiPc);

//std::complex<float> eK0 = cosf(1.5*phiK) + ix*sinf(1.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK1 = cosf(0.5*phiK) + ix*sinf(0.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK2 = cosf(0.5*phiK) - ix*sinf(0.5*phiK); //exp(-ix*phiPsi);
//std::complex<float> eK3 = cosf(1.5*phiK) - ix*sinf(1.5*phiK); //exp(-ix*phiPsi);


std::complex<float> Ampl1 = 0.0;
std::complex<float> Hs1, Hw1, Hw2, Hw3, Hw4;

Hs1 = 1.0;
if(Lstarparity > 0 && hp == -0.5) Hs1 = -1.0;   
int LLb = 0;
if(Lstarparity > 0) LLb = 1;

int LLstar = 0;
if(Lstarparity > 0) LLstar = 1;

// lambda lstar = 0.5, lambdapsi = 0
Hw1 = sqrtf(1.0/6.0)*Bw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) - sqrtf(1.0/6.0)*Bw2*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 )
	- sqrtf(1.0/3.0)*Bw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 ) + sqrtf(1.0/3.0)*Bw4*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 2 );

// lambda lstar = 0.5, lambdapsi = 1
Hw2 = sqrtf(1.0/3.0)*Bw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) + sqrtf(1.0/3.0)*Bw2*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 )
	- sqrtf(1.0/6.0)*Bw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 ) - sqrtf(1.0/6.0)*Bw4*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 2 );

// lambda lstar = -0.5, lambdapsi = -1
Hw3 = -sqrtf(1.0/3.0)*Bw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) + sqrtf(1.0/3.0)*Bw2*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 )
	- sqrtf(1.0/6.0)*Bw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 ) + sqrtf(1.0/6.0)*Bw4*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 2 );

// lambda lstar = -0.5, lambdapsi = 0
Hw4 = -sqrtf(1.0/6.0)*Bw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) - sqrtf(1.0/6.0)*Bw2*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 )
	- sqrtf(1.0/3.0)*Bw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 ) - sqrtf(1.0/3.0)*Bw4*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 2 );



//BreitWignerLstar( float ML, float M0L, float massLb, float gamma0, int LLstar, int LLb )

Ampl1 += Hs1*Hw1* Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK1 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 0.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw2 * Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK1 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 0.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw3 * Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) * emu2 * eL * eK2 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 0.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw4 * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK2 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 0.5, -0.5, hp, thetaLstar );


return Ampl1;
}



//////////////////////////////////////////////////////////////////////////////////////////
//minimal Lstar32 //////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
std::complex<float> HamplitudeLstar32(int Lstarparity, float M, float M0, float gamma, std::complex<float> Bw1, float hL, float hp, float hmu, float thetaLstar, float thetaL, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = 1.0; //cos(hL*phiLstar) + ix*sin(hL*phiLstar); //exp(ix*hL*phiPc);

std::complex<float> eK0 = cosf(1.5*phiK) + ix*sinf(1.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK1 = cosf(0.5*phiK) + ix*sinf(0.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK2 = cosf(0.5*phiK) - ix*sinf(0.5*phiK); //exp(-ix*phiPsi);
std::complex<float> eK3 = cosf(1.5*phiK) - ix*sinf(1.5*phiK); //exp(-ix*phiPsi);

std::complex<float> Ampl1 = 0.0;
std::complex<float> Hs1;
std::complex<float> Hw1, Hw2, Hw3, Hw4, Hw5, Hw6;

Hs1 = 1.0;
if(Lstarparity < 0 && hp == -0.5) Hs1 = -1.0;   /// do not do it when Lstar parity is negative

//int LLb = 1;
//if(Lstarparity > 0) LLb = 2;

int LLstar = 1;
if(Lstarparity > 0) LLstar = 2;



Hw1 = -Bw1/(float)(2.0);
Hw2 = sqrtf(1.0/6.0)*Bw1;
Hw3 = -sqrtf(1.0/12.0)*Bw1;
Hw4 = -sqrtf(1.0/12.0)*Bw1;
Hw5 = sqrtf(1.0/6.0)*Bw1;
Hw6 = -Bw1/(float)(2.0);


//BreitWignerLstar( float ML, float M0L, float massLb, float gamma0, int LLstar, int LLb )
Ampl1 +=  Hs1*Hw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK0 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 1.5, 1.5, hp, thetaLstar ) ;

Ampl1 += Hs1*Hw2*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK1 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 1.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) *emu2 * eL * eK2 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 1.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw4*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK1 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 1.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw5*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK2 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 1.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw6*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) * emu2 * eL * eK3 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 1.5, -1.5, hp, thetaLstar );


return Ampl1;
}


//////////////////////////////////////////////////////////////////////////////////////////
//reloaded extended Lstar32 //////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
std::complex<float> HamplitudeLstar32(int Lstarparity, float M, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hL, float hp, float hmu, float thetaLstar, float thetaL, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = 1.0; //cos(hL*phiLstar) + ix*sin(hL*phiLstar); //exp(ix*hL*phiPc);

std::complex<float> eK0 = cosf(1.5*phiK) + ix*sinf(1.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK1 = cosf(0.5*phiK) + ix*sinf(0.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK2 = cosf(0.5*phiK) - ix*sinf(0.5*phiK); //exp(-ix*phiPsi);
std::complex<float> eK3 = cosf(1.5*phiK) - ix*sinf(1.5*phiK); //exp(-ix*phiPsi);

std::complex<float> Ampl1 = 0.0;
std::complex<float> Hs1;
std::complex<float> Hw1, Hw2, Hw3, Hw4, Hw5, Hw6;

Hs1 = 1.0;
if(Lstarparity < 0 && hp == -0.5) Hs1 = -1.0;   

int LLb = 1;
if(Lstarparity > 0) LLb = 2;

int LLstar = 1;
if(Lstarparity > 0) LLstar = 2;

Hw1 = Bw1/(float)(2.0)*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) - Bw2/(float)(2.0)*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 )
	-sqrtf(1.0/5.0)*Bw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 );

Hw2 = -sqrtf(1.0/6.0)*Bw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) + sqrtf(1.0/6.0)*Bw2*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 )
	-sqrtf(1.0/30.0)*Bw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 );

Hw3 = sqrtf(1.0/12.0)*Bw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) -sqrtf(1.0/12.0)*Bw2*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 ) 
	+ sqrtf(4.0/15.0)*Bw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 );

Hw4 = sqrtf(1.0/12.0)*Bw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) -sqrtf(1.0/12.0)*Bw2*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 ) 
	+ sqrtf(4.0/15.0)*Bw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 );

Hw5 = -sqrtf(1.0/6.0)*Bw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) - sqrtf(1.0/6.0)*Bw2*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 )
	+sqrtf(1.0/30.0)*Bw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 );

Hw6 = Bw1/(float)(2.0)*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) + Bw2/(float)(2.0)*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 )
	+sqrtf(1.0/5.0)*Bw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 );


//BreitWignerLstar( float ML, float M0L, float massLb, float gamma0, int LLstar, int LLb )
Ampl1 +=  Hs1*Hw1* Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK0 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 1.5, 1.5, hp, thetaLstar ) ;

Ampl1 += Hs1*Hw2*Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK1 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 1.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw3* Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) *emu2 * eL * eK2 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 1.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw4* Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK1 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 1.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw5* Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK2 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 1.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw6* Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) * emu2 * eL * eK3 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 1.5, -1.5, hp, thetaLstar );


return Ampl1;
}


bool test_flag = 1;

std::complex<float> HamplitudeLstar52(int Lstarparity, float M, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hL, float hp, float hmu, float thetaLstar, float thetaL, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = 1.0; ///cos(hL*phiLstar) + ix*sin(hL*phiLstar); //exp(ix*hL*phiPc);

std::complex<float> eK0 = cosf(1.5*phiK) + ix*sinf(1.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK1 = cosf(0.5*phiK) + ix*sinf(0.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK2 = cosf(0.5*phiK) - ix*sinf(0.5*phiK); //exp(-ix*phiPsi);
std::complex<float> eK3 = cosf(1.5*phiK) - ix*sinf(1.5*phiK); //exp(-ix*phiPsi);

std::complex<float> Ampl1 = 0.0;
std::complex<float> Hs1;
std::complex<float> Hw1, Hw2, Hw3, Hw4, Hw5, Hw6;

Hs1 = 1.0;
if(Lstarparity > 0 && hp == -0.5) Hs1 = -1.0;   /// do not do it when Lstar parity is negative
//int LLb = 2;
//if(Lstarparity > 0) LLb = 3;

int LLstar = 2;
if(Lstarparity > 0) LLstar = 3;

Hw1 = -sqrtf(0.2)*Bw1;
Hw2 = sqrtf(0.2)*Bw1;
Hw3 = -sqrtf(0.1)*Bw1;
Hw4 = -sqrtf(0.1)*Bw1;
Hw5 = sqrtf(0.2)*Bw1;
Hw6 = -sqrtf(0.2)*Bw1;


//BreitWignerLstar( float ML, float M0L, float massLb, float gamma0, int LLstar, int LLb )
Ampl1 +=  Hs1*Hw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 ) * Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK0 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 2.5, 1.5, hp, thetaLstar ) ;

Ampl1 += Hs1*Hw2*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK1 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 2.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 ) * Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) *emu2 * eL * eK2 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 2.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw4*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 ) * Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK1 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 2.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw5*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK2 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 2.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw6*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 ) * Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) * emu2 * eL * eK3 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 2.5, -1.5, hp, thetaLstar );
    
    /*if (((M0 - 2.110) < 0.01) && test_flag)
    {
        cout << "Amp52:\t" << abs(BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 )) << "\t" << abs(BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 ) * Bw1) << "\t" << abs(Ampl1) << endl;
    }*/

return Ampl1;
}


std::complex<float> HamplitudeLstar72(int Lstarparity, float M, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hL, float hp, float hmu, float thetaLstar, float thetaL, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = 1.0; ///cos(hL*phiLstar) + ix*sin(hL*phiLstar); //exp(ix*hL*phiPc);

std::complex<float> eK0 = cosf(1.5*phiK) + ix*sinf(1.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK1 = cosf(0.5*phiK) + ix*sinf(0.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK2 = cosf(0.5*phiK) - ix*sinf(0.5*phiK); //exp(-ix*phiPsi);
std::complex<float> eK3 = cosf(1.5*phiK) - ix*sinf(1.5*phiK); //exp(-ix*phiPsi);

std::complex<float> Ampl1 = 0.0;
std::complex<float> Hs1;
std::complex<float> Hw1, Hw2, Hw3, Hw4, Hw5, Hw6;

Hs1 = 1.0;
if(Lstarparity < 0 && hp == -0.5) Hs1 = -1.0;   /// do not do it when Lstar parity is negative
//int LLb = 3;
//if(Lstarparity > 0) LLb = 4;

int LLstar = 3;
if(Lstarparity > 0) LLstar = 4;

Hw1 = sqrtf(5.0/28.0)*Bw1;
Hw2 = -sqrtf(3.0/14.0)*Bw1;
Hw3 = sqrtf(3.0/28.0)*Bw1;
Hw4 = sqrtf(3.0/28.0)*Bw1;
Hw5 = -sqrtf(3.0/14.0)*Bw1;
Hw6 = sqrtf(5.0/28.0)*Bw1;


//BreitWignerLstar( float ML, float M0L, float massLb, float gamma0, int LLstar, int LLb )
Ampl1 +=  Hs1*Hw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 2 ) * Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK0 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 3.5, 1.5, hp, thetaLstar ) ;

Ampl1 += Hs1*Hw2*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 2 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK1 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 3.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 2 ) * Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) *emu2 * eL * eK2 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 3.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw4*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 2 ) * Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK1 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 3.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw5*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 2 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK2 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 3.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw6*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 2 ) * Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) * emu2 * eL * eK3 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 3.5, -1.5, hp, thetaLstar );

    /*if (((M0 - 2.100) < 0.01) && test_flag)
    {
        cout << "Amp72:\t" << abs(BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 2 )) << "\t" << abs(BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 2 ) * Bw1) << "\t" << abs(Ampl1) << endl;
    }*/
    
return Ampl1;
}




std::complex<float> HamplitudeLstar92(int Lstarparity, float M, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hL, float hp, float hmu, float thetaLstar, float thetaL, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = 1.0; ///cos(hL*phiLstar) + ix*sin(hL*phiLstar); //exp(ix*hL*phiPc);

std::complex<float> eK0 = cosf(1.5*phiK) + ix*sinf(1.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK1 = cosf(0.5*phiK) + ix*sinf(0.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK2 = cosf(0.5*phiK) - ix*sinf(0.5*phiK); //exp(-ix*phiPsi);
std::complex<float> eK3 = cosf(1.5*phiK) - ix*sinf(1.5*phiK); //exp(-ix*phiPsi);

std::complex<float> Ampl1 = 0.0;
std::complex<float> Hs1;
std::complex<float> Hw1, Hw2, Hw3, Hw4, Hw5, Hw6;

Hs1 = 1.0;
if(Lstarparity > 0 && hp == -0.5) Hs1 = -1.0;   /// do not do it when Lstar parity is negative
int LLb = 4;
if(Lstarparity > 0) LLb = 5;

int LLstar = 4;
if(Lstarparity > 0) LLstar = 5;

Hw1 = -sqrtf(1.0/6.0)*Bw1;
Hw2 = sqrtf(2.0/9.0)*Bw1;
Hw3 = -(float)(0.3333333)*Bw1;
Hw4 = -(float)(0.3333333)*Bw1;
Hw5 = sqrtf(2.0/9.0)*Bw1;
Hw6 = -sqrtf(1.0/6.0)*Bw1;


//BreitWignerLstar( float ML, float M0L, float massLb, float gamma0, int LLstar, int LLb )
Ampl1 +=  Hs1*Hw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 3 ) * Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK0 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 4.5, 1.5, hp, thetaLstar ) ;

Ampl1 += Hs1*Hw2*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 3 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK1 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 4.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 3 ) * Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) *emu2 * eL * eK2 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 4.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw4*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 3 ) * Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK1 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 4.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw5*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 3 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK2 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 4.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw6*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 3 ) * Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) * emu2 * eL * eK3 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 4.5, -1.5, hp, thetaLstar );


return Ampl1;
}


//////////////////////////////////////////////////////////////////////////////////////////
//nonresonant minimal Lstar12 //////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
std::complex<float> HamplitudeLstar12NR(int Lstarparity, float M, float M0, float gamma, std::complex<float> Bw1, float hL, float hp, float hmu, float thetaLstar, float thetaL, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = 1.0; //cos(hL*phiLstar) + ix*sin(hL*phiLstar); //exp(ix*hL*phiPc);

//std::complex<float> eK0 = cosf(1.5*phiK) + ix*sinf(1.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK1 = cosf(0.5*phiK) + ix*sinf(0.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK2 = cosf(0.5*phiK) - ix*sinf(0.5*phiK); //exp(-ix*phiPsi);
//std::complex<float> eK3 = cosf(1.5*phiK) - ix*sinf(1.5*phiK); //exp(-ix*phiPsi);


std::complex<float> Ampl1 = 0.0;
std::complex<float> Hs1, Hw1, Hw2, Hw3, Hw4;

Hs1 = 1.0;
if(Lstarparity > 0 && hp == -0.5) Hs1 = -1.0;   /// do not do it when Lstar parity is negative

//int LLb = 0;
//if(Lstarparity > 0) LLb = 1;

int LLstar = 0;
if(Lstarparity > 0) LLstar = 1;

Hw1 = sqrtf(1.0/6.0)*Bw1;
Hw2 = sqrtf(1.0/3.0)*Bw1;
Hw3 = -sqrtf(1.0/3.0)*Bw1;
Hw4 = -sqrtf(1.0/6.0)*Bw1;


//BreitWignerLstar( float ML, float M0L, float massLb, float gamma0, int LLstar, int LLb )

Ampl1 += Hs1*Hw1*BreitWignerLstarNR( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK1 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 0.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw2*BreitWignerLstarNR( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK1 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 0.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw3*BreitWignerLstarNR( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) * emu2 * eL * eK2 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 0.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw4*BreitWignerLstarNR( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK2 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 0.5, -0.5, hp, thetaLstar );



return Ampl1;
}


//////////////////////////////////////////////////////////////////////////////////////////
//nonresonant minimal Lstar32 //////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
std::complex<float> HamplitudeLstar32NR(int Lstarparity, float M, float M0, float gamma, std::complex<float> Bw1, float hL, float hp, float hmu, float thetaLstar, float thetaL, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = 1.0; //cos(hL*phiLstar) + ix*sin(hL*phiLstar); //exp(ix*hL*phiPc);

std::complex<float> eK0 = cosf(1.5*phiK) + ix*sinf(1.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK1 = cosf(0.5*phiK) + ix*sinf(0.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK2 = cosf(0.5*phiK) - ix*sinf(0.5*phiK); //exp(-ix*phiPsi);
std::complex<float> eK3 = cosf(1.5*phiK) - ix*sinf(1.5*phiK); //exp(-ix*phiPsi);

std::complex<float> Ampl1 = 0.0;
std::complex<float> Hs1;
std::complex<float> Hw1, Hw2, Hw3, Hw4, Hw5, Hw6;

Hs1 = 1.0;
if(Lstarparity < 0 && hp == -0.5) Hs1 = -1.0;   /// do not do it when Lstar parity is negative

//int LLb = 1;
//if(Lstarparity > 0) LLb = 2;

int LLstar = 1;
if(Lstarparity > 0) LLstar = 2;



Hw1 = -Bw1/(float)(2.0);
Hw2 = sqrtf(1.0/6.0)*Bw1;
Hw3 = -sqrtf(1.0/12.0)*Bw1;
Hw4 = -sqrtf(1.0/12.0)*Bw1;
Hw5 = sqrtf(1.0/6.0)*Bw1;
Hw6 = -Bw1/(float)(2.0);


//BreitWignerLstar( float ML, float M0L, float massLb, float gamma0, int LLstar, int LLb )
Ampl1 +=  Hs1*Hw1*BreitWignerLstarNR( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK0 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 1.5, 1.5, hp, thetaLstar ) ;

Ampl1 += Hs1*Hw2*BreitWignerLstarNR( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK1 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 1.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw3*BreitWignerLstarNR( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) *emu2 * eL * eK2 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 1.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw4*BreitWignerLstarNR( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK1 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 1.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw5*BreitWignerLstarNR( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK2 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 1.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw6*BreitWignerLstarNR( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) * emu2 * eL * eK3 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 1.5, -1.5, hp, thetaLstar );


return Ampl1;
}





float HamplitudePcLstar_total(float MPc, float MLstar, float MLb, float M01, float M02, float M03, float M04, float gamma1, float gamma2, float gamma3, float gamma4,
						std::complex<float> Pc1Bw1, std::complex<float> Pc1Bw2, std::complex<float> Pc1Bs1, std::complex<float> Pc1Bs2, std::complex<float> Pc1Bs3,
						std::complex<float> Pc2Bw1, std::complex<float> Pc2Bw2, std::complex<float> Pc2Bs1, std::complex<float> Pc2Bs2, std::complex<float> Pc2Bs3,
						std::complex<float> Pc3Bw1, std::complex<float> Pc3Bw2, std::complex<float> Pc3Bs1, std::complex<float> Pc3Bs2, std::complex<float> Pc3Bs3,
						std::complex<float> Pc4Bw1, std::complex<float> Pc4Bw2, std::complex<float> Pc4Bs1, std::complex<float> Pc4Bs2, std::complex<float> Pc4Bs3,
						std::complex<float> Ls1800_12_Bw1, std::complex<float> Ls1800_12_Bw2, std::complex<float> Ls1800_12_Bw3, std::complex<float> Ls1800_12_Bw4,
						std::complex<float> Ls1810_12_Bw1, std::complex<float> Ls1810_12_Bw2, std::complex<float> Ls1810_12_Bw3,
						std::complex<float> Ls1820_52_Bw1, std::complex<float> Ls1820_52_Bw2, std::complex<float> Ls1820_52_Bw3,
						std::complex<float> Ls1830_52_Bw1, std::complex<float> Ls1830_52_Bw2, std::complex<float> Ls1830_52_Bw3,
						std::complex<float> Ls1890_32_Bw1, std::complex<float> Ls1890_32_Bw2, std::complex<float> Ls1890_32_Bw3,
						std::complex<float> Ls2020_72_Bw1, std::complex<float> Ls2020_72_Bw2, std::complex<float> Ls2020_72_Bw3,
						std::complex<float> Ls2050_32_Bw1, std::complex<float> Ls2050_32_Bw2, std::complex<float> Ls2050_32_Bw3,
						std::complex<float> Ls2100_72_Bw1, std::complex<float> Ls2100_72_Bw2, std::complex<float> Ls2100_72_Bw3,
						std::complex<float> Ls2110_52_Bw1, std::complex<float> Ls2110_52_Bw2, std::complex<float> Ls2110_52_Bw3,
						std::complex<float> Ls2325_32_Bw1, std::complex<float> Ls2325_32_Bw2, std::complex<float> Ls2325_32_Bw3,
						std::complex<float> Ls2350_92_Bw1, std::complex<float> Ls2350_92_Bw2, std::complex<float> LsNR_12_Bw,
						float thetaLstar, float thetaLLstar, float thetaPsiLstar, float phimuLstar, float phiK,
						float thetaPc, float thetaLPc, float thetaPsiPc, float phimuPc, float phiPc, float phiPsi, 
						float alpha_mu, float thetap )
{
	
	float MeL = 0;
	
	std::complex<float> ix(0,1);

	for(int i1 = 0; i1 < 2; i1++)
	for(int i2 = 0; i2 < 2; i2++)
	for(int i3 = 0; i3 < 2; i3++)
	{
	
	float hL = i1 - 0.5;
	float hp = i2 - 0.5;
	float hmu = 2*i3 - 1.0;
	std::complex<float> A1Pc = 0.0;
	std::complex<float> A2Pc = 0.0;
	std::complex<float> A3Pc = 0.0;
	std::complex<float> A4Pc = 0.0;

	if( Pc_32minus_52plus == 1 ){
	 A1Pc = Hamplitude32minus(MPc, M01, gamma1, Pc1Bw1, Pc1Bw2, Pc1Bs1, Pc1Bs2, Pc1Bs3,  hL, hp, hmu, thetaPc, thetaLPc, thetaPsiPc, phimuPc, phiPc, phiPsi, alpha_mu, thetap );
	 A2Pc = Hamplitude52plus(MPc, M02, gamma2, Pc2Bw1, Pc2Bw2, Pc2Bs1, Pc2Bs2, Pc2Bs3,  hL, hp, hmu, thetaPc, thetaLPc, thetaPsiPc, phimuPc, phiPc, phiPsi, alpha_mu, thetap );
	}
	if( Pc_32plus_52minus == 1 ){
	 A1Pc = Hamplitude32plus(MPc, M01, gamma1, Pc1Bw1, Pc1Bw2, Pc1Bs1, Pc1Bs2, Pc1Bs3,  hL, hp, hmu, thetaPc, thetaLPc, thetaPsiPc, phimuPc, phiPc, phiPsi, alpha_mu, thetap );
	 A2Pc = Hamplitude52minus(MPc, M02, gamma2, Pc2Bw1, Pc2Bw2, Pc2Bs1, Pc2Bs2, Pc2Bs3,  hL, hp, hmu, thetaPc, thetaLPc, thetaPsiPc, phimuPc, phiPc, phiPsi, alpha_mu, thetap );
	}
	if( Pc4_12minus_32minus_12minus_32minus == 1 ){
	 A1Pc = Hamplitude32minus(MPc, M01, gamma1, Pc1Bw1, Pc1Bw2, Pc1Bs1, Pc1Bs2, Pc1Bs3,  hL, hp, hmu, thetaPc, thetaLPc, thetaPsiPc, phimuPc, phiPc, phiPsi, alpha_mu, thetap );
	 A2Pc = Hamplitude12minus(MPc, M02, gamma2, Pc2Bw1, Pc2Bw2, Pc2Bs1, Pc2Bs2, Pc2Bs3,  hL, hp, hmu, thetaPc, thetaLPc, thetaPsiPc, phimuPc, phiPc, phiPsi, alpha_mu, thetap );
	 A3Pc = Hamplitude12minus(MPc, M03, gamma3, Pc3Bw1, Pc3Bw2, Pc3Bs1, Pc3Bs2, Pc3Bs3,  hL, hp, hmu, thetaPc, thetaLPc, thetaPsiPc, phimuPc, phiPc, phiPsi, alpha_mu, thetap );
	 A4Pc = Hamplitude32minus(MPc, M04, gamma4, Pc4Bw1, Pc4Bw2, Pc4Bs1, Pc4Bs2, Pc4Bs3,  hL, hp, hmu, thetaPc, thetaLPc, thetaPsiPc, phimuPc, phiPc, phiPsi, alpha_mu, thetap );
	
	}

	//printf("APc32minus_real = %10.5e \t APc32minus_imag = %10.5e\n", A1Pc.real(), A1Pc.imag() );
	//printf("Pc1Bw1_real = %10.5e \t Pc1Bw1_imag = %10.5e\n", Pc1Bw1.real(), Pc1Bw1.imag() );

	//getchar();

	std::complex<float> ALstar1800_12 = HamplitudeLstar12( -1, MLstar, 1.80, 0.300, Ls1800_12_Bw1, Ls1800_12_Bw2, Ls1800_12_Bw3, Ls1800_12_Bw4, hL, hp, hmu, thetaLstar, thetaLLstar, thetaPsiLstar, phimuLstar, phiK );
	std::complex<float> ALstar1810_12 = HamplitudeLstar12( 1, MLstar, 1.81, 0.150, Ls1810_12_Bw1, Ls1810_12_Bw2, Ls1810_12_Bw3, 0.0, hL, hp, hmu, thetaLstar, thetaLLstar, thetaPsiLstar, phimuLstar, phiK );
	std::complex<float> ALstar1820_52 = HamplitudeLstar52( 1, MLstar, 1.82, 0.080, Ls1820_52_Bw1, Ls1820_52_Bw2, Ls1820_52_Bw3, hL, hp, hmu, thetaLstar, thetaLLstar, thetaPsiLstar, phimuLstar, phiK );
	std::complex<float> ALstar1830_52 = HamplitudeLstar52( -1, MLstar, 1.83, 0.095, Ls1830_52_Bw1, Ls1830_52_Bw2, Ls1830_52_Bw3, hL, hp, hmu, thetaLstar, thetaLLstar, thetaPsiLstar, phimuLstar, phiK );
	std::complex<float> ALstar1890_32 = HamplitudeLstar32( 1, MLstar, 1.89, 0.100, Ls1890_32_Bw1, Ls1890_32_Bw2, Ls1890_32_Bw3, hL, hp, hmu, thetaLstar, thetaLLstar, thetaPsiLstar, phimuLstar, phiK );
	/// need amplitude72
	std::complex<float> ALstar2020_72 = HamplitudeLstar72( 1, MLstar, 2.020, 0.100, Ls2020_72_Bw1, Ls2020_72_Bw2, Ls2020_72_Bw3, hL, hp, hmu, thetaLstar, thetaLLstar, thetaPsiLstar, phimuLstar, phiK );
	std::complex<float> ALstar2050_32 = HamplitudeLstar32( -1, MLstar, 2.056, 0.493, Ls2050_32_Bw1, Ls2050_32_Bw2, Ls2050_32_Bw3, hL, hp, hmu, thetaLstar, thetaLLstar, thetaPsiLstar, phimuLstar, phiK );
	std::complex<float> ALstar2100_72 = HamplitudeLstar72( -1, MLstar, 2.100, 0.200, Ls2100_72_Bw1, Ls2100_72_Bw2, Ls2100_72_Bw3, hL, hp, hmu, thetaLstar, thetaLLstar, thetaPsiLstar, phimuLstar, phiK );
	std::complex<float> ALstar2110_52 = HamplitudeLstar52( 1, MLstar, 2.110, 0.200, Ls2110_52_Bw1, Ls2110_52_Bw2, Ls2110_52_Bw3, hL, hp, hmu, thetaLstar, thetaLLstar, thetaPsiLstar, phimuLstar, phiK );
	std::complex<float> ALstar2325_32 = HamplitudeLstar32( -1, MLstar, 2.325, 0.100, Ls2325_32_Bw1, Ls2325_32_Bw2, Ls2325_32_Bw3, hL, hp, hmu, thetaLstar, thetaLLstar, thetaPsiLstar, phimuLstar, phiK );
	std::complex<float> ALstar2350_92 = HamplitudeLstar92( 1, MLstar, 2.350, 0.150, Ls2350_92_Bw1, 0.0, 0.0, hL, hp, hmu, thetaLstar, thetaLLstar, thetaPsiLstar, phimuLstar, phiK );

	std::complex<float> ALstarNR_12 =  0.0;
	if(includeLsNRsimple == 1) ALstarNR_12 = LsNR_12_Bw;
	if(includeLsNR == 1) ALstarNR_12 = HamplitudeLstar12NR( 1, MLstar, 1.80, 0.300, LsNR_12_Bw, hL, hp, hmu, thetaLstar, thetaLLstar, thetaPsiLstar, phimuLstar, phiK );	

	float ad = 0.0;

	std::complex<float> Ampl_int = ALstar1800_12 + ALstar1810_12 + ALstar1820_52 + ALstar1830_52 + ALstar1890_32
			 + ALstar2020_72 + ALstar2050_32 + ALstar2100_72 + ALstar2110_52 + ALstar2325_32 + ALstar2350_92 + ALstarNR_12;

	if(Pc1_interference_off == 0) Ampl_int += A1Pc;
		else ad += pow( abs(A1Pc), 2.0 );
	if(Pc2_interference_off == 0) Ampl_int += A2Pc;
		else ad += pow( abs(A2Pc), 2.0 );
	if(Pc3_interference_off == 0) Ampl_int += A3Pc;
		else ad += pow( abs(A3Pc), 2.0 );
	if(Pc4_interference_off == 0) Ampl_int += A4Pc;
		else ad += pow( abs(A4Pc), 2.0 );
	
	ad += pow( abs(Ampl_int), 2.0 );

	MeL += ad;
	 if(ad != ad){
		printf("A1800=%10.2e\n", abs(ALstar1800_12));
		printf("A1810=%10.2e\n", abs(ALstar1810_12));
		printf("A1820=%10.2e\n", abs(ALstar1820_52));
		printf("A1830=%10.2e\n", abs(ALstar1830_52));
		printf("A1890=%10.2e\n", abs(ALstar1890_32));
		printf("A2020=%10.2e\n", abs(ALstar2020_72));
		printf("A2050=%10.2e\n", abs(ALstar2050_32));
		printf("A2100=%10.2e\n", abs(ALstar2100_72));
		printf("A2110=%10.2e\n", abs(ALstar2110_52));
		printf("A2325=%10.2e\n", abs(ALstar2325_32));
		printf("A2350=%10.2e\n", abs(ALstar2350_92));
		printf("ANR=%10.2e\n", abs(ALstarNR_12));
		printf("APc1=%10.2e\n", abs(A1Pc));
		printf("APc2=%10.2e\n", abs(A2Pc));
		printf("APc3=%10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e\n", abs(A3Pc), M03, gamma3, abs(Pc3Bw1), abs(Pc3Bw2), abs(Pc3Bs1), abs(Pc3Bs2), abs(Pc3Bs3) );
		printf("APc4=%10.2e\n", abs(A4Pc));

		
		getchar();
	}


	}

	//std::complex<float> BLS1_test (0.5, 0.1 );
	//std::complex<float> BLS2_test (0.4, 0.9 );
	//std::complex<float> BLS3_test (0.0, 1.2 );

	//std::complex<float> APc32minus = Hamplitude32minus( 4.42, 4.40, 0.1, 1.0, BLS1_test, 1.0, 1.0, 0.0,  0.5, 0.5, -1.0, 0.2, -0.5, 1.5, -1.2, 0.0, 2.3, 0.4, 0.6 );
	//std::complex<float> APc32plus = Hamplitude32plus( 4.45, 4.39, 0.1, BLS2_test, 0.0, BLS3_test, 1.0, 0.0,  0.5, -0.5, 1.0, 0.2, -0.5, 1.5, -1.2, 0.0, 2.3, -0.4, 0.1 );
	//std::complex<float> APc52plus = Hamplitude52plus(4.30, 4.45, 0.03, BLS1_test, 0.0, BLS3_test, 1.0, 0.0,  0.5, -0.5, 1.0, 0.2, -0.5, 1.5, -1.2, 0.0, 2.3, -0.4, 0.1 );
	//std::complex<float> APc52minus = Hamplitude52minus(4.30, 4.45, 0.03, BLS2_test, 0.0, BLS1_test, 1.0, 0.0,  -0.5, 0.5, 1.0, 0.2, -0.5, 1.5, -1.2, 0.0, 2.3, -0.4, -0.1 );

	//printf("APc32minus_real = %10.5e \t APc32minus_imag = %10.5e\n", APc32minus.real(), APc32minus.imag() );
	//printf("APc32plus_real = %10.5e \t APc32plus_imag = %10.5e\n", APc32plus.real(), APc32plus.imag() );
	//printf("APc52plus_real = %10.5e \t APc52plus_imag = %10.5e\n", APc52plus.real(), APc52plus.imag() );
	//printf("APc52minus_real = %10.5e \t APc52minus_imag = %10.5e\n", APc52minus.real(), APc52minus.imag() );

	///getchar();
	
	if(MeL != MeL) { printf("in Pc total Amplitude MeL is Nan"); getchar();}
	return MeL/3.0;
}

///// angle between Pc and Lambda_b in Lambda_b rest frame, Lambda_b momentum is taken in lab system;
Double_t theta_lambda_b(TLorentzVector vLambda, TLorentzVector vPc)
{

	TVector3 vLambda3 = vLambda.BoostVector();
	TLorentzVector vPc_b = vPc;
	vPc_b.Boost(-vLambda3);

return vPc_b.Vect().Angle( vLambda.Vect() );
}


Double_t theta_lambda_b_ls(TLorentzVector vLambda, TLorentzVector vp, TLorentzVector vK )
{

	TVector3 vLambda3 = vLambda.BoostVector();
	TLorentzVector vLstar_b = vp + vK;
	vLstar_b.Boost(-vLambda3);

return vLstar_b.Vect().Angle( vLambda.Vect() );
}

//////////////////////////////////////////////////////////////////////////////////////////////
///// angle between Jpsi and Pc in Pc rest frame, Pc momentum is taken in Lambda_b rest frame;
Double_t theta_Pc(TLorentzVector vLambda, TLorentzVector vPc, TLorentzVector vpsi)
{
	TVector3 vLambda3 = vLambda.BoostVector();
	TVector3 vPc3 = vPc.BoostVector();
	TLorentzVector vpsi_b = vpsi;
	vpsi_b.Boost(-vPc3);
	TLorentzVector vPc_b2 = vPc;
	vPc_b2.Boost(-vLambda3);

return vpsi_b.Vect().Angle( vPc_b2.Vect() );
}

//////////////////////////////////////////////////////////////////////////////////////////////
///// angle between Kaon and Lstar candidate in Lstar rest frame, Lstar momentum is taken in Lambda_b rest frame;
Double_t theta_Lstar( TLorentzVector vLambda, TLorentzVector vLstar, TLorentzVector vK )
{
	
	TVector3 vLambda3 = vLambda.BoostVector();
	TVector3 vLstar3 = vLstar.BoostVector();

	TLorentzVector vK_b = vK;
	vK_b.Boost(-vLstar3);
	
	TLorentzVector vLstarb = vLstar;
	vLstarb.Boost(-vLambda3);
	return vK_b.Vect().Angle(vLstarb.Vect()) ;

}


///// angle between Jpsi and muon in Jpsi rest frame, Jpsi momentum is taken in Pc rest frame;
Double_t theta_psi(TLorentzVector vPc, TLorentzVector vpsi, TLorentzVector v_mu2)
{

	TVector3 vPc3 = vPc.BoostVector();
	TVector3 vpsi3 = vpsi.BoostVector();
	TLorentzVector v_mu2_b = v_mu2; 
	v_mu2_b.Boost(-vpsi3);
	TLorentzVector vpsi_b = vpsi;
	vpsi_b.Boost(-vPc3);

return v_mu2_b.Vect().Angle(vpsi_b.Vect());
}


///// angle between Jpsi and muon in Jpsi rest frame, Jpsi momentum is taken in Pc rest frame;
Double_t phi_mu(TLorentzVector vPc, TLorentzVector vK, TLorentzVector vpsi, TLorentzVector vp, TLorentzVector v_mu2)
{
	TLorentzVector vK_b = vK;
	TLorentzVector vpsi_b = vpsi;
	TVector3 vPc3 = vPc.BoostVector();
	vK_b.Boost(-vPc3);
	vpsi_b.Boost(-vPc3);

	TVector3 x0 = vK_b.Vect().Unit() - ( vK_b.Vect().Unit() * vpsi_b.Vect().Unit() ) * vpsi_b.Vect().Unit();
	

	TLorentzVector vp_b = vp;
	TLorentzVector v_mu2_b = v_mu2;
	TVector3 vpsi3 = vpsi.BoostVector();
	vp_b.Boost(-vpsi3);
	v_mu2_b.Boost(-vpsi3);
	
	double argy = ( -vp_b.Vect().Unit().Cross(x0.Unit()) ) * v_mu2_b.Vect().Unit();
	double argx = v_mu2_b.Vect().Unit() * x0.Unit();

	return atan2( argy, argx );

}


Double_t phi_mu_Pc(TLorentzVector vPc, TLorentzVector vK, TLorentzVector vpsi, TLorentzVector v_mu2)
{
	TLorentzVector vK_b = vK;
	TLorentzVector vpsi_b = vpsi;
	TVector3 vPc3 = vPc.BoostVector();
	vK_b.Boost(-vPc3);
	vpsi_b.Boost(-vPc3);

	TVector3 x0 = vK_b.Vect().Unit() - ( vK_b.Vect().Unit() * vpsi_b.Vect().Unit() ) * vpsi_b.Vect().Unit();
	
	
	TLorentzVector v_mu2_b = v_mu2;
	TVector3 vpsi3 = vpsi.BoostVector();
	v_mu2_b.Boost(-vpsi3);
	
	double argy = ( vpsi_b.Vect().Unit().Cross(x0.Unit()) ) * v_mu2_b.Vect().Unit();
	double argx = v_mu2_b.Vect().Unit() * x0.Unit();

	return atan2( argy, argx );

}



Double_t phi_mu_Lstar(TLorentzVector vLambda, TLorentzVector vLstar, TLorentzVector vpsi, TLorentzVector v_mu2)
{
	TLorentzVector vLstar_b = vLstar;
	TLorentzVector vpsi_b = vpsi;
	TVector3 vLambda3 = vLambda.BoostVector();
	
	vpsi_b.Boost(-vLambda3);
	vLstar_b.Boost(-vLambda3);

	TVector3 x0 = -vLambda.Vect().Unit() + ( vLambda.Vect().Unit() * vLstar_b.Vect().Unit() ) * vLstar_b.Vect().Unit();
	
	TLorentzVector v_mu2_b = v_mu2;
	TVector3 vpsi3 = vpsi.BoostVector();
	
	v_mu2_b.Boost(-vpsi3);
	
	double argy = ( vpsi_b.Vect().Unit().Cross(x0.Unit()) ) * v_mu2_b.Vect().Unit();
	double argx = v_mu2_b.Vect().Unit() * x0.Unit();

	return atan2( argy, argx );

}

Double_t phi_K_Lstar(TLorentzVector vLambda, TLorentzVector vLstar, TLorentzVector vK, TLorentzVector vpsi)
{
	
	TLorentzVector vK_b = vK;
	TLorentzVector vLstarb = vLstar;
	TLorentzVector vpsib = vpsi;
	TLorentzVector vKb = vK;
	TVector3 vLambda3 = vLambda.BoostVector();
	TVector3 vLstar3 = vLstar.BoostVector();
	

	vLstarb.Boost(-vLambda3);
	vpsib.Boost(-vLstar3);
	vKb.Boost(-vLstar3);

	TVector3 x0 = - vLambda.Vect().Unit() + ( vLambda.Vect().Unit() * vLstarb.Vect().Unit() ) * vLstarb.Vect().Unit();
	
	
	double argy = ( vLstarb.Vect().Unit().Cross(x0.Unit()) ) * vKb.Vect().Unit();
	double argx = x0.Unit() * vKb.Vect().Unit();

	return atan2( argy, argx );

}




Double_t phi_psi(TLorentzVector vLambda, TLorentzVector vPc, TLorentzVector vpsi, TLorentzVector vK )
{
	TVector3 vLambda3 = vLambda.BoostVector();
	TLorentzVector vPc_b = vPc;
	vPc_b.Boost(-vLambda3);
	
	TVector3 x0 = -vLambda.Vect().Unit() + ( vLambda.Vect().Unit() * vPc_b.Vect().Unit() ) * vPc_b.Vect().Unit();
//	printf("%10.5e\t", x0.Mag() ); getchar();

	TLorentzVector vK_b = vK;
	TLorentzVector vpsi_b = vpsi;
	TVector3 vPc3 = vPc.BoostVector();
	vK_b.Boost(-vPc3);
	vpsi_b.Boost(-vPc3);

	double argy = ( -vK_b.Vect().Unit().Cross( x0.Unit() ) ) * vpsi_b.Vect().Unit();   //x0.Unit();
	//double argy = ( vK_b.Vect().Unit().Cross(vpsi_b.Vect().Unit()) ) * x0.Unit();
	double argx = vpsi_b.Vect().Unit() * x0.Unit();

	return atan2(  argy,  argx );
}



Double_t phi_Pc(TLorentzVector vLambda, TLorentzVector vPc, TLorentzVector vLstar)
{
	
	TLorentzVector vPc_b = vPc;
	TLorentzVector vLstar_b = vLstar;
	TVector3 vLambda3 = vLambda.BoostVector();
	

	vPc_b.Boost(-vLambda3);
	vLstar_b.Boost(-vLambda3);

	TVector3 x0 = vLstar_b.Vect().Unit() - ( vLstar_b.Vect().Unit() * vLambda.Vect().Unit() ) * vLambda.Vect().Unit();
	
	
	double argy = ( vLambda.Vect().Unit().Cross(x0.Unit()) ) * vPc_b.Vect().Unit();
	double argx = x0.Unit() * vPc_b.Vect().Unit();

	return atan2( argy, argx );

}

Double_t theta_p(TLorentzVector vpsi, TLorentzVector vp, TLorentzVector vK)
{


	TLorentzVector vpsi_b = vpsi;
	TLorentzVector vK_b = vK;
	//TLorentzVector vLambda_b = vLambda;

	TVector3 vp3 = vp.BoostVector();
	

	vpsi_b.Boost(-vp3);
	vK_b.Boost(-vp3);


	return vK_b.Vect().Angle(vpsi_b.Vect()) ;
}

//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__|| arrays and hisograms  ||__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//

float Jpsipi1_mass[phs_Bd], Jpsipi2_mass[phs_Bd], JpsiK1_mass[phs_Bd], JpsiK2_mass[phs_Bd], K1pi2_mass[phs_Bd], K2pi1_mass[phs_Bd], p1K2_mass[phs_Bd], p2K1_mass[phs_Bd], Jpsip1_mass[phs_Bd], Jpsip2_mass[phs_Bd], Kpi_truth[phs_Bd], Jpsipi_truth[phs_Bd], p_pi[phs_Bd], p_K[phs_Bd], phi_K_a[phs_Bd], alpha_mu_a[phs_Bd], phi_mu_Kstar_a[phs_Bd], theta_psi_X_a[phs_Bd], phi_mu_X_a[phs_Bd], theta_X_a[phs_Bd], theta_Kstar_a[phs_Bd], theta_psi_Kstar_a[phs_Bd], weight[phs_Bd], w_dimuon_a[phs_Bd], KK_mass[phs_Bd], JpsiKpi_mass[phs_Bd], JpsipiK_mass[phs_Bd], JpsiKK_mass[phs_Bd], Jpsipipi_mass[phs_Bd], JpsipK_mass[phs_Bd], JpsiKp_mass[phs_Bd], cos_theta_Zc_Kpi_Bd[phs_Bd], cos_theta_Zc_piK_Bd[phs_Bd],
phi_K_Kpi_Bd[phs_Bd], phi_K_piK_Bd[phs_Bd],
alpha_mu_Kpi_Bd[phs_Bd], alpha_mu_piK_Bd[phs_Bd],
phi_mu_Kstar_Kpi_Bd[phs_Bd], phi_mu_Kstar_piK_Bd[phs_Bd],
phi_mu_X_Kpi_Bd[phs_Bd], phi_mu_X_piK_Bd[phs_Bd],
cos_theta_psi_X_Kpi_Bd[phs_Bd], cos_theta_psi_X_piK_Bd[phs_Bd],
cos_theta_Kstar_Kpi_Bd[phs_Bd], cos_theta_Kstar_piK_Bd[phs_Bd],
cos_theta_psi_Kstar_Kpi_Bd[phs_Bd], cos_theta_psi_Kstar_piK_Bd[phs_Bd],
cos_theta_B0_Kpi_Bd[phs_Bd], cos_theta_B0_piK_Bd[phs_Bd],
phi_psi_Kpi_Bd[phs_Bd], phi_psi_piK_Bd[phs_Bd], Jpsippi_mass[phs_Bd], Jpsipip_mass[phs_Bd];

int signal_a[phs_Bd], control_a[phs_Bd], controlLb_a[phs_Bd];

std::complex<float> BWK1410_1_1_minus[phs_Bd], BWK1410_1_2_minus[phs_Bd], BWK1410_1_3_minus[phs_Bd], BWK1430_0_1_minus[phs_Bd], BWK1430_2_1_minus[phs_Bd], BWK1430_2_2_minus[phs_Bd], BWK1430_2_3_minus[phs_Bd], BWK1680_1_1_minus[phs_Bd], BWK1680_1_2_minus[phs_Bd], BWK1680_1_3_minus[phs_Bd], BWK1780_3_1_minus[phs_Bd], BWK1780_3_2_minus[phs_Bd], BWK1780_3_3_minus[phs_Bd], BWK1950_0_1_minus[phs_Bd], BWK1980_2_1_minus[phs_Bd], BWK1980_2_2_minus[phs_Bd], BWK1980_2_3_minus[phs_Bd], BWK2045_4_1_minus[phs_Bd], BWK2045_4_2_minus[phs_Bd], BWK2045_4_3_minus[phs_Bd];

std::complex<float> BWK1410_1_1_plus[phs_Bd], BWK1410_1_2_plus[phs_Bd], BWK1410_1_3_plus[phs_Bd], BWK1430_0_1_plus[phs_Bd], BWK1430_2_1_plus[phs_Bd], BWK1430_2_2_plus[phs_Bd], BWK1430_2_3_plus[phs_Bd], BWK1680_1_1_plus[phs_Bd], BWK1680_1_2_plus[phs_Bd], BWK1680_1_3_plus[phs_Bd], BWK1780_3_1_plus[phs_Bd], BWK1780_3_2_plus[phs_Bd], BWK1780_3_3_plus[phs_Bd], BWK1950_0_1_plus[phs_Bd], BWK1980_2_1_plus[phs_Bd], BWK1980_2_2_plus[phs_Bd], BWK1980_2_3_plus[phs_Bd], BWK2045_4_1_plus[phs_Bd], BWK2045_4_2_plus[phs_Bd], BWK2045_4_3_plus[phs_Bd];

float JpsiK1_mass_BsKK[phs_Bs], JpsiK2_mass_BsKK[phs_Bs], KK_mass_BsKK[phs_Bs], KK_truth_BsKK[phs_Bs], thetaF_a[phs_Bs], phi_mu_F_a[phs_Bs], phi_KF_a[phs_Bs], theta_psi_F_a[phs_Bs], weight_BsKK[phs_Bs], Jpsipi1_mass_BsKK[phs_Bs], Jpsipi2_mass_BsKK[phs_Bs], K1pi2_mass_BsKK[phs_Bs], K2pi1_mass_BsKK[phs_Bs], p1K2_mass_BsKK[phs_Bs], p2K1_mass_BsKK[phs_Bs], Jpsip1_mass_BsKK[phs_Bs], Jpsip2_mass_BsKK[phs_Bs], w_dimuon_a_BsKK[phs_Bs], JpsiKpi_mass_BsKK[phs_Bs], JpsipiK_mass_BsKK[phs_Bs], JpsiKK_mass_BsKK[phs_Bs], Jpsipipi_mass_BsKK[phs_Bs], JpsipK_mass_BsKK[phs_Bs], JpsiKp_mass_BsKK[phs_Bs], cos_theta_Zc_Kpi_Bs[phs_Bs], cos_theta_Zc_piK_Bs[phs_Bs],
phi_K_Kpi_Bs[phs_Bs], phi_K_piK_Bs[phs_Bs],
alpha_mu_Kpi_Bs[phs_Bs], alpha_mu_piK_Bs[phs_Bs],
phi_mu_Kstar_Kpi_Bs[phs_Bs], phi_mu_Kstar_piK_Bs[phs_Bs],
phi_mu_X_Kpi_Bs[phs_Bs], phi_mu_X_piK_Bs[phs_Bs],
cos_theta_psi_X_Kpi_Bs[phs_Bs], cos_theta_psi_X_piK_Bs[phs_Bs],
cos_theta_Kstar_Kpi_Bs[phs_Bs], cos_theta_Kstar_piK_Bs[phs_Bs],
cos_theta_psi_Kstar_Kpi_Bs[phs_Bs], cos_theta_psi_Kstar_piK_Bs[phs_Bs],
cos_theta_B0_Kpi_Bs[phs_Bs], cos_theta_B0_piK_Bs[phs_Bs],
phi_psi_Kpi_Bs[phs_Bs], phi_psi_piK_Bs[phs_Bs], Jpsippi_mass_BsKK[phs_Bs], Jpsipip_mass_BsKK[phs_Bs];

int signal_a_BsKK[phs_Bs], control_a_BsKK[phs_Bs], controlLb_a_BsKK[phs_Bs];

float JpsiK1_mass_Lb[phs_Lb], JpsiK2_mass_Lb[phs_Lb], KK_mass_Lb[phs_Lb], pK_truth_Lb[phs_Lb], Jpsip_truth_Lb[phs_Lb], weight_Lb[phs_Lb], Jpsipi1_mass_Lb[phs_Lb], Jpsipi2_mass_Lb[phs_Lb], K1pi2_mass_Lb[phs_Lb], K2pi1_mass_Lb[phs_Lb], p1K2_mass_Lb[phs_Lb], p2K1_mass_Lb[phs_Lb], Jpsip1_mass_Lb[phs_Lb], Jpsip2_mass_Lb[phs_Lb], w_dimuon_a_Lb[phs_Lb], JpsiKpi_mass_Lb[phs_Lb], JpsipiK_mass_Lb[phs_Lb], JpsiKK_mass_Lb[phs_Lb], Jpsipipi_mass_Lb[phs_Lb], JpsipK_mass_Lb[phs_Lb], JpsiKp_mass_Lb[phs_Lb], theta_lambda_b_a[phs_Lb], theta_lambda_b_ls_a[phs_Lb], theta_Pc_a[phs_Lb], theta_Lstar_a[phs_Lb], theta_psi_a[phs_Lb], phi_mu_a[phs_Lb], phi_mu_Pc_a[phs_Lb], phi_mu_Lstar_a[phs_Lb], phi_K_Lstar_a[phs_Lb], phi_psi_a[phs_Lb], phi_Pc_a[phs_Lb], theta_p_a[phs_Lb], alpha_mu_Lb_a[phs_Lb], thete_psi_Lstar_a[phs_Lb], cos_theta_Zc_Kpi_Lb[phs_Lb], cos_theta_Zc_piK_Lb[phs_Lb],
phi_K_Kpi_Lb[phs_Lb], phi_K_piK_Lb[phs_Lb],
alpha_mu_Kpi_Lb[phs_Lb], alpha_mu_piK_Lb[phs_Lb],
phi_mu_Kstar_Kpi_Lb[phs_Lb], phi_mu_Kstar_piK_Lb[phs_Lb],
phi_mu_X_Kpi_Lb[phs_Lb], phi_mu_X_piK_Lb[phs_Lb],
cos_theta_psi_X_Kpi_Lb[phs_Lb], cos_theta_psi_X_piK_Lb[phs_Lb],
cos_theta_Kstar_Kpi_Lb[phs_Lb], cos_theta_Kstar_piK_Lb[phs_Lb],
cos_theta_psi_Kstar_Kpi_Lb[phs_Lb], cos_theta_psi_Kstar_piK_Lb[phs_Lb],
cos_theta_B0_Kpi_Lb[phs_Lb], cos_theta_B0_piK_Lb[phs_Lb],
phi_psi_Kpi_Lb[phs_Lb], phi_psi_piK_Lb[phs_Lb], Jpsippi_mass_Lb[phs_Lb], Jpsipip_mass_Lb[phs_Lb];

int signal_a_Lb[phs_Lb], control_a_Lb[phs_Lb], controlLb_a_Lb[phs_Lb];

/*theta_lambda_b
theta_lambda_b_ls
theta_Pc
theta_Lstar
theta_psi
phi_mu
phi_mu_Pc
phi_mu_Lstar
phi_K_Lstar
phi_psi
phi_Pc
theta_p*/

int n_fit_step = 0;

TH2D *Jpsipi_2d_data = 0;
TH2D *Jpsipi_2d_LAMBDA0B_to_JPSI_P_K = 0;
TH2D *Jpsipi_2d_BS_to_JPSI_K_K = 0;
TH2D *Jpsipi_2d_BS_to_JPSI_PI_PI = 0;
TH2D *Jpsipi_2d_BD_to_JPSI_K_PI = 0;// new TH2D("Jpsipi_2d_BD_to_JPSI_K_PI", "Jpsipi_2d_BD_to_JPSI_K_PI", 50, 3.3, 5., 50, 3.3, 5.);
TH2D *Jpsipi_2d_BD_to_JPSI_PI_PI = 0;
TH2D *Jpsipi_2d_BS_to_JPSI_K_PI = 0;
TH2D *Jpsipi_2d_BD_to_JPSI_K_K = 0;
TH2D *Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI = 0;

TH2D *JpsiK_2d_data = 0;
TH2D *JpsiK_2d_LAMBDA0B_to_JPSI_P_K = 0;
TH2D *JpsiK_2d_BS_to_JPSI_K_K = 0;
TH2D *JpsiK_2d_BS_to_JPSI_PI_PI = 0;
TH2D *JpsiK_2d_BD_to_JPSI_K_PI = 0;
TH2D *JpsiK_2d_BD_to_JPSI_PI_PI = 0;
TH2D *JpsiK_2d_BS_to_JPSI_K_PI = 0;
TH2D *JpsiK_2d_BD_to_JPSI_K_K = 0;
TH2D *JpsiK_2d_LAMBDA0B_to_JPSI_P_PI = 0;

TH2D *Kpi_2d_data = 0;
TH2D *Kpi_2d_LAMBDA0B_to_JPSI_P_K = 0;
TH2D *Kpi_2d_BS_to_JPSI_K_K = 0;
TH2D *Kpi_2d_BS_to_JPSI_PI_PI = 0;
TH2D *Kpi_2d_BD_to_JPSI_K_PI = 0;
TH2D *Kpi_2d_BD_to_JPSI_PI_PI = 0;
TH2D *Kpi_2d_BS_to_JPSI_K_PI = 0;
TH2D *Kpi_2d_BD_to_JPSI_K_K = 0;
TH2D *Kpi_2d_LAMBDA0B_to_JPSI_P_PI = 0;

TH2D *cos_theta_Zc_data = 0;
TH2D *cos_theta_Zc_LAMBDA0B_to_JPSI_P_K = 0;
TH2D *cos_theta_Zc_BS_to_JPSI_PI_PI = 0;
TH2D *cos_theta_Zc_BD_to_JPSI_PI_PI = 0;
TH2D *cos_theta_Zc_BD_to_JPSI_K_PI = 0;
TH2D *cos_theta_Zc_BS_to_JPSI_K_K = 0;    
TH2D *cos_theta_Zc_bg = 0;   
TH2D *cos_theta_Zc_BS_to_JPSI_K_PI = 0;
TH2D *cos_theta_Zc_BD_to_JPSI_K_K = 0;
TH2D *cos_theta_Zc_LAMBDA0B_to_JPSI_P_PI = 0;

TH2D *phi_K_data = 0;
TH2D *phi_K_LAMBDA0B_to_JPSI_P_K = 0;
TH2D *phi_K_BS_to_JPSI_PI_PI = 0;
TH2D *phi_K_BD_to_JPSI_PI_PI = 0;
TH2D *phi_K_BD_to_JPSI_K_PI = 0;
TH2D *phi_K_BS_to_JPSI_K_K = 0;    
TH2D *phi_K_bg = 0;   
TH2D *phi_K_BS_to_JPSI_K_PI = 0;
TH2D *phi_K_BD_to_JPSI_K_K = 0;
TH2D *phi_K_LAMBDA0B_to_JPSI_P_PI = 0;

TH2D *alpha_mu_data = 0;
TH2D *alpha_mu_LAMBDA0B_to_JPSI_P_K = 0;
TH2D *alpha_mu_BS_to_JPSI_PI_PI = 0;
TH2D *alpha_mu_BD_to_JPSI_PI_PI = 0;
TH2D *alpha_mu_BD_to_JPSI_K_PI = 0;
TH2D *alpha_mu_BS_to_JPSI_K_K = 0;    
TH2D *alpha_mu_bg = 0;  
TH2D *alpha_mu_BS_to_JPSI_K_PI = 0;
TH2D *alpha_mu_BD_to_JPSI_K_K = 0;
TH2D *alpha_mu_LAMBDA0B_to_JPSI_P_PI = 0;

TH2D *phi_mu_Kstar_data = 0;
TH2D *phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K = 0;
TH2D *phi_mu_Kstar_BS_to_JPSI_PI_PI = 0;
TH2D *phi_mu_Kstar_BD_to_JPSI_PI_PI = 0;
TH2D *phi_mu_Kstar_BD_to_JPSI_K_PI = 0;
TH2D *phi_mu_Kstar_BS_to_JPSI_K_K = 0;    
TH2D *phi_mu_Kstar_bg = 0;   
TH2D *phi_mu_Kstar_BS_to_JPSI_K_PI = 0;
TH2D *phi_mu_Kstar_BD_to_JPSI_K_K = 0;
TH2D *phi_mu_Kstar_LAMBDA0B_to_JPSI_P_PI = 0;

TH2D *phi_mu_X_data = 0;
TH2D *phi_mu_X_LAMBDA0B_to_JPSI_P_K = 0;
TH2D *phi_mu_X_BS_to_JPSI_PI_PI = 0;
TH2D *phi_mu_X_BD_to_JPSI_PI_PI = 0;
TH2D *phi_mu_X_BD_to_JPSI_K_PI = 0;
TH2D *phi_mu_X_BS_to_JPSI_K_K = 0;    
TH2D *phi_mu_X_bg = 0;   
TH2D *phi_mu_X_BS_to_JPSI_K_PI = 0;
TH2D *phi_mu_X_BD_to_JPSI_K_K = 0;
TH2D *phi_mu_X_LAMBDA0B_to_JPSI_P_PI = 0;

TH2D *cos_theta_psi_X_data = 0;
TH2D *cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K = 0;
TH2D *cos_theta_psi_X_BS_to_JPSI_PI_PI = 0;
TH2D *cos_theta_psi_X_BD_to_JPSI_PI_PI = 0;
TH2D *cos_theta_psi_X_BD_to_JPSI_K_PI = 0;
TH2D *cos_theta_psi_X_BS_to_JPSI_K_K = 0;    
TH2D *cos_theta_psi_X_bg = 0;    
TH2D *cos_theta_psi_X_BS_to_JPSI_K_PI = 0;
TH2D *cos_theta_psi_X_BD_to_JPSI_K_K = 0;
TH2D *cos_theta_psi_X_LAMBDA0B_to_JPSI_P_PI = 0;

TH2D *cos_theta_Kstar_data = 0;
TH2D *cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K = 0;
TH2D *cos_theta_Kstar_BS_to_JPSI_PI_PI = 0;
TH2D *cos_theta_Kstar_BD_to_JPSI_PI_PI = 0;
TH2D *cos_theta_Kstar_BD_to_JPSI_K_PI = 0;
TH2D *cos_theta_Kstar_BS_to_JPSI_K_K = 0;    
TH2D *cos_theta_Kstar_bg = 0;    
TH2D *cos_theta_Kstar_BS_to_JPSI_K_PI = 0;
TH2D *cos_theta_Kstar_BD_to_JPSI_K_K = 0;
TH2D *cos_theta_Kstar_LAMBDA0B_to_JPSI_P_PI = 0;

TH2D *cos_theta_psi_Kstar_data = 0;
TH2D *cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K = 0;
TH2D *cos_theta_psi_Kstar_BS_to_JPSI_PI_PI = 0;
TH2D *cos_theta_psi_Kstar_BD_to_JPSI_PI_PI = 0;
TH2D *cos_theta_psi_Kstar_BD_to_JPSI_K_PI = 0;
TH2D *cos_theta_psi_Kstar_BS_to_JPSI_K_K = 0;    
TH2D *cos_theta_psi_Kstar_bg = 0;   
TH2D *cos_theta_psi_Kstar_BS_to_JPSI_K_PI = 0;
TH2D *cos_theta_psi_Kstar_BD_to_JPSI_K_K = 0;
TH2D *cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_PI = 0;

TH2D *cos_theta_B0_data = 0;
TH2D *cos_theta_B0_LAMBDA0B_to_JPSI_P_K = 0;
TH2D *cos_theta_B0_BS_to_JPSI_PI_PI = 0;
TH2D *cos_theta_B0_BD_to_JPSI_PI_PI = 0;
TH2D *cos_theta_B0_BD_to_JPSI_K_PI = 0;
TH2D *cos_theta_B0_BS_to_JPSI_K_K = 0;    
TH2D *cos_theta_B0_bg = 0;  
TH2D *cos_theta_B0_BS_to_JPSI_K_PI = 0;
TH2D *cos_theta_B0_BD_to_JPSI_K_K = 0;
TH2D *cos_theta_B0_LAMBDA0B_to_JPSI_P_PI = 0;

TH2D *phi_psi_data = 0;
TH2D *phi_psi_LAMBDA0B_to_JPSI_P_K = 0;
TH2D *phi_psi_BS_to_JPSI_PI_PI = 0;
TH2D *phi_psi_BD_to_JPSI_PI_PI = 0;
TH2D *phi_psi_BD_to_JPSI_K_PI = 0;
TH2D *phi_psi_BS_to_JPSI_K_K = 0;    
TH2D *phi_psi_bg = 0;
TH2D *phi_psi_BS_to_JPSI_K_PI = 0;
TH2D *phi_psi_BD_to_JPSI_K_K = 0;
TH2D *phi_psi_LAMBDA0B_to_JPSI_P_PI = 0;

TH2D *JpsiK_2d_control_data = 0;
TH2D *JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K = 0;
TH2D *JpsiK_2d_control_BS_to_JPSI_K_K = 0;
TH2D *JpsiK_2d_control_BS_to_JPSI_PI_PI = 0;
TH2D *JpsiK_2d_control_BD_to_JPSI_K_PI = 0;
TH2D *JpsiK_2d_control_BD_to_JPSI_PI_PI = 0;
TH2D *JpsiK_2d_control_BS_to_JPSI_K_PI = 0;
TH2D *JpsiK_2d_control_BD_to_JPSI_K_K = 0;
TH2D *JpsiK_2d_control_LAMBDA0B_to_JPSI_P_PI = 0;

TH1D *KK_1d_control_data = 0;
TH1D *KK_1d_control_LAMBDA0B_to_JPSI_P_K = 0;
TH1D *KK_1d_control_BS_to_JPSI_K_K = 0;
TH1D *KK_1d_control_BS_to_JPSI_PI_PI = 0;
TH1D *KK_1d_control_BD_to_JPSI_K_PI = 0;
TH1D *KK_1d_control_BD_to_JPSI_PI_PI = 0;
TH1D *KK_1d_control_BS_to_JPSI_K_PI = 0;
TH1D *KK_1d_control_BD_to_JPSI_K_K = 0;
TH1D *KK_1d_control_LAMBDA0B_to_JPSI_P_PI = 0;

TH2D *JpsiK_2d_controlLb_data = 0;
TH2D *JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K = 0;
TH2D *JpsiK_2d_controlLb_BS_to_JPSI_K_K = 0;
TH2D *JpsiK_2d_controlLb_BS_to_JPSI_PI_PI = 0;
TH2D *JpsiK_2d_controlLb_BD_to_JPSI_K_PI = 0;
TH2D *JpsiK_2d_controlLb_BD_to_JPSI_PI_PI = 0;
TH2D *JpsiK_2d_controlLb_BS_to_JPSI_K_PI = 0;
TH2D *JpsiK_2d_controlLb_BD_to_JPSI_K_K = 0;
TH2D *JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI = 0;

TH2D *Jpsip_2d_controlLb_data = 0;
TH2D *Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K = 0;
TH2D *Jpsip_2d_controlLb_BS_to_JPSI_K_K = 0;
TH2D *Jpsip_2d_controlLb_BS_to_JPSI_PI_PI = 0;
TH2D *Jpsip_2d_controlLb_BD_to_JPSI_K_PI = 0;
TH2D *Jpsip_2d_controlLb_BD_to_JPSI_PI_PI = 0;
TH2D *Jpsip_2d_controlLb_BS_to_JPSI_K_PI = 0;
TH2D *Jpsip_2d_controlLb_BD_to_JPSI_K_K = 0;
TH2D *Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_PI = 0;

TH2D *pK_2d_controlLb_data = 0;
TH2D *pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K = 0;
TH2D *pK_2d_controlLb_BS_to_JPSI_K_K = 0;
TH2D *pK_2d_controlLb_BS_to_JPSI_PI_PI = 0;
TH2D *pK_2d_controlLb_BD_to_JPSI_K_PI = 0;
TH2D *pK_2d_controlLb_BD_to_JPSI_PI_PI = 0;
TH2D *pK_2d_controlLb_BS_to_JPSI_K_PI = 0;
TH2D *pK_2d_controlLb_BD_to_JPSI_K_K = 0;
TH2D *pK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI = 0;

TH2D *Jpsipi_2d_bg = 0;
TH2D *JpsiK_2d_bg = 0;
TH2D *Kpi_2d_bg = 0;

TH2D *JpsiK_2d_control_bg = 0;
TH1D *KK_1d_control_bg = 0;

TH2D *JpsiK_2d_controlLb_bg = 0;
TH2D *Jpsip_2d_controlLb_bg = 0;
TH2D *pK_2d_controlLb_bg = 0;

TH1D *hist_buffer = 0;

//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__||4 track fit||__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//

TH2D *Kpi_piK_2d_data = 0;
TH2D *Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K = 0;
TH2D *Kpi_piK_2d_BS_to_JPSI_PI_PI = 0;
TH2D *Kpi_piK_2d_BD_to_JPSI_PI_PI = 0;
TH2D *Kpi_piK_2d_BD_to_JPSI_K_PI = 0;
TH2D *Kpi_piK_2d_BS_to_JPSI_K_K = 0;
TH2D *Kpi_piK_2d_BD_to_JPSI_K_K = 0;
TH2D *Kpi_piK_2d_BS_to_JPSI_K_PI = 0;
TH2D *Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI = 0;

TH2D *KK_pipi_2d_data = 0;
TH2D *KK_pipi_2d_LAMBDA0B_to_JPSI_P_K = 0;
TH2D *KK_pipi_2d_BS_to_JPSI_PI_PI = 0;
TH2D *KK_pipi_2d_BD_to_JPSI_PI_PI = 0;
TH2D *KK_pipi_2d_BD_to_JPSI_K_PI = 0;
TH2D *KK_pipi_2d_BS_to_JPSI_K_K = 0;
TH2D *KK_pipi_2d_BD_to_JPSI_K_K = 0;
TH2D *KK_pipi_2d_BS_to_JPSI_K_PI = 0;
TH2D *KK_pipi_2d_LAMBDA0B_to_JPSI_P_PI = 0;

TH2D *pK_Kp_2d_rotate_data = 0;
TH2D *pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_K = 0;
TH2D *pK_Kp_2d_rotate_BS_to_JPSI_PI_PI = 0;
TH2D *pK_Kp_2d_rotate_BD_to_JPSI_PI_PI = 0;
TH2D *pK_Kp_2d_rotate_BD_to_JPSI_K_PI = 0;
TH2D *pK_Kp_2d_rotate_BS_to_JPSI_K_K = 0;
TH2D *pK_Kp_2d_rotate_BD_to_JPSI_K_K = 0;
TH2D *pK_Kp_2d_rotate_BS_to_JPSI_K_PI = 0;
TH2D *pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_PI = 0;

TH1D *KK_1d_data = 0;
TH1D *KK_1d_LAMBDA0B_to_JPSI_P_K = 0;
TH1D *KK_1d_BS_to_JPSI_PI_PI = 0;
TH1D *KK_1d_BD_to_JPSI_PI_PI = 0;
TH1D *KK_1d_BD_to_JPSI_K_PI = 0;
TH1D *KK_1d_BS_to_JPSI_K_K = 0;
TH1D *KK_1d_BD_to_JPSI_K_K = 0;
TH1D *KK_1d_BS_to_JPSI_K_PI = 0;
TH1D *KK_1d_LAMBDA0B_to_JPSI_P_PI = 0;

TH1D *pipi_1d_data = 0;
TH1D *pipi_1d_LAMBDA0B_to_JPSI_P_K = 0;
TH1D *pipi_1d_BS_to_JPSI_PI_PI = 0;
TH1D *pipi_1d_BD_to_JPSI_PI_PI = 0;
TH1D *pipi_1d_BD_to_JPSI_K_PI = 0;
TH1D *pipi_1d_BS_to_JPSI_K_K = 0;
TH1D *pipi_1d_BD_to_JPSI_K_K = 0;
TH1D *pipi_1d_BS_to_JPSI_K_PI = 0;
TH1D *pipi_1d_LAMBDA0B_to_JPSI_P_PI = 0;

TH2D *pK_Kp_2d_data = 0;
TH2D *pK_Kp_2d_LAMBDA0B_to_JPSI_P_K = 0;
TH2D *pK_Kp_2d_BS_to_JPSI_PI_PI = 0;
TH2D *pK_Kp_2d_BD_to_JPSI_PI_PI = 0;
TH2D *pK_Kp_2d_BD_to_JPSI_K_PI = 0;
TH2D *pK_Kp_2d_BS_to_JPSI_K_K = 0;
TH2D *pK_Kp_2d_BD_to_JPSI_K_K = 0;
TH2D *pK_Kp_2d_BS_to_JPSI_K_PI = 0;
TH2D *pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI = 0;

TH2D *ppi_pip_2d_data = 0;
TH2D *ppi_pip_2d_LAMBDA0B_to_JPSI_P_K = 0;
TH2D *ppi_pip_2d_BS_to_JPSI_PI_PI = 0;
TH2D *ppi_pip_2d_BD_to_JPSI_PI_PI = 0;
TH2D *ppi_pip_2d_BD_to_JPSI_K_PI = 0;
TH2D *ppi_pip_2d_BS_to_JPSI_K_K = 0;
TH2D *ppi_pip_2d_BD_to_JPSI_K_K = 0;
TH2D *ppi_pip_2d_BS_to_JPSI_K_PI = 0;
TH2D *ppi_pip_2d_LAMBDA0B_to_JPSI_P_PI = 0;

#define pK_Kp_bins 64
#define pK_Kp_low 5.200
#define pK_Kp_high 6.800
#define pK_Kp_bins2 66
#define pK_Kp_low2 5.150
#define pK_Kp_high2 6.800

#define ppi_pip_bins 74
#define ppi_pip_low 5.05
#define ppi_pip_high 6.9
#define ppi_pip_bins2 84
#define ppi_pip_low2 4.9
#define ppi_pip_high2 7.

#define pK_Kp_bins3 50
#define pK_Kp_low3 5.200
#define pK_Kp_high3 7.000
#define pK_Kp_bins4 51
#define pK_Kp_low4 5.150
#define pK_Kp_high4 7.000

#define Kpi_piK_bins 40
#define Kpi_piK_low 4.900
#define Kpi_piK_high 5.700
#define Kpi_piK_bins2 62
#define Kpi_piK_low2 4.650
#define Kpi_piK_high2 5.900

#define cos_bins 10
#define cos_low 0.0
#define cos_high 1.0

#define phi_bins 10
#define phi_low -3.1416
#define phi_high 3.1416

#define Jpsipi_bins 50
#define Jpsipi_low 3.300
#define Jpsipi_high 5.000

#define JpsiK_bins 50
#define JpsiK_low 3.500
#define JpsiK_high 5.200

#define Kpi_bins 50
#define Kpi_low 1.550
#define Kpi_high 2.400

#define KK_bins 62
#define KK_low 1.680
#define KK_high 2.300

#define JpsiK_bins2 100
#define JpsiK_low2 3.575
#define JpsiK_high2 4.850

#define JpsiK_bins3 50
#define JpsiK_low3 3.575
#define JpsiK_high3 4.850

#define KK_pipi_bins 40
#define KK_pipi_low 4.970
#define KK_pipi_high 6.000

#define KK_pipi_bins2 40
#define KK_pipi_low2 4.550
#define KK_pipi_high2 5.650

#define JpsiK_bins4 50
#define JpsiK_low4 3.600
#define JpsiK_high4 4.850

#define Jpsip_bins 50
#define Jpsip_low 4.000
#define Jpsip_high 5.250

#define pK_bins 50
#define pK_low 2.000
#define pK_high 2.800

#define pK_Kp_bins5 50
#define pK_Kp_low5 7.400
#define pK_Kp_high5 9.500

#define pK_Kp_bins6 50
#define pK_Kp_low6 -1.000
#define pK_Kp_high6 0.900

#define KK_bins2 100
#define KK_low2 4.970
#define KK_high2 6.000

#define pipi_bins 100
#define pipi_low 4.550
#define pipi_high 5.650

#define pipi_bins2 440
#define pipi_low2 4.550
#define pipi_high2 5.650

TH2 *Kpi_piK_2d_full = new TH2D("Kpi_piK_2d_full", "Kpi_piK_2d_full", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);
TH2 *Kpi_piK_2d_bg = new TH2D("Kpi_piK_2d_bg", "Kpi_piK_2d_bg", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);
    
/*TH2 *Kpi_piK_2d_full_for_ea = new TH2D("Kpi_piK_2d_full_for_ea", "Kpi_piK_2d_full_for_ea", 40, 4900. * GEV, 5700. * GEV, 62, 4650. * GEV, 5900. * GEV);
TH2 *Kpi_piK_2d_bg_for_ea = new TH2D("Kpi_piK_2d_bg_for_ea", "Kpi_piK_2d_bg_for_ea", 40, 4900. * GEV, 5700. * GEV, 62, 4650. * GEV, 5900. * GEV);*/

TH2 *KK_pipi_2d_full = new TH2D("KK_pipi_2d_full", "KK_pipi_2d_full", 40, 4970. * GEV, 6000. * GEV, 40, 4550. * GEV, 5650. * GEV);
TH2 *KK_pipi_2d_bg = new TH2D("KK_pipi_2d_bg", "KK_pipi_2d_bg", 40, 4970. * GEV, 6000. * GEV, 40, 4550. * GEV, 5650. * GEV);

TH2 *pK_Kp_2d_bg = new TH2D("pK_Kp_2d_bg", "pK_Kp_2d_bg", pK_Kp_bins, pK_Kp_low, pK_Kp_high, pK_Kp_bins2, pK_Kp_low2, pK_Kp_high2);
TH2 *pK_Kp_2d_full = new TH2D("pK_Kp_2d_full", "pK_Kp_2d_full", pK_Kp_bins, pK_Kp_low, pK_Kp_high, pK_Kp_bins2, pK_Kp_low2, pK_Kp_high2);

TH2 *ppi_pip_2d_bg = new TH2D("ppi_pip_2d_bg", "ppi_pip_2d_bg", ppi_pip_bins, ppi_pip_low, ppi_pip_high, ppi_pip_bins2, ppi_pip_low2, ppi_pip_high2);
TH2 *ppi_pip_2d_full = new TH2D("ppi_pip_2d_full", "ppi_pip_2d_full", ppi_pip_bins, ppi_pip_low, ppi_pip_high, ppi_pip_bins2, ppi_pip_low2, ppi_pip_high2);
    
//TH2 *pK_Kp_2d_test_bg = new TH2D("pK_Kp_2d_test_bg", "pK_Kp_2d_test_bg", 33, 5200. * GEV, 6800. * GEV, 34, 5150. * GEV, 6800. * GEV);
    
TH2 *pK_Kp_2d_rotate_bg = new TH2D("pK_Kp_2d_rotate_bg", "pK_Kp_2d_rotate_bg", pK_Kp_bins5, pK_Kp_low5, pK_Kp_high5, pK_Kp_bins6, pK_Kp_low6, pK_Kp_high6);
TH2 *pK_Kp_2d_rotate_full = new TH2D("pK_Kp_2d_rotate_full", "pK_Kp_2d_rotate_full", pK_Kp_bins5, pK_Kp_low5, pK_Kp_high5, pK_Kp_bins6, pK_Kp_low6, pK_Kp_high6);
    
TH1 *KK_1d_full = new TH1D("KK_1d_full", "KK_1d_full", KK_bins2, KK_low2, KK_high2);
    
TH1 *pipi_1d_bg = new TH1D("pipi_1d_bg", "pipi_1d_bg", pipi_bins, pipi_low, pipi_high);
TH1 *pipi_1d_full = new TH1D("pipi_1d_full", "pipi_1d_full", pipi_bins, pipi_low, pipi_high);

/*TH2D *pK_Kp_2d_test_data = 0;
TH2D *pK_Kp_2d_test_LAMBDA0B_to_JPSI_P_K = 0;
TH2D *pK_Kp_2d_test_BS_to_JPSI_PI_PI = 0;
TH2D *pK_Kp_2d_test_BD_to_JPSI_PI_PI = 0;
TH2D *pK_Kp_2d_test_BD_to_JPSI_K_PI = 0;
TH2D *pK_Kp_2d_test_BS_to_JPSI_K_K = 0;*/

Float_t coef_4_to_s_BD_to_JPSI_K_PI, coef_4_to_c_BD_to_JPSI_K_PI, coef_4_to_clb_BD_to_JPSI_K_PI,
        coef_4_to_s_BS_to_JPSI_K_K, coef_4_to_c_BS_to_JPSI_K_K, coef_4_to_clb_BS_to_JPSI_K_K,
        coef_4_to_s_BD_to_JPSI_PI_PI, coef_4_to_c_BD_to_JPSI_PI_PI, coef_4_to_clb_BD_to_JPSI_PI_PI,
        coef_4_to_s_BS_to_JPSI_PI_PI, coef_4_to_c_BS_to_JPSI_PI_PI, coef_4_to_clb_BS_to_JPSI_PI_PI,
        coef_4_to_s_LAMBDA0B_to_JPSI_P_K, coef_4_to_c_LAMBDA0B_to_JPSI_P_K, coef_4_to_clb_LAMBDA0B_to_JPSI_P_K,
        coef_4_to_s_LAMBDA0B_to_JPSI_P_PI, coef_4_to_c_LAMBDA0B_to_JPSI_P_PI, coef_4_to_clb_LAMBDA0B_to_JPSI_P_PI,
        coef_4_to_s_BD_to_JPSI_K_K, coef_4_to_c_BD_to_JPSI_K_K, coef_4_to_clb_BD_to_JPSI_K_K,
        coef_4_to_s_BS_to_JPSI_K_PI, coef_4_to_c_BS_to_JPSI_K_PI, coef_4_to_clb_BS_to_JPSI_K_PI,
        coef_4_to_s_bg, coef_4_to_c_bg, coef_4_to_clb_bg;

//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//

float n_LAMBDA0B_to_JPSI_P_K,
        n_BD_to_JPSI_K_PI,
        n_BD_to_JPSI_PI_PI,
        n_BS_to_JPSI_PI_PI,
        n_BS_to_JPSI_K_K,
        n_LAMBDA0B_to_JPSI_P_PI,
        n_BD_to_JPSI_K_K,
        n_BS_to_JPSI_K_PI,
        n_comb_bg;
        
float n_control_LAMBDA0B_to_JPSI_P_K,
        n_control_BD_to_JPSI_K_PI,
        n_control_BD_to_JPSI_PI_PI,
        n_control_BS_to_JPSI_PI_PI,
        n_control_BS_to_JPSI_K_K,
        n_control_LAMBDA0B_to_JPSI_P_PI,
        n_control_BD_to_JPSI_K_K,
        n_control_BS_to_JPSI_K_PI,
        n_control_comb_bg;
        
float n_controlLb_LAMBDA0B_to_JPSI_P_K,
        n_controlLb_BD_to_JPSI_K_PI,
        n_controlLb_BD_to_JPSI_PI_PI,
        n_controlLb_BS_to_JPSI_PI_PI,
        n_controlLb_BS_to_JPSI_K_K,
        n_controlLb_LAMBDA0B_to_JPSI_P_PI,
        n_controlLb_BD_to_JPSI_K_K,
        n_controlLb_BS_to_JPSI_K_PI,
        n_controlLb_comb_bg;
        
/*int n_phase = 66148 - 66148 % 4,
    n_phase_control = 30487 - 30487 % 4,
    n_phase_BsKK = 10302 - 10302 % 4,
    n_phase_BsKK_control = 16593 - 16593 % 4;*/
    
Double_t n_global_BD_to_JPSI_K_PI, 
        n_global_BS_to_JPSI_K_K,
        n_global_BS_to_JPSI_K_K_ini,
        n_global_BS_to_JPSI_PI_PI,
        n_global_BD_to_JPSI_PI_PI,
        n_global_LAMBDA0B_to_JPSI_P_K,
        n_global_BD_to_JPSI_K_K,
        n_global_BS_to_JPSI_K_PI,
        n_global_LAMBDA0B_to_JPSI_P_PI;
    
int sa_a[40][62];

int calb_a[50][51];

TF1 *bg_func = new TF1("bg_func", "pow(abs(x - [0]), [1]) * exp([2] + (x - [8]) * [3] + pow((x - [8]), 2.) * [4] + pow((x - [8]), 3.) * [5] + pow((x - [8]), 4.) * [6] + pow((x - [8]), 5.) * [7])", 4.970, 6.);

TH1 *KK_1d_bg = new TH1D("KK_1d_bg", "KK_1d_bg", KK_bins2, KK_low2, KK_high2);

Int_t Kpi_piK_2d_nbinsX;
Int_t Kpi_piK_2d_nbinsY;
Int_t KK_1d_nbins;
Int_t pipi_1d_nbins;
Int_t pK_Kp_2d_nbinsX;
Int_t pK_Kp_2d_nbinsY;
Int_t Jpsipi_2d_nbinsX;
Int_t Jpsipi_2d_nbinsY;
Int_t Kpi_2d_nbinsX;
Int_t Kpi_2d_nbinsY;
Int_t JpsiK_2d_nbinsX;
Int_t JpsiK_2d_nbinsY;
Int_t cos_theta_Zc_nbinsX;
Int_t cos_theta_Zc_nbinsY;
Int_t cos_theta_psi_X_nbinsX;
Int_t cos_theta_psi_X_nbinsY;
Int_t phi_mu_X_nbinsX;
Int_t phi_mu_X_nbinsY;
Int_t JpsiK_2d_control_nbinsX;
Int_t JpsiK_2d_control_nbinsY;
Int_t KK_1d_control_nbins;
Int_t Jpsip_2d_controlLb_nbinsX;
Int_t Jpsip_2d_controlLb_nbinsY;
Int_t pK_2d_controlLb_nbinsX;
Int_t pK_2d_controlLb_nbinsY;
Int_t JpsiK_2d_controlLb_nbinsX;
Int_t JpsiK_2d_controlLb_nbinsY;

void signal_area_fit_func(Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag)
{
    Double_t chisq = 0.0, xx0;
    float w;
    n_fit_step++;
    
    Jpsipi_2d_BD_to_JPSI_K_PI->Reset();
    JpsiK_2d_BD_to_JPSI_K_PI->Reset();
    Kpi_2d_BD_to_JPSI_K_PI->Reset();
    
    Jpsipi_2d_BS_to_JPSI_K_K->Reset();
    JpsiK_2d_BS_to_JPSI_K_K->Reset();
    Kpi_2d_BS_to_JPSI_K_K->Reset();
    
    /*Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->Reset();
    JpsiK_2d_LAMBDA0B_to_JPSI_P_K->Reset();
    Kpi_2d_LAMBDA0B_to_JPSI_P_K->Reset();*/
    
    JpsiK_2d_control_BD_to_JPSI_K_PI->Reset();
    KK_1d_control_BD_to_JPSI_K_PI->Reset();
    
    JpsiK_2d_control_BS_to_JPSI_K_K->Reset();
    KK_1d_control_BS_to_JPSI_K_K->Reset();
    
    /*JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K->Reset();
    KK_1d_control_LAMBDA0B_to_JPSI_P_K->Reset();*/
    
    JpsiK_2d_controlLb_BD_to_JPSI_K_PI->Reset();
    Jpsip_2d_controlLb_BD_to_JPSI_K_PI->Reset();
    pK_2d_controlLb_BD_to_JPSI_K_PI->Reset();
    
    JpsiK_2d_controlLb_BS_to_JPSI_K_K->Reset();
    Jpsip_2d_controlLb_BS_to_JPSI_K_K->Reset();
    pK_2d_controlLb_BS_to_JPSI_K_K->Reset();
    
    /*JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Reset();
    Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Reset();
    pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Reset();*/
    
    Kpi_piK_2d_BD_to_JPSI_K_PI->Reset();
    Kpi_piK_2d_BS_to_JPSI_K_K->Reset();
    //Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Reset();
    
    KK_1d_BD_to_JPSI_K_PI->Reset();
    KK_1d_BS_to_JPSI_K_K->Reset();
    //KK_1d_LAMBDA0B_to_JPSI_P_K->Reset();
    
    pipi_1d_BD_to_JPSI_K_PI->Reset();
    pipi_1d_BS_to_JPSI_K_K->Reset();
    //pipi_1d_LAMBDA0B_to_JPSI_P_K->Reset();
    
    pK_Kp_2d_BD_to_JPSI_K_PI->Reset();
    pK_Kp_2d_BS_to_JPSI_K_K->Reset();
    //pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->Reset();
    
    ppi_pip_2d_BD_to_JPSI_K_PI->Reset();
    ppi_pip_2d_BS_to_JPSI_K_K->Reset();
    //ppi_pip_2d_LAMBDA0B_to_JPSI_P_K->Reset();
    
    cos_theta_Zc_BD_to_JPSI_K_PI->Reset();
    cos_theta_Zc_BS_to_JPSI_K_K->Reset();
    //cos_theta_Zc_LAMBDA0B_to_JPSI_P_K->Reset();
    
    phi_K_BD_to_JPSI_K_PI->Reset();
    phi_K_BS_to_JPSI_K_K->Reset();
    //phi_K_LAMBDA0B_to_JPSI_P_K->Reset();
    
    alpha_mu_BD_to_JPSI_K_PI->Reset();
    alpha_mu_BS_to_JPSI_K_K->Reset();
    //alpha_mu_LAMBDA0B_to_JPSI_P_K->Reset();
    
    phi_mu_Kstar_BD_to_JPSI_K_PI->Reset();
    phi_mu_Kstar_BS_to_JPSI_K_K->Reset();
    //phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K->Reset();
    
    phi_mu_X_BD_to_JPSI_K_PI->Reset();
    phi_mu_X_BS_to_JPSI_K_K->Reset();
    //phi_mu_X_LAMBDA0B_to_JPSI_P_K->Reset();
    
    cos_theta_psi_X_BD_to_JPSI_K_PI->Reset();
    cos_theta_psi_X_BS_to_JPSI_K_K->Reset();
    //cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K->Reset();
    
    cos_theta_Kstar_BD_to_JPSI_K_PI->Reset();
    cos_theta_Kstar_BS_to_JPSI_K_K->Reset();
    //cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->Reset();
    
    cos_theta_psi_Kstar_BD_to_JPSI_K_PI->Reset();
    cos_theta_psi_Kstar_BS_to_JPSI_K_K->Reset();
    //cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K->Reset();
    
    cos_theta_B0_BD_to_JPSI_K_PI->Reset();
    cos_theta_B0_BS_to_JPSI_K_K->Reset();
    //cos_theta_B0_LAMBDA0B_to_JPSI_P_K->Reset();
    
    std::thread thr[THREADS];
    int trd;
    
    bool JpsipK_4tr_fit_mode = JpsipK_fit_mode || JpsipK_fit_mode_comb_only || JpsipK_fit_mode_plus_JpsiKpi || global_area_no_comb_form_4tr;
    bool JpsiKpi_4tr_fit_mode = JpsiKpi_fit_mode || JpsiKpi_fit_mode_no_signal || Jpsipipi_fit_mode_plus_JpsiKpi || JpsiKK_fit_mode_plus_JpsiKpi || Jpsipipi_fit_mode_plus_JpsiKpi_lbfix || JpsiKK_fit_mode_plus_JpsiKpi_lbpipi_fix || JpsiKK_fit_mode_plus_JpsiKpi_no_control_lbpipi_fix || global_area_no_comb_form_4tr || JpsiKpi_fit_mode_comb_only; //|| JpsipK_fit_mode_plus_JpsiKpi 
    bool Jpsipipi_4tr_fit_mode = Jpsipipi_fit_mode_plus_JpsiKpi_lbfix || Jpsipipi_fit_mode_plus_JpsiKpi || Jpsipipi_fit_mode || global_area_no_comb_form_4tr || Jpsipipi_fit_mode_comb_only;
    bool JpsiKK_4tr_fit_mode = JpsiKK_fit_mode || JpsiKK_fit_mode_plus_JpsiKpi || JpsiKK_fit_mode_plus_JpsiKpi_lbpipi_fix || JpsiKK_fit_mode_no_control || JpsiKK_fit_mode_plus_JpsiKpi_no_control_lbpipi_fix || global_area_no_comb_form_4tr || JpsiKK_fit_mode_comb_only;
    bool Jpsippi_4tr_fit_mode = Jpsippi_fit_mode_comb_only;
    bool control_area_Bs_fit_mode = JpsiKK_fit_mode || JpsiKK_fit_mode_plus_JpsiKpi || JpsiKK_fit_mode_plus_JpsiKpi_lbpipi_fix;
    bool Jpsipi_fit_mode = JpsiKpi_fit_mode || signal_area_fit_mode || signal_area_fit_mode_short || signal_area_fit_mode_short_noZc || signal_area_fit_mode_noZc || signal_area_fit_mode_onlyZc;
    bool JpsiK_fit_mode = JpsiKpi_fit_mode || signal_area_fit_mode || signal_area_fit_mode_short || signal_area_fit_mode_short_noZc || signal_area_fit_mode_noZc;
    bool Kpi_fit_mode = JpsiKpi_fit_mode || signal_area_fit_mode || signal_area_fit_mode_short || signal_area_fit_mode_short_noZc || signal_area_fit_mode_noZc || signal_area_fit_mode_onlyZc;
    bool cos_theta_Zc_fit_mode = JpsiKpi_fit_mode || signal_area_fit_mode || signal_area_fit_mode_short || signal_area_fit_mode_short_noZc || signal_area_fit_mode_noZc;
    //bool phi_K_fit_mode = 
    //bool alpha_mu_fit_mode = 
    //bool phi_mu_Kstar_fit_mode = 
    //bool phi_mu_X_fit_mode = 
    //bool cos_theta_psi_X_fit_mode = 
    //bool cos_theta_Kstar_fit_mode = 
    //bool cos_theta_psi_Kstar_fit_mode = 
    
    //cout << "after MT" << endl;
    for(trd=0; trd < THREADS; trd++)
    {
        thr[trd] = (std::thread([trd, par](){
        if (JpsiKpi_fit_mode || signal_area_fit_mode || signal_area_fit_mode_short || signal_area_fit_mode_noZc || signal_area_fit_mode_short_noZc || signal_area_fit_mode_onlyZc)
        {
            for(int ent0 = 0; ent0 < phs_Bd/THREADS; ent0++)
            {
                int ent = ent0 + trd*phs_Bd/THREADS;
                weight[ent] = HamplitudeXKstar_total_accelerated(Jpsipi_truth[ent] / 1000., Kpi_truth[ent] / 1000., 5.27964, par[45], 4.43, par[44], 0.181,
                                    std::complex<float>(par[40] * cos(par[41]), par[40] * sin(par[41])), 
                                    std::complex<float>(par[42] * cos(par[43]), par[42] * sin(par[43])),
                                    std::complex<float>(0., 0.), 
                                    std::complex<float>(0., 0.),
                                    std::complex<float>(par[0] * cos(par[1]), par[0] * sin(par[1])), 
                                    std::complex<float>(par[16] * cos(par[17]), par[16] * sin(par[17])), 
                                    std::complex<float>(par[18] * cos(par[19]), par[18] * sin(par[19])),
                                    std::complex<float>(par[2] * cos(par[3]), par[2] * sin(par[3])), 
                                    std::complex<float>(0., 0.),
                                    std::complex<float>(0., 0.),
                                    std::complex<float>(par[4] * cos(par[5]), par[4] * sin(par[5])), 
                                    std::complex<float>(par[20] * cos(par[21]), par[20] * sin(par[21])), 
                                    std::complex<float>(par[22] * cos(par[23]), par[22] * sin(par[23])),
                                    std::complex<float>(par[6] * cos(par[7]), par[6] * sin(par[7])), 
                                    std::complex<float>(par[24] * cos(par[25]), par[24] * sin(par[25])), 
                                    std::complex<float>(par[26] * cos(par[27]), par[26] * sin(par[27])),
                                    std::complex<float>(par[8] * cos(par[9]), par[8] * sin(par[9])), 
                                    std::complex<float>(par[28] * cos(par[29]), par[28] * sin(par[29])), 
                                    std::complex<float>(par[30] * cos(par[31]), par[30] * sin(par[31])),
                                    std::complex<float>(par[10] * cos(par[11]), par[10] * sin(par[11])), 
                                    std::complex<float>(0., 0.), 
                                    std::complex<float>(0., 0.),
                                    std::complex<float>(par[14] * cos(par[15]), par[14] * sin(par[15])),
                                    std::complex<float>(par[32] * cos(par[33]), par[32] * sin(par[33])),
                                    std::complex<float>(par[34] * cos(par[35]), par[34] * sin(par[35])),
                                    std::complex<float>(par[12] * cos(par[13]), par[12] * sin(par[13])),
                                    std::complex<float>(par[36] * cos(par[37]), par[36] * sin(par[37])),
                                    std::complex<float>(par[38] * cos(par[39]), par[38] * sin(par[39])),
                                    std::complex<float>(par[217] * cos(par[218]), par[217] * sin(par[218])),
                                    theta_X_a[ent], theta_Kstar_a[ent], theta_psi_X_a[ent], theta_psi_Kstar_a[ent], phi_mu_X_a[ent], phi_mu_Kstar_a[ent], 
                                    phi_K_a[ent], alpha_mu_a[ent],
                                    BWK1410_1_1_minus[ent], BWK1410_1_2_minus[ent], BWK1410_1_3_minus[ent], 
                                    BWK1430_0_1_minus[ent], 
                                    BWK1430_2_1_minus[ent], BWK1430_2_2_minus[ent], BWK1430_2_3_minus[ent], 
                                    BWK1680_1_1_minus[ent], BWK1680_1_2_minus[ent], BWK1680_1_3_minus[ent], 
                                    BWK1780_3_1_minus[ent], BWK1780_3_2_minus[ent], BWK1780_3_3_minus[ent], 
                                    BWK1950_0_1_minus[ent], 
                                    BWK1980_2_1_minus[ent], BWK1980_2_2_minus[ent], BWK1980_2_3_minus[ent], 
                                    BWK2045_4_1_minus[ent], BWK2045_4_2_minus[ent], BWK2045_4_3_minus[ent],
                                    BWK1410_1_1_plus[ent], BWK1410_1_2_plus[ent], BWK1410_1_3_plus[ent], 
                                    BWK1430_0_1_plus[ent], 
                                    BWK1430_2_1_plus[ent], BWK1430_2_2_plus[ent], BWK1430_2_3_plus[ent], 
                                    BWK1680_1_1_plus[ent], BWK1680_1_2_plus[ent], BWK1680_1_3_plus[ent], 
                                    BWK1780_3_1_plus[ent], BWK1780_3_2_plus[ent], BWK1780_3_3_plus[ent], 
                                    BWK1950_0_1_plus[ent], 
                                    BWK1980_2_1_plus[ent], BWK1980_2_2_plus[ent], BWK1980_2_3_plus[ent], 
                                    BWK2045_4_1_plus[ent], BWK2045_4_2_plus[ent], BWK2045_4_3_plus[ent]) * w_dimuon_a[ent];
            }
        }
        
        if (JpsiKK_fit_mode || JpsiKK_fit_mode_plus_JpsiKpi || JpsiKK_fit_mode_plus_JpsiKpi_lbpipi_fix)
        {
            for(int ent0 = 0; ent0 < phs_Bs/THREADS; ent0++)
            {
                int ent = ent0 + trd*phs_Bs/THREADS;
                weight_BsKK[ent] = Hamplitudef_total(0., 0., KK_truth_BsKK[ent] / 1000., 5.36688, 0., 0., 0., 0.,
						std::complex<float>(0., 0.), std::complex<float>(0., 0.),
						std::complex<float> (par[47] * cos(par[48]), par[47] * sin(par[48])), 
                        std::complex<float> (par[49] * cos(par[50]), par[49] * sin(par[50])), 
                        std::complex<float> (par[51] * cos(par[52]), par[51] * sin(par[52])),
						std::complex<float> (par[53] * cos(par[54]), par[53] * sin(par[54])), 
                        std::complex<float> (par[55] * cos(par[56]), par[55] * sin(par[56])), 
                        std::complex<float> (par[57] * cos(par[58]), par[57] * sin(par[58])),
						std::complex<float> (par[59] * cos(par[60]), par[59] * sin(par[60])), 
                        std::complex<float> (par[61] * cos(par[62]), par[61] * sin(par[62])), 
                        std::complex<float> (par[63] * cos(par[64]), par[63] * sin(par[64])),
						std::complex<float> (par[65] * cos(par[66]), par[65] * sin(par[66])), 
                        std::complex<float> (par[67] * cos(par[68]), par[67] * sin(par[68])),
                        std::complex<float> (par[69] * cos(par[70]), par[69] * sin(par[70])),
						std::complex<float> (par[71] * cos(par[72]), par[71] * sin(par[72])), 
                        std::complex<float> (par[73] * cos(par[74]), par[73] * sin(par[74])), 
                        std::complex<float> (par[75] * cos(par[76]), par[75] * sin(par[76])),
						std::complex<float> (par[77] * cos(par[78]), par[77] * sin(par[78])),
						0., thetaF_a[ent], 0., theta_psi_F_a[ent], 0., phi_mu_F_a[ent], phi_KF_a[ent], 0., 0., 0.) * w_dimuon_a_BsKK[ent];
            }
        }
        
        /*for(int ent0 = 0; ent0 < phs_Lb/THREADS; ent0++)
        {
            int ent = ent0 + trd*phs_Lb/THREADS;
            weight_Lb[ent] = HamplitudePcLstar_total(Jpsip_truth_Lb[i] / 1000., pK_truth_Lb[i] / 1000., 5.6196, par[209], par[210], 0., 0., par[211], par[212], 0., 0.,
						std::complex<float> (par[189] * cos(par[190]), par[189] * cos(par[190])), 
                        std::complex<float> (par[191] * cos(par[192]), par[191] * cos(par[192])), 
                        std::complex<float> (par[193] * cos(par[194]), par[193] * cos(par[194])), 
                        std::complex<float> (par[195] * cos(par[196]), par[195] * cos(par[196])), 
                        std::complex<float> (par[197] * cos(par[198]), par[197] * cos(par[198])),
						std::complex<float> (par[199] * cos(par[200]), par[199] * cos(par[200])), 
                        std::complex<float> (par[201] * cos(par[202]), par[201] * cos(par[202])), 
                        std::complex<float> (par[203] * cos(par[204]), par[203] * cos(par[204])), 
                        std::complex<float> (par[205] * cos(par[206]), par[205] * cos(par[206])), 
                        std::complex<float> (par[207] * cos(par[208]), par[207] * cos(par[208])),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (par[121] * cos(par[122]), par[121] * cos(par[122])), 
                        std::complex<float> (par[123] * cos(par[124]), par[123] * cos(par[124])), 
                        std::complex<float> (par[125] * cos(par[126]), par[125] * cos(par[126])), 
                        std::complex<float> (par[127] * cos(par[128]), par[127] * cos(par[128])),
						std::complex<float> (par[129] * cos(par[130]), par[129] * cos(par[130])), 
                        std::complex<float> (par[131] * cos(par[132]), par[131] * cos(par[132])), 
                        std::complex<float> (par[133] * cos(par[134]), par[133] * cos(par[134])),
						std::complex<float> (par[135] * cos(par[136]), par[135] * cos(par[136])), 
                        std::complex<float> (par[137] * cos(par[138]), par[137] * cos(par[138])), 
                        std::complex<float> (par[139] * cos(par[140]), par[139] * cos(par[140])),
						std::complex<float> (par[141] * cos(par[142]), par[141] * cos(par[142])), 
                        std::complex<float> (par[143] * cos(par[144]), par[143] * cos(par[144])), 
                        std::complex<float> (par[145] * cos(par[146]), par[145] * cos(par[146])),
						std::complex<float> (par[147] * cos(par[148]), par[147] * cos(par[148])), 
                        std::complex<float> (par[149] * cos(par[150]), par[149] * cos(par[150])), 
                        std::complex<float> (par[151] * cos(par[152]), par[151] * cos(par[152])),
						std::complex<float> (par[153] * cos(par[154]), par[153] * cos(par[154])), 
                        std::complex<float> (par[155] * cos(par[156]), par[155] * cos(par[156])), 
                        std::complex<float> (par[157] * cos(par[158]), par[157] * cos(par[158])),
						std::complex<float> (par[159] * cos(par[160]), par[159] * cos(par[160])), 
                        std::complex<float> (par[161] * cos(par[162]), par[161] * cos(par[162])),
                        std::complex<float> (par[163] * cos(par[164]), par[163] * cos(par[164])),
						std::complex<float> (par[165] * cos(par[166]), par[165] * cos(par[166])), 
                        std::complex<float> (par[167] * cos(par[168]), par[167] * cos(par[168])), 
                        std::complex<float> (par[169] * cos(par[170]), par[169] * cos(par[170])),
						std::complex<float> (par[171] * cos(par[172]), par[171] * cos(par[172])), 
                        std::complex<float> (par[173] * cos(par[174]), par[173] * cos(par[174])), 
                        std::complex<float> (par[175] * cos(par[176]), par[175] * cos(par[176])),
						std::complex<float> (par[177] * cos(par[178]), par[177] * cos(par[178])), 
                        std::complex<float> (par[179] * cos(par[180]), par[179] * cos(par[180])), 
                        std::complex<float> (par[181] * cos(par[182]), par[181] * cos(par[182])),
						std::complex<float> (par[183] * cos(par[184]), par[183] * cos(par[184])), 
                        std::complex<float> (par[185] * cos(par[186]), par[185] * cos(par[186])), 
                        std::complex<float> (par[187] * cos(par[188]), par[187] * cos(par[188])),
						theta_Lstar_a[i], theta_lambda_b_ls_a[i], thete_psi_Lstar_a[i], phi_mu_Lstar_a[i], phi_K_Lstar_a[i],
						theta_Pc_a[i], theta_lambda_b_a[i], theta_psi_a[i], phi_mu_Pc_a[i], phi_Pc_a[i], phi_psi_a[i], 
						alpha_mu_Lb_a[i], theta_p_a[i]) * w_dimuon_a_Lb[i];
        }*/
    }));
    }
    
    for(trd=0; trd < THREADS; trd++)
    {
        ////printf("joining %d thread...\n", trd);
        thr[trd].join();    
    }  
    
    coef_4_to_s_BD_to_JPSI_K_PI = 0.;
    coef_4_to_c_BD_to_JPSI_K_PI = 0.;
    coef_4_to_s_BS_to_JPSI_K_K = 0.;
    coef_4_to_c_BS_to_JPSI_K_K = 0.;
    //coef_4_to_s_LAMBDA0B_to_JPSI_P_K = 0.;
    //coef_4_to_c_LAMBDA0B_to_JPSI_P_K = 0.;
    
    for(int i = 0; i < phs_Bd - phs_Bd % THREADS; i++)
    {
        if (signal_a[i]) {
            Jpsipi_2d_BD_to_JPSI_K_PI->Fill(Jpsipi1_mass[i], Jpsipi2_mass[i], weight[i]);
            JpsiK_2d_BD_to_JPSI_K_PI->Fill(JpsiK1_mass[i], JpsiK2_mass[i], weight[i]);
            Kpi_2d_BD_to_JPSI_K_PI->Fill(K1pi2_mass[i], K2pi1_mass[i], weight[i]);
            cos_theta_Zc_BD_to_JPSI_K_PI->Fill(cos_theta_Zc_Kpi_Bd[i], cos_theta_Zc_piK_Bd[i], weight[i]);
            cos_theta_psi_Kstar_BD_to_JPSI_K_PI->Fill(cos_theta_psi_Kstar_Kpi_Bd[i], cos_theta_psi_Kstar_piK_Bd[i], weight[i]);
            phi_mu_X_BD_to_JPSI_K_PI->Fill(phi_mu_X_Kpi_Bd[i], phi_mu_X_piK_Bd[i], weight[i]);
            phi_K_BD_to_JPSI_K_PI->Fill(phi_K_Kpi_Bd[i], phi_K_piK_Bd[i], weight[i]);
            cos_theta_Kstar_BD_to_JPSI_K_PI->Fill(cos_theta_Kstar_Kpi_Bd[i], cos_theta_Kstar_piK_Bd[i], weight[i]);
            cos_theta_B0_BD_to_JPSI_K_PI->Fill(cos_theta_B0_Kpi_Bd[i], cos_theta_B0_piK_Bd[i], weight[i]);
            
            /*phi_K_BD_to_JPSI_K_PI->Fill(phi_K_Kpi_Bd[i], phi_K_piK_Bd[i], weight[i]);
            alpha_mu_BD_to_JPSI_K_PI->Fill(alpha_mu_Kpi_Bd[i], alpha_mu_piK_Bd[i], weight[i]);
            phi_mu_Kstar_BD_to_JPSI_K_PI->Fill(phi_mu_Kstar_Kpi_Bd[i], phi_mu_Kstar_piK_Bd[i], weight[i]);
            cos_theta_Kstar_BD_to_JPSI_K_PI->Fill(cos_theta_Kstar_Kpi_Bd[i], cos_theta_Kstar_piK_Bd[i], weight[i]);
            cos_theta_psi_Kstar_BD_to_JPSI_K_PI->Fill(cos_theta_psi_Kstar_Kpi_Bd[i], cos_theta_psi_Kstar_piK_Bd[i], weight[i]);
            cos_theta_Kstar_BD_to_JPSI_K_PI->Fill(cos_theta_Kstar_Kpi_Bd[i], cos_theta_Kstar_piK_Bd[i], weight[i]);
            */
            coef_4_to_s_BD_to_JPSI_K_PI += weight[i];
        }
        
        if (control_a[i]) {
            JpsiK_2d_control_BD_to_JPSI_K_PI->Fill(JpsiK1_mass[i], JpsiK2_mass[i], weight[i]);
            KK_1d_control_BD_to_JPSI_K_PI->Fill(KK_mass[i], weight[i]);
            coef_4_to_c_BD_to_JPSI_K_PI += weight[i];
        }
        
        if (controlLb_a[i]) {
            JpsiK_2d_controlLb_BD_to_JPSI_K_PI->Fill(JpsiK1_mass[i], JpsiK2_mass[i], weight[i]);
            Jpsip_2d_controlLb_BD_to_JPSI_K_PI->Fill(Jpsip1_mass[i], Jpsip2_mass[i], weight[i]);
            pK_2d_controlLb_BD_to_JPSI_K_PI->Fill(p1K2_mass[i], p2K1_mass[i], weight[i]);
            coef_4_to_clb_BD_to_JPSI_K_PI += weight[i];
        }
            
        Kpi_piK_2d_BD_to_JPSI_K_PI->Fill(JpsiKpi_mass[i], JpsipiK_mass[i], weight[i]);
        KK_1d_BD_to_JPSI_K_PI->Fill(JpsiKK_mass[i], weight[i]);
        pipi_1d_BD_to_JPSI_K_PI->Fill(Jpsipipi_mass[i], weight[i]);
        pK_Kp_2d_BD_to_JPSI_K_PI->Fill(JpsipK_mass[i], JpsiKp_mass[i], weight[i]);
        ppi_pip_2d_BD_to_JPSI_K_PI->Fill(Jpsippi_mass[i], Jpsipip_mass[i], weight[i]);
    }
    
    for(int i = 0; i < phs_Bs; i++)
    {
        if (signal_a_BsKK[i]) {
            Jpsipi_2d_BS_to_JPSI_K_K->Fill(Jpsipi1_mass_BsKK[i], Jpsipi2_mass_BsKK[i], weight_BsKK[i]);
            JpsiK_2d_BS_to_JPSI_K_K->Fill(JpsiK1_mass_BsKK[i], JpsiK2_mass_BsKK[i], weight_BsKK[i]);
            Kpi_2d_BS_to_JPSI_K_K->Fill(K1pi2_mass_BsKK[i], K2pi1_mass_BsKK[i], weight_BsKK[i]);
            cos_theta_Zc_BS_to_JPSI_K_K->Fill(cos_theta_Zc_Kpi_Bs[i], cos_theta_Zc_piK_Bs[i], weight_BsKK[i]);
            cos_theta_psi_Kstar_BS_to_JPSI_K_K->Fill(cos_theta_psi_Kstar_Kpi_Bs[i], cos_theta_psi_Kstar_piK_Bs[i], weight_BsKK[i]);
            phi_mu_X_BS_to_JPSI_K_K->Fill(phi_mu_X_Kpi_Bs[i], phi_mu_X_piK_Bs[i], weight_BsKK[i]);
            phi_K_BS_to_JPSI_K_K->Fill(phi_K_Kpi_Bs[i], phi_K_piK_Bs[i], weight_BsKK[i]);
            cos_theta_Kstar_BS_to_JPSI_K_K->Fill(cos_theta_Kstar_Kpi_Bs[i], cos_theta_Kstar_piK_Bs[i], weight_BsKK[i]);
            cos_theta_B0_BS_to_JPSI_K_K->Fill(cos_theta_B0_Kpi_Bs[i], cos_theta_B0_piK_Bs[i], weight_BsKK[i]);
            /*alpha_mu_BS_to_JPSI_K_K->Fill(alpha_mu_Kpi_Bs[i], alpha_mu_piK_Bs[i], weight[i]);
            phi_mu_Kstar_BS_to_JPSI_K_K->Fill(phi_mu_Kstar_Kpi_Bs[i], phi_mu_Kstar_piK_Bs[i], weight[i]);
            
            cos_theta_psi_Kstar_BS_to_JPSI_K_K->Fill(cos_theta_psi_Kstar_Kpi_Bs[i], cos_theta_psi_Kstar_piK_Bs[i], weight[i]);
            cos_theta_psi_X_BS_to_JPSI_K_K->Fill(cos_theta_psi_X_Kpi_Bs[i], cos_theta_psi_X_piK_Bs[i], weight[i]);
            */
            coef_4_to_s_BS_to_JPSI_K_K += weight_BsKK[i];
        }
            
        if (control_a_BsKK[i]) {
            JpsiK_2d_control_BS_to_JPSI_K_K->Fill(JpsiK1_mass_BsKK[i], JpsiK2_mass_BsKK[i], weight_BsKK[i]);
            KK_1d_control_BS_to_JPSI_K_K->Fill(KK_mass_BsKK[i], weight_BsKK[i]);
            coef_4_to_c_BS_to_JPSI_K_K += weight_BsKK[i];
        }
        
        if (controlLb_a_BsKK[i]) {
            JpsiK_2d_controlLb_BS_to_JPSI_K_K->Fill(JpsiK1_mass_BsKK[i], JpsiK2_mass_BsKK[i], weight_BsKK[i]);
            Jpsip_2d_controlLb_BS_to_JPSI_K_K->Fill(Jpsip1_mass_BsKK[i], Jpsip2_mass_BsKK[i], weight_BsKK[i]);
            pK_2d_controlLb_BS_to_JPSI_K_K->Fill(p1K2_mass_BsKK[i], p2K1_mass_BsKK[i], weight_BsKK[i]);
            coef_4_to_clb_BS_to_JPSI_K_K += weight_BsKK[i];
        }
            
        Kpi_piK_2d_BS_to_JPSI_K_K->Fill(JpsiKpi_mass_BsKK[i], JpsipiK_mass_BsKK[i], weight_BsKK[i]);
        KK_1d_BS_to_JPSI_K_K->Fill(JpsiKK_mass_BsKK[i], weight_BsKK[i]);
        pipi_1d_BS_to_JPSI_K_K->Fill(Jpsipipi_mass_BsKK[i], weight_BsKK[i]);
        pK_Kp_2d_BS_to_JPSI_K_K->Fill(JpsipK_mass_BsKK[i], JpsiKp_mass_BsKK[i], weight_BsKK[i]);
        ppi_pip_2d_BS_to_JPSI_K_K->Fill(Jpsippi_mass_BsKK[i], Jpsipip_mass_BsKK[i], weight_BsKK[i]);
    }
    
    /*for(int i = 0; i < phs_Lb - phs_Lb % THREADS; i++)
    {
        if (signal_a_Lb[i]) {
            Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->Fill(Jpsipi1_mass_Lb[i], Jpsipi2_mass_Lb[i], weight_Lb[i]);
            JpsiK_2d_LAMBDA0B_to_JPSI_P_K->Fill(JpsiK1_mass_Lb[i], JpsiK2_mass_Lb[i], weight_Lb[i]);
            Kpi_2d_LAMBDA0B_to_JPSI_P_K->Fill(K1pi2_mass_Lb[i], K2pi1_mass_Lb[i], weight_Lb[i]);
            cos_theta_Zc_LAMBDA0B_to_JPSI_P_K->Fill(cos_theta_Zc_Kpi_Lb[i], cos_theta_Zc_piK_Lb[i], weight_Lb[i]);
            cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K->Fill(cos_theta_psi_Kstar_Kpi_Lb[i], cos_theta_psi_Kstar_piK_Lb[i], weight_Lb[i]);
            phi_K_LAMBDA0B_to_JPSI_P_K->Fill(phi_K_Kpi_Lb[i], phi_K_piK_Lb[i], weight_Lb[i]);
            phi_mu_X_LAMBDA0B_to_JPSI_P_K->Fill(phi_mu_X_Kpi_Lb[i], phi_mu_X_piK_Lb[i], weight_Lb[i]);
            cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->Fill(cos_theta_Kstar_Kpi_Lb[i], cos_theta_Kstar_piK_Lb[i], weight_Lb[i]);
            cos_theta_B0_LAMBDA0B_to_JPSI_P_K->Fill(cos_theta_B0_Kpi_Lb[i], cos_theta_B0_piK_Lb[i], weight_Lb[i]);*/
            /*cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K->Fill(cos_theta_psi_X_Kpi_Lb[i], cos_theta_psi_X_piK_Lb[i], weight[i]);
            phi_mu_X_LAMBDA0B_to_JPSI_P_K->Fill(phi_mu_X_Kpi_Lb[i], phi_mu_X_piK_Lb[i], weight[i]);
            cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->Fill(cos_theta_Kstar_Kpi_Lb[i], cos_theta_Kstar_piK_Lb[i], weight[i]);*/
            
            /*phi_K_LAMBDA0B_to_JPSI_P_K->Fill(phi_K_Kpi_Lb[i], phi_K_piK_Lb[i], weight[i]);
            alpha_mu_LAMBDA0B_to_JPSI_P_K->Fill(alpha_mu_Kpi_Lb[i], alpha_mu_piK_Lb[i], weight[i]);
            phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K->Fill(phi_mu_Kstar_Kpi_Lb[i], phi_mu_Kstar_piK_Lb[i], weight[i]);
            cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->Fill(cos_theta_Kstar_Kpi_Lb[i], cos_theta_Kstar_piK_Lb[i], weight[i]);
            cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K->Fill(cos_theta_psi_Kstar_Kpi_Lb[i], cos_theta_psi_Kstar_piK_Lb[i], weight[i]);
            cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->Fill(cos_theta_Kstar_Kpi_Lb[i], cos_theta_Kstar_piK_Lb[i], weight[i]);
            */ 
            /*coef_4_to_s_LAMBDA0B_to_JPSI_P_K += weight_Lb[i];
        }
        
        if (control_a_Lb[i]) {
            JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K->Fill(JpsiK1_mass_Lb[i], JpsiK2_mass_Lb[i], weight_Lb[i]);
            KK_1d_control_LAMBDA0B_to_JPSI_P_K->Fill(KK_mass_Lb[i], weight_Lb[i]);
            coef_4_to_c_LAMBDA0B_to_JPSI_P_K += weight_Lb[i];
        }  */
        
        /*if (controlLb_a_Lb[i]) {
            JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Fill(JpsiK1_mass_Lb[i], JpsiK2_mass_Lb[i], weight_Lb[i]);
            Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Fill(Jpsip1_mass_Lb[i], Jpsip2_mass_Lb[i], weight_Lb[i]);
            pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Fill(p1K2_mass_Lb[i], p2K1_mass_Lb[i], weight_Lb[i]);
            coef_4_to_clb_LAMBDA0B_to_JPSI_P_K += weight_Lb[i];
        }*/
            
        /*Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Fill(JpsiKpi_mass_Lb[i], JpsipiK_mass_Lb[i], weight_Lb[i]);
        KK_1d_LAMBDA0B_to_JPSI_P_K->Fill(JpsiKK_mass_Lb[i], weight_Lb[i]);
        pipi_1d_LAMBDA0B_to_JPSI_P_K->Fill(Jpsipipi_mass_Lb[i], weight_Lb[i]);
        pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->Fill(JpsipK_mass_Lb[i], JpsiKp_mass_Lb[i], weight_Lb[i]);
    }*/
    
    coef_4_to_s_BD_to_JPSI_K_PI = coef_4_to_s_BD_to_JPSI_K_PI / Kpi_piK_2d_BD_to_JPSI_K_PI->Integral();
    coef_4_to_c_BD_to_JPSI_K_PI = coef_4_to_c_BD_to_JPSI_K_PI / KK_1d_BD_to_JPSI_K_PI->Integral();
    coef_4_to_clb_BD_to_JPSI_K_PI = coef_4_to_clb_BD_to_JPSI_K_PI / pK_Kp_2d_BD_to_JPSI_K_PI->Integral();
    coef_4_to_s_BS_to_JPSI_K_K = coef_4_to_s_BS_to_JPSI_K_K / Kpi_piK_2d_BS_to_JPSI_K_K->Integral();
    coef_4_to_c_BS_to_JPSI_K_K = coef_4_to_c_BS_to_JPSI_K_K / KK_1d_BS_to_JPSI_K_K->Integral();
    coef_4_to_clb_BS_to_JPSI_K_K = coef_4_to_clb_BS_to_JPSI_K_K / pK_Kp_2d_BS_to_JPSI_K_K->Integral();
    //coef_4_to_s_LAMBDA0B_to_JPSI_P_K = coef_4_to_s_LAMBDA0B_to_JPSI_P_K / Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Integral();
    //coef_4_to_c_LAMBDA0B_to_JPSI_P_K = coef_4_to_c_LAMBDA0B_to_JPSI_P_K / KK_1d_LAMBDA0B_to_JPSI_P_K->Integral();
    //coef_4_to_clb_LAMBDA0B_to_JPSI_P_K = coef_4_to_clb_LAMBDA0B_to_JPSI_P_K / pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->Integral();
    
    n_global_BS_to_JPSI_K_K         = par[84];
    n_global_BS_to_JPSI_PI_PI       = par[81];
    n_global_BD_to_JPSI_PI_PI       = par[82];
    n_global_LAMBDA0B_to_JPSI_P_K   = par[80];
    n_global_LAMBDA0B_to_JPSI_P_PI  = par[214];
    n_global_BD_to_JPSI_K_K         = par[215];
    n_global_BS_to_JPSI_K_PI        = par[216];
        
    if (JpsipK_fit_mode || JpsipK_fit_mode_plus_JpsiKpi)
    {
        n_global_BS_to_JPSI_K_K = n_global_BS_to_JPSI_K_K * par[83] / n_global_BD_to_JPSI_K_PI;
        n_global_BS_to_JPSI_PI_PI = n_global_BS_to_JPSI_PI_PI * par[83] / n_global_BD_to_JPSI_K_PI;
        n_global_BD_to_JPSI_PI_PI = n_global_BD_to_JPSI_PI_PI * par[83] / n_global_BD_to_JPSI_K_PI;
        n_global_LAMBDA0B_to_JPSI_P_PI  = n_global_LAMBDA0B_to_JPSI_P_PI * par[83] / n_global_BD_to_JPSI_K_PI;
        n_global_BD_to_JPSI_K_K         = n_global_BD_to_JPSI_K_K * par[83] / n_global_BD_to_JPSI_K_PI;
        n_global_BS_to_JPSI_K_PI        = n_global_BS_to_JPSI_K_PI * par[83] / n_global_BD_to_JPSI_K_PI;
    }
    
    if (Jpsipipi_fit_mode || Jpsipipi_fit_mode_plus_JpsiKpi)
    {
        n_global_BS_to_JPSI_K_K = n_global_BS_to_JPSI_K_K * par[83] / n_global_BD_to_JPSI_K_PI;
        n_global_LAMBDA0B_to_JPSI_P_K = n_global_LAMBDA0B_to_JPSI_P_K * par[83] / n_global_BD_to_JPSI_K_PI;
        n_global_LAMBDA0B_to_JPSI_P_PI  = n_global_LAMBDA0B_to_JPSI_P_PI * par[83] / n_global_BD_to_JPSI_K_PI;
        n_global_BD_to_JPSI_K_K         = n_global_BD_to_JPSI_K_K * par[83] / n_global_BD_to_JPSI_K_PI;
        n_global_BS_to_JPSI_K_PI        = n_global_BS_to_JPSI_K_PI * par[83] / n_global_BD_to_JPSI_K_PI;
    }
    
    if (Jpsipipi_fit_mode_plus_JpsiKpi_lbfix)
    {
        n_global_BS_to_JPSI_K_K = n_global_BS_to_JPSI_K_K * par[83] / n_global_BD_to_JPSI_K_PI;
        n_global_LAMBDA0B_to_JPSI_P_PI  = n_global_LAMBDA0B_to_JPSI_P_PI * par[83] / n_global_BD_to_JPSI_K_PI;
        n_global_BD_to_JPSI_K_K         = n_global_BD_to_JPSI_K_K * par[83] / n_global_BD_to_JPSI_K_PI;
        n_global_BS_to_JPSI_K_PI        = n_global_BS_to_JPSI_K_PI * par[83] / n_global_BD_to_JPSI_K_PI;
        //n_global_LAMBDA0B_to_JPSI_P_K = n_global_LAMBDA0B_to_JPSI_P_K * par[83] / n_global_BD_to_JPSI_K_PI;
    }
    
    if (JpsiKK_fit_mode || JpsiKK_fit_mode_no_control || JpsiKK_fit_mode_plus_JpsiKpi)
    {
        n_global_BS_to_JPSI_PI_PI = n_global_BS_to_JPSI_PI_PI * par[83] / n_global_BD_to_JPSI_K_PI;
        n_global_BD_to_JPSI_PI_PI = n_global_BD_to_JPSI_PI_PI * par[83] / n_global_BD_to_JPSI_K_PI;
        n_global_LAMBDA0B_to_JPSI_P_K = n_global_LAMBDA0B_to_JPSI_P_K * par[83] / n_global_BD_to_JPSI_K_PI;
        n_global_LAMBDA0B_to_JPSI_P_PI  = n_global_LAMBDA0B_to_JPSI_P_PI * par[83] / n_global_BD_to_JPSI_K_PI;
        n_global_BD_to_JPSI_K_K         = n_global_BD_to_JPSI_K_K * par[83] / n_global_BD_to_JPSI_K_PI;
        n_global_BS_to_JPSI_K_PI        = n_global_BS_to_JPSI_K_PI * par[83] / n_global_BD_to_JPSI_K_PI;
    }
    
    if (JpsiKpi_fit_mode || JpsiKpi_fit_mode_no_signal)
    {
        n_global_BS_to_JPSI_PI_PI = n_global_BS_to_JPSI_PI_PI * n_global_BS_to_JPSI_K_K / n_global_BS_to_JPSI_K_K_ini;
        n_global_BD_to_JPSI_PI_PI = n_global_BD_to_JPSI_PI_PI * n_global_BS_to_JPSI_K_K / n_global_BS_to_JPSI_K_K_ini;
        n_global_LAMBDA0B_to_JPSI_P_K = n_global_LAMBDA0B_to_JPSI_P_K * n_global_BS_to_JPSI_K_K / n_global_BS_to_JPSI_K_K_ini;
        n_global_LAMBDA0B_to_JPSI_P_PI  = n_global_LAMBDA0B_to_JPSI_P_PI * n_global_BS_to_JPSI_K_K / n_global_BS_to_JPSI_K_K_ini;
        n_global_BD_to_JPSI_K_K         = n_global_BD_to_JPSI_K_K *n_global_BS_to_JPSI_K_K/ n_global_BS_to_JPSI_K_K_ini;
        n_global_BS_to_JPSI_K_PI        = n_global_BS_to_JPSI_K_PI * n_global_BS_to_JPSI_K_K / n_global_BS_to_JPSI_K_K_ini;
    }
    
    Kpi_piK_2d_BD_to_JPSI_K_PI->Scale(par[83] / Kpi_piK_2d_BD_to_JPSI_K_PI->Integral(1, Kpi_piK_bins, 1, Kpi_piK_bins2));
    Kpi_piK_2d_BS_to_JPSI_K_K->Scale(n_global_BS_to_JPSI_K_K / Kpi_piK_2d_BS_to_JPSI_K_K->Integral(1, Kpi_piK_bins, 1, Kpi_piK_bins2));
    Kpi_piK_2d_BS_to_JPSI_PI_PI->Scale(n_global_BS_to_JPSI_PI_PI / Kpi_piK_2d_BS_to_JPSI_PI_PI->Integral(1, Kpi_piK_bins, 1, Kpi_piK_bins2));
    Kpi_piK_2d_BD_to_JPSI_PI_PI->Scale(n_global_BD_to_JPSI_PI_PI / Kpi_piK_2d_BD_to_JPSI_PI_PI->Integral(1, Kpi_piK_bins, 1, Kpi_piK_bins2));
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Scale(n_global_LAMBDA0B_to_JPSI_P_K / Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Integral(1, Kpi_piK_bins, 1, Kpi_piK_bins2));
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->Scale(n_global_LAMBDA0B_to_JPSI_P_PI / Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->Integral(1, Kpi_piK_bins, 1, Kpi_piK_bins2));
    Kpi_piK_2d_BD_to_JPSI_K_K->Scale(n_global_BD_to_JPSI_K_K / Kpi_piK_2d_BD_to_JPSI_K_K->Integral(1, Kpi_piK_bins, 1, Kpi_piK_bins2));
    Kpi_piK_2d_BS_to_JPSI_K_PI->Scale(n_global_BS_to_JPSI_K_PI / Kpi_piK_2d_BS_to_JPSI_K_PI->Integral(1, Kpi_piK_bins, 1, Kpi_piK_bins2));    
    
    KK_1d_BD_to_JPSI_K_PI->Scale(par[83] / KK_1d_BD_to_JPSI_K_PI->Integral(1, KK_bins2));
    KK_1d_BS_to_JPSI_K_K->Scale(n_global_BS_to_JPSI_K_K / KK_1d_BS_to_JPSI_K_K->Integral(1, KK_bins2));
    KK_1d_BS_to_JPSI_PI_PI->Scale(n_global_BS_to_JPSI_PI_PI / KK_1d_BS_to_JPSI_PI_PI->Integral(1, KK_bins2));
    KK_1d_BD_to_JPSI_PI_PI->Scale(n_global_BD_to_JPSI_PI_PI / KK_1d_BD_to_JPSI_PI_PI->Integral(1, KK_bins2));
    KK_1d_LAMBDA0B_to_JPSI_P_K->Scale(n_global_LAMBDA0B_to_JPSI_P_K / KK_1d_LAMBDA0B_to_JPSI_P_K->Integral(1, KK_bins2));
    KK_1d_LAMBDA0B_to_JPSI_P_PI->Scale(n_global_LAMBDA0B_to_JPSI_P_PI / KK_1d_LAMBDA0B_to_JPSI_P_PI->Integral(1, KK_bins2));
    KK_1d_BD_to_JPSI_K_K->Scale(n_global_BD_to_JPSI_K_K / KK_1d_BD_to_JPSI_K_K->Integral(1, KK_bins2));
    KK_1d_BS_to_JPSI_K_PI->Scale(n_global_BS_to_JPSI_K_PI / KK_1d_BS_to_JPSI_K_PI->Integral(1, KK_bins2));
    
    pipi_1d_BD_to_JPSI_K_PI->Scale(par[83] / pipi_1d_BD_to_JPSI_K_PI->Integral(1, pipi_bins));
    pipi_1d_BS_to_JPSI_K_K->Scale(n_global_BS_to_JPSI_K_K / pipi_1d_BS_to_JPSI_K_K->Integral(1, pipi_bins));
    pipi_1d_BS_to_JPSI_PI_PI->Scale(n_global_BS_to_JPSI_PI_PI / pipi_1d_BS_to_JPSI_PI_PI->Integral(1, pipi_bins));
    pipi_1d_BD_to_JPSI_PI_PI->Scale(n_global_BD_to_JPSI_PI_PI / pipi_1d_BD_to_JPSI_PI_PI->Integral(1, pipi_bins));
    pipi_1d_LAMBDA0B_to_JPSI_P_K->Scale(n_global_LAMBDA0B_to_JPSI_P_K / pipi_1d_LAMBDA0B_to_JPSI_P_K->Integral(1, pipi_bins));
    pipi_1d_LAMBDA0B_to_JPSI_P_PI->Scale(n_global_LAMBDA0B_to_JPSI_P_PI / pipi_1d_LAMBDA0B_to_JPSI_P_PI->Integral(1, pipi_bins));
    pipi_1d_BD_to_JPSI_K_K->Scale(n_global_BD_to_JPSI_K_K / pipi_1d_BD_to_JPSI_K_K->Integral(1, pipi_bins));
    pipi_1d_BS_to_JPSI_K_PI->Scale(n_global_BS_to_JPSI_K_PI / pipi_1d_BS_to_JPSI_K_PI->Integral(1, pipi_bins));
    
    pK_Kp_2d_BD_to_JPSI_K_PI->Scale(par[83] / pK_Kp_2d_BD_to_JPSI_K_PI->Integral(1, pK_Kp_bins, 1, pK_Kp_bins2));
    pK_Kp_2d_BS_to_JPSI_K_K->Scale(n_global_BS_to_JPSI_K_K / pK_Kp_2d_BS_to_JPSI_K_K->Integral(1, pK_Kp_bins, 1, pK_Kp_bins2));
    pK_Kp_2d_BS_to_JPSI_PI_PI->Scale(n_global_BS_to_JPSI_PI_PI / pK_Kp_2d_BS_to_JPSI_PI_PI->Integral(1, pK_Kp_bins, 1, pK_Kp_bins2));
    pK_Kp_2d_BD_to_JPSI_PI_PI->Scale(n_global_BD_to_JPSI_PI_PI / pK_Kp_2d_BD_to_JPSI_PI_PI->Integral(1, pK_Kp_bins, 1, pK_Kp_bins2));
    pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->Scale(n_global_LAMBDA0B_to_JPSI_P_K / pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->Integral(1, pK_Kp_bins, 1, pK_Kp_bins2));
    pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI->Scale(n_global_LAMBDA0B_to_JPSI_P_PI / pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI->Integral(1, pK_Kp_bins, 1, pK_Kp_bins2));
    pK_Kp_2d_BD_to_JPSI_K_K->Scale(n_global_BD_to_JPSI_K_K / pK_Kp_2d_BD_to_JPSI_K_K->Integral(1, pK_Kp_bins, 1, pK_Kp_bins2));
    pK_Kp_2d_BS_to_JPSI_K_PI->Scale(n_global_BS_to_JPSI_K_PI / pK_Kp_2d_BS_to_JPSI_K_PI->Integral(1, pK_Kp_bins, 1, pK_Kp_bins2));
    
    ppi_pip_2d_BD_to_JPSI_K_PI->Scale(par[83] / ppi_pip_2d_BD_to_JPSI_K_PI->Integral(1, ppi_pip_bins, 1, ppi_pip_bins2));
    ppi_pip_2d_BS_to_JPSI_K_K->Scale(n_global_BS_to_JPSI_K_K / ppi_pip_2d_BS_to_JPSI_K_K->Integral(1, ppi_pip_bins, 1, ppi_pip_bins2));
    ppi_pip_2d_BS_to_JPSI_PI_PI->Scale(n_global_BS_to_JPSI_PI_PI / ppi_pip_2d_BS_to_JPSI_PI_PI->Integral(1, ppi_pip_bins, 1, ppi_pip_bins2));
    ppi_pip_2d_BD_to_JPSI_PI_PI->Scale(n_global_BD_to_JPSI_PI_PI / ppi_pip_2d_BD_to_JPSI_PI_PI->Integral(1, ppi_pip_bins, 1, ppi_pip_bins2));
    ppi_pip_2d_LAMBDA0B_to_JPSI_P_K->Scale(n_global_LAMBDA0B_to_JPSI_P_K / ppi_pip_2d_LAMBDA0B_to_JPSI_P_K->Integral(1, ppi_pip_bins, 1, ppi_pip_bins2));
    ppi_pip_2d_LAMBDA0B_to_JPSI_P_PI->Scale(n_global_LAMBDA0B_to_JPSI_P_PI / ppi_pip_2d_LAMBDA0B_to_JPSI_P_PI->Integral(1, ppi_pip_bins, 1, ppi_pip_bins2));
    ppi_pip_2d_BD_to_JPSI_K_K->Scale(n_global_BD_to_JPSI_K_K / ppi_pip_2d_BD_to_JPSI_K_K->Integral(1, ppi_pip_bins, 1, ppi_pip_bins2));
    ppi_pip_2d_BS_to_JPSI_K_PI->Scale(n_global_BS_to_JPSI_K_PI / ppi_pip_2d_BS_to_JPSI_K_PI->Integral(1, ppi_pip_bins, 1, ppi_pip_bins2));
    
    //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__
    
    n_BD_to_JPSI_K_PI = coef_4_to_s_BD_to_JPSI_K_PI * par[83];
    n_BS_to_JPSI_K_K = coef_4_to_s_BS_to_JPSI_K_K * n_global_BS_to_JPSI_K_K;
    n_BS_to_JPSI_PI_PI = coef_4_to_s_BS_to_JPSI_PI_PI * n_global_BS_to_JPSI_PI_PI;
    n_BD_to_JPSI_PI_PI = coef_4_to_s_BD_to_JPSI_PI_PI * n_global_BD_to_JPSI_PI_PI;
    n_LAMBDA0B_to_JPSI_P_K = coef_4_to_s_LAMBDA0B_to_JPSI_P_K * n_global_LAMBDA0B_to_JPSI_P_K;
    n_LAMBDA0B_to_JPSI_P_PI = coef_4_to_s_LAMBDA0B_to_JPSI_P_PI * n_global_LAMBDA0B_to_JPSI_P_PI;
    n_BD_to_JPSI_K_K = coef_4_to_s_BD_to_JPSI_K_K * n_global_BD_to_JPSI_K_K;
    n_BS_to_JPSI_K_PI = coef_4_to_s_BS_to_JPSI_K_PI * n_global_BS_to_JPSI_K_PI;
    
    n_control_BD_to_JPSI_K_PI = coef_4_to_c_BD_to_JPSI_K_PI * par[83];
    n_control_BS_to_JPSI_K_K = coef_4_to_c_BS_to_JPSI_K_K * n_global_BS_to_JPSI_K_K;
    n_control_BS_to_JPSI_PI_PI = coef_4_to_c_BS_to_JPSI_PI_PI * n_global_BS_to_JPSI_PI_PI;
    n_control_BD_to_JPSI_PI_PI = coef_4_to_c_BD_to_JPSI_PI_PI * n_global_BD_to_JPSI_PI_PI;
    n_control_LAMBDA0B_to_JPSI_P_K = coef_4_to_c_LAMBDA0B_to_JPSI_P_K * n_global_LAMBDA0B_to_JPSI_P_K;
    n_control_LAMBDA0B_to_JPSI_P_PI = coef_4_to_c_LAMBDA0B_to_JPSI_P_PI * n_global_LAMBDA0B_to_JPSI_P_PI;
    n_control_BD_to_JPSI_K_K = coef_4_to_c_BD_to_JPSI_K_K * n_global_BD_to_JPSI_K_K;
    n_control_BS_to_JPSI_K_PI = coef_4_to_c_BS_to_JPSI_K_PI * n_global_BS_to_JPSI_K_PI;
    
    n_controlLb_BD_to_JPSI_K_PI = coef_4_to_clb_BD_to_JPSI_K_PI * par[83];
    n_controlLb_BS_to_JPSI_K_K = coef_4_to_clb_BS_to_JPSI_K_K * n_global_BS_to_JPSI_K_K;
    n_controlLb_BS_to_JPSI_PI_PI = coef_4_to_clb_BS_to_JPSI_PI_PI * n_global_BS_to_JPSI_PI_PI;
    n_controlLb_BD_to_JPSI_PI_PI = coef_4_to_clb_BD_to_JPSI_PI_PI * n_global_BD_to_JPSI_PI_PI;
    n_controlLb_LAMBDA0B_to_JPSI_P_K = coef_4_to_clb_LAMBDA0B_to_JPSI_P_K * n_global_LAMBDA0B_to_JPSI_P_K;
    n_controlLb_LAMBDA0B_to_JPSI_P_PI = coef_4_to_clb_LAMBDA0B_to_JPSI_P_PI * n_global_LAMBDA0B_to_JPSI_P_PI;
    n_controlLb_BD_to_JPSI_K_K = coef_4_to_clb_BD_to_JPSI_K_K * n_global_BD_to_JPSI_K_K;
    n_controlLb_BS_to_JPSI_K_PI = coef_4_to_clb_BS_to_JPSI_K_PI * n_global_BS_to_JPSI_K_PI;
    
    n_comb_bg = 0.;
    
    float st = 1. / sqrt(2.);
    
    float xmin_Kpi = 4.900;
    float xmax_Kpi = 5.900;
    float xmin_piK = 4.650;
    float xmax_piK = 5.900;
    float xmin_KK = 4.97;
    float xmin_pipi = 4.55;
    float xmin_pK = 7.4;
    float xmin_Kp = 5.15;
    
    for(int i = 1; i <= Kpi_piK_2d_bg->ProjectionX()->GetNbinsX(); i++)
    {
        for(int j = 1; j <= Kpi_piK_2d_bg->ProjectionY()->GetNbinsX(); j++)
        {
            
            double xx0 = Kpi_piK_2d_data->ProjectionX()->GetBinCenter(i); 
            double yy0 = Kpi_piK_2d_data->ProjectionY()->GetBinCenter(j);
            double x = st * xx0 + st * yy0 - 6.6;
            double y = -st * xx0 + st * yy0;
            Kpi_piK_2d_bg->SetBinContent(i, j, exp(par[85] + x * par[86] + pow(x, 2.) * par[87] + pow(x, 3.) * par[88]) * exp(-1. * pow( (y + par[89]) / (par[90] + x * par[91] + pow(x, 2.) * par[92]), 2.)));            
            if (sa_a[i-1][j-1] == 1)
            {
                n_comb_bg += Kpi_piK_2d_bg->GetBinContent(i, j);
            }
        }
    }
    Double_t Kpi_piK_norm_bg = par[213] / Kpi_piK_2d_bg->Integral();
    n_comb_bg = n_comb_bg * Kpi_piK_norm_bg;
    Kpi_piK_2d_bg->Scale(Kpi_piK_norm_bg);
    
    n_controlLb_comb_bg = 0.;
    
    for(int i = 1; i <= pK_Kp_2d_bg->ProjectionX()->GetNbinsX(); i++)
    {
        for(int j = 1; j <= pK_Kp_2d_bg->ProjectionY()->GetNbinsX(); j++)
        {
            
            double xx0 = pK_Kp_2d_data->ProjectionX()->GetBinCenter(i); 
            double yy0 = pK_Kp_2d_data->ProjectionY()->GetBinCenter(j);
            double x = st * xx0 + st * yy0 - par[120];
            double y = -st * xx0 + st * yy0;
            pK_Kp_2d_bg->SetBinContent(i, j, pow(abs(x - par[110]), par[111]) * exp(par[112] + x * par[113] + pow(x, 2.) * par[114] + pow(x, 3.) * par[115]) * exp(-1. * pow( (y + par[116]) / (par[117] + x * par[118] + pow(x, 2.) * par[119]), 2.)));
            if (calb_a[i-1][j-1] == 1)
            {
                n_controlLb_comb_bg += pK_Kp_2d_bg->GetBinContent(i, j);
            }
        }
    }
    Double_t pK_Kp_norm_bg = (par[213] + pK_Kp_2d_data->Integral() - Kpi_piK_2d_data->Integral())  / pK_Kp_2d_bg->Integral();
    pK_Kp_2d_bg->Scale(pK_Kp_norm_bg);
    n_controlLb_comb_bg = n_controlLb_comb_bg * pK_Kp_norm_bg;
    
    for(int i = 1; i <= ppi_pip_2d_bg->ProjectionX()->GetNbinsX(); i++)
    {
        for(int j = 1; j <= ppi_pip_2d_bg->ProjectionY()->GetNbinsX(); j++)
        {
            
            double xx0 = ppi_pip_2d_data->ProjectionX()->GetBinCenter(i); 
            double yy0 = ppi_pip_2d_data->ProjectionY()->GetBinCenter(j);
            double x = st * xx0 + st * yy0 - par[229];
            double y = -st * xx0 + st * yy0;
            ppi_pip_2d_bg->SetBinContent(i, j, pow(abs(x - par[219]), par[220]) * exp(par[221] + x * par[222] + pow(x, 2.) * par[223] + pow(x, 3.) * par[224]) * exp(-1. * pow( (y + par[225]) / (par[226] + x * par[227] + pow(x, 2.) * par[228]), 2.)));
        }
    }
    Double_t ppi_pip_norm_bg = (par[213] + ppi_pip_2d_data->Integral() - Kpi_piK_2d_data->Integral())  / ppi_pip_2d_bg->Integral();
    ppi_pip_2d_bg->Scale(ppi_pip_norm_bg);
    
    for(int i = 1; i <= pipi_1d_bg->GetNbinsX(); i++)
    {
        double xx0 = pipi_1d_data->GetBinCenter(i); 
        double x = xx0 - xmin_pipi;
        pipi_1d_bg->SetBinContent(i, pow(abs(xx0 - par[101]), par[102]) * exp(par[103] + x * par[104] + pow(x, 2.) * par[105] + pow(x, 3.) * par[106] + pow(x, 4.) * par[107] + pow(x, 5.) * par[108] + pow(x, 6.) * par[109]));            
    }
    pipi_1d_bg->Scale(par[213] / pipi_1d_bg->Integral());
    
    
    float xmin_KK_o = 4.970;
    
    for(int i = 1; i <= KK_1d_data->GetNbinsX(); i++)
    {
        double xx0 = KK_1d_data->GetBinCenter(i); 
        double x = xx0 - xmin_KK;
        double nb = 0.0; double nberr = 0.0; double fb = 0.0;
        KK_1d_bg->SetBinContent(i, pow(abs(xx0 - par[93]), par[94]) * exp(par[95] + x * par[96] + pow(x, 2.) * par[97] + pow(x, 3.) * par[98] + pow(x, 4.) * par[99] + pow(x, 5.) * par[100]));
        //cout << i << "\t" << pow(xx0 - x0_KK, d0_KK) * exp(p0_KK + x * p1_KK + pow(x, 2.) * p2_KK + pow(x, 3.) * p3_KK) << endl;
    }
    
    Double_t KK_norm_bg = par[213] / KK_1d_bg->Integral();
    KK_1d_bg->Scale(KK_norm_bg);
    
    bg_func->SetParameter(0, par[93]);
    bg_func->SetParameter(1, par[94]);
    bg_func->SetParameter(2, par[95]);
    bg_func->SetParameter(3, par[96]);
    bg_func->SetParameter(4, par[97]);
    bg_func->SetParameter(5, par[98]);
    bg_func->SetParameter(6, par[99]);
    bg_func->SetParameter(7, par[100]);
    bg_func->SetParameter(8, xmin_KK_o);
    
    float b_size = KK_1d_bg->GetBinCenter(11) - KK_1d_bg->GetBinCenter(10);
    
    float xx_n = KK_1d_bg->GetBinCenter(20);
    
    float x_n = xx_n - xmin_KK_o;
    float norm = pow(abs(xx_n - par[93]), par[94]) * exp(par[95] + x_n * par[96] + pow(x_n, 2.) * par[97] + pow(x_n, 3.) * par[98] + pow(x_n, 4.) * par[99] + pow(x_n, 5.) * par[100]) / bg_func->Integral(xx_n - b_size / 2., xx_n + b_size / 2.);
    
    n_control_comb_bg = bg_func->Integral(5.336, 5.396) * norm;
    n_control_comb_bg = n_control_comb_bg * KK_norm_bg;
    
    //cout << n_comb_bg << endl;
    //cout << n_control_comb_bg << endl;
    
    //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__ 
    
    //cout << Jpsipi_2d_BD_to_JPSI_K_PI->Integral() << endl;
    //cout << n_BD_to_JPSI_K_PI << endl;
    
    Jpsipi_2d_BD_to_JPSI_K_PI->Scale(1. / Jpsipi_2d_BD_to_JPSI_K_PI->Integral(1, Jpsipi_2d_nbinsX, 1, Jpsipi_2d_nbinsY) * n_BD_to_JPSI_K_PI);
    Kpi_2d_BD_to_JPSI_K_PI->Scale(1. / Kpi_2d_BD_to_JPSI_K_PI->Integral(1, Kpi_2d_nbinsX, 1, Kpi_2d_nbinsY) * n_BD_to_JPSI_K_PI);
    JpsiK_2d_BD_to_JPSI_K_PI->Scale(1. / JpsiK_2d_BD_to_JPSI_K_PI->Integral(1, JpsiK_2d_nbinsX, 1, JpsiK_2d_nbinsY) * n_BD_to_JPSI_K_PI);
    cos_theta_Zc_BD_to_JPSI_K_PI->Scale(1. / cos_theta_Zc_BD_to_JPSI_K_PI->Integral() * n_BD_to_JPSI_K_PI);
    phi_K_BD_to_JPSI_K_PI->Scale(1. / phi_K_BD_to_JPSI_K_PI->Integral() * n_BD_to_JPSI_K_PI);
    alpha_mu_BD_to_JPSI_K_PI->Scale(1. / alpha_mu_BD_to_JPSI_K_PI->Integral() * n_BD_to_JPSI_K_PI);
    phi_mu_Kstar_BD_to_JPSI_K_PI->Scale(1. / phi_mu_Kstar_BD_to_JPSI_K_PI->Integral() * n_BD_to_JPSI_K_PI);
    phi_mu_X_BD_to_JPSI_K_PI->Scale(1. / phi_mu_X_BD_to_JPSI_K_PI->Integral() * n_BD_to_JPSI_K_PI);
    cos_theta_psi_X_BD_to_JPSI_K_PI->Scale(1. / cos_theta_psi_X_BD_to_JPSI_K_PI->Integral() * n_BD_to_JPSI_K_PI);
    cos_theta_Kstar_BD_to_JPSI_K_PI->Scale(1. / cos_theta_Kstar_BD_to_JPSI_K_PI->Integral() * n_BD_to_JPSI_K_PI);
    cos_theta_psi_Kstar_BD_to_JPSI_K_PI->Scale(1. / cos_theta_psi_Kstar_BD_to_JPSI_K_PI->Integral() * n_BD_to_JPSI_K_PI);
    cos_theta_B0_BD_to_JPSI_K_PI->Scale(1. / cos_theta_B0_BD_to_JPSI_K_PI->Integral() * n_BD_to_JPSI_K_PI);
    
    Jpsipi_2d_BS_to_JPSI_PI_PI->Scale(1. / Jpsipi_2d_BS_to_JPSI_PI_PI->Integral(1, Jpsipi_2d_nbinsX, 1, Jpsipi_2d_nbinsY) * n_BS_to_JPSI_PI_PI);
    Kpi_2d_BS_to_JPSI_PI_PI->Scale(1. / Kpi_2d_BS_to_JPSI_PI_PI->Integral(1, Kpi_2d_nbinsX, 1, Kpi_2d_nbinsY) * n_BS_to_JPSI_PI_PI);
    JpsiK_2d_BS_to_JPSI_PI_PI->Scale(1. / JpsiK_2d_BS_to_JPSI_PI_PI->Integral(1, JpsiK_2d_nbinsX, 1, JpsiK_2d_nbinsY) * n_BS_to_JPSI_PI_PI);
    cos_theta_Zc_BS_to_JPSI_PI_PI->Scale(1. / cos_theta_Zc_BS_to_JPSI_PI_PI->Integral() * n_BS_to_JPSI_PI_PI);
    phi_K_BS_to_JPSI_PI_PI->Scale(1. / phi_K_BS_to_JPSI_PI_PI->Integral() * n_BS_to_JPSI_PI_PI);
    alpha_mu_BS_to_JPSI_PI_PI->Scale(1. / alpha_mu_BS_to_JPSI_PI_PI->Integral() * n_BS_to_JPSI_PI_PI);
    phi_mu_Kstar_BS_to_JPSI_PI_PI->Scale(1. / phi_mu_Kstar_BS_to_JPSI_PI_PI->Integral() * n_BS_to_JPSI_PI_PI);
    phi_mu_X_BS_to_JPSI_PI_PI->Scale(1. / phi_mu_X_BS_to_JPSI_PI_PI->Integral() * n_BS_to_JPSI_PI_PI);
    cos_theta_psi_X_BS_to_JPSI_PI_PI->Scale(1. / cos_theta_psi_X_BS_to_JPSI_PI_PI->Integral() * n_BS_to_JPSI_PI_PI);
    cos_theta_Kstar_BS_to_JPSI_PI_PI->Scale(1. / cos_theta_Kstar_BS_to_JPSI_PI_PI->Integral() * n_BS_to_JPSI_PI_PI);
    cos_theta_psi_Kstar_BS_to_JPSI_PI_PI->Scale(1. / cos_theta_psi_Kstar_BS_to_JPSI_PI_PI->Integral() * n_BS_to_JPSI_PI_PI);
    cos_theta_B0_BS_to_JPSI_PI_PI->Scale(1. / cos_theta_B0_BS_to_JPSI_PI_PI->Integral() * n_BS_to_JPSI_PI_PI);
    
    Jpsipi_2d_BD_to_JPSI_PI_PI->Scale(1. / Jpsipi_2d_BD_to_JPSI_PI_PI->Integral() * n_BD_to_JPSI_PI_PI);
    Kpi_2d_BD_to_JPSI_PI_PI->Scale(1. / Kpi_2d_BD_to_JPSI_PI_PI->Integral() * n_BD_to_JPSI_PI_PI);
    JpsiK_2d_BD_to_JPSI_PI_PI->Scale(1. / JpsiK_2d_BD_to_JPSI_PI_PI->Integral(1, JpsiK_2d_nbinsX, 1, JpsiK_2d_nbinsY) * n_BD_to_JPSI_PI_PI);
    cos_theta_Zc_BD_to_JPSI_PI_PI->Scale(1. / cos_theta_Zc_BD_to_JPSI_PI_PI->Integral() * n_BD_to_JPSI_PI_PI);
    phi_K_BD_to_JPSI_PI_PI->Scale(1. / phi_K_BD_to_JPSI_PI_PI->Integral() * n_BD_to_JPSI_PI_PI);
    alpha_mu_BD_to_JPSI_PI_PI->Scale(1. / alpha_mu_BD_to_JPSI_PI_PI->Integral() * n_BD_to_JPSI_PI_PI);
    phi_mu_Kstar_BD_to_JPSI_PI_PI->Scale(1. / phi_mu_Kstar_BD_to_JPSI_PI_PI->Integral() * n_BD_to_JPSI_PI_PI);
    phi_mu_X_BD_to_JPSI_PI_PI->Scale(1. / phi_mu_X_BD_to_JPSI_PI_PI->Integral() * n_BD_to_JPSI_PI_PI);
    cos_theta_psi_X_BD_to_JPSI_PI_PI->Scale(1. / cos_theta_psi_X_BD_to_JPSI_PI_PI->Integral() * n_BD_to_JPSI_PI_PI);
    cos_theta_Kstar_BD_to_JPSI_PI_PI->Scale(1. / cos_theta_Kstar_BD_to_JPSI_PI_PI->Integral() * n_BD_to_JPSI_PI_PI);
    cos_theta_psi_Kstar_BD_to_JPSI_PI_PI->Scale(1. / cos_theta_psi_Kstar_BD_to_JPSI_PI_PI->Integral() * n_BD_to_JPSI_PI_PI);
    cos_theta_B0_BD_to_JPSI_PI_PI->Scale(1. / cos_theta_B0_BD_to_JPSI_PI_PI->Integral() * n_BD_to_JPSI_PI_PI);
    
    Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->Scale(1. / Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->Integral(1, Jpsipi_2d_nbinsX, 1, Jpsipi_2d_nbinsY) * n_LAMBDA0B_to_JPSI_P_K);
    Kpi_2d_LAMBDA0B_to_JPSI_P_K->Scale(1. / Kpi_2d_LAMBDA0B_to_JPSI_P_K->Integral(1, Kpi_2d_nbinsX, 1, Kpi_2d_nbinsY) * n_LAMBDA0B_to_JPSI_P_K);
    JpsiK_2d_LAMBDA0B_to_JPSI_P_K->Scale(1. / JpsiK_2d_LAMBDA0B_to_JPSI_P_K->Integral(1, JpsiK_2d_nbinsX, 1, JpsiK_2d_nbinsY) * n_LAMBDA0B_to_JPSI_P_K);
    cos_theta_Zc_LAMBDA0B_to_JPSI_P_K->Scale(1. / cos_theta_Zc_LAMBDA0B_to_JPSI_P_K->Integral() * n_LAMBDA0B_to_JPSI_P_K);
    phi_K_LAMBDA0B_to_JPSI_P_K->Scale(1. / phi_K_LAMBDA0B_to_JPSI_P_K->Integral() * n_LAMBDA0B_to_JPSI_P_K);
    alpha_mu_LAMBDA0B_to_JPSI_P_K->Scale(1. / alpha_mu_LAMBDA0B_to_JPSI_P_K->Integral() * n_LAMBDA0B_to_JPSI_P_K);
    phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K->Scale(1. / phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K->Integral() * n_LAMBDA0B_to_JPSI_P_K);
    phi_mu_X_LAMBDA0B_to_JPSI_P_K->Scale(1. / phi_mu_X_LAMBDA0B_to_JPSI_P_K->Integral() * n_LAMBDA0B_to_JPSI_P_K);
    cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K->Scale(1. / cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K->Integral() * n_LAMBDA0B_to_JPSI_P_K);
    cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->Scale(1. / cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->Integral() * n_LAMBDA0B_to_JPSI_P_K);
    cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K->Scale(1. / cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K->Integral() * n_LAMBDA0B_to_JPSI_P_K);
    cos_theta_B0_LAMBDA0B_to_JPSI_P_K->Scale(1. / cos_theta_B0_LAMBDA0B_to_JPSI_P_K->Integral() * n_LAMBDA0B_to_JPSI_P_K);
    
    Jpsipi_2d_BS_to_JPSI_K_K->Scale(1. / Jpsipi_2d_BS_to_JPSI_K_K->Integral(1, Jpsipi_2d_nbinsX, 1, Jpsipi_2d_nbinsY) * n_BS_to_JPSI_K_K);
    Kpi_2d_BS_to_JPSI_K_K->Scale(1. / Kpi_2d_BS_to_JPSI_K_K->Integral(1, Kpi_2d_nbinsX, 1, Kpi_2d_nbinsY) * n_BS_to_JPSI_K_K);
    JpsiK_2d_BS_to_JPSI_K_K->Scale(1. / JpsiK_2d_BS_to_JPSI_K_K->Integral(1, JpsiK_2d_nbinsX, 1, JpsiK_2d_nbinsY) * n_BS_to_JPSI_K_K);
    cos_theta_Zc_BS_to_JPSI_K_K->Scale(1. / cos_theta_Zc_BS_to_JPSI_K_K->Integral() * n_BS_to_JPSI_K_K);
    phi_K_BS_to_JPSI_K_K->Scale(1. / phi_K_BS_to_JPSI_K_K->Integral() * n_BS_to_JPSI_K_K);
    alpha_mu_BS_to_JPSI_K_K->Scale(1. / alpha_mu_BS_to_JPSI_K_K->Integral() * n_BS_to_JPSI_K_K);
    phi_mu_Kstar_BS_to_JPSI_K_K->Scale(1. / phi_mu_Kstar_BS_to_JPSI_K_K->Integral() * n_BS_to_JPSI_K_K);
    phi_mu_X_BS_to_JPSI_K_K->Scale(1. / phi_mu_X_BS_to_JPSI_K_K->Integral() * n_BS_to_JPSI_K_K);
    cos_theta_psi_X_BS_to_JPSI_K_K->Scale(1. / cos_theta_psi_X_BS_to_JPSI_K_K->Integral() * n_BS_to_JPSI_K_K);
    cos_theta_Kstar_BS_to_JPSI_K_K->Scale(1. / cos_theta_Kstar_BS_to_JPSI_K_K->Integral() * n_BS_to_JPSI_K_K);
    cos_theta_psi_Kstar_BS_to_JPSI_K_K->Scale(1. / cos_theta_psi_Kstar_BS_to_JPSI_K_K->Integral() * n_BS_to_JPSI_K_K);
    cos_theta_B0_BS_to_JPSI_K_K->Scale(1. / cos_theta_B0_BS_to_JPSI_K_K->Integral() * n_BS_to_JPSI_K_K);
    
    Jpsipi_2d_BD_to_JPSI_K_K->Scale(1. / Jpsipi_2d_BD_to_JPSI_K_K->Integral(1, Jpsipi_2d_nbinsX, 1, Jpsipi_2d_nbinsY) * n_BD_to_JPSI_K_K);
    Kpi_2d_BD_to_JPSI_K_K->Scale(1. / Kpi_2d_BD_to_JPSI_K_K->Integral(1, Kpi_2d_nbinsX, 1, Kpi_2d_nbinsY) * n_BD_to_JPSI_K_K);
    JpsiK_2d_BD_to_JPSI_K_K->Scale(1. / JpsiK_2d_BD_to_JPSI_K_K->Integral(1, JpsiK_2d_nbinsX, 1, JpsiK_2d_nbinsY) * n_BD_to_JPSI_K_K);
    cos_theta_Zc_BD_to_JPSI_K_K->Scale(1. / cos_theta_Zc_BD_to_JPSI_K_K->Integral() * n_BD_to_JPSI_K_K);
    phi_K_BD_to_JPSI_K_K->Scale(1. / phi_K_BD_to_JPSI_K_K->Integral() * n_BD_to_JPSI_K_K);
    alpha_mu_BD_to_JPSI_K_K->Scale(1. / alpha_mu_BD_to_JPSI_K_K->Integral() * n_BD_to_JPSI_K_K);
    phi_mu_Kstar_BD_to_JPSI_K_K->Scale(1. / phi_mu_Kstar_BD_to_JPSI_K_K->Integral() * n_BD_to_JPSI_K_K);
    phi_mu_X_BD_to_JPSI_K_K->Scale(1. / phi_mu_X_BD_to_JPSI_K_K->Integral() * n_BD_to_JPSI_K_K);
    cos_theta_psi_X_BD_to_JPSI_K_K->Scale(1. / cos_theta_psi_X_BD_to_JPSI_K_K->Integral() * n_BD_to_JPSI_K_K);
    cos_theta_Kstar_BD_to_JPSI_K_K->Scale(1. / cos_theta_Kstar_BD_to_JPSI_K_K->Integral() * n_BD_to_JPSI_K_K);
    cos_theta_psi_Kstar_BD_to_JPSI_K_K->Scale(1. / cos_theta_psi_Kstar_BD_to_JPSI_K_K->Integral() * n_BD_to_JPSI_K_K);
    cos_theta_B0_BD_to_JPSI_K_K->Scale(1. / cos_theta_B0_BD_to_JPSI_K_K->Integral() * n_BD_to_JPSI_K_K);
    
    Jpsipi_2d_BS_to_JPSI_K_PI->Scale(1. / Jpsipi_2d_BS_to_JPSI_K_PI->Integral(1, Jpsipi_2d_nbinsX, 1, Jpsipi_2d_nbinsY) * n_BS_to_JPSI_K_PI);
    Kpi_2d_BS_to_JPSI_K_PI->Scale(1. / Kpi_2d_BS_to_JPSI_K_PI->Integral(1, Kpi_2d_nbinsX, 1, Kpi_2d_nbinsY) * n_BS_to_JPSI_K_PI);
    JpsiK_2d_BS_to_JPSI_K_PI->Scale(1. / JpsiK_2d_BS_to_JPSI_K_PI->Integral(1, JpsiK_2d_nbinsX, 1, JpsiK_2d_nbinsY) * n_BS_to_JPSI_K_PI);
    cos_theta_Zc_BS_to_JPSI_K_PI->Scale(1. / cos_theta_Zc_BS_to_JPSI_K_PI->Integral() * n_BS_to_JPSI_K_PI);
    phi_K_BS_to_JPSI_K_PI->Scale(1. / phi_K_BS_to_JPSI_K_PI->Integral() * n_BS_to_JPSI_K_PI);
    alpha_mu_BS_to_JPSI_K_PI->Scale(1. / alpha_mu_BS_to_JPSI_K_PI->Integral() * n_BS_to_JPSI_K_PI);
    phi_mu_Kstar_BS_to_JPSI_K_PI->Scale(1. / phi_mu_Kstar_BS_to_JPSI_K_PI->Integral() * n_BS_to_JPSI_K_PI);
    phi_mu_X_BS_to_JPSI_K_PI->Scale(1. / phi_mu_X_BS_to_JPSI_K_PI->Integral() * n_BS_to_JPSI_K_PI);
    cos_theta_psi_X_BS_to_JPSI_K_PI->Scale(1. / cos_theta_psi_X_BS_to_JPSI_K_PI->Integral() * n_BS_to_JPSI_K_PI);
    cos_theta_Kstar_BS_to_JPSI_K_PI->Scale(1. / cos_theta_Kstar_BS_to_JPSI_K_PI->Integral() * n_BS_to_JPSI_K_PI);
    cos_theta_psi_Kstar_BS_to_JPSI_K_PI->Scale(1. / cos_theta_psi_Kstar_BS_to_JPSI_K_PI->Integral() * n_BS_to_JPSI_K_PI);
    cos_theta_B0_BS_to_JPSI_K_PI->Scale(1. / cos_theta_B0_BS_to_JPSI_K_PI->Integral() * n_BS_to_JPSI_K_PI);
    
    Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->Scale(1. / Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->Integral(1, Jpsipi_2d_nbinsX, 1, Jpsipi_2d_nbinsY) * n_LAMBDA0B_to_JPSI_P_PI);
    Kpi_2d_LAMBDA0B_to_JPSI_P_PI->Scale(1. / Kpi_2d_LAMBDA0B_to_JPSI_P_PI->Integral(1, Kpi_2d_nbinsX, 1, Kpi_2d_nbinsY) * n_LAMBDA0B_to_JPSI_P_PI);
    JpsiK_2d_LAMBDA0B_to_JPSI_P_PI->Scale(1. / JpsiK_2d_LAMBDA0B_to_JPSI_P_PI->Integral(1, JpsiK_2d_nbinsX, 1, JpsiK_2d_nbinsY) * n_LAMBDA0B_to_JPSI_P_PI);
    cos_theta_Zc_LAMBDA0B_to_JPSI_P_PI->Scale(1. / cos_theta_Zc_LAMBDA0B_to_JPSI_P_PI->Integral() * n_LAMBDA0B_to_JPSI_P_PI);
    phi_K_LAMBDA0B_to_JPSI_P_PI->Scale(1. / phi_K_LAMBDA0B_to_JPSI_P_PI->Integral() * n_LAMBDA0B_to_JPSI_P_PI);
    alpha_mu_LAMBDA0B_to_JPSI_P_PI->Scale(1. / alpha_mu_LAMBDA0B_to_JPSI_P_PI->Integral() * n_LAMBDA0B_to_JPSI_P_PI);
    phi_mu_Kstar_LAMBDA0B_to_JPSI_P_PI->Scale(1. / phi_mu_Kstar_LAMBDA0B_to_JPSI_P_PI->Integral() * n_LAMBDA0B_to_JPSI_P_PI);
    phi_mu_X_LAMBDA0B_to_JPSI_P_PI->Scale(1. / phi_mu_X_LAMBDA0B_to_JPSI_P_PI->Integral() * n_LAMBDA0B_to_JPSI_P_PI);
    cos_theta_psi_X_LAMBDA0B_to_JPSI_P_PI->Scale(1. / cos_theta_psi_X_LAMBDA0B_to_JPSI_P_PI->Integral() * n_LAMBDA0B_to_JPSI_P_PI);
    cos_theta_Kstar_LAMBDA0B_to_JPSI_P_PI->Scale(1. / cos_theta_Kstar_LAMBDA0B_to_JPSI_P_PI->Integral() * n_LAMBDA0B_to_JPSI_P_PI);
    cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_PI->Scale(1. / cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_PI->Integral() * n_LAMBDA0B_to_JPSI_P_PI);
    cos_theta_B0_LAMBDA0B_to_JPSI_P_PI->Scale(1. / cos_theta_B0_LAMBDA0B_to_JPSI_P_PI->Integral() * n_LAMBDA0B_to_JPSI_P_PI);
    
    Jpsipi_2d_bg->Scale(1. / Jpsipi_2d_bg->Integral(1, Jpsipi_2d_nbinsX, 1, Jpsipi_2d_nbinsY) * n_comb_bg);
    Kpi_2d_bg->Scale(1. / Kpi_2d_bg->Integral(1, Kpi_2d_nbinsX, 1, Kpi_2d_nbinsY) * n_comb_bg);
    JpsiK_2d_bg->Scale(1. / JpsiK_2d_bg->Integral(1, JpsiK_2d_nbinsX, 1, JpsiK_2d_nbinsY) * n_comb_bg);
    cos_theta_Zc_bg->Scale(1. / cos_theta_Zc_bg->Integral() * n_comb_bg);
    phi_K_bg->Scale(1. / phi_K_bg->Integral() * n_comb_bg);
    alpha_mu_bg->Scale(1. / alpha_mu_bg->Integral() * n_comb_bg);
    phi_mu_Kstar_bg->Scale(1. / phi_mu_Kstar_bg->Integral() * n_comb_bg);
    phi_mu_X_bg->Scale(1. / phi_mu_X_bg->Integral() * n_comb_bg);
    cos_theta_psi_X_bg->Scale(1. / cos_theta_psi_X_bg->Integral() * n_comb_bg);
    cos_theta_Kstar_bg->Scale(1. / cos_theta_Kstar_bg->Integral() * n_comb_bg);
    cos_theta_psi_Kstar_bg->Scale(1. / cos_theta_psi_Kstar_bg->Integral() * n_comb_bg);
    cos_theta_B0_bg->Scale(1. / cos_theta_B0_bg->Integral() * n_comb_bg);
    
    //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__
    
    JpsiK_2d_control_BD_to_JPSI_K_PI->Scale(1. / JpsiK_2d_control_BD_to_JPSI_K_PI->Integral(1, JpsiK_2d_control_nbinsX, 1, JpsiK_2d_control_nbinsY) * n_control_BD_to_JPSI_K_PI);
    KK_1d_control_BD_to_JPSI_K_PI->Scale(1. / KK_1d_control_BD_to_JPSI_K_PI->Integral(1, KK_1d_control_nbins) * n_control_BD_to_JPSI_K_PI);
    
    JpsiK_2d_control_BS_to_JPSI_PI_PI->Scale(1. / JpsiK_2d_control_BS_to_JPSI_PI_PI->Integral(1, JpsiK_2d_control_nbinsX, 1, JpsiK_2d_control_nbinsY) * n_control_BS_to_JPSI_PI_PI);
    KK_1d_control_BS_to_JPSI_PI_PI->Scale(1. / KK_1d_control_BS_to_JPSI_PI_PI->Integral(1, KK_1d_control_nbins) * n_control_BS_to_JPSI_PI_PI);
    
    JpsiK_2d_control_BD_to_JPSI_PI_PI->Scale(1. / JpsiK_2d_control_BD_to_JPSI_PI_PI->Integral(1, JpsiK_2d_control_nbinsX, 1, JpsiK_2d_control_nbinsY) * n_control_BD_to_JPSI_PI_PI);
    KK_1d_control_BD_to_JPSI_PI_PI->Scale(1. / KK_1d_control_BD_to_JPSI_PI_PI->Integral(1, KK_1d_control_nbins) * n_control_BD_to_JPSI_PI_PI);
    
    JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K->Scale(1. / JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K->Integral(1, JpsiK_2d_control_nbinsX, 1, JpsiK_2d_control_nbinsY) * n_control_LAMBDA0B_to_JPSI_P_K);
    KK_1d_control_LAMBDA0B_to_JPSI_P_K->Scale(1. / KK_1d_control_LAMBDA0B_to_JPSI_P_K->Integral(1, KK_1d_control_nbins) * n_control_LAMBDA0B_to_JPSI_P_K);
    
    JpsiK_2d_control_BS_to_JPSI_K_K->Scale(1. / JpsiK_2d_control_BS_to_JPSI_K_K->Integral(1, JpsiK_2d_control_nbinsX, 1, JpsiK_2d_control_nbinsY) * n_BS_to_JPSI_K_K);
    KK_1d_control_BS_to_JPSI_K_K->Scale(1. / KK_1d_control_BS_to_JPSI_K_K->Integral(1, KK_1d_control_nbins) * n_BS_to_JPSI_K_K);
    
    JpsiK_2d_control_LAMBDA0B_to_JPSI_P_PI->Scale(1. / JpsiK_2d_control_LAMBDA0B_to_JPSI_P_PI->Integral(1, JpsiK_2d_control_nbinsX, 1, JpsiK_2d_control_nbinsY) * n_LAMBDA0B_to_JPSI_P_PI);
    KK_1d_control_LAMBDA0B_to_JPSI_P_PI->Scale(1. / KK_1d_control_LAMBDA0B_to_JPSI_P_PI->Integral(1, KK_1d_control_nbins) * n_LAMBDA0B_to_JPSI_P_PI);
    
    JpsiK_2d_control_BD_to_JPSI_K_K->Scale(1. / JpsiK_2d_control_BD_to_JPSI_K_K->Integral(1, JpsiK_2d_control_nbinsX, 1, JpsiK_2d_control_nbinsY) * n_BD_to_JPSI_K_K);
    KK_1d_control_BD_to_JPSI_K_K->Scale(1. / KK_1d_control_BD_to_JPSI_K_K->Integral(1, KK_1d_control_nbins) * n_BD_to_JPSI_K_K);
    
    JpsiK_2d_control_BS_to_JPSI_K_PI->Scale(1. / JpsiK_2d_control_BS_to_JPSI_K_PI->Integral(1, JpsiK_2d_control_nbinsX, 1, JpsiK_2d_control_nbinsY) * n_BS_to_JPSI_K_PI);
    KK_1d_control_BS_to_JPSI_K_PI->Scale(1. / KK_1d_control_BS_to_JPSI_K_PI->Integral(1, KK_1d_control_nbins) * n_BS_to_JPSI_K_PI);
    
    JpsiK_2d_control_bg->Scale(1. / JpsiK_2d_control_bg->Integral(1, JpsiK_2d_control_nbinsX, 1, JpsiK_2d_control_nbinsY) * n_control_comb_bg);
    KK_1d_control_bg->Scale(1. / KK_1d_control_bg->Integral(1, KK_1d_control_nbins) * n_control_comb_bg);
    
    //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__
    
    Jpsip_2d_controlLb_BD_to_JPSI_K_PI->Scale(1. / Jpsip_2d_controlLb_BD_to_JPSI_K_PI->Integral(1, Jpsip_2d_controlLb_nbinsX, 1, Jpsip_2d_controlLb_nbinsY) * n_controlLb_BD_to_JPSI_K_PI);
    pK_2d_controlLb_BD_to_JPSI_K_PI->Scale(1. / pK_2d_controlLb_BD_to_JPSI_K_PI->Integral(1, pK_2d_controlLb_nbinsX, 1, pK_2d_controlLb_nbinsY) * n_controlLb_BD_to_JPSI_K_PI);
    JpsiK_2d_controlLb_BD_to_JPSI_K_PI->Scale(1. / JpsiK_2d_controlLb_BD_to_JPSI_K_PI->Integral(1, JpsiK_2d_controlLb_nbinsX, 1, JpsiK_2d_controlLb_nbinsY) * n_controlLb_BD_to_JPSI_K_PI);
    
    Jpsip_2d_controlLb_BS_to_JPSI_PI_PI->Scale(1. / Jpsip_2d_controlLb_BS_to_JPSI_PI_PI->Integral(1, Jpsip_2d_controlLb_nbinsX, 1, Jpsip_2d_controlLb_nbinsY) * n_controlLb_BS_to_JPSI_PI_PI);
    pK_2d_controlLb_BS_to_JPSI_PI_PI->Scale(1. / pK_2d_controlLb_BS_to_JPSI_PI_PI->Integral(1, pK_2d_controlLb_nbinsX, 1, pK_2d_controlLb_nbinsY) * n_controlLb_BS_to_JPSI_PI_PI);
    JpsiK_2d_controlLb_BS_to_JPSI_PI_PI->Scale(1. / JpsiK_2d_controlLb_BS_to_JPSI_PI_PI->Integral(1, JpsiK_2d_controlLb_nbinsX, 1, JpsiK_2d_controlLb_nbinsY) * n_controlLb_BS_to_JPSI_PI_PI);
    
    Jpsip_2d_controlLb_BD_to_JPSI_PI_PI->Scale(1. / Jpsip_2d_controlLb_BD_to_JPSI_PI_PI->Integral(1, Jpsip_2d_controlLb_nbinsX, 1, Jpsip_2d_controlLb_nbinsY) * n_controlLb_BD_to_JPSI_PI_PI);
    pK_2d_controlLb_BD_to_JPSI_PI_PI->Scale(1. / pK_2d_controlLb_BD_to_JPSI_PI_PI->Integral(1, pK_2d_controlLb_nbinsX, 1, pK_2d_controlLb_nbinsY) * n_controlLb_BD_to_JPSI_PI_PI);
    JpsiK_2d_controlLb_BD_to_JPSI_PI_PI->Scale(1. / JpsiK_2d_controlLb_BD_to_JPSI_PI_PI->Integral(1, JpsiK_2d_controlLb_nbinsX, 1, JpsiK_2d_controlLb_nbinsY) * n_controlLb_BD_to_JPSI_PI_PI);
    
    Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Scale(1. / Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Integral(1, Jpsip_2d_controlLb_nbinsX, 1, Jpsip_2d_controlLb_nbinsY) * n_controlLb_LAMBDA0B_to_JPSI_P_K);
    pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Scale(1. / pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Integral(1, pK_2d_controlLb_nbinsX, 1, pK_2d_controlLb_nbinsY) * n_controlLb_LAMBDA0B_to_JPSI_P_K);
    JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Scale(1. / JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Integral(1, JpsiK_2d_controlLb_nbinsX, 1, JpsiK_2d_controlLb_nbinsY) * n_controlLb_LAMBDA0B_to_JPSI_P_K);
    
    Jpsip_2d_controlLb_BS_to_JPSI_K_K->Scale(1. / Jpsip_2d_controlLb_BS_to_JPSI_K_K->Integral(1, Jpsip_2d_controlLb_nbinsX, 1, Jpsip_2d_controlLb_nbinsY) * n_controlLb_BS_to_JPSI_K_K);
    pK_2d_controlLb_BS_to_JPSI_K_K->Scale(1. / pK_2d_controlLb_BS_to_JPSI_K_K->Integral(1, pK_2d_controlLb_nbinsX, 1, pK_2d_controlLb_nbinsY) * n_controlLb_BS_to_JPSI_K_K);
    JpsiK_2d_controlLb_BS_to_JPSI_K_K->Scale(1. / JpsiK_2d_controlLb_BS_to_JPSI_K_K->Integral(1, JpsiK_2d_controlLb_nbinsX, 1, JpsiK_2d_controlLb_nbinsY) * n_controlLb_BS_to_JPSI_K_K);
    
    Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->Scale(1. / Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->Integral(1, Jpsip_2d_controlLb_nbinsX, 1, Jpsip_2d_controlLb_nbinsY) * n_controlLb_LAMBDA0B_to_JPSI_P_PI);
    pK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->Scale(1. / pK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->Integral(1, pK_2d_controlLb_nbinsX, 1, pK_2d_controlLb_nbinsY) * n_controlLb_LAMBDA0B_to_JPSI_P_PI);
    JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->Scale(1. / JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->Integral(1, JpsiK_2d_controlLb_nbinsX, 1, JpsiK_2d_controlLb_nbinsY) * n_controlLb_LAMBDA0B_to_JPSI_P_PI);
    
    Jpsip_2d_controlLb_BS_to_JPSI_K_PI->Scale(1. / Jpsip_2d_controlLb_BS_to_JPSI_K_PI->Integral(1, Jpsip_2d_controlLb_nbinsX, 1, Jpsip_2d_controlLb_nbinsY) * n_controlLb_BS_to_JPSI_K_PI);
    pK_2d_controlLb_BS_to_JPSI_K_PI->Scale(1. / pK_2d_controlLb_BS_to_JPSI_K_PI->Integral(1, pK_2d_controlLb_nbinsX, 1, pK_2d_controlLb_nbinsY) * n_controlLb_BS_to_JPSI_K_PI);
    JpsiK_2d_controlLb_BS_to_JPSI_K_PI->Scale(1. / JpsiK_2d_controlLb_BS_to_JPSI_K_PI->Integral(1, JpsiK_2d_controlLb_nbinsX, 1, JpsiK_2d_controlLb_nbinsY) * n_controlLb_BS_to_JPSI_K_PI);
    
    Jpsip_2d_controlLb_BD_to_JPSI_K_K->Scale(1. / Jpsip_2d_controlLb_BD_to_JPSI_K_K->Integral(1, Jpsip_2d_controlLb_nbinsX, 1, Jpsip_2d_controlLb_nbinsY) * n_controlLb_BD_to_JPSI_K_K);
    pK_2d_controlLb_BD_to_JPSI_K_K->Scale(1. / pK_2d_controlLb_BD_to_JPSI_K_K->Integral(1, pK_2d_controlLb_nbinsX, 1, pK_2d_controlLb_nbinsY) * n_controlLb_BD_to_JPSI_K_K);
    JpsiK_2d_controlLb_BD_to_JPSI_K_K->Scale(1. / JpsiK_2d_controlLb_BD_to_JPSI_K_K->Integral(1, JpsiK_2d_controlLb_nbinsX, 1, JpsiK_2d_controlLb_nbinsY) * n_controlLb_BD_to_JPSI_K_K);
    
    Jpsip_2d_controlLb_bg->Scale(1. / Jpsip_2d_controlLb_bg->Integral(1, Jpsip_2d_controlLb_nbinsX, 1, Jpsip_2d_controlLb_nbinsY) * n_controlLb_comb_bg);
    pK_2d_controlLb_bg->Scale(1. / pK_2d_controlLb_bg->Integral(1, pK_2d_controlLb_nbinsX, 1, pK_2d_controlLb_nbinsY) * n_controlLb_comb_bg);
    JpsiK_2d_controlLb_bg->Scale(1. / JpsiK_2d_controlLb_bg->Integral(1, JpsiK_2d_controlLb_nbinsX, 1, JpsiK_2d_controlLb_nbinsY) * n_controlLb_comb_bg);
    
    /*cout << "\nsignal area:\n" << endl;
    cout << "n_BD_to_JPSI_K_PI\t" << n_BD_to_JPSI_K_PI << endl;
    cout << "n_BS_to_JPSI_K_K\t" << n_BS_to_JPSI_K_K << endl;
    cout << "n_BS_to_JPSI_PI_PI\t" << n_BS_to_JPSI_PI_PI << endl;
    cout << "n_BD_to_JPSI_PI_PI\t" << n_BD_to_JPSI_PI_PI << endl;
    cout << "n_LAMBDA0B_to_JPSI_P_K\t" << n_LAMBDA0B_to_JPSI_P_K << endl;
    cout << "n_comb_bg\t" << n_comb_bg << endl; */
    
    /*cout << "\ncontrol area:\n" << endl;
    cout << "n_BD_to_JPSI_K_PI\t" << n_control_BD_to_JPSI_K_PI << endl;
    cout << "n_BS_to_JPSI_K_K\t" << n_control_BS_to_JPSI_K_K << endl;
    cout << "n_BS_to_JPSI_PI_PI\t" << n_control_BS_to_JPSI_PI_PI << endl;
    cout << "n_BD_to_JPSI_PI_PI\t" << n_control_BD_to_JPSI_PI_PI << endl;
    cout << "n_LAMBDA0B_to_JPSI_P_K\t" << n_control_LAMBDA0B_to_JPSI_P_K << endl;
    cout << "n_comb_bg\t" << n_control_comb_bg << endl; */
    
    if (Jpsipi_fit_mode)
    {
        for(int i = 1; i <= Jpsipi_2d_data->ProjectionX()->GetNbinsX(); i++)
        {
            for(int k = 1; k <= Jpsipi_2d_data->ProjectionY()->GetNbinsX(); k++)
            {
                //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
                //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
                double nb = 0.0; double nberr = 0.0; double fb = 0.0;
                double bglevel0_psipi = Jpsipi_2d_bg->GetBinContent(i, k);
                double bglevel1_psipi = Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
                double bglevel2_psipi = Jpsipi_2d_BD_to_JPSI_PI_PI->GetBinContent(i, k);
                double bglevel3_psipi = Jpsipi_2d_BS_to_JPSI_PI_PI->GetBinContent(i, k);
                double bglevel4_psipi = Jpsipi_2d_BS_to_JPSI_K_K->GetBinContent(i, k);
                double bglevel5_psipi = Jpsipi_2d_BD_to_JPSI_K_PI->GetBinContent(i, k);
                if (RARE_DECAYS)
                {
                    fb += Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                    fb += Jpsipi_2d_BS_to_JPSI_K_PI->GetBinContent(i, k);
                    fb += Jpsipi_2d_BD_to_JPSI_K_K->GetBinContent(i, k);
                }
                //cout << bglevel0_psipi << endl;
                nb = Jpsipi_2d_data->GetBinContent(i, k);
                fb += bglevel0_psipi + bglevel1_psipi + bglevel2_psipi + bglevel3_psipi + bglevel4_psipi + bglevel5_psipi;
                if( fb > 0.0 ) chisq += -nb*log(fb) + fb;
                //if ((-nb*log(fb) + fb) > 0.) {cout << i << "\t" << k << endl;}
            }
        }
    }
    
    if (Kpi_fit_mode) 
    {
        for(int i = 1; i <= Kpi_2d_data->ProjectionX()->GetNbinsX(); i++)
        {
            for(int k = 1; k <= Kpi_2d_data->ProjectionY()->GetNbinsX(); k++)
            {
                //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
                //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
                double nb = 0.0; double nberr = 0.0; double fb = 0.0;
                double bglevel0_psipi = Kpi_2d_bg->GetBinContent(i, k);
                double bglevel1_psipi = Kpi_2d_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
                double bglevel2_psipi = Kpi_2d_BD_to_JPSI_PI_PI->GetBinContent(i, k);
                double bglevel3_psipi = Kpi_2d_BS_to_JPSI_PI_PI->GetBinContent(i, k);
                double bglevel4_psipi = Kpi_2d_BS_to_JPSI_K_K->GetBinContent(i, k);
                double bglevel5_psipi = Kpi_2d_BD_to_JPSI_K_PI->GetBinContent(i, k);
                if (RARE_DECAYS)
                {
                    fb += Kpi_2d_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                    fb += Kpi_2d_BS_to_JPSI_K_PI->GetBinContent(i, k);
                    fb += Kpi_2d_BD_to_JPSI_K_K->GetBinContent(i, k);
                }
                nb = Kpi_2d_data->GetBinContent(i, k);
                fb += bglevel0_psipi + bglevel1_psipi + bglevel2_psipi + bglevel3_psipi + bglevel4_psipi + bglevel5_psipi;
                if( fb > 0.0 ) chisq += -nb*log(fb) + fb;
            }
        }
    }
    
    if (JpsiK_fit_mode)
    {
        for(int i = 1; i <= JpsiK_2d_data->ProjectionX()->GetNbinsX(); i++)
        {
            for(int k = 1; k <= JpsiK_2d_data->ProjectionY()->GetNbinsX(); k++)
            {
                //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
                //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
                double nb = 0.0; double nberr = 0.0; double fb = 0.0;
                double bglevel0_psipi = JpsiK_2d_bg->GetBinContent(i, k);
                double bglevel1_psipi = JpsiK_2d_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
                double bglevel2_psipi = JpsiK_2d_BD_to_JPSI_PI_PI->GetBinContent(i, k);
                double bglevel3_psipi = JpsiK_2d_BS_to_JPSI_PI_PI->GetBinContent(i, k);
                double bglevel4_psipi = JpsiK_2d_BS_to_JPSI_K_K->GetBinContent(i, k);
                double bglevel5_psipi = JpsiK_2d_BD_to_JPSI_K_PI->GetBinContent(i, k);
                if (RARE_DECAYS)
                {
                    fb += JpsiK_2d_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                    fb += JpsiK_2d_BS_to_JPSI_K_PI->GetBinContent(i, k);
                    fb += JpsiK_2d_BD_to_JPSI_K_K->GetBinContent(i, k);
                }
                nb = JpsiK_2d_data->GetBinContent(i, k);
                fb += bglevel0_psipi + bglevel1_psipi + bglevel2_psipi + bglevel3_psipi + bglevel4_psipi + bglevel5_psipi;
                if( fb > 0.0 ) chisq += -nb*log(fb) + fb;
            }
        }
    }
    
    if (cos_theta_Zc_fit_mode)
    {
        for (int i = 1; i <= cos_theta_Zc_data->ProjectionX()->GetNbinsX(); i++)
        {
            for (int k = 1; k <= cos_theta_Zc_data->ProjectionY()->GetNbinsX(); k++)
            {
                //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
                //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
                double nb = 0.0; double nberr = 0.0; double fb = 0.0;
                double bglevel0_psipi = cos_theta_Zc_bg->GetBinContent(i, k);
                double bglevel1_psipi = cos_theta_Zc_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
                double bglevel2_psipi = cos_theta_Zc_BD_to_JPSI_PI_PI->GetBinContent(i, k);
                double bglevel3_psipi = cos_theta_Zc_BS_to_JPSI_PI_PI->GetBinContent(i, k);
                double bglevel4_psipi = cos_theta_Zc_BS_to_JPSI_K_K->GetBinContent(i, k);
                double bglevel5_psipi = cos_theta_Zc_BD_to_JPSI_K_PI->GetBinContent(i, k);
                if (RARE_DECAYS)
                {
                    fb += cos_theta_Zc_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                    fb += cos_theta_Zc_BS_to_JPSI_K_PI->GetBinContent(i, k);
                    fb += cos_theta_Zc_BD_to_JPSI_K_K->GetBinContent(i, k);
                }
                nb = cos_theta_Zc_data->GetBinContent(i, k);
                fb += bglevel0_psipi + bglevel1_psipi + bglevel2_psipi + bglevel3_psipi + bglevel4_psipi + bglevel5_psipi;
                if( fb > 0.0 ) chisq += -nb*log(fb) + fb;
            }
        }
    }
    
    if (signal_area_fit_mode || signal_area_fit_mode_noZc)
    {    
        for (int i = 1; i <= cos_theta_B0_data->ProjectionX()->GetNbinsX(); i++)
        {
            for (int k = 1; k <= cos_theta_B0_data->ProjectionY()->GetNbinsX(); k++)
            {
                //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
                //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
                double nb = 0.0; double nberr = 0.0; double fb = 0.0;
                double bglevel0_psipi = cos_theta_B0_bg->GetBinContent(i, k);
                double bglevel1_psipi = cos_theta_B0_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
                double bglevel2_psipi = cos_theta_B0_BD_to_JPSI_PI_PI->GetBinContent(i, k);
                double bglevel3_psipi = cos_theta_B0_BS_to_JPSI_PI_PI->GetBinContent(i, k);
                double bglevel4_psipi = cos_theta_B0_BS_to_JPSI_K_K->GetBinContent(i, k);
                double bglevel5_psipi = cos_theta_B0_BD_to_JPSI_K_PI->GetBinContent(i, k);
                if (RARE_DECAYS)
                {
                    fb += cos_theta_B0_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                    fb += cos_theta_B0_BS_to_JPSI_K_PI->GetBinContent(i, k);
                    fb += cos_theta_B0_BD_to_JPSI_K_K->GetBinContent(i, k);
                }
                nb = cos_theta_B0_data->GetBinContent(i, k);
                fb += bglevel0_psipi + bglevel1_psipi + bglevel2_psipi + bglevel3_psipi + bglevel4_psipi + bglevel5_psipi;
                if( fb > 0.0 ) chisq += -nb*log(fb) + fb;
            }
        }
        
        for (int i = 1; i <= cos_theta_Kstar_data->ProjectionX()->GetNbinsX(); i++)
        {
            for (int k = 1; k <= cos_theta_Kstar_data->ProjectionY()->GetNbinsX(); k++)
            {
                //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
                //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
                double nb = 0.0; double nberr = 0.0; double fb = 0.0;
                double bglevel0_psipi = cos_theta_Kstar_bg->GetBinContent(i, k);
                double bglevel1_psipi = cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
                double bglevel2_psipi = cos_theta_Kstar_BD_to_JPSI_PI_PI->GetBinContent(i, k);
                double bglevel3_psipi = cos_theta_Kstar_BS_to_JPSI_PI_PI->GetBinContent(i, k);
                double bglevel4_psipi = cos_theta_Kstar_BS_to_JPSI_K_K->GetBinContent(i, k);
                double bglevel5_psipi = cos_theta_Kstar_BD_to_JPSI_K_PI->GetBinContent(i, k);
                if (RARE_DECAYS)
                {
                    fb += cos_theta_Kstar_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                    fb += cos_theta_Kstar_BS_to_JPSI_K_PI->GetBinContent(i, k);
                    fb += cos_theta_Kstar_BD_to_JPSI_K_K->GetBinContent(i, k);
                }
                nb = cos_theta_Kstar_data->GetBinContent(i, k);
                fb += bglevel0_psipi + bglevel1_psipi + bglevel2_psipi + bglevel3_psipi + bglevel4_psipi + bglevel5_psipi;
                if( fb > 0.0 ) chisq += -nb*log(fb) + fb;
            }
        }
        
        for (int i = 1; i <= cos_theta_psi_Kstar_data->ProjectionX()->GetNbinsX(); i++)
        {
            for (int k = 1; k <= cos_theta_psi_Kstar_data->ProjectionY()->GetNbinsX(); k++)
            {
                //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
                //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
                double nb = 0.0; double nberr = 0.0; double fb = 0.0;
                double bglevel0_psipi = cos_theta_psi_Kstar_bg->GetBinContent(i, k);
                double bglevel1_psipi = cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
                double bglevel2_psipi = cos_theta_psi_Kstar_BD_to_JPSI_PI_PI->GetBinContent(i, k);
                double bglevel3_psipi = cos_theta_psi_Kstar_BS_to_JPSI_PI_PI->GetBinContent(i, k);
                double bglevel4_psipi = cos_theta_psi_Kstar_BS_to_JPSI_K_K->GetBinContent(i, k);
                double bglevel5_psipi = cos_theta_psi_Kstar_BD_to_JPSI_K_PI->GetBinContent(i, k);
                if (RARE_DECAYS)
                {
                    fb += cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                    fb += cos_theta_psi_Kstar_BS_to_JPSI_K_PI->GetBinContent(i, k);
                    fb += cos_theta_psi_Kstar_BD_to_JPSI_K_K->GetBinContent(i, k);
                }
                nb = cos_theta_psi_Kstar_data->GetBinContent(i, k);
                fb += bglevel0_psipi + bglevel1_psipi + bglevel2_psipi + bglevel3_psipi + bglevel4_psipi + bglevel5_psipi;
                if( fb > 0.0 ) chisq += -nb*log(fb) + fb;
            }
        }
    
        for (int i = 1; i <= phi_mu_X_data->ProjectionX()->GetNbinsX(); i++)
        {
            for (int k = 1; k <= phi_mu_X_data->ProjectionY()->GetNbinsX(); k++)
            {
                //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
                //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
                double nb = 0.0; double nberr = 0.0; double fb = 0.0;
                double bglevel0_psipi = phi_mu_X_bg->GetBinContent(i, k);
                double bglevel1_psipi = phi_mu_X_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
                double bglevel2_psipi = phi_mu_X_BD_to_JPSI_PI_PI->GetBinContent(i, k);
                double bglevel3_psipi = phi_mu_X_BS_to_JPSI_PI_PI->GetBinContent(i, k);
                double bglevel4_psipi = phi_mu_X_BS_to_JPSI_K_K->GetBinContent(i, k);
                double bglevel5_psipi = phi_mu_X_BD_to_JPSI_K_PI->GetBinContent(i, k);
                if (RARE_DECAYS)
                {
                    fb += phi_mu_X_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                    fb += phi_mu_X_BS_to_JPSI_K_PI->GetBinContent(i, k);
                    fb += phi_mu_X_BD_to_JPSI_K_K->GetBinContent(i, k);
                }
                nb = phi_mu_X_data->GetBinContent(i, k);
                fb += bglevel0_psipi + bglevel1_psipi + bglevel2_psipi + bglevel3_psipi + bglevel4_psipi + bglevel5_psipi;
                if( fb > 0.0 ) chisq += -nb*log(fb) + fb;
            }
        }
    
        for (int i = 1; i <= phi_K_data->ProjectionX()->GetNbinsX(); i++)
        {
            for (int k = 1; k <= phi_K_data->ProjectionY()->GetNbinsX(); k++)
            {
                //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
                //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
                double nb = 0.0; double nberr = 0.0; double fb = 0.0;
                double bglevel0_psipi = phi_K_bg->GetBinContent(i, k);
                double bglevel1_psipi = phi_K_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
                double bglevel2_psipi = phi_K_BD_to_JPSI_PI_PI->GetBinContent(i, k);
                double bglevel3_psipi = phi_K_BS_to_JPSI_PI_PI->GetBinContent(i, k);
                double bglevel4_psipi = phi_K_BS_to_JPSI_K_K->GetBinContent(i, k);
                double bglevel5_psipi = phi_K_BD_to_JPSI_K_PI->GetBinContent(i, k);
                if (RARE_DECAYS)
                {
                    fb += phi_K_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                    fb += phi_K_BS_to_JPSI_K_PI->GetBinContent(i, k);
                    fb += phi_K_BD_to_JPSI_K_K->GetBinContent(i, k);
                }
                nb = phi_K_data->GetBinContent(i, k);
                fb += bglevel0_psipi + bglevel1_psipi + bglevel2_psipi + bglevel3_psipi + bglevel4_psipi + bglevel5_psipi;
                if( fb > 0.0 ) chisq += -nb*log(fb) + fb;
            }
        }
    
        //float xmin_psiK = 3600.; // for change
        //float xmax_psiK = 4900.; // for change
    }
    
    if (control_area_Bs_fit_mode)
    {
        for (int i = 1; i <= JpsiK_2d_control_data->ProjectionX()->GetNbinsX(); i++)
        {
            for (int k = 1; k <= JpsiK_2d_control_data->ProjectionY()->GetNbinsX(); k++)
            {
                //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
                //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
                double nb = 0.0; double nberr = 0.0; double fb = 0.0;
                double bglevel0_psipi = JpsiK_2d_control_bg->GetBinContent(i, k);
                double bglevel1_psipi = JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
                double bglevel2_psipi = JpsiK_2d_control_BD_to_JPSI_PI_PI->GetBinContent(i, k);
                double bglevel3_psipi = JpsiK_2d_control_BS_to_JPSI_PI_PI->GetBinContent(i, k);
                double bglevel4_psipi = JpsiK_2d_control_BS_to_JPSI_K_K->GetBinContent(i, k);
                double bglevel5_psipi = JpsiK_2d_control_BD_to_JPSI_K_PI->GetBinContent(i, k);
                if (RARE_DECAYS)
                {
                    fb += JpsiK_2d_control_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                    fb += JpsiK_2d_control_BS_to_JPSI_K_PI->GetBinContent(i, k);
                    fb += JpsiK_2d_control_BD_to_JPSI_K_K->GetBinContent(i, k);
                }
                nb = JpsiK_2d_control_data->GetBinContent(i, k);
                fb += bglevel0_psipi + bglevel1_psipi + bglevel2_psipi + bglevel3_psipi + bglevel4_psipi + bglevel5_psipi;
                if( fb > 0.0 ) chisq += -nb*log(fb) + fb;
            }
        }
    
        for (int i = 1; i <= KK_1d_control_data->GetNbinsX(); i++)
        {
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            double bglevel0_psipi = KK_1d_control_bg->GetBinContent(i);
            double bglevel1_psipi = KK_1d_control_LAMBDA0B_to_JPSI_P_K->GetBinContent(i);
            double bglevel2_psipi = KK_1d_control_BD_to_JPSI_PI_PI->GetBinContent(i);
            double bglevel3_psipi = KK_1d_control_BS_to_JPSI_PI_PI->GetBinContent(i);
            double bglevel4_psipi = KK_1d_control_BS_to_JPSI_K_K->GetBinContent(i);
            double bglevel5_psipi = KK_1d_control_BD_to_JPSI_K_PI->GetBinContent(i);
            if (RARE_DECAYS)
            {
                fb += KK_1d_control_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i);
                fb += KK_1d_control_BS_to_JPSI_K_PI->GetBinContent(i);
                fb += KK_1d_control_BD_to_JPSI_K_K->GetBinContent(i);
            }
            nb = KK_1d_control_data->GetBinContent(i);
            fb += bglevel0_psipi + bglevel1_psipi + bglevel2_psipi + bglevel3_psipi + bglevel4_psipi + bglevel5_psipi;
            if( fb > 0.0 ) chisq += -nb*log(fb) + fb;
        }
    }
    
    /*for (int i = 1; i <= JpsiK_2d_controlLb_data->ProjectionX()->GetNbinsX(); i++)
    {
        for (int k = 1; k <= JpsiK_2d_controlLb_data->ProjectionY()->GetNbinsX(); k++)
        {
            //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
            //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            double bglevel0_psipi = JpsiK_2d_controlLb_bg->GetBinContent(i, k);
            double bglevel1_psipi = JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
            double bglevel2_psipi = JpsiK_2d_controlLb_BD_to_JPSI_PI_PI->GetBinContent(i, k);
            double bglevel3_psipi = JpsiK_2d_controlLb_BS_to_JPSI_PI_PI->GetBinContent(i, k);
            double bglevel4_psipi = JpsiK_2d_controlLb_BS_to_JPSI_K_K->GetBinContent(i, k);
            double bglevel5_psipi = JpsiK_2d_controlLb_BD_to_JPSI_K_PI->GetBinContent(i, k);
            nb = JpsiK_2d_controlLb_data->GetBinContent(i, k);
            fb = bglevel0_psipi + bglevel1_psipi + bglevel2_psipi + bglevel3_psipi + bglevel4_psipi + bglevel5_psipi;
            if( fb > 0.0 ) chisq += -nb*log(fb) + fb;
        }
    }
    
    for (int i = 1; i <= Jpsip_2d_controlLb_data->ProjectionX()->GetNbinsX(); i++)
    {
        for (int k = 1; k <= Jpsip_2d_controlLb_data->ProjectionY()->GetNbinsX(); k++)
        {
            //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
            //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            double bglevel0_psipi = Jpsip_2d_controlLb_bg->GetBinContent(i, k);
            double bglevel1_psipi = Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
            double bglevel2_psipi = Jpsip_2d_controlLb_BD_to_JPSI_PI_PI->GetBinContent(i, k);
            double bglevel3_psipi = Jpsip_2d_controlLb_BS_to_JPSI_PI_PI->GetBinContent(i, k);
            double bglevel4_psipi = Jpsip_2d_controlLb_BS_to_JPSI_K_K->GetBinContent(i, k);
            double bglevel5_psipi = Jpsip_2d_controlLb_BD_to_JPSI_K_PI->GetBinContent(i, k);
            nb = Jpsip_2d_controlLb_data->GetBinContent(i, k);
            fb = bglevel0_psipi + bglevel1_psipi + bglevel2_psipi + bglevel3_psipi + bglevel4_psipi + bglevel5_psipi;
            if( fb > 0.0 ) chisq += -nb*log(fb) + fb;
        }
    }
    
    for (int i = 1; i <= pK_2d_controlLb_data->ProjectionX()->GetNbinsX(); i++)
    {
        for (int k = 1; k <= pK_2d_controlLb_data->ProjectionY()->GetNbinsX(); k++)
        {
            //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
            //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            double bglevel0_psipi = pK_2d_controlLb_bg->GetBinContent(i, k);
            double bglevel1_psipi = pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
            double bglevel2_psipi = pK_2d_controlLb_BD_to_JPSI_PI_PI->GetBinContent(i, k);
            double bglevel3_psipi = pK_2d_controlLb_BS_to_JPSI_PI_PI->GetBinContent(i, k);
            double bglevel4_psipi = pK_2d_controlLb_BS_to_JPSI_K_K->GetBinContent(i, k);
            double bglevel5_psipi = pK_2d_controlLb_BD_to_JPSI_K_PI->GetBinContent(i, k);
            nb = pK_2d_controlLb_data->GetBinContent(i, k);
            fb = bglevel0_psipi + bglevel1_psipi + bglevel2_psipi + bglevel3_psipi + bglevel4_psipi + bglevel5_psipi;
            if( fb > 0.0 ) chisq += -nb*log(fb) + fb;
        }
    }*/
    
    //float integral = 0.;
    
    if (JpsiKpi_4tr_fit_mode)
    {
        for(int i = 1; i <= Kpi_piK_2d_data->ProjectionX()->GetNbinsX(); i++)
        {
            for(int j = 1; j <= Kpi_piK_2d_data->ProjectionY()->GetNbinsX(); j++)  //
            {
                double xx0 = Kpi_piK_2d_data->ProjectionX()->GetBinCenter(i); 
                double yy0 = Kpi_piK_2d_data->ProjectionY()->GetBinCenter(j);
                double x = st * xx0 + st * yy0 - 6.6;
                double y = -st * xx0 + st * yy0;
                double nb = 0.0; double nberr = 0.0; double fb = 0.0;
                fb = Kpi_piK_2d_bg->GetBinContent(i, j);
                fb += Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, j);
                fb += Kpi_piK_2d_BS_to_JPSI_PI_PI->GetBinContent(i, j);
                fb += Kpi_piK_2d_BD_to_JPSI_PI_PI->GetBinContent(i, j);
                fb += Kpi_piK_2d_BD_to_JPSI_K_PI->GetBinContent(i, j);
                fb += Kpi_piK_2d_BS_to_JPSI_K_K->GetBinContent(i, j);
                if (RARE_DECAYS)
                {
                    fb += Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, j);
                    fb += Kpi_piK_2d_BS_to_JPSI_K_PI->GetBinContent(i, j);
                    fb += Kpi_piK_2d_BD_to_JPSI_K_K->GetBinContent(i, j);
                }
                nb = Kpi_piK_2d_data->GetBinContent(i, j);
                //integral += exp(par[85] + x * par[86] + pow(x, 2.) * par[87] + pow(x, 3.) * par[88]) * exp(-1. * pow( (y + par[89]) / (par[90] + x * par[91] + pow(x, 2.) * par[92]), 2.));
                if( fb > 0.0 ) chisq += -nb*log(fb) + fb;
                //cout << "nb = " << nb << " || fb = " << fb << " || par[0] = " << par[0] << endl;
            }
        }
    }
    
    if (Jpsippi_4tr_fit_mode)
    {
        for(int i = 1; i <= ppi_pip_2d_data->ProjectionX()->GetNbinsX(); i++)
        {
            for(int j = 1; j <= ppi_pip_2d_data->ProjectionY()->GetNbinsX(); j++)  //
            {
                //double xx0 = ppi_pip_2d_data->ProjectionX()->GetBinCenter(i); 
                //double yy0 = ppi_pip_2d_data->ProjectionY()->GetBinCenter(j);
                double nb = 0.0; double nberr = 0.0; double fb = 0.0;
                fb = ppi_pip_2d_bg->GetBinContent(i, j);
                fb += ppi_pip_2d_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, j);
                fb += ppi_pip_2d_BS_to_JPSI_PI_PI->GetBinContent(i, j);
                fb += ppi_pip_2d_BD_to_JPSI_PI_PI->GetBinContent(i, j);
                fb += ppi_pip_2d_BD_to_JPSI_K_PI->GetBinContent(i, j);
                fb += ppi_pip_2d_BS_to_JPSI_K_K->GetBinContent(i, j);
                if (RARE_DECAYS)
                {
                    fb += ppi_pip_2d_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, j);
                    fb += ppi_pip_2d_BS_to_JPSI_K_PI->GetBinContent(i, j);
                    fb += ppi_pip_2d_BD_to_JPSI_K_K->GetBinContent(i, j);
                }
                nb = ppi_pip_2d_data->GetBinContent(i, j);
                //integral += exp(par[85] + x * par[86] + pow(x, 2.) * par[87] + pow(x, 3.) * par[88]) * exp(-1. * pow( (y + par[89]) / (par[90] + x * par[91] + pow(x, 2.) * par[92]), 2.));
                if( fb > 0.0 ) chisq += -nb*log(fb) + fb;
                //cout << "nb = " << nb << " || fb = " << fb << " || par[0] = " << par[0] << endl;
            }
        }
    }
    
    if (JpsiKK_4tr_fit_mode)
    {
        for(int i = 1; i <= KK_1d_data->GetNbinsX(); i++)
        {
            double xx0 = KK_1d_data->GetBinCenter(i); 
            double x = xx0 - xmin_KK;
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            fb = KK_1d_bg->GetBinContent(i);
            fb += KK_1d_LAMBDA0B_to_JPSI_P_K->GetBinContent(i);
            fb += KK_1d_BS_to_JPSI_PI_PI->GetBinContent(i);
            fb += KK_1d_BD_to_JPSI_PI_PI->GetBinContent(i);
            fb += KK_1d_BD_to_JPSI_K_PI->GetBinContent(i);
            fb += KK_1d_BS_to_JPSI_K_K->GetBinContent(i);
            if (RARE_DECAYS)
            {
                fb += KK_1d_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i);
                fb += KK_1d_BS_to_JPSI_K_PI->GetBinContent(i);
                fb += KK_1d_BD_to_JPSI_K_K->GetBinContent(i);
            }
            nb = KK_1d_data->GetBinContent(i);
            if( fb > 0.0 ) chisq += -nb*log(fb) + fb;
        //cout << "nb = " << nb << " || fb = " << fb << " || par[0] = " << par[0] << endl;
        }
    }
    
    if (Jpsipipi_4tr_fit_mode)
    {
        for(int i = 1; i <= pipi_1d_data->GetNbinsX(); i++)
        {
            double xx0 = pipi_1d_data->GetBinCenter(i); 
            double x = xx0 - xmin_pipi;
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            fb = pipi_1d_bg->GetBinContent(i);;
            fb += pipi_1d_LAMBDA0B_to_JPSI_P_K->GetBinContent(i);
            fb += pipi_1d_BS_to_JPSI_PI_PI->GetBinContent(i);
            fb += pipi_1d_BD_to_JPSI_PI_PI->GetBinContent(i);
            fb += pipi_1d_BD_to_JPSI_K_PI->GetBinContent(i);
            fb += pipi_1d_BS_to_JPSI_K_K->GetBinContent(i);
            if (RARE_DECAYS)
            {
                fb += pipi_1d_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i);
                fb += pipi_1d_BS_to_JPSI_K_PI->GetBinContent(i);
                fb += pipi_1d_BD_to_JPSI_K_K->GetBinContent(i);
            }
            nb = pipi_1d_data->GetBinContent(i);
            if( fb > 0.0 ) chisq += -nb*log(fb) + fb;
            //cout << "nb = " << nb << " || fb = " << fb << " || par[0] = " << par[0] << endl;
        }
    }
    
    if (JpsipK_4tr_fit_mode)
    {
        for(int i = 1; i <= pK_Kp_2d_data->ProjectionX()->GetNbinsX(); i++)
        {
            for(int j = 1; j <= pK_Kp_2d_data->ProjectionY()->GetNbinsX(); j++)  //
            {
                double xx0 = pK_Kp_2d_data->ProjectionX()->GetBinCenter(i); 
                double yy0 = pK_Kp_2d_data->ProjectionY()->GetBinCenter(j);
                double x = st * xx0 + st * yy0 - par[120];
                double y = -st * xx0 + st * yy0;
                double nb = 0.0; double nberr = 0.0; double fb = 0.0;
                fb = pK_Kp_2d_bg->GetBinContent(i, j);
                fb += pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, j);
                fb += pK_Kp_2d_BS_to_JPSI_PI_PI->GetBinContent(i, j);
                fb += pK_Kp_2d_BD_to_JPSI_PI_PI->GetBinContent(i, j);
                fb += pK_Kp_2d_BD_to_JPSI_K_PI->GetBinContent(i, j);
                fb += pK_Kp_2d_BS_to_JPSI_K_K->GetBinContent(i, j);
                if (RARE_DECAYS)
                {
                    fb += pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, j);
                    fb += pK_Kp_2d_BS_to_JPSI_K_PI->GetBinContent(i, j);
                    fb += pK_Kp_2d_BD_to_JPSI_K_K->GetBinContent(i, j);
                }
                nb = pK_Kp_2d_data->GetBinContent(i, j);
                if( fb > 0.0 ) chisq += -nb*log(fb) + fb;
                //cout << "nb = " << nb << " || fb = " << fb << " || par[0] = " << par[0] << endl;
            }
        }
    }
    
    /*if (n_fit_step == 1) {
    
    Jpsipi_2d_bg->SetLineColor(1);
    Jpsipi_2d_bg->SetFillColor(42);
    Jpsipi_2d_BS_to_JPSI_K_K->SetFillColor(30);
    Jpsipi_2d_BD_to_JPSI_K_PI->SetFillColor(7);
    Jpsipi_2d_BD_to_JPSI_PI_PI->SetFillColor(8);
    Jpsipi_2d_BS_to_JPSI_PI_PI->SetFillColor(46);
    Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    Jpsipi_2d_data->SetMarkerStyle(kFullCircle);
    Jpsipi_2d_data->SetMarkerSize(0.7);
    
    JpsiK_2d_bg->SetLineColor(1);
    JpsiK_2d_bg->SetFillColor(42);
    JpsiK_2d_BS_to_JPSI_K_K->SetFillColor(30);
    JpsiK_2d_BD_to_JPSI_K_PI->SetFillColor(7);
    JpsiK_2d_BD_to_JPSI_PI_PI->SetFillColor(8);
    JpsiK_2d_BS_to_JPSI_PI_PI->SetFillColor(46);
    JpsiK_2d_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    JpsiK_2d_data->SetMarkerStyle(kFullCircle);
    JpsiK_2d_data->SetMarkerSize(0.7);
    
    THStack hs_JpsipiX("hs_JpsipiX","Jpsipi_signalX");
    
    hs_JpsipiX.Add(Jpsipi_2d_bg->ProjectionX());
    hs_JpsipiX.Add(Jpsipi_2d_BD_to_JPSI_PI_PI->ProjectionX());
    hs_JpsipiX.Add(Jpsipi_2d_BS_to_JPSI_PI_PI->ProjectionX());
    hs_JpsipiX.Add(Jpsipi_2d_BS_to_JPSI_K_K->ProjectionX());
    hs_JpsipiX.Add(Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->ProjectionX());
    hs_JpsipiX.Add(Jpsipi_2d_BD_to_JPSI_K_PI->ProjectionX());
    TCanvas * c_JpsipiX = new TCanvas("jpsipi_mass_ProjectionX");
    Jpsipi_2d_data->ProjectionX()->SetStats(kFALSE);
    Jpsipi_2d_data->ProjectionX()->GetXaxis()->SetTitle("GeV");
    Jpsipi_2d_data->ProjectionX()->GetYaxis()->SetTitle("Events / 34 MeV");
    Jpsipi_2d_data->ProjectionX()->GetYaxis()->SetRangeUser(0., 2500.);
    Jpsipi_2d_data->ProjectionX()->Draw("");
    hs_JpsipiX.Draw("hist same");
    Jpsipi_2d_data->ProjectionX()->Draw("same");
    c_JpsipiX->SaveAs("test1.png");
    //legend_Jpsipi_X->Draw();
    
    for(int i = 0; i < 10; i++)
    {
        cout << weight[i] << endl;
    }
    
    cout << "\n";
    
     cout << Jpsipi_truth[0] << endl;
    cout << Kpi_truth[0] << endl;
    cout << theta_X_a[0] << endl;
    cout << theta_Kstar_a[0] << endl;
    cout << theta_psi_X_a[0] << endl;
    cout << theta_psi_Kstar_a[0] << endl;
    cout << phi_mu_X_a[0] << endl;
    cout << phi_mu_Kstar_a[0] << endl;
    cout << phi_K_a[0] << endl;
    cout << alpha_mu_a[0] << endl;
    cout << BWK1410_1_1_minus[0] << endl;
    cout << BWK1410_1_2_minus[0] << endl;
    cout << BWK1410_1_3_minus[0] << endl;
    cout << BWK1430_0_1_minus[0] << endl;
    cout << BWK1430_2_1_minus[0] << endl;
    cout << BWK1430_2_2_minus[0] << endl;
    cout << BWK1430_2_3_minus[0] << endl;
    cout << BWK1680_1_1_minus[0] << endl;
    cout << BWK1680_1_2_minus[0] << endl;
    cout << BWK1680_1_3_minus[0] << endl;
    cout << BWK1780_3_1_minus[0] << endl;
    cout << BWK1780_3_2_minus[0] << endl;
    cout << BWK1780_3_3_minus[0] << endl;
    cout << BWK1950_0_1_minus[0] << endl;
    cout << BWK1980_2_1_minus[0] << endl;
    cout << BWK1980_2_2_minus[0] << endl;
    cout << BWK1980_2_3_minus[0] << endl;
    cout << BWK2045_4_1_minus[0] << endl;
    cout << BWK2045_4_2_minus[0] << endl;
    cout << BWK2045_4_3_minus[0] << endl;
    cout << BWK1410_1_1_plus[0] << endl;
    cout << BWK1410_1_2_plus[0] << endl;
    cout << BWK1410_1_3_plus[0] << endl;
    cout << BWK1430_0_1_plus[0] << endl;
    cout << BWK1430_2_1_plus[0] << endl;
    cout << BWK1430_2_2_plus[0] << endl;
    cout << BWK1430_2_3_plus[0] << endl;
    cout << BWK1680_1_1_plus[0] << endl;
    cout << BWK1680_1_2_plus[0] << endl;
    cout << BWK1680_1_3_plus[0] << endl;
    cout << BWK1780_3_1_plus[0] << endl;
    cout << BWK1780_3_2_plus[0] << endl;
    cout << BWK1780_3_3_plus[0] << endl;
    cout << BWK1950_0_1_plus[0] << endl;
    cout << BWK1980_2_1_plus[0] << endl;
    cout << BWK1980_2_2_plus[0] << endl;
    cout << BWK1980_2_3_plus[0] << endl;
    cout << BWK2045_4_1_plus[0] << endl;
    cout << BWK2045_4_2_plus[0] << endl;
    cout << BWK2045_4_3_plus[0] << endl;
    cout << w_dimuon_a[0] << "\n" << endl;
    
    for(int i = 0; i < 46; i++)
    {
        cout << par[i] << endl;
    }
    
    }*/
    
    //cout << chisq << "\t" << n_fit_step  << endl; // "\t" << integral
    f = chisq;
}

int signal_area_fit_Zc1plus_v16(int threads, std::string par_in, std::string par_out, Int_t fit_style, Int_t decay_model)
{  
    std::string pictures_path;
    std::string angles_path;
    
    char writetext00[100];
    char writetext01[100];

    sprintf(writetext00,"#font[72]{ATLAS} Internal");
    sprintf(writetext01,"#sqrt{s} = 13 TeV;  138.2 fb^{-1}");

    TLatex *t = new TLatex();
    t->SetNDC(1);
    t->SetTextAlign(13);
    t->SetTextColor(kBlack);
    t->SetTextSize(0.035);
    t->SetTextFont(42);
    
    if (RARE_DECAYS)
    {
        cout << "!!! Attention !!!\nYou turn ON Rare decays\n!!! Attention !!!\n" << endl;
    }

    switch (decay_model)
    {
        case 0:
            cout << "\n\n\t\t|| MODEL Zc1plus ||\n" << endl;
            MODEL = 0;
            pictures_path = "pictures_Zc1plus/";
            angles_path = "angles_Zc1plus/";
            break;
        case 1:
            cout << "\n\n\t\t|| MODEL noZc ||\n" << endl;
            MODEL = 1;
            pictures_path = "pictures_noZc/";
            angles_path = "angles_noZc/";
            break;
        case 2:
            cout << "\n\n\t\t|| MODEL Zc1minus ||\n" << endl;
            MODEL = 2;
            pictures_path = "pictures_Zc1minus/";
            angles_path = "angles_Zc1minus/";
            break;
        case 3:
            cout << "\n\n\t\t|| MODEL Zc0minus ||\n" << endl;
            MODEL = 3;
            pictures_path = "pictures_Zc0minus/";
            angles_path = "angles_Zc0minus/";
            break;
        case 4:
            cout << "\n\n\t\t|| MODEL Zc2minus ||\n" << endl;
            MODEL = 4;
            pictures_path = "pictures_Zc2minus/";
            angles_path = "angles_Zc2minus/";
            break;
        case 5:
            cout << "\n\n\t\t|| MODEL Zc2plus ||\n" << endl;
            MODEL = 5;
            pictures_path = "pictures_Zc2plus/";
            angles_path = "angles_Zc2plus/";
            break;
        case 6:
            cout << "\n\n\t\t|| MODEL drawing pictures for test ||\n" << endl;
            MODEL = 6;
            pictures_path = "pictures_test/";
            angles_path = "angles_test/";
            break;
        default:
            cout << "\n!!! Choose proper MODEL (0-6) !!!\n" << endl;
            return 1;
            break;
    }
        
    if (JpsipK_fit_mode && (Jpsipipi_fit_mode || Jpsipipi_fit_mode_plus_JpsiKpi)) {cout << "JpsipK_fit_mode and Jpsipipi_fit_mode flags both equal to 1" << endl; return 1;}
    if (JpsipK_fit_mode && JpsiKK_fit_mode) {cout << "JpsipK_fit_mode and JpsiKK_fit_mode flags both equal to 1" << endl; return 1;}
    if (JpsipK_fit_mode && (JpsiKpi_fit_mode || JpsiKpi_fit_mode_no_signal)) {cout << "JpsipK_fit_mode and JpsiKpi_fit_mode flags both equal to 1" << endl; return 1;}
    if ((Jpsipipi_fit_mode || Jpsipipi_fit_mode_plus_JpsiKpi) && JpsipK_fit_mode) {cout << "Jpsipipi_fit_mode and JpsipK_fit_mode flags both equal to 1" << endl; return 1;}
    if ((Jpsipipi_fit_mode || Jpsipipi_fit_mode_plus_JpsiKpi) && JpsiKK_fit_mode) {cout << "Jpsipipi_fit_mode and JpsiKK_fit_mode flags both equal to 1" << endl; return 1;}
    if ((Jpsipipi_fit_mode || Jpsipipi_fit_mode_plus_JpsiKpi) && (JpsiKpi_fit_mode || JpsiKpi_fit_mode_no_signal)) {cout << "Jpsipipi_fit_mode and JpsiKpi_fit_mode flags both equal to 1" << endl; return 1;}
    
    JpsipK_fit_mode_plus_JpsiKpi = 0; Jpsipipi_fit_mode_plus_JpsiKpi = 0;
    JpsiKK_fit_mode_plus_JpsiKpi = 0; JpsiKpi_fit_mode_no_signal = 0;
    signal_area_fit_mode = 0;
    Jpsipipi_fit_mode_plus_JpsiKpi_lbfix = 0;
    JpsiKK_fit_mode_plus_JpsiKpi_lbpipi_fix = 0;
    JpsiKK_fit_mode_plus_JpsiKpi_no_control_lbpipi_fix = 0;
    signal_area_fit_mode_short = 0;
    JpsipK_fit_mode_comb_only = 0;
    fit_mode = 1;
    global_area_no_comb_form_4tr = 0;
    Jpsipipi_fit_mode_comb_only = 0;
    JpsiKK_fit_mode_comb_only = 0;
    JpsiKpi_fit_mode_comb_only = 0;
    signal_area_fit_mode_short_noZc = 0;
    signal_area_fit_mode_noZc = 0;
    signal_area_fit_mode_onlyZc = 0;
    Jpsippi_fit_mode_comb_only = 0;
    
    switch (fit_style)
    {
        case 0:
            cout << "\nStarting fit JpsipK_fit_mode_plus_JpsiKpi...\n" << endl;
            JpsipK_fit_mode_plus_JpsiKpi = 1;
            break;
        case 1:
            cout << "\nStarting fit Jpsipipi_fit_mode_plus_JpsiKpi...\n" << endl;
            Jpsipipi_fit_mode_plus_JpsiKpi = 1;
            break;
        case 2:
            cout << "\nStarting fit JpsiKK_fit_mode_plus_JpsiKpi...\n" << endl;
            JpsiKK_fit_mode_plus_JpsiKpi = 1;
            break;
        case 3:
            cout << "\nStarting fit JpsiKpi_fit_mode_no_signal...\n" << endl;
            JpsiKpi_fit_mode_no_signal = 1;
            break;
        case 4:
            cout << "\nStarting fit signal_area_fit_mode...\n" << endl;
            signal_area_fit_mode = 1;
            break;
        case 5:
            cout << "\nStarting fit Jpsipipi_fit_mode_plus_JpsiKpi_lbfix...\n" << endl;
            Jpsipipi_fit_mode_plus_JpsiKpi_lbfix = 1;
            break;
        case 6:
            cout << "\nStarting fit JpsiKK_fit_mode_plus_JpsiKpi_lbpipi_fix...\n" << endl;
            JpsiKK_fit_mode_plus_JpsiKpi_lbpipi_fix = 1;
            break;
        case 7:
            cout << "\nStarting fit JpsiKK_fit_mode_plus_JpsiKpi_no_control_lbpipi_fix...\n" << endl;
            JpsiKK_fit_mode_plus_JpsiKpi_no_control_lbpipi_fix = 1;
            break;
        case 8:
            cout << "\nStarting fit signal_area_fit_mode_short...\n" << endl;
            signal_area_fit_mode_short = 1;
            break;
        case 9:
            cout << "\nStarting fit JpsipK_fit_mode_comb_only...\n" << endl;
            JpsipK_fit_mode_comb_only = 1;
            break;
        case 10:
            cout << "\nStarting fit global_area_no_comb_form_4tr...\n" << endl;
            global_area_no_comb_form_4tr = 1;
            break; 
        case 11:
            cout << "\nStarting fit Jpsipipi_fit_mode_comb_only...\n" << endl;
            Jpsipipi_fit_mode_comb_only = 1;
            break;
        case 12:
            cout << "\nStarting fit JpsiKK_fit_mode_comb_only...\n" << endl;
            JpsiKK_fit_mode_comb_only = 1;
            break;
        case 13:
            cout << "\nStarting fit JpsiKpi_fit_mode_comb_only...\n" << endl;
            JpsiKpi_fit_mode_comb_only = 1;
            break;
        case 14:
            cout << "\nStarting fit signal_area_fit_mode_short_noZc...\n" << endl;
            signal_area_fit_mode_short_noZc = 1;
            break;
        case 15:
            cout << "\nStarting fit signal_area_fit_mode_noZc...\n" << endl;
            signal_area_fit_mode_noZc = 1;
            break;
        case 16:
            cout << "\nStarting fit signal_area_fit_mode_onlyZc...\n" << endl;
            signal_area_fit_mode_onlyZc = 1;
            break;
        case 17:
            cout << "\nStarting fit Jpsippi_fit_mode_comb_only...\n" << endl;
            Jpsippi_fit_mode_comb_only = 1;
            break;
        case 101:
            cout << "\nMode: only drawing pictures\n" << endl;
            fit_mode = 0;
            break;
        default:
            cout << "\n!!! Choose proper fit_style (0-9 & 101) !!!\n" << endl;
            return 1;
            break;
    }
    
    THREADS = threads;
    std::string par_in_name = "parameters/" + par_in;
    TFile *f_in_par = new TFile(par_in_name.c_str(), "open");
    TVectorD *vpar = (TVectorD*)f_in_par->Get("parameters");
    
    for(int j=0; j<214; j++){
	
	if( (j >= 0 && j <= 43)   ){
	  if( j % 2 == 1){  
		
		int n = (int)((*vpar)(j)/2/PI);   (*vpar)(j) =  (*vpar)(j) - n*2*PI;  // force phase to 0..2PI range
		if((*vpar)(j-1) < 0){(*vpar)(j-1) *= -1.0; (*vpar)(j) += PI; }        // force amplitude to be positive, phase gets additional PI
		if(j > 1) (*vpar)(j) = (*vpar)(j) - (*vpar)(1);                       //subtraction of the K1410 phase
		}//ifj%2

	}//if 0..43



	if( (j >= 47 && j <= 78)   ){
	  if( j % 2 == 0){  
		int n = (int)((*vpar)(j)/2/PI);   (*vpar)(j) =  (*vpar)(j) - n*2*PI;  // force phase to 0..2PI range
		if((*vpar)(j-1) < 0){(*vpar)(j-1) *= -1.0; (*vpar)(j) += PI; }  // force amplitude to be positive, phase gets additional PI 
		if(j > 48) (*vpar)(j) = (*vpar)(j) - (*vpar)(48);
		}
	}// if 47..78

    } //for
    (*vpar)(1) = 0.0;
    (*vpar)(48) = 0.0;
    
    Double_t buf_amp, buf_phi;
    
    if ((*vpar).GetNrows() < 222 )
    {
        buf_amp = 0.;
        buf_phi = 0.;
    }
    else 
    {   
        buf_amp = (*vpar)(217);
        buf_phi = (*vpar)(218);
    }
    
    TFile *f_sas = new TFile("ROOT_files/signal_area_shape.root", "open");
    TH2 *area_hist = (TH2D*)f_sas->Get("area_hist");
    
    TFile *f_cas = new TFile("ROOT_files/signal_area_shape_Lb.root", "open");
    TH2 *area_hist_Lb = (TH2D*)f_cas->Get("area_hist");
    
    n_global_BD_to_JPSI_K_PI = (*vpar)(83), 
    n_global_BS_to_JPSI_K_K = (*vpar)(84),
    n_global_BS_to_JPSI_K_K_ini = (*vpar)(84),
    n_global_BS_to_JPSI_PI_PI = (*vpar)(81),
    n_global_BD_to_JPSI_PI_PI = (*vpar)(82),
    n_global_LAMBDA0B_to_JPSI_P_K = (*vpar)(80);
    n_global_LAMBDA0B_to_JPSI_P_PI = (*vpar)(214);
    n_global_BD_to_JPSI_K_K = (*vpar)(215);
    n_global_BS_to_JPSI_K_PI = (*vpar)(216);
    
    for(int i = 0; i < 40; i++)
    {
        for(int k = 0; k < 62; k++)
        {
            sa_a[i][k] = 0;
            if (area_hist->GetBinContent(i + 1, k + 1) > 0.)
            {
                sa_a[i][k] = 1;
            }
        }
    }
    
    for(int i = 0; i < 50; i++)
    {
        for(int k = 0; k < 51; k++)
        {
            calb_a[i][k] = 0;
            if (area_hist_Lb->GetBinContent(i + 1, k + 1) > 0.)
            {
                calb_a[i][k] = 1;
            }
        }
    }
    
    TFile *f_4tr = new TFile("ROOT_files/fitting_Zc1plus.root");
    
    Kpi_piK_2d_data = (TH2D*)f_4tr->Get("Kpi_piK_2d_data");
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K = (TH2D*)f_4tr->Get("Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K");
    Kpi_piK_2d_BS_to_JPSI_PI_PI = (TH2D*)f_4tr->Get("Kpi_piK_2d_BS_to_JPSI_PI_PI");
    Kpi_piK_2d_BD_to_JPSI_PI_PI = (TH2D*)f_4tr->Get("Kpi_piK_2d_BD_to_JPSI_PI_PI");
    Kpi_piK_2d_BD_to_JPSI_K_PI = (TH2D*)f_4tr->Get("Kpi_piK_2d_BD_to_JPSI_K_PI");
    Kpi_piK_2d_BS_to_JPSI_K_K = (TH2D*)f_4tr->Get("Kpi_piK_2d_BS_to_JPSI_K_K");
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI = (TH2D*)f_4tr->Get("Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI");
    Kpi_piK_2d_BD_to_JPSI_K_K = (TH2D*)f_4tr->Get("Kpi_piK_2d_BD_to_JPSI_K_K");
    Kpi_piK_2d_BS_to_JPSI_K_PI = (TH2D*)f_4tr->Get("Kpi_piK_2d_BS_to_JPSI_K_PI");
    
//    TH2 *Kpi_piK_2d_bg = new TH2D("Kpi_piK_2d_bg", "Kpi_piK_2d_bg", 40, 4.900, 5.700, 62, 4.650, 5.900);

    pK_Kp_2d_data = (TH2D*)f_4tr->Get("pK_Kp_2d_test_data");
    pK_Kp_2d_LAMBDA0B_to_JPSI_P_K = (TH2D*)f_4tr->Get("pK_Kp_2d_test_LAMBDA0B_to_JPSI_P_K");
    pK_Kp_2d_BS_to_JPSI_PI_PI = (TH2D*)f_4tr->Get("pK_Kp_2d_test_BS_to_JPSI_PI_PI");
    pK_Kp_2d_BD_to_JPSI_PI_PI = (TH2D*)f_4tr->Get("pK_Kp_2d_test_BD_to_JPSI_PI_PI");
    pK_Kp_2d_BD_to_JPSI_K_PI = (TH2D*)f_4tr->Get("pK_Kp_2d_test_BD_to_JPSI_K_PI");
    pK_Kp_2d_BS_to_JPSI_K_K = (TH2D*)f_4tr->Get("pK_Kp_2d_test_BS_to_JPSI_K_K");
    pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI = (TH2D*)f_4tr->Get("pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI");
    pK_Kp_2d_BD_to_JPSI_K_K = (TH2D*)f_4tr->Get("pK_Kp_2d_BD_to_JPSI_K_K");
    pK_Kp_2d_BS_to_JPSI_K_PI = (TH2D*)f_4tr->Get("pK_Kp_2d_BS_to_JPSI_K_PI");
    
    ppi_pip_2d_data = (TH2D*)f_4tr->Get("ppi_pip_2d_data");
    ppi_pip_2d_LAMBDA0B_to_JPSI_P_K = (TH2D*)f_4tr->Get("ppi_pip_2d_LAMBDA0B_to_JPSI_P_K");
    ppi_pip_2d_BS_to_JPSI_PI_PI = (TH2D*)f_4tr->Get("ppi_pip_2d_BS_to_JPSI_PI_PI");
    ppi_pip_2d_BD_to_JPSI_PI_PI = (TH2D*)f_4tr->Get("ppi_pip_2d_BD_to_JPSI_PI_PI");
    ppi_pip_2d_BD_to_JPSI_K_PI = (TH2D*)f_4tr->Get("ppi_pip_2d_BD_to_JPSI_K_PI");
    ppi_pip_2d_BS_to_JPSI_K_K = (TH2D*)f_4tr->Get("ppi_pip_2d_BS_to_JPSI_K_K");
    ppi_pip_2d_LAMBDA0B_to_JPSI_P_PI = (TH2D*)f_4tr->Get("ppi_pip_2d_LAMBDA0B_to_JPSI_P_PI");
    ppi_pip_2d_BD_to_JPSI_K_K = (TH2D*)f_4tr->Get("ppi_pip_2d_BD_to_JPSI_K_K");
    ppi_pip_2d_BS_to_JPSI_K_PI = (TH2D*)f_4tr->Get("ppi_pip_2d_BS_to_JPSI_K_PI");
    
    pK_Kp_2d_rotate_data = (TH2D*)f_4tr->Get("pK_Kp_2d_rotate_data");
    pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_K = (TH2D*)f_4tr->Get("pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_K");
    pK_Kp_2d_rotate_BS_to_JPSI_PI_PI = (TH2D*)f_4tr->Get("pK_Kp_2d_rotate_BS_to_JPSI_PI_PI");
    pK_Kp_2d_rotate_BD_to_JPSI_PI_PI = (TH2D*)f_4tr->Get("pK_Kp_2d_rotate_BD_to_JPSI_PI_PI");
    pK_Kp_2d_rotate_BD_to_JPSI_K_PI = (TH2D*)f_4tr->Get("pK_Kp_2d_rotate_BD_to_JPSI_K_PI");
    pK_Kp_2d_rotate_BS_to_JPSI_K_K = (TH2D*)f_4tr->Get("pK_Kp_2d_rotate_BS_to_JPSI_K_K");
    pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_PI = (TH2D*)f_4tr->Get("pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_PI");
    pK_Kp_2d_rotate_BD_to_JPSI_K_K = (TH2D*)f_4tr->Get("pK_Kp_2d_rotate_BD_to_JPSI_K_K");
    pK_Kp_2d_rotate_BS_to_JPSI_K_PI = (TH2D*)f_4tr->Get("pK_Kp_2d_rotate_BS_to_JPSI_K_PI");
    
    /*pK_Kp_2d_test_data = (TH2D*)f_4tr->Get("pK_Kp_2d_test_data");
    pK_Kp_2d_test_LAMBDA0B_to_JPSI_P_K = (TH2D*)f_4tr->Get("pK_Kp_2d_test_LAMBDA0B_to_JPSI_P_K");
    pK_Kp_2d_test_BS_to_JPSI_PI_PI = (TH2D*)f_4tr->Get("pK_Kp_2d_test_BS_to_JPSI_PI_PI");
    pK_Kp_2d_test_BD_to_JPSI_PI_PI = (TH2D*)f_4tr->Get("pK_Kp_2d_test_BD_to_JPSI_PI_PI");
    pK_Kp_2d_test_BD_to_JPSI_K_PI = (TH2D*)f_4tr->Get("pK_Kp_2d_test_BD_to_JPSI_K_PI");
    pK_Kp_2d_test_BS_to_JPSI_K_K = (TH2D*)f_4tr->Get("pK_Kp_2d_test_BS_to_JPSI_K_K");*/
    
    KK_1d_data = (TH1D*)f_4tr->Get("KK_1d_data");
    KK_1d_LAMBDA0B_to_JPSI_P_K = (TH1D*)f_4tr->Get("KK_1d_LAMBDA0B_to_JPSI_P_K");
    KK_1d_BS_to_JPSI_PI_PI = (TH1D*)f_4tr->Get("KK_1d_BS_to_JPSI_PI_PI");
    KK_1d_BD_to_JPSI_PI_PI = (TH1D*)f_4tr->Get("KK_1d_BD_to_JPSI_PI_PI");
    KK_1d_BD_to_JPSI_K_PI = (TH1D*)f_4tr->Get("KK_1d_BD_to_JPSI_K_PI");
    KK_1d_BS_to_JPSI_K_K = (TH1D*)f_4tr->Get("KK_1d_BS_to_JPSI_K_K");
    KK_1d_LAMBDA0B_to_JPSI_P_PI = (TH1D*)f_4tr->Get("KK_1d_LAMBDA0B_to_JPSI_P_PI");
    KK_1d_BD_to_JPSI_K_K = (TH1D*)f_4tr->Get("KK_1d_BD_to_JPSI_K_K");
    KK_1d_BS_to_JPSI_K_PI = (TH1D*)f_4tr->Get("KK_1d_BS_to_JPSI_K_PI");
    
    pipi_1d_data = (TH1D*)f_4tr->Get("pipi_1d_data");
    pipi_1d_LAMBDA0B_to_JPSI_P_K = (TH1D*)f_4tr->Get("pipi_1d_LAMBDA0B_to_JPSI_P_K");
    pipi_1d_BS_to_JPSI_PI_PI = (TH1D*)f_4tr->Get("pipi_1d_BS_to_JPSI_PI_PI");
    pipi_1d_BD_to_JPSI_PI_PI = (TH1D*)f_4tr->Get("pipi_1d_BD_to_JPSI_PI_PI");
    pipi_1d_BD_to_JPSI_K_PI = (TH1D*)f_4tr->Get("pipi_1d_BD_to_JPSI_K_PI");
    pipi_1d_BS_to_JPSI_K_K = (TH1D*)f_4tr->Get("pipi_1d_BS_to_JPSI_K_K");
    pipi_1d_LAMBDA0B_to_JPSI_P_PI = (TH1D*)f_4tr->Get("pipi_1d_LAMBDA0B_to_JPSI_P_PI");
    pipi_1d_BD_to_JPSI_K_K = (TH1D*)f_4tr->Get("pipi_1d_BD_to_JPSI_K_K");
    pipi_1d_BS_to_JPSI_K_PI = (TH1D*)f_4tr->Get("pipi_1d_BS_to_JPSI_K_PI");
    
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo_|BD_to_JPSI_K_PI_signal_area|_oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
    
    TFile *f_in = new TFile("ROOT_files/signal_area_Zc1plus.root");
    
    f_in->GetObject("Jpsipi_2d_data", Jpsipi_2d_data);
    f_in->GetObject("Jpsipi_2d_LAMBDA0B_to_JPSI_P_K", Jpsipi_2d_LAMBDA0B_to_JPSI_P_K);
    f_in->GetObject("Jpsipi_2d_BS_to_JPSI_K_K", Jpsipi_2d_BS_to_JPSI_K_K);
    f_in->GetObject("Jpsipi_2d_BS_to_JPSI_PI_PI", Jpsipi_2d_BS_to_JPSI_PI_PI);
    f_in->GetObject("Jpsipi_2d_BD_to_JPSI_K_PI", Jpsipi_2d_BD_to_JPSI_K_PI);
    f_in->GetObject("Jpsipi_2d_BD_to_JPSI_PI_PI", Jpsipi_2d_BD_to_JPSI_PI_PI);
    f_in->GetObject("Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI", Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI);
    f_in->GetObject("Jpsipi_2d_BD_to_JPSI_K_K", Jpsipi_2d_BD_to_JPSI_K_K);
    f_in->GetObject("Jpsipi_2d_BS_to_JPSI_K_PI", Jpsipi_2d_BS_to_JPSI_K_PI);
    
    f_in->GetObject("JpsiK_2d_data", JpsiK_2d_data);
    f_in->GetObject("JpsiK_2d_LAMBDA0B_to_JPSI_P_K", JpsiK_2d_LAMBDA0B_to_JPSI_P_K);
    f_in->GetObject("JpsiK_2d_BS_to_JPSI_K_K", JpsiK_2d_BS_to_JPSI_K_K);
    f_in->GetObject("JpsiK_2d_BS_to_JPSI_PI_PI", JpsiK_2d_BS_to_JPSI_PI_PI);
    f_in->GetObject("JpsiK_2d_BD_to_JPSI_K_PI", JpsiK_2d_BD_to_JPSI_K_PI);
    f_in->GetObject("JpsiK_2d_BD_to_JPSI_PI_PI", JpsiK_2d_BD_to_JPSI_PI_PI);
    f_in->GetObject("JpsiK_2d_LAMBDA0B_to_JPSI_P_PI", JpsiK_2d_LAMBDA0B_to_JPSI_P_PI);
    f_in->GetObject("JpsiK_2d_BD_to_JPSI_K_K", JpsiK_2d_BD_to_JPSI_K_K);
    f_in->GetObject("JpsiK_2d_BS_to_JPSI_K_PI", JpsiK_2d_BS_to_JPSI_K_PI);
    
    f_in->GetObject("Kpi_2d_data", Kpi_2d_data);
    f_in->GetObject("Kpi_2d_LAMBDA0B_to_JPSI_P_K", Kpi_2d_LAMBDA0B_to_JPSI_P_K);
    f_in->GetObject("Kpi_2d_BS_to_JPSI_K_K", Kpi_2d_BS_to_JPSI_K_K);
    f_in->GetObject("Kpi_2d_BS_to_JPSI_PI_PI", Kpi_2d_BS_to_JPSI_PI_PI);
    f_in->GetObject("Kpi_2d_BD_to_JPSI_K_PI", Kpi_2d_BD_to_JPSI_K_PI);
    f_in->GetObject("Kpi_2d_BD_to_JPSI_PI_PI", Kpi_2d_BD_to_JPSI_PI_PI);
    f_in->GetObject("Kpi_2d_LAMBDA0B_to_JPSI_P_PI", Kpi_2d_LAMBDA0B_to_JPSI_P_PI);
    f_in->GetObject("Kpi_2d_BD_to_JPSI_K_K", Kpi_2d_BD_to_JPSI_K_K);
    f_in->GetObject("Kpi_2d_BS_to_JPSI_K_PI", Kpi_2d_BS_to_JPSI_K_PI);
    
    TFile *f_cos = new TFile("ROOT_files/cos_signal.root");
    
    f_cos->GetObject("cos_theta_Zc_data", cos_theta_Zc_data);
    f_cos->GetObject("cos_theta_Zc_LAMBDA0B_to_JPSI_P_K", cos_theta_Zc_LAMBDA0B_to_JPSI_P_K);
    f_cos->GetObject("cos_theta_Zc_BS_to_JPSI_PI_PI", cos_theta_Zc_BS_to_JPSI_PI_PI);
    f_cos->GetObject("cos_theta_Zc_BD_to_JPSI_PI_PI", cos_theta_Zc_BD_to_JPSI_PI_PI);
    f_cos->GetObject("cos_theta_Zc_BD_to_JPSI_K_PI", cos_theta_Zc_BD_to_JPSI_K_PI);
    f_cos->GetObject("cos_theta_Zc_BS_to_JPSI_K_K", cos_theta_Zc_BS_to_JPSI_K_K);
    f_cos->GetObject("cos_theta_Zc_LAMBDA0B_to_JPSI_P_PI", cos_theta_Zc_LAMBDA0B_to_JPSI_P_PI);
    f_cos->GetObject("cos_theta_Zc_BD_to_JPSI_K_K", cos_theta_Zc_BD_to_JPSI_K_K);
    f_cos->GetObject("cos_theta_Zc_BS_to_JPSI_K_PI", cos_theta_Zc_BS_to_JPSI_K_PI);
    
    f_cos->GetObject("cos_theta_B0_data", cos_theta_B0_data);
    f_cos->GetObject("cos_theta_B0_LAMBDA0B_to_JPSI_P_K", cos_theta_B0_LAMBDA0B_to_JPSI_P_K);
    f_cos->GetObject("cos_theta_B0_BS_to_JPSI_PI_PI", cos_theta_B0_BS_to_JPSI_PI_PI);
    f_cos->GetObject("cos_theta_B0_BD_to_JPSI_PI_PI", cos_theta_B0_BD_to_JPSI_PI_PI);
    f_cos->GetObject("cos_theta_B0_BD_to_JPSI_K_PI", cos_theta_B0_BD_to_JPSI_K_PI);
    f_cos->GetObject("cos_theta_B0_BS_to_JPSI_K_K", cos_theta_B0_BS_to_JPSI_K_K);
    f_cos->GetObject("cos_theta_B0_LAMBDA0B_to_JPSI_P_PI", cos_theta_B0_LAMBDA0B_to_JPSI_P_PI);
    f_cos->GetObject("cos_theta_B0_BD_to_JPSI_K_K", cos_theta_B0_BD_to_JPSI_K_K);
    f_cos->GetObject("cos_theta_B0_BS_to_JPSI_K_PI", cos_theta_B0_BS_to_JPSI_K_PI);
    
    f_cos->GetObject("phi_psi_data", phi_psi_data);
    f_cos->GetObject("phi_psi_LAMBDA0B_to_JPSI_P_K", phi_psi_LAMBDA0B_to_JPSI_P_K);
    f_cos->GetObject("phi_psi_BS_to_JPSI_PI_PI", phi_psi_BS_to_JPSI_PI_PI);
    f_cos->GetObject("phi_psi_BD_to_JPSI_PI_PI", phi_psi_BD_to_JPSI_PI_PI);
    f_cos->GetObject("phi_psi_BD_to_JPSI_K_PI", phi_psi_BD_to_JPSI_K_PI);
    f_cos->GetObject("phi_psi_BS_to_JPSI_K_K", phi_psi_BS_to_JPSI_K_K);
    f_cos->GetObject("phi_psi_LAMBDA0B_to_JPSI_P_PI", phi_psi_LAMBDA0B_to_JPSI_P_PI);
    f_cos->GetObject("phi_psi_BD_to_JPSI_K_K", phi_psi_BD_to_JPSI_K_K);
    f_cos->GetObject("phi_psi_BS_to_JPSI_K_PI", phi_psi_BS_to_JPSI_K_PI);
    
    f_cos->GetObject("phi_K_data", phi_K_data);
    f_cos->GetObject("phi_K_LAMBDA0B_to_JPSI_P_K", phi_K_LAMBDA0B_to_JPSI_P_K);
    f_cos->GetObject("phi_K_BS_to_JPSI_PI_PI", phi_K_BS_to_JPSI_PI_PI);
    f_cos->GetObject("phi_K_BD_to_JPSI_PI_PI", phi_K_BD_to_JPSI_PI_PI);
    f_cos->GetObject("phi_K_BD_to_JPSI_K_PI", phi_K_BD_to_JPSI_K_PI);
    f_cos->GetObject("phi_K_BS_to_JPSI_K_K", phi_K_BS_to_JPSI_K_K);
    f_cos->GetObject("phi_K_LAMBDA0B_to_JPSI_P_PI", phi_K_LAMBDA0B_to_JPSI_P_PI);
    f_cos->GetObject("phi_K_BD_to_JPSI_K_K", phi_K_BD_to_JPSI_K_K);
    f_cos->GetObject("phi_K_BS_to_JPSI_K_PI", phi_K_BS_to_JPSI_K_PI);
    
    f_cos->GetObject("alpha_mu_data", alpha_mu_data);
    f_cos->GetObject("alpha_mu_LAMBDA0B_to_JPSI_P_K", alpha_mu_LAMBDA0B_to_JPSI_P_K);
    f_cos->GetObject("alpha_mu_BS_to_JPSI_PI_PI", alpha_mu_BS_to_JPSI_PI_PI);
    f_cos->GetObject("alpha_mu_BD_to_JPSI_PI_PI", alpha_mu_BD_to_JPSI_PI_PI);
    f_cos->GetObject("alpha_mu_BD_to_JPSI_K_PI", alpha_mu_BD_to_JPSI_K_PI);
    f_cos->GetObject("alpha_mu_BS_to_JPSI_K_K", alpha_mu_BS_to_JPSI_K_K);
    f_cos->GetObject("alpha_mu_LAMBDA0B_to_JPSI_P_PI", alpha_mu_LAMBDA0B_to_JPSI_P_PI);
    f_cos->GetObject("alpha_mu_BD_to_JPSI_K_K", alpha_mu_BD_to_JPSI_K_K);
    f_cos->GetObject("alpha_mu_BS_to_JPSI_K_PI", alpha_mu_BS_to_JPSI_K_PI);
    
    f_cos->GetObject("phi_mu_Kstar_data", phi_mu_Kstar_data);
    f_cos->GetObject("phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K", phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K);
    f_cos->GetObject("phi_mu_Kstar_BS_to_JPSI_PI_PI", phi_mu_Kstar_BS_to_JPSI_PI_PI);
    f_cos->GetObject("phi_mu_Kstar_BD_to_JPSI_PI_PI", phi_mu_Kstar_BD_to_JPSI_PI_PI);
    f_cos->GetObject("phi_mu_Kstar_BD_to_JPSI_K_PI", phi_mu_Kstar_BD_to_JPSI_K_PI);
    f_cos->GetObject("phi_mu_Kstar_BS_to_JPSI_K_K", phi_mu_Kstar_BS_to_JPSI_K_K);
    f_cos->GetObject("phi_mu_Kstar_LAMBDA0B_to_JPSI_P_PI", phi_mu_Kstar_LAMBDA0B_to_JPSI_P_PI);
    f_cos->GetObject("phi_mu_Kstar_BD_to_JPSI_K_K", phi_mu_Kstar_BD_to_JPSI_K_K);
    f_cos->GetObject("phi_mu_Kstar_BS_to_JPSI_K_PI", phi_mu_Kstar_BS_to_JPSI_K_PI);
    
    f_cos->GetObject("phi_mu_X_data", phi_mu_X_data);
    f_cos->GetObject("phi_mu_X_LAMBDA0B_to_JPSI_P_K", phi_mu_X_LAMBDA0B_to_JPSI_P_K);
    f_cos->GetObject("phi_mu_X_BS_to_JPSI_PI_PI", phi_mu_X_BS_to_JPSI_PI_PI);
    f_cos->GetObject("phi_mu_X_BD_to_JPSI_PI_PI", phi_mu_X_BD_to_JPSI_PI_PI);
    f_cos->GetObject("phi_mu_X_BD_to_JPSI_K_PI", phi_mu_X_BD_to_JPSI_K_PI);
    f_cos->GetObject("phi_mu_X_BS_to_JPSI_K_K", phi_mu_X_BS_to_JPSI_K_K);
    f_cos->GetObject("phi_mu_X_LAMBDA0B_to_JPSI_P_PI", phi_mu_X_LAMBDA0B_to_JPSI_P_PI);
    f_cos->GetObject("phi_mu_X_BD_to_JPSI_K_K", phi_mu_X_BD_to_JPSI_K_K);
    f_cos->GetObject("phi_mu_X_BS_to_JPSI_K_PI", phi_mu_X_BS_to_JPSI_K_PI);
    
    f_cos->GetObject("cos_theta_psi_X_data", cos_theta_psi_X_data);
    f_cos->GetObject("cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K", cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K);
    f_cos->GetObject("cos_theta_psi_X_BS_to_JPSI_PI_PI", cos_theta_psi_X_BS_to_JPSI_PI_PI);
    f_cos->GetObject("cos_theta_psi_X_BD_to_JPSI_PI_PI", cos_theta_psi_X_BD_to_JPSI_PI_PI);
    f_cos->GetObject("cos_theta_psi_X_BD_to_JPSI_K_PI", cos_theta_psi_X_BD_to_JPSI_K_PI);
    f_cos->GetObject("cos_theta_psi_X_BS_to_JPSI_K_K", cos_theta_psi_X_BS_to_JPSI_K_K);
    f_cos->GetObject("cos_theta_psi_X_LAMBDA0B_to_JPSI_P_PI", cos_theta_psi_X_LAMBDA0B_to_JPSI_P_PI);
    f_cos->GetObject("cos_theta_psi_X_BD_to_JPSI_K_K", cos_theta_psi_X_BD_to_JPSI_K_K);
    f_cos->GetObject("cos_theta_psi_X_BS_to_JPSI_K_PI", cos_theta_psi_X_BS_to_JPSI_K_PI);
    
    f_cos->GetObject("cos_theta_Kstar_data", cos_theta_Kstar_data);
    f_cos->GetObject("cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K", cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K);
    f_cos->GetObject("cos_theta_Kstar_BS_to_JPSI_PI_PI", cos_theta_Kstar_BS_to_JPSI_PI_PI);
    f_cos->GetObject("cos_theta_Kstar_BD_to_JPSI_PI_PI", cos_theta_Kstar_BD_to_JPSI_PI_PI);
    f_cos->GetObject("cos_theta_Kstar_BD_to_JPSI_K_PI", cos_theta_Kstar_BD_to_JPSI_K_PI);
    f_cos->GetObject("cos_theta_Kstar_BS_to_JPSI_K_K", cos_theta_Kstar_BS_to_JPSI_K_K);
    f_cos->GetObject("cos_theta_Kstar_LAMBDA0B_to_JPSI_P_PI", cos_theta_Kstar_LAMBDA0B_to_JPSI_P_PI);
    f_cos->GetObject("cos_theta_Kstar_BD_to_JPSI_K_K", cos_theta_Kstar_BD_to_JPSI_K_K);
    f_cos->GetObject("cos_theta_Kstar_BS_to_JPSI_K_PI", cos_theta_Kstar_BS_to_JPSI_K_PI);
    
    f_cos->GetObject("cos_theta_psi_Kstar_data", cos_theta_psi_Kstar_data);
    f_cos->GetObject("cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K", cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K);
    f_cos->GetObject("cos_theta_psi_Kstar_BS_to_JPSI_PI_PI", cos_theta_psi_Kstar_BS_to_JPSI_PI_PI);
    f_cos->GetObject("cos_theta_psi_Kstar_BD_to_JPSI_PI_PI", cos_theta_psi_Kstar_BD_to_JPSI_PI_PI);
    f_cos->GetObject("cos_theta_psi_Kstar_BD_to_JPSI_K_PI", cos_theta_psi_Kstar_BD_to_JPSI_K_PI);
    f_cos->GetObject("cos_theta_psi_Kstar_BS_to_JPSI_K_K", cos_theta_psi_Kstar_BS_to_JPSI_K_K);
    f_cos->GetObject("cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_PI", cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_PI);
    f_cos->GetObject("cos_theta_psi_Kstar_BD_to_JPSI_K_K", cos_theta_psi_Kstar_BD_to_JPSI_K_K);
    f_cos->GetObject("cos_theta_psi_Kstar_BS_to_JPSI_K_PI", cos_theta_psi_Kstar_BS_to_JPSI_K_PI);
    
    TFile *f_cos_bg = new TFile("ROOT_files/cos_signal_bg.root");
    
    f_cos_bg->GetObject("cos_theta_Zc_bg", cos_theta_Zc_bg);
    f_cos_bg->GetObject("cos_theta_B0_bg", cos_theta_B0_bg);
    f_cos_bg->GetObject("phi_psi_bg", phi_psi_bg);
    f_cos_bg->GetObject("phi_K_bg", phi_K_bg);
    f_cos_bg->GetObject("alpha_mu_bg", alpha_mu_bg);
    f_cos_bg->GetObject("phi_mu_Kstar_bg", phi_mu_Kstar_bg);
    f_cos_bg->GetObject("phi_mu_X_bg", phi_mu_X_bg);
    f_cos_bg->GetObject("cos_theta_psi_X_bg", cos_theta_psi_X_bg);
    f_cos_bg->GetObject("cos_theta_Kstar_bg", cos_theta_Kstar_bg);
    f_cos_bg->GetObject("cos_theta_psi_Kstar_bg", cos_theta_psi_Kstar_bg);
    
    TFile *f_phs = new TFile("ROOT_files/phase_space.root");
    
    TVectorD *vcoef = (TVectorD*)f_phs->Get("counters");
    
    coef_4_to_s_LAMBDA0B_to_JPSI_P_K = (*vcoef)(0);
    coef_4_to_s_BD_to_JPSI_PI_PI = (*vcoef)(1);
    coef_4_to_s_BS_to_JPSI_PI_PI = (*vcoef)(2);
    coef_4_to_s_LAMBDA0B_to_JPSI_P_PI = (*vcoef)(3);
    coef_4_to_s_BD_to_JPSI_K_K = (*vcoef)(4);
    coef_4_to_s_BS_to_JPSI_K_PI = (*vcoef)(5);
    coef_4_to_c_LAMBDA0B_to_JPSI_P_K = (*vcoef)(6);
    coef_4_to_c_BD_to_JPSI_PI_PI = (*vcoef)(7);
    coef_4_to_c_BS_to_JPSI_PI_PI = (*vcoef)(8);
    coef_4_to_c_LAMBDA0B_to_JPSI_P_PI = (*vcoef)(9);
    coef_4_to_c_BD_to_JPSI_K_K = (*vcoef)(10);
    coef_4_to_c_BS_to_JPSI_K_PI = (*vcoef)(11);
    coef_4_to_clb_LAMBDA0B_to_JPSI_P_K = (*vcoef)(12);
    coef_4_to_clb_BD_to_JPSI_PI_PI = (*vcoef)(13);
    coef_4_to_clb_BS_to_JPSI_PI_PI = (*vcoef)(14);
    coef_4_to_clb_LAMBDA0B_to_JPSI_P_PI = (*vcoef)(15);
    coef_4_to_clb_BD_to_JPSI_K_K = (*vcoef)(16);
    coef_4_to_clb_BS_to_JPSI_K_PI = (*vcoef)(17);
    
    float muon1_px, muon1_py, muon1_pz, muon2_px, muon2_py, muon2_pz, kaon_px, kaon_py, kaon_pz, pion_px, pion_py, pion_pz, 
        jpsipi1, jpsipi2, jpsiK1, jpsiK2, K1pi2, K2pi1, w_dimuon, K1K2, jpsiKpi, jpsipiK, jpsiKK, jpsipipi, jpsipK, jpsiKp, p1K2, p2K1, jpsip1, jpsip2, jpsippi, jpsipip, 
        cos_theta_Zc_Kpi, cos_theta_Zc_piK,
        phi_K_Kpi, phi_K_piK,
        alpha_mu_Kpi, alpha_mu_piK,
        phi_mu_Kstar_Kpi, phi_mu_Kstar_piK,
        phi_mu_X_Kpi, phi_mu_X_piK,
        cos_theta_psi_X_Kpi, cos_theta_psi_X_piK,
        cos_theta_Kstar_Kpi, cos_theta_Kstar_piK,
        cos_theta_psi_Kstar_Kpi, cos_theta_psi_Kstar_piK,
        cos_theta_B0_Kpi, cos_theta_B0_piK,
        phi_psi_Kpi, phi_psi_piK;
    int signal, control, controlLb;
    TTree *tree = (TTree*)f_phs->Get("phs_BD_to_JPSI_K_PI");
    Long64_t nentries = tree->GetEntries();
    
    tree->SetBranchAddress("mu1_px", &muon1_px);
    tree->SetBranchAddress("mu1_py", &muon1_py);
    tree->SetBranchAddress("mu1_pz", &muon1_pz);
    tree->SetBranchAddress("mu2_px", &muon2_px);
    tree->SetBranchAddress("mu2_py", &muon2_py);
    tree->SetBranchAddress("mu2_pz", &muon2_pz);
    tree->SetBranchAddress("K_px", &kaon_px);
    tree->SetBranchAddress("K_py", &kaon_py);
    tree->SetBranchAddress("K_pz", &kaon_pz);
    tree->SetBranchAddress("pi_px", &pion_px);
    tree->SetBranchAddress("pi_py", &pion_py);
    tree->SetBranchAddress("pi_pz", &pion_pz);
    tree->SetBranchAddress("jpsipi1", &jpsipi1);
    tree->SetBranchAddress("jpsipi2", &jpsipi2);
    tree->SetBranchAddress("jpsiK1", &jpsiK1);
    tree->SetBranchAddress("jpsiK2", &jpsiK2);
    tree->SetBranchAddress("K1pi2", &K1pi2);
    tree->SetBranchAddress("K2pi1", &K2pi1);
    tree->SetBranchAddress("w_dimuon", &w_dimuon);
    tree->SetBranchAddress("signal", &signal);
    tree->SetBranchAddress("control", &control);
    tree->SetBranchAddress("K1K2", &K1K2);
    tree->SetBranchAddress("jpsiKpi", &jpsiKpi);
    tree->SetBranchAddress("jpsipiK", &jpsipiK);
    tree->SetBranchAddress("jpsiKK", &jpsiKK);
    tree->SetBranchAddress("jpsipipi", &jpsipipi);
    tree->SetBranchAddress("jpsipK", &jpsipK);
    tree->SetBranchAddress("jpsiKp", &jpsiKp);
    tree->SetBranchAddress("controlLb", &controlLb);
    tree->SetBranchAddress("jpsip1", &jpsip1);
    tree->SetBranchAddress("jpsip2", &jpsip2);
    tree->SetBranchAddress("p1K2", &p1K2);
    tree->SetBranchAddress("p2K1", &p2K1);
    tree->SetBranchAddress("cos_theta_Zc_Kpi", &cos_theta_Zc_Kpi);
    tree->SetBranchAddress("cos_theta_Zc_piK", &cos_theta_Zc_piK);
    tree->SetBranchAddress("phi_K_Kpi", &phi_K_Kpi);
    tree->SetBranchAddress("phi_K_piK", &phi_K_piK);
    tree->SetBranchAddress("alpha_mu_Kpi", &alpha_mu_Kpi);
    tree->SetBranchAddress("alpha_mu_piK", &alpha_mu_piK);
    tree->SetBranchAddress("phi_mu_Kstar_Kpi", &phi_mu_Kstar_Kpi);
    tree->SetBranchAddress("phi_mu_Kstar_piK", &phi_mu_Kstar_piK);
    tree->SetBranchAddress("phi_mu_X_Kpi", &phi_mu_X_Kpi);
    tree->SetBranchAddress("phi_mu_X_piK", &phi_mu_X_piK);
    tree->SetBranchAddress("cos_theta_psi_X_Kpi", &cos_theta_psi_X_Kpi);
    tree->SetBranchAddress("cos_theta_psi_X_piK", &cos_theta_psi_X_piK);
    tree->SetBranchAddress("cos_theta_Kstar_Kpi", &cos_theta_Kstar_Kpi);
    tree->SetBranchAddress("cos_theta_Kstar_piK", &cos_theta_Kstar_piK);
    tree->SetBranchAddress("cos_theta_psi_Kstar_Kpi", &cos_theta_psi_Kstar_Kpi);
    tree->SetBranchAddress("cos_theta_psi_Kstar_piK", &cos_theta_psi_Kstar_piK);
    tree->SetBranchAddress("cos_theta_B0_Kpi", &cos_theta_B0_Kpi);
    tree->SetBranchAddress("cos_theta_B0_piK", &cos_theta_B0_piK);
    tree->SetBranchAddress("phi_psi_Kpi", &phi_psi_Kpi);
    tree->SetBranchAddress("phi_psi_piK", &phi_psi_piK);
    tree->SetBranchAddress("jpsippi", &jpsippi);
    tree->SetBranchAddress("jpsipip", &jpsipip);
    
    TLorentzVector mu1, mu2, pion, kaon, psi, Zc, Bd, Kstar, kaon1, kaon2, Bs, f, proton, Pc, Lstar, Lb;
    
    std::complex<float> ix(0,1);
    
    for(int i = 1; i <= nentries; i++)
    {
        tree->GetEntry(i);
        
        mu1.SetXYZM(muon1_px, muon1_py, muon1_pz, 105.65837);
        mu2.SetXYZM(muon2_px, muon2_py, muon2_pz, 105.65837);
        pion.SetXYZM(pion_px, pion_py, pion_pz, 139.57);
        kaon.SetXYZM(kaon_px, kaon_py, kaon_pz, 493.677);
        
        psi = mu1 + mu2;
        Zc = psi + pion;
        Bd = psi + pion + kaon;
        Kstar = pion + kaon;
        
        Jpsipi1_mass[i-1] = jpsipi1;
        Jpsipi2_mass[i-1] = jpsipi2;
        JpsiK1_mass[i-1] = jpsiK1;
        JpsiK2_mass[i-1] = jpsiK2;
        K1pi2_mass[i-1] = K1pi2;
        K2pi1_mass[i-1] = K2pi1;
        Kpi_truth[i-1] = (kaon + pion).M();
        Jpsipi_truth[i-1] = (mu1 + mu2 + pion).M();
        p_pi[i-1] = (pion.Vect()).Mag();
        p_K[i-1] = (kaon.Vect()).Mag();
        phi_K_a[i-1] = phi_K(Bd, Kstar, kaon, psi);
        alpha_mu_a[i-1] = alpha_mu(mu2, psi, Zc, Bd);
        phi_mu_Kstar_a[i-1] = phi_mu_Kstar(Bd, Kstar, psi, mu2);
        theta_psi_X_a[i-1] = theta_psi_X(Zc, psi, mu2);
        phi_mu_X_a[i-1] = phi_mu_X(Zc, Bd, psi, mu2);
        theta_X_a[i-1] = theta_X(Bd, Zc, psi);
        theta_Kstar_a[i-1] = theta_Kstar( Bd, Kstar, kaon );
        theta_psi_Kstar_a[i-1] = theta_psi_Kstar(Bd, psi, mu2);
        w_dimuon_a[i - 1] = w_dimuon;
        JpsiKpi_mass[i - 1] = jpsiKpi;
        signal_a[i - 1] = signal;
        control_a[i - 1] = control;
        KK_mass[i - 1] = K1K2;
        JpsiKpi_mass[i - 1] = jpsiKpi;
        JpsipiK_mass[i - 1] = jpsipiK;
        JpsiKK_mass[i - 1] = jpsiKK;
        Jpsipipi_mass[i - 1] = jpsipipi;
        JpsipK_mass[i - 1] = jpsipK;
        JpsiKp_mass[i - 1] = jpsiKp;
        Jpsippi_mass[i - 1] = jpsippi;
        Jpsipip_mass[i - 1] = jpsipip;
        controlLb_a[i - 1] = controlLb;
        Jpsip1_mass[i - 1] = jpsip1;
        Jpsip2_mass[i - 1] = jpsip2;
        p1K2_mass[i - 1] = p1K2;
        p2K1_mass[i - 1] = p2K1;
        cos_theta_Zc_Kpi_Bd[i - 1] = cos_theta_Zc_Kpi;
        cos_theta_Zc_piK_Bd[i - 1] = cos_theta_Zc_piK;
        phi_K_Kpi_Bd[i - 1] = phi_K_Kpi;
        phi_K_piK_Bd[i - 1] = phi_K_piK;
        alpha_mu_Kpi_Bd[i - 1] = alpha_mu_Kpi;
        alpha_mu_piK_Bd[i - 1] = alpha_mu_piK;
        phi_mu_Kstar_Kpi_Bd[i - 1] = phi_mu_Kstar_Kpi;
        phi_mu_Kstar_piK_Bd[i - 1] = phi_mu_Kstar_piK;
        phi_mu_X_Kpi_Bd[i - 1] = phi_mu_X_Kpi;
        phi_mu_X_piK_Bd[i - 1] = phi_mu_X_piK;
        cos_theta_psi_X_Kpi_Bd[i - 1] = cos_theta_psi_X_Kpi;
        cos_theta_psi_X_piK_Bd[i - 1] = cos_theta_psi_X_piK;
        cos_theta_Kstar_Kpi_Bd[i - 1] = cos_theta_Kstar_Kpi;
        cos_theta_Kstar_piK_Bd[i - 1] = cos_theta_Kstar_piK;
        cos_theta_psi_Kstar_Kpi_Bd[i - 1] = cos_theta_psi_Kstar_Kpi;
        cos_theta_psi_Kstar_piK_Bd[i - 1] = cos_theta_psi_Kstar_piK;
        cos_theta_B0_Kpi_Bd[i - 1] = cos_theta_B0_Kpi;
        cos_theta_B0_piK_Bd[i - 1] = cos_theta_B0_piK;
        phi_psi_Kpi_Bd[i - 1] = phi_psi_Kpi;
        phi_psi_piK_Bd[i - 1] = phi_psi_piK;
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1410_1_1_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.421, 5.27964, 0.236, 0, 1) * ((float)(-1.) *(float)(sqrt(1.0/3.0))* (float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 0.0, 0.0, theta_Kstar_a[i-1]) + (float)(sqrt(1.0/3.0))* (float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 1.0, 0.0, theta_Kstar_a[i-1]) * ((float)cos(phi_mu_Kstar_a[i-1]) + ix*(float)sin(phi_mu_Kstar_a[i-1])) * ((float)cos(phi_K_a[i-1]) + ix*(float)sin(phi_K_a[i-1])) + (float)(sqrt(1.0/3.0))*(float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1])*(float)Wigner_matrix(1.0, -1.0, 0.0, theta_Kstar_a[i-1])*((float)cos(phi_mu_Kstar_a[i-1]) - ix*(float)sin(phi_mu_Kstar_a[i-1]))*((float)cos(phi_K_a[i-1]) - ix*(float)sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1410_1_1_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.421, 5.27964, 0.236, 0, 1) * ((float)(-1.) *(float)(sqrt(1.0/3.0))* (float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 0.0, 0.0, theta_Kstar_a[i-1]) + (float)(sqrt(1.0/3.0))* (float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(1.0/3.0))*(float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1])*(float)Wigner_matrix(1.0, -1.0, 0.0, theta_Kstar_a[i-1])*(cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1]))*(cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1410_1_2_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.421, 5.27964, 0.236, 1, 1) * ((float)(-1.)*(float)(sqrt(1.0/2.0))*(float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(1.0/2.0))*(float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1])*(float)Wigner_matrix(1.0, -1.0, 0.0, theta_Kstar_a[i-1])*(cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1]))*(cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1410_1_2_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.421, 5.27964, 0.236, 1, 1) * ((float)(-1.)*(float)(sqrt(1.0/2.0))*(float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(1.0/2.0))*(float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1])*(float)Wigner_matrix(1.0, -1.0, 0.0, theta_Kstar_a[i-1])*(cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1]))*(cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1410_1_3_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.421, 5.27964, 0.236, 2, 1) * ((float)(sqrt(2.0/3.0))*(float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 0.0, 0.0, theta_Kstar_a[i-1]) + (float)(sqrt(1.0/6.0))*(float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(1.0/6.0))*(float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1])*(float)Wigner_matrix(1.0, -1.0, 0.0, theta_Kstar_a[i-1])*(cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1]))*(cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1410_1_3_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.421, 5.27964, 0.236, 2, 1) * ((float)(sqrt(2.0/3.0))*(float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 0.0, 0.0, theta_Kstar_a[i-1]) + (float)(sqrt(1.0/6.0))*(float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(1.0/6.0))*(float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1])*(float)Wigner_matrix(1.0, -1.0, 0.0, theta_Kstar_a[i-1])*(cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1]))*(cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1430_0_1_minus[i-1] = (float)(-1.) * BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.425, 5.27964, 0.270, 1, 0) * (float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]);
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1430_0_1_plus[i-1] = (float)(-1.) * BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.425, 5.27964, 0.270, 1, 0) * (float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]);
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1430_2_1_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.4324, 5.27964, 0.109, 1, 2) * ((float)(sqrt(2.0/5.0)) * (float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 0.0, 0.0, theta_Kstar_a[i-1]) -(float)(sqrt(3.0/10.0))* (float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) -(float)(sqrt(3.0/10.0))*(float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1430_2_1_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.4324, 5.27964, 0.109, 1, 2) * ((float)(sqrt(2.0/5.0)) * (float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 0.0, 0.0, theta_Kstar_a[i-1]) -(float)(sqrt(3.0/10.0))* (float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) -(float)(sqrt(3.0/10.0))*(float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1430_2_2_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.4324, 5.27964, 0.109, 2, 2) * ((float)(sqrt(1.0/2.0))* (float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(sqrt(1.0/2.0))* (float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1430_2_2_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.4324, 5.27964, 0.109, 2, 2) * ((float)(sqrt(1.0/2.0))* (float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(sqrt(1.0/2.0))* (float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1430_2_3_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.4324, 5.27964, 0.109, 3, 2) * ((float)(-1.) * (float)(sqrt(3.0/5.0))*(float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 0.0, 0.0, theta_Kstar_a[i-1]) - (float)(sqrt(1.0/5.0))*(float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(sqrt(1.0/5.0))*(float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1430_2_3_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.4324, 5.27964, 0.109, 3, 2) * ((float)(-1.) * (float)(sqrt(3.0/5.0))*(float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 0.0, 0.0, theta_Kstar_a[i-1]) - (float)(sqrt(1.0/5.0))*(float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(sqrt(1.0/5.0))*(float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1680_1_1_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.718, 5.27964, 0.322, 0, 1) * ((float)(-1.) *(float)(sqrt(1.0/3.0))* (float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 0.0, 0.0, theta_Kstar_a[i-1]) + (float)(sqrt(1.0/3.0))* (float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(1.0/3.0))*(float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1])*(float)Wigner_matrix(1.0, -1.0, 0.0, theta_Kstar_a[i-1])*(cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1]))*(cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1680_1_1_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.718, 5.27964, 0.322, 0, 1) * ((float)(-1.) *(float)(sqrt(1.0/3.0))* (float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 0.0, 0.0, theta_Kstar_a[i-1]) + (float)(sqrt(1.0/3.0))* (float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(1.0/3.0))*(float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1])*(float)Wigner_matrix(1.0, -1.0, 0.0, theta_Kstar_a[i-1])*(cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1]))*(cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1680_1_2_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.718, 5.27964, 0.322, 1, 1) * ((float)(-1.)*(float)(sqrt(1.0/2.0))*(float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(1.0/2.0))*(float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1])*(float)Wigner_matrix(1.0, -1.0, 0.0, theta_Kstar_a[i-1])*(cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1]))*(cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1680_1_2_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.718, 5.27964, 0.322, 1, 1) * ((float)(-1.)*(float)(sqrt(1.0/2.0))*(float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(1.0/2.0))*(float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1])*(float)Wigner_matrix(1.0, -1.0, 0.0, theta_Kstar_a[i-1])*(cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1]))*(cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1680_1_3_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.718, 5.27964, 0.322, 2, 1) * ((float)(sqrt(2.0/3.0))*(float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 0.0, 0.0, theta_Kstar_a[i-1]) + (float)(sqrt(1.0/6.0))*(float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(1.0/6.0))*(float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1])*(float)Wigner_matrix(1.0, -1.0, 0.0, theta_Kstar_a[i-1])*(cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1]))*(cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1680_1_3_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.718, 5.27964, 0.322, 2, 1) * ((float)(sqrt(2.0/3.0))*(float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 0.0, 0.0, theta_Kstar_a[i-1]) + (float)(sqrt(1.0/6.0))*(float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(1.0/6.0))*(float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1])*(float)Wigner_matrix(1.0, -1.0, 0.0, theta_Kstar_a[i-1])*(cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1]))*(cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1780_3_1_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.776, 5.27964, 0.159, 2, 3) * ((float)(-1.) * (float)(sqrt(3.0/7.0))*(float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, 0.0, 0.0, theta_Kstar_a[i-1]) + (float)(sqrt(2.0/7.0))* (float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(2.0/7.0))*(float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1780_3_1_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.776, 5.27964, 0.159, 2, 3) * ((float)(-1.) * (float)(sqrt(3.0/7.0))*(float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, 0.0, 0.0, theta_Kstar_a[i-1]) + (float)(sqrt(2.0/7.0))* (float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(2.0/7.0))*(float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1780_3_2_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.776, 5.27964, 0.159, 3, 3) * ((float)(-1.) * (float)(sqrt(1.0/2.0))*(float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(1.0/2.0))*(float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1780_3_2_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.776, 5.27964, 0.159, 3, 3) * ((float)(-1.) * (float)(sqrt(1.0/2.0))*(float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(1.0/2.0))*(float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1780_3_3_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.776, 5.27964, 0.159, 4, 3) * ((float)(sqrt(4.0/7.0))*(float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, 0.0, 0.0, theta_Kstar_a[i-1]) + (float)(sqrt(3.0/14.0))* (float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(3.0/14.0))* (float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1780_3_3_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.776, 5.27964, 0.159, 4, 3) * ((float)(sqrt(4.0/7.0))*(float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, 0.0, 0.0, theta_Kstar_a[i-1]) + (float)(sqrt(3.0/14.0))* (float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(3.0/14.0))* (float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1950_0_1_minus[i-1] = (float)(-1.) * BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.945, 5.27964, 0.201, 1, 0) * (float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]);
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1950_0_1_plus[i-1] = (float)(-1.) * BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.945, 5.27964, 0.201, 1, 0) * (float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]);
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1980_2_1_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.974, 5.27964, 0.376, 1, 2) * ((float)(sqrt(2.0/5.0)) * (float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 0.0, 0.0, theta_Kstar_a[i-1]) -(float)(sqrt(3.0/10.0))* (float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) -(float)(sqrt(3.0/10.0))*(float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1980_2_1_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.974, 5.27964, 0.376, 1, 2) * ((float)(sqrt(2.0/5.0)) * (float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 0.0, 0.0, theta_Kstar_a[i-1]) -(float)(sqrt(3.0/10.0))* (float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) -(float)(sqrt(3.0/10.0))*(float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1980_2_2_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.974, 5.27964, 0.376, 2, 2) * ((float)(sqrt(1.0/2.0))* (float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(sqrt(1.0/2.0))* (float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1980_2_2_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.974, 5.27964, 0.376, 2, 2) * ((float)(sqrt(1.0/2.0))* (float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(sqrt(1.0/2.0))* (float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1980_2_3_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.974, 5.27964, 0.376, 3, 2) * ((float)(-1.) * (float)(sqrt(3.0/5.0))*(float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 0.0, 0.0, theta_Kstar_a[i-1]) - (float)(sqrt(1.0/5.0))*(float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(sqrt(1.0/5.0))*(float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1980_2_3_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.974, 5.27964, 0.376, 3, 2) * ((float)(-1.) * (float)(sqrt(3.0/5.0))*(float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 0.0, 0.0, theta_Kstar_a[i-1]) - (float)(sqrt(1.0/5.0))*(float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(sqrt(1.0/5.0))*(float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK2045_4_1_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 2.045, 5.27964, 0.198, 3, 4) * ((float)(sqrt(4.0/9.0))*(float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, 0.0, 0.0, theta_Kstar_a[i-1]) - (float)(1.0/3.0*sqrt(5.0/2.0))* (float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(1.0/3.0*sqrt(5.0/2.0))* (float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK2045_4_1_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 2.045, 5.27964, 0.198, 3, 4) * ((float)(sqrt(4.0/9.0))*(float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, 0.0, 0.0, theta_Kstar_a[i-1]) - (float)(1.0/3.0*sqrt(5.0/2.0))* (float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(1.0/3.0*sqrt(5.0/2.0))* (float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK2045_4_2_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 2.045, 5.27964, 0.198, 4, 4) * ((float)(sqrt(1.0/2.0)) * (float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(sqrt(1.0/2.0)) * (float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK2045_4_2_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 2.045, 5.27964, 0.198, 4, 4) * ((float)(sqrt(1.0/2.0)) * (float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(sqrt(1.0/2.0)) * (float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK2045_4_3_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 2.045, 5.27964, 0.198, 5, 4) * ((float)(-1.) * (float)(sqrt(5.0/9.0))* (float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, 0.0, 0.0, theta_Kstar_a[i-1]) - (float)(sqrt(2.0/9.0))* (float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(sqrt(2.0/9.0))*(float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK2045_4_3_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 2.045, 5.27964, 0.198, 5, 4) * ((float)(-1.) * (float)(sqrt(5.0/9.0))* (float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, 0.0, 0.0, theta_Kstar_a[i-1]) - (float)(sqrt(2.0/9.0))* (float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(sqrt(2.0/9.0))*(float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        weight[i-1] = HamplitudeXKstar_total_accelerated(Jpsipi_truth[i-1] / 1000., Kpi_truth[i-1] / 1000., 5.27964, (*vpar)(45), 4.43, (*vpar)(44), 0.181,
                                    std::complex<float>((*vpar)(40) * cos((*vpar)(41)), (*vpar)(40) * sin((*vpar)(41))), 
                                    std::complex<float>((*vpar)(42) * cos((*vpar)(43)), (*vpar)(42) * sin((*vpar)(43))),
                                    std::complex<float>(0., 0.), 
                                    std::complex<float>(0., 0.),
                                    std::complex<float>((*vpar)(0) * cos((*vpar)(1)), (*vpar)(0) * sin((*vpar)(1))), 
                                    std::complex<float>((*vpar)(16) * cos((*vpar)(17)), (*vpar)(16) * sin((*vpar)(17))), 
                                    std::complex<float>((*vpar)(18) * cos((*vpar)(19)), (*vpar)(18) * sin((*vpar)(19))),
                                    std::complex<float>((*vpar)(2) * cos((*vpar)(3)), (*vpar)(2) * sin((*vpar)(3))), 
                                    std::complex<float>(0., 0.),
                                    std::complex<float>(0., 0.),
                                    std::complex<float>((*vpar)(4) * cos((*vpar)(5)), (*vpar)(4) * sin((*vpar)(5))), 
                                    std::complex<float>((*vpar)(20) * cos((*vpar)(21)), (*vpar)(20) * sin((*vpar)(21))), 
                                    std::complex<float>((*vpar)(22) * cos((*vpar)(23)), (*vpar)(22) * sin((*vpar)(23))),
                                    std::complex<float>((*vpar)(6) * cos((*vpar)(7)), (*vpar)(6) * sin((*vpar)(7))), 
                                    std::complex<float>((*vpar)(24) * cos((*vpar)(25)), (*vpar)(24) * sin((*vpar)(25))), 
                                    std::complex<float>((*vpar)(26) * cos((*vpar)(27)), (*vpar)(26) * sin((*vpar)(27))),
                                    std::complex<float>((*vpar)(8) * cos((*vpar)(9)), (*vpar)(9) * sin((*vpar)(9))), 
                                    std::complex<float>((*vpar)(28) * cos((*vpar)(29)), (*vpar)(28) * sin((*vpar)(29))), 
                                    std::complex<float>((*vpar)(30) * cos((*vpar)(31)), (*vpar)(30) * sin((*vpar)(31))),
                                    std::complex<float>((*vpar)(10) * cos((*vpar)(11)), (*vpar)(10) * sin((*vpar)(11))), 
                                    std::complex<float>(0., 0.), 
                                    std::complex<float>(0., 0.),
                                    std::complex<float>((*vpar)(14) * cos((*vpar)(15)), (*vpar)(14) * sin((*vpar)(15))),
                                    std::complex<float>((*vpar)(32) * cos((*vpar)(33)), (*vpar)(32) * sin((*vpar)(33))),
                                    std::complex<float>((*vpar)(34) * cos((*vpar)(35)), (*vpar)(34) * sin((*vpar)(35))),
                                    std::complex<float>((*vpar)(12) * cos((*vpar)(13)), (*vpar)(12) * sin((*vpar)(13))),
                                    std::complex<float>((*vpar)(36) * cos((*vpar)(37)), (*vpar)(36) * sin((*vpar)(37))),
                                    std::complex<float>((*vpar)(38) * cos((*vpar)(39)), (*vpar)(38) * sin((*vpar)(39))),
                                    std::complex<float>(buf_amp * cos(buf_phi), buf_amp * sin(buf_phi)),
                                    theta_X_a[i-1], theta_Kstar_a[i-1], theta_psi_X_a[i-1], theta_psi_Kstar_a[i-1], phi_mu_X_a[i-1], phi_mu_Kstar_a[i-1], 
                                    phi_K_a[i-1], alpha_mu_a[i-1],
                                    BWK1410_1_1_minus[i-1], BWK1410_1_2_minus[i-1], BWK1410_1_3_minus[i-1], 
                                    BWK1430_0_1_minus[i-1], 
                                    BWK1430_2_1_minus[i-1], BWK1430_2_2_minus[i-1], BWK1430_2_3_minus[i-1], 
                                    BWK1680_1_1_minus[i-1], BWK1680_1_2_minus[i-1], BWK1680_1_3_minus[i-1], 
                                    BWK1780_3_1_minus[i-1], BWK1780_3_2_minus[i-1], BWK1780_3_3_minus[i-1], 
                                    BWK1950_0_1_minus[i-1], 
                                    BWK1980_2_1_minus[i-1], BWK1980_2_2_minus[i-1], BWK1980_2_3_minus[i-1], 
                                    BWK2045_4_1_minus[i-1], BWK2045_4_2_minus[i-1], BWK2045_4_3_minus[i-1],
                                    BWK1410_1_1_plus[i-1], BWK1410_1_2_plus[i-1], BWK1410_1_3_plus[i-1], 
                                    BWK1430_0_1_plus[i-1], 
                                    BWK1430_2_1_plus[i-1], BWK1430_2_2_plus[i-1], BWK1430_2_3_plus[i-1], 
                                    BWK1680_1_1_plus[i-1], BWK1680_1_2_plus[i-1], BWK1680_1_3_plus[i-1], 
                                    BWK1780_3_1_plus[i-1], BWK1780_3_2_plus[i-1], BWK1780_3_3_plus[i-1], 
                                    BWK1950_0_1_plus[i-1], 
                                    BWK1980_2_1_plus[i-1], BWK1980_2_2_plus[i-1], BWK1980_2_3_plus[i-1], 
                                    BWK2045_4_1_plus[i-1], BWK2045_4_2_plus[i-1], BWK2045_4_3_plus[i-1]) * w_dimuon_a[i - 1];
    }
    
    /*Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->Scale(n_LAMBDA0B_to_JPSI_P_K / Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->Integral());
    //Jpsipi_2d_BD_to_JPSI_K_PI->Scale(n_BD_to_JPSI_K_PI / Jpsipi_2d_BD_to_JPSI_K_PI->Integral());
    Jpsipi_2d_BD_to_JPSI_PI_PI->Scale(n_BD_to_JPSI_PI_PI / Jpsipi_2d_BD_to_JPSI_PI_PI->Integral());
    Jpsipi_2d_BS_to_JPSI_PI_PI->Scale(n_BS_to_JPSI_PI_PI / Jpsipi_2d_BS_to_JPSI_PI_PI->Integral());
    Jpsipi_2d_BS_to_JPSI_K_K->Scale(n_BS_to_JPSI_K_K / Jpsipi_2d_BS_to_JPSI_K_K->Integral());
    
    JpsiK_2d_LAMBDA0B_to_JPSI_P_K->Scale(n_LAMBDA0B_to_JPSI_P_K / JpsiK_2d_LAMBDA0B_to_JPSI_P_K->Integral());
    JpsiK_2d_BD_to_JPSI_K_PI->Scale(n_BD_to_JPSI_K_PI / JpsiK_2d_BD_to_JPSI_K_PI->Integral());
    JpsiK_2d_BD_to_JPSI_PI_PI->Scale(n_BD_to_JPSI_PI_PI / JpsiK_2d_BD_to_JPSI_PI_PI->Integral());
    JpsiK_2d_BS_to_JPSI_PI_PI->Scale(n_BS_to_JPSI_PI_PI / JpsiK_2d_BS_to_JPSI_PI_PI->Integral());
    JpsiK_2d_BS_to_JPSI_K_K->Scale(n_BS_to_JPSI_K_K / JpsiK_2d_BS_to_JPSI_K_K->Integral());
    
    Kpi_2d_LAMBDA0B_to_JPSI_P_K->Scale(n_LAMBDA0B_to_JPSI_P_K / Kpi_2d_LAMBDA0B_to_JPSI_P_K->Integral());
    Kpi_2d_BD_to_JPSI_K_PI->Scale(n_BD_to_JPSI_K_PI / Kpi_2d_BD_to_JPSI_K_PI->Integral());
    Kpi_2d_BD_to_JPSI_PI_PI->Scale(n_BD_to_JPSI_PI_PI / Kpi_2d_BD_to_JPSI_PI_PI->Integral());
    Kpi_2d_BS_to_JPSI_PI_PI->Scale(n_BS_to_JPSI_PI_PI / Kpi_2d_BS_to_JPSI_PI_PI->Integral());
    Kpi_2d_BS_to_JPSI_K_K->Scale(n_BS_to_JPSI_K_K / Kpi_2d_BS_to_JPSI_K_K->Integral());*/
    
    TTree *tree_BsKK = (TTree*)f_phs->Get("phs_BS_to_JPSI_K_K");
    Long64_t nentries_BsKK = tree_BsKK->GetEntries();
    
//     cout << nentries_BsKK << endl;
    
    float kaon1_px, kaon1_py, kaon1_pz, kaon2_px, kaon2_py, kaon2_pz; 
    
    tree_BsKK->SetBranchAddress("mu1_px", &muon1_px);
    tree_BsKK->SetBranchAddress("mu1_py", &muon1_py);
    tree_BsKK->SetBranchAddress("mu1_pz", &muon1_pz);
    tree_BsKK->SetBranchAddress("mu2_px", &muon2_px);
    tree_BsKK->SetBranchAddress("mu2_py", &muon2_py);
    tree_BsKK->SetBranchAddress("mu2_pz", &muon2_pz);
    tree_BsKK->SetBranchAddress("K1_px", &kaon1_px);
    tree_BsKK->SetBranchAddress("K1_py", &kaon1_py);
    tree_BsKK->SetBranchAddress("K1_pz", &kaon1_pz);
    tree_BsKK->SetBranchAddress("K2_px", &kaon2_px);
    tree_BsKK->SetBranchAddress("K2_py", &kaon2_py);
    tree_BsKK->SetBranchAddress("K2_pz", &kaon2_pz);
    tree_BsKK->SetBranchAddress("jpsiK1", &jpsiK1);
    tree_BsKK->SetBranchAddress("jpsiK2", &jpsiK2);
    tree_BsKK->SetBranchAddress("jpsipi1", &jpsipi1);
    tree_BsKK->SetBranchAddress("jpsipi2", &jpsipi2);
    tree_BsKK->SetBranchAddress("K1pi2", &K1pi2);
    tree_BsKK->SetBranchAddress("K2pi1", &K2pi1);
    tree_BsKK->SetBranchAddress("w_dimuon", &w_dimuon);
    tree_BsKK->SetBranchAddress("signal", &signal);
    tree_BsKK->SetBranchAddress("control", &control);
    tree_BsKK->SetBranchAddress("K1K2", &K1K2);
    tree_BsKK->SetBranchAddress("jpsiKpi", &jpsiKpi);
    tree_BsKK->SetBranchAddress("jpsipiK", &jpsipiK);
    tree_BsKK->SetBranchAddress("jpsiKK", &jpsiKK);
    tree_BsKK->SetBranchAddress("jpsipipi", &jpsipipi);
    tree_BsKK->SetBranchAddress("jpsipK", &jpsipK);
    tree_BsKK->SetBranchAddress("jpsiKp", &jpsiKp);
    tree_BsKK->SetBranchAddress("controlLb", &controlLb);
    tree_BsKK->SetBranchAddress("jpsip1", &jpsip1);
    tree_BsKK->SetBranchAddress("jpsip2", &jpsip2);
    tree_BsKK->SetBranchAddress("p1K2", &p1K2);
    tree_BsKK->SetBranchAddress("p2K1", &p2K1);
    tree_BsKK->SetBranchAddress("cos_theta_Zc_Kpi", &cos_theta_Zc_Kpi);
    tree_BsKK->SetBranchAddress("cos_theta_Zc_piK", &cos_theta_Zc_piK);
    tree_BsKK->SetBranchAddress("phi_K_Kpi", &phi_K_Kpi);
    tree_BsKK->SetBranchAddress("phi_K_piK", &phi_K_piK);
    tree_BsKK->SetBranchAddress("alpha_mu_Kpi", &alpha_mu_Kpi);
    tree_BsKK->SetBranchAddress("alpha_mu_piK", &alpha_mu_piK);
    tree_BsKK->SetBranchAddress("phi_mu_Kstar_Kpi", &phi_mu_Kstar_Kpi);
    tree_BsKK->SetBranchAddress("phi_mu_Kstar_piK", &phi_mu_Kstar_piK);
    tree_BsKK->SetBranchAddress("phi_mu_X_Kpi", &phi_mu_X_Kpi);
    tree_BsKK->SetBranchAddress("phi_mu_X_piK", &phi_mu_X_piK);
    tree_BsKK->SetBranchAddress("cos_theta_psi_X_Kpi", &cos_theta_psi_X_Kpi);
    tree_BsKK->SetBranchAddress("cos_theta_psi_X_piK", &cos_theta_psi_X_piK);
    tree_BsKK->SetBranchAddress("cos_theta_Kstar_Kpi", &cos_theta_Kstar_Kpi);
    tree_BsKK->SetBranchAddress("cos_theta_Kstar_piK", &cos_theta_Kstar_piK);
    tree_BsKK->SetBranchAddress("cos_theta_psi_Kstar_Kpi", &cos_theta_psi_Kstar_Kpi);
    tree_BsKK->SetBranchAddress("cos_theta_psi_Kstar_piK", &cos_theta_psi_Kstar_piK);
    tree_BsKK->SetBranchAddress("cos_theta_B0_Kpi", &cos_theta_B0_Kpi);
    tree_BsKK->SetBranchAddress("cos_theta_B0_piK", &cos_theta_B0_piK);
    tree_BsKK->SetBranchAddress("phi_psi_Kpi", &phi_psi_Kpi);
    tree_BsKK->SetBranchAddress("phi_psi_piK", &phi_psi_piK);
    tree_BsKK->SetBranchAddress("jpsippi", &jpsippi);
    tree_BsKK->SetBranchAddress("jpsipip", &jpsipip);
    
    for(int i = 1; i <= nentries_BsKK; i++)
    {
        tree_BsKK->GetEntry(i);
        
        mu1.SetXYZM(muon1_px, muon1_py, muon1_pz, 105.65837);
        mu2.SetXYZM(muon2_px, muon2_py, muon2_pz, 105.65837);
        kaon1.SetXYZM(kaon1_px, kaon1_py, kaon1_pz, 493.677);
        kaon2.SetXYZM(kaon2_px, kaon2_py, kaon2_pz, 493.677);
        
        psi = mu1 + mu2;
        f = kaon1 + kaon2;
        Bs = psi + kaon1 + kaon2;
        
        JpsiK1_mass_BsKK[i-1] = jpsiK1;
        JpsiK2_mass_BsKK[i-1] = jpsiK2;
        Jpsipi1_mass_BsKK[i-1] = jpsipi1;
        Jpsipi2_mass_BsKK[i-1] = jpsipi2;
        K1pi2_mass_BsKK[i-1] = K1pi2;
        K2pi1_mass_BsKK[i-1] = K2pi1;
        KK_truth_BsKK[i - 1] = (kaon1 + kaon2).M();
        thetaF_a[i - 1] = theta_Kstar( Bs, f, kaon1 );
        phi_mu_F_a[i - 1] = phi_mu_Kstar(Bs, f, psi, mu2); 
        phi_KF_a[i - 1] = phi_K(Bs, f, kaon1, psi);
        theta_psi_F_a[i - 1] = theta_psi_Kstar(Bs, psi, mu2);
        w_dimuon_a_BsKK[i - 1] = w_dimuon;
        signal_a_BsKK[i - 1] = signal;
        control_a_BsKK[i - 1] = control;
        KK_mass_BsKK[i - 1] = K1K2;
        JpsiKpi_mass_BsKK[i - 1] = jpsiKpi;
        JpsipiK_mass_BsKK[i - 1] = jpsipiK;
        JpsiKK_mass_BsKK[i - 1] = jpsiKK;
        Jpsipipi_mass_BsKK[i - 1] = jpsipipi;
        JpsipK_mass_BsKK[i - 1] = jpsipK;
        JpsiKp_mass_BsKK[i - 1] = jpsiKp;
        Jpsippi_mass_BsKK[i - 1] = jpsippi;
        Jpsipip_mass_BsKK[i - 1] = jpsipip;
        controlLb_a_BsKK[i - 1] = controlLb;
        Jpsip1_mass_BsKK[i - 1] = jpsip1;
        Jpsip2_mass_BsKK[i - 1] = jpsip2;
        p1K2_mass_BsKK[i - 1] = p1K2;
        p2K1_mass_BsKK[i - 1] = p2K1;
        cos_theta_Zc_Kpi_Bs[i - 1] = cos_theta_Zc_Kpi;
        cos_theta_Zc_piK_Bs[i - 1] = cos_theta_Zc_piK;
        phi_K_Kpi_Bs[i - 1] = phi_K_Kpi;
        phi_K_piK_Bs[i - 1] = phi_K_piK;
        alpha_mu_Kpi_Bs[i - 1] = alpha_mu_Kpi;
        alpha_mu_piK_Bs[i - 1] = alpha_mu_piK;
        phi_mu_Kstar_Kpi_Bs[i - 1] = phi_mu_Kstar_Kpi;
        phi_mu_Kstar_piK_Bs[i - 1] = phi_mu_Kstar_piK;
        phi_mu_X_Kpi_Bs[i - 1] = phi_mu_X_Kpi;
        phi_mu_X_piK_Bs[i - 1] = phi_mu_X_piK;
        cos_theta_psi_X_Kpi_Bs[i - 1] = cos_theta_psi_X_Kpi;
        cos_theta_psi_X_piK_Bs[i - 1] = cos_theta_psi_X_piK;
        cos_theta_Kstar_Kpi_Bs[i - 1] = cos_theta_Kstar_Kpi;
        cos_theta_Kstar_piK_Bs[i - 1] = cos_theta_Kstar_piK;
        cos_theta_psi_Kstar_Kpi_Bs[i - 1] = cos_theta_psi_Kstar_Kpi;
        cos_theta_psi_Kstar_piK_Bs[i - 1] = cos_theta_psi_Kstar_piK;
        cos_theta_B0_Kpi_Bs[i - 1] = cos_theta_B0_Kpi;
        cos_theta_B0_piK_Bs[i - 1] = cos_theta_B0_piK;
        phi_psi_Kpi_Bs[i - 1] = phi_psi_Kpi;
        phi_psi_piK_Bs[i - 1] = phi_psi_piK;
        
        weight_BsKK[i - 1] = Hamplitudef_total(0., 0., KK_truth_BsKK[i - 1] / 1000., 5.36688, 0., 0., 0., 0.,
						std::complex<float>(0., 0.), std::complex<float>(0., 0.),
						std::complex<float> ((*vpar)(47) * cos((*vpar)(48)), (*vpar)(47) * sin((*vpar)(48))), 
                        std::complex<float> ((*vpar)(49) * cos((*vpar)(50)), (*vpar)(49) * sin((*vpar)(50))), 
                        std::complex<float> ((*vpar)(51) * cos((*vpar)(52)), (*vpar)(51) * sin((*vpar)(52))),
						std::complex<float> ((*vpar)(53) * cos((*vpar)(54)), (*vpar)(53) * sin((*vpar)(54))), 
                        std::complex<float> ((*vpar)(55) * cos((*vpar)(56)), (*vpar)(55) * sin((*vpar)(56))), 
                        std::complex<float> ((*vpar)(57) * cos((*vpar)(58)), (*vpar)(57) * sin((*vpar)(58))),
						std::complex<float> ((*vpar)(59) * cos((*vpar)(60)), (*vpar)(59) * sin((*vpar)(60))), 
                        std::complex<float> ((*vpar)(61) * cos((*vpar)(62)), (*vpar)(61) * sin((*vpar)(62))), 
                        std::complex<float> ((*vpar)(63) * cos((*vpar)(64)), (*vpar)(63) * sin((*vpar)(64))),
						std::complex<float> ((*vpar)(65) * cos((*vpar)(66)), (*vpar)(65) * sin((*vpar)(66))), 
                        std::complex<float> ((*vpar)(67) * cos((*vpar)(68)), (*vpar)(67) * sin((*vpar)(68))),
                        std::complex<float> ((*vpar)(69) * cos((*vpar)(70)), (*vpar)(69) * sin((*vpar)(70))),
						std::complex<float> ((*vpar)(71) * cos((*vpar)(72)), (*vpar)(71) * sin((*vpar)(72))), 
                        std::complex<float> ((*vpar)(73) * cos((*vpar)(74)), (*vpar)(73) * sin((*vpar)(74))), 
                        std::complex<float> ((*vpar)(75) * cos((*vpar)(76)), (*vpar)(75) * sin((*vpar)(76))),
						std::complex<float> ((*vpar)(77) * cos((*vpar)(78)), (*vpar)(77) * sin((*vpar)(78))),
						0., thetaF_a[i - 1], 0., theta_psi_F_a[i - 1], 0., phi_mu_F_a[i - 1], phi_KF_a[i - 1], 0., 0., 0.) * w_dimuon_a_BsKK[i - 1];
        
    }
    
    /*Jpsipi_2d_BS_to_JPSI_K_K->Scale(n_BS_to_JPSI_K_K / Jpsipi_2d_BS_to_JPSI_K_K->Integral());
    JpsiK_2d_BS_to_JPSI_K_K->Scale(n_BS_to_JPSI_K_K / JpsiK_2d_BS_to_JPSI_K_K->Integral());
    Kpi_2d_BS_to_JPSI_K_K->Scale(n_BS_to_JPSI_K_K / Kpi_2d_BS_to_JPSI_K_K->Integral());*/
    
    //TFile *f_in1 = new TFile("signal_area_bg_for_syst_1_3.root"); //for systematics
    //TFile *f_in1 = new TFile("signal_area_bg_for_syst_1_315.root"); //for_systematics
    TFile *f_in1 = new TFile("ROOT_files/signal_area_bg.root");
    
    f_in1->GetObject("Jpsipi_2d_bg", Jpsipi_2d_bg);
    f_in1->GetObject("JpsiK_2d_bg", JpsiK_2d_bg);
    f_in1->GetObject("Kpi_2d_bg", Kpi_2d_bg);
    
    /*Jpsipi_2d_bg->Scale(n_comb_bg / Jpsipi_2d_bg->Integral());
    JpsiK_2d_bg->Scale(n_comb_bg / JpsiK_2d_bg->Integral());
    Kpi_2d_bg->Scale(n_comb_bg / Kpi_2d_bg->Integral());*/
    
    TTree *tree_Lb = (TTree*)f_phs->Get("phs_LAMBDA0B_to_JPSI_P_K");
    Long64_t nentries_Lb = tree_Lb->GetEntries();
    
    float proton_px, proton_py, proton_pz; 
    
    tree_Lb->SetBranchAddress("mu1_px", &muon1_px);
    tree_Lb->SetBranchAddress("mu1_py", &muon1_py);
    tree_Lb->SetBranchAddress("mu1_pz", &muon1_pz);
    tree_Lb->SetBranchAddress("mu2_px", &muon2_px);
    tree_Lb->SetBranchAddress("mu2_py", &muon2_py);
    tree_Lb->SetBranchAddress("mu2_pz", &muon2_pz);
    tree_Lb->SetBranchAddress("p_px", &proton_px);
    tree_Lb->SetBranchAddress("p_py", &proton_py);
    tree_Lb->SetBranchAddress("p_pz", &proton_pz);
    tree_Lb->SetBranchAddress("K_px", &kaon_px);
    tree_Lb->SetBranchAddress("K_py", &kaon_py);
    tree_Lb->SetBranchAddress("K_pz", &kaon_pz);
    tree_Lb->SetBranchAddress("jpsiK1", &jpsiK1);
    tree_Lb->SetBranchAddress("jpsiK2", &jpsiK2);
    tree_Lb->SetBranchAddress("jpsipi1", &jpsipi1);
    tree_Lb->SetBranchAddress("jpsipi2", &jpsipi2);
    tree_Lb->SetBranchAddress("K1pi2", &K1pi2);
    tree_Lb->SetBranchAddress("K2pi1", &K2pi1);
    tree_Lb->SetBranchAddress("w_dimuon", &w_dimuon);
    tree_Lb->SetBranchAddress("signal", &signal);
    tree_Lb->SetBranchAddress("control", &control);
    tree_Lb->SetBranchAddress("K1K2", &K1K2);
    tree_Lb->SetBranchAddress("jpsiKpi", &jpsiKpi);
    tree_Lb->SetBranchAddress("jpsipiK", &jpsipiK);
    tree_Lb->SetBranchAddress("jpsiKK", &jpsiKK);
    tree_Lb->SetBranchAddress("jpsipipi", &jpsipipi);
    tree_Lb->SetBranchAddress("jpsipK", &jpsipK);
    tree_Lb->SetBranchAddress("jpsiKp", &jpsiKp);
    tree_Lb->SetBranchAddress("controlLb", &controlLb);
    tree_Lb->SetBranchAddress("jpsip1", &jpsip1);
    tree_Lb->SetBranchAddress("jpsip2", &jpsip2);
    tree_Lb->SetBranchAddress("p1K2", &p1K2);
    tree_Lb->SetBranchAddress("p2K1", &p2K1);
    tree_Lb->SetBranchAddress("cos_theta_Zc_Kpi", &cos_theta_Zc_Kpi);
    tree_Lb->SetBranchAddress("cos_theta_Zc_piK", &cos_theta_Zc_piK);
    tree_Lb->SetBranchAddress("phi_K_Kpi", &phi_K_Kpi);
    tree_Lb->SetBranchAddress("phi_K_piK", &phi_K_piK);
    tree_Lb->SetBranchAddress("alpha_mu_Kpi", &alpha_mu_Kpi);
    tree_Lb->SetBranchAddress("alpha_mu_piK", &alpha_mu_piK);
    tree_Lb->SetBranchAddress("phi_mu_Kstar_Kpi", &phi_mu_Kstar_Kpi);
    tree_Lb->SetBranchAddress("phi_mu_Kstar_piK", &phi_mu_Kstar_piK);
    tree_Lb->SetBranchAddress("phi_mu_X_Kpi", &phi_mu_X_Kpi);
    tree_Lb->SetBranchAddress("phi_mu_X_piK", &phi_mu_X_piK);
    tree_Lb->SetBranchAddress("cos_theta_psi_X_Kpi", &cos_theta_psi_X_Kpi);
    tree_Lb->SetBranchAddress("cos_theta_psi_X_piK", &cos_theta_psi_X_piK);
    tree_Lb->SetBranchAddress("cos_theta_Kstar_Kpi", &cos_theta_Kstar_Kpi);
    tree_Lb->SetBranchAddress("cos_theta_Kstar_piK", &cos_theta_Kstar_piK);
    tree_Lb->SetBranchAddress("cos_theta_psi_Kstar_Kpi", &cos_theta_psi_Kstar_Kpi);
    tree_Lb->SetBranchAddress("cos_theta_psi_Kstar_piK", &cos_theta_psi_Kstar_piK);
    tree_Lb->SetBranchAddress("cos_theta_B0_Kpi", &cos_theta_B0_Kpi);
    tree_Lb->SetBranchAddress("cos_theta_B0_piK", &cos_theta_B0_piK);
    tree_Lb->SetBranchAddress("phi_psi_Kpi", &phi_psi_Kpi);
    tree_Lb->SetBranchAddress("phi_psi_piK", &phi_psi_piK);
    tree_Lb->SetBranchAddress("jpsippi", &jpsippi);
    tree_Lb->SetBranchAddress("jpsipip", &jpsipip);
    
    for(int i = 1; i <= nentries_Lb; i++)
    {
        tree_Lb->GetEntry(i);
        mu1.SetXYZM(muon1_px, muon1_py, muon1_pz, 105.65837);
        mu2.SetXYZM(muon2_px, muon2_py, muon2_pz, 105.65837);
        proton.SetXYZM(proton_px, proton_py, proton_pz, 938.272081);
        kaon.SetXYZM(kaon_px, kaon_py, kaon_pz, 493.677);
        
        psi = mu1 + mu2;
        Lstar = proton + kaon;
        Lb = psi + proton + kaon;
        Pc = psi + proton;
        
        JpsiK1_mass_Lb[i-1] = jpsiK1;
        JpsiK2_mass_Lb[i-1] = jpsiK2;
        Jpsipi1_mass_Lb[i-1] = jpsipi1;
        Jpsipi2_mass_Lb[i-1] = jpsipi2;
        K1pi2_mass_Lb[i-1] = K1pi2;
        K2pi1_mass_Lb[i-1] = K2pi1;
        pK_truth_Lb[i - 1] = (proton + kaon).M();
        Jpsip_truth_Lb[i - 1] = (psi + proton).M();
        w_dimuon_a_Lb[i - 1] = w_dimuon;
        signal_a_Lb[i - 1] = signal;
        control_a_Lb[i - 1] = control;
        KK_mass_Lb[i - 1] = K1K2;
        JpsiKpi_mass_Lb[i - 1] = jpsiKpi;
        JpsipiK_mass_Lb[i - 1] = jpsipiK;
        JpsiKK_mass_Lb[i - 1] = jpsiKK;
        Jpsipipi_mass_Lb[i - 1] = jpsipipi;
        JpsipK_mass_Lb[i - 1] = jpsipK;
        JpsiKp_mass_Lb[i - 1] = jpsiKp;
        Jpsippi_mass_Lb[i - 1] = jpsippi;
        Jpsipip_mass_Lb[i - 1] = jpsipip;
        
        /*psi = kaon; // for test
        kaon = proton;
        proton = psi;
        
        psi = mu1 + mu2;
        Lstar = proton + kaon;
        Lb = psi + proton + kaon;
        Pc = psi + proton;*/
        
        theta_lambda_b_a[i - 1] = theta_lambda_b(Lb, Pc);
        theta_lambda_b_ls_a[i - 1] = theta_lambda_b_ls(Lb, proton, kaon); 
        theta_Pc_a[i - 1] = theta_Pc(Lb, Pc, psi); 
        theta_Lstar_a[i - 1] = theta_Lstar(Lb, Lstar, kaon); 
        theta_psi_a[i - 1] = theta_psi(Pc, psi, mu2); 
        phi_mu_a[i - 1] = phi_mu(Pc, kaon, psi, proton, mu2); 
        phi_mu_Pc_a[i - 1] = phi_mu_Pc(Pc, kaon, psi, mu2); 
        phi_mu_Lstar_a[i - 1] = phi_mu_Lstar(Lb, Lstar, psi, mu2); 
        phi_K_Lstar_a[i - 1] = phi_K_Lstar(Lb, Lstar, kaon, psi); 
        phi_psi_a[i - 1] = phi_psi(Lb, Pc, psi, kaon); 
        phi_Pc_a[i - 1] = phi_Pc(Lb, Pc, Lstar); 
        theta_p_a[i - 1] = theta_p(psi, proton, kaon);
        alpha_mu_Lb_a[i - 1] = alpha_mu(mu2, psi, Pc, Lb);
        thete_psi_Lstar_a[i - 1] = theta_psi(Lstar, psi, mu2);
        controlLb_a_Lb[i - 1] = controlLb;
        Jpsip1_mass_Lb[i - 1] = jpsip1;
        Jpsip2_mass_Lb[i - 1] = jpsip2;
        p1K2_mass_Lb[i - 1] = p1K2;
        p2K1_mass_Lb[i - 1] = p2K1;
        cos_theta_Zc_Kpi_Lb[i - 1] = cos_theta_Zc_Kpi;
        cos_theta_Zc_piK_Lb[i - 1] = cos_theta_Zc_piK;
        phi_K_Kpi_Lb[i - 1] = phi_K_Kpi;
        phi_K_piK_Lb[i - 1] = phi_K_piK;
        alpha_mu_Kpi_Lb[i - 1] = alpha_mu_Kpi;
        alpha_mu_piK_Lb[i - 1] = alpha_mu_piK;
        phi_mu_Kstar_Kpi_Lb[i - 1] = phi_mu_Kstar_Kpi;
        phi_mu_Kstar_piK_Lb[i - 1] = phi_mu_Kstar_piK;
        phi_mu_X_Kpi_Lb[i - 1] = phi_mu_X_Kpi;
        phi_mu_X_piK_Lb[i - 1] = phi_mu_X_piK;
        cos_theta_psi_X_Kpi_Lb[i - 1] = cos_theta_psi_X_Kpi;
        cos_theta_psi_X_piK_Lb[i - 1] = cos_theta_psi_X_piK;
        cos_theta_Kstar_Kpi_Lb[i - 1] = cos_theta_Kstar_Kpi;
        cos_theta_Kstar_piK_Lb[i - 1] = cos_theta_Kstar_piK;
        cos_theta_psi_Kstar_Kpi_Lb[i - 1] = cos_theta_psi_Kstar_Kpi;
        cos_theta_psi_Kstar_piK_Lb[i - 1] = cos_theta_psi_Kstar_piK;
        cos_theta_B0_Kpi_Lb[i - 1] = cos_theta_B0_Kpi;
        cos_theta_B0_piK_Lb[i - 1] = cos_theta_B0_piK;
        phi_psi_Kpi_Lb[i - 1] = phi_psi_Kpi;
        phi_psi_piK_Lb[i - 1] = phi_psi_piK;
        
        weight_Lb[i - 1] = HamplitudePcLstar_total(Jpsip_truth_Lb[i - 1] / 1000., pK_truth_Lb[i - 1] / 1000., 5.6196, (*vpar)(209), (*vpar)(210), 0., 0., (*vpar)(211), (*vpar)(212), 0., 0.,
						std::complex<float> ((*vpar)(189) * cos((*vpar)(190)), (*vpar)(189) * cos((*vpar)(190))), 
                        std::complex<float> ((*vpar)(191) * cos((*vpar)(192)), (*vpar)(191) * cos((*vpar)(192))), 
                        std::complex<float> ((*vpar)(193) * cos((*vpar)(194)), (*vpar)(193) * cos((*vpar)(194))), 
                        std::complex<float> ((*vpar)(195) * cos((*vpar)(196)), (*vpar)(195) * cos((*vpar)(196))), 
                        std::complex<float> ((*vpar)(197) * cos((*vpar)(198)), (*vpar)(197) * cos((*vpar)(198))),
						std::complex<float> ((*vpar)(199) * cos((*vpar)(200)), (*vpar)(199) * cos((*vpar)(200))), 
                        std::complex<float> ((*vpar)(201) * cos((*vpar)(202)), (*vpar)(201) * cos((*vpar)(202))), 
                        std::complex<float> ((*vpar)(203) * cos((*vpar)(204)), (*vpar)(203) * cos((*vpar)(204))), 
                        std::complex<float> ((*vpar)(205) * cos((*vpar)(206)), (*vpar)(205) * cos((*vpar)(206))), 
                        std::complex<float> ((*vpar)(207) * cos((*vpar)(208)), (*vpar)(207) * cos((*vpar)(208))),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> ((*vpar)(121) * cos((*vpar)(122)), (*vpar)(121) * cos((*vpar)(122))), 
                        std::complex<float> ((*vpar)(123) * cos((*vpar)(124)), (*vpar)(123) * cos((*vpar)(124))), 
                        std::complex<float> ((*vpar)(125) * cos((*vpar)(126)), (*vpar)(125) * cos((*vpar)(126))), 
                        std::complex<float> ((*vpar)(127) * cos((*vpar)(128)), (*vpar)(127) * cos((*vpar)(128))),
						std::complex<float> ((*vpar)(129) * cos((*vpar)(130)), (*vpar)(129) * cos((*vpar)(130))), 
                        std::complex<float> ((*vpar)(131) * cos((*vpar)(132)), (*vpar)(131) * cos((*vpar)(132))), 
                        std::complex<float> ((*vpar)(133) * cos((*vpar)(134)), (*vpar)(133) * cos((*vpar)(134))),
						std::complex<float> ((*vpar)(135) * cos((*vpar)(136)), (*vpar)(135) * cos((*vpar)(136))), 
                        std::complex<float> ((*vpar)(137) * cos((*vpar)(138)), (*vpar)(137) * cos((*vpar)(138))), 
                        std::complex<float> ((*vpar)(139) * cos((*vpar)(140)), (*vpar)(139) * cos((*vpar)(140))),
						std::complex<float> ((*vpar)(141) * cos((*vpar)(142)), (*vpar)(141) * cos((*vpar)(142))), 
                        std::complex<float> ((*vpar)(143) * cos((*vpar)(144)), (*vpar)(143) * cos((*vpar)(144))), 
                        std::complex<float> ((*vpar)(145) * cos((*vpar)(146)), (*vpar)(145) * cos((*vpar)(146))),
						std::complex<float> ((*vpar)(147) * cos((*vpar)(148)), (*vpar)(147) * cos((*vpar)(148))), 
                        std::complex<float> ((*vpar)(149) * cos((*vpar)(150)), (*vpar)(149) * cos((*vpar)(150))), 
                        std::complex<float> ((*vpar)(151) * cos((*vpar)(152)), (*vpar)(151) * cos((*vpar)(152))),
						std::complex<float> ((*vpar)(153) * cos((*vpar)(154)), (*vpar)(153) * cos((*vpar)(154))), 
                        std::complex<float> ((*vpar)(155) * cos((*vpar)(156)), (*vpar)(155) * cos((*vpar)(156))), 
                        std::complex<float> ((*vpar)(157) * cos((*vpar)(158)), (*vpar)(157) * cos((*vpar)(158))),
						std::complex<float> ((*vpar)(159) * cos((*vpar)(160)), (*vpar)(159) * cos((*vpar)(160))), 
                        std::complex<float> ((*vpar)(161) * cos((*vpar)(162)), (*vpar)(161) * cos((*vpar)(162))),
                        std::complex<float> ((*vpar)(163) * cos((*vpar)(164)), (*vpar)(163) * cos((*vpar)(164))),
						std::complex<float> ((*vpar)(165) * cos((*vpar)(166)), (*vpar)(165) * cos((*vpar)(166))), 
                        std::complex<float> ((*vpar)(167) * cos((*vpar)(168)), (*vpar)(167) * cos((*vpar)(168))), 
                        std::complex<float> ((*vpar)(169) * cos((*vpar)(170)), (*vpar)(169) * cos((*vpar)(170))),
						std::complex<float> ((*vpar)(171) * cos((*vpar)(172)), (*vpar)(171) * cos((*vpar)(172))), 
                        std::complex<float> ((*vpar)(173) * cos((*vpar)(174)), (*vpar)(173) * cos((*vpar)(174))), 
                        std::complex<float> ((*vpar)(175) * cos((*vpar)(176)), (*vpar)(175) * cos((*vpar)(176))),
						std::complex<float> ((*vpar)(177) * cos((*vpar)(178)), (*vpar)(177) * cos((*vpar)(178))), 
                        std::complex<float> ((*vpar)(179) * cos((*vpar)(180)), (*vpar)(179) * cos((*vpar)(180))), 
                        std::complex<float> ((*vpar)(181) * cos((*vpar)(182)), (*vpar)(181) * cos((*vpar)(182))),
						std::complex<float> ((*vpar)(183) * cos((*vpar)(184)), (*vpar)(183) * cos((*vpar)(184))), 
                        std::complex<float> ((*vpar)(185) * cos((*vpar)(186)), (*vpar)(185) * cos((*vpar)(186))), 
                        std::complex<float> ((*vpar)(187) * cos((*vpar)(188)), (*vpar)(187) * cos((*vpar)(188))),
						theta_Lstar_a[i - 1], theta_lambda_b_ls_a[i - 1], thete_psi_Lstar_a[i - 1], phi_mu_Lstar_a[i - 1], phi_K_Lstar_a[i - 1],
						theta_Pc_a[i - 1], theta_lambda_b_a[i - 1], theta_psi_a[i - 1], phi_mu_Pc_a[i - 1], phi_Pc_a[i - 1], phi_psi_a[i - 1], 
						alpha_mu_Lb_a[i - 1], theta_p_a[i - 1]) * w_dimuon_a_Lb[i - 1]; // for_syst
           //weight_Lb[i - 1] = 1.; 
        
    }
    
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo_|BS_to_JPSI_K_K_control_area|_oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
    
    TFile *f_in_control = new TFile("ROOT_files/control_area_BsKK_Zc1plus.root");
    
    f_in_control->GetObject("JpsiK_2d_control_data", JpsiK_2d_control_data);
    f_in_control->GetObject("JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K", JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K);
    f_in_control->GetObject("JpsiK_2d_control_BS_to_JPSI_K_K", JpsiK_2d_control_BS_to_JPSI_K_K);
    f_in_control->GetObject("JpsiK_2d_control_BS_to_JPSI_PI_PI", JpsiK_2d_control_BS_to_JPSI_PI_PI);
    f_in_control->GetObject("JpsiK_2d_control_BD_to_JPSI_K_PI", JpsiK_2d_control_BD_to_JPSI_K_PI);
    f_in_control->GetObject("JpsiK_2d_control_BD_to_JPSI_PI_PI", JpsiK_2d_control_BD_to_JPSI_PI_PI);
    f_in_control->GetObject("JpsiK_2d_control_LAMBDA0B_to_JPSI_P_PI", JpsiK_2d_control_LAMBDA0B_to_JPSI_P_PI);
    f_in_control->GetObject("JpsiK_2d_control_BD_to_JPSI_K_K", JpsiK_2d_control_BD_to_JPSI_K_K);
    f_in_control->GetObject("JpsiK_2d_control_BS_to_JPSI_K_PI", JpsiK_2d_control_BS_to_JPSI_K_PI);
    
    f_in_control->GetObject("KK_1d_control_data", KK_1d_control_data);
    f_in_control->GetObject("KK_1d_control_LAMBDA0B_to_JPSI_P_K", KK_1d_control_LAMBDA0B_to_JPSI_P_K);
    f_in_control->GetObject("KK_1d_control_BS_to_JPSI_K_K", KK_1d_control_BS_to_JPSI_K_K);
    f_in_control->GetObject("KK_1d_control_BS_to_JPSI_PI_PI", KK_1d_control_BS_to_JPSI_PI_PI);
    f_in_control->GetObject("KK_1d_control_BD_to_JPSI_K_PI", KK_1d_control_BD_to_JPSI_K_PI);
    f_in_control->GetObject("KK_1d_control_BD_to_JPSI_PI_PI", KK_1d_control_BD_to_JPSI_PI_PI);
    f_in_control->GetObject("KK_1d_control_LAMBDA0B_to_JPSI_P_PI", KK_1d_control_LAMBDA0B_to_JPSI_P_PI);
    f_in_control->GetObject("KK_1d_control_BD_to_JPSI_K_K", KK_1d_control_BD_to_JPSI_K_K);
    f_in_control->GetObject("KK_1d_control_BS_to_JPSI_K_PI", KK_1d_control_BS_to_JPSI_K_PI);    
    
    TFile *f_in1_control = new TFile("ROOT_files/control_area_BsKK_bg.root");
    
    f_in1_control->GetObject("JpsiK_2d_control_bg", JpsiK_2d_control_bg);
    f_in1_control->GetObject("KK_1d_control_bg", KK_1d_control_bg);
    
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__||LAMBDA0B_to_JPSI_P_K_control_area|_oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
    
    TFile *f_in_controlLb = new TFile("ROOT_files/control_area_Lb.root");
    
    f_in_controlLb->GetObject("JpsiK_2d_controlLb_data", JpsiK_2d_controlLb_data);
    f_in_controlLb->GetObject("JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K", JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K);
    f_in_controlLb->GetObject("JpsiK_2d_controlLb_BS_to_JPSI_K_K", JpsiK_2d_controlLb_BS_to_JPSI_K_K);
    f_in_controlLb->GetObject("JpsiK_2d_controlLb_BS_to_JPSI_PI_PI", JpsiK_2d_controlLb_BS_to_JPSI_PI_PI);
    f_in_controlLb->GetObject("JpsiK_2d_controlLb_BD_to_JPSI_K_PI", JpsiK_2d_controlLb_BD_to_JPSI_K_PI);
    f_in_controlLb->GetObject("JpsiK_2d_controlLb_BD_to_JPSI_PI_PI", JpsiK_2d_controlLb_BD_to_JPSI_PI_PI);
    f_in_controlLb->GetObject("JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI", JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI);
    f_in_controlLb->GetObject("JpsiK_2d_controlLb_BD_to_JPSI_K_K", JpsiK_2d_controlLb_BD_to_JPSI_K_K);
    f_in_controlLb->GetObject("JpsiK_2d_controlLb_BS_to_JPSI_K_PI", JpsiK_2d_controlLb_BS_to_JPSI_K_PI);
    
    f_in_controlLb->GetObject("Jpsip_2d_controlLb_data", Jpsip_2d_controlLb_data);
    f_in_controlLb->GetObject("Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K", Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K);
    f_in_controlLb->GetObject("Jpsip_2d_controlLb_BS_to_JPSI_K_K", Jpsip_2d_controlLb_BS_to_JPSI_K_K);
    f_in_controlLb->GetObject("Jpsip_2d_controlLb_BS_to_JPSI_PI_PI", Jpsip_2d_controlLb_BS_to_JPSI_PI_PI);
    f_in_controlLb->GetObject("Jpsip_2d_controlLb_BD_to_JPSI_K_PI", Jpsip_2d_controlLb_BD_to_JPSI_K_PI);
    f_in_controlLb->GetObject("Jpsip_2d_controlLb_BD_to_JPSI_PI_PI", Jpsip_2d_controlLb_BD_to_JPSI_PI_PI); 
    f_in_controlLb->GetObject("Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_PI", Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_PI);
    f_in_controlLb->GetObject("Jpsip_2d_controlLb_BD_to_JPSI_K_K", Jpsip_2d_controlLb_BD_to_JPSI_K_K);
    f_in_controlLb->GetObject("Jpsip_2d_controlLb_BS_to_JPSI_K_PI", Jpsip_2d_controlLb_BS_to_JPSI_K_PI);
    
    f_in_controlLb->GetObject("pK_2d_controlLb_data", pK_2d_controlLb_data);
    f_in_controlLb->GetObject("pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K", pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K);
    f_in_controlLb->GetObject("pK_2d_controlLb_BS_to_JPSI_K_K", pK_2d_controlLb_BS_to_JPSI_K_K);
    f_in_controlLb->GetObject("pK_2d_controlLb_BS_to_JPSI_PI_PI", pK_2d_controlLb_BS_to_JPSI_PI_PI);
    f_in_controlLb->GetObject("pK_2d_controlLb_BD_to_JPSI_K_PI", pK_2d_controlLb_BD_to_JPSI_K_PI);
    f_in_controlLb->GetObject("pK_2d_controlLb_BD_to_JPSI_PI_PI", pK_2d_controlLb_BD_to_JPSI_PI_PI);
    f_in_controlLb->GetObject("pK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI", pK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI);
    f_in_controlLb->GetObject("pK_2d_controlLb_BD_to_JPSI_K_K", pK_2d_controlLb_BD_to_JPSI_K_K);
    f_in_controlLb->GetObject("pK_2d_controlLb_BS_to_JPSI_K_PI", pK_2d_controlLb_BS_to_JPSI_K_PI);
    
    TFile *f_in1_controlLb = new TFile("ROOT_files/control_area_Lb_bg.root");
    //
    f_in1_controlLb->GetObject("JpsiK_2d_controlLb_bg", JpsiK_2d_controlLb_bg);
    f_in1_controlLb->GetObject("Jpsip_2d_controlLb_bg", Jpsip_2d_controlLb_bg);
    f_in1_controlLb->GetObject("pK_2d_controlLb_bg", pK_2d_controlLb_bg); 
    
    coef_4_to_s_LAMBDA0B_to_JPSI_P_K = 0.;
    coef_4_to_c_LAMBDA0B_to_JPSI_P_K = 0.;
    coef_4_to_clb_LAMBDA0B_to_JPSI_P_K = 0.;
    
    for(int i = 0; i < phs_Lb; i++)
    {
        if (signal_a_Lb[i]) {
            Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->Fill(Jpsipi1_mass_Lb[i], Jpsipi2_mass_Lb[i], weight_Lb[i]);
            JpsiK_2d_LAMBDA0B_to_JPSI_P_K->Fill(JpsiK1_mass_Lb[i], JpsiK2_mass_Lb[i], weight_Lb[i]);
            Kpi_2d_LAMBDA0B_to_JPSI_P_K->Fill(K1pi2_mass_Lb[i], K2pi1_mass_Lb[i], weight_Lb[i]);
            cos_theta_Zc_LAMBDA0B_to_JPSI_P_K->Fill(cos_theta_Zc_Kpi_Lb[i], cos_theta_Zc_piK_Lb[i], weight_Lb[i]);
            cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K->Fill(cos_theta_psi_Kstar_Kpi_Lb[i], cos_theta_psi_Kstar_piK_Lb[i], weight_Lb[i]);
            phi_K_LAMBDA0B_to_JPSI_P_K->Fill(phi_K_Kpi_Lb[i], phi_K_piK_Lb[i], weight_Lb[i]);
            phi_mu_X_LAMBDA0B_to_JPSI_P_K->Fill(phi_mu_X_Kpi_Lb[i], phi_mu_X_piK_Lb[i], weight_Lb[i]);
            cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->Fill(cos_theta_Kstar_Kpi_Lb[i], cos_theta_Kstar_piK_Lb[i], weight_Lb[i]);
            cos_theta_B0_LAMBDA0B_to_JPSI_P_K->Fill(cos_theta_B0_Kpi_Lb[i], cos_theta_B0_piK_Lb[i], weight_Lb[i]);
            cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K->Fill(cos_theta_psi_X_Kpi_Lb[i], cos_theta_psi_X_piK_Lb[i], weight[i]);
            phi_mu_X_LAMBDA0B_to_JPSI_P_K->Fill(phi_mu_X_Kpi_Lb[i], phi_mu_X_piK_Lb[i], weight[i]);
            cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->Fill(cos_theta_Kstar_Kpi_Lb[i], cos_theta_Kstar_piK_Lb[i], weight[i]);
            
            /*phi_K_LAMBDA0B_to_JPSI_P_K->Fill(phi_K_Kpi_Lb[i], phi_K_piK_Lb[i], weight[i]);
            alpha_mu_LAMBDA0B_to_JPSI_P_K->Fill(alpha_mu_Kpi_Lb[i], alpha_mu_piK_Lb[i], weight[i]);
            phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K->Fill(phi_mu_Kstar_Kpi_Lb[i], phi_mu_Kstar_piK_Lb[i], weight[i]);
            cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->Fill(cos_theta_Kstar_Kpi_Lb[i], cos_theta_Kstar_piK_Lb[i], weight[i]);
            cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K->Fill(cos_theta_psi_Kstar_Kpi_Lb[i], cos_theta_psi_Kstar_piK_Lb[i], weight[i]);
            cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->Fill(cos_theta_Kstar_Kpi_Lb[i], cos_theta_Kstar_piK_Lb[i], weight[i]);
            */ 
            coef_4_to_s_LAMBDA0B_to_JPSI_P_K += weight_Lb[i];
        }
        
        if (control_a_Lb[i]) {
            JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K->Fill(JpsiK1_mass_Lb[i], JpsiK2_mass_Lb[i], weight_Lb[i]);
            KK_1d_control_LAMBDA0B_to_JPSI_P_K->Fill(KK_mass_Lb[i], weight_Lb[i]);
            coef_4_to_c_LAMBDA0B_to_JPSI_P_K += weight_Lb[i];
        }  
        
        if (controlLb_a_Lb[i]) {
            JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Fill(JpsiK1_mass_Lb[i], JpsiK2_mass_Lb[i], weight_Lb[i]);
            Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Fill(Jpsip1_mass_Lb[i], Jpsip2_mass_Lb[i], weight_Lb[i]);
            pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Fill(p1K2_mass_Lb[i], p2K1_mass_Lb[i], weight_Lb[i]);
            coef_4_to_clb_LAMBDA0B_to_JPSI_P_K += weight_Lb[i];
        }
            
        Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Fill(JpsiKpi_mass_Lb[i], JpsipiK_mass_Lb[i], weight_Lb[i]);
        KK_1d_LAMBDA0B_to_JPSI_P_K->Fill(JpsiKK_mass_Lb[i], weight_Lb[i]);
        pipi_1d_LAMBDA0B_to_JPSI_P_K->Fill(Jpsipipi_mass_Lb[i], weight_Lb[i]);
        pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->Fill(JpsipK_mass_Lb[i], JpsiKp_mass_Lb[i], weight_Lb[i]);
    }
    
    coef_4_to_s_LAMBDA0B_to_JPSI_P_K = coef_4_to_s_LAMBDA0B_to_JPSI_P_K / Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Integral();
    coef_4_to_c_LAMBDA0B_to_JPSI_P_K = coef_4_to_c_LAMBDA0B_to_JPSI_P_K / KK_1d_LAMBDA0B_to_JPSI_P_K->Integral();
    coef_4_to_clb_LAMBDA0B_to_JPSI_P_K = coef_4_to_clb_LAMBDA0B_to_JPSI_P_K / pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->Integral();
    
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__|fit_procedure|__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
    
    test_flag = 0;
    
    int a_Jpsipi[50][50], a_JpsiK[50][50], a_Kpi[50][50];
    int n_Jpsipi = 0, n_JpsiK = 0, n_Kpi = 0;
    
    for(int i = 0; i < 50; i++)
    {
        for(int j = 0; j < 50; j++)
        {
            if (Jpsipi_2d_data->GetBinContent(i, j) > 0.) 
            {
                n_Jpsipi++;
                a_Jpsipi[i][j] = 1;
            }
            else
            {
                a_Jpsipi[i][j] = 0;
            }
            
            if (JpsiK_2d_data->GetBinContent(i, j) > 0.) 
            {
                n_JpsiK++;
                a_JpsiK[i][j] = 1;
            }
            else
            {
                a_JpsiK[i][j] = 0;
            }
            
            if (Kpi_2d_data->GetBinContent(i, j) > 0.) 
            {
                n_Kpi++;
                a_Kpi[i][j] = 1;
            }
            else
            {
                a_Kpi[i][j] = 0;
            }
        }
    }
    
    Kpi_piK_2d_nbinsX = Kpi_piK_2d_BD_to_JPSI_K_PI->ProjectionX()->GetNbinsX();
    Kpi_piK_2d_nbinsY = Kpi_piK_2d_BD_to_JPSI_K_PI->ProjectionY()->GetNbinsX();
    KK_1d_nbins = KK_1d_BD_to_JPSI_K_PI->GetNbinsX();
    pipi_1d_nbins = pipi_1d_BD_to_JPSI_K_PI->GetNbinsX();
    pK_Kp_2d_nbinsX = pK_Kp_2d_BD_to_JPSI_K_PI->ProjectionX()->GetNbinsX();
    pK_Kp_2d_nbinsY = pK_Kp_2d_BD_to_JPSI_K_PI->ProjectionY()->GetNbinsX();
    Jpsipi_2d_nbinsX = Jpsipi_2d_BD_to_JPSI_K_PI->ProjectionX()->GetNbinsX();
    Jpsipi_2d_nbinsY = Jpsipi_2d_BD_to_JPSI_K_PI->ProjectionY()->GetNbinsX();
    Kpi_2d_nbinsX = Kpi_2d_BD_to_JPSI_K_PI->ProjectionX()->GetNbinsX();
    Kpi_2d_nbinsY = Kpi_2d_BD_to_JPSI_K_PI->ProjectionY()->GetNbinsX();
    JpsiK_2d_nbinsX = JpsiK_2d_BD_to_JPSI_K_PI->ProjectionX()->GetNbinsX();
    JpsiK_2d_nbinsY = JpsiK_2d_BD_to_JPSI_K_PI->ProjectionY()->GetNbinsX();
    cos_theta_Zc_nbinsX = cos_theta_Zc_BD_to_JPSI_K_PI->ProjectionX()->GetNbinsX();
    cos_theta_Zc_nbinsY = cos_theta_Zc_BD_to_JPSI_K_PI->ProjectionY()->GetNbinsX();
    cos_theta_psi_X_nbinsX = cos_theta_psi_X_BD_to_JPSI_K_PI->ProjectionX()->GetNbinsX();
    cos_theta_psi_X_nbinsY = cos_theta_psi_X_BD_to_JPSI_K_PI->ProjectionY()->GetNbinsX();
    phi_mu_X_nbinsX = phi_mu_X_BD_to_JPSI_K_PI->ProjectionX()->GetNbinsX();
    phi_mu_X_nbinsY = phi_mu_X_BD_to_JPSI_K_PI->ProjectionY()->GetNbinsX();
    JpsiK_2d_control_nbinsX = JpsiK_2d_control_BD_to_JPSI_K_PI->ProjectionX()->GetNbinsX();
    JpsiK_2d_control_nbinsY = JpsiK_2d_control_BD_to_JPSI_K_PI->ProjectionY()->GetNbinsX();
    KK_1d_control_nbins = KK_1d_control_BD_to_JPSI_K_PI->GetNbinsX();
    Jpsip_2d_controlLb_nbinsX = Jpsip_2d_controlLb_BD_to_JPSI_K_PI->ProjectionX()->GetNbinsX();
    Jpsip_2d_controlLb_nbinsY = Jpsip_2d_controlLb_BD_to_JPSI_K_PI->ProjectionY()->GetNbinsX();
    pK_2d_controlLb_nbinsX = pK_2d_controlLb_BD_to_JPSI_K_PI->ProjectionX()->GetNbinsX();
    pK_2d_controlLb_nbinsY = pK_2d_controlLb_BD_to_JPSI_K_PI->ProjectionY()->GetNbinsX();
    JpsiK_2d_controlLb_nbinsX = JpsiK_2d_controlLb_BD_to_JPSI_K_PI->ProjectionX()->GetNbinsX();
    JpsiK_2d_controlLb_nbinsY = JpsiK_2d_controlLb_BD_to_JPSI_K_PI->ProjectionY()->GetNbinsX();
    
    TMinuit *Min = new TMinuit(230);
    Min->SetFCN(signal_area_fit_func);
    
    Double_t vstart[230];
    Double_t step[230];
    
    for(int i = 0; i <= 213; i++)
    {
        vstart[i] = (*vpar)(i);
    }
    
    if ((*vpar).GetNrows() < 222)
    {
        for(int i = 214; i <= 216; i++)
        {
            vstart[i] = 100.;
        }
        vstart[217] = 0.;
        vstart[218] = 0.;
    }
    else
    {   
        for(int i = 214; i <= 218; i++)
        {
            vstart[i] = (*vpar)(i);
        }
    }
    
    if ((*vpar).GetNrows() < 233)
    {
        vstart[219] = 0.;
        vstart[220] = 1.;
        vstart[221] = 0.;
        vstart[222] = 0.;
        vstart[223] = 0.;
        vstart[224] = 0.;
        vstart[225] = 1.;
        vstart[226] = 1.;
        vstart[227] = 0.;
        vstart[228] = 0.;
        vstart[229] = 7.;
    }
    else
    {   
        for(int i = 219; i <= 229; i++)
        {
            vstart[i] = (*vpar)(i);
        }
    }
    
    TRandom *tr = new TRandom();
    tr->SetSeed(rand());

    /*for(int i = 2; i < 44; i = i + 2)
    {   
        vstart[i] += tr->Gaus(0., vstart[i] / 10.);
    }

    for(int i = 3; i < 44; i = i + 2)
    {   
        vstart[i] += tr->Gaus(0., 0.15);
    }

    vstart[44] += tr->Gaus(0., 0.04);
    vstart[45] += tr->Gaus(0., 0.02);

    for(int i = 49; i < 77; i = i + 2)
    {   
        vstart[i] += tr->Gaus(0., vstart[i] / 10.);
    }

    for(int i = 50; i < 77; i = i + 2)
    {   
        vstart[i] += tr->Gaus(0., 0.15);
    }*/
    
    /*for(int i = 80; i < 85; i = i + 1)
    {   
        vstart[i] += tr->Gaus(0., vstart[i] / 20.);
    }*/
    
    Double_t step_c[121] = {0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.01, 0.01, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 100., 100., 100., 100., 100., 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01}; // 3.5746
    
    for(int i = 0; i <= 120; i++)
    {
        step[i] = step_c[i];
    }
    
    for(int i = 121; i <= 208; i++)
    {
        if (i % 2 == 1) {step[i] = 0.1;}
        if (i % 2 == 0) {step[i] = 0.05;}
    }
    
    for(int i = 209; i <= 212; i++)
    {
        step[i] = 0.01;
    }
    
    step[213] = 100.;
    step[214] = 50.;
    step[215] = 50.;
    step[216] = 50.;
    step[217] = 0.1;
    step[218] = 0.05;
    
    for(int i = 219; i <= 229; i++)
    {
        step[i] = 0.01;
    }
    
    //Double_t vstart[46] = {3.5746, -7.93451, 1.31998, 1.84057, 20.0575, 16.5005, -1.24044, -18.9953, -418.7, -16.0238, 0.279479, 11.2291, 496.636, 0.00180622, 26.9619, 26.9619, 1.88133, 11.6268, 6.36857, -18.83, 0.00960553, 2.52659, 53.4462, -6.44662, 1.45676, -0.922654, 0.354629, 5.19828, 400.057, 93.6205, -320.021, 26.3613, 0.00956211, 0.00350352, 133.805, -15.3857, 0.00827755, -1.98554, -15675, 0, 11.2659, 19.0313, 84.1563, 0.586158, 0.306206, 4.196};
    
    /*{2.64828e+00, 0.00000e+00, 3.84354e-01, 1.53425e+01, 2.44836e+01, -3.79378e+00, 2.21921e+00, 1.03745e+01, 8.62417e+01,  -4.17078e+00, 2.88211e-01,  -1.20179e+01, 1.38063e+04, 4.34186e+00 ,  6.19396e+01, -1.18442e+01, 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.,  6.19573e+00, 9.84581e-01, 0., 0., 2.88074e-01, 4.16808e+00};*/
    
    /*Double_t vstart[46] = {-3065.02 * 1.19, 381.83 * 1.19, 1388.59 * 0.67, -914.75 * 0.67, 19129.9 * 1., -26370.9 * 1., -252.664 * 0.57, 1715.69 * 0.57, -192903, -257736, -730.154, -73.6031, 11.507, -48.5535, 0.698198, 0.247431, 1, 1, 1, 1, -47375.9, -47375.9, -2968.64 * 1.19, 2087.29 * 1.19, -4945.11 * 1.19, -9219.04 * 1.19, -140319 * 1., 10000 * 1., -31857.6 * 1., -28272.1 * 1., 2141.32 * 0.57, 1588.98 * 0.57, -13672.2 * 0.57, -3695.75 * 0.57, 14467.7, 1e+06, 84284.5, 60720.6, -835.321, 1e+06, 204870, -101914, 6991.91, 1e+06, 100000, 1e+06};
    Double_t step[46] = {12.2017, 73.9241, 24.7675, 8.38426, 944.872, 854.802, 22.8486, 57.8146, 2832.82, 2161.62, 5.04214, 3.61082, 1.99999, 1.41421, 0.178626, 0.0999774, 0, 0, 0, 0, 1254.57, 1254.57, 623.995, 694.626, 473.547, 77.5703, 1.41421, 1.41421, 682.663, 1669.37, 665.407, 161.344, 140.081, 289.615, 1.41421, 1.41421, 5846.32, 3899.66, 1.41421, 1.41421, 2599.36, 6568.49, 1.41421, 1.41421, 1.41421, 1.41421};*/
    /*{3.5746, -7.93451, 1.31998, 1.84057, 20.0575, 16.5005, -1.24044, -18.9953, -418.7, -16.0238, 0.279479, 11.2291, 496.636, 0.00180622, 26.9619, 26.9619, 1.88133, 11.6268, 6.36857, -18.83, 0.00960553, 2.52659, 53.4462, -6.44662, 1.45676, -0.922654, 0.354629, 5.19828, 400.057, 93.6205, -320.021, 26.3613, 0.00956211, 0.00350352, 133.805, -15.3857, 0.00827755, -1.98554, -15675, 0, 11.2659, 19.0313, 84.1563, 0.586158, 0.306206, 4.196};*/ //<-latest from meeting
    
/*{3.5746, -7.93451, 1.31998, 1.84057, 20.0575, 16.5005, -1.24044, -18.9953, -418.7, -16.0238, 0.279479, 11.2291, 496.636, 0.00180622, 26.9619, 26.9619, 1.88133, 11.6268, 6.36857, -18.83, 0.00960553, 2.52659, 53.4462, -6.44662, 1.45676, -0.922654, 0.354629, 5.19828, 400.057, 93.6205, -320.021, 26.3613, 0.00956211, 0.00350352, 133.805, -15.3857, 0.00827755, -1.98554, -15675, 0, 11.2659, 19.0313, 84.1563, 0.586158, 0.306206, 4.196}*/
/*{3.5746, -7.96312, 1.32082, 1.84057, 20.0575, 16.5005, -1.24044, -18.9953, -435.731, -16.0238, 0.279479, 11.2291, 496.636, 0.00180622, 26.9619, 26.9619, 1.88133, 11.6255, 6.36857, -18.83, 0.00960553, 2.52659, 53.4462, -6.44662, 1.45676, -0.922654, 0.354629, 5.19828, 400.057, 93.6205, -320.021, 26.3613, 0.00956211, 0.00350352, 133.805, -15.4944, 0.00827755, -1.98554, -15675, 0, 11.2659, 19.0313, 84.1563, 0.586158, 0.306206, 4.196}*/
// { , , , , , , , , , , , , , , , , 1., 1., 1., 1., , , , , , , , , , , , , , , , , , , , , , , , , , };
    
    Int_t ierflg;
    
    /*vstart[14] = 1.;
    vstart[32] = 0.;
    vstart[34] = 0.;
    
    for(int i = 16; i <= 39; i++)
    {
        vstart[i] = 0.;
    }
    
    vstart[0] = 1.;
    vstart[2] = 1.;
    vstart[4] = 1.;
    vstart[6] = 1.;
    vstart[8] = 1.;
    vstart[10] = 1.;
    vstart[12] = 1.;*/
    
    //vstart[83] = vstart[83] - 434.; // for systematics
    //vstart[80] = vstart[80] - vstart[80] * 527. / 25188.; // for systematics
    //vstart[81] = vstart[81] - vstart[81] * 527. / 25188.; // for systematics
    //vstart[82] = vstart[82] - vstart[82] * 527. / 25188.; // for systematics
    //vstart[84] = vstart[84] - vstart[84] * 527. / 25188.; // for systematics
    
    Min->mnparm(0, "K1410_1_Bw1_amp", vstart[0], step[0], 0., 1.0e12, ierflg);
    Min->mnparm(1, "K1410_1_Bw1_phi", vstart[1], step[1], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(2, "K1430_0_Bw1_amp", vstart[2], step[2], 0., 1.0e12, ierflg);
    Min->mnparm(3, "K1430_0_Bw1_phi", vstart[3], step[3], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(4, "K1430_2_Bw1_amp", vstart[4], step[4], 0., 1.0e12, ierflg);
    Min->mnparm(5, "K1430_2_Bw1_phi", vstart[5], step[5], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(6, "K1680_1_Bw1_amp", vstart[6], step[6], 0., 1.0e12, ierflg);
    Min->mnparm(7, "K1680_1_Bw1_phi", vstart[7], step[7], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(8, "K1780_3_Bw1_amp", vstart[8], step[8], 0., 1.0e12, ierflg);
    Min->mnparm(9, "K1780_3_Bw1_phi", vstart[9], step[9], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(10, "K1950_0_Bw1_amp", vstart[10], step[10], 0., 1.0e12, ierflg);
    Min->mnparm(11, "K1950_0_Bw1_phi", vstart[11], step[11], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(12, "K2045_4_Bw1_amp", vstart[12], step[12], 0., 1.0e12, ierflg);
    Min->mnparm(13, "K2045_4_Bw1_phi", vstart[13], step[13], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(14, "K1980_2_Bw1_amp", vstart[14], step[14], 0., 1.0e12, ierflg);
    Min->mnparm(15, "K1980_2_Bw1_phi", vstart[15], step[15], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(16, "K1410_1_Bw2_amp", vstart[16], step[16], 0., 1.0e12, ierflg);
    Min->mnparm(17, "K1410_1_Bw2_phi", vstart[17], step[17], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(18, "K1410_1_Bw3_amp", vstart[18], step[18], 0., 1.0e12, ierflg);
    Min->mnparm(19, "K1410_1_Bw3_phi", vstart[19], step[19], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(20, "K1430_2_Bw2_amp", vstart[20], step[20], 0., 1.0e12, ierflg);
    Min->mnparm(21, "K1430_2_Bw2_phi", vstart[21], step[21], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(22, "K1430_2_Bw3_amp", vstart[22], step[22], 0., 1.0e12, ierflg);
    Min->mnparm(23, "K1430_2_Bw3_phi", vstart[23], step[23], -8.0*PI, 8.0*PI, ierflg);
	Min->mnparm(24, "K1680_1_Bw2_amp", vstart[24], step[24], 0., 1.0e12, ierflg);
    Min->mnparm(25, "K1680_1_Bw2_phi", vstart[25], step[25], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(26, "K1680_1_Bw3_amp", vstart[26], step[26], 0., 1.0e12, ierflg);
    Min->mnparm(27, "K1680_1_Bw3_phi", vstart[27], step[27], -8.0*PI, 8.0*PI, ierflg);
	Min->mnparm(28, "K1780_3_Bw2_amp", vstart[28], step[28], 0., 1.0e12, ierflg);
    Min->mnparm(29, "K1780_3_Bw2_phi", vstart[29], step[29], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(30, "K1780_3_Bw3_amp", vstart[30], step[30], 0., 1.0e12, ierflg);
    Min->mnparm(31, "K1780_3_Bw3_phi", vstart[31], step[31], -8.0*PI, 8.0*PI, ierflg);
	Min->mnparm(32, "K1980_2_Bw2_amp", vstart[32], step[32], 0., 1.0e12, ierflg);
    Min->mnparm(33, "K1980_2_Bw2_phi", vstart[33], step[33], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(34, "K1980_2_Bw3_amp", vstart[34], step[34], 0., 1.0e12, ierflg);
    Min->mnparm(35, "K1980_2_Bw3_phi", vstart[35], step[35], -8.0*PI, 8.0*PI, ierflg);
	Min->mnparm(36, "K2045_4_Bw2_amp", vstart[36], step[36], 0., 1.0e12, ierflg);
    Min->mnparm(37, "K2045_4_Bw2_phi", vstart[37], step[37], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(38, "K2045_4_Bw3_amp", vstart[38], step[38], 0., 1.0e12, ierflg);
    Min->mnparm(39, "K2045_4_Bw3_phi", vstart[39], step[39], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(40, "X1Bs1_amp", vstart[40], step[40], 0., 1.0e12, ierflg);
    Min->mnparm(41, "X1Bs1_phi", vstart[41], step[41], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(42, "X1Bs2_amp", vstart[42], step[42], 0., 1.0e12, ierflg);
    Min->mnparm(43, "X1Bs2_phi", vstart[43], step[43], -8.0*PI, 8.0*PI, ierflg);
	Min->mnparm(44, "gamma_Zc4200", vstart[44], step[44], 0., 1.2, ierflg);
	Min->mnparm(45, "m_Zc4200", vstart[45], step[45], 3.9, 4.7, ierflg);
    Min->mnparm(46, "n_BD_to_JPSI_K_PI_par", vstart[46], step[46], 0, 0, ierflg);
    Min->mnparm(47, "phi1680_1_Bw1_amp", vstart[47], step[47], 0., 1.0e12, ierflg);
    Min->mnparm(48, "phi1680_1_Bw1_phi", vstart[48], step[48], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(49, "phi1680_1_Bw2_amp", vstart[49], step[49], 0., 1.0e12, ierflg);
    Min->mnparm(50, "phi1680_1_Bw2_phi", vstart[50], step[50], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(51, "phi1680_1_Bw3_amp", vstart[51], step[51], 0., 1.0e12, ierflg);
    Min->mnparm(52, "phi1680_1_Bw3_phi", vstart[52], step[52], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(53, "f1525_2_Bw1_amp", vstart[53], step[53], 0., 1.0e12, ierflg);
    Min->mnparm(54, "f1525_2_Bw1_phi", vstart[54], step[54], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(55, "f1525_2_Bw2_amp", vstart[55], step[55], 0., 1.0e12, ierflg);
    Min->mnparm(56, "f1525_2_Bw2_phi", vstart[56], step[56], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(57, "f1525_2_Bw3_amp", vstart[57], step[57], 0., 1.0e12, ierflg);
    Min->mnparm(58, "f1525_2_Bw3_phi", vstart[58], step[58], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(59, "f1640_2_Bw1_amp", vstart[59], step[59], 0., 1.0e12, ierflg);
    Min->mnparm(60, "f1640_2_Bw1_phi", vstart[60], step[60], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(61, "f1640_2_Bw2_amp", vstart[61], step[61], 0., 1.0e12, ierflg);
    Min->mnparm(62, "f1640_2_Bw2_phi", vstart[62], step[62], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(63, "f1640_2_Bw3_amp", vstart[63], step[63], 0., 1.0e12, ierflg);
    Min->mnparm(64, "f1640_2_Bw3_phi", vstart[64], step[64], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(65, "f1750_2_Bw1_amp", vstart[65], step[65], 0., 1.0e12, ierflg);
    Min->mnparm(66, "f1750_2_Bw1_phi", vstart[66], step[66], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(67, "f1750_2_Bw2_amp", vstart[67], step[67], 0., 1.0e12, ierflg);
    Min->mnparm(68, "f1750_2_Bw2_phi", vstart[68], step[68], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(69, "f1750_2_Bw3_amp", vstart[69], step[69], 0., 1.0e12, ierflg);
    Min->mnparm(70, "f1750_2_Bw3_phi", vstart[70], step[70], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(71, "f1950_2_Bw1_amp", vstart[71], step[71], 0., 1.0e12, ierflg);
    Min->mnparm(72, "f1950_2_Bw1_phi", vstart[72], step[72], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(73, "f1950_2_Bw2_amp", vstart[73], step[73], 0., 1.0e12, ierflg);
    Min->mnparm(74, "f1950_2_Bw2_phi", vstart[74], step[74], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(75, "f1950_2_Bw3_amp", vstart[75], step[75], 0., 1.0e12, ierflg);
    Min->mnparm(76, "f1950_2_Bw3_phi", vstart[76], step[76], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(77, "NR_Bw1_amp", vstart[77], step[77], 0., 1.0e12, ierflg);
    Min->mnparm(78, "NR_Bw1_phi", vstart[78], step[78], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(79, "n_BS_to_JPSI_K_K_par", vstart[79], step[79], 0, 0, ierflg);
    Min->mnparm(80, "n_lambda", vstart[80], step[80], 0, 0, ierflg);
    Min->mnparm(81, "n_BS_PIPI", vstart[81], step[81], 0, 0, ierflg);
    Min->mnparm(82, "n_BD_PIPI", vstart[82], step[82], 0, 0, ierflg);
    Min->mnparm(83, "n_BD_KPI", vstart[83], step[83], 0, 0, ierflg);
    Min->mnparm(84, "n_BS_KK", vstart[84], step[84], 0, 0, ierflg);
    Min->mnparm(85, "p0", vstart[85], step[85], 0, 0, ierflg);
    Min->mnparm(86, "p1", vstart[86], step[86], 0, 0, ierflg);
    Min->mnparm(87, "p2", vstart[87], step[87], 0, 0, ierflg);
    Min->mnparm(88, "p3", vstart[88], step[88], 0, 0, ierflg);
    Min->mnparm(89, "a0", vstart[89], step[89], 0, 0, ierflg);
    Min->mnparm(90, "b0", vstart[90], step[90], 0, 0, ierflg);
    Min->mnparm(91, "b1", vstart[91], step[91], 0, 0, ierflg);
    Min->mnparm(92, "b2", vstart[92], step[92], 0, 0, ierflg);
    Min->mnparm(93, "x0_KK", vstart[93], step[93], 0, 0, ierflg);
    Min->mnparm(94, "d0_KK", vstart[94], step[94], 0, 0, ierflg);
    Min->mnparm(95, "p0_KK", vstart[95], step[95], 0, 0, ierflg);
    Min->mnparm(96, "p1_KK", vstart[96], step[96], 0, 0, ierflg);
    Min->mnparm(97, "p2_KK", vstart[97], step[97], 0, 0, ierflg);
    Min->mnparm(98, "p3_KK", vstart[98], step[98], 0, 0, ierflg);
    Min->mnparm(99, "p4_KK", vstart[99], step[99], 0, 0, ierflg);
    Min->mnparm(100, "p5_KK", vstart[100], step[100], 0, 0, ierflg);
    Min->mnparm(101, "x0_pipi", vstart[101], step[101], 0, 0, ierflg);
    Min->mnparm(102, "d0_pipi", vstart[102], step[102], 0, 0, ierflg);
    Min->mnparm(103, "p0_pipi", vstart[103], step[103], 0, 0, ierflg);
    Min->mnparm(104, "p1_pipi", vstart[104], step[104], 0, 0, ierflg);
    Min->mnparm(105, "p2_pipi", vstart[105], step[105], 0, 0, ierflg);
    Min->mnparm(106, "p3_pipi", vstart[106], step[106], 0, 0, ierflg);
    Min->mnparm(107, "p4_pipi", vstart[107], step[107], 0, 0, ierflg);
    Min->mnparm(108, "p5_pipi", vstart[108], step[108], 0, 0, ierflg);
    Min->mnparm(109, "p6_pipi", vstart[109], step[109], 0, 0, ierflg);
    Min->mnparm(110, "x0_pKKp", vstart[110], step[110], 0, 0, ierflg);
    Min->mnparm(111, "d0_pKKp", vstart[111], step[111], 0, 0, ierflg);
    Min->mnparm(112, "p0_pKKp", vstart[112], step[112], 0, 0, ierflg);
    Min->mnparm(113, "p1_pKKp", vstart[113], step[113], 0, 0, ierflg);
    Min->mnparm(114, "p2_pKKp", vstart[114], step[114], 0, 0, ierflg);
    Min->mnparm(115, "p3_pKKp", vstart[115], step[115], 0, 0, ierflg);
    Min->mnparm(116, "a0_pKKp", vstart[116], step[116], 0, 0, ierflg);
    Min->mnparm(117, "b0_pKKp", vstart[117], step[117], 0, 0, ierflg);
    Min->mnparm(118, "b1_pKKp", vstart[118], step[118], 0, 0, ierflg);
    Min->mnparm(119, "b2_pKKp", vstart[119], step[119], 0, 0, ierflg);
    Min->mnparm(120, "u0_pKKp", vstart[120], step[120], 0, 0, ierflg);
    Min->mnparm(121, "Ls1800_12_Bw1_amp", vstart[121], step[121], 0, 0, ierflg);
    Min->mnparm(122, "Ls1800_12_Bw1_phi", vstart[122], step[122], 0, 0, ierflg);
    Min->mnparm(123, "Ls1800_12_Bw2_amp", vstart[123], step[123], 0, 0, ierflg);
    Min->mnparm(124, "Ls1800_12_Bw2_phi", vstart[124], step[124], 0, 0, ierflg);
    Min->mnparm(125, "Ls1800_12_Bw3_amp", vstart[125], step[125], 0, 0, ierflg);
    Min->mnparm(126, "Ls1800_12_Bw3_phi", vstart[126], step[126], 0, 0, ierflg);
    Min->mnparm(127, "Ls1800_12_Bw4_amp", vstart[127], step[127], 0, 0, ierflg);
    Min->mnparm(128, "Ls1800_12_Bw4_phi", vstart[128], step[128], 0, 0, ierflg);
    Min->mnparm(129, "Ls1810_12_Bw1_amp", vstart[129], step[129], 0, 0, ierflg);
    Min->mnparm(130, "Ls1810_12_Bw1_phi", vstart[130], step[130], 0, 0, ierflg);
    Min->mnparm(131, "Ls1810_12_Bw2_amp", vstart[131], step[131], 0, 0, ierflg);
    Min->mnparm(132, "Ls1810_12_Bw2_phi", vstart[132], step[132], 0, 0, ierflg);
    Min->mnparm(133, "Ls1810_12_Bw3_amp", vstart[133], step[133], 0, 0, ierflg);
    Min->mnparm(134, "Ls1810_12_Bw3_phi", vstart[134], step[134], 0, 0, ierflg);
    Min->mnparm(135, "Ls1820_52_Bw1_amp", vstart[135], step[135], 0, 0, ierflg);
    Min->mnparm(136, "Ls1820_52_Bw1_phi", vstart[136], step[136], 0, 0, ierflg);
    Min->mnparm(137, "Ls1820_52_Bw2_amp", vstart[137], step[137], 0, 0, ierflg);
    Min->mnparm(138, "Ls1820_52_Bw2_phi", vstart[138], step[138], 0, 0, ierflg);
    Min->mnparm(139, "Ls1820_52_Bw3_amp", vstart[139], step[139], 0, 0, ierflg);
    Min->mnparm(140, "Ls1820_52_Bw3_phi", vstart[140], step[140], 0, 0, ierflg);
    Min->mnparm(141, "Ls1830_52_Bw1_amp", vstart[141], step[141], 0, 0, ierflg);
    Min->mnparm(142, "Ls1830_52_Bw1_phi", vstart[142], step[142], 0, 0, ierflg);
    Min->mnparm(143, "Ls1830_52_Bw2_amp", vstart[143], step[143], 0, 0, ierflg);
    Min->mnparm(144, "Ls1830_52_Bw2_phi", vstart[144], step[144], 0, 0, ierflg);
    Min->mnparm(145, "Ls1830_52_Bw3_amp", vstart[145], step[145], 0, 0, ierflg);
    Min->mnparm(146, "Ls1830_52_Bw3_phi", vstart[146], step[146], 0, 0, ierflg);
    Min->mnparm(147, "Ls1890_32_Bw1_amp", vstart[147], step[147], 0, 0, ierflg);
    Min->mnparm(148, "Ls1890_32_Bw1_phi", vstart[148], step[148], 0, 0, ierflg);
    Min->mnparm(149, "Ls1890_32_Bw2_amp", vstart[149], step[149], 0, 0, ierflg);
    Min->mnparm(150, "Ls1890_32_Bw2_phi", vstart[150], step[150], 0, 0, ierflg);
    Min->mnparm(151, "Ls1890_32_Bw3_amp", vstart[151], step[151], 0, 0, ierflg);
    Min->mnparm(152, "Ls1890_32_Bw3_phi", vstart[152], step[152], 0, 0, ierflg);
    Min->mnparm(153, "Ls2020_72_Bw1_amp", vstart[153], step[153], 0, 0, ierflg);
    Min->mnparm(154, "Ls2020_72_Bw1_phi", vstart[154], step[154], 0, 0, ierflg);
    Min->mnparm(155, "Ls2020_72_Bw2_amp", vstart[155], step[155], 0, 0, ierflg);
    Min->mnparm(156, "Ls2020_72_Bw2_phi", vstart[156], step[156], 0, 0, ierflg);
    Min->mnparm(157, "Ls2020_72_Bw3_amp", vstart[157], step[157], 0, 0, ierflg);
    Min->mnparm(158, "Ls2020_72_Bw3_phi", vstart[158], step[158], 0, 0, ierflg);
    Min->mnparm(159, "Ls2050_32_Bw1_amp", vstart[159], step[159], 0, 0, ierflg);
    Min->mnparm(160, "Ls2050_32_Bw1_phi", vstart[160], step[160], 0, 0, ierflg);
    Min->mnparm(161, "Ls2050_32_Bw2_amp", vstart[161], step[161], 0, 0, ierflg);
    Min->mnparm(162, "Ls2050_32_Bw2_phi", vstart[162], step[162], 0, 0, ierflg);
    Min->mnparm(163, "Ls2050_32_Bw3_amp", vstart[163], step[163], 0, 0, ierflg);
    Min->mnparm(164, "Ls2050_32_Bw3_phi", vstart[164], step[164], 0, 0, ierflg);
    Min->mnparm(165, "Ls2100_72_Bw1_amp", vstart[165], step[165], 0, 0, ierflg);
    Min->mnparm(166, "Ls2100_72_Bw1_phi", vstart[166], step[166], 0, 0, ierflg);
    Min->mnparm(167, "Ls2100_72_Bw2_amp", vstart[167], step[167], 0, 0, ierflg);
    Min->mnparm(168, "Ls2100_72_Bw2_phi", vstart[168], step[168], 0, 0, ierflg);
    Min->mnparm(169, "Ls2100_72_Bw3_amp", vstart[169], step[169], 0, 0, ierflg);
    Min->mnparm(170, "Ls2100_72_Bw3_phi", vstart[170], step[170], 0, 0, ierflg);
    Min->mnparm(171, "Ls2110_52_Bw1_amp", vstart[171], step[171], 0, 0, ierflg);
    Min->mnparm(172, "Ls2110_52_Bw1_phi", vstart[172], step[172], 0, 0, ierflg);
    Min->mnparm(173, "Ls2110_52_Bw2_amp", vstart[173], step[173], 0, 0, ierflg);
    Min->mnparm(174, "Ls2110_52_Bw2_phi", vstart[174], step[174], 0, 0, ierflg);
    Min->mnparm(175, "Ls2110_52_Bw3_amp", vstart[175], step[175], 0, 0, ierflg);
    Min->mnparm(176, "Ls2110_52_Bw3_phi", vstart[176], step[176], 0, 0, ierflg);
    Min->mnparm(177, "Ls2325_32_Bw1_amp", vstart[177], step[177], 0, 0, ierflg);
    Min->mnparm(178, "Ls2325_32_Bw1_phi", vstart[178], step[178], 0, 0, ierflg);
    Min->mnparm(179, "Ls2325_32_Bw2_amp", vstart[179], step[179], 0, 0, ierflg);
    Min->mnparm(180, "Ls2325_32_Bw2_phi", vstart[180], step[180], 0, 0, ierflg);
    Min->mnparm(181, "Ls2325_32_Bw3_amp", vstart[181], step[181], 0, 0, ierflg);
    Min->mnparm(182, "Ls2325_32_Bw3_phi", vstart[182], step[182], 0, 0, ierflg);
    Min->mnparm(183, "Ls2350_92_Bw1_amp", vstart[183], step[183], 0, 0, ierflg);
    Min->mnparm(184, "Ls2350_92_Bw1_phi", vstart[184], step[184], 0, 0, ierflg);
    Min->mnparm(185, "Ls2350_92_Bw2_amp", vstart[185], step[185], 0, 0, ierflg);
    Min->mnparm(186, "Ls2350_92_Bw2_phi", vstart[186], step[186], 0, 0, ierflg);
    Min->mnparm(187, "LsNR_12_Bw_amp", vstart[187], step[187], 0, 0, ierflg);
    Min->mnparm(188, "LsNR_12_Bw_phi", vstart[188], step[188], 0, 0, ierflg);
    Min->mnparm(189, "Pc1Bw1_amp", vstart[189], step[189], 0, 0, ierflg);
    Min->mnparm(190, "Pc1Bw1_phi", vstart[190], step[190], 0, 0, ierflg);
    Min->mnparm(191, "Pc1Bw2_amp", vstart[191], step[191], 0, 0, ierflg);
    Min->mnparm(192, "Pc1Bw2_phi", vstart[192], step[192], 0, 0, ierflg);
    Min->mnparm(193, "Pc1Bs1_amp", vstart[193], step[193], 0, 0, ierflg);
    Min->mnparm(194, "Pc1Bs1_phi", vstart[194], step[194], 0, 0, ierflg);
    Min->mnparm(195, "Pc1Bs2_amp", vstart[195], step[195], 0, 0, ierflg);
    Min->mnparm(196, "Pc1Bs2_phi", vstart[196], step[196], 0, 0, ierflg);
    Min->mnparm(197, "Pc1Bs3_amp", vstart[197], step[197], 0, 0, ierflg);
    Min->mnparm(198, "Pc1Bs3_phi", vstart[198], step[198], 0, 0, ierflg);
    Min->mnparm(199, "Pc2Bw1_amp", vstart[199], step[199], 0, 0, ierflg);
    Min->mnparm(200, "Pc2Bw1_phi", vstart[200], step[200], 0, 0, ierflg);
    Min->mnparm(201, "Pc2Bw2_amp", vstart[201], step[201], 0, 0, ierflg);
    Min->mnparm(202, "Pc2Bw2_phi", vstart[202], step[202], 0, 0, ierflg);
    Min->mnparm(203, "Pc2Bs1_amp", vstart[203], step[203], 0, 0, ierflg);
    Min->mnparm(204, "Pc2Bs1_phi", vstart[204], step[204], 0, 0, ierflg);
    Min->mnparm(205, "Pc2Bs2_amp", vstart[205], step[205], 0, 0, ierflg);
    Min->mnparm(206, "Pc2Bs2_phi", vstart[206], step[206], 0, 0, ierflg);
    Min->mnparm(207, "Pc2Bs3_amp", vstart[207], step[207], 0, 0, ierflg);
    Min->mnparm(208, "Pc2Bs3_phi", vstart[208], step[208], 0, 0, ierflg);
    Min->mnparm(209, "M01", vstart[209], step[209], 0, 0, ierflg);
    Min->mnparm(210, "M02", vstart[210], step[210], 0, 0, ierflg);
    Min->mnparm(211, "gamma1", vstart[211], step[211], 0, 0, ierflg);
    Min->mnparm(212, "gamma2", vstart[212], step[212], 0, 0, ierflg);
    Min->mnparm(213, "comb_bg_norm", vstart[213], step[213], 0, 0, ierflg);
    Min->mnparm(214, "n_LB_PPI", vstart[214], step[214], 0., 10000., ierflg);
    Min->mnparm(215, "n_BD_KK", vstart[215], step[215], 0., 10000., ierflg);
    Min->mnparm(216, "n_BS_KPI", vstart[216], step[216], 0., 10000., ierflg);
    Min->mnparm(217, "KNR_0_Bw_amp", vstart[217], step[217], 0., 1.0e12, ierflg);
    Min->mnparm(218, "KNR_0_Bw_phi", vstart[218], step[218], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(219, "x0_ppipip", vstart[219], step[219], 0, 0, ierflg);
    Min->mnparm(220, "d0_ppipip", vstart[220], step[220], 0, 0, ierflg);
    Min->mnparm(221, "p0_ppipip", vstart[221], step[221], 0, 0, ierflg);
    Min->mnparm(222, "p1_ppipip", vstart[222], step[222], 0, 0, ierflg);
    Min->mnparm(223, "p2_ppipip", vstart[223], step[223], 0, 0, ierflg);
    Min->mnparm(224, "p3_ppipip", vstart[224], step[224], 0, 0, ierflg);
    Min->mnparm(225, "a0_ppipip", vstart[225], step[225], 0, 0, ierflg);
    Min->mnparm(226, "b0_ppipip", vstart[226], step[226], 0, 0, ierflg);
    Min->mnparm(227, "b1_ppipip", vstart[227], step[227], 0, 0, ierflg);
    Min->mnparm(228, "b2_ppipip", vstart[228], step[228], 0, 0, ierflg);
    Min->mnparm(229, "u0_ppipip", vstart[229], step[229], 0, 0, ierflg);    
    
    Min->FixParameter(85); //should always be fixedd as p0 for comb bg
    Min->FixParameter(103); //should always be fixedd as p0 for comb bg
    Min->FixParameter(95); //should always be fixedd as p0 for comb bg
    Min->FixParameter(112); //should always be fixedd as p0 for comb bg
    Min->FixParameter(221);
    
    /*Min->FixParameter(0); // 1410_1
    Min->FixParameter(1); // 1410_1
    Min->FixParameter(2); // 1430_0
    Min->FixParameter(3); // 1430_0
    Min->FixParameter(4); // 1430_2
    Min->FixParameter(5); // 1430_2
    Min->FixParameter(6);
    Min->FixParameter(7);
    Min->FixParameter(8);
    Min->FixParameter(9);
    Min->FixParameter(10);
    Min->FixParameter(11);
    Min->FixParameter(12);
    Min->FixParameter(13);
    Min->FixParameter(14);
    Min->FixParameter(15);
    Min->FixParameter(16); // 1410_1
    Min->FixParameter(17); // 1410_1
    Min->FixParameter(18); // 1410_1
    Min->FixParameter(19); // 1410_1
    Min->FixParameter(20); // 1430_2
    Min->FixParameter(21); // 1430_2
    Min->FixParameter(22); // 1430_2
    Min->FixParameter(23); // 1430_2
    Min->FixParameter(24);
    Min->FixParameter(25);
    Min->FixParameter(26);
    Min->FixParameter(27);
    Min->FixParameter(28);
    Min->FixParameter(29);
    Min->FixParameter(30);
    Min->FixParameter(31);
    Min->FixParameter(32);
    Min->FixParameter(33);
    Min->FixParameter(34);
    Min->FixParameter(35);
    Min->FixParameter(36);
    Min->FixParameter(37);
    Min->FixParameter(38);
    Min->FixParameter(39);
	Min->FixParameter(40); // Zc parameters should be fixed
    Min->FixParameter(41); // should be fixed
    Min->FixParameter(42); // should be fixed
    Min->FixParameter(43); // should be fixed
    Min->FixParameter(44); // should be fixed
    Min->FixParameter(45); // should be fixed
    Min->FixParameter(46); // it should always be fixed
    Min->FixParameter(47); // Bs->JpsiKK matrix element starts
    Min->FixParameter(48);
    Min->FixParameter(49);
    Min->FixParameter(50);
    Min->FixParameter(51);
    Min->FixParameter(52);
    Min->FixParameter(53);
    Min->FixParameter(54);
    Min->FixParameter(55);
    Min->FixParameter(56);
    Min->FixParameter(57);
    Min->FixParameter(58);
    Min->FixParameter(59);
    Min->FixParameter(60);
    Min->FixParameter(61);
    Min->FixParameter(62);
    Min->FixParameter(63);
    Min->FixParameter(64);
    Min->FixParameter(65);
    Min->FixParameter(66);
    Min->FixParameter(67);
    Min->FixParameter(68);
    Min->FixParameter(69);
    Min->FixParameter(70);
    Min->FixParameter(71);
    Min->FixParameter(72);
    Min->FixParameter(73);
    Min->FixParameter(74);
    Min->FixParameter(75);
    Min->FixParameter(76);
    Min->FixParameter(77);
    Min->FixParameter(78);
    Min->FixParameter(79); // it should always be fixed */
    /*Min->FixParameter(80);
    Min->FixParameter(81);
    Min->FixParameter(82);
    Min->FixParameter(83);
    Min->FixParameter(84);*/
    /*Min->FixParameter(85);
    Min->FixParameter(86);
    Min->FixParameter(87);
    Min->FixParameter(88);
    Min->FixParameter(89);
    Min->FixParameter(90);
    Min->FixParameter(91);
    Min->FixParameter(92);
    Min->FixParameter(93);
    Min->FixParameter(94);
    Min->FixParameter(95);
    Min->FixParameter(96);
    Min->FixParameter(97);
    Min->FixParameter(98);
    Min->FixParameter(99); // that is unused parameters
    Min->FixParameter(100); // that is unused parameters
    Min->FixParameter(101);
    Min->FixParameter(102);
    Min->FixParameter(103);
    Min->FixParameter(104);
    Min->FixParameter(105);
    Min->FixParameter(106);
    Min->FixParameter(107);
    Min->FixParameter(108); // that is unused parameters
    Min->FixParameter(109); // that is unused parameters */
    /*Min->FixParameter(110);
    Min->FixParameter(111);
    Min->FixParameter(112);
    Min->FixParameter(113);
    Min->FixParameter(114);
    Min->FixParameter(115);
    Min->FixParameter(116);
    Min->FixParameter(117);
    Min->FixParameter(118);
    Min->FixParameter(119);
    Min->FixParameter(120);*/
    /*Min->FixParameter(121);
    Min->FixParameter(122);
    Min->FixParameter(123);
    Min->FixParameter(124);
    Min->FixParameter(125);
    Min->FixParameter(126);
    Min->FixParameter(127);
    Min->FixParameter(128);
    Min->FixParameter(129);
    Min->FixParameter(130);
    Min->FixParameter(131);
    Min->FixParameter(132);
    Min->FixParameter(133);
    Min->FixParameter(134);
    Min->FixParameter(135);
    Min->FixParameter(136);
    Min->FixParameter(137);
    Min->FixParameter(138);
    Min->FixParameter(139);
    Min->FixParameter(140);
    Min->FixParameter(141);
    Min->FixParameter(142);
    Min->FixParameter(143);
    Min->FixParameter(144);
    Min->FixParameter(145);
    Min->FixParameter(146);
    Min->FixParameter(147);
    Min->FixParameter(148);
    Min->FixParameter(149);
    Min->FixParameter(150);
    Min->FixParameter(151);
    Min->FixParameter(152);
    Min->FixParameter(153);
    Min->FixParameter(154);
    Min->FixParameter(155);
    Min->FixParameter(156);
    Min->FixParameter(157);
    Min->FixParameter(158);
    Min->FixParameter(159);
    Min->FixParameter(160);
    Min->FixParameter(161);
    Min->FixParameter(162);
    Min->FixParameter(163);
    Min->FixParameter(164);
    Min->FixParameter(165);
    Min->FixParameter(166);
    Min->FixParameter(167);
    Min->FixParameter(168);
    Min->FixParameter(169);
    Min->FixParameter(170);
    Min->FixParameter(171);
    Min->FixParameter(172);
    Min->FixParameter(173);
    Min->FixParameter(174);
    Min->FixParameter(175);
    Min->FixParameter(176);
    Min->FixParameter(177);
    Min->FixParameter(178);
    Min->FixParameter(179);
    Min->FixParameter(180);
    Min->FixParameter(181);
    Min->FixParameter(182);
    Min->FixParameter(183);
    Min->FixParameter(184);
    Min->FixParameter(185);
    Min->FixParameter(186);
    Min->FixParameter(187);
    Min->FixParameter(188);
    Min->FixParameter(189);
    Min->FixParameter(190);
    Min->FixParameter(191);
    Min->FixParameter(192);
    Min->FixParameter(193);
    Min->FixParameter(194);
    Min->FixParameter(195);
    Min->FixParameter(196);
    Min->FixParameter(197);
    Min->FixParameter(198);
    Min->FixParameter(199);
    Min->FixParameter(200);
    Min->FixParameter(201);
    Min->FixParameter(202);
    Min->FixParameter(203);
    Min->FixParameter(204);
    Min->FixParameter(205);
    Min->FixParameter(206);
    Min->FixParameter(207);
    Min->FixParameter(208);
    Min->FixParameter(209);
    Min->FixParameter(210);
    Min->FixParameter(211);
    Min->FixParameter(212);*/
    
    if (global_area_no_comb_form_4tr)
    {
        for(int i = 0; i < 80; i++)
        {
            Min->FixParameter(i);
        }
        for(int i = 85; i < 213; i++)
        {
            Min->FixParameter(i);
        }
        if (!RARE_DECAYS)
        {
            Min->FixParameter(214);
            Min->FixParameter(215);
            Min->FixParameter(216);
        }
        Min->FixParameter(217);
        Min->FixParameter(218);
        for(int i = 219; i < 230; i++)
        {
            Min->FixParameter(i);
        }
    }
    
    if (Jpsipipi_fit_mode_comb_only)
    {
        for(int i = 0; i < 101; i++)
        {
            Min->FixParameter(i);
        }
        for(int i = 108; i < 219; i++)
        {
            Min->FixParameter(i);
        }
        for(int i = 219; i < 230; i++)
        {
            Min->FixParameter(i);
        }
    }
    
    if (JpsiKK_fit_mode_comb_only)
    {
        for(int i = 0; i < 93; i++)
        {
            Min->FixParameter(i);
        }
        for(int i = 99; i < 219; i++)
        {
            Min->FixParameter(i);
        }
        for(int i = 219; i < 230; i++)
        {
            Min->FixParameter(i);
        }
    }
    
    if (JpsiKpi_fit_mode_comb_only)
    {
        for(int i = 0; i < 85; i++)
        {
            Min->FixParameter(i);
        }
        for(int i = 93; i < 219; i++)
        {
            Min->FixParameter(i);
        }
        for(int i = 219; i < 230; i++)
        {
            Min->FixParameter(i);
        }
    }
    
    if (JpsipK_fit_mode || JpsipK_fit_mode_plus_JpsiKpi)
    {
        for(int i = 0; i < 80; i++)
        {
            Min->FixParameter(i);
        }
        Min->FixParameter(81); Min->FixParameter(82); 
        for(int i = 84; i < 219; i++)
        {
            Min->FixParameter(i);
        }
		for(int i = 219; i < 230; i++)
        {
            Min->FixParameter(i);
        }
		/*for(int i = 121; i < 213; i++)
        {
            Min->FixParameter(i);
        }
        for(int i = 214; i < 219; i++)
        {
            Min->FixParameter(i);
        }*/
        //for(int i = 121; i < 213; i++)
        //{
        //    Min->FixParameter(i);
        //}
    }
    
    if (JpsipK_fit_mode_comb_only)
    {
        for(int i = 0; i < 110; i++)
        {
            Min->FixParameter(i);
        }
        for(int i = 121; i < 219; i++)
        {
            Min->FixParameter(i);
        }
        for(int i = 219; i < 230; i++)
        {
            Min->FixParameter(i);
        }
    }
    
    if (Jpsipipi_fit_mode || Jpsipipi_fit_mode_plus_JpsiKpi || Jpsipipi_fit_mode_plus_JpsiKpi_lbfix)
    {
        for(int i = 0; i < 80; i++)
        {
            Min->FixParameter(i);
        }
        Min->FixParameter(80);  
        for(int i = 84; i < 101; i++)
        {
            Min->FixParameter(i);
        }
        for(int i = 108; i < 219; i++)
        {
            Min->FixParameter(i);
        }
        for(int i = 219; i < 230; i++)
        {
            Min->FixParameter(i);
        }
    }
    
    if (JpsiKK_fit_mode || JpsiKK_fit_mode_plus_JpsiKpi || JpsiKK_fit_mode_plus_JpsiKpi_lbpipi_fix)
    {
        for(int i = 0; i < 49; i++)
        {
            Min->FixParameter(i);
        }
        Min->FixParameter(79); Min->FixParameter(80); Min->FixParameter(81); Min->FixParameter(82);   
        for(int i = 85; i < 219; i++)
        {
            Min->FixParameter(i);
        }
        /*for(int i = 99; i < 219; i++)
        {
            Min->FixParameter(i);
        }*/
        for(int i = 219; i < 230; i++)
        {
            Min->FixParameter(i);
        }
    }
    
    if (JpsiKK_fit_mode_no_control || JpsiKK_fit_mode_plus_JpsiKpi_no_control_lbpipi_fix)
    {
        for(int i = 0; i < 78; i++)
        {
            Min->FixParameter(i);
        }
        Min->FixParameter(78); Min->FixParameter(79); Min->FixParameter(80); Min->FixParameter(81); Min->FixParameter(82);   
        for(int i = 85; i < 93; i++)
        {
            Min->FixParameter(i);
        }
        for(int i = 99; i < 219; i++)
        {
            Min->FixParameter(i);
        }
        for(int i = 219; i < 230; i++)
        {
            Min->FixParameter(i);
        }
    }
    
    if (JpsiKpi_fit_mode)
    {
        Min->FixParameter(0);
        Min->FixParameter(1);
        for(int i = 46; i < 80; i++)
        {
            Min->FixParameter(i);
        }
        Min->FixParameter(80); Min->FixParameter(81); Min->FixParameter(82);    
        for(int i = 93; i < 217; i++)
        {
            Min->FixParameter(i);
        }
        if (MODEL == 2 || MODEL == 3 || MODEL == 5)
        {
            Min->FixParameter(42);
            Min->FixParameter(43);
        }
        if (MODEL == 1)
        {
            Min->FixParameter(40);
            Min->FixParameter(41);
            Min->FixParameter(42);
            Min->FixParameter(43);
            Min->FixParameter(44);
            Min->FixParameter(45);
        }
        for(int i = 219; i < 230; i++)
        {
            Min->FixParameter(i);
        }
    }
    
    if (signal_area_fit_mode || signal_area_fit_mode_short)
    {
        Min->FixParameter(0);
        Min->FixParameter(1);
        for(int i = 46; i < 80; i++)
        {
            Min->FixParameter(i);
        }
        for(int i = 80; i < 217; i++)
        {
            Min->FixParameter(i);
        }
        if (MODEL == 2 || MODEL == 3 || MODEL == 5)
        {
            Min->FixParameter(42);
            Min->FixParameter(43);
        }
        if (MODEL == 1)
        {
            Min->FixParameter(40);
            Min->FixParameter(41);
            Min->FixParameter(42);
            Min->FixParameter(43);
            Min->FixParameter(44);
            Min->FixParameter(45);
        }
        for(int i = 219; i < 230; i++)
        {
            Min->FixParameter(i);
        }
    }
    
    if (signal_area_fit_mode_onlyZc)
    {
        for(int i = 0; i < 40; i++)
        {
            Min->FixParameter(i);
        }
        
        for(int i = 46; i < 219; i++)
        {
            Min->FixParameter(i);
        }
        if (MODEL == 2 || MODEL == 3 || MODEL == 5)
        {
            Min->FixParameter(42);
            Min->FixParameter(43);
        }
        for(int i = 219; i < 230; i++)
        {
            Min->FixParameter(i);
        }
    }
    
    if (signal_area_fit_mode_noZc || signal_area_fit_mode_short_noZc)
    {
        Min->FixParameter(0);
        Min->FixParameter(1);
        for(int i = 40; i < 80; i++)
        {
            Min->FixParameter(i);
        }
        for(int i = 80; i < 217; i++)
        {
            Min->FixParameter(i);
        }
        for(int i = 219; i < 230; i++)
        {
            Min->FixParameter(i);
        }
    }
    
    if (JpsiKpi_fit_mode_no_signal)
    {
        for(int i = 0; i < 80; i++)
        {
            Min->FixParameter(i);
        }
        Min->FixParameter(80); Min->FixParameter(81); Min->FixParameter(82);    
        for(int i = 93; i < 219; i++)
        {
            Min->FixParameter(i);
        }
        for(int i = 219; i < 230; i++)
        {
            Min->FixParameter(i);
        }
    }
    
    if (Jpsippi_fit_mode_comb_only)
    {
        for(int i = 0; i < 219; i++)
        {
            Min->FixParameter(i);
        }
    }
    
    //Min->SetErrorDef(0.5);
    
    Double_t Ls1800_12_Bw1_amp, Ls1800_12_Bw1_phi, Ls1800_12_Bw2_amp, Ls1800_12_Bw2_phi, Ls1800_12_Bw3_amp, Ls1800_12_Bw3_phi, Ls1800_12_Bw4_amp, Ls1800_12_Bw4_phi, Ls1810_12_Bw1_amp, Ls1810_12_Bw1_phi, Ls1810_12_Bw2_amp, Ls1810_12_Bw2_phi, Ls1810_12_Bw3_amp, Ls1810_12_Bw3_phi, Ls1820_52_Bw1_amp, Ls1820_52_Bw1_phi, Ls1820_52_Bw2_amp, Ls1820_52_Bw2_phi, Ls1820_52_Bw3_amp, Ls1820_52_Bw3_phi, Ls1830_52_Bw1_amp, Ls1830_52_Bw1_phi, Ls1830_52_Bw2_amp, Ls1830_52_Bw2_phi, Ls1830_52_Bw3_amp, Ls1830_52_Bw3_phi, Ls1890_32_Bw1_amp, Ls1890_32_Bw1_phi, Ls1890_32_Bw2_amp, Ls1890_32_Bw2_phi, Ls1890_32_Bw3_amp, Ls1890_32_Bw3_phi, Ls2020_72_Bw1_amp, Ls2020_72_Bw1_phi, Ls2020_72_Bw2_amp, Ls2020_72_Bw2_phi, Ls2020_72_Bw3_amp, Ls2020_72_Bw3_phi, Ls2050_32_Bw1_amp, Ls2050_32_Bw1_phi, Ls2050_32_Bw2_amp, Ls2050_32_Bw2_phi, Ls2050_32_Bw3_amp, Ls2050_32_Bw3_phi, Ls2100_72_Bw1_amp, Ls2100_72_Bw1_phi, Ls2100_72_Bw2_amp, Ls2100_72_Bw2_phi, Ls2100_72_Bw3_amp, Ls2100_72_Bw3_phi, Ls2110_52_Bw1_amp, Ls2110_52_Bw1_phi, Ls2110_52_Bw2_amp, Ls2110_52_Bw2_phi, Ls2110_52_Bw3_amp, Ls2110_52_Bw3_phi, Ls2325_32_Bw1_amp, Ls2325_32_Bw1_phi, Ls2325_32_Bw2_amp, Ls2325_32_Bw2_phi, Ls2325_32_Bw3_amp, Ls2325_32_Bw3_phi, Ls2350_92_Bw1_amp, Ls2350_92_Bw1_phi, Ls2350_92_Bw2_amp, Ls2350_92_Bw2_phi, LsNR_12_Bw_amp, LsNR_12_Bw_phi, Pc1Bw1_amp, Pc1Bw1_phi, Pc1Bw2_amp, Pc1Bw2_phi, Pc1Bs1_amp, Pc1Bs1_phi, Pc1Bs2_amp, Pc1Bs2_phi, Pc1Bs3_amp, Pc1Bs3_phi, Pc2Bw1_amp, Pc2Bw1_phi, Pc2Bw2_amp, Pc2Bw2_phi, Pc2Bs1_amp, Pc2Bs1_phi, Pc2Bs2_amp, Pc2Bs2_phi, Pc2Bs3_amp, Pc2Bs3_phi, M01, M02, gamma1, gamma2, comb_bg_norm;
    
    Double_t dLs1800_12_Bw1_amp, dLs1800_12_Bw1_phi, dLs1800_12_Bw2_amp, dLs1800_12_Bw2_phi, dLs1800_12_Bw3_amp, dLs1800_12_Bw3_phi, dLs1800_12_Bw4_amp, dLs1800_12_Bw4_phi, dLs1810_12_Bw1_amp, dLs1810_12_Bw1_phi, dLs1810_12_Bw2_amp, dLs1810_12_Bw2_phi, dLs1810_12_Bw3_amp, dLs1810_12_Bw3_phi, dLs1820_52_Bw1_amp, dLs1820_52_Bw1_phi, dLs1820_52_Bw2_amp, dLs1820_52_Bw2_phi, dLs1820_52_Bw3_amp, dLs1820_52_Bw3_phi, dLs1830_52_Bw1_amp, dLs1830_52_Bw1_phi, dLs1830_52_Bw2_amp, dLs1830_52_Bw2_phi, dLs1830_52_Bw3_amp, dLs1830_52_Bw3_phi, dLs1890_32_Bw1_amp, dLs1890_32_Bw1_phi, dLs1890_32_Bw2_amp, dLs1890_32_Bw2_phi, dLs1890_32_Bw3_amp, dLs1890_32_Bw3_phi, dLs2020_72_Bw1_amp, dLs2020_72_Bw1_phi, dLs2020_72_Bw2_amp, dLs2020_72_Bw2_phi, dLs2020_72_Bw3_amp, dLs2020_72_Bw3_phi, dLs2050_32_Bw1_amp, dLs2050_32_Bw1_phi, dLs2050_32_Bw2_amp, dLs2050_32_Bw2_phi, dLs2050_32_Bw3_amp, dLs2050_32_Bw3_phi, dLs2100_72_Bw1_amp, dLs2100_72_Bw1_phi, dLs2100_72_Bw2_amp, dLs2100_72_Bw2_phi, dLs2100_72_Bw3_amp, dLs2100_72_Bw3_phi, dLs2110_52_Bw1_amp, dLs2110_52_Bw1_phi, dLs2110_52_Bw2_amp, dLs2110_52_Bw2_phi, dLs2110_52_Bw3_amp, dLs2110_52_Bw3_phi, dLs2325_32_Bw1_amp, dLs2325_32_Bw1_phi, dLs2325_32_Bw2_amp, dLs2325_32_Bw2_phi, dLs2325_32_Bw3_amp, dLs2325_32_Bw3_phi, dLs2350_92_Bw1_amp, dLs2350_92_Bw1_phi, dLs2350_92_Bw2_amp, dLs2350_92_Bw2_phi, dLsNR_12_Bw_amp, dLsNR_12_Bw_phi, dPc1Bw1_amp, dPc1Bw1_phi, dPc1Bw2_amp, dPc1Bw2_phi, dPc1Bs1_amp, dPc1Bs1_phi, dPc1Bs2_amp, dPc1Bs2_phi, dPc1Bs3_amp, dPc1Bs3_phi, dPc2Bw1_amp, dPc2Bw1_phi, dPc2Bw2_amp, dPc2Bw2_phi, dPc2Bs1_amp, dPc2Bs1_phi, dPc2Bs2_amp, dPc2Bs2_phi, dPc2Bs3_amp, dPc2Bs3_phi, dM01, dM02, dgamma1, dgamma2, dcomb_bg_norm;

    Double_t K1410_1_Bw1_amp, K1410_1_Bw1_phi, K1430_0_Bw1_amp, K1430_0_Bw1_phi, K1430_2_Bw1_amp, K1430_2_Bw1_phi, K1680_1_Bw1_amp, K1680_1_Bw1_phi, K1780_3_Bw1_amp, K1780_3_Bw1_phi, K1950_0_Bw1_amp, K1950_0_Bw1_phi, K2045_4_Bw1_amp, K2045_4_Bw1_phi, par1_Kpi, par2_Kpi, par1_jpsipi, par2_jpsipi, par1_jpsiK, par2_jpsiK, K1980_2_Bw1_amp, K1980_2_Bw1_phi, KNR_0_Bw_amp, KNR_0_Bw_phi;
    Double_t K1410_1_Bw2_amp, K1410_1_Bw2_phi, K1410_1_Bw3_amp, K1410_1_Bw3_phi, K1430_2_Bw2_amp, K1430_2_Bw2_phi, K1430_2_Bw3_amp, K1430_2_Bw3_phi, K1680_1_Bw2_amp, K1680_1_Bw2_phi, K1680_1_Bw3_amp, K1680_1_Bw3_phi, K1780_3_Bw2_amp, K1780_3_Bw2_phi, K1780_3_Bw3_amp, K1780_3_Bw3_phi, K1980_2_Bw2_amp, K1980_2_Bw2_phi, K1980_2_Bw3_amp, K1980_2_Bw3_phi, K2045_4_Bw2_amp, K2045_4_Bw2_phi, K2045_4_Bw3_amp, K2045_4_Bw3_phi, X1Bs1_amp, X1Bs1_phi, X1Bs2_amp, X1Bs2_phi, gamma_Zc4200, m_Zc4200, n_BD_to_JPSI_K_PI_par, phi1680_1_Bw1_amp, phi1680_1_Bw1_phi, phi1680_1_Bw2_amp, phi1680_1_Bw2_phi, phi1680_1_Bw3_amp, phi1680_1_Bw3_phi, f1525_2_Bw1_amp, f1525_2_Bw1_phi, f1525_2_Bw2_amp, f1525_2_Bw2_phi, f1525_2_Bw3_amp, f1525_2_Bw3_phi, f1640_2_Bw1_amp, f1640_2_Bw1_phi, f1640_2_Bw2_amp, f1640_2_Bw2_phi, f1640_2_Bw3_amp, f1640_2_Bw3_phi, f1750_2_Bw1_amp, f1750_2_Bw1_phi, f1750_2_Bw2_amp, f1750_2_Bw2_phi, f1750_2_Bw3_amp, f1750_2_Bw3_phi, f1950_2_Bw1_amp, f1950_2_Bw1_phi, f1950_2_Bw2_amp, f1950_2_Bw2_phi, f1950_2_Bw3_amp, f1950_2_Bw3_phi, NR_Bw1_amp, NR_Bw1_phi, n_BS_to_JPSI_K_K_par;
                        
    Double_t dK1410_1_Bw1_amp, dK1410_1_Bw1_phi, dK1430_0_Bw1_amp, dK1430_0_Bw1_phi, dK1430_2_Bw1_amp, dK1430_2_Bw1_phi, dK1680_1_Bw1_amp, dK1680_1_Bw1_phi, dK1780_3_Bw1_amp, dK1780_3_Bw1_phi, dK1950_0_Bw1_amp, dK1950_0_Bw1_phi, dK2045_4_Bw1_amp, dK2045_4_Bw1_phi, dpar1_Kpi, dpar2_Kpi, dpar1_jpsipi, dpar2_jpsipi, dpar1_jpsiK, dpar2_jpsiK, dK1980_2_Bw1_amp, dK1980_2_Bw1_phi, dK1410_1_Bw2_amp, dK1410_1_Bw2_phi, dK1410_1_Bw3_amp, dK1410_1_Bw3_phi, dK1430_2_Bw2_amp, dK1430_2_Bw2_phi, dK1430_2_Bw3_amp, dK1430_2_Bw3_phi, dK1680_1_Bw2_amp, dK1680_1_Bw2_phi, dK1680_1_Bw3_amp, dK1680_1_Bw3_phi, dK1780_3_Bw2_amp, dK1780_3_Bw2_phi, dK1780_3_Bw3_amp, dK1780_3_Bw3_phi, dK1980_2_Bw2_amp, dK1980_2_Bw2_phi, dK1980_2_Bw3_amp, dK1980_2_Bw3_phi, dK2045_4_Bw2_amp, dK2045_4_Bw2_phi, dK2045_4_Bw3_amp, dK2045_4_Bw3_phi, dX1Bs1_amp, dX1Bs1_phi, dX1Bs2_amp, dX1Bs2_phi, dgamma_Zc4200, dm_Zc4200, dn_BD_to_JPSI_K_PI_par, dphi1680_1_Bw1_amp, dphi1680_1_Bw1_phi, dphi1680_1_Bw2_amp, dphi1680_1_Bw2_phi, dphi1680_1_Bw3_amp, dphi1680_1_Bw3_phi, df1525_2_Bw1_amp, df1525_2_Bw1_phi, df1525_2_Bw2_amp, df1525_2_Bw2_phi, df1525_2_Bw3_amp, df1525_2_Bw3_phi, df1640_2_Bw1_amp, df1640_2_Bw1_phi, df1640_2_Bw2_amp, df1640_2_Bw2_phi, df1640_2_Bw3_amp, df1640_2_Bw3_phi, df1750_2_Bw1_amp, df1750_2_Bw1_phi, df1750_2_Bw2_amp, df1750_2_Bw2_phi, df1750_2_Bw3_amp, df1750_2_Bw3_phi, df1950_2_Bw1_amp, df1950_2_Bw1_phi, df1950_2_Bw2_amp, df1950_2_Bw2_phi, df1950_2_Bw3_amp, df1950_2_Bw3_phi, dNR_Bw1_amp, dNR_Bw1_phi, dn_BS_to_JPSI_K_K_par, dKNR_0_Bw_amp, dKNR_0_Bw_phi;
    
    Double_t n_lambda, n_BS_PIPI, n_BD_PIPI, n_BD_KPI, n_BS_KK, p0, p1, p2, p3, a0, b0, b1, b2, x0_KK, d0_KK, p0_KK, p1_KK, p2_KK, p3_KK, p4_KK, p5_KK, x0_pipi, d0_pipi, p0_pipi, p1_pipi, p2_pipi, p3_pipi, p4_pipi, p5_pipi, p6_pipi, x0_pKKp, d0_pKKp, p0_pKKp, p1_pKKp, p2_pKKp, p3_pKKp, a0_pKKp, b0_pKKp, b1_pKKp, b2_pKKp, u0_pKKp, n_LB_PPI, n_BD_KK, n_BS_KPI, x0_ppipip, d0_ppipip, p0_ppipip, p1_ppipip, p2_ppipip, p3_ppipip, a0_ppipip, b0_ppipip, b1_ppipip, b2_ppipip, u0_ppipip;  
    
    Double_t dn_lambda, dn_BS_PIPI, dn_BD_PIPI, dn_BD_KPI, dn_BS_KK, dp0, dp1, dp2, dp3, da0, db0, db1, db2, dx0_KK, dd0_KK, dp0_KK, dp1_KK, dp2_KK, dp3_KK, dp4_KK, dp5_KK, dx0_pipi, dd0_pipi, dp0_pipi, dp1_pipi, dp2_pipi, dp3_pipi, dp4_pipi, dp5_pipi, dp6_pipi, dx0_pKKp, dd0_pKKp, dp0_pKKp, dp1_pKKp, dp2_pKKp, dp3_pKKp, da0_pKKp, db0_pKKp, db1_pKKp, db2_pKKp, du0_pKKp, dn_LB_PPI, dn_BD_KK, dn_BS_KPI, dx0_ppipip, dd0_ppipip, dp0_ppipip, dp1_ppipip, dp2_ppipip, dp3_ppipip, da0_ppipip, db0_ppipip, db1_ppipip, db2_ppipip, du0_ppipip;
    
    Double_t arglist[10];
    arglist[0] = 1;
    Min->SetErrorDef(0.5);
    //вызов статистики:
    Double_t amin = 0.,edm,errdef;
    Int_t nvpar,nparx,icstat;
    
    clock_t start = clock();
    
    if (fit_mode)
    {    
        Min->mnexcm("SET ERR", arglist, 1, ierflg);
    
        arglist[0] = 100000; //число итераций
        arglist[1] = 0.01;   //целевая точность
        Min->mnexcm("MIGRAD", arglist , 2,ierflg);
    
        arglist[0] = 50000;
        arglist[1] = 0.01;

        Min->mnstat(amin,edm,errdef,nvpar,nparx,icstat);
    
        int cntr00 = 0;
        while( icstat < 3 && cntr00 < 3 )
        {
            arglist[0] = 500000;
            arglist[1] = 0.01;
            Min->mnexcm("MIGRAD", arglist ,2,ierflg);

            arglist[0] = 100000;
            arglist[1] = 0.01;
            Min->mnexcm( "HESse", arglist, 2, ierflg);

            Min->mnstat(amin,edm,errdef,nvpar,nparx,icstat);

            cntr00++;
        }
    }
    
    clock_t end = clock();
    
    Min->GetParameter(0, K1410_1_Bw1_amp, dK1410_1_Bw1_amp);
    Min->GetParameter(1, K1410_1_Bw1_phi, dK1410_1_Bw1_phi);
    Min->GetParameter(2, K1430_0_Bw1_amp, dK1430_0_Bw1_amp);
    Min->GetParameter(3, K1430_0_Bw1_phi, dK1430_0_Bw1_phi);
    Min->GetParameter(4, K1430_2_Bw1_amp, dK1430_2_Bw1_amp);
    Min->GetParameter(5, K1430_2_Bw1_phi, dK1430_2_Bw1_phi);
    Min->GetParameter(6, K1680_1_Bw1_amp, dK1680_1_Bw1_amp);
    Min->GetParameter(7, K1680_1_Bw1_phi, dK1680_1_Bw1_phi);
    Min->GetParameter(8, K1780_3_Bw1_amp, dK1780_3_Bw1_amp);
    Min->GetParameter(9, K1780_3_Bw1_phi, dK1780_3_Bw1_phi);
    Min->GetParameter(10, K1950_0_Bw1_amp, dK1950_0_Bw1_amp);
    Min->GetParameter(11, K1950_0_Bw1_phi, dK1950_0_Bw1_phi);
    Min->GetParameter(12, K2045_4_Bw1_amp, dK2045_4_Bw1_amp);
    Min->GetParameter(13, K2045_4_Bw1_phi, dK2045_4_Bw1_phi);
    Min->GetParameter(14, K1980_2_Bw1_amp, dK1980_2_Bw1_amp);
    Min->GetParameter(15, K1980_2_Bw1_phi, dK1980_2_Bw1_phi);
    Min->GetParameter(16, K1410_1_Bw2_amp, dK1410_1_Bw2_amp);
    Min->GetParameter(17, K1410_1_Bw2_phi, dK1410_1_Bw2_phi);
    Min->GetParameter(18, K1410_1_Bw3_amp, dK1410_1_Bw3_amp);
    Min->GetParameter(19, K1410_1_Bw3_phi, dK1410_1_Bw3_phi);
    Min->GetParameter(20, K1430_2_Bw2_amp, dK1430_2_Bw2_amp);
    Min->GetParameter(21, K1430_2_Bw2_phi, dK1430_2_Bw2_phi);
    Min->GetParameter(22, K1430_2_Bw3_amp, dK1430_2_Bw3_amp);
    Min->GetParameter(23, K1430_2_Bw3_phi, dK1430_2_Bw3_phi);
	Min->GetParameter(24, K1680_1_Bw2_amp, dK1680_1_Bw2_amp);
    Min->GetParameter(25, K1680_1_Bw2_phi, dK1680_1_Bw2_phi);
    Min->GetParameter(26, K1680_1_Bw3_amp, dK1680_1_Bw3_amp);
    Min->GetParameter(27, K1680_1_Bw3_phi, dK1680_1_Bw3_phi);
	Min->GetParameter(28, K1780_3_Bw2_amp, dK1780_3_Bw2_amp);
    Min->GetParameter(29, K1780_3_Bw2_phi, dK1780_3_Bw2_phi);
    Min->GetParameter(30, K1780_3_Bw3_amp, dK1780_3_Bw3_amp);
    Min->GetParameter(31, K1780_3_Bw3_phi, dK1780_3_Bw3_phi);
	Min->GetParameter(32, K1980_2_Bw2_amp, dK1980_2_Bw2_amp);
    Min->GetParameter(33, K1980_2_Bw2_phi, dK1980_2_Bw2_phi);
    Min->GetParameter(34, K1980_2_Bw3_amp, dK1980_2_Bw3_amp);
    Min->GetParameter(35, K1980_2_Bw3_phi, dK1980_2_Bw3_phi);
	Min->GetParameter(36, K2045_4_Bw2_amp, dK2045_4_Bw2_amp);
    Min->GetParameter(37, K2045_4_Bw2_phi, dK2045_4_Bw2_phi);
    Min->GetParameter(38, K2045_4_Bw3_amp, dK2045_4_Bw3_amp);
    Min->GetParameter(39, K2045_4_Bw3_phi, dK2045_4_Bw3_phi);
    Min->GetParameter(40, X1Bs1_amp, dX1Bs1_amp);
    Min->GetParameter(41, X1Bs1_phi, dX1Bs1_phi);
	Min->GetParameter(42, X1Bs2_amp, dX1Bs2_amp);
    Min->GetParameter(43, X1Bs2_phi, dX1Bs2_phi);
    Min->GetParameter(44, gamma_Zc4200, dgamma_Zc4200);
    Min->GetParameter(45, m_Zc4200, dm_Zc4200); 
    Min->GetParameter(46, n_BD_to_JPSI_K_PI_par, dn_BD_to_JPSI_K_PI_par);
    Min->GetParameter(47, phi1680_1_Bw1_amp, dphi1680_1_Bw1_amp);
    Min->GetParameter(48, phi1680_1_Bw1_phi, dphi1680_1_Bw1_phi);
    Min->GetParameter(49, phi1680_1_Bw2_amp, dphi1680_1_Bw2_amp);
    Min->GetParameter(50, phi1680_1_Bw2_phi, dphi1680_1_Bw2_phi);
    Min->GetParameter(51, phi1680_1_Bw3_amp, dphi1680_1_Bw3_amp);
    Min->GetParameter(52, phi1680_1_Bw3_phi, dphi1680_1_Bw3_phi);
    Min->GetParameter(53, f1525_2_Bw1_amp, df1525_2_Bw1_amp);
    Min->GetParameter(54, f1525_2_Bw1_phi, df1525_2_Bw1_phi);
    Min->GetParameter(55, f1525_2_Bw2_amp, df1525_2_Bw2_amp);
    Min->GetParameter(56, f1525_2_Bw2_phi, df1525_2_Bw2_phi);
    Min->GetParameter(57, f1525_2_Bw3_amp, df1525_2_Bw3_amp);
    Min->GetParameter(58, f1525_2_Bw3_phi, df1525_2_Bw3_phi);
    Min->GetParameter(59, f1640_2_Bw1_amp, df1640_2_Bw1_amp);
    Min->GetParameter(60, f1640_2_Bw1_phi, df1640_2_Bw1_phi);
    Min->GetParameter(61, f1640_2_Bw2_amp, df1640_2_Bw2_amp);
    Min->GetParameter(62, f1640_2_Bw2_phi, df1640_2_Bw2_phi);
    Min->GetParameter(63, f1640_2_Bw3_amp, df1640_2_Bw3_amp);
    Min->GetParameter(64, f1640_2_Bw3_phi, df1640_2_Bw3_phi);
    Min->GetParameter(65, f1750_2_Bw1_amp, df1750_2_Bw1_amp);
    Min->GetParameter(66, f1750_2_Bw1_phi, df1750_2_Bw1_phi);
    Min->GetParameter(67, f1750_2_Bw2_amp, df1750_2_Bw2_amp);
    Min->GetParameter(68, f1750_2_Bw2_phi, df1750_2_Bw2_phi);
    Min->GetParameter(69, f1750_2_Bw3_amp, df1750_2_Bw3_amp);
    Min->GetParameter(70, f1750_2_Bw3_phi, df1750_2_Bw3_phi);
    Min->GetParameter(71, f1950_2_Bw1_amp, df1950_2_Bw1_amp);
    Min->GetParameter(72, f1950_2_Bw1_phi, df1950_2_Bw1_phi);
    Min->GetParameter(73, f1950_2_Bw2_amp, df1950_2_Bw2_amp);
    Min->GetParameter(74, f1950_2_Bw2_phi, df1950_2_Bw2_phi);
    Min->GetParameter(75, f1950_2_Bw3_amp, df1950_2_Bw3_amp);
    Min->GetParameter(76, f1950_2_Bw3_phi, df1950_2_Bw3_phi);
    Min->GetParameter(77, NR_Bw1_amp, dNR_Bw1_amp);
    Min->GetParameter(78, NR_Bw1_phi, dNR_Bw1_phi);
    Min->GetParameter(79, n_BS_to_JPSI_K_K_par, dn_BS_to_JPSI_K_K_par);
    Min->GetParameter(80, n_lambda, dn_lambda);
    Min->GetParameter(81, n_BS_PIPI, dn_BS_PIPI);
    Min->GetParameter(82, n_BD_PIPI, dn_BD_PIPI);
    Min->GetParameter(83, n_BD_KPI, dn_BD_KPI);
    Min->GetParameter(84, n_BS_KK, dn_BS_KK);
    Min->GetParameter(85, p0, dp0);
    Min->GetParameter(86, p1, dp1);
    Min->GetParameter(87, p2, dp2);
    Min->GetParameter(88, p3, dp3);
    Min->GetParameter(89, a0, da0);
    Min->GetParameter(90, b0, db0);
    Min->GetParameter(91, b1, db1);
    Min->GetParameter(92, b2, db2);
    Min->GetParameter(93, x0_KK, dx0_KK);
    Min->GetParameter(94, d0_KK, dd0_KK);
    Min->GetParameter(95, p0_KK, dp0_KK);
    Min->GetParameter(96, p1_KK, dp1_KK);
    Min->GetParameter(97, p2_KK, dp2_KK);
    Min->GetParameter(98, p3_KK, dp3_KK);
    Min->GetParameter(99, p4_KK, dp4_KK);
    Min->GetParameter(100, p5_KK, dp5_KK);
    Min->GetParameter(101, x0_pipi, dx0_pipi);
    Min->GetParameter(102, d0_pipi, dd0_pipi);
    Min->GetParameter(103, p0_pipi, dp0_pipi);
    Min->GetParameter(104, p1_pipi, dp1_pipi);
    Min->GetParameter(105, p2_pipi, dp2_pipi);
    Min->GetParameter(106, p3_pipi, dp3_pipi);
    Min->GetParameter(107, p4_pipi, dp4_pipi);
    Min->GetParameter(108, p5_pipi, dp5_pipi);
    Min->GetParameter(109, p6_pipi, dp6_pipi);
    Min->GetParameter(110, x0_pKKp, dx0_pKKp);
    Min->GetParameter(111, d0_pKKp, dd0_pKKp);
    Min->GetParameter(112, p0_pKKp, dp0_pKKp);
    Min->GetParameter(113, p1_pKKp, dp1_pKKp);
    Min->GetParameter(114, p2_pKKp, dp2_pKKp);
    Min->GetParameter(115, p3_pKKp, dp3_pKKp);
    Min->GetParameter(116, a0_pKKp, da0_pKKp);
    Min->GetParameter(117, b0_pKKp, db0_pKKp);
    Min->GetParameter(118, b1_pKKp, db1_pKKp);
    Min->GetParameter(119, b2_pKKp, db2_pKKp);
    Min->GetParameter(120, u0_pKKp, du0_pKKp); 
    Min->GetParameter(121, Ls1800_12_Bw1_amp, dLs1800_12_Bw1_amp);
    Min->GetParameter(122, Ls1800_12_Bw1_phi, dLs1800_12_Bw1_phi);
    Min->GetParameter(123, Ls1800_12_Bw2_amp, dLs1800_12_Bw2_amp);
    Min->GetParameter(124, Ls1800_12_Bw2_phi, dLs1800_12_Bw2_phi);
    Min->GetParameter(125, Ls1800_12_Bw3_amp, dLs1800_12_Bw3_amp);
    Min->GetParameter(126, Ls1800_12_Bw3_phi, dLs1800_12_Bw3_phi);
    Min->GetParameter(127, Ls1800_12_Bw4_amp, dLs1800_12_Bw4_amp);
    Min->GetParameter(128, Ls1800_12_Bw4_phi, dLs1800_12_Bw4_phi);
    Min->GetParameter(129, Ls1810_12_Bw1_amp, dLs1810_12_Bw1_amp);
    Min->GetParameter(130, Ls1810_12_Bw1_phi, dLs1810_12_Bw1_phi);
    Min->GetParameter(131, Ls1810_12_Bw2_amp, dLs1810_12_Bw2_amp);
    Min->GetParameter(132, Ls1810_12_Bw2_phi, dLs1810_12_Bw2_phi);
    Min->GetParameter(133, Ls1810_12_Bw3_amp, dLs1810_12_Bw3_amp);
    Min->GetParameter(134, Ls1810_12_Bw3_phi, dLs1810_12_Bw3_phi);
    Min->GetParameter(135, Ls1820_52_Bw1_amp, dLs1820_52_Bw1_amp);
    Min->GetParameter(136, Ls1820_52_Bw1_phi, dLs1820_52_Bw1_phi);
    Min->GetParameter(137, Ls1820_52_Bw2_amp, dLs1820_52_Bw2_amp);
    Min->GetParameter(138, Ls1820_52_Bw2_phi, dLs1820_52_Bw2_phi);
    Min->GetParameter(139, Ls1820_52_Bw3_amp, dLs1820_52_Bw3_amp);
    Min->GetParameter(140, Ls1820_52_Bw3_phi, dLs1820_52_Bw3_phi); 
    Min->GetParameter(141, Ls1830_52_Bw1_amp, dLs1830_52_Bw1_amp);
    Min->GetParameter(142, Ls1830_52_Bw1_phi, dLs1830_52_Bw1_phi);
    Min->GetParameter(143, Ls1830_52_Bw2_amp, dLs1830_52_Bw2_amp);
    Min->GetParameter(144, Ls1830_52_Bw2_phi, dLs1830_52_Bw2_phi);
    Min->GetParameter(145, Ls1830_52_Bw3_amp, dLs1830_52_Bw3_amp);
    Min->GetParameter(146, Ls1830_52_Bw3_phi, dLs1830_52_Bw3_phi);
    Min->GetParameter(147, Ls1890_32_Bw1_amp, dLs1890_32_Bw1_amp);
    Min->GetParameter(148, Ls1890_32_Bw1_phi, dLs1890_32_Bw1_phi);
    Min->GetParameter(149, Ls1890_32_Bw2_amp, dLs1890_32_Bw2_amp);
    Min->GetParameter(150, Ls1890_32_Bw2_phi, dLs1890_32_Bw2_phi);
    Min->GetParameter(151, Ls1890_32_Bw3_amp, dLs1890_32_Bw3_amp);
    Min->GetParameter(152, Ls1890_32_Bw3_phi, dLs1890_32_Bw3_phi);
    Min->GetParameter(153, Ls2020_72_Bw1_amp, dLs2020_72_Bw1_amp);
    Min->GetParameter(154, Ls2020_72_Bw1_phi, dLs2020_72_Bw1_phi);
    Min->GetParameter(155, Ls2020_72_Bw2_amp, dLs2020_72_Bw2_amp);
    Min->GetParameter(156, Ls2020_72_Bw2_phi, dLs2020_72_Bw2_phi);
    Min->GetParameter(157, Ls2020_72_Bw3_amp, dLs2020_72_Bw3_amp);
    Min->GetParameter(158, Ls2020_72_Bw3_phi, dLs2020_72_Bw3_phi);
    Min->GetParameter(159, Ls2050_32_Bw1_amp, dLs2050_32_Bw1_amp);
    Min->GetParameter(160, Ls2050_32_Bw1_phi, dLs2050_32_Bw1_phi); 
    Min->GetParameter(161, Ls2050_32_Bw2_amp, dLs2050_32_Bw2_amp);
    Min->GetParameter(162, Ls2050_32_Bw2_phi, dLs2050_32_Bw2_phi);
    Min->GetParameter(163, Ls2050_32_Bw3_amp, dLs2050_32_Bw3_amp);
    Min->GetParameter(164, Ls2050_32_Bw3_phi, dLs2050_32_Bw3_phi);
    Min->GetParameter(165, Ls2100_72_Bw1_amp, dLs2100_72_Bw1_amp);
    Min->GetParameter(166, Ls2100_72_Bw1_phi, dLs2100_72_Bw1_phi);
    Min->GetParameter(167, Ls2100_72_Bw2_amp, dLs2100_72_Bw2_amp);
    Min->GetParameter(168, Ls2100_72_Bw2_phi, dLs2100_72_Bw2_phi);
    Min->GetParameter(169, Ls2100_72_Bw3_amp, dLs2100_72_Bw3_amp);
    Min->GetParameter(170, Ls2100_72_Bw3_phi, dLs2100_72_Bw3_phi);
    Min->GetParameter(171, Ls2110_52_Bw1_amp, dLs2110_52_Bw1_amp);
    Min->GetParameter(172, Ls2110_52_Bw1_phi, dLs2110_52_Bw1_phi);
    Min->GetParameter(173, Ls2110_52_Bw2_amp, dLs2110_52_Bw2_amp);
    Min->GetParameter(174, Ls2110_52_Bw2_phi, dLs2110_52_Bw2_phi);
    Min->GetParameter(175, Ls2110_52_Bw3_amp, dLs2110_52_Bw3_amp);
    Min->GetParameter(176, Ls2110_52_Bw3_phi, dLs2110_52_Bw3_phi);
    Min->GetParameter(177, Ls2325_32_Bw1_amp, dLs2325_32_Bw1_amp);
    Min->GetParameter(178, Ls2325_32_Bw1_phi, dLs2325_32_Bw1_phi);
    Min->GetParameter(179, Ls2325_32_Bw2_amp, dLs2325_32_Bw2_amp);
    Min->GetParameter(180, Ls2325_32_Bw2_phi, dLs2325_32_Bw2_phi); 
    Min->GetParameter(181, Ls2325_32_Bw3_amp, dLs2325_32_Bw3_amp);
    Min->GetParameter(182, Ls2325_32_Bw3_phi, dLs2325_32_Bw3_phi);
    Min->GetParameter(183, Ls2350_92_Bw1_amp, dLs2350_92_Bw1_amp);
    Min->GetParameter(184, Ls2350_92_Bw1_phi, dLs2350_92_Bw1_phi);
    Min->GetParameter(185, Ls2350_92_Bw2_amp, dLs2350_92_Bw2_amp);
    Min->GetParameter(186, Ls2350_92_Bw2_phi, dLs2350_92_Bw2_phi);
    Min->GetParameter(187, LsNR_12_Bw_amp, dLsNR_12_Bw_amp);
    Min->GetParameter(188, LsNR_12_Bw_phi, dLsNR_12_Bw_phi);
    Min->GetParameter(189, Pc1Bw1_amp, dPc1Bw1_amp);
    Min->GetParameter(190, Pc1Bw1_phi, dPc1Bw1_phi);
    Min->GetParameter(191, Pc1Bw2_amp, dPc1Bw2_amp);
    Min->GetParameter(192, Pc1Bw2_phi, dPc1Bw2_phi);
    Min->GetParameter(193, Pc1Bs1_amp, dPc1Bs1_amp);
    Min->GetParameter(194, Pc1Bs1_phi, dPc1Bs1_phi);
    Min->GetParameter(195, Pc1Bs2_amp, dPc1Bs2_amp);
    Min->GetParameter(196, Pc1Bs2_phi, dPc1Bs2_phi);
    Min->GetParameter(197, Pc1Bs3_amp, dPc1Bs3_amp);
    Min->GetParameter(198, Pc1Bs3_phi, dPc1Bs3_phi);
    Min->GetParameter(199, Pc2Bw1_amp, dPc2Bw1_amp);
    Min->GetParameter(200, Pc2Bw1_phi, dPc2Bw1_phi);
    Min->GetParameter(201, Pc2Bw2_amp, dPc2Bw2_amp);
    Min->GetParameter(202, Pc2Bw2_phi, dPc2Bw2_phi);
    Min->GetParameter(203, Pc2Bs1_amp, dPc2Bs1_amp);
    Min->GetParameter(204, Pc2Bs1_phi, dPc2Bs1_phi);
    Min->GetParameter(205, Pc2Bs2_amp, dPc2Bs2_amp);
    Min->GetParameter(206, Pc2Bs2_phi, dPc2Bs2_phi);
    Min->GetParameter(207, Pc2Bs3_amp, dPc2Bs3_amp);
    Min->GetParameter(208, Pc2Bs3_phi, dPc2Bs3_phi);
    Min->GetParameter(209, M01, dM01);
    Min->GetParameter(210, M02, dM02);
    Min->GetParameter(211, gamma1, dgamma1);
    Min->GetParameter(212, gamma2, dgamma2); 
    Min->GetParameter(213, comb_bg_norm, dcomb_bg_norm);
    Min->GetParameter(214, n_LB_PPI, dn_LB_PPI);
    Min->GetParameter(215, n_BD_KK, dn_BD_KK);
    Min->GetParameter(216, n_BS_KPI, dn_BS_KPI); 
    Min->GetParameter(217, KNR_0_Bw_amp, dKNR_0_Bw_amp);
    Min->GetParameter(218, KNR_0_Bw_phi, dKNR_0_Bw_phi);
    Min->GetParameter(219, x0_ppipip, dx0_ppipip);
    Min->GetParameter(220, d0_ppipip, dd0_ppipip);
    Min->GetParameter(221, p0_ppipip, dp0_ppipip);
    Min->GetParameter(222, p1_ppipip, dp1_ppipip);
    Min->GetParameter(223, p2_ppipip, dp2_ppipip);
    Min->GetParameter(224, p3_ppipip, dp3_ppipip);
    Min->GetParameter(225, a0_ppipip, da0_ppipip);
    Min->GetParameter(226, b0_ppipip, db0_ppipip);
    Min->GetParameter(227, b1_ppipip, db1_ppipip);
    Min->GetParameter(228, b2_ppipip, db2_ppipip);
    Min->GetParameter(229, u0_ppipip, du0_ppipip); 
    
    if (JpsipK_fit_mode || JpsipK_fit_mode_plus_JpsiKpi || JpsipK_fit_mode_comb_only)
    {
        n_BS_PIPI = n_BS_PIPI * n_BD_KPI / n_global_BD_to_JPSI_K_PI;
        n_BD_PIPI = n_BD_PIPI * n_BD_KPI / n_global_BD_to_JPSI_K_PI;
        n_LB_PPI = n_LB_PPI * n_BD_KPI / n_global_BD_to_JPSI_K_PI;
        n_BD_KK = n_BD_KK * n_BD_KPI / n_global_BD_to_JPSI_K_PI;
        n_BS_KPI = n_BS_KPI * n_BD_KPI / n_global_BD_to_JPSI_K_PI;
        cout << "\n______________________________" << endl;
        cout << "n_lambda = " << n_lambda << "\nn_BD_PIPI = " << n_BD_PIPI << "\nn_BS_PIPI = " << n_BS_PIPI << endl;
        cout << "n_BD_KPI = " << n_BD_KPI << "\nn_BS_KK = " << n_BS_KK << "\nn_LB_PPI = " << n_LB_PPI << "\nn_BD_KK = " << n_BD_KK << "\nn_BS_KPI = " << n_BS_KPI << endl;
        cout << "______________________________\n" << endl;
    }
    
    if (Jpsipipi_fit_mode || Jpsipipi_fit_mode_plus_JpsiKpi)
    {
        n_lambda = n_lambda * n_BD_KPI / n_global_BD_to_JPSI_K_PI;
        n_BS_KK = n_BS_KK * n_BD_KPI / n_global_BD_to_JPSI_K_PI;
        n_LB_PPI = n_LB_PPI * n_BD_KPI / n_global_BD_to_JPSI_K_PI;
        n_BD_KK = n_BD_KK * n_BD_KPI / n_global_BD_to_JPSI_K_PI;
        n_BS_KPI = n_BS_KPI * n_BD_KPI / n_global_BD_to_JPSI_K_PI;
        cout << "n_lambda = " << n_lambda << "\nn_BD_PIPI = " << n_BD_PIPI << "\nn_BS_PIPI = " << n_BS_PIPI << endl;
        cout << "n_BD_KPI = " << n_BD_KPI << "\nn_BS_KK = " << n_BS_KK << "\nn_LB_PPI = " << n_LB_PPI << "\nn_BD_KK = " << n_BD_KK << "\nn_BS_KPI = " << n_BS_KPI << endl;
        cout << "______________________________\n" << endl;
    }
    
    if (Jpsipipi_fit_mode_plus_JpsiKpi_lbfix)
    {
        //n_lambda = n_lambda * n_BD_KPI / n_global_BD_to_JPSI_K_PI;
        n_BS_KK = n_BS_KK * n_BD_KPI / n_global_BD_to_JPSI_K_PI;
        n_LB_PPI = n_LB_PPI * n_BD_KPI / n_global_BD_to_JPSI_K_PI;
        n_BD_KK = n_BD_KK * n_BD_KPI / n_global_BD_to_JPSI_K_PI;
        n_BS_KPI = n_BS_KPI * n_BD_KPI / n_global_BD_to_JPSI_K_PI;
        cout << "n_lambda = " << n_lambda << "\nn_BD_PIPI = " << n_BD_PIPI << "\nn_BS_PIPI = " << n_BS_PIPI << endl;
        cout << "n_BD_KPI = " << n_BD_KPI << "\nn_BS_KK = " << n_BS_KK << "\nn_LB_PPI = " << n_LB_PPI << "\nn_BD_KK = " << n_BD_KK << "\nn_BS_KPI = " << n_BS_KPI << endl;
        cout << "______________________________\n" << endl;
    }
    
    if (JpsiKK_fit_mode || JpsiKK_fit_mode_no_control || JpsiKK_fit_mode_plus_JpsiKpi)
    {
        n_lambda = n_lambda * n_BD_KPI / n_global_BD_to_JPSI_K_PI;
        n_BS_PIPI = n_BS_PIPI * n_BD_KPI / n_global_BD_to_JPSI_K_PI;
        n_BD_PIPI = n_BD_PIPI * n_BD_KPI / n_global_BD_to_JPSI_K_PI;
        n_LB_PPI = n_LB_PPI * n_BD_KPI / n_global_BD_to_JPSI_K_PI;
        n_BD_KK = n_BD_KK * n_BD_KPI / n_global_BD_to_JPSI_K_PI;
        n_BS_KPI = n_BS_KPI * n_BD_KPI / n_global_BD_to_JPSI_K_PI;
        cout << "n_lambda = " << n_lambda << "\nn_BD_PIPI = " << n_BD_PIPI << "\nn_BS_PIPI = " << n_BS_PIPI << endl;
        cout << "n_BD_KPI = " << n_BD_KPI << "\nn_BS_KK = " << n_BS_KK << "\nn_LB_PPI = " << n_LB_PPI << "\nn_BD_KK = " << n_BD_KK << "\nn_BS_KPI = " << n_BS_KPI << endl;
        cout << "______________________________\n" << endl;
    }
    
    if (JpsiKK_fit_mode_plus_JpsiKpi_lbpipi_fix || JpsiKK_fit_mode_plus_JpsiKpi_no_control_lbpipi_fix || signal_area_fit_mode || signal_area_fit_mode_short)
    {
        cout << "n_lambda = " << n_lambda << "\nn_BD_PIPI = " << n_BD_PIPI << "\nn_BS_PIPI = " << n_BS_PIPI << endl;
        cout << "n_BD_KPI = " << n_BD_KPI << "\nn_BS_KK = " << n_BS_KK << "\nn_LB_PPI = " << n_LB_PPI << "\nn_BD_KK = " << n_BD_KK << "\nn_BS_KPI = " << n_BS_KPI << endl;
        cout << "______________________________\n" << endl;
    }
    
    if (JpsiKpi_fit_mode || JpsiKpi_fit_mode_no_signal)
    {
        n_lambda = n_lambda * n_BS_KK / n_global_BS_to_JPSI_K_K_ini;
        n_BS_PIPI = n_BS_PIPI * n_BS_KK / n_global_BS_to_JPSI_K_K_ini;
        n_BD_PIPI = n_BD_PIPI * n_BS_KK / n_global_BS_to_JPSI_K_K_ini;
        cout << "n_lambda = " << n_lambda << "\nn_BD_PIPI = " << n_BD_PIPI << "\nn_BS_PIPI = " << n_BS_PIPI << endl;
        cout << "n_BD_KPI = " << n_BD_KPI << "\nn_BS_KK = " << n_BS_KK << "\nn_LB_PPI = " << n_LB_PPI << "\nn_BD_KK = " << n_BD_KK << "\nn_BS_KPI = " << n_BS_KPI << endl;
        cout << "______________________________\n" << endl;
    }
    
//____ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo____
// Creating output vector of parameters (Result of signal area fit procedure)   
//____ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo____ 
    
    std::string par_out_name = "parameters/" + par_out;
    TFile *f_out = new TFile(par_out_name.c_str(), "recreate");
    TVectorD parameters(233);
    //parameters->pushback();
    parameters[0] = K1410_1_Bw1_amp;
    parameters[1] = K1410_1_Bw1_phi;
    parameters[2] = K1430_0_Bw1_amp;
    parameters[3] = K1430_0_Bw1_phi;
    parameters[4] = K1430_2_Bw1_amp;
    parameters[5] = K1430_2_Bw1_phi;
    parameters[6] = K1680_1_Bw1_amp;
    parameters[7] = K1680_1_Bw1_phi;
    parameters[8] = K1780_3_Bw1_amp;
    parameters[9] = K1780_3_Bw1_phi;
    parameters[10] = K1950_0_Bw1_amp;
    parameters[11] = K1950_0_Bw1_phi;
    parameters[12] = K2045_4_Bw1_amp;
    parameters[13] = K2045_4_Bw1_phi;
    parameters[14] = K1980_2_Bw1_amp;
    parameters[15] = K1980_2_Bw1_phi;
    parameters[16] = K1410_1_Bw2_amp;
    parameters[17] = K1410_1_Bw2_phi;
    parameters[18] = K1410_1_Bw3_amp;
    parameters[19] = K1410_1_Bw3_phi;
    parameters[20] = K1430_2_Bw2_amp;
    parameters[21] = K1430_2_Bw2_phi;
    parameters[22] = K1430_2_Bw3_amp;
    parameters[23] = K1430_2_Bw3_phi;
	parameters[24] = K1680_1_Bw2_amp;
    parameters[25] = K1680_1_Bw2_phi;
    parameters[26] = K1680_1_Bw3_amp;
    parameters[27] = K1680_1_Bw3_phi;
    parameters[28] = K1780_3_Bw2_amp;
    parameters[29] = K1780_3_Bw2_phi;
    parameters[30] = K1780_3_Bw3_amp;
    parameters[31] = K1780_3_Bw3_phi;
	parameters[32] = K1980_2_Bw2_amp;
    parameters[33] = K1980_2_Bw2_phi;
    parameters[34] = K1980_2_Bw3_amp;
    parameters[35] = K1980_2_Bw3_phi;
	parameters[36] = K2045_4_Bw2_amp;
    parameters[37] = K2045_4_Bw2_phi;
    parameters[38] = K2045_4_Bw3_amp;
    parameters[39] = K2045_4_Bw3_phi;
    parameters[40] = X1Bs1_amp;
    parameters[41] = X1Bs1_phi;
	parameters[42] = X1Bs2_amp;
    parameters[43] = X1Bs2_phi;
    parameters[44] = gamma_Zc4200;
    parameters[45] = m_Zc4200;
    parameters[46] = n_BD_to_JPSI_K_PI_par;
    parameters[47] = phi1680_1_Bw1_amp;
    parameters[48] = phi1680_1_Bw1_phi;
    parameters[49] = phi1680_1_Bw2_amp;
    parameters[50] = phi1680_1_Bw2_phi;
    parameters[51] = phi1680_1_Bw3_amp;
    parameters[52] = phi1680_1_Bw3_phi;
    parameters[53] = f1525_2_Bw1_amp;
    parameters[54] = f1525_2_Bw1_phi;
    parameters[55] = f1525_2_Bw2_amp;
    parameters[56] = f1525_2_Bw2_phi;
    parameters[57] = f1525_2_Bw3_amp;
    parameters[58] = f1525_2_Bw3_phi;
    parameters[59] = f1640_2_Bw1_amp;
    parameters[60] = f1640_2_Bw1_phi;
    parameters[61] = f1640_2_Bw2_amp;
    parameters[62] = f1640_2_Bw2_phi;
    parameters[63] = f1640_2_Bw3_amp;
    parameters[64] = f1640_2_Bw3_phi;
    parameters[65] = f1750_2_Bw1_amp;
    parameters[66] = f1750_2_Bw1_phi;
    parameters[67] = f1750_2_Bw2_amp;
    parameters[68] = f1750_2_Bw2_phi;
    parameters[69] = f1750_2_Bw3_amp;
    parameters[70] = f1750_2_Bw3_phi;
    parameters[71] = f1950_2_Bw1_amp;
    parameters[72] = f1950_2_Bw1_phi;
    parameters[73] = f1950_2_Bw2_amp;
    parameters[74] = f1950_2_Bw2_phi;
    parameters[75] = f1950_2_Bw3_amp;
    parameters[76] = f1950_2_Bw3_phi;
    parameters[77] = NR_Bw1_amp;
    parameters[78] = NR_Bw1_phi;
    parameters[79] = n_BS_to_JPSI_K_K_par;
    parameters[80] = n_lambda;
    parameters[81] = n_BS_PIPI;
    parameters[82] = n_BD_PIPI;
    parameters[83] = n_BD_KPI;
    parameters[84] = n_BS_KK;
    parameters[85] = p0;
    parameters[86] = p1;
    parameters[87] = p2;
    parameters[88] = p3;
    parameters[89] = a0;
    parameters[90] = b0;
    parameters[91] = b1;
    parameters[92] = b2;
    parameters[93] = x0_KK;
    parameters[94] = d0_KK;
    parameters[95] = p0_KK;
    parameters[96] = p1_KK;
    parameters[97] = p2_KK;
    parameters[98] = p3_KK;
    parameters[99] = p4_KK;
    parameters[100] = p5_KK;
    parameters[101] = x0_pipi;
    parameters[102] = d0_pipi;
    parameters[103] = p0_pipi;
    parameters[104] = p1_pipi;
    parameters[105] = p2_pipi;
    parameters[106] = p3_pipi;
    parameters[107] = p4_pipi;
    parameters[108] = p5_pipi;
    parameters[109] = p6_pipi;
    parameters[110] = x0_pKKp;
    parameters[111] = d0_pKKp;
    parameters[112] = p0_pKKp;
    parameters[113] = p1_pKKp;
    parameters[114] = p2_pKKp;
    parameters[115] = p3_pKKp;
    parameters[116] = a0_pKKp;
    parameters[117] = b0_pKKp;
    parameters[118] = b1_pKKp;
    parameters[119] = b2_pKKp;
    parameters[120] = u0_pKKp;
    parameters[121] = Ls1800_12_Bw1_amp;
    parameters[122] = Ls1800_12_Bw1_phi;
    parameters[123] = Ls1800_12_Bw2_amp;
    parameters[124] = Ls1800_12_Bw2_phi;
    parameters[125] = Ls1800_12_Bw3_amp;
    parameters[126] = Ls1800_12_Bw3_phi;
    parameters[127] = Ls1800_12_Bw4_amp;
    parameters[128] = Ls1800_12_Bw4_phi;
    parameters[129] = Ls1810_12_Bw1_amp;
    parameters[130] = Ls1810_12_Bw1_phi;
    parameters[131] = Ls1810_12_Bw2_amp;
    parameters[132] = Ls1810_12_Bw2_phi;
    parameters[133] = Ls1810_12_Bw3_amp;
    parameters[134] = Ls1810_12_Bw3_phi;
    parameters[135] = Ls1820_52_Bw1_amp;
    parameters[136] = Ls1820_52_Bw1_phi;
    parameters[137] = Ls1820_52_Bw2_amp;
    parameters[138] = Ls1820_52_Bw2_phi;
    parameters[139] = Ls1820_52_Bw3_amp;
    parameters[140] = Ls1820_52_Bw3_phi;
    parameters[141] = Ls1830_52_Bw1_amp;
    parameters[142] = Ls1830_52_Bw1_phi;
    parameters[143] = Ls1830_52_Bw2_amp;
    parameters[144] = Ls1830_52_Bw2_phi;
    parameters[145] = Ls1830_52_Bw3_amp;
    parameters[146] = Ls1830_52_Bw3_phi;
    parameters[147] = Ls1890_32_Bw1_amp;
    parameters[148] = Ls1890_32_Bw1_phi;
    parameters[149] = Ls1890_32_Bw2_amp;
    parameters[150] = Ls1890_32_Bw2_phi;
    parameters[151] = Ls1890_32_Bw3_amp;
    parameters[152] = Ls1890_32_Bw3_phi;
    parameters[153] = Ls2020_72_Bw1_amp;
    parameters[154] = Ls2020_72_Bw1_phi;
    parameters[155] = Ls2020_72_Bw2_amp;
    parameters[156] = Ls2020_72_Bw2_phi;
    parameters[157] = Ls2020_72_Bw3_amp;
    parameters[158] = Ls2020_72_Bw3_phi;
    parameters[159] = Ls2050_32_Bw1_amp;
    parameters[160] = Ls2050_32_Bw1_phi;
    parameters[161] = Ls2050_32_Bw2_amp;
    parameters[162] = Ls2050_32_Bw2_phi;
    parameters[163] = Ls2050_32_Bw3_amp;
    parameters[164] = Ls2050_32_Bw3_phi;
    parameters[165] = Ls2100_72_Bw1_amp;
    parameters[166] = Ls2100_72_Bw1_phi;
    parameters[167] = Ls2100_72_Bw2_amp;
    parameters[168] = Ls2100_72_Bw2_phi;
    parameters[169] = Ls2100_72_Bw3_amp;
    parameters[170] = Ls2100_72_Bw3_phi;
    parameters[171] = Ls2110_52_Bw1_amp;
    parameters[172] = Ls2110_52_Bw1_phi;
    parameters[173] = Ls2110_52_Bw2_amp;
    parameters[174] = Ls2110_52_Bw2_phi;
    parameters[175] = Ls2110_52_Bw3_amp;
    parameters[176] = Ls2110_52_Bw3_phi;
    parameters[177] = Ls2325_32_Bw1_amp;
    parameters[178] = Ls2325_32_Bw1_phi;
    parameters[179] = Ls2325_32_Bw2_amp;
    parameters[180] = Ls2325_32_Bw2_phi;
    parameters[181] = Ls2325_32_Bw3_amp;
    parameters[182] = Ls2325_32_Bw3_phi;
    parameters[183] = Ls2350_92_Bw1_amp;
    parameters[184] = Ls2350_92_Bw1_phi;
    parameters[185] = Ls2350_92_Bw2_amp;
    parameters[186] = Ls2350_92_Bw2_phi;
    parameters[187] = LsNR_12_Bw_amp;
    parameters[188] = LsNR_12_Bw_phi;
    parameters[189] = Pc1Bw1_amp;
    parameters[190] = Pc1Bw1_phi;
    parameters[191] = Pc1Bw2_amp;
    parameters[192] = Pc1Bw2_phi;
    parameters[193] = Pc1Bs1_amp;
    parameters[194] = Pc1Bs1_phi;
    parameters[195] = Pc1Bs2_amp;
    parameters[196] = Pc1Bs2_phi;
    parameters[197] = Pc1Bs3_amp;
    parameters[198] = Pc1Bs3_phi;
    parameters[199] = Pc2Bw1_amp;
    parameters[200] = Pc2Bw1_phi;
    parameters[201] = Pc2Bw2_amp;
    parameters[202] = Pc2Bw2_phi;
    parameters[203] = Pc2Bs1_amp;
    parameters[204] = Pc2Bs1_phi;
    parameters[205] = Pc2Bs2_amp;
    parameters[206] = Pc2Bs2_phi;
    parameters[207] = Pc2Bs3_amp;
    parameters[208] = Pc2Bs3_phi;
    parameters[209] = M01;
    parameters[210] = M02;
    parameters[211] = gamma1;
    parameters[212] = gamma2;
    parameters[213] = comb_bg_norm;
    parameters[214] = n_LB_PPI;
    parameters[215] = n_BD_KK;
    parameters[216] = n_BS_KPI;
    parameters[217] = KNR_0_Bw_amp;
    parameters[218] = KNR_0_Bw_phi;
    parameters[219] = x0_ppipip;
    parameters[220] = d0_ppipip;
    parameters[221] = p0_ppipip;
    parameters[222] = p1_ppipip;
    parameters[223] = p2_ppipip;
    parameters[224] = p3_ppipip;
    parameters[225] = a0_ppipip;
    parameters[226] = b0_ppipip;
    parameters[227] = b1_ppipip;
    parameters[228] = b2_ppipip;
    parameters[229] = u0_ppipip;
    parameters[230] = amin;
    parameters[231] = dgamma_Zc4200;
    parameters[232] = dm_Zc4200;
    parameters.Write("parameters");
    
    f_out->Close();
    
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
    
    /*float xmin_KK_o = 4.970;
    
    bg_func->SetParameter(0, par[93]);
    bg_func->SetParameter(1, par[94]);
    bg_func->SetParameter(2, par[95]);
    bg_func->SetParameter(3, par[96]);
    bg_func->SetParameter(4, par[97]);
    bg_func->SetParameter(5, par[98]);
    bg_func->SetParameter(6, par[99]);
    bg_func->SetParameter(7, par[100]);
    bg_func->SetParameter(8, xmin_KK_o);
    
    n_control_comb_bg = bg_func->Integral(5.336, 5.396);*/
    
	//cout << "{" << K1410_1_Bw1_amp << ", " << K1410_1_Bw1_phi << ", " << K1430_0_Bw1_amp << ", " << K1430_0_Bw1_phi << ", " << K1430_2_Bw1_amp << ", " << K1430_2_Bw1_phi << ", " << K1680_1_Bw1_amp << ", " << K1680_1_Bw1_phi << ", " << K1780_3_Bw1_amp << ", " << K1780_3_Bw1_phi << ", " << K1950_0_Bw1_amp << ", " << K1950_0_Bw1_phi << ", " << K2045_4_Bw1_amp << ", " << K2045_4_Bw1_phi << ", " << K1980_2_Bw1_amp << ", " << K1980_2_Bw1_amp << ", " << K1410_1_Bw2_amp << ", " << K1410_1_Bw2_phi << ", " << K1410_1_Bw3_amp << ", " << K1410_1_Bw3_phi << ", " << K1430_2_Bw2_amp << ", " << K1430_2_Bw2_phi << ", " << K1430_2_Bw3_amp << ", " << K1430_2_Bw3_phi << ", " << K1680_1_Bw2_amp << ", " << K1680_1_Bw2_phi << ", " << K1680_1_Bw3_amp << ", " << K1680_1_Bw3_phi << ", " << K1780_3_Bw2_amp << ", " << K1780_3_Bw2_phi << ", " << K1780_3_Bw3_amp << ", " << K1780_3_Bw3_phi << ", " << K1980_2_Bw2_amp << ", " << K1980_2_Bw2_phi << ", " << K1980_2_Bw3_amp << ", " << K1980_2_Bw3_phi << ", " << K2045_4_Bw2_amp << ", " << K2045_4_Bw2_phi << ", " << K2045_4_Bw3_amp << ", " << K2045_4_Bw3_phi << ", " << X1Bs1_amp << ", " << X1Bs1_phi << ", " << X1Bs2_amp << ", " << X1Bs2_phi << ", " << gamma_Zc4200 << ", " << m_Zc4200 << "}" << endl;

	//cout << "{" << dK1410_1_Bw1_amp << ", " << dK1410_1_Bw1_phi << ", " << dK1430_0_Bw1_amp << ", " << dK1430_0_Bw1_phi << ", " << dK1430_2_Bw1_amp << ", " << dK1430_2_Bw1_phi << ", " << dK1680_1_Bw1_amp << ", " << dK1680_1_Bw1_phi << ", " << dK1780_3_Bw1_amp << ", " << dK1780_3_Bw1_phi << ", " << dK1950_0_Bw1_amp << ", " << dK1950_0_Bw1_phi << ", " << dK2045_4_Bw1_amp << ", " << dK2045_4_Bw1_phi << ", " << dK1980_2_Bw1_amp << ", " << dK1980_2_Bw1_amp << ", " << dK1410_1_Bw2_amp << ", " << dK1410_1_Bw2_phi << ", " << dK1410_1_Bw3_amp << ", " << dK1410_1_Bw3_phi << ", " << dK1430_2_Bw2_amp << ", " << dK1430_2_Bw2_phi << ", " << dK1430_2_Bw3_amp << ", " << dK1430_2_Bw3_phi << ", " << dK1680_1_Bw2_amp << ", " << dK1680_1_Bw2_phi << ", " << dK1680_1_Bw3_amp << ", " << dK1680_1_Bw3_phi << ", " << dK1780_3_Bw2_amp << ", " << dK1780_3_Bw2_phi << ", " << dK1780_3_Bw3_amp << ", " << dK1780_3_Bw3_phi << ", " << dK1980_2_Bw2_amp << ", " << dK1980_2_Bw2_phi << ", " << dK1980_2_Bw3_amp << ", " << dK1980_2_Bw3_phi << ", " << dK2045_4_Bw2_amp << ", " << dK2045_4_Bw2_phi << ", " << dK2045_4_Bw3_amp << ", " << dK2045_4_Bw3_phi << ", " << dX1Bs1_amp << ", " << dX1Bs1_phi << ", " << dX1Bs2_amp << ", " << dX1Bs2_phi << ", " << dgamma_Zc4200 << ", " << dm_Zc4200 << "}" << endl;
 
    float me;
    
    Kpi_2d_BD_to_JPSI_K_PI->Reset();
    Jpsipi_2d_BD_to_JPSI_K_PI->Reset();
    JpsiK_2d_BD_to_JPSI_K_PI->Reset();
    
    Kpi_2d_BS_to_JPSI_K_K->Reset();
    Jpsipi_2d_BS_to_JPSI_K_K->Reset();
    JpsiK_2d_BS_to_JPSI_K_K->Reset();
    
    Kpi_2d_LAMBDA0B_to_JPSI_P_K->Reset();
    Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->Reset();
    JpsiK_2d_LAMBDA0B_to_JPSI_P_K->Reset();
    
    JpsiK_2d_control_BD_to_JPSI_K_PI->Reset();
    KK_1d_control_BD_to_JPSI_K_PI->Reset();
    
    JpsiK_2d_control_BS_to_JPSI_K_K->Reset();
    KK_1d_control_BS_to_JPSI_K_K->Reset();
    
    JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K->Reset();
    KK_1d_control_LAMBDA0B_to_JPSI_P_K->Reset();
    
    Kpi_piK_2d_BS_to_JPSI_K_K->Reset();
    Kpi_piK_2d_BD_to_JPSI_K_PI->Reset();
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Reset();
    
    ppi_pip_2d_BS_to_JPSI_K_K->Reset();
    ppi_pip_2d_BD_to_JPSI_K_PI->Reset();
    ppi_pip_2d_LAMBDA0B_to_JPSI_P_K->Reset();
    
    pK_Kp_2d_BS_to_JPSI_K_K->Reset();
    pK_Kp_2d_BD_to_JPSI_K_PI->Reset();
    pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->Reset();
    
    /*pK_Kp_2d_test_BS_to_JPSI_K_K->Reset();
    pK_Kp_2d_test_BD_to_JPSI_K_PI->Reset();
    pK_Kp_2d_test_LAMBDA0B_to_JPSI_P_K->Reset();*/
    
    KK_1d_BS_to_JPSI_K_K->Reset();
    KK_1d_BD_to_JPSI_K_PI->Reset();
    KK_1d_LAMBDA0B_to_JPSI_P_K->Reset();
    
    pipi_1d_BD_to_JPSI_K_PI->Reset();
    pipi_1d_BS_to_JPSI_K_K->Reset();
    pipi_1d_LAMBDA0B_to_JPSI_P_K->Reset();
    
    pK_2d_controlLb_BD_to_JPSI_K_PI->Reset();
    Jpsip_2d_controlLb_BD_to_JPSI_K_PI->Reset();
    JpsiK_2d_controlLb_BD_to_JPSI_K_PI->Reset();
    
    pK_2d_controlLb_BS_to_JPSI_K_K->Reset();
    Jpsip_2d_controlLb_BS_to_JPSI_K_K->Reset();
    JpsiK_2d_controlLb_BS_to_JPSI_K_K->Reset();
    
    pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Reset();
    Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Reset();
    JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Reset();
    
    coef_4_to_s_BD_to_JPSI_K_PI = 0.;
    coef_4_to_c_BD_to_JPSI_K_PI = 0.;
    coef_4_to_clb_BD_to_JPSI_K_PI = 0.;
    coef_4_to_s_BS_to_JPSI_K_K = 0.;
    coef_4_to_c_BS_to_JPSI_K_K = 0.;
    coef_4_to_clb_BS_to_JPSI_K_K = 0.;
    coef_4_to_s_LAMBDA0B_to_JPSI_P_K = 0.;
    coef_4_to_c_LAMBDA0B_to_JPSI_P_K = 0.;
    coef_4_to_clb_LAMBDA0B_to_JPSI_P_K = 0.;
    
    cos_theta_Zc_BS_to_JPSI_K_K->Reset();
    cos_theta_Zc_BD_to_JPSI_K_PI->Reset();
    cos_theta_Zc_LAMBDA0B_to_JPSI_P_K->Reset();
    
    cos_theta_B0_BS_to_JPSI_K_K->Reset();
    cos_theta_B0_BD_to_JPSI_K_PI->Reset();
    cos_theta_B0_LAMBDA0B_to_JPSI_P_K->Reset();
    
    phi_psi_BS_to_JPSI_K_K->Reset();
    phi_psi_BD_to_JPSI_K_PI->Reset();
    phi_psi_LAMBDA0B_to_JPSI_P_K->Reset();
    
    phi_K_BD_to_JPSI_K_PI->Reset();
    phi_K_BS_to_JPSI_K_K->Reset();
    phi_K_LAMBDA0B_to_JPSI_P_K->Reset();
    
    alpha_mu_BD_to_JPSI_K_PI->Reset();
    alpha_mu_BS_to_JPSI_K_K->Reset();
    alpha_mu_LAMBDA0B_to_JPSI_P_K->Reset();
    
    phi_mu_Kstar_BD_to_JPSI_K_PI->Reset();
    phi_mu_Kstar_BS_to_JPSI_K_K->Reset();
    phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K->Reset();
    
    phi_mu_X_BD_to_JPSI_K_PI->Reset();
    phi_mu_X_BS_to_JPSI_K_K->Reset();
    phi_mu_X_LAMBDA0B_to_JPSI_P_K->Reset();
    
    cos_theta_psi_X_BD_to_JPSI_K_PI->Reset();
    cos_theta_psi_X_BS_to_JPSI_K_K->Reset();
    cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K->Reset();
    
    cos_theta_Kstar_BD_to_JPSI_K_PI->Reset();
    cos_theta_Kstar_BS_to_JPSI_K_K->Reset();
    cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->Reset();
    
    cos_theta_psi_Kstar_BD_to_JPSI_K_PI->Reset();
    cos_theta_psi_Kstar_BS_to_JPSI_K_K->Reset();
    cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K->Reset();
    
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__||signal_area_histograms||__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__// 
    
    for (int i = 0; i < phs_Bd; i++)
    {
        me = HamplitudeXKstar_total_accelerated(Jpsipi_truth[i] / 1000., Kpi_truth[i] / 1000., 5.27964, m_Zc4200, 4.43, gamma_Zc4200, 0.181,
						std::complex<float>(X1Bs1_amp * cos(X1Bs1_phi), X1Bs1_amp * sin(X1Bs1_phi)), 
                        std::complex<float>(X1Bs2_amp * cos(X1Bs2_phi), X1Bs2_amp * sin(X1Bs2_phi)),
						std::complex<float>(0., 0.), 
                        std::complex<float>(0., 0.),
                        std::complex<float>(K1410_1_Bw1_amp * cos(K1410_1_Bw1_phi), K1410_1_Bw1_amp * sin(K1410_1_Bw1_phi)), 
                        std::complex<float>(K1410_1_Bw2_amp * cos(K1410_1_Bw2_phi), K1410_1_Bw2_amp * sin(K1410_1_Bw2_phi)), 
                        std::complex<float>(K1410_1_Bw3_amp * cos(K1410_1_Bw3_phi), K1410_1_Bw3_amp * sin(K1410_1_Bw3_phi)),
						std::complex<float>(K1430_0_Bw1_amp * cos(K1430_0_Bw1_phi), K1430_0_Bw1_amp * sin(K1430_0_Bw1_phi)), 
                        std::complex<float>(0., 0.), 
                        std::complex<float>(0., 0.),
						std::complex<float>(K1430_2_Bw1_amp * cos(K1430_2_Bw1_phi), K1430_2_Bw1_amp * sin(K1430_2_Bw1_phi)), 
                        std::complex<float>(K1430_2_Bw2_amp * cos(K1430_2_Bw2_phi), K1430_2_Bw2_amp * sin(K1430_2_Bw2_phi)), 
                        std::complex<float>(K1430_2_Bw3_amp * cos(K1430_2_Bw3_phi), K1430_2_Bw3_amp * sin(K1430_2_Bw3_phi)),
						std::complex<float>(K1680_1_Bw1_amp * cos(K1680_1_Bw1_phi), K1680_1_Bw1_amp * sin(K1680_1_Bw1_phi)), 
                        std::complex<float>(K1680_1_Bw2_amp * cos(K1680_1_Bw2_phi), K1680_1_Bw2_amp * sin(K1680_1_Bw2_phi)), 
                        std::complex<float>(K1680_1_Bw3_amp * cos(K1680_1_Bw3_phi), K1680_1_Bw3_amp * sin(K1680_1_Bw3_phi)),
						std::complex<float>(K1780_3_Bw1_amp * cos(K1780_3_Bw1_phi), K1780_3_Bw1_amp * sin(K1780_3_Bw1_phi)), 
                        std::complex<float>(K1780_3_Bw2_amp * cos(K1780_3_Bw2_phi), K1780_3_Bw2_amp * sin(K1780_3_Bw2_phi)), 
                        std::complex<float>(K1780_3_Bw3_amp * cos(K1780_3_Bw3_phi), K1780_3_Bw3_amp * sin(K1780_3_Bw3_phi)),
						std::complex<float>(K1950_0_Bw1_amp * cos(K1950_0_Bw1_phi), K1950_0_Bw1_amp * sin(K1950_0_Bw1_phi)), 
                        std::complex<float>(0., 0.), 
                        std::complex<float>(0., 0.),
						std::complex<float>(K1980_2_Bw1_amp * cos(K1980_2_Bw1_phi), K1980_2_Bw1_amp * sin(K1980_2_Bw1_phi)),
                        std::complex<float>(K1980_2_Bw2_amp * cos(K1980_2_Bw2_phi), K1980_2_Bw2_amp * sin(K1980_2_Bw2_phi)), 
                        std::complex<float>(K1980_2_Bw3_amp * cos(K1980_2_Bw3_phi), K1980_2_Bw3_amp * sin(K1980_2_Bw3_phi)),
						std::complex<float>(K2045_4_Bw1_amp * cos(K2045_4_Bw1_phi), K2045_4_Bw1_amp * sin(K2045_4_Bw1_phi)),
                        std::complex<float>(K2045_4_Bw2_amp * cos(K2045_4_Bw2_phi), K2045_4_Bw2_amp * sin(K2045_4_Bw2_phi)), 
                        std::complex<float>(K2045_4_Bw3_amp * cos(K2045_4_Bw3_phi), K2045_4_Bw3_amp * sin(K2045_4_Bw3_phi)),
						std::complex<float>(KNR_0_Bw_amp * cos(KNR_0_Bw_phi), KNR_0_Bw_amp * sin(KNR_0_Bw_phi)),
						theta_X_a[i], theta_Kstar_a[i], theta_psi_X_a[i], theta_psi_Kstar_a[i], phi_mu_X_a[i], phi_mu_Kstar_a[i], 
						phi_K_a[i], alpha_mu_a[i],
                        BWK1410_1_1_minus[i], BWK1410_1_2_minus[i], BWK1410_1_3_minus[i], 
                        BWK1430_0_1_minus[i], 
                        BWK1430_2_1_minus[i], BWK1430_2_2_minus[i], BWK1430_2_3_minus[i], 
                        BWK1680_1_1_minus[i], BWK1680_1_2_minus[i], BWK1680_1_3_minus[i], 
                        BWK1780_3_1_minus[i], BWK1780_3_2_minus[i], BWK1780_3_3_minus[i], 
                        BWK1950_0_1_minus[i], 
                        BWK1980_2_1_minus[i], BWK1980_2_2_minus[i], BWK1980_2_3_minus[i], 
                        BWK2045_4_1_minus[i], BWK2045_4_2_minus[i], BWK2045_4_3_minus[i],
                        BWK1410_1_1_plus[i], BWK1410_1_2_plus[i], BWK1410_1_3_plus[i], 
                        BWK1430_0_1_plus[i], 
                        BWK1430_2_1_plus[i], BWK1430_2_2_plus[i], BWK1430_2_3_plus[i], 
                        BWK1680_1_1_plus[i], BWK1680_1_2_plus[i], BWK1680_1_3_plus[i], 
                        BWK1780_3_1_plus[i], BWK1780_3_2_plus[i], BWK1780_3_3_plus[i], 
                        BWK1950_0_1_plus[i], 
                        BWK1980_2_1_plus[i], BWK1980_2_2_plus[i], BWK1980_2_3_plus[i], 
                        BWK2045_4_1_plus[i], BWK2045_4_2_plus[i], BWK2045_4_3_plus[i]) * w_dimuon_a[i];
        //me  = 1.;
        if (signal_a[i]) {
        Kpi_2d_BD_to_JPSI_K_PI->Fill(K1pi2_mass[i], K2pi1_mass[i], me);
        Jpsipi_2d_BD_to_JPSI_K_PI->Fill(Jpsipi1_mass[i], Jpsipi2_mass[i], me);
        JpsiK_2d_BD_to_JPSI_K_PI->Fill(JpsiK1_mass[i], JpsiK2_mass[i], me);
        cos_theta_Zc_BD_to_JPSI_K_PI->Fill(cos_theta_Zc_Kpi_Bd[i], cos_theta_Zc_piK_Bd[i], me);
        phi_K_BD_to_JPSI_K_PI->Fill(phi_K_Kpi_Bd[i], phi_K_piK_Bd[i], me);
        alpha_mu_BD_to_JPSI_K_PI->Fill(alpha_mu_Kpi_Bd[i], alpha_mu_piK_Bd[i], me);
        phi_mu_Kstar_BD_to_JPSI_K_PI->Fill(phi_mu_Kstar_Kpi_Bd[i], phi_mu_Kstar_piK_Bd[i], me);
        phi_mu_X_BD_to_JPSI_K_PI->Fill(phi_mu_X_Kpi_Bd[i], phi_mu_X_piK_Bd[i], me);
        cos_theta_psi_X_BD_to_JPSI_K_PI->Fill(cos_theta_psi_X_Kpi_Bd[i], cos_theta_psi_X_piK_Bd[i], me);
        cos_theta_Kstar_BD_to_JPSI_K_PI->Fill(cos_theta_Kstar_Kpi_Bd[i], cos_theta_Kstar_piK_Bd[i], me);
        cos_theta_psi_Kstar_BD_to_JPSI_K_PI->Fill(cos_theta_psi_Kstar_Kpi_Bd[i], cos_theta_psi_Kstar_piK_Bd[i], me);
        cos_theta_B0_BD_to_JPSI_K_PI->Fill(cos_theta_B0_Kpi_Bd[i], cos_theta_B0_piK_Bd[i], me);
        phi_psi_BD_to_JPSI_K_PI->Fill(phi_psi_Kpi_Bd[i], phi_psi_piK_Bd[i], me);
        coef_4_to_s_BD_to_JPSI_K_PI += me;
        }
        
        if (control_a[i])
        {
            JpsiK_2d_control_BD_to_JPSI_K_PI->Fill(JpsiK1_mass[i], JpsiK2_mass[i], me);
            KK_1d_control_BD_to_JPSI_K_PI->Fill(KK_mass[i], me);
            coef_4_to_c_BD_to_JPSI_K_PI += me;
        }
        if (controlLb_a[i])
        {
            JpsiK_2d_controlLb_BD_to_JPSI_K_PI->Fill(JpsiK1_mass[i], JpsiK2_mass[i], me);
            Jpsip_2d_controlLb_BD_to_JPSI_K_PI->Fill(Jpsip1_mass[i], Jpsip2_mass[i], me);
            pK_2d_controlLb_BD_to_JPSI_K_PI->Fill(p1K2_mass[i], p2K1_mass[i], me);
            coef_4_to_clb_BD_to_JPSI_K_PI += me;
        }
        
        Kpi_piK_2d_BD_to_JPSI_K_PI->Fill(JpsiKpi_mass[i], JpsipiK_mass[i], me);
        KK_1d_BD_to_JPSI_K_PI->Fill(JpsiKK_mass[i], me);
        pipi_1d_BD_to_JPSI_K_PI->Fill(Jpsipipi_mass[i], me);
        pK_Kp_2d_BD_to_JPSI_K_PI->Fill(JpsipK_mass[i], JpsiKp_mass[i], me);
        pK_Kp_2d_rotate_BD_to_JPSI_K_PI->Fill((1. / sqrt(2.)) * JpsipK_mass[i] + (1. / sqrt(2.)) * JpsiKp_mass[i], -(1. / sqrt(2.)) * JpsipK_mass[i] + (1. / sqrt(2.)) * JpsiKp_mass[i], me);
        //pK_Kp_2d_test_BD_to_JPSI_K_PI->Fill(JpsipK_mass[i], JpsiKp_mass[i], me);
        ppi_pip_2d_BD_to_JPSI_K_PI->Fill(Jpsippi_mass[i], Jpsipip_mass[i], me);
        
        /*if (i < 10)
        {
            cout << me << endl;
        }*/
        
        //cout << me << "\t" << K1pi2_mass[i] << "\t" << Jpsipi1_mass[i] << "\t" << JpsiK1_mass[i] << endl;
        
        //Kpi_err_sum_bd->Fill(Kpi_mass[i], me * me); // may be benefit
		//Jpsipi_err_sum_bd->Fill(Jpsipi_mass[i], me * me); // may be benefit
		//JpsiK_err_sum_bd->Fill(JpsiK_mass[i], me * me); // may be benefit
    }
    
    for (int i = 0; i < phs_Bs; i++)
    {
        me = Hamplitudef_total(0., 0., KK_truth_BsKK[i] / 1000., 5.36688, 0., 0., 0., 0.,
						std::complex<float>(0., 0.), std::complex<float>(0., 0.),
						std::complex<float> (phi1680_1_Bw1_amp * cos(phi1680_1_Bw1_phi), phi1680_1_Bw1_amp * sin(phi1680_1_Bw1_phi)), 
                        std::complex<float> (phi1680_1_Bw2_amp * cos(phi1680_1_Bw2_phi), phi1680_1_Bw2_amp * sin(phi1680_1_Bw2_phi)), 
                        std::complex<float> (phi1680_1_Bw3_amp * cos(phi1680_1_Bw3_phi), phi1680_1_Bw3_amp * sin(phi1680_1_Bw3_phi)),
						std::complex<float> (f1525_2_Bw1_amp * cos(f1525_2_Bw1_phi), f1525_2_Bw1_amp * sin(f1525_2_Bw1_phi)), 
                        std::complex<float> (f1525_2_Bw2_amp * cos(f1525_2_Bw2_phi), f1525_2_Bw2_amp * sin(f1525_2_Bw2_phi)), 
                        std::complex<float> (f1525_2_Bw3_amp * cos(f1525_2_Bw3_phi), f1525_2_Bw3_amp * sin(f1525_2_Bw3_phi)),
						std::complex<float> (f1640_2_Bw1_amp * cos(f1640_2_Bw1_phi), f1640_2_Bw1_amp * sin(f1640_2_Bw1_phi)), 
                        std::complex<float> (f1640_2_Bw2_amp * cos(f1640_2_Bw2_phi), f1640_2_Bw2_amp * sin(f1640_2_Bw2_phi)), 
                        std::complex<float> (f1640_2_Bw3_amp * cos(f1640_2_Bw3_phi), f1640_2_Bw3_amp * sin(f1640_2_Bw3_phi)),
						std::complex<float> (f1750_2_Bw1_amp * cos(f1750_2_Bw1_phi), f1750_2_Bw1_amp * sin(f1750_2_Bw1_phi)), 
                        std::complex<float> (f1750_2_Bw2_amp * cos(f1750_2_Bw2_phi), f1750_2_Bw2_amp * sin(f1750_2_Bw2_phi)),
                        std::complex<float> (f1750_2_Bw3_amp * cos(f1750_2_Bw3_phi), f1750_2_Bw3_amp * sin(f1750_2_Bw3_phi)),
						std::complex<float> (f1950_2_Bw1_amp * cos(f1950_2_Bw1_phi), f1950_2_Bw1_amp * sin(f1950_2_Bw1_phi)), 
                        std::complex<float> (f1950_2_Bw2_amp * cos(f1950_2_Bw2_phi), f1950_2_Bw2_amp * sin(f1950_2_Bw2_phi)), 
                        std::complex<float> (f1950_2_Bw3_amp * cos(f1950_2_Bw3_phi), f1950_2_Bw3_amp * sin(f1950_2_Bw3_phi)),
						std::complex<float> (NR_Bw1_amp * cos(NR_Bw1_phi), NR_Bw1_amp * sin(NR_Bw1_phi)),
						0., thetaF_a[i], 0., theta_psi_F_a[i], 0., phi_mu_F_a[i], phi_KF_a[i], 0., 0., 0.) * w_dimuon_a_BsKK[i];
        
        if (signal_a_BsKK[i])
        {
            Kpi_2d_BS_to_JPSI_K_K->Fill(K1pi2_mass_BsKK[i], K2pi1_mass_BsKK[i], me);
            Jpsipi_2d_BS_to_JPSI_K_K->Fill(Jpsipi1_mass_BsKK[i], Jpsipi2_mass_BsKK[i], me);
            JpsiK_2d_BS_to_JPSI_K_K->Fill(JpsiK1_mass_BsKK[i], JpsiK2_mass_BsKK[i], me);
            cos_theta_Zc_BS_to_JPSI_K_K->Fill(cos_theta_Zc_Kpi_Bs[i], cos_theta_Zc_piK_Bs[i], me);
            phi_K_BS_to_JPSI_K_K->Fill(phi_K_Kpi_Bs[i], phi_K_piK_Bs[i], me);
            alpha_mu_BS_to_JPSI_K_K->Fill(alpha_mu_Kpi_Bs[i], alpha_mu_piK_Bs[i], me);
            phi_mu_Kstar_BS_to_JPSI_K_K->Fill(phi_mu_Kstar_Kpi_Bs[i], phi_mu_Kstar_piK_Bs[i], me);
            phi_mu_X_BS_to_JPSI_K_K->Fill(phi_mu_X_Kpi_Bs[i], phi_mu_X_piK_Bs[i], me);
            cos_theta_psi_X_BS_to_JPSI_K_K->Fill(cos_theta_psi_X_Kpi_Bs[i], cos_theta_psi_X_piK_Bs[i], me);
            cos_theta_Kstar_BS_to_JPSI_K_K->Fill(cos_theta_Kstar_Kpi_Bs[i], cos_theta_Kstar_piK_Bs[i], me);
            cos_theta_psi_Kstar_BS_to_JPSI_K_K->Fill(cos_theta_psi_Kstar_Kpi_Bs[i], cos_theta_psi_Kstar_piK_Bs[i], me);
            cos_theta_B0_BS_to_JPSI_K_K->Fill(cos_theta_B0_Kpi_Bs[i], cos_theta_B0_piK_Bs[i], me);
            phi_psi_BS_to_JPSI_K_K->Fill(phi_psi_Kpi_Bs[i], phi_psi_piK_Bs[i], me);
            coef_4_to_s_BS_to_JPSI_K_K += me;
        }
        if (control_a_BsKK[i])
        {
            JpsiK_2d_control_BS_to_JPSI_K_K->Fill(JpsiK1_mass_BsKK[i], JpsiK2_mass_BsKK[i], me);
            KK_1d_control_BS_to_JPSI_K_K->Fill(KK_mass_BsKK[i], me);
            coef_4_to_c_BS_to_JPSI_K_K += me;
        }
        if (controlLb_a_BsKK[i])
        {
            JpsiK_2d_controlLb_BS_to_JPSI_K_K->Fill(JpsiK1_mass_BsKK[i], JpsiK2_mass_BsKK[i], me);
            Jpsip_2d_controlLb_BS_to_JPSI_K_K->Fill(Jpsip1_mass_BsKK[i], Jpsip2_mass_BsKK[i], me);
            pK_2d_controlLb_BS_to_JPSI_K_K->Fill(p1K2_mass_BsKK[i], p2K1_mass_BsKK[i], me);
            coef_4_to_clb_BS_to_JPSI_K_K += me;
        }
        
        Kpi_piK_2d_BS_to_JPSI_K_K->Fill(JpsiKpi_mass_BsKK[i], JpsipiK_mass_BsKK[i], me);
        KK_1d_BS_to_JPSI_K_K->Fill(JpsiKK_mass_BsKK[i], me);
        pipi_1d_BS_to_JPSI_K_K->Fill(Jpsipipi_mass_BsKK[i], me);
        pK_Kp_2d_BS_to_JPSI_K_K->Fill(JpsipK_mass_BsKK[i], JpsiKp_mass_BsKK[i], me);
        pK_Kp_2d_rotate_BS_to_JPSI_K_K->Fill((1. / sqrt(2.)) * JpsipK_mass_BsKK[i] + (1. / sqrt(2.)) * JpsiKp_mass_BsKK[i], -(1. / sqrt(2.)) * JpsipK_mass_BsKK[i] + (1. / sqrt(2.)) * JpsiKp_mass_BsKK[i], me);
        //pK_Kp_2d_test_BS_to_JPSI_K_K->Fill(JpsipK_mass_BsKK[i], JpsiKp_mass_BsKK[i], me);
        ppi_pip_2d_BS_to_JPSI_K_K->Fill(Jpsippi_mass_BsKK[i], Jpsipip_mass_BsKK[i], me);
    }
    
    for (int i = 0; i < phs_Lb; i++)
    {
        me = HamplitudePcLstar_total(Jpsip_truth_Lb[i] / 1000., pK_truth_Lb[i] / 1000., 5.6196, M01, M02, 0., 0., gamma1, gamma2, 0., 0.,
						std::complex<float> (Pc1Bw1_amp * cos(Pc1Bw1_phi), Pc1Bw1_amp * cos(Pc1Bw1_phi)), 
                        std::complex<float> (Pc1Bw2_amp * cos(Pc1Bw2_phi), Pc1Bw2_amp * cos(Pc1Bw2_phi)), 
                        std::complex<float> (Pc1Bs1_amp * cos(Pc1Bs1_phi), Pc1Bs1_amp * cos(Pc1Bs1_phi)), 
                        std::complex<float> (Pc1Bs2_amp * cos(Pc1Bs2_phi), Pc1Bs2_amp * cos(Pc1Bs2_phi)), 
                        std::complex<float> (Pc1Bs3_amp * cos(Pc1Bs3_phi), Pc1Bs3_amp * cos(Pc1Bs3_phi)),
						std::complex<float> (Pc2Bw1_amp * cos(Pc2Bw1_phi), Pc2Bw1_amp * cos(Pc2Bw1_phi)), 
                        std::complex<float> (Pc2Bw2_amp * cos(Pc2Bw2_phi), Pc2Bw2_amp * cos(Pc2Bw2_phi)), 
                        std::complex<float> (Pc2Bs1_amp * cos(Pc2Bs1_phi), Pc2Bs1_amp * cos(Pc2Bs1_phi)), 
                        std::complex<float> (Pc2Bs2_amp * cos(Pc2Bs2_phi), Pc2Bs2_amp * cos(Pc2Bs2_phi)), 
                        std::complex<float> (Pc2Bs3_amp * cos(Pc2Bs3_phi), Pc2Bs3_amp * cos(Pc2Bs3_phi)),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (Ls1800_12_Bw1_amp * cos(Ls1800_12_Bw1_phi), Ls1800_12_Bw1_amp * cos(Ls1800_12_Bw1_phi)), 
                        std::complex<float> (Ls1800_12_Bw2_amp * cos(Ls1800_12_Bw2_phi), Ls1800_12_Bw2_amp * cos(Ls1800_12_Bw2_phi)), 
                        std::complex<float> (Ls1800_12_Bw3_amp * cos(Ls1800_12_Bw3_phi), Ls1800_12_Bw3_amp * cos(Ls1800_12_Bw3_phi)), 
                        std::complex<float> (Ls1800_12_Bw4_amp * cos(Ls1800_12_Bw4_phi), Ls1800_12_Bw4_amp * cos(Ls1800_12_Bw4_phi)),
						std::complex<float> (Ls1810_12_Bw1_amp * cos(Ls1810_12_Bw1_phi), Ls1810_12_Bw1_amp * cos(Ls1810_12_Bw1_phi)), 
                        std::complex<float> (Ls1810_12_Bw2_amp * cos(Ls1810_12_Bw2_phi), Ls1810_12_Bw2_amp * cos(Ls1810_12_Bw2_phi)), 
                        std::complex<float> (Ls1810_12_Bw3_amp * cos(Ls1810_12_Bw3_phi), Ls1810_12_Bw3_amp * cos(Ls1810_12_Bw3_phi)),
						std::complex<float> (Ls1820_52_Bw1_amp * cos(Ls1820_52_Bw1_phi), Ls1820_52_Bw1_amp * cos(Ls1820_52_Bw1_phi)), 
                        std::complex<float> (Ls1820_52_Bw2_amp * cos(Ls1820_52_Bw2_phi), Ls1820_52_Bw2_amp * cos(Ls1820_52_Bw2_phi)), 
                        std::complex<float> (Ls1820_52_Bw3_amp * cos(Ls1820_52_Bw3_phi), Ls1820_52_Bw3_amp * cos(Ls1820_52_Bw3_phi)),
						std::complex<float> (Ls1830_52_Bw1_amp * cos(Ls1830_52_Bw1_phi), Ls1830_52_Bw1_amp * cos(Ls1830_52_Bw1_phi)), 
                        std::complex<float> (Ls1830_52_Bw2_amp * cos(Ls1830_52_Bw2_phi), Ls1830_52_Bw2_amp * cos(Ls1830_52_Bw2_phi)), 
                        std::complex<float> (Ls1830_52_Bw3_amp * cos(Ls1830_52_Bw3_phi), Ls1830_52_Bw3_amp * cos(Ls1830_52_Bw3_phi)),
						std::complex<float> (Ls1890_32_Bw1_amp * cos(Ls1890_32_Bw1_phi), Ls1890_32_Bw1_amp * cos(Ls1890_32_Bw1_phi)), 
                        std::complex<float> (Ls1890_32_Bw2_amp * cos(Ls1890_32_Bw2_phi), Ls1890_32_Bw2_amp * cos(Ls1890_32_Bw2_phi)), 
                        std::complex<float> (Ls1890_32_Bw3_amp * cos(Ls1890_32_Bw3_phi), Ls1890_32_Bw3_amp * cos(Ls1890_32_Bw3_phi)),
						std::complex<float> (Ls2020_72_Bw1_amp * cos(Ls2020_72_Bw1_phi), Ls2020_72_Bw1_amp * cos(Ls2020_72_Bw1_phi)), 
                        std::complex<float> (Ls2020_72_Bw2_amp * cos(Ls2020_72_Bw2_phi), Ls2020_72_Bw2_amp * cos(Ls2020_72_Bw2_phi)), 
                        std::complex<float> (Ls2020_72_Bw3_amp * cos(Ls2020_72_Bw3_phi), Ls2020_72_Bw3_amp * cos(Ls2020_72_Bw3_phi)),
						std::complex<float> (Ls2050_32_Bw1_amp * cos(Ls2050_32_Bw1_phi), Ls2050_32_Bw1_amp * cos(Ls2050_32_Bw1_phi)), 
                        std::complex<float> (Ls2050_32_Bw2_amp * cos(Ls2050_32_Bw2_phi), Ls2050_32_Bw2_amp * cos(Ls2050_32_Bw2_phi)),
                        std::complex<float> (Ls2050_32_Bw3_amp * cos(Ls2050_32_Bw3_phi), Ls2050_32_Bw3_amp * cos(Ls2050_32_Bw3_phi)),
						std::complex<float> (Ls2100_72_Bw1_amp * cos(Ls2100_72_Bw1_phi), Ls2100_72_Bw1_amp * cos(Ls2100_72_Bw1_phi)), 
                        std::complex<float> (Ls2100_72_Bw2_amp * cos(Ls2100_72_Bw2_phi), Ls2100_72_Bw2_amp * cos(Ls2100_72_Bw2_phi)), 
                        std::complex<float> (Ls2100_72_Bw3_amp * cos(Ls2100_72_Bw3_phi), Ls2100_72_Bw3_amp * cos(Ls2100_72_Bw3_phi)),
						std::complex<float> (Ls2110_52_Bw1_amp * cos(Ls2110_52_Bw1_phi), Ls2110_52_Bw1_amp * cos(Ls2110_52_Bw1_phi)), 
                        std::complex<float> (Ls2110_52_Bw2_amp * cos(Ls2110_52_Bw2_phi), Ls2110_52_Bw2_amp * cos(Ls2110_52_Bw2_phi)), 
                        std::complex<float> (Ls2110_52_Bw3_amp * cos(Ls2110_52_Bw3_phi), Ls2110_52_Bw3_amp * cos(Ls2110_52_Bw3_phi)),
						std::complex<float> (Ls2325_32_Bw1_amp * cos(Ls2325_32_Bw1_phi), Ls2325_32_Bw1_amp * cos(Ls2325_32_Bw1_phi)), 
                        std::complex<float> (Ls2325_32_Bw2_amp * cos(Ls2325_32_Bw2_phi), Ls2325_32_Bw2_amp * cos(Ls2325_32_Bw2_phi)), 
                        std::complex<float> (Ls2325_32_Bw3_amp * cos(Ls2325_32_Bw3_phi), Ls2325_32_Bw3_amp * cos(Ls2325_32_Bw3_phi)),
						std::complex<float> (Ls2350_92_Bw1_amp * cos(Ls2350_92_Bw1_phi), Ls2350_92_Bw1_amp * cos(Ls2350_92_Bw1_phi)), 
                        std::complex<float> (Ls2350_92_Bw2_amp * cos(Ls2350_92_Bw2_phi), Ls2350_92_Bw2_amp * cos(Ls2350_92_Bw2_phi)), 
                        std::complex<float> (LsNR_12_Bw_amp * cos(LsNR_12_Bw_phi), LsNR_12_Bw_amp * cos(LsNR_12_Bw_phi)),
						theta_Lstar_a[i], theta_lambda_b_ls_a[i], thete_psi_Lstar_a[i], phi_mu_Lstar_a[i], phi_K_Lstar_a[i],
						theta_Pc_a[i], theta_lambda_b_a[i], theta_psi_a[i], phi_mu_Pc_a[i], phi_Pc_a[i], phi_psi_a[i], 
						alpha_mu_Lb_a[i], theta_p_a[i]) * w_dimuon_a_Lb[i];
                //me = 1.;
        
        if (signal_a_Lb[i])
        {
            Kpi_2d_LAMBDA0B_to_JPSI_P_K->Fill(K1pi2_mass_Lb[i], K2pi1_mass_Lb[i], me);
            Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->Fill(Jpsipi1_mass_Lb[i], Jpsipi2_mass_Lb[i], me);
            JpsiK_2d_LAMBDA0B_to_JPSI_P_K->Fill(JpsiK1_mass_Lb[i], JpsiK2_mass_Lb[i], me);
            cos_theta_Zc_LAMBDA0B_to_JPSI_P_K->Fill(cos_theta_Zc_Kpi_Lb[i], cos_theta_Zc_piK_Lb[i], me);
            phi_K_LAMBDA0B_to_JPSI_P_K->Fill(phi_K_Kpi_Lb[i], phi_K_piK_Lb[i], me);
            alpha_mu_LAMBDA0B_to_JPSI_P_K->Fill(alpha_mu_Kpi_Lb[i], alpha_mu_piK_Lb[i], me);
            phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K->Fill(phi_mu_Kstar_Kpi_Lb[i], phi_mu_Kstar_piK_Lb[i], me);
            phi_mu_X_LAMBDA0B_to_JPSI_P_K->Fill(phi_mu_X_Kpi_Lb[i], phi_mu_X_piK_Lb[i], me);
            cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K->Fill(cos_theta_psi_X_Kpi_Lb[i], cos_theta_psi_X_piK_Lb[i], me);
            cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->Fill(cos_theta_Kstar_Kpi_Lb[i], cos_theta_Kstar_piK_Lb[i], me);
            cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K->Fill(cos_theta_psi_Kstar_Kpi_Lb[i], cos_theta_psi_Kstar_piK_Lb[i], me);
            cos_theta_B0_LAMBDA0B_to_JPSI_P_K->Fill(cos_theta_B0_Kpi_Lb[i], cos_theta_B0_piK_Lb[i], me);
            phi_psi_LAMBDA0B_to_JPSI_P_K->Fill(phi_psi_Kpi_Lb[i], phi_psi_piK_Lb[i], me);
            coef_4_to_s_LAMBDA0B_to_JPSI_P_K += me;
        }
        if (control_a_Lb[i])
        {
            JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K->Fill(JpsiK1_mass_Lb[i], JpsiK2_mass_Lb[i], me);
            KK_1d_control_LAMBDA0B_to_JPSI_P_K->Fill(KK_mass_Lb[i], me);
            coef_4_to_c_LAMBDA0B_to_JPSI_P_K += me;
        }
        if (controlLb_a_Lb[i])
        {
            JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Fill(JpsiK1_mass_Lb[i], JpsiK2_mass_Lb[i], me);
            Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Fill(Jpsip1_mass_Lb[i], Jpsip2_mass_Lb[i], me);
            pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Fill(p1K2_mass_Lb[i], p2K1_mass_Lb[i], me);
            coef_4_to_clb_LAMBDA0B_to_JPSI_P_K += me;
        }
        
        Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Fill(JpsiKpi_mass_Lb[i], JpsipiK_mass_Lb[i], me);
        KK_1d_LAMBDA0B_to_JPSI_P_K->Fill(JpsiKK_mass_Lb[i], me);
        pipi_1d_LAMBDA0B_to_JPSI_P_K->Fill(Jpsipipi_mass_Lb[i], me);
        pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->Fill(JpsipK_mass_Lb[i], JpsiKp_mass_Lb[i], me);
        pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_K->Fill((1. / sqrt(2.)) * JpsipK_mass_Lb[i] + (1. / sqrt(2.)) * JpsiKp_mass_Lb[i], -(1. / sqrt(2.)) * JpsipK_mass_Lb[i] + (1. / sqrt(2.)) * JpsiKp_mass_Lb[i], me);
        //pK_Kp_2d_test_LAMBDA0B_to_JPSI_P_K->Fill(JpsipK_mass_Lb[i], JpsiKp_mass_Lb[i], me);
        ppi_pip_2d_LAMBDA0B_to_JPSI_P_K->Fill(Jpsippi_mass_Lb[i], Jpsipip_mass_Lb[i], me);
    }
    
    ov_uf_bins_Clear(Kpi_piK_2d_data);
    ov_uf_bins_Clear(Kpi_piK_2d_BD_to_JPSI_K_PI);
    ov_uf_bins_Clear(Kpi_piK_2d_BD_to_JPSI_PI_PI);
    ov_uf_bins_Clear(Kpi_piK_2d_BS_to_JPSI_K_K);
    ov_uf_bins_Clear(Kpi_piK_2d_BS_to_JPSI_PI_PI);
    ov_uf_bins_Clear(Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K);
    ov_uf_bins_Clear(Kpi_piK_2d_bg);
    ov_uf_bins_Clear(Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI);
    ov_uf_bins_Clear(Kpi_piK_2d_BD_to_JPSI_K_K);
    ov_uf_bins_Clear(Kpi_piK_2d_BS_to_JPSI_K_PI);
    
    ov_uf_bins_Clear(pK_Kp_2d_data);
    ov_uf_bins_Clear(pK_Kp_2d_BD_to_JPSI_K_PI);
    ov_uf_bins_Clear(pK_Kp_2d_BD_to_JPSI_PI_PI);
    ov_uf_bins_Clear(pK_Kp_2d_BS_to_JPSI_K_K);
    ov_uf_bins_Clear(pK_Kp_2d_BS_to_JPSI_PI_PI);
    ov_uf_bins_Clear(pK_Kp_2d_LAMBDA0B_to_JPSI_P_K);
    ov_uf_bins_Clear(pK_Kp_2d_bg);
    ov_uf_bins_Clear(pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI);
    ov_uf_bins_Clear(pK_Kp_2d_BD_to_JPSI_K_K);
    ov_uf_bins_Clear(pK_Kp_2d_BS_to_JPSI_K_PI);
    
    ov_uf_bins_Clear(ppi_pip_2d_data);
    ov_uf_bins_Clear(ppi_pip_2d_BD_to_JPSI_K_PI);
    ov_uf_bins_Clear(ppi_pip_2d_BD_to_JPSI_PI_PI);
    ov_uf_bins_Clear(ppi_pip_2d_BS_to_JPSI_K_K);
    ov_uf_bins_Clear(ppi_pip_2d_BS_to_JPSI_PI_PI);
    ov_uf_bins_Clear(ppi_pip_2d_LAMBDA0B_to_JPSI_P_K);
    ov_uf_bins_Clear(ppi_pip_2d_bg);
    ov_uf_bins_Clear(ppi_pip_2d_LAMBDA0B_to_JPSI_P_PI);
    ov_uf_bins_Clear(ppi_pip_2d_BD_to_JPSI_K_K);
    ov_uf_bins_Clear(ppi_pip_2d_BS_to_JPSI_K_PI);
    
    ov_uf_bins_Clear(pK_Kp_2d_rotate_data);
    ov_uf_bins_Clear(pK_Kp_2d_rotate_BD_to_JPSI_K_PI);
    ov_uf_bins_Clear(pK_Kp_2d_rotate_BD_to_JPSI_PI_PI);
    ov_uf_bins_Clear(pK_Kp_2d_rotate_BS_to_JPSI_K_K);
    ov_uf_bins_Clear(pK_Kp_2d_rotate_BS_to_JPSI_PI_PI);
    ov_uf_bins_Clear(pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_K);
    ov_uf_bins_Clear(pK_Kp_2d_rotate_bg);
    ov_uf_bins_Clear(pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_PI);
    ov_uf_bins_Clear(pK_Kp_2d_rotate_BD_to_JPSI_K_K);
    ov_uf_bins_Clear(pK_Kp_2d_rotate_BS_to_JPSI_K_PI);
    
    ov_uf_bins_Clear(Jpsipi_2d_data);
    ov_uf_bins_Clear(Jpsipi_2d_BD_to_JPSI_K_PI);
    ov_uf_bins_Clear(Jpsipi_2d_BD_to_JPSI_PI_PI);
    ov_uf_bins_Clear(Jpsipi_2d_BS_to_JPSI_K_K);
    ov_uf_bins_Clear(Jpsipi_2d_BS_to_JPSI_PI_PI);
    ov_uf_bins_Clear(Jpsipi_2d_LAMBDA0B_to_JPSI_P_K);
    ov_uf_bins_Clear(Jpsipi_2d_bg);
    ov_uf_bins_Clear(Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI);
    ov_uf_bins_Clear(Jpsipi_2d_BD_to_JPSI_K_K);
    ov_uf_bins_Clear(Jpsipi_2d_BS_to_JPSI_K_PI);
    
    ov_uf_bins_Clear(Kpi_2d_data);
    ov_uf_bins_Clear(Kpi_2d_BD_to_JPSI_K_PI);
    ov_uf_bins_Clear(Kpi_2d_BD_to_JPSI_PI_PI);
    ov_uf_bins_Clear(Kpi_2d_BS_to_JPSI_K_K);
    ov_uf_bins_Clear(Kpi_2d_BS_to_JPSI_PI_PI);
    ov_uf_bins_Clear(Kpi_2d_LAMBDA0B_to_JPSI_P_K);
    ov_uf_bins_Clear(Kpi_2d_bg);
    ov_uf_bins_Clear(Kpi_2d_LAMBDA0B_to_JPSI_P_PI);
    ov_uf_bins_Clear(Kpi_2d_BD_to_JPSI_K_K);
    ov_uf_bins_Clear(Kpi_2d_BS_to_JPSI_K_PI);
    
    ov_uf_bins_Clear(JpsiK_2d_data);
    ov_uf_bins_Clear(JpsiK_2d_BD_to_JPSI_K_PI);
    ov_uf_bins_Clear(JpsiK_2d_BD_to_JPSI_PI_PI);
    ov_uf_bins_Clear(JpsiK_2d_BS_to_JPSI_K_K);
    ov_uf_bins_Clear(JpsiK_2d_BS_to_JPSI_PI_PI);
    ov_uf_bins_Clear(JpsiK_2d_LAMBDA0B_to_JPSI_P_K);
    ov_uf_bins_Clear(JpsiK_2d_bg);
    ov_uf_bins_Clear(JpsiK_2d_LAMBDA0B_to_JPSI_P_PI);
    ov_uf_bins_Clear(JpsiK_2d_BD_to_JPSI_K_K);
    ov_uf_bins_Clear(JpsiK_2d_BS_to_JPSI_K_PI);
    
    ov_uf_bins_Clear(cos_theta_Zc_data);
    ov_uf_bins_Clear(cos_theta_Zc_BD_to_JPSI_K_PI);
    ov_uf_bins_Clear(cos_theta_Zc_BD_to_JPSI_PI_PI);
    ov_uf_bins_Clear(cos_theta_Zc_BS_to_JPSI_K_K);
    ov_uf_bins_Clear(cos_theta_Zc_BS_to_JPSI_PI_PI);
    ov_uf_bins_Clear(cos_theta_Zc_LAMBDA0B_to_JPSI_P_K);
    ov_uf_bins_Clear(cos_theta_Zc_bg);
    ov_uf_bins_Clear(cos_theta_Zc_LAMBDA0B_to_JPSI_P_PI);
    ov_uf_bins_Clear(cos_theta_Zc_BD_to_JPSI_K_K);
    ov_uf_bins_Clear(cos_theta_Zc_BS_to_JPSI_K_PI);
    
    ov_uf_bins_Clear(cos_theta_psi_X_data);
    ov_uf_bins_Clear(cos_theta_psi_X_BD_to_JPSI_K_PI);
    ov_uf_bins_Clear(cos_theta_psi_X_BD_to_JPSI_PI_PI);
    ov_uf_bins_Clear(cos_theta_psi_X_BS_to_JPSI_K_K);
    ov_uf_bins_Clear(cos_theta_psi_X_BS_to_JPSI_PI_PI);
    ov_uf_bins_Clear(cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K);
    ov_uf_bins_Clear(cos_theta_psi_X_bg);
    ov_uf_bins_Clear(cos_theta_psi_X_LAMBDA0B_to_JPSI_P_PI);
    ov_uf_bins_Clear(cos_theta_psi_X_BD_to_JPSI_K_K);
    ov_uf_bins_Clear(cos_theta_psi_X_BS_to_JPSI_K_PI);
    
    ov_uf_bins_Clear(phi_mu_X_data);
    ov_uf_bins_Clear(phi_mu_X_BD_to_JPSI_K_PI);
    ov_uf_bins_Clear(phi_mu_X_BD_to_JPSI_PI_PI);
    ov_uf_bins_Clear(phi_mu_X_BS_to_JPSI_K_K);
    ov_uf_bins_Clear(phi_mu_X_BS_to_JPSI_PI_PI);
    ov_uf_bins_Clear(phi_mu_X_LAMBDA0B_to_JPSI_P_K);
    ov_uf_bins_Clear(phi_mu_X_bg);
    ov_uf_bins_Clear(phi_mu_X_LAMBDA0B_to_JPSI_P_PI);
    ov_uf_bins_Clear(phi_mu_X_BD_to_JPSI_K_K);
    ov_uf_bins_Clear(phi_mu_X_BS_to_JPSI_K_PI);
    
    ov_uf_bins_Clear(cos_theta_B0_data);
    ov_uf_bins_Clear(cos_theta_B0_BD_to_JPSI_K_PI);
    ov_uf_bins_Clear(cos_theta_B0_BD_to_JPSI_PI_PI);
    ov_uf_bins_Clear(cos_theta_B0_BS_to_JPSI_K_K);
    ov_uf_bins_Clear(cos_theta_B0_BS_to_JPSI_PI_PI);
    ov_uf_bins_Clear(cos_theta_B0_LAMBDA0B_to_JPSI_P_K);
    ov_uf_bins_Clear(cos_theta_B0_bg);
    ov_uf_bins_Clear(cos_theta_B0_LAMBDA0B_to_JPSI_P_PI);
    ov_uf_bins_Clear(cos_theta_B0_BD_to_JPSI_K_K);
    ov_uf_bins_Clear(cos_theta_B0_BS_to_JPSI_K_PI);
    
    ov_uf_bins_Clear(phi_psi_data);
    ov_uf_bins_Clear(phi_psi_BD_to_JPSI_K_PI);
    ov_uf_bins_Clear(phi_psi_BD_to_JPSI_PI_PI);
    ov_uf_bins_Clear(phi_psi_BS_to_JPSI_K_K);
    ov_uf_bins_Clear(phi_psi_BS_to_JPSI_PI_PI);
    ov_uf_bins_Clear(phi_psi_LAMBDA0B_to_JPSI_P_K);
    ov_uf_bins_Clear(phi_psi_bg);
    ov_uf_bins_Clear(phi_psi_LAMBDA0B_to_JPSI_P_PI);
    ov_uf_bins_Clear(phi_psi_BD_to_JPSI_K_K);
    ov_uf_bins_Clear(phi_psi_BS_to_JPSI_K_PI);
    
    ov_uf_bins_Clear(JpsiK_2d_control_data);
    ov_uf_bins_Clear(JpsiK_2d_control_BD_to_JPSI_K_PI);
    ov_uf_bins_Clear(JpsiK_2d_control_BD_to_JPSI_PI_PI);
    ov_uf_bins_Clear(JpsiK_2d_control_BS_to_JPSI_K_K);
    ov_uf_bins_Clear(JpsiK_2d_control_BS_to_JPSI_PI_PI);
    ov_uf_bins_Clear(JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K);
    ov_uf_bins_Clear(JpsiK_2d_control_bg);
    ov_uf_bins_Clear(JpsiK_2d_LAMBDA0B_to_JPSI_P_PI);
    ov_uf_bins_Clear(JpsiK_2d_BD_to_JPSI_K_K);
    ov_uf_bins_Clear(JpsiK_2d_BS_to_JPSI_K_PI);
    
    ov_uf_bins_Clear(Jpsip_2d_controlLb_data);
    ov_uf_bins_Clear(Jpsip_2d_controlLb_BD_to_JPSI_K_PI);
    ov_uf_bins_Clear(Jpsip_2d_controlLb_BD_to_JPSI_PI_PI);
    ov_uf_bins_Clear(Jpsip_2d_controlLb_BS_to_JPSI_K_K);
    ov_uf_bins_Clear(Jpsip_2d_controlLb_BS_to_JPSI_PI_PI);
    ov_uf_bins_Clear(Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K);
    ov_uf_bins_Clear(Jpsip_2d_controlLb_bg);
    ov_uf_bins_Clear(Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_PI);
    ov_uf_bins_Clear(Jpsip_2d_controlLb_BD_to_JPSI_K_K);
    ov_uf_bins_Clear(Jpsip_2d_controlLb_BS_to_JPSI_K_PI);
    
    ov_uf_bins_Clear(pK_2d_controlLb_data);
    ov_uf_bins_Clear(pK_2d_controlLb_BD_to_JPSI_K_PI);
    ov_uf_bins_Clear(pK_2d_controlLb_BD_to_JPSI_PI_PI);
    ov_uf_bins_Clear(pK_2d_controlLb_BS_to_JPSI_K_K);
    ov_uf_bins_Clear(pK_2d_controlLb_BS_to_JPSI_PI_PI);
    ov_uf_bins_Clear(pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K);
    ov_uf_bins_Clear(pK_2d_controlLb_bg);
    ov_uf_bins_Clear(pK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI);
    ov_uf_bins_Clear(pK_2d_controlLb_BD_to_JPSI_K_K);
    ov_uf_bins_Clear(pK_2d_controlLb_BS_to_JPSI_K_PI);
    
    ov_uf_bins_Clear(JpsiK_2d_controlLb_data);
    ov_uf_bins_Clear(JpsiK_2d_controlLb_BD_to_JPSI_K_PI);
    ov_uf_bins_Clear(JpsiK_2d_controlLb_BD_to_JPSI_PI_PI);
    ov_uf_bins_Clear(JpsiK_2d_controlLb_BS_to_JPSI_K_K);
    ov_uf_bins_Clear(JpsiK_2d_controlLb_BS_to_JPSI_PI_PI);
    ov_uf_bins_Clear(JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K);
    ov_uf_bins_Clear(JpsiK_2d_controlLb_bg);
    ov_uf_bins_Clear(JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI);
    ov_uf_bins_Clear(JpsiK_2d_controlLb_BD_to_JPSI_K_K);
    ov_uf_bins_Clear(JpsiK_2d_controlLb_BS_to_JPSI_K_PI);
    
    coef_4_to_s_BD_to_JPSI_K_PI = coef_4_to_s_BD_to_JPSI_K_PI / Kpi_piK_2d_BD_to_JPSI_K_PI->Integral();
    coef_4_to_c_BD_to_JPSI_K_PI = coef_4_to_c_BD_to_JPSI_K_PI / KK_1d_BD_to_JPSI_K_PI->Integral();
    coef_4_to_clb_BD_to_JPSI_K_PI = coef_4_to_clb_BD_to_JPSI_K_PI / pK_Kp_2d_BD_to_JPSI_K_PI->Integral();
    
    coef_4_to_s_BS_to_JPSI_K_K = coef_4_to_s_BS_to_JPSI_K_K / Kpi_piK_2d_BS_to_JPSI_K_K->Integral();
    coef_4_to_c_BS_to_JPSI_K_K = coef_4_to_c_BS_to_JPSI_K_K / KK_1d_BS_to_JPSI_K_K->Integral();
    coef_4_to_clb_BS_to_JPSI_K_K = coef_4_to_clb_BS_to_JPSI_K_K / pK_Kp_2d_BS_to_JPSI_K_K->Integral();
    
    coef_4_to_s_LAMBDA0B_to_JPSI_P_K = coef_4_to_s_LAMBDA0B_to_JPSI_P_K / Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Integral();
    coef_4_to_c_LAMBDA0B_to_JPSI_P_K = coef_4_to_c_LAMBDA0B_to_JPSI_P_K / KK_1d_LAMBDA0B_to_JPSI_P_K->Integral();
    coef_4_to_clb_LAMBDA0B_to_JPSI_P_K = coef_4_to_clb_LAMBDA0B_to_JPSI_P_K / pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->Integral();
    
    n_BD_to_JPSI_K_PI = coef_4_to_s_BD_to_JPSI_K_PI * n_BD_KPI;
    n_BS_to_JPSI_K_K = coef_4_to_s_BS_to_JPSI_K_K * n_BS_KK;
    n_BS_to_JPSI_PI_PI = coef_4_to_s_BS_to_JPSI_PI_PI * n_BS_PIPI;
    n_BD_to_JPSI_PI_PI = coef_4_to_s_BD_to_JPSI_PI_PI * n_BD_PIPI;
    n_LAMBDA0B_to_JPSI_P_K = coef_4_to_s_LAMBDA0B_to_JPSI_P_K * n_lambda;
    n_LAMBDA0B_to_JPSI_P_PI = coef_4_to_s_LAMBDA0B_to_JPSI_P_PI * n_LB_PPI;
    n_BD_to_JPSI_K_K = coef_4_to_s_BD_to_JPSI_K_K * n_BD_KK;
    n_BS_to_JPSI_K_PI = coef_4_to_s_BS_to_JPSI_K_PI * n_BS_KPI;
    
    n_control_BD_to_JPSI_K_PI = coef_4_to_c_BD_to_JPSI_K_PI * n_BD_KPI;
    n_control_BS_to_JPSI_K_K = coef_4_to_c_BS_to_JPSI_K_K * n_BS_KK;
    n_control_BS_to_JPSI_PI_PI = coef_4_to_c_BS_to_JPSI_PI_PI * n_BS_PIPI;
    n_control_BD_to_JPSI_PI_PI = coef_4_to_c_BD_to_JPSI_PI_PI * n_BD_PIPI;
    n_control_LAMBDA0B_to_JPSI_P_K = coef_4_to_c_LAMBDA0B_to_JPSI_P_K * n_lambda; 
    n_control_LAMBDA0B_to_JPSI_P_PI = coef_4_to_c_LAMBDA0B_to_JPSI_P_PI * n_LB_PPI;
    n_control_BD_to_JPSI_K_K = coef_4_to_c_BD_to_JPSI_K_K * n_BD_KK;
    n_control_BS_to_JPSI_K_PI = coef_4_to_c_BS_to_JPSI_K_PI * n_BS_KPI;
    
    n_controlLb_BD_to_JPSI_K_PI = coef_4_to_clb_BD_to_JPSI_K_PI * n_BD_KPI;
    n_controlLb_BS_to_JPSI_K_K = coef_4_to_clb_BS_to_JPSI_K_K * n_BS_KK;
    n_controlLb_BS_to_JPSI_PI_PI = coef_4_to_clb_BS_to_JPSI_PI_PI * n_BS_PIPI;
    n_controlLb_BD_to_JPSI_PI_PI = coef_4_to_clb_BD_to_JPSI_PI_PI * n_BD_PIPI;
    n_controlLb_LAMBDA0B_to_JPSI_P_K = coef_4_to_clb_LAMBDA0B_to_JPSI_P_K * n_lambda;
    n_controlLb_LAMBDA0B_to_JPSI_P_PI = coef_4_to_clb_LAMBDA0B_to_JPSI_P_PI * n_LB_PPI;
    n_controlLb_BD_to_JPSI_K_K = coef_4_to_clb_BD_to_JPSI_K_K * n_BD_KK;
    n_controlLb_BS_to_JPSI_K_PI = coef_4_to_clb_BS_to_JPSI_K_PI * n_BS_KPI;
    
    n_comb_bg = 0.;
    n_control_comb_bg = 0.;
    n_controlLb_comb_bg = 0.;
    
    float st = 1. / sqrt(2.);
    
    Double_t xx, yy, x, y, xmin_Kpi = 4.9, xmin_piK = 4.65, xmin_KK = 4.97, xmin_pipi = 4.55, xmin_pK = 5.2, xmin_Kp = 5.15, xmin_pK_rotate = 7.4;
    
    for(int i = 1; i <= Kpi_piK_2d_bg->ProjectionX()->GetNbinsX(); i++)
    {
        for(int j = 1; j <= Kpi_piK_2d_bg->ProjectionY()->GetNbinsX(); j++)
        {
            
            double xx0 = Kpi_piK_2d_data->ProjectionX()->GetBinCenter(i); 
            double yy0 = Kpi_piK_2d_data->ProjectionY()->GetBinCenter(j);
            double x = st * xx0 + st * yy0 - 6.6;
            double y = -st * xx0 + st * yy0;
            Kpi_piK_2d_bg->SetBinContent(i, j, exp(p0 + x * p1 + pow(x, 2.) * p2 + pow(x, 3.) * p3) * exp(-1. * pow((y + a0) / (b0 + x * b1 + pow(x, 2.) * b2), 2.)));            
            if (sa_a[i-1][j-1] == 1)
            {
                n_comb_bg += Kpi_piK_2d_bg->GetBinContent(i, j);
            }
        }
    }
    Double_t Kpi_piK_norm_bg = comb_bg_norm / Kpi_piK_2d_bg->Integral();
    n_comb_bg = n_comb_bg * Kpi_piK_norm_bg;
    Kpi_piK_2d_bg->Scale(Kpi_piK_norm_bg);
    
    for(int i = 1; i <= pK_Kp_2d_bg->ProjectionX()->GetNbinsX(); i++)
    {
        for(int j = 1; j <= pK_Kp_2d_bg->ProjectionY()->GetNbinsX(); j++)
        {
            
            double xx0 = pK_Kp_2d_data->ProjectionX()->GetBinCenter(i); 
            double yy0 = pK_Kp_2d_data->ProjectionY()->GetBinCenter(j);
            double x = st * xx0 + st * yy0 - u0_pKKp;
            double y = -st * xx0 + st * yy0;
            pK_Kp_2d_bg->SetBinContent(i, j, pow(abs(x - x0_pKKp), d0_pKKp) * exp(p0_pKKp + x * p1_pKKp + pow(x, 2.) * p2_pKKp + pow(x, 3.) * p3_pKKp) * exp(-1. * pow((y + a0_pKKp) / (b0_pKKp + x * b1_pKKp + pow(x, 2.) * b2_pKKp), 2.)));
        }
    }
    Double_t pK_Kp_norm_bg = (comb_bg_norm + pK_Kp_2d_data->Integral() - Kpi_piK_2d_data->Integral())  / pK_Kp_2d_bg->Integral();
    pK_Kp_2d_bg->Scale(pK_Kp_norm_bg);
    
    for(int i = 1; i <= ppi_pip_2d_bg->ProjectionX()->GetNbinsX(); i++)
    {
        for(int j = 1; j <= ppi_pip_2d_bg->ProjectionY()->GetNbinsX(); j++)
        {
            
            double xx0 = ppi_pip_2d_data->ProjectionX()->GetBinCenter(i); 
            double yy0 = ppi_pip_2d_data->ProjectionY()->GetBinCenter(j);
            double x = st * xx0 + st * yy0 - u0_ppipip;
            double y = -st * xx0 + st * yy0;
            ppi_pip_2d_bg->SetBinContent(i, j, pow(abs(x - x0_ppipip), d0_ppipip) * exp(p0_ppipip + x * p1_ppipip + pow(x, 2.) * p2_ppipip + pow(x, 3.) * p3_ppipip) * exp(-1. * pow((y + a0_ppipip) / (b0_ppipip + x * b1_ppipip + pow(x, 2.) * b2_ppipip), 2.)));
        }
    }
    Double_t ppi_pip_norm_bg = (comb_bg_norm + ppi_pip_2d_data->Integral() - Kpi_piK_2d_data->Integral())  / ppi_pip_2d_bg->Integral();
    ppi_pip_2d_bg->Scale(ppi_pip_norm_bg);
    
    for(int i = 1; i <= pK_Kp_2d_rotate_bg->ProjectionX()->GetNbinsX(); i++)
    {
        for(int j = 1; j <= pK_Kp_2d_rotate_bg->ProjectionY()->GetNbinsX(); j++)
        {
            double x = pK_Kp_2d_rotate_data->ProjectionX()->GetBinCenter(i) - u0_pKKp; 
            double y = pK_Kp_2d_rotate_data->ProjectionY()->GetBinCenter(j);
            pK_Kp_2d_rotate_bg->SetBinContent(i, j, pow(abs(x - x0_pKKp), d0_pKKp) * exp(p0_pKKp + x * p1_pKKp + pow(x, 2.) * p2_pKKp + pow(x, 3.) * p3_pKKp) * exp(-1. * pow((y + a0_pKKp) / (b0_pKKp + x * b1_pKKp + pow(x, 2.) * b2_pKKp), 2.)));
        }
    }
    Double_t pK_Kp_norm_bg_rotate = (comb_bg_norm + pK_Kp_2d_data->Integral() - Kpi_piK_2d_data->Integral())  / pK_Kp_2d_rotate_bg->Integral();
    pK_Kp_2d_rotate_bg->Scale(pK_Kp_norm_bg_rotate);
    
    for(int i = 1; i <= pipi_1d_bg->GetNbinsX(); i++)
    {
        double xx0 = pipi_1d_data->GetBinCenter(i); 
        double x = xx0 - xmin_pipi;
        pipi_1d_bg->SetBinContent(i, pow(abs(xx0 - x0_pipi), d0_pipi) * exp(p0_pipi + x * p1_pipi + pow(x, 2.) * p2_pipi + pow(x, 3.) * p3_pipi + pow(x, 4.) * p4_pipi + pow(x, 5.) * p5_pipi + pow(x, 6.) * p6_pipi));            
    }
    pipi_1d_bg->Scale(comb_bg_norm / pipi_1d_bg->Integral());
    
    for(int i = 0; i < 50; i++)
    {
        for(int j = 0; j < 51; j++)
        {
            if (calb_a[i][j] == 1)
            {
                double xx0 = pK_Kp_2d_data->ProjectionX()->GetBinCenter(i + 1); 
                double yy0 = pK_Kp_2d_data->ProjectionY()->GetBinCenter(j + 1);
                double x = st * xx0 + st * yy0 - u0_pKKp;
                double y = -st * xx0 + st * yy0;
                n_controlLb_comb_bg += pow(abs(x - x0_pKKp), d0_pKKp) * exp(p0_pKKp + x * p1_pKKp + pow(x, 2.) * p2_pKKp + pow(x, 3.) * p3_pKKp) * exp(-1. * pow((y + a0_pKKp) / (b0_pKKp + x * b1_pKKp + pow(x, 2.) * b2_pKKp), 2.));
            } 
        }
    }
    
    for(int i = 1; i <= KK_1d_data->GetNbinsX(); i++)
    {
        double xx0 = KK_1d_data->GetBinCenter(i); 
        double x = xx0 - xmin_KK;
        double nb = 0.0; double nberr = 0.0; double fb = 0.0;
        KK_1d_bg->SetBinContent(i, pow(abs(xx0 - x0_KK), d0_KK) * exp(p0_KK + x * p1_KK + pow(x, 2.) * p2_KK + pow(x, 3.) * p3_KK + pow(x, 4.) * p4_KK + pow(x, 5.) * p5_KK));
        //cout << i << "\t" << pow(xx0 - x0_KK, d0_KK) * exp(p0_KK + x * p1_KK + pow(x, 2.) * p2_KK + pow(x, 3.) * p3_KK) << endl;
    }
    
    Double_t KK_norm_bg = comb_bg_norm / KK_1d_bg->Integral();
    KK_1d_bg->Scale(KK_norm_bg);
    
    float xmin_KK_o = 4.970;
    
    bg_func->SetParameter(0, x0_KK);
    bg_func->SetParameter(1, d0_KK);
    bg_func->SetParameter(2, p0_KK);
    bg_func->SetParameter(3, p1_KK);
    bg_func->SetParameter(4, p2_KK);
    bg_func->SetParameter(5, p3_KK);
    bg_func->SetParameter(6, p4_KK);
    bg_func->SetParameter(7, p5_KK);
    bg_func->SetParameter(8, xmin_KK_o);
    
    float b_size = KK_1d_bg->GetBinCenter(11) - KK_1d_bg->GetBinCenter(10);
    
    float xx_n = KK_1d_bg->GetBinCenter(20);
    
    float x_n = xx_n - xmin_KK_o;
    float norm = pow(abs(xx_n - x0_KK), d0_KK) * exp(p0_KK + x_n * p1_KK + pow(x_n, 2.) * p2_KK + pow(x_n, 3.) * p3_KK + pow(x_n, 4.) * p4_KK + pow(x_n, 5.) * p5_KK) / bg_func->Integral(xx_n - b_size / 2., xx_n + b_size / 2.);
    
    n_control_comb_bg = bg_func->Integral(5.336, 5.396) * norm;
    n_control_comb_bg = n_control_comb_bg * KK_norm_bg;
    
    float n_tot_test = (n_BD_to_JPSI_K_PI + n_BS_to_JPSI_K_K + n_BS_to_JPSI_PI_PI + n_BD_to_JPSI_PI_PI + n_LAMBDA0B_to_JPSI_P_K + n_comb_bg);
    
    if (RARE_DECAYS)
    {
        n_tot_test += n_LAMBDA0B_to_JPSI_P_PI + n_BD_to_JPSI_K_K + n_BS_to_JPSI_K_PI;
    }
    
    cout << "\nAfter fit statistics:" << endl;
    cout << "\nsignal area:\n" << endl;
    cout << "n_BD_to_JPSI_K_PI\t" << n_BD_to_JPSI_K_PI << "\t" << n_BD_to_JPSI_K_PI / n_tot_test << endl;
    cout << "n_BS_to_JPSI_K_K\t" << n_BS_to_JPSI_K_K << "\t"<< n_BS_to_JPSI_K_K / n_tot_test << endl;
    cout << "n_BS_to_JPSI_PI_PI\t" << n_BS_to_JPSI_PI_PI << "\t"<< n_BS_to_JPSI_PI_PI / n_tot_test << endl;
    cout << "n_BD_to_JPSI_PI_PI\t" << n_BD_to_JPSI_PI_PI << "\t"<< n_BD_to_JPSI_PI_PI / n_tot_test << endl;
    cout << "n_LAMBDA0B_to_JPSI_P_K\t" << n_LAMBDA0B_to_JPSI_P_K << "\t"<< n_LAMBDA0B_to_JPSI_P_K / n_tot_test << endl;
    if (RARE_DECAYS)
    {
        cout << "n_LAMBDA0B_to_JPSI_P_PI\t" << n_LAMBDA0B_to_JPSI_P_PI << "\t"<< n_LAMBDA0B_to_JPSI_P_PI / n_tot_test << endl;
        cout << "n_BD_to_JPSI_K_K\t" << n_BD_to_JPSI_K_K << "\t"<< n_BD_to_JPSI_K_K / n_tot_test << endl;
        cout << "n_BS_to_JPSI_K_PI\t" << n_BS_to_JPSI_K_PI << "\t"<< n_BS_to_JPSI_K_PI / n_tot_test << endl;
    }
    cout << "n_comb_bg\t" << n_comb_bg << "\t"<< n_comb_bg / n_tot_test << endl; 
    cout << "describing:\t" << n_tot_test / Jpsipi_2d_data->Integral() << endl;
    
    float n_tot_test_control = (n_control_BD_to_JPSI_K_PI + n_control_BS_to_JPSI_K_K + n_control_BS_to_JPSI_PI_PI + n_control_BD_to_JPSI_PI_PI + n_control_LAMBDA0B_to_JPSI_P_K + n_control_comb_bg);
    
    if (RARE_DECAYS)
    {
        n_tot_test_control += n_control_LAMBDA0B_to_JPSI_P_PI + n_control_BD_to_JPSI_K_K + n_control_BS_to_JPSI_K_PI;
    }
    
    cout << "\ncontrol area:\n" << endl;
    cout << "n_BD_to_JPSI_K_PI\t" << n_control_BD_to_JPSI_K_PI << endl;
    cout << "n_BS_to_JPSI_K_K\t" << n_control_BS_to_JPSI_K_K << endl;
    cout << "n_BS_to_JPSI_PI_PI\t" << n_control_BS_to_JPSI_PI_PI << endl;
    cout << "n_BD_to_JPSI_PI_PI\t" << n_control_BD_to_JPSI_PI_PI << endl;
    cout << "n_LAMBDA0B_to_JPSI_P_K\t" << n_control_LAMBDA0B_to_JPSI_P_K << endl;
    if (RARE_DECAYS)
    {
        cout << "n_LAMBDA0B_to_JPSI_P_PI\t" << n_control_LAMBDA0B_to_JPSI_P_PI << endl;
        cout << "n_BD_to_JPSI_K_K\t" << n_control_BD_to_JPSI_K_K << endl;
        cout << "n_BS_to_JPSI_K_PI\t" << n_control_BS_to_JPSI_K_PI << endl;
    }
    cout << "n_comb_bg\t" << n_control_comb_bg << endl; 
    cout << "describing:\t" << n_tot_test_control / JpsiK_2d_control_data->Integral() << endl;
    
    float n_tot_test_controlLb = (n_controlLb_BD_to_JPSI_K_PI + n_controlLb_BS_to_JPSI_K_K + n_controlLb_BS_to_JPSI_PI_PI + n_controlLb_BD_to_JPSI_PI_PI + n_controlLb_LAMBDA0B_to_JPSI_P_K + n_controlLb_comb_bg);
    
    if (RARE_DECAYS)
    {
        n_tot_test_controlLb += n_controlLb_LAMBDA0B_to_JPSI_P_PI + n_controlLb_BD_to_JPSI_K_K + n_controlLb_BS_to_JPSI_K_PI;
    }
    
    cout << "\ncontrol area Lb:\n" << endl;
    cout << "n_BD_to_JPSI_K_PI\t" << n_controlLb_BD_to_JPSI_K_PI << endl;
    cout << "n_BS_to_JPSI_K_K\t" << n_controlLb_BS_to_JPSI_K_K << endl;
    cout << "n_BS_to_JPSI_PI_PI\t" << n_controlLb_BS_to_JPSI_PI_PI << endl;
    cout << "n_BD_to_JPSI_PI_PI\t" << n_controlLb_BD_to_JPSI_PI_PI << endl;
    cout << "n_LAMBDA0B_to_JPSI_P_K\t" << n_controlLb_LAMBDA0B_to_JPSI_P_K << endl;
    if (RARE_DECAYS)
    {
        cout << "n_LAMBDA0B_to_JPSI_P_PI\t" << n_controlLb_LAMBDA0B_to_JPSI_P_PI << endl;
        cout << "n_BD_to_JPSI_K_K\t" << n_controlLb_BD_to_JPSI_K_K << endl;
        cout << "n_BS_to_JPSI_K_PI\t" << n_controlLb_BS_to_JPSI_K_PI << endl;
    }
    cout << "n_comb_bg\t" << n_controlLb_comb_bg << endl; 
    cout << "describing:\t" << n_tot_test_controlLb / JpsiK_2d_controlLb_data->Integral() << endl;

    Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->Scale(1. / Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->Integral() * n_LAMBDA0B_to_JPSI_P_PI);
    Kpi_2d_LAMBDA0B_to_JPSI_P_PI->Scale(1. / Kpi_2d_LAMBDA0B_to_JPSI_P_PI->Integral() * n_LAMBDA0B_to_JPSI_P_PI);
    JpsiK_2d_LAMBDA0B_to_JPSI_P_PI->Scale(1. / JpsiK_2d_LAMBDA0B_to_JPSI_P_PI->Integral() * n_LAMBDA0B_to_JPSI_P_PI);
    cos_theta_Zc_LAMBDA0B_to_JPSI_P_PI->Scale(1. / cos_theta_Zc_LAMBDA0B_to_JPSI_P_PI->Integral() * n_LAMBDA0B_to_JPSI_P_PI);
    phi_K_LAMBDA0B_to_JPSI_P_PI->Scale(1. / phi_K_LAMBDA0B_to_JPSI_P_PI->Integral() * n_LAMBDA0B_to_JPSI_P_PI);
    alpha_mu_LAMBDA0B_to_JPSI_P_PI->Scale(1. / alpha_mu_LAMBDA0B_to_JPSI_P_PI->Integral() * n_LAMBDA0B_to_JPSI_P_PI);
    phi_mu_Kstar_LAMBDA0B_to_JPSI_P_PI->Scale(1. / phi_mu_Kstar_LAMBDA0B_to_JPSI_P_PI->Integral() * n_LAMBDA0B_to_JPSI_P_PI);
    phi_mu_X_LAMBDA0B_to_JPSI_P_PI->Scale(1. / phi_mu_X_LAMBDA0B_to_JPSI_P_PI->Integral() * n_LAMBDA0B_to_JPSI_P_PI);
    cos_theta_psi_X_LAMBDA0B_to_JPSI_P_PI->Scale(1. / cos_theta_psi_X_LAMBDA0B_to_JPSI_P_PI->Integral() * n_LAMBDA0B_to_JPSI_P_PI);
    cos_theta_Kstar_LAMBDA0B_to_JPSI_P_PI->Scale(1. / cos_theta_Kstar_LAMBDA0B_to_JPSI_P_PI->Integral() * n_LAMBDA0B_to_JPSI_P_PI);
    cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_PI->Scale(1. / cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_PI->Integral() * n_LAMBDA0B_to_JPSI_P_PI);
    cos_theta_B0_LAMBDA0B_to_JPSI_P_PI->Scale(1. / cos_theta_B0_LAMBDA0B_to_JPSI_P_PI->Integral() * n_LAMBDA0B_to_JPSI_P_PI);
    phi_psi_LAMBDA0B_to_JPSI_P_PI->Scale(1. / phi_psi_LAMBDA0B_to_JPSI_P_PI->Integral() * n_LAMBDA0B_to_JPSI_P_PI);
    
    Jpsipi_2d_BD_to_JPSI_K_K->Scale(1. / Jpsipi_2d_BD_to_JPSI_K_K->Integral() * n_BD_to_JPSI_K_K);
    Kpi_2d_BD_to_JPSI_K_K->Scale(1. / Kpi_2d_BD_to_JPSI_K_K->Integral() * n_BD_to_JPSI_K_K);
    JpsiK_2d_BD_to_JPSI_K_K->Scale(1. / JpsiK_2d_BD_to_JPSI_K_K->Integral() * n_BD_to_JPSI_K_K);
    cos_theta_Zc_BD_to_JPSI_K_K->Scale(1. / cos_theta_Zc_BD_to_JPSI_K_K->Integral() * n_BD_to_JPSI_K_K);
    phi_K_BD_to_JPSI_K_K->Scale(1. / phi_K_BD_to_JPSI_K_K->Integral() * n_BD_to_JPSI_K_K);
    alpha_mu_BD_to_JPSI_K_K->Scale(1. / alpha_mu_BD_to_JPSI_K_K->Integral() * n_BD_to_JPSI_K_K);
    phi_mu_Kstar_BD_to_JPSI_K_K->Scale(1. / phi_mu_Kstar_BD_to_JPSI_K_K->Integral() * n_BD_to_JPSI_K_K);
    phi_mu_X_BD_to_JPSI_K_K->Scale(1. / phi_mu_X_BD_to_JPSI_K_K->Integral() * n_BD_to_JPSI_K_K);
    cos_theta_psi_X_BD_to_JPSI_K_K->Scale(1. / cos_theta_psi_X_BD_to_JPSI_K_K->Integral() * n_BD_to_JPSI_K_K);
    cos_theta_Kstar_BD_to_JPSI_K_K->Scale(1. / cos_theta_Kstar_BD_to_JPSI_K_K->Integral() * n_BD_to_JPSI_K_K);
    cos_theta_psi_Kstar_BD_to_JPSI_K_K->Scale(1. / cos_theta_psi_Kstar_BD_to_JPSI_K_K->Integral() * n_BD_to_JPSI_K_K);
    cos_theta_B0_BD_to_JPSI_K_K->Scale(1. / cos_theta_B0_BD_to_JPSI_K_K->Integral() * n_BD_to_JPSI_K_K);
    phi_psi_BD_to_JPSI_K_K->Scale(1. / phi_psi_BD_to_JPSI_K_K->Integral() * n_BD_to_JPSI_K_K);
    
    Jpsipi_2d_BS_to_JPSI_K_PI->Scale(1. / Jpsipi_2d_BS_to_JPSI_K_PI->Integral() * n_BS_to_JPSI_K_PI);
    Kpi_2d_BS_to_JPSI_K_PI->Scale(1. / Kpi_2d_BS_to_JPSI_K_PI->Integral() * n_BS_to_JPSI_K_PI);
    JpsiK_2d_BS_to_JPSI_K_PI->Scale(1. / JpsiK_2d_BS_to_JPSI_K_PI->Integral() * n_BS_to_JPSI_K_PI);
    cos_theta_Zc_BS_to_JPSI_K_PI->Scale(1. / cos_theta_Zc_BS_to_JPSI_K_PI->Integral() * n_BS_to_JPSI_K_PI);
    phi_K_BS_to_JPSI_K_PI->Scale(1. / phi_K_BS_to_JPSI_K_PI->Integral() * n_BS_to_JPSI_K_PI);
    alpha_mu_BS_to_JPSI_K_PI->Scale(1. / alpha_mu_BS_to_JPSI_K_PI->Integral() * n_BS_to_JPSI_K_PI);
    phi_mu_Kstar_BS_to_JPSI_K_PI->Scale(1. / phi_mu_Kstar_BS_to_JPSI_K_PI->Integral() * n_BS_to_JPSI_K_PI);
    phi_mu_X_BS_to_JPSI_K_PI->Scale(1. / phi_mu_X_BS_to_JPSI_K_PI->Integral() * n_BS_to_JPSI_K_PI);
    cos_theta_psi_X_BS_to_JPSI_K_PI->Scale(1. / cos_theta_psi_X_BS_to_JPSI_K_PI->Integral() * n_BS_to_JPSI_K_PI);
    cos_theta_Kstar_BS_to_JPSI_K_PI->Scale(1. / cos_theta_Kstar_BS_to_JPSI_K_PI->Integral() * n_BS_to_JPSI_K_PI);
    cos_theta_psi_Kstar_BS_to_JPSI_K_PI->Scale(1. / cos_theta_psi_Kstar_BS_to_JPSI_K_PI->Integral() * n_BS_to_JPSI_K_PI);
    cos_theta_B0_BS_to_JPSI_K_PI->Scale(1. / cos_theta_B0_BS_to_JPSI_K_PI->Integral() * n_BS_to_JPSI_K_PI);
    phi_psi_BS_to_JPSI_K_PI->Scale(1. / phi_psi_BS_to_JPSI_K_PI->Integral() * n_BS_to_JPSI_K_PI);
    
    Jpsipi_2d_BS_to_JPSI_PI_PI->Scale(1. / Jpsipi_2d_BS_to_JPSI_PI_PI->Integral() * n_BS_to_JPSI_PI_PI);
    Kpi_2d_BS_to_JPSI_PI_PI->Scale(1. / Kpi_2d_BS_to_JPSI_PI_PI->Integral() * n_BS_to_JPSI_PI_PI);
    JpsiK_2d_BS_to_JPSI_PI_PI->Scale(1. / JpsiK_2d_BS_to_JPSI_PI_PI->Integral() * n_BS_to_JPSI_PI_PI);
    cos_theta_Zc_BS_to_JPSI_PI_PI->Scale(1. / cos_theta_Zc_BS_to_JPSI_PI_PI->Integral() * n_BS_to_JPSI_PI_PI);
    phi_K_BS_to_JPSI_PI_PI->Scale(1. / phi_K_BS_to_JPSI_PI_PI->Integral() * n_BS_to_JPSI_PI_PI);
    alpha_mu_BS_to_JPSI_PI_PI->Scale(1. / alpha_mu_BS_to_JPSI_PI_PI->Integral() * n_BS_to_JPSI_PI_PI);
    phi_mu_Kstar_BS_to_JPSI_PI_PI->Scale(1. / phi_mu_Kstar_BS_to_JPSI_PI_PI->Integral() * n_BS_to_JPSI_PI_PI);
    phi_mu_X_BS_to_JPSI_PI_PI->Scale(1. / phi_mu_X_BS_to_JPSI_PI_PI->Integral() * n_BS_to_JPSI_PI_PI);
    cos_theta_psi_X_BS_to_JPSI_PI_PI->Scale(1. / cos_theta_psi_X_BS_to_JPSI_PI_PI->Integral() * n_BS_to_JPSI_PI_PI);
    cos_theta_Kstar_BS_to_JPSI_PI_PI->Scale(1. / cos_theta_Kstar_BS_to_JPSI_PI_PI->Integral() * n_BS_to_JPSI_PI_PI);
    cos_theta_psi_Kstar_BS_to_JPSI_PI_PI->Scale(1. / cos_theta_psi_Kstar_BS_to_JPSI_PI_PI->Integral() * n_BS_to_JPSI_PI_PI);
    cos_theta_B0_BS_to_JPSI_PI_PI->Scale(1. / cos_theta_B0_BS_to_JPSI_PI_PI->Integral() * n_BS_to_JPSI_PI_PI);
    phi_psi_BS_to_JPSI_PI_PI->Scale(1. / phi_psi_BS_to_JPSI_PI_PI->Integral() * n_BS_to_JPSI_PI_PI);
    
    
    Jpsipi_2d_BD_to_JPSI_PI_PI->Scale(1. / Jpsipi_2d_BD_to_JPSI_PI_PI->Integral() * n_BD_to_JPSI_PI_PI);
    Kpi_2d_BD_to_JPSI_PI_PI->Scale(1. / Kpi_2d_BD_to_JPSI_PI_PI->Integral() * n_BD_to_JPSI_PI_PI);
    JpsiK_2d_BD_to_JPSI_PI_PI->Scale(1. / JpsiK_2d_BD_to_JPSI_PI_PI->Integral() * n_BD_to_JPSI_PI_PI);
    cos_theta_Zc_BD_to_JPSI_PI_PI->Scale(1. / cos_theta_Zc_BD_to_JPSI_PI_PI->Integral() * n_BD_to_JPSI_PI_PI);
    phi_K_BD_to_JPSI_PI_PI->Scale(1. / phi_K_BD_to_JPSI_PI_PI->Integral() * n_BD_to_JPSI_PI_PI);
    alpha_mu_BD_to_JPSI_PI_PI->Scale(1. / alpha_mu_BD_to_JPSI_PI_PI->Integral() * n_BD_to_JPSI_PI_PI);
    phi_mu_Kstar_BD_to_JPSI_PI_PI->Scale(1. / phi_mu_Kstar_BD_to_JPSI_PI_PI->Integral() * n_BD_to_JPSI_PI_PI);
    phi_mu_X_BD_to_JPSI_PI_PI->Scale(1. / phi_mu_X_BD_to_JPSI_PI_PI->Integral() * n_BD_to_JPSI_PI_PI);
    cos_theta_psi_X_BD_to_JPSI_PI_PI->Scale(1. / cos_theta_psi_X_BD_to_JPSI_PI_PI->Integral() * n_BD_to_JPSI_PI_PI);
    cos_theta_Kstar_BD_to_JPSI_PI_PI->Scale(1. / cos_theta_Kstar_BD_to_JPSI_PI_PI->Integral() * n_BD_to_JPSI_PI_PI);
    cos_theta_psi_Kstar_BD_to_JPSI_PI_PI->Scale(1. / cos_theta_psi_Kstar_BD_to_JPSI_PI_PI->Integral() * n_BD_to_JPSI_PI_PI);
    cos_theta_B0_BD_to_JPSI_PI_PI->Scale(1. / cos_theta_B0_BD_to_JPSI_PI_PI->Integral() * n_BD_to_JPSI_PI_PI);
    phi_psi_BD_to_JPSI_PI_PI->Scale(1. / phi_psi_BD_to_JPSI_PI_PI->Integral() * n_BD_to_JPSI_PI_PI);
    
    Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->Scale(1. / Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->Integral() * n_LAMBDA0B_to_JPSI_P_K);
    Kpi_2d_LAMBDA0B_to_JPSI_P_K->Scale(1. / Kpi_2d_LAMBDA0B_to_JPSI_P_K->Integral() * n_LAMBDA0B_to_JPSI_P_K);
    JpsiK_2d_LAMBDA0B_to_JPSI_P_K->Scale(1. / JpsiK_2d_LAMBDA0B_to_JPSI_P_K->Integral() * n_LAMBDA0B_to_JPSI_P_K);
    cos_theta_Zc_LAMBDA0B_to_JPSI_P_K->Scale(1. / cos_theta_Zc_LAMBDA0B_to_JPSI_P_K->Integral() * n_LAMBDA0B_to_JPSI_P_K);
    phi_K_LAMBDA0B_to_JPSI_P_K->Scale(1. / phi_K_LAMBDA0B_to_JPSI_P_K->Integral() * n_LAMBDA0B_to_JPSI_P_K);
    alpha_mu_LAMBDA0B_to_JPSI_P_K->Scale(1. / alpha_mu_LAMBDA0B_to_JPSI_P_K->Integral() * n_LAMBDA0B_to_JPSI_P_K);
    phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K->Scale(1. / phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K->Integral() * n_LAMBDA0B_to_JPSI_P_K);
    phi_mu_X_LAMBDA0B_to_JPSI_P_K->Scale(1. / phi_mu_X_LAMBDA0B_to_JPSI_P_K->Integral() * n_LAMBDA0B_to_JPSI_P_K);
    cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K->Scale(1. / cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K->Integral() * n_LAMBDA0B_to_JPSI_P_K);
    cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->Scale(1. / cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->Integral() * n_LAMBDA0B_to_JPSI_P_K);
    cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K->Scale(1. / cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K->Integral() * n_LAMBDA0B_to_JPSI_P_K);
    cos_theta_B0_LAMBDA0B_to_JPSI_P_K->Scale(1. / cos_theta_B0_LAMBDA0B_to_JPSI_P_K->Integral() * n_LAMBDA0B_to_JPSI_P_K);
    phi_psi_LAMBDA0B_to_JPSI_P_K->Scale(1. / phi_psi_LAMBDA0B_to_JPSI_P_K->Integral() * n_LAMBDA0B_to_JPSI_P_K);
    
    Jpsipi_2d_bg->Scale(1. / Jpsipi_2d_bg->Integral() * n_comb_bg);
    Kpi_2d_bg->Scale(1. / Kpi_2d_bg->Integral() * n_comb_bg);
    JpsiK_2d_bg->Scale(1. / JpsiK_2d_bg->Integral() * n_comb_bg);
    cos_theta_Zc_bg->Scale(1. / cos_theta_Zc_bg->Integral() * n_comb_bg);
    phi_K_bg->Scale(1. / phi_K_bg->Integral() * n_comb_bg);
    alpha_mu_bg->Scale(1. / alpha_mu_bg->Integral() * n_comb_bg);
    phi_mu_Kstar_bg->Scale(1. / phi_mu_Kstar_bg->Integral() * n_comb_bg);
    phi_mu_X_bg->Scale(1. / phi_mu_X_bg->Integral() * n_comb_bg);
    cos_theta_psi_X_bg->Scale(1. / cos_theta_psi_X_bg->Integral() * n_comb_bg);
    cos_theta_Kstar_bg->Scale(1. / cos_theta_Kstar_bg->Integral() * n_comb_bg);
    cos_theta_psi_Kstar_bg->Scale(1. / cos_theta_psi_Kstar_bg->Integral() * n_comb_bg);
    cos_theta_B0_bg->Scale(1. / cos_theta_B0_bg->Integral() * n_comb_bg);
    phi_psi_bg->Scale(1. / phi_psi_bg->Integral() * n_comb_bg);
    
    
    //Kpi_2d_BD_to_JPSI_K_PI->ProjectionX()->Draw();
    
    //Kpi_err_sum->Add(Kpi_err_sum_bd, pow(1. / Kpi_mass_BD_to_JPSI_K_PI->Integral() * n_bd_Kpi, 2.)); // потом сделать pow
	//Jpsipi_err_sum->Add(Jpsipi_err_sum_bd, pow(1. / Jpsipi_mass_BD_to_JPSI_K_PI->Integral() * n_bd_Kpi, 2.));
	//JpsiK_err_sum->Add(JpsiK_err_sum_bd, pow(1. / JpsiK_mass_BD_to_JPSI_K_PI->Integral() * n_bd_Kpi, 2.));
    Kpi_2d_BD_to_JPSI_K_PI->Scale(1. / Kpi_2d_BD_to_JPSI_K_PI->Integral() * n_BD_to_JPSI_K_PI);
	TH2D *Kpi_2d_total = (TH2D*)Kpi_2d_BD_to_JPSI_K_PI->Clone("Kpi_2d_total");
    Jpsipi_2d_BD_to_JPSI_K_PI->Scale(1. / Jpsipi_2d_BD_to_JPSI_K_PI->Integral() * n_BD_to_JPSI_K_PI);
    TH2D *Jpsipi_2d_total = (TH2D*)Jpsipi_2d_BD_to_JPSI_K_PI->Clone("Jpsipi_2d_total");
    JpsiK_2d_BD_to_JPSI_K_PI->Scale(1. / JpsiK_2d_BD_to_JPSI_K_PI->Integral() * n_BD_to_JPSI_K_PI);
    TH2D *JpsiK_2d_total = (TH2D*)JpsiK_2d_BD_to_JPSI_K_PI->Clone("JpsiK_2d_total");
    cos_theta_Zc_BD_to_JPSI_K_PI->Scale(1. / cos_theta_Zc_BD_to_JPSI_K_PI->Integral() * n_BD_to_JPSI_K_PI);
    TH2D *cos_theta_Zc_total = (TH2D*)cos_theta_Zc_BD_to_JPSI_K_PI->Clone("cos_theta_Zc_total");
    phi_K_BD_to_JPSI_K_PI->Scale(1. / phi_K_BD_to_JPSI_K_PI->Integral() * n_BD_to_JPSI_K_PI);
    TH2D *phi_K_total = (TH2D*)phi_K_BD_to_JPSI_K_PI->Clone("phi_K_total");
    alpha_mu_BD_to_JPSI_K_PI->Scale(1. / alpha_mu_BD_to_JPSI_K_PI->Integral() * n_BD_to_JPSI_K_PI);
    TH2D *alpha_mu_total = (TH2D*)alpha_mu_BD_to_JPSI_K_PI->Clone("alpha_mu_total");
    phi_mu_Kstar_BD_to_JPSI_K_PI->Scale(1. / phi_mu_Kstar_BD_to_JPSI_K_PI->Integral() * n_BD_to_JPSI_K_PI);
    TH2D *phi_mu_Kstar_total = (TH2D*)phi_mu_Kstar_BD_to_JPSI_K_PI->Clone("phi_mu_Kstar_total");
    phi_mu_X_BD_to_JPSI_K_PI->Scale(1. / phi_mu_X_BD_to_JPSI_K_PI->Integral() * n_BD_to_JPSI_K_PI);
    TH2D *phi_mu_X_total = (TH2D*)phi_mu_X_BD_to_JPSI_K_PI->Clone("phi_mu_X_total");
    cos_theta_psi_X_BD_to_JPSI_K_PI->Scale(1. / cos_theta_psi_X_BD_to_JPSI_K_PI->Integral() * n_BD_to_JPSI_K_PI);
    TH2D *cos_theta_psi_X_total = (TH2D*)cos_theta_psi_X_BD_to_JPSI_K_PI->Clone("cos_theta_psi_X_total");
    cos_theta_Kstar_BD_to_JPSI_K_PI->Scale(1. / cos_theta_Kstar_BD_to_JPSI_K_PI->Integral() * n_BD_to_JPSI_K_PI);
    TH2D *cos_theta_Kstar_total = (TH2D*)cos_theta_Kstar_BD_to_JPSI_K_PI->Clone("cos_theta_Kstar_total");
    cos_theta_psi_Kstar_BD_to_JPSI_K_PI->Scale(1. / cos_theta_psi_Kstar_BD_to_JPSI_K_PI->Integral() * n_BD_to_JPSI_K_PI);
    TH2D *cos_theta_psi_Kstar_total = (TH2D*)cos_theta_psi_Kstar_BD_to_JPSI_K_PI->Clone("cos_theta_psi_Kstar_total");
    cos_theta_B0_BD_to_JPSI_K_PI->Scale(1. / cos_theta_B0_BD_to_JPSI_K_PI->Integral() * n_BD_to_JPSI_K_PI);
    TH2D *cos_theta_B0_total = (TH2D*)cos_theta_B0_BD_to_JPSI_K_PI->Clone("cos_theta_B0_total");
    phi_psi_BD_to_JPSI_K_PI->Scale(1. / phi_psi_BD_to_JPSI_K_PI->Integral() * n_BD_to_JPSI_K_PI);
    TH2D *phi_psi_total = (TH2D*)phi_psi_BD_to_JPSI_K_PI->Clone("phi_psi_total");
    
    Kpi_2d_BS_to_JPSI_K_K->Scale(1. / Kpi_2d_BS_to_JPSI_K_K->Integral() * n_BS_to_JPSI_K_K);
    Jpsipi_2d_BS_to_JPSI_K_K->Scale(1. / Jpsipi_2d_BS_to_JPSI_K_K->Integral() * n_BS_to_JPSI_K_K);
    JpsiK_2d_BS_to_JPSI_K_K->Scale(1. / JpsiK_2d_BS_to_JPSI_K_K->Integral() * n_BS_to_JPSI_K_K);
    cos_theta_Zc_BS_to_JPSI_K_K->Scale(1. / cos_theta_Zc_BS_to_JPSI_K_K->Integral() * n_BS_to_JPSI_K_K);
    phi_K_BS_to_JPSI_K_K->Scale(1. / phi_K_BS_to_JPSI_K_K->Integral() * n_BS_to_JPSI_K_K);
    alpha_mu_BS_to_JPSI_K_K->Scale(1. / alpha_mu_BS_to_JPSI_K_K->Integral() * n_BS_to_JPSI_K_K);
    phi_mu_Kstar_BS_to_JPSI_K_K->Scale(1. / phi_mu_Kstar_BS_to_JPSI_K_K->Integral() * n_BS_to_JPSI_K_K);
    phi_mu_X_BS_to_JPSI_K_K->Scale(1. / phi_mu_X_BS_to_JPSI_K_K->Integral() * n_BS_to_JPSI_K_K);
    cos_theta_psi_X_BS_to_JPSI_K_K->Scale(1. / cos_theta_psi_X_BS_to_JPSI_K_K->Integral() * n_BS_to_JPSI_K_K);
    cos_theta_Kstar_BS_to_JPSI_K_K->Scale(1. / cos_theta_Kstar_BS_to_JPSI_K_K->Integral() * n_BS_to_JPSI_K_K);
    cos_theta_psi_Kstar_BS_to_JPSI_K_K->Scale(1. / cos_theta_psi_Kstar_BS_to_JPSI_K_K->Integral() * n_BS_to_JPSI_K_K);
    cos_theta_B0_BS_to_JPSI_K_K->Scale(1. / cos_theta_B0_BS_to_JPSI_K_K->Integral() * n_BS_to_JPSI_K_K);
    phi_psi_BS_to_JPSI_K_K->Scale(1. / phi_psi_BS_to_JPSI_K_K->Integral() * n_BS_to_JPSI_K_K);
    
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__||control_area_BS_to_JPSI_K_K_histograms||__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__// 
    
    JpsiK_2d_control_BS_to_JPSI_K_K->Scale(1. / JpsiK_2d_control_BS_to_JPSI_K_K->Integral() * n_control_BS_to_JPSI_K_K);
    TH2D *JpsiK_2d_control_total = (TH2D*)JpsiK_2d_control_BS_to_JPSI_K_K->Clone("JpsiK_2d_control_total");
    KK_1d_control_BS_to_JPSI_K_K->Scale(1. / KK_1d_control_BS_to_JPSI_K_K->Integral() * n_control_BS_to_JPSI_K_K);
    TH1D *KK_1d_control_total = (TH1D*)KK_1d_control_BS_to_JPSI_K_K->Clone("KK_1d_control_total");

    JpsiK_2d_control_LAMBDA0B_to_JPSI_P_PI->Scale(1. / JpsiK_2d_control_LAMBDA0B_to_JPSI_P_PI->Integral() * n_control_LAMBDA0B_to_JPSI_P_PI);
    KK_1d_control_LAMBDA0B_to_JPSI_P_PI->Scale(1. / KK_1d_control_LAMBDA0B_to_JPSI_P_PI->Integral() * n_control_LAMBDA0B_to_JPSI_P_PI);
    
    JpsiK_2d_control_BD_to_JPSI_K_K->Scale(1. / JpsiK_2d_control_BD_to_JPSI_K_K->Integral() * n_control_BD_to_JPSI_K_K);
    KK_1d_control_BD_to_JPSI_K_K->Scale(1. / KK_1d_control_BD_to_JPSI_K_K->Integral() * n_control_BD_to_JPSI_K_K);
    
    JpsiK_2d_control_BS_to_JPSI_K_PI->Scale(1. / JpsiK_2d_control_BS_to_JPSI_K_PI->Integral() * n_control_BS_to_JPSI_K_PI);
    KK_1d_control_BS_to_JPSI_K_PI->Scale(1. / KK_1d_control_BS_to_JPSI_K_PI->Integral() * n_control_BS_to_JPSI_K_PI);
    
    JpsiK_2d_control_BD_to_JPSI_K_PI->Scale(1. / JpsiK_2d_control_BD_to_JPSI_K_PI->Integral() * n_control_BD_to_JPSI_K_PI);
    KK_1d_control_BD_to_JPSI_K_PI->Scale(1. / KK_1d_control_BD_to_JPSI_K_PI->Integral() * n_control_BD_to_JPSI_K_PI);
    
    JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K->Scale(1. / JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K->Integral() * n_control_LAMBDA0B_to_JPSI_P_K);
    KK_1d_control_LAMBDA0B_to_JPSI_P_K->Scale(1. / KK_1d_control_LAMBDA0B_to_JPSI_P_K->Integral() * n_control_LAMBDA0B_to_JPSI_P_K);
    
    JpsiK_2d_control_BD_to_JPSI_PI_PI->Scale(1. / JpsiK_2d_control_BD_to_JPSI_PI_PI->Integral() * n_control_BD_to_JPSI_PI_PI);
    KK_1d_control_BD_to_JPSI_PI_PI->Scale(1. / KK_1d_control_BD_to_JPSI_PI_PI->Integral() * n_control_BD_to_JPSI_PI_PI);
    
    JpsiK_2d_control_BS_to_JPSI_PI_PI->Scale(1. / JpsiK_2d_control_BS_to_JPSI_PI_PI->Integral() * n_control_BS_to_JPSI_PI_PI);
    KK_1d_control_BS_to_JPSI_PI_PI->Scale(1. / KK_1d_control_BS_to_JPSI_PI_PI->Integral() * n_control_BS_to_JPSI_PI_PI);
    
    JpsiK_2d_control_bg->Scale(1. / JpsiK_2d_control_bg->Integral() * n_control_comb_bg);
    KK_1d_control_bg->Scale(1. / KK_1d_control_bg->Integral() * n_control_comb_bg);
    
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__||control_area_LAMBDA0B_to_JPSI_P_K_histograms||__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
    
    pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Scale(1. / pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Integral() * n_controlLb_LAMBDA0B_to_JPSI_P_K);
	TH2D *pK_2d_controlLb_total = (TH2D*)pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Clone("pK_2d_controlLb_total");
    Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Scale(1. / Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Integral() * n_controlLb_LAMBDA0B_to_JPSI_P_K);
    TH2D *Jpsip_2d_controlLb_total = (TH2D*)Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Clone("Jpsip_2d_controlLb_total");
    JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Scale(1. / JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Integral() * n_controlLb_LAMBDA0B_to_JPSI_P_K);
    TH2D *JpsiK_2d_controlLb_total = (TH2D*)JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Clone("JpsiK_2d_controlLb_total");
    
    pK_2d_controlLb_BD_to_JPSI_K_PI->Scale(1. / pK_2d_controlLb_BD_to_JPSI_K_PI->Integral() * n_controlLb_BD_to_JPSI_K_PI);
    Jpsip_2d_controlLb_BD_to_JPSI_K_PI->Scale(1. / Jpsip_2d_controlLb_BD_to_JPSI_K_PI->Integral() * n_controlLb_BD_to_JPSI_K_PI);
    JpsiK_2d_controlLb_BD_to_JPSI_K_PI->Scale(1. / JpsiK_2d_controlLb_BD_to_JPSI_K_PI->Integral() * n_controlLb_BD_to_JPSI_K_PI);
    
    pK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->Scale(1. / pK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->Integral() * n_controlLb_LAMBDA0B_to_JPSI_P_PI);
    Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->Scale(1. / Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->Integral() * n_controlLb_LAMBDA0B_to_JPSI_P_PI);
    JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->Scale(1. / JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->Integral() * n_controlLb_LAMBDA0B_to_JPSI_P_PI);
    
    pK_2d_controlLb_BD_to_JPSI_K_K->Scale(1. / pK_2d_controlLb_BD_to_JPSI_K_K->Integral() * n_controlLb_BD_to_JPSI_K_K);
    Jpsip_2d_controlLb_BD_to_JPSI_K_K->Scale(1. / Jpsip_2d_controlLb_BD_to_JPSI_K_K->Integral() * n_controlLb_BD_to_JPSI_K_K);
    JpsiK_2d_controlLb_BD_to_JPSI_K_K->Scale(1. / JpsiK_2d_controlLb_BD_to_JPSI_K_K->Integral() * n_controlLb_BD_to_JPSI_K_K);
    
    pK_2d_controlLb_BS_to_JPSI_K_PI->Scale(1. / pK_2d_controlLb_BS_to_JPSI_K_PI->Integral() * n_controlLb_BS_to_JPSI_K_PI);
    Jpsip_2d_controlLb_BS_to_JPSI_K_PI->Scale(1. / Jpsip_2d_controlLb_BS_to_JPSI_K_PI->Integral() * n_controlLb_BS_to_JPSI_K_PI);
    JpsiK_2d_controlLb_BS_to_JPSI_K_PI->Scale(1. / JpsiK_2d_controlLb_BS_to_JPSI_K_PI->Integral() * n_controlLb_BS_to_JPSI_K_PI);
    
    pK_2d_controlLb_BS_to_JPSI_K_K->Scale(1. / pK_2d_controlLb_BS_to_JPSI_K_K->Integral() * n_controlLb_BS_to_JPSI_K_K);
    Jpsip_2d_controlLb_BS_to_JPSI_K_K->Scale(1. / Jpsip_2d_controlLb_BS_to_JPSI_K_K->Integral() * n_controlLb_BS_to_JPSI_K_K);
    JpsiK_2d_controlLb_BS_to_JPSI_K_K->Scale(1. / JpsiK_2d_controlLb_BS_to_JPSI_K_K->Integral() * n_controlLb_BS_to_JPSI_K_K);
    
    pK_2d_controlLb_BD_to_JPSI_PI_PI->Scale(1. / pK_2d_controlLb_BD_to_JPSI_PI_PI->Integral() * n_controlLb_BD_to_JPSI_PI_PI);
    Jpsip_2d_controlLb_BD_to_JPSI_PI_PI->Scale(1. / Jpsip_2d_controlLb_BD_to_JPSI_PI_PI->Integral() * n_controlLb_BD_to_JPSI_PI_PI);
    JpsiK_2d_controlLb_BD_to_JPSI_PI_PI->Scale(1. / JpsiK_2d_controlLb_BD_to_JPSI_PI_PI->Integral() * n_controlLb_BD_to_JPSI_PI_PI);
    
    pK_2d_controlLb_BS_to_JPSI_PI_PI->Scale(1. / pK_2d_controlLb_BS_to_JPSI_PI_PI->Integral() * n_controlLb_BS_to_JPSI_PI_PI);
    Jpsip_2d_controlLb_BS_to_JPSI_PI_PI->Scale(1. / Jpsip_2d_controlLb_BS_to_JPSI_PI_PI->Integral() * n_controlLb_BS_to_JPSI_PI_PI);
    JpsiK_2d_controlLb_BS_to_JPSI_PI_PI->Scale(1. / JpsiK_2d_controlLb_BS_to_JPSI_PI_PI->Integral() * n_controlLb_BS_to_JPSI_PI_PI);
    
    pK_2d_controlLb_bg->Scale(1. / pK_2d_controlLb_bg->Integral() * n_controlLb_comb_bg);
    Jpsip_2d_controlLb_bg->Scale(1. / Jpsip_2d_controlLb_bg->Integral() * n_controlLb_comb_bg);
    JpsiK_2d_controlLb_bg->Scale(1. / JpsiK_2d_controlLb_bg->Integral() * n_controlLb_comb_bg);
    
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__||signal_area_pictures||__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
    
    std::string pictures_path_cpp, pictures_path_png;
    
    Jpsipi_2d_bg->SetLineColor(1);
    Jpsipi_2d_bg->SetFillColor(42);
    Jpsipi_2d_BS_to_JPSI_K_K->SetFillColor(30);
    Jpsipi_2d_BD_to_JPSI_K_PI->SetFillColor(7);
    Jpsipi_2d_BD_to_JPSI_PI_PI->SetFillColor(8);
    Jpsipi_2d_BS_to_JPSI_PI_PI->SetFillColor(46);
    Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->SetFillColor(95);
    Jpsipi_2d_BD_to_JPSI_K_K->SetFillColor(12);
    Jpsipi_2d_BS_to_JPSI_K_PI->SetFillColor(6);
    Jpsipi_2d_data->SetMarkerStyle(kFullCircle);
    Jpsipi_2d_data->SetMarkerSize(0.7);
    
    JpsiK_2d_bg->SetLineColor(1);
    JpsiK_2d_bg->SetFillColor(42);
    JpsiK_2d_BS_to_JPSI_K_K->SetFillColor(30);
    JpsiK_2d_BD_to_JPSI_K_PI->SetFillColor(7);
    JpsiK_2d_BD_to_JPSI_PI_PI->SetFillColor(8);
    JpsiK_2d_BS_to_JPSI_PI_PI->SetFillColor(46);
    JpsiK_2d_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    JpsiK_2d_LAMBDA0B_to_JPSI_P_PI->SetFillColor(95);
    JpsiK_2d_BD_to_JPSI_K_K->SetFillColor(12);
    JpsiK_2d_BS_to_JPSI_K_PI->SetFillColor(6);
    JpsiK_2d_data->SetMarkerStyle(kFullCircle);
    JpsiK_2d_data->SetMarkerSize(0.7);
    
    Kpi_2d_bg->SetLineColor(1);
    Kpi_2d_bg->SetFillColor(42);
    Kpi_2d_BS_to_JPSI_K_K->SetFillColor(30);
    Kpi_2d_BD_to_JPSI_K_PI->SetFillColor(7);
    Kpi_2d_BD_to_JPSI_PI_PI->SetFillColor(8);
    Kpi_2d_BS_to_JPSI_PI_PI->SetFillColor(46);
    Kpi_2d_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    Kpi_2d_LAMBDA0B_to_JPSI_P_PI->SetFillColor(95);
    Kpi_2d_BD_to_JPSI_K_K->SetFillColor(12);
    Kpi_2d_BS_to_JPSI_K_PI->SetFillColor(6);
    Kpi_2d_data->SetMarkerStyle(kFullCircle);
    Kpi_2d_data->SetMarkerSize(0.7);
    
    TLegend *legend_Jpsipi_X = new TLegend(.60,.60,.90,.90);
    legend_Jpsipi_X->AddEntry(Jpsipi_2d_data,"data");
	legend_Jpsipi_X->AddEntry(Jpsipi_2d_BD_to_JPSI_K_PI,"B_{d} -> J/#psi K #pi");
	legend_Jpsipi_X->AddEntry(Jpsipi_2d_LAMBDA0B_to_JPSI_P_K,"#Lambda_{b} -> J/#psi p K");
	legend_Jpsipi_X->AddEntry(Jpsipi_2d_BS_to_JPSI_K_K,"B_{s} -> J/#psi K K");
	legend_Jpsipi_X->AddEntry(Jpsipi_2d_BS_to_JPSI_PI_PI,"B_{s} -> J/#psi #pi #pi");
	legend_Jpsipi_X->AddEntry(Jpsipi_2d_BD_to_JPSI_PI_PI,"B_{d} -> J/#psi #pi #pi");
    if (RARE_DECAYS)
    {
        legend_Jpsipi_X->AddEntry(Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI,"#Lambda_{b} -> J/#psi p #pi");
        legend_Jpsipi_X->AddEntry(Jpsipi_2d_BD_to_JPSI_K_K,"B_{d} -> J/#psi K K");
        legend_Jpsipi_X->AddEntry(Jpsipi_2d_BS_to_JPSI_K_PI,"B_{s} -> J/#psi K #pi");
    }
	legend_Jpsipi_X->AddEntry(Jpsipi_2d_bg,"comb background");
    
    THStack hs_JpsipiX("hs_JpsipiX","Jpsipi_signalX");
    
    hs_JpsipiX.Add(Jpsipi_2d_bg->ProjectionX());
    hs_JpsipiX.Add(Jpsipi_2d_BD_to_JPSI_PI_PI->ProjectionX());
    hs_JpsipiX.Add(Jpsipi_2d_BS_to_JPSI_PI_PI->ProjectionX());
    hs_JpsipiX.Add(Jpsipi_2d_BS_to_JPSI_K_K->ProjectionX());
    hs_JpsipiX.Add(Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->ProjectionX());
    hs_JpsipiX.Add(Jpsipi_2d_BD_to_JPSI_K_PI->ProjectionX());
    if (RARE_DECAYS)
    {
        hs_JpsipiX.Add(Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionX());
        hs_JpsipiX.Add(Jpsipi_2d_BD_to_JPSI_K_K->ProjectionX());
        hs_JpsipiX.Add(Jpsipi_2d_BS_to_JPSI_K_PI->ProjectionX());
    }
    TCanvas * c_JpsipiX = new TCanvas("jpsipi_mass_ProjectionX");
    
    hist_buffer = (TH1D*)Jpsipi_2d_data->ProjectionX()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("m(J/#psi, h_{1}=#pi), GeV");
    hist_buffer->GetYaxis()->SetTitle("Events / 34 MeV");
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_JpsipiX.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    legend_Jpsipi_X->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = pictures_path + "Jpsipi_2d_ProjectionX.C";
    pictures_path_png = pictures_path + "Jpsipi_2d_ProjectionX.png";
    c_JpsipiX->SaveAs(pictures_path_cpp.c_str());
    c_JpsipiX->SaveAs(pictures_path_png.c_str());
    
    THStack hs_JpsipiY("hs_JpsipiY","Jpsipi_signalY");
    
    hs_JpsipiY.Add(Jpsipi_2d_bg->ProjectionY());
    hs_JpsipiY.Add(Jpsipi_2d_BD_to_JPSI_PI_PI->ProjectionY());
    hs_JpsipiY.Add(Jpsipi_2d_BS_to_JPSI_PI_PI->ProjectionY());
    hs_JpsipiY.Add(Jpsipi_2d_BS_to_JPSI_K_K->ProjectionY());
    hs_JpsipiY.Add(Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->ProjectionY());
    hs_JpsipiY.Add(Jpsipi_2d_BD_to_JPSI_K_PI->ProjectionY());
    if (RARE_DECAYS)
    {
        hs_JpsipiY.Add(Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionY());
        hs_JpsipiY.Add(Jpsipi_2d_BD_to_JPSI_K_K->ProjectionY());
        hs_JpsipiY.Add(Jpsipi_2d_BS_to_JPSI_K_PI->ProjectionY());
    }
    TCanvas * c_JpsipiY = new TCanvas("jpsipi_mass_ProjectionY");
    
    hist_buffer = (TH1D*)Jpsipi_2d_data->ProjectionY()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("m(J/#psi, h_{2}=#pi), GeV");
    hist_buffer->GetYaxis()->SetTitle("Events / 34 MeV");
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_JpsipiY.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    legend_Jpsipi_X->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = pictures_path + "Jpsipi_2d_ProjectionY.C";
    pictures_path_png = pictures_path + "Jpsipi_2d_ProjectionY.png";
    c_JpsipiY->SaveAs(pictures_path_cpp.c_str());
    c_JpsipiY->SaveAs(pictures_path_png.c_str());
    THStack hs_JpsiKX("hs_JpsiKX","JpsiK_signalX");
    
    hs_JpsiKX.Add(JpsiK_2d_bg->ProjectionX());
    hs_JpsiKX.Add(JpsiK_2d_BD_to_JPSI_PI_PI->ProjectionX());
    hs_JpsiKX.Add(JpsiK_2d_BS_to_JPSI_PI_PI->ProjectionX());
    hs_JpsiKX.Add(JpsiK_2d_BS_to_JPSI_K_K->ProjectionX());
    hs_JpsiKX.Add(JpsiK_2d_LAMBDA0B_to_JPSI_P_K->ProjectionX());
    hs_JpsiKX.Add(JpsiK_2d_BD_to_JPSI_K_PI->ProjectionX());
    if (RARE_DECAYS)
    {
        hs_JpsiKX.Add(JpsiK_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionX());
        hs_JpsiKX.Add(JpsiK_2d_BD_to_JPSI_K_K->ProjectionX());
        hs_JpsiKX.Add(JpsiK_2d_BS_to_JPSI_K_PI->ProjectionX());
    }
    TCanvas * c_JpsiKX = new TCanvas("JpsiK_mass_ProjectionX");
    
    hist_buffer = (TH1D*)JpsiK_2d_data->ProjectionX()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("m(J/#psi, h_{1}=K), GeV");
    hist_buffer->GetYaxis()->SetTitle("Events / 34 MeV");
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_JpsiKX.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    legend_Jpsipi_X->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = pictures_path + "JpsiK_2d_ProjectionX.C";
    pictures_path_png = pictures_path + "JpsiK_2d_ProjectionX.png";
    c_JpsiKX->SaveAs(pictures_path_cpp.c_str());
    c_JpsiKX->SaveAs(pictures_path_png.c_str());
    
    THStack hs_JpsiKY("hs_JpsiKY","JpsiK_signalY");
    
    hs_JpsiKY.Add(JpsiK_2d_bg->ProjectionY());
    hs_JpsiKY.Add(JpsiK_2d_BD_to_JPSI_PI_PI->ProjectionY());
    hs_JpsiKY.Add(JpsiK_2d_BS_to_JPSI_PI_PI->ProjectionY());
    hs_JpsiKY.Add(JpsiK_2d_BS_to_JPSI_K_K->ProjectionY());
    hs_JpsiKY.Add(JpsiK_2d_LAMBDA0B_to_JPSI_P_K->ProjectionY());
    hs_JpsiKY.Add(JpsiK_2d_BD_to_JPSI_K_PI->ProjectionY());
    if (RARE_DECAYS)
    {
        hs_JpsiKY.Add(JpsiK_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionY());
        hs_JpsiKY.Add(JpsiK_2d_BD_to_JPSI_K_K->ProjectionY());
        hs_JpsiKY.Add(JpsiK_2d_BS_to_JPSI_K_PI->ProjectionY());
    }
    TCanvas * c_JpsiKY = new TCanvas("JpsiK_mass_ProjectionY");
    
    hist_buffer = (TH1D*)JpsiK_2d_data->ProjectionY()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("m(J/#psi, h_{2}=K), GeV");
    hist_buffer->GetYaxis()->SetTitle("Events / 34 MeV");
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_JpsiKY.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    legend_Jpsipi_X->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = pictures_path + "JpsiK_2d_ProjectionY.C";
    pictures_path_png = pictures_path + "JpsiK_2d_ProjectionY.png";
    c_JpsiKY->SaveAs(pictures_path_cpp.c_str());
    c_JpsiKY->SaveAs(pictures_path_png.c_str());
    
    THStack hs_KpiX("hs_KpiX","Kpi_signalX");
    
    hs_KpiX.Add(Kpi_2d_bg->ProjectionX());
    hs_KpiX.Add(Kpi_2d_BD_to_JPSI_PI_PI->ProjectionX());
    hs_KpiX.Add(Kpi_2d_BS_to_JPSI_PI_PI->ProjectionX());
    hs_KpiX.Add(Kpi_2d_BS_to_JPSI_K_K->ProjectionX());
    hs_KpiX.Add(Kpi_2d_LAMBDA0B_to_JPSI_P_K->ProjectionX());
    hs_KpiX.Add(Kpi_2d_BD_to_JPSI_K_PI->ProjectionX());
    if (RARE_DECAYS)
    {
        hs_KpiX.Add(Kpi_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionX());
        hs_KpiX.Add(Kpi_2d_BD_to_JPSI_K_K->ProjectionX());
        hs_KpiX.Add(Kpi_2d_BS_to_JPSI_K_PI->ProjectionX());
    }
    TCanvas * c_KpiX = new TCanvas("Kpi_mass_ProjectionX");
    
    hist_buffer = (TH1D*)Kpi_2d_data->ProjectionX()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("m(h_{1}=K, h_{2}=#pi), GeV");
    hist_buffer->GetYaxis()->SetTitle("Events / 17 MeV");
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_KpiX.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    legend_Jpsipi_X->Draw();
    t->DrawLatex(0.35,0.88, writetext00);
    t->DrawLatex(0.35,0.83, writetext01);
    pictures_path_cpp = pictures_path + "Kpi_2d_ProjectionX.C";
    pictures_path_png = pictures_path + "Kpi_2d_ProjectionX.png";
    c_KpiX->SaveAs(pictures_path_cpp.c_str());
    c_KpiX->SaveAs(pictures_path_png.c_str());
    
    THStack hs_KpiY("hs_KpiY","Kpi_signalY");
    
    hs_KpiY.Add(Kpi_2d_bg->ProjectionY());
    hs_KpiY.Add(Kpi_2d_BD_to_JPSI_PI_PI->ProjectionY());
    hs_KpiY.Add(Kpi_2d_BS_to_JPSI_PI_PI->ProjectionY());
    hs_KpiY.Add(Kpi_2d_BS_to_JPSI_K_K->ProjectionY());
    hs_KpiY.Add(Kpi_2d_LAMBDA0B_to_JPSI_P_K->ProjectionY());
    hs_KpiY.Add(Kpi_2d_BD_to_JPSI_K_PI->ProjectionY());
    if (RARE_DECAYS)
    {
        hs_KpiY.Add(Kpi_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionY());
        hs_KpiY.Add(Kpi_2d_BD_to_JPSI_K_K->ProjectionY());
        hs_KpiY.Add(Kpi_2d_BS_to_JPSI_K_PI->ProjectionY());
    }
    TCanvas * c_KpiY = new TCanvas("Kpi_mass_ProjectionY");
    
    hist_buffer = (TH1D*)Kpi_2d_data->ProjectionY()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("m(h_{1}=#pi, h_{2}=K), GeV");
    hist_buffer->GetYaxis()->SetTitle("Events / 17 MeV");
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_KpiY.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    legend_Jpsipi_X->Draw();
    t->DrawLatex(0.35,0.88, writetext00);
    t->DrawLatex(0.35,0.83, writetext01);
    pictures_path_cpp = pictures_path + "Kpi_2d_ProjectionY.C";
    pictures_path_png = pictures_path + "Kpi_2d_ProjectionY.png";
    c_KpiY->SaveAs(pictures_path_cpp.c_str());
    c_KpiY->SaveAs(pictures_path_png.c_str());
    
    
    TH2* Jpsipi_2d_K1410_1 = new TH2D("Jpsipi_mass_K1410_1", "Jpsipi_mass_K1410_1", 50, 3.3, 5., 50, 3.3, 5.);
	TH2* Jpsipi_2d_K1430_0 = new TH2D("Jpsipi_mass_K1430_0", "Jpsipi_mass_K1430_0", 50, 3.3, 5., 50, 3.3, 5.);
	TH2* Jpsipi_2d_K1430_2 = new TH2D("Jpsipi_mass_K1430_2", "Jpsipi_mass_K1430_2", 50, 3.3, 5., 50, 3.3, 5.);
	TH2* Jpsipi_2d_K1680_1 = new TH2D("Jpsipi_mass_K1680_1", "Jpsipi_mass_K1680_1", 50, 3.3, 5., 50, 3.3, 5.);
	TH2* Jpsipi_2d_K1780_3 = new TH2D("Jpsipi_mass_K1780_3", "Jpsipi_mass_K1780_3", 50, 3.3, 5., 50, 3.3, 5.);
	TH2* Jpsipi_2d_K1950_0 = new TH2D("Jpsipi_mass_K1950_0", "Jpsipi_mass_K1950_0", 50, 3.3, 5., 50, 3.3, 5.);
	TH2* Jpsipi_2d_K1980_2 = new TH2D("Jpsipi_mass_K1980_2", "Jpsipi_mass_K1980_2", 50, 3.3, 5., 50, 3.3, 5.);
	TH2* Jpsipi_2d_K2045_4 = new TH2D("Jpsipi_mass_K2045_4", "Jpsipi_mass_K2045_4", 50, 3.3, 5., 50, 3.3, 5.);
    TH2* Jpsipi_2d_KNR_0 = new TH2D("Jpsipi_mass_KNR_0", "Jpsipi_mass_KNR_0", 50, 3.3, 5., 50, 3.3, 5.);
    TH2* Jpsipi_2d_Zc4200 = new TH2D("Jpsipi_mass_Zc4200", "Jpsipi_mass_Zc4200", 50, 3.3, 5., 50, 3.3, 5.);
    TH2* Jpsipi_2d_sum = new TH2D("Jpsipi_mass_sum", "Jpsipi_mass_sum", 50, 3.3, 5., 50, 3.3, 5.);
    
    TH2* JpsiK_2d_K1410_1 = new TH2D("JpsiK_mass_K1410_1", "JpsiK_mass_K1410_1", 50, 3.5, 5.2, 50, 3.5, 5.2);
	TH2* JpsiK_2d_K1430_0 = new TH2D("JpsiK_mass_K1430_0", "JpsiK_mass_K1430_0", 50, 3.5, 5.2, 50, 3.5, 5.2);
	TH2* JpsiK_2d_K1430_2 = new TH2D("JpsiK_mass_K1430_2", "JpsiK_mass_K1430_2", 50, 3.5, 5.2, 50, 3.5, 5.2);
	TH2* JpsiK_2d_K1680_1 = new TH2D("JpsiK_mass_K1680_1", "JpsiK_mass_K1680_1", 50, 3.5, 5.2, 50, 3.5, 5.2);
	TH2* JpsiK_2d_K1780_3 = new TH2D("JpsiK_mass_K1780_3", "JpsiK_mass_K1780_3", 50, 3.5, 5.2, 50, 3.5, 5.2);
	TH2* JpsiK_2d_K1950_0 = new TH2D("JpsiK_mass_K1950_0", "JpsiK_mass_K1950_0", 50, 3.5, 5.2, 50, 3.5, 5.2);
	TH2* JpsiK_2d_K1980_2 = new TH2D("JpsiK_mass_K1980_2", "JpsiK_mass_K1980_2", 50, 3.5, 5.2, 50, 3.5, 5.2);
	TH2* JpsiK_2d_K2045_4 = new TH2D("JpsiK_mass_K2045_4", "JpsiK_mass_K2045_4", 50, 3.5, 5.2, 50, 3.5, 5.2);
    TH2* JpsiK_2d_KNR_0 = new TH2D("JpsiK_mass_K2NR_0", "JpsiK_mass_KNR_0", 50, 3.5, 5.2, 50, 3.5, 5.2);
    TH2* JpsiK_2d_Zc4200 = new TH2D("JpsiK_mass_Zc4200", "JpsiK_mass_Zc4200", 50, 3.5, 5.2, 50, 3.5, 5.2);
    TH2* JpsiK_2d_sum = new TH2D("JpsiK_mass_sum", "JpsiK_mass_sum", 50, 3.5, 5.2, 50, 3.5, 5.2);

	TH2* Kpi_2d_K1410_1 = new TH2D("Kpi_mass_K1410_1", "Kpi_mass_K1410_1", 50, 1.55, 2.4, 50, 1.55, 2.4);
	TH2* Kpi_2d_K1430_0 = new TH2D("Kpi_mass_K1430_0", "Kpi_mass_K1430_0", 50, 1.55, 2.4, 50, 1.55, 2.4);
	TH2* Kpi_2d_K1430_2 = new TH2D("Kpi_mass_K1430_2", "Kpi_mass_K1430_2", 50, 1.55, 2.4, 50, 1.55, 2.4);
	TH2* Kpi_2d_K1680_1 = new TH2D("Kpi_mass_K1680_1", "Kpi_mass_K1680_1", 50, 1.55, 2.4, 50, 1.55, 2.4);
	TH2* Kpi_2d_K1780_3 = new TH2D("Kpi_mass_K1780_3", "Kpi_mass_K1780_3", 50, 1.55, 2.4, 50, 1.55, 2.4);
	TH2* Kpi_2d_K1950_0 = new TH2D("Kpi_mass_K1950_0", "Kpi_mass_K1950_0", 50, 1.55, 2.4, 50, 1.55, 2.4);
	TH2* Kpi_2d_K1980_2 = new TH2D("Kpi_mass_K1980_2", "Kpi_mass_K1980_2", 50, 1.55, 2.4, 50, 1.55, 2.4);
	TH2* Kpi_2d_K2045_4 = new TH2D("Kpi_mass_K2045_4", "Kpi_mass_K2045_4", 50, 1.55, 2.4, 50, 1.55, 2.4);
    TH2* Kpi_2d_KNR_0 = new TH2D("Kpi_mass_KNR_0", "Kpi_mass_KNR_0", 50, 1.55, 2.4, 50, 1.55, 2.4);
    TH2* Kpi_2d_Zc4200 = new TH2D("Kpi_mass_Zc4200", "Kpi_mass_Zc4200", 50, 1.55, 2.4, 50, 1.55, 2.4);
    TH2* Kpi_2d_sum = new TH2D("Kpi_mass_sum", "Kpi_mass_sum", 50, 1.55, 2.4, 50, 1.55, 2.4);

	for (int i = 0; i < phs_Bd; i++)
    {
		if (signal_a[i]) {
        float w_K1410_1 = 0., w_K1430_0 = 0., w_K1430_2 = 0., w_K1680_1 = 0., w_K1780_3 = 0., w_K1950_0 = 0.,  w_K1980_2 = 0., w_K2045_4 = 0., w_KNR_0 = 0., w_Zc4200 = 0.;
        for(int i3 = 0; i3 < 2; i3++)
		{
			float hmu = 2*i3 - 1.0;
			w_K1410_1 += pow(abs(HamplitudeKstar1(Kpi_truth[i] / 1000., 5.27964, 1.421, 0.236, std::complex<float>(K1410_1_Bw1_amp * cos(K1410_1_Bw1_phi), K1410_1_Bw1_amp * sin(K1410_1_Bw1_phi)), std::complex<float>(K1410_1_Bw2_amp * cos(K1410_1_Bw2_phi), K1410_1_Bw2_amp * sin(K1410_1_Bw2_phi)), std::complex<float>(K1410_1_Bw3_amp * cos(K1410_1_Bw3_phi), K1410_1_Bw3_amp * sin(K1410_1_Bw3_phi)), hmu, theta_Kstar_a[i], theta_psi_Kstar_a[i], phi_mu_Kstar_a[i], phi_K_a[i] )), 2.);
			w_K1430_0 += pow(abs(HamplitudeKstar0(Kpi_truth[i] / 1000., 5.27964, 1.425, 0.270, std::complex<float>(K1430_0_Bw1_amp * cos(K1430_0_Bw1_phi), K1430_0_Bw1_amp * sin(K1430_0_Bw1_phi)), std::complex<float>(0., 0.), std::complex<float>(0., 0.), hmu, theta_Kstar_a[i], theta_psi_Kstar_a[i], phi_mu_Kstar_a[i], phi_K_a[i] )), 2.);
			w_K1430_2 += pow(abs(HamplitudeKstar2(Kpi_truth[i] / 1000., 5.27964, 1.4324, 0.109, std::complex<float>(K1430_2_Bw1_amp * cos(K1430_2_Bw1_phi), K1430_2_Bw1_amp * sin(K1430_2_Bw1_phi)), std::complex<float>(K1430_2_Bw2_amp * cos(K1430_2_Bw2_phi), K1430_2_Bw2_amp * sin(K1430_2_Bw2_phi)), std::complex<float>(K1430_2_Bw3_amp * cos(K1430_2_Bw3_phi), K1430_2_Bw3_amp * sin(K1430_2_Bw3_phi)), hmu, theta_Kstar_a[i], theta_psi_Kstar_a[i], phi_mu_Kstar_a[i], phi_K_a[i] )), 2.);
			w_K1680_1 += pow(abs(HamplitudeKstar1(Kpi_truth[i] / 1000., 5.27964, 1.718, 0.322, std::complex<float>(K1680_1_Bw1_amp * cos(K1680_1_Bw1_phi), K1680_1_Bw1_amp * sin(K1680_1_Bw1_phi)), std::complex<float>(K1680_1_Bw2_amp * cos(K1680_1_Bw2_phi), K1680_1_Bw2_amp * sin(K1680_1_Bw2_phi)), std::complex<float>(K1680_1_Bw3_amp * cos(K1680_1_Bw3_phi), K1680_1_Bw3_amp * sin(K1680_1_Bw3_phi)), hmu, theta_Kstar_a[i], theta_psi_Kstar_a[i], phi_mu_Kstar_a[i], phi_K_a[i] )), 2.);
			w_K1780_3 += pow(abs(HamplitudeKstar3(Kpi_truth[i] / 1000., 5.27964, 1.776, 0.159, std::complex<float>(K1780_3_Bw1_amp * cos(K1780_3_Bw1_phi), K1780_3_Bw1_amp * sin(K1780_3_Bw1_phi)), std::complex<float>(K1780_3_Bw2_amp * cos(K1780_3_Bw2_phi), K1780_3_Bw2_amp * sin(K1780_3_Bw2_phi)), std::complex<float>(K1780_3_Bw3_amp * cos(K1780_3_Bw3_phi), K1780_3_Bw3_amp * sin(K1780_3_Bw3_phi)), hmu, theta_Kstar_a[i], theta_psi_Kstar_a[i], phi_mu_Kstar_a[i], phi_K_a[i] )), 2.);
			w_K1950_0 += pow(abs(HamplitudeKstar0(Kpi_truth[i] / 1000., 5.27964, 1.945, 0.201, std::complex<float>(K1950_0_Bw1_amp * cos(K1950_0_Bw1_phi), K1950_0_Bw1_amp * sin(K1950_0_Bw1_phi)), std::complex<float>(0., 0.), std::complex<float>(0., 0.), hmu, theta_Kstar_a[i], theta_psi_Kstar_a[i], phi_mu_Kstar_a[i], phi_K_a[i] )), 2.);
			//w_K1980_2 += pow(abs(HamplitudeKstar2(Kpi_truth[i] / 1000., 5.27964, 1.974, 0.376, std::complex<float>(K1980_2_Bw1_amp * cos(K1980_2_Bw1_phi), K1980_2_Bw1_amp * sin(K1980_2_Bw1_phi)), std::complex<float>(K1980_2_Bw2_amp * cos(K1980_2_Bw2_phi), K1980_2_Bw2_amp * sin(K1980_2_Bw2_phi)), std::complex<float>(K1980_2_Bw3_amp * cos(K1980_2_Bw3_phi), K1980_2_Bw3_amp * sin(K1980_2_Bw3_phi)), hmu, theta_Kstar_a[i], theta_psi_Kstar_a[i], phi_mu_Kstar_a[i], phi_K_a[i] )), 2.);
            w_K1980_2 += pow(abs(HamplitudeKstar2_accelerated(Kpi_truth[i] / 1000., 5.27964, 1.974, 0.376, std::complex<float>(K1980_2_Bw1_amp * cos(K1980_2_Bw1_phi), K1980_2_Bw1_amp * sin(K1980_2_Bw1_phi)), std::complex<float>(K1980_2_Bw2_amp * cos(K1980_2_Bw2_phi), K1980_2_Bw2_amp * sin(K1980_2_Bw2_phi)), std::complex<float>(K1980_2_Bw3_amp * cos(K1980_2_Bw3_phi), K1980_2_Bw3_amp * sin(K1980_2_Bw3_phi)), hmu, theta_Kstar_a[i], theta_psi_Kstar_a[i], phi_mu_Kstar_a[i], phi_K_a[i], BWK1980_2_1_minus[i], BWK1980_2_2_minus[i], BWK1980_2_3_minus[i], BWK1980_2_1_plus[i], BWK1980_2_2_plus[i], BWK1980_2_3_plus[i] )), 2.);
			w_K2045_4 += pow(abs(HamplitudeKstar4(Kpi_truth[i] / 1000., 5.27964, 2.045, 0.198, std::complex<float>(K2045_4_Bw1_amp * cos(K2045_4_Bw1_phi), K2045_4_Bw1_amp * sin(K2045_4_Bw1_phi)), std::complex<float>(K2045_4_Bw2_amp * cos(K2045_4_Bw2_phi), K2045_4_Bw2_amp * sin(K2045_4_Bw2_phi)), std::complex<float>(K2045_4_Bw3_amp * cos(K2045_4_Bw3_phi), K2045_4_Bw3_amp * sin(K2045_4_Bw3_phi)), hmu, theta_Kstar_a[i], theta_psi_Kstar_a[i], phi_mu_Kstar_a[i], phi_K_a[i] )), 2.);
            w_KNR_0 += pow(abs(HamplitudeKstarNR0(Kpi_truth[i] / 1000., 5.27964, 1.425, 0.270, std::complex<float>(KNR_0_Bw_amp * cos(KNR_0_Bw_phi), KNR_0_Bw_amp * sin(KNR_0_Bw_phi)), 0., 0., hmu, theta_Kstar_a[i], theta_psi_Kstar_a[i], phi_mu_Kstar_a[i], phi_K_a[i] )), 2.);
            if (MODEL == 0)
            {
                w_Zc4200 += pow(abs(HamplitudeX1plus(Jpsipi_truth[i] / 1000., 5.27964, m_Zc4200, gamma_Zc4200, std::complex<float>(X1Bs1_amp * cos(X1Bs1_phi), X1Bs1_amp * sin(X1Bs1_phi)), std::complex<float>(X1Bs2_amp * cos(X1Bs2_phi), X1Bs2_amp * sin(X1Bs2_phi)), hmu, theta_X_a[i], theta_psi_X_a[i], phi_mu_X_a[i], alpha_mu_a[i] )), 2.);
            }
            if (MODEL == 2)
            {
                w_Zc4200 += pow(abs(HamplitudeX1minus(Jpsipi_truth[i] / 1000., 5.27964, m_Zc4200, gamma_Zc4200, std::complex<float>(X1Bs1_amp * cos(X1Bs1_phi), X1Bs1_amp * sin(X1Bs1_phi)), std::complex<float>(X1Bs2_amp * cos(X1Bs2_phi), X1Bs2_amp * sin(X1Bs2_phi)), hmu, theta_X_a[i], theta_psi_X_a[i], phi_mu_X_a[i], alpha_mu_a[i] )), 2.);
            }
            if (MODEL == 3)
            {
                w_Zc4200 += pow(abs(HamplitudeX0minus(Jpsipi_truth[i] / 1000., 5.27964, m_Zc4200, gamma_Zc4200, std::complex<float>(X1Bs1_amp * cos(X1Bs1_phi), X1Bs1_amp * sin(X1Bs1_phi)), std::complex<float>(X1Bs2_amp * cos(X1Bs2_phi), X1Bs2_amp * sin(X1Bs2_phi)), hmu, theta_X_a[i], theta_psi_X_a[i], phi_mu_X_a[i], alpha_mu_a[i] )), 2.);
            }
            if (MODEL == 4)
            {
                w_Zc4200 += pow(abs(HamplitudeX2minus(Jpsipi_truth[i] / 1000., 5.27964, m_Zc4200, gamma_Zc4200, std::complex<float>(X1Bs1_amp * cos(X1Bs1_phi), X1Bs1_amp * sin(X1Bs1_phi)), std::complex<float>(X1Bs2_amp * cos(X1Bs2_phi), X1Bs2_amp * sin(X1Bs2_phi)), hmu, theta_X_a[i], theta_psi_X_a[i], phi_mu_X_a[i], alpha_mu_a[i] )), 2.);
            }
            if (MODEL == 5)
            {
                w_Zc4200 += pow(abs(HamplitudeX2plus(Jpsipi_truth[i] / 1000., 5.27964, m_Zc4200, gamma_Zc4200, std::complex<float>(X1Bs1_amp * cos(X1Bs1_phi), X1Bs1_amp * sin(X1Bs1_phi)), std::complex<float>(X1Bs2_amp * cos(X1Bs2_phi), X1Bs2_amp * sin(X1Bs2_phi)), hmu, theta_X_a[i], theta_psi_X_a[i], phi_mu_X_a[i], alpha_mu_a[i] )), 2.);
            }
            //std::complex<float> test_c = std::complex<float>(X1Bs1_amp * cos(X1Bs1_phi), X1Bs1_amp * sin(X1Bs1_phi));
            //std::complex<float>(X1Bs2_amp * cos(X1Bs2_phi), X1Bs2_amp * sin(X1Bs2_phi))
    	}
    	
		JpsiK_2d_K1410_1->Fill(JpsiK1_mass[i], JpsiK2_mass[i], w_K1410_1 / 3. * w_dimuon_a[i]);
		JpsiK_2d_K1430_0->Fill(JpsiK1_mass[i], JpsiK2_mass[i], w_K1430_0 / 3. * w_dimuon_a[i]);
		JpsiK_2d_K1430_2->Fill(JpsiK1_mass[i], JpsiK2_mass[i], w_K1430_2 / 3. * w_dimuon_a[i]);
		JpsiK_2d_K1680_1->Fill(JpsiK1_mass[i], JpsiK2_mass[i], w_K1680_1 / 3. * w_dimuon_a[i]);
		JpsiK_2d_K1780_3->Fill(JpsiK1_mass[i], JpsiK2_mass[i], w_K1780_3 / 3. * w_dimuon_a[i]);
		JpsiK_2d_K1950_0->Fill(JpsiK1_mass[i], JpsiK2_mass[i], w_K1950_0 / 3. * w_dimuon_a[i]);
		JpsiK_2d_K1980_2->Fill(JpsiK1_mass[i], JpsiK2_mass[i], w_K1980_2 / 3. * w_dimuon_a[i]);
		JpsiK_2d_K2045_4->Fill(JpsiK1_mass[i], JpsiK2_mass[i], w_K2045_4 / 3. * w_dimuon_a[i]);
        JpsiK_2d_Zc4200->Fill(JpsiK1_mass[i], JpsiK2_mass[i], w_Zc4200/ 3. * w_dimuon_a[i]);
        JpsiK_2d_KNR_0->Fill(JpsiK1_mass[i], JpsiK2_mass[i], w_KNR_0/ 3. * w_dimuon_a[i]);
        
        Jpsipi_2d_K1410_1->Fill(Jpsipi1_mass[i], Jpsipi2_mass[i], w_K1410_1 / 3. * w_dimuon_a[i]);
		Jpsipi_2d_K1430_0->Fill(Jpsipi1_mass[i], Jpsipi2_mass[i], w_K1430_0 / 3. * w_dimuon_a[i]);
		Jpsipi_2d_K1430_2->Fill(Jpsipi1_mass[i], Jpsipi2_mass[i], w_K1430_2 / 3. * w_dimuon_a[i]);
		Jpsipi_2d_K1680_1->Fill(Jpsipi1_mass[i], Jpsipi2_mass[i], w_K1680_1 / 3. * w_dimuon_a[i]);
		Jpsipi_2d_K1780_3->Fill(Jpsipi1_mass[i], Jpsipi2_mass[i], w_K1780_3 / 3. * w_dimuon_a[i]);
		Jpsipi_2d_K1950_0->Fill(Jpsipi1_mass[i], Jpsipi2_mass[i], w_K1950_0 / 3. * w_dimuon_a[i]);
		Jpsipi_2d_K1980_2->Fill(Jpsipi1_mass[i], Jpsipi2_mass[i], w_K1980_2 / 3. * w_dimuon_a[i]);
		Jpsipi_2d_K2045_4->Fill(Jpsipi1_mass[i], Jpsipi2_mass[i], w_K2045_4 / 3. * w_dimuon_a[i]);
        Jpsipi_2d_Zc4200->Fill(Jpsipi1_mass[i], Jpsipi2_mass[i], w_Zc4200 / 3. * w_dimuon_a[i]);
        Jpsipi_2d_KNR_0->Fill(Jpsipi1_mass[i], Jpsipi2_mass[i], w_KNR_0 / 3. * w_dimuon_a[i]);
        
        Kpi_2d_K1410_1->Fill(K1pi2_mass[i], K2pi1_mass[i], w_K1410_1 / 3. * w_dimuon_a[i]);
		Kpi_2d_K1430_0->Fill(K1pi2_mass[i], K2pi1_mass[i], w_K1430_0 / 3. * w_dimuon_a[i]);
		Kpi_2d_K1430_2->Fill(K1pi2_mass[i], K2pi1_mass[i], w_K1430_2 / 3. * w_dimuon_a[i]);
		Kpi_2d_K1680_1->Fill(K1pi2_mass[i], K2pi1_mass[i], w_K1680_1 / 3. * w_dimuon_a[i]);
		Kpi_2d_K1780_3->Fill(K1pi2_mass[i], K2pi1_mass[i], w_K1780_3 / 3. * w_dimuon_a[i]);
		Kpi_2d_K1950_0->Fill(K1pi2_mass[i], K2pi1_mass[i], w_K1950_0 / 3. * w_dimuon_a[i]);
		Kpi_2d_K1980_2->Fill(K1pi2_mass[i], K2pi1_mass[i], w_K1980_2 / 3. * w_dimuon_a[i]);
		Kpi_2d_K2045_4->Fill(K1pi2_mass[i], K2pi1_mass[i], w_K2045_4 / 3. * w_dimuon_a[i]);
        Kpi_2d_Zc4200->Fill(K1pi2_mass[i], K2pi1_mass[i], w_Zc4200 / 3. * w_dimuon_a[i]);
        Kpi_2d_KNR_0->Fill(K1pi2_mass[i], K2pi1_mass[i], w_KNR_0 / 3. * w_dimuon_a[i]);    
        }
	}
	
	float norm_Jpsipi = 1. / (Jpsipi_2d_K1410_1->Integral() + Jpsipi_2d_K1430_0->Integral() + Jpsipi_2d_K1430_2->Integral() + Jpsipi_2d_K1680_1->Integral() + Jpsipi_2d_K1780_3->Integral() + Jpsipi_2d_K1950_0->Integral() + Jpsipi_2d_K1980_2->Integral() + Jpsipi_2d_K2045_4->Integral() + Jpsipi_2d_Zc4200->Integral() + Jpsipi_2d_KNR_0->Integral()) * n_BD_to_JPSI_K_PI;
    
    TCanvas * c_Bdwave_JpsipiX = new TCanvas("Bdwave_mass_JpsipiX");
	//Jpsipi_2d_total->SetStats(kFALSE);
	Jpsipi_2d_total->SetLineColor(1);
	Jpsipi_2d_K1410_1->SetLineColor(2);
	Jpsipi_2d_K1430_0->SetLineColor(3);
	Jpsipi_2d_K1430_2->SetLineColor(4);
	Jpsipi_2d_K1680_1->SetLineColor(5);
	Jpsipi_2d_K1780_3->SetLineColor(6);
	Jpsipi_2d_K1950_0->SetLineColor(7);
	Jpsipi_2d_K1980_2->SetLineColor(8);
	Jpsipi_2d_K2045_4->SetLineColor(9);
    Jpsipi_2d_Zc4200->SetLineColor(46);
    Jpsipi_2d_Zc4200->SetFillColor(46);
    Jpsipi_2d_KNR_0->SetLineColor(47);

	Jpsipi_2d_total->SetLineWidth(3);
	Jpsipi_2d_K1410_1->SetLineWidth(2);
	Jpsipi_2d_K1430_0->SetLineWidth(2);
	Jpsipi_2d_K1430_2->SetLineWidth(2);
	Jpsipi_2d_K1680_1->SetLineWidth(2);
	Jpsipi_2d_K1780_3->SetLineWidth(2);
	Jpsipi_2d_K1950_0->SetLineWidth(2);
	Jpsipi_2d_K1980_2->SetLineWidth(2);
	Jpsipi_2d_K2045_4->SetLineWidth(2);
    Jpsipi_2d_Zc4200->SetLineWidth(2);
    Jpsipi_2d_KNR_0->SetLineWidth(2);

	Jpsipi_2d_K1410_1->Scale(norm_Jpsipi);
	Jpsipi_2d_K1430_0->Scale(norm_Jpsipi);
	Jpsipi_2d_K1430_2->Scale(norm_Jpsipi);
	Jpsipi_2d_K1680_1->Scale(norm_Jpsipi);
	Jpsipi_2d_K1780_3->Scale(norm_Jpsipi);
	Jpsipi_2d_K1950_0->Scale(norm_Jpsipi);
	Jpsipi_2d_K1980_2->Scale(norm_Jpsipi);
	Jpsipi_2d_K2045_4->Scale(norm_Jpsipi);
    Jpsipi_2d_Zc4200->Scale(norm_Jpsipi);
    Jpsipi_2d_KNR_0->Scale(norm_Jpsipi);
	
	hist_buffer = (TH1D*)Jpsipi_2d_total->ProjectionX()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("m(J/#psi, h_{1}=#pi), GeV");
    hist_buffer->GetYaxis()->SetTitle("Events / 34 MeV");
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("hist");
    Jpsipi_2d_Zc4200->ProjectionX()->Draw("hist same");
	Jpsipi_2d_K1410_1->ProjectionX()->Draw("hist same");
	Jpsipi_2d_K1430_0->ProjectionX()->Draw("hist same");
	Jpsipi_2d_K1430_2->ProjectionX()->Draw("hist same");
	Jpsipi_2d_K1680_1->ProjectionX()->Draw("hist same");
	Jpsipi_2d_K1780_3->ProjectionX()->Draw("hist same");
	Jpsipi_2d_K1950_0->ProjectionX()->Draw("hist same");
	Jpsipi_2d_K1980_2->ProjectionX()->Draw("hist same");
	Jpsipi_2d_K2045_4->ProjectionX()->Draw("hist same");
    Jpsipi_2d_KNR_0->ProjectionX()->Draw("hist same");
    
	TLegend *legend_1 = new TLegend(.60,.60,.90,.90);
    legend_1->AddEntry(Jpsipi_2d_total,"total");
	legend_1->AddEntry(Jpsipi_2d_K1410_1,"K_{1}*(1410)");
	legend_1->AddEntry(Jpsipi_2d_K1430_0,"K_{0}*(1430)");
	legend_1->AddEntry(Jpsipi_2d_K1430_2,"K_{2}*(1430)");
	legend_1->AddEntry(Jpsipi_2d_K1680_1,"K_{1}*(1680)");
	legend_1->AddEntry(Jpsipi_2d_K1780_3,"K_{3}*(1780)");
	legend_1->AddEntry(Jpsipi_2d_K1950_0,"K_{0}*(1950)");
	legend_1->AddEntry(Jpsipi_2d_K1980_2,"K_{2}*(1980)");
	legend_1->AddEntry(Jpsipi_2d_K2045_4,"K_{4}*(2045)");
    legend_1->AddEntry(Jpsipi_2d_KNR_0,"KNR_{0}");
    legend_1->AddEntry(Jpsipi_2d_Zc4200,"Zc(4200)");
	legend_1->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = pictures_path + "Bdwave_Jpsipi_2d_ProjectionX.C";
    pictures_path_png = pictures_path + "Bdwave_Jpsipi_2d_ProjectionX.png";
    c_Bdwave_JpsipiX->SaveAs(pictures_path_cpp.c_str());
    c_Bdwave_JpsipiX->SaveAs(pictures_path_png.c_str());
    
    TCanvas * c_Bdwave_JpsipiY = new TCanvas("Bdwave_mass_JpsipiY");
	//Jpsipi_2d_total->SetStats(kFALSE);
	
    hist_buffer = (TH1D*)Jpsipi_2d_total->ProjectionY()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("m(J/#psi, h_{2}=#pi), GeV");
    hist_buffer->GetYaxis()->SetTitle("Events / 34 MeV");
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("hist");
    Jpsipi_2d_Zc4200->ProjectionY()->Draw("hist same");
    Jpsipi_2d_K1410_1->ProjectionY()->Draw("hist same");
	Jpsipi_2d_K1430_0->ProjectionY()->Draw("hist same");
	Jpsipi_2d_K1430_2->ProjectionY()->Draw("hist same");
	Jpsipi_2d_K1680_1->ProjectionY()->Draw("hist same");
	Jpsipi_2d_K1780_3->ProjectionY()->Draw("hist same");
	Jpsipi_2d_K1950_0->ProjectionY()->Draw("hist same");
	Jpsipi_2d_K1980_2->ProjectionY()->Draw("hist same");
	Jpsipi_2d_K2045_4->ProjectionY()->Draw("hist same");
    Jpsipi_2d_KNR_0->ProjectionY()->Draw("hist same");
    
	
	legend_1->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = pictures_path + "Bdwave_Jpsipi_2d_ProjectionY.C";
    pictures_path_png = pictures_path + "Bdwave_Jpsipi_2d_ProjectionY.png";
    c_Bdwave_JpsipiY->SaveAs(pictures_path_cpp.c_str());
    c_Bdwave_JpsipiY->SaveAs(pictures_path_png.c_str());
    
    float norm_JpsiK = 1. / (JpsiK_2d_K1410_1->Integral() + JpsiK_2d_K1430_0->Integral() + JpsiK_2d_K1430_2->Integral() + JpsiK_2d_K1680_1->Integral() + JpsiK_2d_K1780_3->Integral() + JpsiK_2d_K1950_0->Integral() + JpsiK_2d_K1980_2->Integral() + JpsiK_2d_K2045_4->Integral() + JpsiK_2d_Zc4200->Integral() + JpsiK_2d_KNR_0->Integral()) * n_BD_to_JPSI_K_PI;
    
    cout << "\nJpsiK_2d_K1410_1\t" << JpsiK_2d_K1410_1->Integral() << endl;
    cout << "JpsiK_2d_K1430_0\t" << JpsiK_2d_K1430_0->Integral() << endl;
    cout << "JpsiK_2d_K1430_2\t" << JpsiK_2d_K1430_2->Integral() << endl;
    cout << "JpsiK_2d_K1680_1\t" << JpsiK_2d_K1680_1->Integral() << endl;
    cout << "JpsiK_2d_K1780_3\t" << JpsiK_2d_K1780_3->Integral() << endl;
    cout << "JpsiK_2d_K1950_0\t" << JpsiK_2d_K1950_0->Integral() << endl;
    cout << "JpsiK_2d_K1980_2\t" << JpsiK_2d_K1980_2->Integral() << endl;
    cout << "JpsiK_2d_K2045_4\t" << JpsiK_2d_K2045_4->Integral() << endl;
    cout << "JpsiK_2d_KNR_0\t" << JpsiK_2d_KNR_0->Integral() << endl;
    cout << "JpsiK_2d_Zc4200\t\t" << JpsiK_2d_Zc4200->Integral() << "\n" << endl;
    
    TCanvas * c_Bdwave_JpsiKX = new TCanvas("Bdwave_mass_JpsiKX");
	//JpsiK_2d_total->SetStats(kFALSE);
	JpsiK_2d_total->SetLineColor(1);
	JpsiK_2d_K1410_1->SetLineColor(2);
	JpsiK_2d_K1430_0->SetLineColor(3);
	JpsiK_2d_K1430_2->SetLineColor(4);
	JpsiK_2d_K1680_1->SetLineColor(5);
	JpsiK_2d_K1780_3->SetLineColor(6);
	JpsiK_2d_K1950_0->SetLineColor(7);
	JpsiK_2d_K1980_2->SetLineColor(8);
	JpsiK_2d_K2045_4->SetLineColor(9);
    JpsiK_2d_Zc4200->SetLineColor(46);
    JpsiK_2d_Zc4200->SetFillColor(46);
    JpsiK_2d_KNR_0->SetLineColor(47);

	JpsiK_2d_total->SetLineWidth(3);
	JpsiK_2d_K1410_1->SetLineWidth(2);
	JpsiK_2d_K1430_0->SetLineWidth(2);
	JpsiK_2d_K1430_2->SetLineWidth(2);
	JpsiK_2d_K1680_1->SetLineWidth(2);
	JpsiK_2d_K1780_3->SetLineWidth(2);
	JpsiK_2d_K1950_0->SetLineWidth(2);
	JpsiK_2d_K1980_2->SetLineWidth(2);
	JpsiK_2d_K2045_4->SetLineWidth(2);
    JpsiK_2d_KNR_0->SetLineWidth(2);
    JpsiK_2d_Zc4200->SetLineWidth(2);

	JpsiK_2d_K1410_1->Scale(norm_JpsiK);
	JpsiK_2d_K1430_0->Scale(norm_JpsiK);
	JpsiK_2d_K1430_2->Scale(norm_JpsiK);
	JpsiK_2d_K1680_1->Scale(norm_JpsiK);
	JpsiK_2d_K1780_3->Scale(norm_JpsiK);
	JpsiK_2d_K1950_0->Scale(norm_JpsiK);
	JpsiK_2d_K1980_2->Scale(norm_JpsiK);
	JpsiK_2d_K2045_4->Scale(norm_JpsiK);
    JpsiK_2d_KNR_0->Scale(norm_JpsiK);
    JpsiK_2d_Zc4200->Scale(norm_JpsiK);
    
    cout << "\nJpsiK_2d_K1410_1\t" << JpsiK_2d_K1410_1->Integral() << endl;
    cout << "JpsiK_2d_K1430_0\t" << JpsiK_2d_K1430_0->Integral() << endl;
    cout << "JpsiK_2d_K1430_2\t" << JpsiK_2d_K1430_2->Integral() << endl;
    cout << "JpsiK_2d_K1680_1\t" << JpsiK_2d_K1680_1->Integral() << endl;
    cout << "JpsiK_2d_K1780_3\t" << JpsiK_2d_K1780_3->Integral() << endl;
    cout << "JpsiK_2d_K1950_0\t" << JpsiK_2d_K1950_0->Integral() << endl;
    cout << "JpsiK_2d_K1980_2\t" << JpsiK_2d_K1980_2->Integral() << endl;
    cout << "JpsiK_2d_K2045_4\t" << JpsiK_2d_K2045_4->Integral() << endl;
    cout << "JpsiK_2d_KNR_0\t" << JpsiK_2d_KNR_0->Integral() << endl;
    cout << "JpsiK_2d_Zc4200\t\t" << JpsiK_2d_Zc4200->Integral() << "\n" << endl;
	
	hist_buffer = (TH1D*)JpsiK_2d_total->ProjectionX()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("m(J/#psi, h_{1}=K), GeV");
    hist_buffer->GetYaxis()->SetTitle("Events / 34 MeV");
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("hist");
    JpsiK_2d_Zc4200->ProjectionX()->Draw("hist same");
	JpsiK_2d_K1410_1->ProjectionX()->Draw("hist same");
	JpsiK_2d_K1430_0->ProjectionX()->Draw("hist same");
	JpsiK_2d_K1430_2->ProjectionX()->Draw("hist same");
	JpsiK_2d_K1680_1->ProjectionX()->Draw("hist same");
	JpsiK_2d_K1780_3->ProjectionX()->Draw("hist same");
	JpsiK_2d_K1950_0->ProjectionX()->Draw("hist same");
	JpsiK_2d_K1980_2->ProjectionX()->Draw("hist same");
	JpsiK_2d_K2045_4->ProjectionX()->Draw("hist same");
    JpsiK_2d_KNR_0->ProjectionX()->Draw("hist same");
	
	legend_1->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = pictures_path + "Bdwave_JpsiK_2d_ProjectionX.C";
    pictures_path_png = pictures_path + "Bdwave_JpsiK_2d_ProjectionX.png";
    c_Bdwave_JpsiKX->SaveAs(pictures_path_cpp.c_str());
    c_Bdwave_JpsiKX->SaveAs(pictures_path_png.c_str());
    
    TCanvas * c_Bdwave_JpsiKY = new TCanvas("Bdwave_mass_JpsiKY");
	//JpsiK_2d_total->SetStats(kFALSE);
	
    hist_buffer = (TH1D*)JpsiK_2d_total->ProjectionY()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("m(J/#psi, h_{2}=K), GeV");
    hist_buffer->GetYaxis()->SetTitle("Events / 34 MeV");
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("hist");
    JpsiK_2d_Zc4200->ProjectionY()->Draw("hist same");
	JpsiK_2d_K1410_1->ProjectionY()->Draw("hist same");
	JpsiK_2d_K1430_0->ProjectionY()->Draw("hist same");
	JpsiK_2d_K1430_2->ProjectionY()->Draw("hist same");
	JpsiK_2d_K1680_1->ProjectionY()->Draw("hist same");
	JpsiK_2d_K1780_3->ProjectionY()->Draw("hist same");
	JpsiK_2d_K1950_0->ProjectionY()->Draw("hist same");
	JpsiK_2d_K1980_2->ProjectionY()->Draw("hist same");
	JpsiK_2d_K2045_4->ProjectionY()->Draw("hist same");
    JpsiK_2d_KNR_0->ProjectionY()->Draw("hist same");
	
	legend_1->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = pictures_path + "Bdwave_JpsiK_2d_ProjectionY.C";
    pictures_path_png = pictures_path + "Bdwave_JpsiK_2d_ProjectionY.png";
    c_Bdwave_JpsiKY->SaveAs(pictures_path_cpp.c_str());
    c_Bdwave_JpsiKY->SaveAs(pictures_path_png.c_str());
    
    float norm_Kpi = 1. / (Kpi_2d_K1410_1->Integral() + Kpi_2d_K1430_0->Integral() + Kpi_2d_K1430_2->Integral() + Kpi_2d_K1680_1->Integral() + Kpi_2d_K1780_3->Integral() + Kpi_2d_K1950_0->Integral() + Kpi_2d_K1980_2->Integral() + Kpi_2d_K2045_4->Integral() + Kpi_2d_Zc4200->Integral() + Kpi_2d_KNR_0->Integral()) * n_BD_to_JPSI_K_PI;
    
    TCanvas * c_Bdwave_KpiX = new TCanvas("Bdwave_mass_KpiX");
	//Kpi_2d_total->SetStats(kFALSE);
	Kpi_2d_total->SetLineColor(1);
	Kpi_2d_K1410_1->SetLineColor(2);
	Kpi_2d_K1430_0->SetLineColor(3);
	Kpi_2d_K1430_2->SetLineColor(4);
	Kpi_2d_K1680_1->SetLineColor(5);
	Kpi_2d_K1780_3->SetLineColor(6);
	Kpi_2d_K1950_0->SetLineColor(7);
	Kpi_2d_K1980_2->SetLineColor(8);
	Kpi_2d_K2045_4->SetLineColor(9);
    Kpi_2d_Zc4200->SetLineColor(46);
    Kpi_2d_Zc4200->SetFillColor(46);
    Kpi_2d_KNR_0->SetLineColor(47);

	Kpi_2d_total->SetLineWidth(3);
	Kpi_2d_K1410_1->SetLineWidth(2);
	Kpi_2d_K1430_0->SetLineWidth(2);
	Kpi_2d_K1430_2->SetLineWidth(2);
	Kpi_2d_K1680_1->SetLineWidth(2);
	Kpi_2d_K1780_3->SetLineWidth(2);
	Kpi_2d_K1950_0->SetLineWidth(2);
	Kpi_2d_K1980_2->SetLineWidth(2);
	Kpi_2d_K2045_4->SetLineWidth(2);
    Kpi_2d_KNR_0->SetLineWidth(2);
    Kpi_2d_Zc4200->SetLineWidth(2);

	Kpi_2d_K1410_1->Scale(norm_Kpi);
	Kpi_2d_K1430_0->Scale(norm_Kpi);
	Kpi_2d_K1430_2->Scale(norm_Kpi);
	Kpi_2d_K1680_1->Scale(norm_Kpi);
	Kpi_2d_K1780_3->Scale(norm_Kpi);
	Kpi_2d_K1950_0->Scale(norm_Kpi);
	Kpi_2d_K1980_2->Scale(norm_Kpi);
	Kpi_2d_K2045_4->Scale(norm_Kpi);
    Kpi_2d_KNR_0->Scale(norm_Kpi);
    Kpi_2d_Zc4200->Scale(norm_Kpi);
	
	hist_buffer = (TH1D*)Kpi_2d_total->ProjectionX()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("m(h_{1}=K h_{2}=#pi), GeV");
    hist_buffer->GetYaxis()->SetTitle("Events / 17 MeV");
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("hist");
    Kpi_2d_Zc4200->ProjectionX()->Draw("hist same");
	Kpi_2d_K1410_1->ProjectionX()->Draw("hist same");
	Kpi_2d_K1430_0->ProjectionX()->Draw("hist same");
	Kpi_2d_K1430_2->ProjectionX()->Draw("hist same");
	Kpi_2d_K1680_1->ProjectionX()->Draw("hist same");
	Kpi_2d_K1780_3->ProjectionX()->Draw("hist same");
	Kpi_2d_K1950_0->ProjectionX()->Draw("hist same");
	Kpi_2d_K1980_2->ProjectionX()->Draw("hist same");
	Kpi_2d_K2045_4->ProjectionX()->Draw("hist same");
    Kpi_2d_KNR_0->ProjectionX()->Draw("hist same");
	
	legend_1->Draw();
    t->DrawLatex(0.35,0.88, writetext00);
    t->DrawLatex(0.35,0.83, writetext01);
    pictures_path_cpp = pictures_path + "Bdwave_Kpi_2d_ProjectionX.C";
    pictures_path_png = pictures_path + "Bdwave_Kpi_2d_ProjectionX.png";
    c_Bdwave_KpiX->SaveAs(pictures_path_cpp.c_str());
    c_Bdwave_KpiX->SaveAs(pictures_path_png.c_str());
    
    TCanvas * c_Bdwave_KpiY = new TCanvas("Bdwave_mass_KpiY");
	//Kpi_2d_total->SetStats(kFALSE);
	
    hist_buffer = (TH1D*)Kpi_2d_total->ProjectionY()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("m(h_{1}=#pi, h_{2}=K), GeV");
    hist_buffer->GetYaxis()->SetTitle("Events / 17 MeV");
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("hist");
    Kpi_2d_Zc4200->ProjectionY()->Draw("hist same");
	Kpi_2d_K1410_1->ProjectionY()->Draw("hist same");
	Kpi_2d_K1430_0->ProjectionY()->Draw("hist same");
	Kpi_2d_K1430_2->ProjectionY()->Draw("hist same");
	Kpi_2d_K1680_1->ProjectionY()->Draw("hist same");
	Kpi_2d_K1780_3->ProjectionY()->Draw("hist same");
	Kpi_2d_K1950_0->ProjectionY()->Draw("hist same");
	Kpi_2d_K1980_2->ProjectionY()->Draw("hist same");
	Kpi_2d_K2045_4->ProjectionY()->Draw("hist same");
    Kpi_2d_KNR_0->ProjectionY()->Draw("hist same");
	
	legend_1->Draw();
    t->DrawLatex(0.35,0.88, writetext00);
    t->DrawLatex(0.35,0.83, writetext01);
    pictures_path_cpp = pictures_path + "Bdwave_Kpi_2d_ProjectionY.C";
    pictures_path_png = pictures_path + "Bdwave_Kpi_2d_ProjectionY.png";
    c_Bdwave_KpiY->SaveAs(pictures_path_cpp.c_str());
    c_Bdwave_KpiY->SaveAs(pictures_path_png.c_str());
    
//____ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo____
// Drawing pictures for control area of Bs -> Jpsi K K decays   
//____ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo____ 
    
    JpsiK_2d_control_bg->SetLineColor(1);
    JpsiK_2d_control_bg->SetFillColor(42);
    JpsiK_2d_control_BS_to_JPSI_K_K->SetFillColor(30);
    JpsiK_2d_control_BD_to_JPSI_K_PI->SetFillColor(7);
    JpsiK_2d_control_BD_to_JPSI_PI_PI->SetFillColor(8);
    JpsiK_2d_control_BS_to_JPSI_PI_PI->SetFillColor(46);
    JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    JpsiK_2d_control_LAMBDA0B_to_JPSI_P_PI->SetFillColor(95);
    JpsiK_2d_control_BD_to_JPSI_K_K->SetFillColor(12);
    JpsiK_2d_control_BS_to_JPSI_K_PI->SetFillColor(6);
    JpsiK_2d_control_data->SetMarkerStyle(kFullCircle);
    JpsiK_2d_control_data->SetMarkerSize(0.7);
    
    KK_1d_control_bg->SetLineColor(1);
    KK_1d_control_bg->SetFillColor(42);
    KK_1d_control_BS_to_JPSI_K_K->SetFillColor(30);
    KK_1d_control_BD_to_JPSI_K_PI->SetFillColor(7);
    KK_1d_control_BD_to_JPSI_PI_PI->SetFillColor(8);
    KK_1d_control_BS_to_JPSI_PI_PI->SetFillColor(46);
    KK_1d_control_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    KK_1d_control_LAMBDA0B_to_JPSI_P_PI->SetFillColor(95);
    KK_1d_control_BD_to_JPSI_K_K->SetFillColor(12);
    KK_1d_control_BS_to_JPSI_K_PI->SetFillColor(6);
    KK_1d_control_data->SetMarkerStyle(kFullCircle);
    KK_1d_control_data->SetMarkerSize(0.7);
    
    THStack hs_JpsiKX_control("hs_JpsiKX_control","JpsiK_signalX_control");
    
    hs_JpsiKX_control.Add(JpsiK_2d_control_bg->ProjectionX());
    hs_JpsiKX_control.Add(JpsiK_2d_control_BD_to_JPSI_PI_PI->ProjectionX());
    hs_JpsiKX_control.Add(JpsiK_2d_control_BS_to_JPSI_PI_PI->ProjectionX());
    hs_JpsiKX_control.Add(JpsiK_2d_control_BS_to_JPSI_K_K->ProjectionX());
    hs_JpsiKX_control.Add(JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K->ProjectionX());
    hs_JpsiKX_control.Add(JpsiK_2d_control_BD_to_JPSI_K_PI->ProjectionX());
    if (RARE_DECAYS)
    {
        hs_JpsiKX_control.Add(JpsiK_2d_control_LAMBDA0B_to_JPSI_P_PI->ProjectionX());
        hs_JpsiKX_control.Add(JpsiK_2d_control_BD_to_JPSI_K_K->ProjectionX());
        hs_JpsiKX_control.Add(JpsiK_2d_control_BS_to_JPSI_K_PI->ProjectionX());
    }
    TCanvas * c_JpsiKX_control = new TCanvas("JpsiK_mass_ProjectionX_control");
    JpsiK_2d_control_data->ProjectionX()->SetStats(kFALSE);
    JpsiK_2d_control_data->SetTitle("");
    
    hist_buffer = (TH1D*)JpsiK_2d_control_data->ProjectionX()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("m(J/#psi, h_{1}=K), GeV");
    hist_buffer->GetYaxis()->SetTitle("Events / 25.5 MeV");
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.2);
    
    hist_buffer->Draw("");
    hs_JpsiKX_control.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    legend_Jpsipi_X->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = pictures_path + "JpsiK_2d_control_ProjectionX.C";
    pictures_path_png = pictures_path + "JpsiK_2d_control_ProjectionX.png";
    c_JpsiKX_control->SaveAs(pictures_path_cpp.c_str());
    c_JpsiKX_control->SaveAs(pictures_path_png.c_str());
    
    THStack hs_JpsiKY_control("hs_JpsiKY_control","JpsiK_signalY_control");
    
    hs_JpsiKY_control.Add(JpsiK_2d_control_bg->ProjectionY());
    hs_JpsiKY_control.Add(JpsiK_2d_control_BD_to_JPSI_PI_PI->ProjectionY());
    hs_JpsiKY_control.Add(JpsiK_2d_control_BS_to_JPSI_PI_PI->ProjectionY());
    hs_JpsiKY_control.Add(JpsiK_2d_control_BS_to_JPSI_K_K->ProjectionY());
    hs_JpsiKY_control.Add(JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K->ProjectionY());
    hs_JpsiKY_control.Add(JpsiK_2d_control_BD_to_JPSI_K_PI->ProjectionY());
    if (RARE_DECAYS)
    {
        hs_JpsiKY_control.Add(JpsiK_2d_control_LAMBDA0B_to_JPSI_P_PI->ProjectionY());
        hs_JpsiKY_control.Add(JpsiK_2d_control_BD_to_JPSI_K_K->ProjectionY());
        hs_JpsiKY_control.Add(JpsiK_2d_control_BS_to_JPSI_K_PI->ProjectionY());
    }
    TCanvas * c_JpsiKY_control = new TCanvas("JpsiK_mass_ProjectionY_control");
    
    hist_buffer = (TH1D*)JpsiK_2d_control_data->ProjectionY()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("m(J/#psi, h_{2}=K), GeV");
    hist_buffer->GetYaxis()->SetTitle("Events / 25.5 MeV");
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.2);
    
    hist_buffer->Draw("");;
    hs_JpsiKY_control.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    legend_Jpsipi_X->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = pictures_path + "JpsiK_2d_control_ProjectionY.C";
    pictures_path_png = pictures_path + "JpsiK_2d_control_ProjectionY.png";
    c_JpsiKY_control->SaveAs(pictures_path_cpp.c_str());
    c_JpsiKY_control->SaveAs(pictures_path_png.c_str());
    
    THStack hs_KK_control("hs_KK_control","KK_control");
    
    hs_KK_control.Add(KK_1d_control_bg);
    hs_KK_control.Add(KK_1d_control_BD_to_JPSI_PI_PI);
    hs_KK_control.Add(KK_1d_control_BS_to_JPSI_PI_PI);
    hs_KK_control.Add(KK_1d_control_BS_to_JPSI_K_K);
    hs_KK_control.Add(KK_1d_control_LAMBDA0B_to_JPSI_P_K);
    hs_KK_control.Add(KK_1d_control_BD_to_JPSI_K_PI);
    if (RARE_DECAYS)
    {
        hs_KK_control.Add(KK_1d_control_LAMBDA0B_to_JPSI_P_PI);
        hs_KK_control.Add(KK_1d_control_BD_to_JPSI_K_K);
        hs_KK_control.Add(KK_1d_control_BS_to_JPSI_K_PI);
    }
    TCanvas * c_KK_control = new TCanvas("KK_mass_control");
    KK_1d_control_data->SetStats(kFALSE);
    KK_1d_control_data->GetXaxis()->SetTitle("m(h_{1}=K, h_{2}=K), GeV");
    KK_1d_control_data->GetYaxis()->SetTitle("Events / 10 MeV");
    KK_1d_control_data->GetYaxis()->SetRangeUser(0., KK_1d_control_data->GetMaximum() * 1.1);
    KK_1d_control_data->SetTitle("");
    KK_1d_control_data->Draw("");
    hs_KK_control.Draw("hist same");
    KK_1d_control_data->Draw("same");
    KK_1d_control_data->Draw("same axis");
    legend_Jpsipi_X->Draw();
    t->DrawLatex(0.35,0.88, writetext00);
    t->DrawLatex(0.35,0.83, writetext01);
    pictures_path_cpp = pictures_path + "KK_1d_control.C";
    pictures_path_png = pictures_path + "KK_1d_control.png";
    c_KK_control->SaveAs(pictures_path_cpp.c_str());
    c_KK_control->SaveAs(pictures_path_png.c_str());
    
    TH2 *JpsiK_mass_phi1680_1 = new TH2D("JpsiK_mass_phi1680_1", "JpsiK_mass_phi1680_1", 50, 3.575, 4.850, 50, 3.575, 4.850);
    TH2 *JpsiK_mass_f1525_2 = new TH2D("JpsiK_mass_f1525_2", "JpsiK_mass_f1525_2", 50, 3.575, 4.850, 50, 3.575, 4.850);
    TH2 *JpsiK_mass_f1640_2 = new TH2D("JpsiK_mass_f1640_2", "JpsiK_mass_f1640_2", 50, 3.575, 4.850, 50, 3.575, 4.850);
    TH2 *JpsiK_mass_f1750_2 = new TH2D("JpsiK_mass_f1750_2", "JpsiK_mass_f1750_2", 50, 3.575, 4.850, 50, 3.575, 4.850);
    TH2 *JpsiK_mass_f1950_2 = new TH2D("JpsiK_mass_f1950_2", "JpsiK_mass_f1950_2", 50, 3.575, 4.850, 50, 3.575, 4.850);
    TH2 *JpsiK_mass_NR = new TH2D("JpsiK_mass_NR", "JpsiK_mass_NR", 50, 3.575, 4.850, 50, 3.575, 4.850);
    TH2 *JpsiK_mass_sum = new TH2D("JpsiK_mass_sum_control", "JpsiK_mass_sum_control", 50, 3.575, 4.850, 50, 3.575, 4.850);
    
    TH1 *KK_mass_phi1680_1 = new TH1D("KK_mass_phi1680_1", "KK_mass_phi1680_1", 62, 1.680, 2.300);
    TH1 *KK_mass_f1525_2 = new TH1D("KK_mass_f1525_2", "KK_mass_f1525_2", 62, 1.680, 2.300);
    TH1 *KK_mass_f1640_2 = new TH1D("KK_mass_f1640_2", "KK_mass_f1640_2", 62, 1.680, 2.300);
    TH1 *KK_mass_f1750_2 = new TH1D("KK_mass_f1750_2", "KK_mass_f1750_2", 62, 1.680, 2.300);
    TH1 *KK_mass_f1950_2 = new TH1D("KK_mass_f1950_2", "KK_mass_f1950_2", 62, 1.680, 2.300);
    TH1 *KK_mass_NR = new TH1D("KK_mass_NR", "KK_mass_NR", 62, 1.680, 2.300);
    TH1 *KK_mass_sum = new TH1D("KK_mass_sum", "KK_mass_sum", 62, 1.680, 2.300);
    
    for (int i = 0; i < phs_Bs; i++)
    {
		if (control_a_BsKK[i]) {
        float w_phi1680_1 = 0., w_f1525_2 = 0., w_f1640_2 = 0., w_f1750_2 = 0., w_f1950_2 = 0., w_NR = 0.;
        for(int i3 = 0; i3 < 2; i3++)
		{
			float hmu = 2*i3 - 1.0;
			
            w_phi1680_1 = pow(abs(Hamplitudephi1(KK_truth_BsKK[i] / 1000., 5.36688, 1.680, 0.150, std::complex<float>(phi1680_1_Bw1_amp * cos(phi1680_1_Bw1_phi), phi1680_1_Bw1_amp * sin(phi1680_1_Bw1_phi)), std::complex<float>(phi1680_1_Bw2_amp * cos(phi1680_1_Bw2_phi), phi1680_1_Bw2_amp * sin(phi1680_1_Bw2_phi)), std::complex<float>(phi1680_1_Bw3_amp * cos(phi1680_1_Bw3_phi), phi1680_1_Bw3_amp * sin(phi1680_1_Bw3_phi)), hmu, thetaF_a[i], theta_psi_F_a[i], phi_mu_F_a[i], phi_KF_a[i])), 2.);
            w_f1525_2 = pow(abs(Hamplitudef2(KK_truth_BsKK[i] / 1000., 5.36688, 1.525, 0.073, std::complex<float>(f1525_2_Bw1_amp * cos(f1525_2_Bw1_phi), f1525_2_Bw1_amp * sin(f1525_2_Bw1_phi)), std::complex<float>(f1525_2_Bw2_amp * cos(f1525_2_Bw2_phi), f1525_2_Bw2_amp * sin(f1525_2_Bw2_phi)), std::complex<float>(f1525_2_Bw3_amp * cos(f1525_2_Bw3_phi), f1525_2_Bw3_amp * sin(f1525_2_Bw3_phi)), hmu, thetaF_a[i], theta_psi_F_a[i], phi_mu_F_a[i], phi_KF_a[i])), 2.);
            w_f1640_2 = pow(abs(Hamplitudef2(KK_truth_BsKK[i] / 1000., 5.36688, 1.639, 0.099, std::complex<float>(f1640_2_Bw1_amp * cos(f1640_2_Bw1_phi), f1640_2_Bw1_amp * sin(f1640_2_Bw1_phi)), std::complex<float>(f1640_2_Bw2_amp * cos(f1640_2_Bw2_phi), f1640_2_Bw2_amp * sin(f1640_2_Bw1_phi)), std::complex<float>(f1640_2_Bw3_amp * cos(f1640_2_Bw3_phi), f1640_2_Bw3_amp * sin(f1640_2_Bw3_phi)), hmu, thetaF_a[i], theta_psi_F_a[i], phi_mu_F_a[i], phi_KF_a[i])), 2.);
            w_f1750_2 = pow(abs(Hamplitudef2(KK_truth_BsKK[i] / 1000., 5.36688, 1.750, 0.099, std::complex<float>(f1750_2_Bw1_amp * cos(f1750_2_Bw1_phi), f1750_2_Bw1_amp * sin(f1750_2_Bw1_phi)), std::complex<float>(f1750_2_Bw2_amp * cos(f1750_2_Bw2_phi), f1750_2_Bw2_amp * sin(f1750_2_Bw2_phi)), std::complex<float>(f1750_2_Bw3_amp * cos(f1750_2_Bw3_phi), f1750_2_Bw3_amp * sin(f1750_2_Bw3_phi)), hmu, thetaF_a[i], theta_psi_F_a[i], phi_mu_F_a[i], phi_KF_a[i])), 2.);
            w_f1950_2 = pow(abs(Hamplitudef2(KK_truth_BsKK[i] / 1000., 5.36688, 1.944, 0.472, std::complex<float>(f1950_2_Bw1_amp * cos(f1950_2_Bw1_phi), f1950_2_Bw1_amp * sin(f1950_2_Bw1_phi)), std::complex<float>(f1950_2_Bw2_amp * cos(f1950_2_Bw2_phi), f1950_2_Bw2_amp * sin(f1950_2_Bw2_phi)), std::complex<float>(f1950_2_Bw3_amp * cos(f1950_2_Bw3_phi), f1950_2_Bw3_amp * sin(f1950_2_Bw3_phi)), hmu, thetaF_a[i], theta_psi_F_a[i], phi_mu_F_a[i], phi_KF_a[i])), 2.);
            w_NR = pow(abs(std::complex<float>(NR_Bw1_amp * cos(NR_Bw1_phi), NR_Bw1_amp * sin(NR_Bw1_phi))), 2.);
            //std::complex<float> test_c = std::complex<float>(X1Bs1_amp * cos(X1Bs1_phi), X1Bs1_amp * sin(X1Bs1_phi));
            //std::complex<float>(X1Bs2_amp * cos(X1Bs2_phi), X1Bs2_amp * sin(X1Bs2_phi))
    	}
    	
    	JpsiK_mass_phi1680_1->Fill(JpsiK1_mass_BsKK[i], JpsiK2_mass_BsKK[i], w_phi1680_1 / 3. * w_dimuon_a_BsKK[i]);
        JpsiK_mass_f1525_2->Fill(JpsiK1_mass_BsKK[i], JpsiK2_mass_BsKK[i], w_f1525_2 / 3. * w_dimuon_a_BsKK[i]);
        JpsiK_mass_f1640_2->Fill(JpsiK1_mass_BsKK[i], JpsiK2_mass_BsKK[i], w_f1640_2 / 3. * w_dimuon_a_BsKK[i]);
        JpsiK_mass_f1750_2->Fill(JpsiK1_mass_BsKK[i], JpsiK2_mass_BsKK[i], w_f1750_2 / 3. * w_dimuon_a_BsKK[i]);
        JpsiK_mass_f1950_2->Fill(JpsiK1_mass_BsKK[i], JpsiK2_mass_BsKK[i], w_f1950_2 / 3. * w_dimuon_a_BsKK[i]);
        JpsiK_mass_NR->Fill(JpsiK1_mass_BsKK[i], JpsiK2_mass_BsKK[i], w_NR / 3. * w_dimuon_a_BsKK[i]);
    
        KK_mass_phi1680_1->Fill(KK_mass_BsKK[i], w_phi1680_1 / 3. * w_dimuon_a_BsKK[i]);
        KK_mass_f1525_2->Fill(KK_mass_BsKK[i], w_f1525_2 / 3. * w_dimuon_a_BsKK[i]);
        KK_mass_f1640_2->Fill(KK_mass_BsKK[i], w_f1640_2 / 3. * w_dimuon_a_BsKK[i]);
        KK_mass_f1750_2->Fill(KK_mass_BsKK[i], w_f1750_2 / 3. * w_dimuon_a_BsKK[i]);
        KK_mass_f1950_2->Fill(KK_mass_BsKK[i], w_f1950_2 / 3. * w_dimuon_a_BsKK[i]);
        KK_mass_NR->Fill(KK_mass_BsKK[i], w_NR / 3. * w_dimuon_a_BsKK[i]);}
	}
	
	float norm_JpsiK_control = 1. / (JpsiK_mass_phi1680_1->Integral() + JpsiK_mass_f1525_2->Integral() + JpsiK_mass_f1640_2->Integral() + JpsiK_mass_f1750_2->Integral() + JpsiK_mass_f1950_2->Integral() + JpsiK_mass_NR->Integral()) * n_control_BS_to_JPSI_K_K;
    
    TCanvas * c_Bswave_JpsiKX = new TCanvas("Bswave_mass_JpsiKX");
	//JpsiK_2d_total->SetStats(kFALSE);
	JpsiK_2d_control_total->SetLineColor(1);
	JpsiK_mass_phi1680_1->SetLineColor(2);
	JpsiK_mass_f1525_2->SetLineColor(3);
	JpsiK_mass_f1640_2->SetLineColor(4);
	JpsiK_mass_f1750_2->SetLineColor(5);
	JpsiK_mass_f1950_2->SetLineColor(6);
	JpsiK_mass_NR->SetLineColor(7);

	JpsiK_2d_control_total->SetLineWidth(3);
	JpsiK_mass_phi1680_1->SetLineWidth(2);
	JpsiK_mass_f1525_2->SetLineWidth(2);
	JpsiK_mass_f1640_2->SetLineWidth(2);
	JpsiK_mass_f1750_2->SetLineWidth(2);
	JpsiK_mass_f1950_2->SetLineWidth(2);
	JpsiK_mass_NR->SetLineWidth(2);

	//JpsiK_2d_control_total->Scale(norm_JpsiK_control);
	JpsiK_mass_phi1680_1->Scale(norm_JpsiK_control);
	JpsiK_mass_f1525_2->Scale(norm_JpsiK_control);
	JpsiK_mass_f1640_2->Scale(norm_JpsiK_control);
	JpsiK_mass_f1750_2->Scale(norm_JpsiK_control);
	JpsiK_mass_f1950_2->Scale(norm_JpsiK_control);
	JpsiK_mass_NR->Scale(norm_JpsiK_control);
	
    hist_buffer = (TH1D*)JpsiK_2d_control_total->ProjectionX()->Clone("hist_buffer");
    
	hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("m(J/#psi, h_{1}=K), GeV");
    hist_buffer->GetYaxis()->SetTitle("Events / 25.5 MeV");
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("hist");
	JpsiK_mass_phi1680_1->ProjectionX()->Draw("hist same");
	JpsiK_mass_f1525_2->ProjectionX()->Draw("hist same");
	JpsiK_mass_f1640_2->ProjectionX()->Draw("hist same");
	JpsiK_mass_f1750_2->ProjectionX()->Draw("hist same");
	JpsiK_mass_f1950_2->ProjectionX()->Draw("hist same");
	JpsiK_mass_NR->ProjectionX()->Draw("hist same");
    
    TLegend *legend_control = new TLegend(.60,.60,.90,.90);
    legend_control->AddEntry(JpsiK_2d_control_total,"total");
	legend_control->AddEntry(JpsiK_mass_phi1680_1,"#phi_{1}(1680)");
	legend_control->AddEntry(JpsiK_mass_f1525_2,"f_{2}(1525)");
	legend_control->AddEntry(JpsiK_mass_f1640_2,"f_{2}(1640)");
	legend_control->AddEntry(JpsiK_mass_f1750_2,"f_{2}(1750)");
	legend_control->AddEntry(JpsiK_mass_f1950_2,"f_{2}(1950)");
	legend_control->AddEntry(JpsiK_mass_NR,"Non resonant");
	
	legend_control->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = pictures_path + "Bswave_JpsiK_2d_ProjectionX.C";
    pictures_path_png = pictures_path + "Bswave_JpsiK_2d_ProjectionX.png";
    c_Bswave_JpsiKX->SaveAs(pictures_path_cpp.c_str());
    c_Bswave_JpsiKX->SaveAs(pictures_path_png.c_str());
    
    TCanvas * c_Bswave_JpsiKY = new TCanvas("Bswave_mass_JpsiKY");
	//JpsiK_2d_total->SetStats(kFALSE);
    
    hist_buffer = (TH1D*)JpsiK_2d_control_total->ProjectionY()->Clone("hist_buffer");
	
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("m(J/#psi, h_{2}=K), GeV");
    hist_buffer->GetYaxis()->SetTitle("Events / 25.5 MeV");
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("hist");
	JpsiK_mass_phi1680_1->ProjectionY()->Draw("hist same");
	JpsiK_mass_f1525_2->ProjectionY()->Draw("hist same");
	JpsiK_mass_f1640_2->ProjectionY()->Draw("hist same");
	JpsiK_mass_f1750_2->ProjectionY()->Draw("hist same");
	JpsiK_mass_f1950_2->ProjectionY()->Draw("hist same");
	JpsiK_mass_NR->ProjectionY()->Draw("hist same");
	
	legend_control->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = pictures_path + "Bswave_JpsiK_2d_ProjectionY.C";
    pictures_path_png = pictures_path + "Bswave_JpsiK_2d_ProjectionY.png";
    c_Bswave_JpsiKY->SaveAs(pictures_path_cpp.c_str());
    c_Bswave_JpsiKY->SaveAs(pictures_path_png.c_str());
    
    float norm_KK_control = 1. / (KK_mass_phi1680_1->Integral() + KK_mass_f1525_2->Integral() + KK_mass_f1640_2->Integral() + KK_mass_f1750_2->Integral() + KK_mass_f1950_2->Integral() + KK_mass_NR->Integral()) * n_control_BS_to_JPSI_K_K;
    
    TCanvas * c_Bswave_KK = new TCanvas("Bswave_mass_KK");
	//JpsiK_2d_total->SetStats(kFALSE);
	KK_1d_control_total->SetLineColor(1);
	KK_mass_phi1680_1->SetLineColor(2);
	KK_mass_f1525_2->SetLineColor(3);
	KK_mass_f1640_2->SetLineColor(4);
	KK_mass_f1750_2->SetLineColor(5);
	KK_mass_f1950_2->SetLineColor(6);
	KK_mass_NR->SetLineColor(7);

	KK_1d_control_total->SetLineWidth(3);
	KK_mass_phi1680_1->SetLineWidth(2);
	KK_mass_f1525_2->SetLineWidth(2);
	KK_mass_f1640_2->SetLineWidth(2);
	KK_mass_f1750_2->SetLineWidth(2);
	KK_mass_f1950_2->SetLineWidth(2);
	KK_mass_NR->SetLineWidth(2);

	//KK_1d_control_total->Scale(norm_KK_control);
	KK_mass_phi1680_1->Scale(norm_KK_control);
	KK_mass_f1525_2->Scale(norm_KK_control);
	KK_mass_f1640_2->Scale(norm_KK_control);
	KK_mass_f1750_2->Scale(norm_KK_control);
	KK_mass_f1950_2->Scale(norm_KK_control);
	KK_mass_NR->Scale(norm_KK_control);
    
    KK_1d_control_total->SetStats(kFALSE);
    KK_1d_control_total->SetTitle("");
    KK_1d_control_total->GetXaxis()->SetTitle("m(h_{1}=K, h_{2}=K), GeV");
    KK_1d_control_total->GetYaxis()->SetTitle("Events / 10 MeV");
    KK_1d_control_total->GetYaxis()->SetRangeUser(0., KK_1d_control_total->GetMaximum() * 1.1);
    
    KK_1d_control_total->Draw("hist");
	KK_mass_phi1680_1->Draw("hist same");
	KK_mass_f1525_2->Draw("hist same");
	KK_mass_f1640_2->Draw("hist same");
	KK_mass_f1750_2->Draw("hist same");
	KK_mass_f1950_2->Draw("hist same");
	KK_mass_NR->Draw("hist same");
	
	legend_control->Draw();
    t->DrawLatex(0.3,0.88, writetext00);
    t->DrawLatex(0.3,0.83, writetext01);
    pictures_path_cpp = pictures_path + "Bswave_KK_1d.C";
    pictures_path_png = pictures_path + "Bswave_KK_1d.png";
    c_Bswave_KK->SaveAs(pictures_path_cpp.c_str());
    c_Bswave_KK->SaveAs(pictures_path_png.c_str());
    
//____ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo____
// Drawing pictures for control area of Lb -> Jpsi p K decays   
//____ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo____ 
    
    Jpsip_2d_controlLb_bg->SetLineColor(1);
    Jpsip_2d_controlLb_bg->SetFillColor(42);
    Jpsip_2d_controlLb_BS_to_JPSI_K_K->SetFillColor(30);
    Jpsip_2d_controlLb_BD_to_JPSI_K_PI->SetFillColor(7);
    Jpsip_2d_controlLb_BD_to_JPSI_PI_PI->SetFillColor(8);
    Jpsip_2d_controlLb_BS_to_JPSI_PI_PI->SetFillColor(46);
    Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->SetFillColor(95);
    Jpsip_2d_controlLb_BD_to_JPSI_K_K->SetFillColor(12);
    Jpsip_2d_controlLb_BS_to_JPSI_K_PI->SetFillColor(6);
    Jpsip_2d_controlLb_data->SetMarkerStyle(kFullCircle);
    Jpsip_2d_controlLb_data->SetMarkerSize(0.7);
    
    JpsiK_2d_controlLb_bg->SetLineColor(1);
    JpsiK_2d_controlLb_bg->SetFillColor(42);
    JpsiK_2d_controlLb_BS_to_JPSI_K_K->SetFillColor(30);
    JpsiK_2d_controlLb_BD_to_JPSI_K_PI->SetFillColor(7);
    JpsiK_2d_controlLb_BD_to_JPSI_PI_PI->SetFillColor(8);
    JpsiK_2d_controlLb_BS_to_JPSI_PI_PI->SetFillColor(46);
    JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->SetFillColor(95);
    JpsiK_2d_controlLb_BD_to_JPSI_K_K->SetFillColor(12);
    JpsiK_2d_controlLb_BS_to_JPSI_K_PI->SetFillColor(6);
    JpsiK_2d_controlLb_data->SetMarkerStyle(kFullCircle);
    JpsiK_2d_controlLb_data->SetMarkerSize(0.7);
    
    pK_2d_controlLb_bg->SetLineColor(1);
    pK_2d_controlLb_bg->SetFillColor(42);
    pK_2d_controlLb_BS_to_JPSI_K_K->SetFillColor(30);
    pK_2d_controlLb_BD_to_JPSI_K_PI->SetFillColor(7);
    pK_2d_controlLb_BD_to_JPSI_PI_PI->SetFillColor(8);
    pK_2d_controlLb_BS_to_JPSI_PI_PI->SetFillColor(46);
    pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    pK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->SetFillColor(95);
    pK_2d_controlLb_BD_to_JPSI_K_K->SetFillColor(12);
    pK_2d_controlLb_BS_to_JPSI_K_PI->SetFillColor(6);
    pK_2d_controlLb_data->SetMarkerStyle(kFullCircle);
    pK_2d_controlLb_data->SetMarkerSize(0.7);
    
    TLegend *legend_Jpsip_X = new TLegend(.60,.60,.90,.90);
    legend_Jpsip_X->AddEntry(Jpsip_2d_controlLb_data,"data");
	legend_Jpsip_X->AddEntry(Jpsip_2d_controlLb_BD_to_JPSI_K_PI,"B_{d} -> J/#psi K #pi");
	legend_Jpsip_X->AddEntry(Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K,"#Lambda_{b} -> J/#psi p K");
	legend_Jpsip_X->AddEntry(Jpsip_2d_controlLb_BS_to_JPSI_K_K,"B_{s} -> J/#psi K K");
	legend_Jpsip_X->AddEntry(Jpsip_2d_controlLb_BS_to_JPSI_PI_PI,"B_{s} -> J/#psi #pi #pi");
	legend_Jpsip_X->AddEntry(Jpsip_2d_controlLb_BD_to_JPSI_PI_PI,"B_{d} -> J/#psi #pi #pi");
    if (RARE_DECAYS)
    {
        legend_Jpsip_X->AddEntry(Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_PI,"#Lambda_{b} -> J/#psi p #pi");
        legend_Jpsip_X->AddEntry(Jpsip_2d_controlLb_BD_to_JPSI_K_K,"B_{d} -> J/#psi K K");
        legend_Jpsip_X->AddEntry(Jpsip_2d_controlLb_BS_to_JPSI_K_PI,"B_{s} -> J/#psi K #pi");
    }
	legend_Jpsip_X->AddEntry(Jpsip_2d_controlLb_bg,"comb background");
    
    THStack hs_Jpsip_controlLbX("hs_Jpsip_controlLbX","Jpsip_controlLbX");
    
    hs_Jpsip_controlLbX.Add(Jpsip_2d_controlLb_bg->ProjectionX());
    hs_Jpsip_controlLbX.Add(Jpsip_2d_controlLb_BD_to_JPSI_PI_PI->ProjectionX());
    hs_Jpsip_controlLbX.Add(Jpsip_2d_controlLb_BS_to_JPSI_PI_PI->ProjectionX());
    hs_Jpsip_controlLbX.Add(Jpsip_2d_controlLb_BS_to_JPSI_K_K->ProjectionX());
    hs_Jpsip_controlLbX.Add(Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K->ProjectionX());
    hs_Jpsip_controlLbX.Add(Jpsip_2d_controlLb_BD_to_JPSI_K_PI->ProjectionX());
    if (RARE_DECAYS)
    {
        hs_Jpsip_controlLbX.Add(Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->ProjectionX());
        hs_Jpsip_controlLbX.Add(Jpsip_2d_controlLb_BD_to_JPSI_K_K->ProjectionX());
        hs_Jpsip_controlLbX.Add(Jpsip_2d_controlLb_BS_to_JPSI_K_PI->ProjectionX());
    }
    TCanvas * c_Jpsip_controlLbX = new TCanvas("jpsip_mass_ProjectionX");
    Jpsip_2d_controlLb_data->ProjectionX()->SetStats(kFALSE);
    Jpsip_2d_controlLb_data->ProjectionX()->GetXaxis()->SetTitle("GeV");
    Jpsip_2d_controlLb_data->ProjectionX()->GetYaxis()->SetTitle("Events / 25 MeV");
    Jpsip_2d_controlLb_data->ProjectionX()->GetYaxis()->SetRangeUser(0., 1700.);
    Jpsip_2d_controlLb_data->SetTitle("");
    Jpsip_2d_controlLb_data->ProjectionX()->Draw("");
    hs_Jpsip_controlLbX.Draw("hist same");
    Jpsip_2d_controlLb_data->ProjectionX()->Draw("same");
    Jpsip_2d_controlLb_data->ProjectionX()->Draw("same axis");
    legend_Jpsip_X->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = pictures_path + "Jpsip_2d_controlLb_ProjectionX.C";
    pictures_path_png = pictures_path + "Jpsip_2d_controlLb_ProjectionX.png";
    c_Jpsip_controlLbX->SaveAs(pictures_path_cpp.c_str());
    c_Jpsip_controlLbX->SaveAs(pictures_path_png.c_str());
    
    THStack hs_Jpsip_controlLbY("hs_Jpsip_controlLbY","Jpsip_controlLbY");
    
    hs_Jpsip_controlLbY.Add(Jpsip_2d_controlLb_bg->ProjectionY());
    hs_Jpsip_controlLbY.Add(Jpsip_2d_controlLb_BD_to_JPSI_PI_PI->ProjectionY());
    hs_Jpsip_controlLbY.Add(Jpsip_2d_controlLb_BS_to_JPSI_PI_PI->ProjectionY());
    hs_Jpsip_controlLbY.Add(Jpsip_2d_controlLb_BS_to_JPSI_K_K->ProjectionY());
    hs_Jpsip_controlLbY.Add(Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K->ProjectionY());
    hs_Jpsip_controlLbY.Add(Jpsip_2d_controlLb_BD_to_JPSI_K_PI->ProjectionY());
    if (RARE_DECAYS)
    {
        hs_Jpsip_controlLbY.Add(Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->ProjectionY());
        hs_Jpsip_controlLbY.Add(Jpsip_2d_controlLb_BD_to_JPSI_K_K->ProjectionY());
        hs_Jpsip_controlLbY.Add(Jpsip_2d_controlLb_BS_to_JPSI_K_PI->ProjectionY());
    }
    TCanvas * c_Jpsip_controlLbY = new TCanvas("jpsip_mass_ProjectionY");
    Jpsip_2d_controlLb_data->ProjectionY()->SetStats(kFALSE);
    Jpsip_2d_controlLb_data->ProjectionY()->GetYaxis()->SetTitle("GeV");
    Jpsip_2d_controlLb_data->ProjectionY()->GetYaxis()->SetTitle("Events / 25 MeV");
    Jpsip_2d_controlLb_data->ProjectionY()->GetYaxis()->SetRangeUser(0., 1700.);
    Jpsip_2d_controlLb_data->ProjectionY()->Draw("");
    hs_Jpsip_controlLbY.Draw("hist same");
    Jpsip_2d_controlLb_data->ProjectionY()->Draw("same");
    Jpsip_2d_controlLb_data->ProjectionY()->Draw("same axis");
    legend_Jpsip_X->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = pictures_path + "Jpsip_2d_controlLb_ProjectionY.C";
    pictures_path_png = pictures_path + "Jpsip_2d_controlLb_ProjectionY.png";
    c_Jpsip_controlLbY->SaveAs(pictures_path_cpp.c_str());
    c_Jpsip_controlLbY->SaveAs(pictures_path_png.c_str());
    
    THStack hs_JpsiK_controlLbX("hs_JpsiK_controlLbX","JpsiK_controlLbX");
    
    hs_JpsiK_controlLbX.Add(JpsiK_2d_controlLb_bg->ProjectionX());
    hs_JpsiK_controlLbX.Add(JpsiK_2d_controlLb_BD_to_JPSI_PI_PI->ProjectionX());
    hs_JpsiK_controlLbX.Add(JpsiK_2d_controlLb_BS_to_JPSI_PI_PI->ProjectionX());
    hs_JpsiK_controlLbX.Add(JpsiK_2d_controlLb_BS_to_JPSI_K_K->ProjectionX());
    hs_JpsiK_controlLbX.Add(JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->ProjectionX());
    hs_JpsiK_controlLbX.Add(JpsiK_2d_controlLb_BD_to_JPSI_K_PI->ProjectionX());
    if (RARE_DECAYS)
    {
        hs_JpsiK_controlLbX.Add(JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->ProjectionX());
        hs_JpsiK_controlLbX.Add(JpsiK_2d_controlLb_BD_to_JPSI_K_K->ProjectionX());
        hs_JpsiK_controlLbX.Add(JpsiK_2d_controlLb_BS_to_JPSI_K_PI->ProjectionX());
    }
    TCanvas * c_JpsiK_controlLbX = new TCanvas("jpsip_mass_ProjectionX");
    JpsiK_2d_controlLb_data->ProjectionX()->SetStats(kFALSE);
    JpsiK_2d_controlLb_data->ProjectionX()->GetXaxis()->SetTitle("GeV");
    JpsiK_2d_controlLb_data->ProjectionX()->GetYaxis()->SetTitle("Events / 25 MeV");
    JpsiK_2d_controlLb_data->ProjectionX()->GetYaxis()->SetRangeUser(0., 1700.);
    JpsiK_2d_controlLb_data->SetTitle("");
    JpsiK_2d_controlLb_data->ProjectionX()->Draw("");
    hs_JpsiK_controlLbX.Draw("hist same");
    JpsiK_2d_controlLb_data->ProjectionX()->Draw("same");
    JpsiK_2d_controlLb_data->ProjectionX()->Draw("same axis");
    legend_Jpsip_X->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = pictures_path + "JpsiK_2d_controlLb_ProjectionX.C";
    pictures_path_png = pictures_path + "JpsiK_2d_controlLb_ProjectionX.png";
    c_JpsiK_controlLbX->SaveAs(pictures_path_cpp.c_str());
    c_JpsiK_controlLbX->SaveAs(pictures_path_png.c_str());
    
    THStack hs_JpsiK_controlLbY("hs_JpsiK_controlLbY","JpsiK_controlLbY");
    
    hs_JpsiK_controlLbY.Add(JpsiK_2d_controlLb_bg->ProjectionY());
    hs_JpsiK_controlLbY.Add(JpsiK_2d_controlLb_BD_to_JPSI_PI_PI->ProjectionY());
    hs_JpsiK_controlLbY.Add(JpsiK_2d_controlLb_BS_to_JPSI_PI_PI->ProjectionY());
    hs_JpsiK_controlLbY.Add(JpsiK_2d_controlLb_BS_to_JPSI_K_K->ProjectionY());
    hs_JpsiK_controlLbY.Add(JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->ProjectionY());
    hs_JpsiK_controlLbY.Add(JpsiK_2d_controlLb_BD_to_JPSI_K_PI->ProjectionY());
    if (RARE_DECAYS)
    {
        hs_JpsiK_controlLbY.Add(JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->ProjectionY());
        hs_JpsiK_controlLbY.Add(JpsiK_2d_controlLb_BD_to_JPSI_K_K->ProjectionY());
        hs_JpsiK_controlLbY.Add(JpsiK_2d_controlLb_BS_to_JPSI_K_PI->ProjectionY());
    }
    TCanvas * c_JpsiK_controlLbY = new TCanvas("jpsip_mass_ProjectionY");
    JpsiK_2d_controlLb_data->ProjectionY()->SetStats(kFALSE);
    JpsiK_2d_controlLb_data->ProjectionY()->GetYaxis()->SetTitle("GeV");
    JpsiK_2d_controlLb_data->ProjectionY()->GetYaxis()->SetTitle("Events / 25 MeV");
    JpsiK_2d_controlLb_data->ProjectionY()->GetYaxis()->SetRangeUser(0., 1700.);
    JpsiK_2d_controlLb_data->ProjectionY()->Draw("");
    hs_JpsiK_controlLbY.Draw("hist same");
    JpsiK_2d_controlLb_data->ProjectionY()->Draw("same");
    JpsiK_2d_controlLb_data->ProjectionY()->Draw("same axis");
    legend_Jpsip_X->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = pictures_path + "JpsiK_2d_controlLb_ProjectionY.C";
    pictures_path_png = pictures_path + "JpsiK_2d_controlLb_ProjectionY.png";
    c_JpsiK_controlLbY->SaveAs(pictures_path_cpp.c_str());
    c_JpsiK_controlLbY->SaveAs(pictures_path_png.c_str());
    
    THStack hs_pK_controlLbX("hs_pK_controlLbX","pK_controlLbX");
    
    hs_pK_controlLbX.Add(pK_2d_controlLb_bg->ProjectionX());
    hs_pK_controlLbX.Add(pK_2d_controlLb_BD_to_JPSI_PI_PI->ProjectionX());
    hs_pK_controlLbX.Add(pK_2d_controlLb_BS_to_JPSI_PI_PI->ProjectionX());
    hs_pK_controlLbX.Add(pK_2d_controlLb_BS_to_JPSI_K_K->ProjectionX());
    hs_pK_controlLbX.Add(pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->ProjectionX());
    hs_pK_controlLbX.Add(pK_2d_controlLb_BD_to_JPSI_K_PI->ProjectionX());
    if (RARE_DECAYS)
    {
        hs_pK_controlLbX.Add(pK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->ProjectionX());
        hs_pK_controlLbX.Add(pK_2d_controlLb_BD_to_JPSI_K_K->ProjectionX());
        hs_pK_controlLbX.Add(pK_2d_controlLb_BS_to_JPSI_K_PI->ProjectionX());
    }
    TCanvas * c_pK_controlLbX = new TCanvas("jpsip_mass_ProjectionX");
    pK_2d_controlLb_data->ProjectionX()->SetStats(kFALSE);
    pK_2d_controlLb_data->ProjectionX()->GetXaxis()->SetTitle("GeV");
    pK_2d_controlLb_data->ProjectionX()->GetYaxis()->SetTitle("Events / 16 MeV");
    pK_2d_controlLb_data->ProjectionX()->GetYaxis()->SetRangeUser(0., 1700.);
    pK_2d_controlLb_data->SetTitle("");
    pK_2d_controlLb_data->ProjectionX()->Draw("");
    hs_pK_controlLbX.Draw("hist same");
    pK_2d_controlLb_data->ProjectionX()->Draw("same");
    pK_2d_controlLb_data->ProjectionX()->Draw("same axis");
    legend_Jpsip_X->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = pictures_path + "pK_2d_controlLb_ProjectionX.C";
    pictures_path_png = pictures_path + "pK_2d_controlLb_ProjectionX.png";
    c_pK_controlLbX->SaveAs(pictures_path_cpp.c_str());
    c_pK_controlLbX->SaveAs(pictures_path_png.c_str());
    
    THStack hs_pK_controlLbY("hs_pK_controlLbY","pK_controlLbY");
    
    hs_pK_controlLbY.Add(pK_2d_controlLb_bg->ProjectionY());
    hs_pK_controlLbY.Add(pK_2d_controlLb_BD_to_JPSI_PI_PI->ProjectionY());
    hs_pK_controlLbY.Add(pK_2d_controlLb_BS_to_JPSI_PI_PI->ProjectionY());
    hs_pK_controlLbY.Add(pK_2d_controlLb_BS_to_JPSI_K_K->ProjectionY());
    hs_pK_controlLbY.Add(pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->ProjectionY());
    hs_pK_controlLbY.Add(pK_2d_controlLb_BD_to_JPSI_K_PI->ProjectionY());
    if (RARE_DECAYS)
    {
        hs_pK_controlLbY.Add(pK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->ProjectionY());
        hs_pK_controlLbY.Add(pK_2d_controlLb_BD_to_JPSI_K_K->ProjectionY());
        hs_pK_controlLbY.Add(pK_2d_controlLb_BS_to_JPSI_K_PI->ProjectionY());
    }
    TCanvas * c_pK_controlLbY = new TCanvas("jpsip_mass_ProjectionY");
    pK_2d_controlLb_data->ProjectionY()->SetStats(kFALSE);
    pK_2d_controlLb_data->ProjectionY()->GetYaxis()->SetTitle("GeV");
    pK_2d_controlLb_data->ProjectionY()->GetYaxis()->SetTitle("Events / 16 MeV");
    pK_2d_controlLb_data->ProjectionY()->GetYaxis()->SetRangeUser(0., 1500.);
    pK_2d_controlLb_data->ProjectionY()->Draw("");
    hs_pK_controlLbY.Draw("hist same");
    pK_2d_controlLb_data->ProjectionY()->Draw("same");
    pK_2d_controlLb_data->ProjectionY()->Draw("same axis");
    legend_Jpsip_X->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = pictures_path + "pK_2d_controlLb_ProjectionY.C";
    pictures_path_png = pictures_path + "pK_2d_controlLb_ProjectionY.png";
    c_pK_controlLbY->SaveAs(pictures_path_cpp.c_str());
    c_pK_controlLbY->SaveAs(pictures_path_png.c_str());
    
    TH2 *JpsiK_mass_Ls1800_12 = new TH2D("JpsiK_mass_Ls1800_12", "JpsiK_mass_Ls1800_12", 50, 3.600, 4.850, 50, 3.600, 4.850);
    TH2 *JpsiK_mass_Ls1810_12 = new TH2D("JpsiK_mass_Ls1810_12", "JpsiK_mass_Ls1810_12", 50, 3.600, 4.850, 50, 3.600, 4.850);
    TH2 *JpsiK_mass_Ls1820_52 = new TH2D("JpsiK_mass_Ls1820_52", "JpsiK_mass_Ls1820_52", 50, 3.600, 4.850, 50, 3.600, 4.850);
    TH2 *JpsiK_mass_Ls1830_52 = new TH2D("JpsiK_mass_Ls1830_52", "JpsiK_mass_Ls1830_52", 50, 3.600, 4.850, 50, 3.600, 4.850);
    TH2 *JpsiK_mass_Ls1890_32 = new TH2D("JpsiK_mass_Ls1890_32", "JpsiK_mass_Ls1890_32", 50, 3.600, 4.850, 50, 3.600, 4.850);
    TH2 *JpsiK_mass_Ls2020_72 = new TH2D("JpsiK_mass_Ls2020_72", "JpsiK_mass_Ls2020_72", 50, 3.600, 4.850, 50, 3.600, 4.850);
    TH2 *JpsiK_mass_Ls2050_32 = new TH2D("JpsiK_mass_Ls2050_32", "JpsiK_mass_Ls2050_32", 50, 3.600, 4.850, 50, 3.600, 4.850);
    TH2 *JpsiK_mass_Ls2100_72 = new TH2D("JpsiK_mass_Ls2100_72", "JpsiK_mass_Ls2100_72", 50, 3.600, 4.850, 50, 3.600, 4.850);
    TH2 *JpsiK_mass_Ls2110_52 = new TH2D("JpsiK_mass_Ls2110_52", "JpsiK_mass_Ls2110_52", 50, 3.600, 4.850, 50, 3.600, 4.850);
    TH2 *JpsiK_mass_Ls2325_32 = new TH2D("JpsiK_mass_Ls2325_32", "JpsiK_mass_Ls2325_32", 50, 3.600, 4.850, 50, 3.600, 4.850);
    TH2 *JpsiK_mass_Ls2350_92 = new TH2D("JpsiK_mass_Ls2350_92", "JpsiK_mass_Ls2350_92", 50, 3.600, 4.850, 50, 3.600, 4.850);
    TH2 *JpsiK_mass_LsNR_12 = new TH2D("JpsiK_mass_LsNR_12", "JpsiK_mass_LsNR_12", 50, 3.600, 4.850, 50, 3.600, 4.850);
    TH2 *JpsiK_mass_Pc1 = new TH2D("JpsiK_mass_Pc1", "JpsiK_mass_Pc1", 50, 3.600, 4.850, 50, 3.600, 4.850);
    TH2 *JpsiK_mass_Pc2 = new TH2D("JpsiK_mass_Pc2", "JpsiK_mass_Pc2", 50, 3.600, 4.850, 50, 3.600, 4.850);
    TH2 *JpsiK_mass_sum_controlLb = new TH2D("JpsiK_mass_sum_controlLb", "JpsiK_mass_sum_controlLb", 50, 3.600, 4.850, 50, 3.600, 4.850);
    
    TH2 *Jpsip_mass_Ls1800_12 = new TH2D("Jpsip_mass_Ls1800_12", "Jpsip_mass_Ls1800_12", 50, 4.000, 5.250, 50, 4.000, 5.250);
    TH2 *Jpsip_mass_Ls1810_12 = new TH2D("Jpsip_mass_Ls1810_12", "Jpsip_mass_Ls1810_12", 50, 4.000, 5.250, 50, 4.000, 5.250);
    TH2 *Jpsip_mass_Ls1820_52 = new TH2D("Jpsip_mass_Ls1820_52", "Jpsip_mass_Ls1820_52", 50, 4.000, 5.250, 50, 4.000, 5.250);
    TH2 *Jpsip_mass_Ls1830_52 = new TH2D("Jpsip_mass_Ls1830_52", "Jpsip_mass_Ls1830_52", 50, 4.000, 5.250, 50, 4.000, 5.250);
    TH2 *Jpsip_mass_Ls1890_32 = new TH2D("Jpsip_mass_Ls1890_32", "Jpsip_mass_Ls1890_32", 50, 4.000, 5.250, 50, 4.000, 5.250);
    TH2 *Jpsip_mass_Ls2020_72 = new TH2D("Jpsip_mass_Ls2020_72", "Jpsip_mass_Ls2020_72", 50, 4.000, 5.250, 50, 4.000, 5.250);
    TH2 *Jpsip_mass_Ls2050_32 = new TH2D("Jpsip_mass_Ls2050_32", "Jpsip_mass_Ls2050_32", 50, 4.000, 5.250, 50, 4.000, 5.250);
    TH2 *Jpsip_mass_Ls2100_72 = new TH2D("Jpsip_mass_Ls2100_72", "Jpsip_mass_Ls2100_72", 50, 4.000, 5.250, 50, 4.000, 5.250);
    TH2 *Jpsip_mass_Ls2110_52 = new TH2D("Jpsip_mass_Ls2110_52", "Jpsip_mass_Ls2110_52", 50, 4.000, 5.250, 50, 4.000, 5.250);
    TH2 *Jpsip_mass_Ls2325_32 = new TH2D("Jpsip_mass_Ls2325_32", "Jpsip_mass_Ls2325_32", 50, 4.000, 5.250, 50, 4.000, 5.250);
    TH2 *Jpsip_mass_Ls2350_92 = new TH2D("Jpsip_mass_Ls2350_92", "Jpsip_mass_Ls2350_92", 50, 4.000, 5.250, 50, 4.000, 5.250);
    TH2 *Jpsip_mass_LsNR_12 = new TH2D("Jpsip_mass_LsNR_12", "Jpsip_mass_LsNR_12", 50, 4.000, 5.250, 50, 4.000, 5.250);
    TH2 *Jpsip_mass_Pc1 = new TH2D("Jpsip_mass_Pc1", "Jpsip_mass_Pc1", 50, 4.000, 5.250, 50, 4.000, 5.250);
    TH2 *Jpsip_mass_Pc2 = new TH2D("Jpsip_mass_Pc2", "Jpsip_mass_Pc2", 50, 4.000, 5.250, 50, 4.000, 5.250);
    TH2 *Jpsip_mass_sum_controlLb = new TH2D("Jpsip_mass_sum_controlLb", "Jpsip_mass_sum_controlLb", 50, 4.000, 5.250, 50, 4.000, 5.250);
    
    TH2 *pK_mass_Ls1800_12 = new TH2D("pK_mass_Ls1800_12", "pK_mass_Ls1800_12", 50, 2.000, 2.800, 50, 2.000, 2.800);
    TH2 *pK_mass_Ls1810_12 = new TH2D("pK_mass_Ls1810_12", "pK_mass_Ls1810_12", 50, 2.000, 2.800, 50, 2.000, 2.800);
    TH2 *pK_mass_Ls1820_52 = new TH2D("pK_mass_Ls1820_52", "pK_mass_Ls1820_52", 50, 2.000, 2.800, 50, 2.000, 2.800);
    TH2 *pK_mass_Ls1830_52 = new TH2D("pK_mass_Ls1830_52", "pK_mass_Ls1830_52", 50, 2.000, 2.800, 50, 2.000, 2.800);
    TH2 *pK_mass_Ls1890_32 = new TH2D("pK_mass_Ls1890_32", "pK_mass_Ls1890_32", 50, 2.000, 2.800, 50, 2.000, 2.800);
    TH2 *pK_mass_Ls2020_72 = new TH2D("pK_mass_Ls2020_72", "pK_mass_Ls2020_72", 50, 2.000, 2.800, 50, 2.000, 2.800);
    TH2 *pK_mass_Ls2050_32 = new TH2D("pK_mass_Ls2050_32", "pK_mass_Ls2050_32", 50, 2.000, 2.800, 50, 2.000, 2.800);
    TH2 *pK_mass_Ls2100_72 = new TH2D("pK_mass_Ls2100_72", "pK_mass_Ls2100_72", 50, 2.000, 2.800, 50, 2.000, 2.800);
    TH2 *pK_mass_Ls2110_52 = new TH2D("pK_mass_Ls2110_52", "pK_mass_Ls2110_52", 50, 2.000, 2.800, 50, 2.000, 2.800);
    TH2 *pK_mass_Ls2325_32 = new TH2D("pK_mass_Ls2325_32", "pK_mass_Ls2325_32", 50, 2.000, 2.800, 50, 2.000, 2.800);
    TH2 *pK_mass_Ls2350_92 = new TH2D("pK_mass_Ls2350_92", "pK_mass_Ls2350_92", 50, 2.000, 2.800, 50, 2.000, 2.800);
    TH2 *pK_mass_LsNR_12 = new TH2D("pK_mass_LsNR_12", "pK_mass_LsNR_12", 50, 2.000, 2.800, 50, 2.000, 2.800);
    TH2 *pK_mass_Pc1 = new TH2D("pK_mass_Pc1", "pK_mass_Pc1", 50, 2.000, 2.800, 50, 2.000, 2.800);
    TH2 *pK_mass_Pc2 = new TH2D("pK_mass_Pc2", "pK_mass_Pc2", 50, 2.000, 2.800, 50, 2.000, 2.800);
    TH2 *pK_mass_sum_controlLb = new TH2D("pK_mass_sum_controlLb", "pK_mass_sum_controlLb", 50, 2.000, 2.800, 50, 2.000, 2.800);
    
    Double_t w_Ls1800_12_sum = 0., w_Ls1810_12_sum = 0., w_Ls1820_52_sum = 0., w_Ls1830_52_sum = 0., w_Ls1890_32_sum = 0., w_Ls2020_72_sum = 0., w_Ls2050_32_sum = 0., w_Ls2100_72_sum = 0., w_Ls2110_52_sum = 0., w_Ls2325_32_sum = 0., w_Ls2350_92_sum = 0., w_LsNR_12_sum = 0., w_Pc1_sum = 0., w_Pc2_sum = 0.;
    
    Double_t w_Ls1800_12 = 0., w_Ls1810_12 = 0., w_Ls1820_52 = 0., w_Ls1830_52 = 0., w_Ls1890_32 = 0., w_Ls2020_72 = 0., w_Ls2050_32 = 0., w_Ls2100_72 = 0., w_Ls2110_52 = 0., w_Ls2325_32 = 0., w_Ls2350_92 = 0., w_LsNR_12 = 0., w_Pc1 = 0., w_Pc2 = 0.;
    
    TH1D * w_Lb_relation = new TH1D("w_Lb_relation", "w_Lb_relation", 100., 0., 150.);
    
    for (int i = 0; i < phs_Lb; i++)
    {
		if (controlLb_a_Lb[i]) {
        w_Ls1800_12 = HamplitudePcLstar_total(Jpsip_truth_Lb[i] / 1000., pK_truth_Lb[i] / 1000., 5.6196, M01, M02, 0., 0., gamma1, gamma2, 0., 0.,
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (Ls1800_12_Bw1_amp * cos(Ls1800_12_Bw1_phi), Ls1800_12_Bw1_amp * cos(Ls1800_12_Bw1_phi)), 
                        std::complex<float> (Ls1800_12_Bw2_amp * cos(Ls1800_12_Bw2_phi), Ls1800_12_Bw2_amp * cos(Ls1800_12_Bw2_phi)), 
                        std::complex<float> (Ls1800_12_Bw3_amp * cos(Ls1800_12_Bw3_phi), Ls1800_12_Bw3_amp * cos(Ls1800_12_Bw3_phi)), 
                        std::complex<float> (Ls1800_12_Bw4_amp * cos(Ls1800_12_Bw4_phi), Ls1800_12_Bw4_amp * cos(Ls1800_12_Bw4_phi)),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						theta_Lstar_a[i], theta_lambda_b_ls_a[i], thete_psi_Lstar_a[i], phi_mu_Lstar_a[i], phi_K_Lstar_a[i],
						theta_Pc_a[i], theta_lambda_b_a[i], theta_psi_a[i], phi_mu_Pc_a[i], phi_Pc_a[i], phi_psi_a[i], 
						alpha_mu_Lb_a[i], theta_p_a[i]) * w_dimuon_a_Lb[i];  
            
        w_Ls1810_12 = HamplitudePcLstar_total(Jpsip_truth_Lb[i] / 1000., pK_truth_Lb[i] / 1000., 5.6196, M01, M02, 0., 0., gamma1, gamma2, 0., 0.,
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (Ls1810_12_Bw1_amp * cos(Ls1810_12_Bw1_phi), Ls1810_12_Bw1_amp * cos(Ls1810_12_Bw1_phi)), 
                        std::complex<float> (Ls1810_12_Bw2_amp * cos(Ls1810_12_Bw2_phi), Ls1810_12_Bw2_amp * cos(Ls1810_12_Bw2_phi)), 
                        std::complex<float> (Ls1810_12_Bw3_amp * cos(Ls1810_12_Bw3_phi), Ls1810_12_Bw3_amp * cos(Ls1810_12_Bw3_phi)),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.),
						theta_Lstar_a[i], theta_lambda_b_ls_a[i], thete_psi_Lstar_a[i], phi_mu_Lstar_a[i], phi_K_Lstar_a[i],
						theta_Pc_a[i], theta_lambda_b_a[i], theta_psi_a[i], phi_mu_Pc_a[i], phi_Pc_a[i], phi_psi_a[i], 
						alpha_mu_Lb_a[i], theta_p_a[i]) * w_dimuon_a_Lb[i];
            
        w_Ls1820_52 = HamplitudePcLstar_total(Jpsip_truth_Lb[i] / 1000., pK_truth_Lb[i] / 1000., 5.6196, M01, M02, 0., 0., gamma1, gamma2, 0., 0.,
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (Ls1820_52_Bw1_amp * cos(Ls1820_52_Bw1_phi), Ls1820_52_Bw1_amp * cos(Ls1820_52_Bw1_phi)), 
                        std::complex<float> (Ls1820_52_Bw2_amp * cos(Ls1820_52_Bw2_phi), Ls1820_52_Bw2_amp * cos(Ls1820_52_Bw2_phi)), 
                        std::complex<float> (Ls1820_52_Bw3_amp * cos(Ls1820_52_Bw3_phi), Ls1820_52_Bw3_amp * cos(Ls1820_52_Bw3_phi)),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						theta_Lstar_a[i], theta_lambda_b_ls_a[i], thete_psi_Lstar_a[i], phi_mu_Lstar_a[i], phi_K_Lstar_a[i],
						theta_Pc_a[i], theta_lambda_b_a[i], theta_psi_a[i], phi_mu_Pc_a[i], phi_Pc_a[i], phi_psi_a[i], 
						alpha_mu_Lb_a[i], theta_p_a[i]) * w_dimuon_a_Lb[i];
            
        w_Ls1830_52 = HamplitudePcLstar_total(Jpsip_truth_Lb[i] / 1000., pK_truth_Lb[i] / 1000., 5.6196, M01, M02, 0., 0., gamma1, gamma2, 0., 0.,
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (Ls1830_52_Bw1_amp * cos(Ls1830_52_Bw1_phi), Ls1830_52_Bw1_amp * cos(Ls1830_52_Bw1_phi)), 
                        std::complex<float> (Ls1830_52_Bw2_amp * cos(Ls1830_52_Bw2_phi), Ls1830_52_Bw2_amp * cos(Ls1830_52_Bw2_phi)), 
                        std::complex<float> (Ls1830_52_Bw3_amp * cos(Ls1830_52_Bw3_phi), Ls1830_52_Bw3_amp * cos(Ls1830_52_Bw3_phi)),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						theta_Lstar_a[i], theta_lambda_b_ls_a[i], thete_psi_Lstar_a[i], phi_mu_Lstar_a[i], phi_K_Lstar_a[i],
						theta_Pc_a[i], theta_lambda_b_a[i], theta_psi_a[i], phi_mu_Pc_a[i], phi_Pc_a[i], phi_psi_a[i], 
						alpha_mu_Lb_a[i], theta_p_a[i]) * w_dimuon_a_Lb[i];
            
        w_Ls1890_32 = HamplitudePcLstar_total(Jpsip_truth_Lb[i] / 1000., pK_truth_Lb[i] / 1000., 5.6196, M01, M02, 0., 0., gamma1, gamma2, 0., 0.,
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (Ls1890_32_Bw1_amp * cos(Ls1890_32_Bw1_phi), Ls1890_32_Bw1_amp * cos(Ls1890_32_Bw1_phi)), 
                        std::complex<float> (Ls1890_32_Bw2_amp * cos(Ls1890_32_Bw2_phi), Ls1890_32_Bw2_amp * cos(Ls1890_32_Bw2_phi)), 
                        std::complex<float> (Ls1890_32_Bw3_amp * cos(Ls1890_32_Bw3_phi), Ls1890_32_Bw3_amp * cos(Ls1890_32_Bw3_phi)),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						theta_Lstar_a[i], theta_lambda_b_ls_a[i], thete_psi_Lstar_a[i], phi_mu_Lstar_a[i], phi_K_Lstar_a[i],
						theta_Pc_a[i], theta_lambda_b_a[i], theta_psi_a[i], phi_mu_Pc_a[i], phi_Pc_a[i], phi_psi_a[i], 
						alpha_mu_Lb_a[i], theta_p_a[i]) * w_dimuon_a_Lb[i];
            
        w_Ls2020_72 = HamplitudePcLstar_total(Jpsip_truth_Lb[i] / 1000., pK_truth_Lb[i] / 1000., 5.6196, M01, M02, 0., 0., gamma1, gamma2, 0., 0.,
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (Ls2020_72_Bw1_amp * cos(Ls2020_72_Bw1_phi), Ls2020_72_Bw1_amp * cos(Ls2020_72_Bw1_phi)), 
                        std::complex<float> (Ls2020_72_Bw2_amp * cos(Ls2020_72_Bw2_phi), Ls2020_72_Bw2_amp * cos(Ls2020_72_Bw2_phi)), 
                        std::complex<float> (Ls2020_72_Bw3_amp * cos(Ls2020_72_Bw3_phi), Ls2020_72_Bw3_amp * cos(Ls2020_72_Bw3_phi)),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						theta_Lstar_a[i], theta_lambda_b_ls_a[i], thete_psi_Lstar_a[i], phi_mu_Lstar_a[i], phi_K_Lstar_a[i],
						theta_Pc_a[i], theta_lambda_b_a[i], theta_psi_a[i], phi_mu_Pc_a[i], phi_Pc_a[i], phi_psi_a[i], 
						alpha_mu_Lb_a[i], theta_p_a[i]) * w_dimuon_a_Lb[i];
            
        w_Ls2050_32 = HamplitudePcLstar_total(Jpsip_truth_Lb[i] / 1000., pK_truth_Lb[i] / 1000., 5.6196, M01, M02, 0., 0., gamma1, gamma2, 0., 0.,
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (Ls2050_32_Bw1_amp * cos(Ls2050_32_Bw1_phi), Ls2050_32_Bw1_amp * cos(Ls2050_32_Bw1_phi)), 
                        std::complex<float> (Ls2050_32_Bw2_amp * cos(Ls2050_32_Bw2_phi), Ls2050_32_Bw2_amp * cos(Ls2050_32_Bw2_phi)),
                        std::complex<float> (Ls2050_32_Bw3_amp * cos(Ls2050_32_Bw3_phi), Ls2050_32_Bw3_amp * cos(Ls2050_32_Bw3_phi)),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						theta_Lstar_a[i], theta_lambda_b_ls_a[i], thete_psi_Lstar_a[i], phi_mu_Lstar_a[i], phi_K_Lstar_a[i],
						theta_Pc_a[i], theta_lambda_b_a[i], theta_psi_a[i], phi_mu_Pc_a[i], phi_Pc_a[i], phi_psi_a[i], 
						alpha_mu_Lb_a[i], theta_p_a[i]) * w_dimuon_a_Lb[i];
            
        w_Ls2100_72 = HamplitudePcLstar_total(Jpsip_truth_Lb[i] / 1000., pK_truth_Lb[i] / 1000., 5.6196, M01, M02, 0., 0., gamma1, gamma2, 0., 0.,
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.),
						std::complex<float> (Ls2100_72_Bw1_amp * cos(Ls2100_72_Bw1_phi), Ls2100_72_Bw1_amp * cos(Ls2100_72_Bw1_phi)), 
                        std::complex<float> (Ls2100_72_Bw2_amp * cos(Ls2100_72_Bw2_phi), Ls2100_72_Bw2_amp * cos(Ls2100_72_Bw2_phi)), 
                        std::complex<float> (Ls2100_72_Bw3_amp * cos(Ls2100_72_Bw3_phi), Ls2100_72_Bw3_amp * cos(Ls2100_72_Bw3_phi)),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						theta_Lstar_a[i], theta_lambda_b_ls_a[i], thete_psi_Lstar_a[i], phi_mu_Lstar_a[i], phi_K_Lstar_a[i],
						theta_Pc_a[i], theta_lambda_b_a[i], theta_psi_a[i], phi_mu_Pc_a[i], phi_Pc_a[i], phi_psi_a[i], 
						alpha_mu_Lb_a[i], theta_p_a[i]) * w_dimuon_a_Lb[i];
            
        w_Ls2110_52 = HamplitudePcLstar_total(Jpsip_truth_Lb[i] / 1000., pK_truth_Lb[i] / 1000., 5.6196, M01, M02, 0., 0., gamma1, gamma2, 0., 0.,
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (Ls2110_52_Bw1_amp * cos(Ls2110_52_Bw1_phi), Ls2110_52_Bw1_amp * cos(Ls2110_52_Bw1_phi)), 
                        std::complex<float> (Ls2110_52_Bw2_amp * cos(Ls2110_52_Bw2_phi), Ls2110_52_Bw2_amp * cos(Ls2110_52_Bw2_phi)), 
                        std::complex<float> (Ls2110_52_Bw3_amp * cos(Ls2110_52_Bw3_phi), Ls2110_52_Bw3_amp * cos(Ls2110_52_Bw3_phi)),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						theta_Lstar_a[i], theta_lambda_b_ls_a[i], thete_psi_Lstar_a[i], phi_mu_Lstar_a[i], phi_K_Lstar_a[i],
						theta_Pc_a[i], theta_lambda_b_a[i], theta_psi_a[i], phi_mu_Pc_a[i], phi_Pc_a[i], phi_psi_a[i], 
						alpha_mu_Lb_a[i], theta_p_a[i]) * w_dimuon_a_Lb[i];
            
        w_Ls2325_32 = HamplitudePcLstar_total(Jpsip_truth_Lb[i] / 1000., pK_truth_Lb[i] / 1000., 5.6196, M01, M02, 0., 0., gamma1, gamma2, 0., 0.,
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (Ls2325_32_Bw1_amp * cos(Ls2325_32_Bw1_phi), Ls2325_32_Bw1_amp * cos(Ls2325_32_Bw1_phi)), 
                        std::complex<float> (Ls2325_32_Bw2_amp * cos(Ls2325_32_Bw2_phi), Ls2325_32_Bw2_amp * cos(Ls2325_32_Bw2_phi)), 
                        std::complex<float> (Ls2325_32_Bw3_amp * cos(Ls2325_32_Bw3_phi), Ls2325_32_Bw3_amp * cos(Ls2325_32_Bw3_phi)),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						theta_Lstar_a[i], theta_lambda_b_ls_a[i], thete_psi_Lstar_a[i], phi_mu_Lstar_a[i], phi_K_Lstar_a[i],
						theta_Pc_a[i], theta_lambda_b_a[i], theta_psi_a[i], phi_mu_Pc_a[i], phi_Pc_a[i], phi_psi_a[i], 
						alpha_mu_Lb_a[i], theta_p_a[i]) * w_dimuon_a_Lb[i];
            
        w_Ls2350_92 = HamplitudePcLstar_total(Jpsip_truth_Lb[i] / 1000., pK_truth_Lb[i] / 1000., 5.6196, M01, M02, 0., 0., gamma1, gamma2, 0., 0.,
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (Ls2350_92_Bw1_amp * cos(Ls2350_92_Bw1_phi), Ls2350_92_Bw1_amp * cos(Ls2350_92_Bw1_phi)), 
                        std::complex<float> (Ls2350_92_Bw2_amp * cos(Ls2350_92_Bw2_phi), Ls2350_92_Bw2_amp * cos(Ls2350_92_Bw2_phi)), 
                        std::complex<float> (0., 0.),
						theta_Lstar_a[i], theta_lambda_b_ls_a[i], thete_psi_Lstar_a[i], phi_mu_Lstar_a[i], phi_K_Lstar_a[i],
						theta_Pc_a[i], theta_lambda_b_a[i], theta_psi_a[i], phi_mu_Pc_a[i], phi_Pc_a[i], phi_psi_a[i], 
						alpha_mu_Lb_a[i], theta_p_a[i]) * w_dimuon_a_Lb[i];
            
        w_LsNR_12 = HamplitudePcLstar_total(Jpsip_truth_Lb[i] / 1000., pK_truth_Lb[i] / 1000., 5.6196, M01, M02, 0., 0., gamma1, gamma2, 0., 0.,
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (LsNR_12_Bw_amp * cos(LsNR_12_Bw_phi), LsNR_12_Bw_amp * cos(LsNR_12_Bw_phi)),
						theta_Lstar_a[i], theta_lambda_b_ls_a[i], thete_psi_Lstar_a[i], phi_mu_Lstar_a[i], phi_K_Lstar_a[i],
						theta_Pc_a[i], theta_lambda_b_a[i], theta_psi_a[i], phi_mu_Pc_a[i], phi_Pc_a[i], phi_psi_a[i], 
						alpha_mu_Lb_a[i], theta_p_a[i]) * w_dimuon_a_Lb[i];
            
        w_Pc1 = HamplitudePcLstar_total(Jpsip_truth_Lb[i] / 1000., pK_truth_Lb[i] / 1000., 5.6196, M01, M02, 0., 0., gamma1, gamma2, 0., 0.,
						std::complex<float> (Pc1Bw1_amp * cos(Pc1Bw1_phi), Pc1Bw1_amp * cos(Pc1Bw1_phi)), 
                        std::complex<float> (Pc1Bw2_amp * cos(Pc1Bw2_phi), Pc1Bw2_amp * cos(Pc1Bw2_phi)), 
                        std::complex<float> (Pc1Bs1_amp * cos(Pc1Bs1_phi), Pc1Bs1_amp * cos(Pc1Bs1_phi)), 
                        std::complex<float> (Pc1Bs2_amp * cos(Pc1Bs2_phi), Pc1Bs2_amp * cos(Pc1Bs2_phi)), 
                        std::complex<float> (Pc1Bs3_amp * cos(Pc1Bs3_phi), Pc1Bs3_amp * cos(Pc1Bs3_phi)),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						theta_Lstar_a[i], theta_lambda_b_ls_a[i], thete_psi_Lstar_a[i], phi_mu_Lstar_a[i], phi_K_Lstar_a[i],
						theta_Pc_a[i], theta_lambda_b_a[i], theta_psi_a[i], phi_mu_Pc_a[i], phi_Pc_a[i], phi_psi_a[i], 
						alpha_mu_Lb_a[i], theta_p_a[i]) * w_dimuon_a_Lb[i];
            
        w_Pc2 = HamplitudePcLstar_total(Jpsip_truth_Lb[i] / 1000., pK_truth_Lb[i] / 1000., 5.6196, M01, M02, 0., 0., gamma1, gamma2, 0., 0.,
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (Pc2Bw1_amp * cos(Pc2Bw1_phi), Pc2Bw1_amp * cos(Pc2Bw1_phi)), 
                        std::complex<float> (Pc2Bw2_amp * cos(Pc2Bw2_phi), Pc2Bw2_amp * cos(Pc2Bw2_phi)), 
                        std::complex<float> (Pc2Bs1_amp * cos(Pc2Bs1_phi), Pc2Bs1_amp * cos(Pc2Bs1_phi)), 
                        std::complex<float> (Pc2Bs2_amp * cos(Pc2Bs2_phi), Pc2Bs2_amp * cos(Pc2Bs2_phi)), 
                        std::complex<float> (Pc2Bs3_amp * cos(Pc2Bs3_phi), Pc2Bs3_amp * cos(Pc2Bs3_phi)),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.), 
                        std::complex<float> (0., 0.),
						theta_Lstar_a[i], theta_lambda_b_ls_a[i], thete_psi_Lstar_a[i], phi_mu_Lstar_a[i], phi_K_Lstar_a[i],
						theta_Pc_a[i], theta_lambda_b_a[i], theta_psi_a[i], phi_mu_Pc_a[i], phi_Pc_a[i], phi_psi_a[i], 
						alpha_mu_Lb_a[i], theta_p_a[i]) * w_dimuon_a_Lb[i];
    	
    	
    	w_Ls1800_12_sum += w_Ls1800_12;
        w_Ls1810_12_sum += w_Ls1810_12;
        w_Ls1820_52_sum += w_Ls1820_52;
        w_Ls1830_52_sum += w_Ls1830_52;
        w_Ls1890_32_sum += w_Ls1890_32;
        w_Ls2020_72_sum += w_Ls2020_72;
        w_Ls2050_32_sum += w_Ls2050_32;
        w_Ls2100_72_sum += w_Ls2100_72;
        w_Ls2110_52_sum += w_Ls2110_52;
        w_Ls2325_32_sum += w_Ls2325_32;
        w_Ls2350_92_sum += w_Ls2350_92;
        
    	w_Lb_relation->Fill(w_Ls2100_72 / w_Ls2110_52);
    	
    	JpsiK_mass_Ls1800_12->Fill(JpsiK1_mass_Lb[i], JpsiK2_mass_Lb[i], w_Ls1800_12);
        JpsiK_mass_Ls1810_12->Fill(JpsiK1_mass_Lb[i], JpsiK2_mass_Lb[i], w_Ls1810_12);
        JpsiK_mass_Ls1820_52->Fill(JpsiK1_mass_Lb[i], JpsiK2_mass_Lb[i], w_Ls1820_52);
        JpsiK_mass_Ls1830_52->Fill(JpsiK1_mass_Lb[i], JpsiK2_mass_Lb[i], w_Ls1830_52);
        JpsiK_mass_Ls1890_32->Fill(JpsiK1_mass_Lb[i], JpsiK2_mass_Lb[i], w_Ls1890_32);
        JpsiK_mass_Ls2020_72->Fill(JpsiK1_mass_Lb[i], JpsiK2_mass_Lb[i], w_Ls2020_72);
        JpsiK_mass_Ls2050_32->Fill(JpsiK1_mass_Lb[i], JpsiK2_mass_Lb[i], w_Ls2050_32);
        JpsiK_mass_Ls2100_72->Fill(JpsiK1_mass_Lb[i], JpsiK2_mass_Lb[i], w_Ls2100_72);
        JpsiK_mass_Ls2110_52->Fill(JpsiK1_mass_Lb[i], JpsiK2_mass_Lb[i], w_Ls2110_52);
        JpsiK_mass_Ls2325_32->Fill(JpsiK1_mass_Lb[i], JpsiK2_mass_Lb[i], w_Ls2325_32);
        JpsiK_mass_Ls2350_92->Fill(JpsiK1_mass_Lb[i], JpsiK2_mass_Lb[i], w_Ls2350_92);
        JpsiK_mass_LsNR_12->Fill(JpsiK1_mass_Lb[i], JpsiK2_mass_Lb[i], w_LsNR_12);
        JpsiK_mass_Pc1->Fill(JpsiK1_mass_Lb[i], JpsiK2_mass_Lb[i], w_Pc1);
        JpsiK_mass_Pc2->Fill(JpsiK1_mass_Lb[i], JpsiK2_mass_Lb[i], w_Pc2);
        
        Jpsip_mass_Ls1800_12->Fill(Jpsip1_mass_Lb[i], Jpsip2_mass_Lb[i], w_Ls1800_12);
        Jpsip_mass_Ls1810_12->Fill(Jpsip1_mass_Lb[i], Jpsip2_mass_Lb[i], w_Ls1810_12);
        Jpsip_mass_Ls1820_52->Fill(Jpsip1_mass_Lb[i], Jpsip2_mass_Lb[i], w_Ls1820_52);
        Jpsip_mass_Ls1830_52->Fill(Jpsip1_mass_Lb[i], Jpsip2_mass_Lb[i], w_Ls1830_52);
        Jpsip_mass_Ls1890_32->Fill(Jpsip1_mass_Lb[i], Jpsip2_mass_Lb[i], w_Ls1890_32);
        Jpsip_mass_Ls2020_72->Fill(Jpsip1_mass_Lb[i], Jpsip2_mass_Lb[i], w_Ls2020_72);
        Jpsip_mass_Ls2050_32->Fill(Jpsip1_mass_Lb[i], Jpsip2_mass_Lb[i], w_Ls2050_32);
        Jpsip_mass_Ls2100_72->Fill(Jpsip1_mass_Lb[i], Jpsip2_mass_Lb[i], w_Ls2100_72);
        Jpsip_mass_Ls2110_52->Fill(Jpsip1_mass_Lb[i], Jpsip2_mass_Lb[i], w_Ls2110_52);
        Jpsip_mass_Ls2325_32->Fill(Jpsip1_mass_Lb[i], Jpsip2_mass_Lb[i], w_Ls2325_32);
        Jpsip_mass_Ls2350_92->Fill(Jpsip1_mass_Lb[i], Jpsip2_mass_Lb[i], w_Ls2350_92);
        Jpsip_mass_LsNR_12->Fill(Jpsip1_mass_Lb[i], Jpsip2_mass_Lb[i], w_LsNR_12);
        Jpsip_mass_Pc1->Fill(Jpsip1_mass_Lb[i], Jpsip2_mass_Lb[i], w_Pc1);
        Jpsip_mass_Pc2->Fill(Jpsip1_mass_Lb[i], Jpsip2_mass_Lb[i], w_Pc2);
        
        pK_mass_Ls1800_12->Fill(p1K2_mass_Lb[i], p2K1_mass_Lb[i], w_Ls1800_12);
        pK_mass_Ls1810_12->Fill(p1K2_mass_Lb[i], p2K1_mass_Lb[i], w_Ls1810_12);
        pK_mass_Ls1820_52->Fill(p1K2_mass_Lb[i], p2K1_mass_Lb[i], w_Ls1820_52);
        pK_mass_Ls1830_52->Fill(p1K2_mass_Lb[i], p2K1_mass_Lb[i], w_Ls1830_52);
        pK_mass_Ls1890_32->Fill(p1K2_mass_Lb[i], p2K1_mass_Lb[i], w_Ls1890_32);
        pK_mass_Ls2020_72->Fill(p1K2_mass_Lb[i], p2K1_mass_Lb[i], w_Ls2020_72);
        pK_mass_Ls2050_32->Fill(p1K2_mass_Lb[i], p2K1_mass_Lb[i], w_Ls2050_32);
        pK_mass_Ls2100_72->Fill(p1K2_mass_Lb[i], p2K1_mass_Lb[i], w_Ls2100_72);
        pK_mass_Ls2110_52->Fill(p1K2_mass_Lb[i], p2K1_mass_Lb[i], w_Ls2110_52);
        pK_mass_Ls2325_32->Fill(p1K2_mass_Lb[i], p2K1_mass_Lb[i], w_Ls2325_32);
        pK_mass_Ls2350_92->Fill(p1K2_mass_Lb[i], p2K1_mass_Lb[i], w_Ls2350_92);
        pK_mass_LsNR_12->Fill(p1K2_mass_Lb[i], p2K1_mass_Lb[i], w_LsNR_12);
        pK_mass_Pc1->Fill(p1K2_mass_Lb[i], p2K1_mass_Lb[i], w_Pc1);
        pK_mass_Pc2->Fill(p1K2_mass_Lb[i], p2K1_mass_Lb[i], w_Pc2);
	}
    }
    
    
    
    cout << "\nw_Ls1800_12_sum\t" << w_Ls1800_12_sum << endl;
    cout << "w_Ls1810_12_sum\t" << w_Ls1810_12_sum << endl;
    cout << "w_Ls1820_52_sum\t" << w_Ls1820_52_sum << endl;
    cout << "w_Ls1830_52_sum\t" << w_Ls1830_52_sum << endl;
    cout << "w_Ls1890_32_sum\t" << w_Ls1890_32_sum << endl;
    cout << "w_Ls2020_72_sum\t" << w_Ls2020_72_sum << endl;
    cout << "w_Ls2050_32_sum\t" << w_Ls2050_32_sum << endl;
    cout << "w_Ls2100_72_sum\t" << w_Ls2100_72_sum << endl;
    cout << "w_Ls2110_52_sum\t" << w_Ls2110_52_sum << endl;
    cout << "w_Ls2325_32_sum\t" << w_Ls2325_32_sum << endl;
    cout << "w_Ls2350_92_sum\t" << w_Ls2350_92_sum << "\n" << endl;
	
	float norm_Jpsip = 1. / (Jpsip_mass_Ls1800_12->Integral() + Jpsip_mass_Ls1810_12->Integral() + Jpsip_mass_Ls1820_52->Integral() + Jpsip_mass_Ls1830_52->Integral() + Jpsip_mass_Ls1890_32->Integral() + Jpsip_mass_Ls2020_72->Integral() + Jpsip_mass_Ls2050_32->Integral() + Jpsip_mass_Ls2100_72->Integral() + Jpsip_mass_Ls2110_52->Integral() + Jpsip_mass_Ls2325_32->Integral() + Jpsip_mass_Ls2350_92->Integral() + Jpsip_mass_LsNR_12->Integral() + Jpsip_mass_Pc1->Integral() + Jpsip_mass_Pc2 ->Integral()) * n_controlLb_LAMBDA0B_to_JPSI_P_K;
    
    TCanvas * c_w_Lb_relation = new TCanvas("c_w_Lb_relation");
    w_Lb_relation->Draw();
    c_w_Lb_relation->SaveAs("w_Lb_relation.png");
    
    TCanvas * c_Bdwave_controlLb_JpsipX = new TCanvas("Bdwave_controlLb_JpsipX");
    
	//Jpsipi_2d_total->SetStats(kFALSE);
	Jpsip_2d_controlLb_total->SetLineColor(1);
	Jpsip_mass_Ls1800_12->SetLineColor(2);
	Jpsip_mass_Ls1810_12->SetLineColor(3);
	Jpsip_mass_Ls1830_52->SetLineColor(4);
	Jpsip_mass_Ls1890_32->SetLineColor(5);
	Jpsip_mass_Ls2020_72->SetLineColor(6);
	Jpsip_mass_Ls2050_32->SetLineColor(7);
	Jpsip_mass_Ls2100_72->SetLineColor(8);
	Jpsip_mass_Ls2110_52->SetLineColor(9);
    Jpsip_mass_Ls2325_32->SetLineColor(21);
    Jpsip_mass_Ls2350_92->SetLineColor(31);
	Jpsip_mass_LsNR_12->SetLineColor(41);
    Jpsip_mass_Pc1->SetLineColor(51);
    Jpsip_mass_Pc2->SetLineColor(61);

	Jpsip_2d_controlLb_total->SetLineWidth(3);
	Jpsip_mass_Ls1800_12->SetLineWidth(2);
	Jpsip_mass_Ls1810_12->SetLineWidth(2);
	Jpsip_mass_Ls1830_52->SetLineWidth(2);
	Jpsip_mass_Ls1890_32->SetLineWidth(2);
	Jpsip_mass_Ls2020_72->SetLineWidth(2);
	Jpsip_mass_Ls2050_32->SetLineWidth(2);
	Jpsip_mass_Ls2100_72->SetLineWidth(2);
	Jpsip_mass_Ls2110_52->SetLineWidth(2);
    Jpsip_mass_Ls2325_32->SetLineWidth(2);
    Jpsip_mass_Ls2350_92->SetLineWidth(2);
	Jpsip_mass_LsNR_12->SetLineWidth(2);
    Jpsip_mass_Pc1->SetLineWidth(2);
    Jpsip_mass_Pc2->SetLineWidth(2);

	Jpsip_mass_Ls1800_12->Scale(norm_Jpsip);
	Jpsip_mass_Ls1810_12->Scale(norm_Jpsip);
	Jpsip_mass_Ls1830_52->Scale(norm_Jpsip);
	Jpsip_mass_Ls1890_32->Scale(norm_Jpsip);
	Jpsip_mass_Ls2020_72->Scale(norm_Jpsip);
	Jpsip_mass_Ls2050_32->Scale(norm_Jpsip);
	Jpsip_mass_Ls2100_72->Scale(norm_Jpsip);
	Jpsip_mass_Ls2110_52->Scale(norm_Jpsip);
    Jpsip_mass_Ls2325_32->Scale(norm_Jpsip);
    Jpsip_mass_Ls2350_92->Scale(norm_Jpsip);
	Jpsip_mass_LsNR_12->Scale(norm_Jpsip);
    Jpsip_mass_Pc1->Scale(norm_Jpsip);
    Jpsip_mass_Pc2->Scale(norm_Jpsip);
    
    cout << "\nPrinting Lb amplitudes:" << endl;
    cout << "Jpsip_mass_Ls1800_12:\t" << Jpsip_mass_Ls1800_12->Integral() << endl;
    cout << "Jpsip_mass_Ls1810_12:\t" << Jpsip_mass_Ls1810_12->Integral() << endl;
    cout << "Jpsip_mass_Ls1830_52:\t" << Jpsip_mass_Ls1830_52->Integral() << endl;
    cout << "Jpsip_mass_Ls1890_32:\t" << Jpsip_mass_Ls1890_32->Integral() << endl;
    cout << "Jpsip_mass_Ls2020_72:\t" << Jpsip_mass_Ls2020_72->Integral() << endl;
    cout << "Jpsip_mass_Ls2050_32:\t" << Jpsip_mass_Ls2050_32->Integral() << endl;
    cout << "Jpsip_mass_Ls2100_72:\t" << Jpsip_mass_Ls2100_72->Integral() << endl;
    cout << "Jpsip_mass_Ls2110_52:\t" << Jpsip_mass_Ls2110_52->Integral() << endl;
    cout << "Jpsip_mass_Ls2325_32:\t" << Jpsip_mass_Ls2325_32->Integral() << endl;
    cout << "Jpsip_mass_Ls2350_92:\t" << Jpsip_mass_Ls2350_92->Integral() << endl;
    cout << "Jpsip_mass_LsNR_12:\t" << Jpsip_mass_LsNR_12->Integral() << endl;
    cout << "Jpsip_mass_Pc1:\t" << Jpsip_mass_Pc1->Integral() << endl;
    cout << "Jpsip_mass_Pc2:\t" << Jpsip_mass_Pc2->Integral() << "\n" << endl;
	
	Jpsip_2d_controlLb_total->ProjectionX()->SetStats(kFALSE);
    Jpsip_2d_controlLb_total->ProjectionX()->Draw("hist");
	Jpsip_mass_Ls1800_12->ProjectionX()->Draw("hist same");
	Jpsip_mass_Ls1810_12->ProjectionX()->Draw("hist same");
	Jpsip_mass_Ls1830_52->ProjectionX()->Draw("hist same");
	Jpsip_mass_Ls1890_32->ProjectionX()->Draw("hist same");
	Jpsip_mass_Ls2020_72->ProjectionX()->Draw("hist same");
	Jpsip_mass_Ls2050_32->ProjectionX()->Draw("hist same");
	Jpsip_mass_Ls2100_72->ProjectionX()->Draw("hist same");
	Jpsip_mass_Ls2110_52->ProjectionX()->Draw("hist same");
    Jpsip_mass_Ls2325_32->ProjectionX()->Draw("hist same");
    Jpsip_mass_Ls2350_92->ProjectionX()->Draw("hist same");
	Jpsip_mass_LsNR_12->ProjectionX()->Draw("hist same");
	Jpsip_mass_Pc1->ProjectionX()->Draw("hist same");
    Jpsip_mass_Pc2->ProjectionX()->Draw("hist same");
	TLegend *legend_Jpsip = new TLegend(.60,.60,.90,.90);
    legend_Jpsip->AddEntry(Jpsip_2d_controlLb_total,"total");
	legend_Jpsip->AddEntry(Jpsip_mass_Ls1800_12,"#Lambda_{1}*(1800)");
	legend_Jpsip->AddEntry(Jpsip_mass_Ls1810_12,"#Lambda_{0}*(1810)");
	legend_Jpsip->AddEntry(Jpsip_mass_Ls1830_52,"#Lambda_{2}*(1830)");
	legend_Jpsip->AddEntry(Jpsip_mass_Ls1890_32,"#Lambda_{1}*(1890)");
	legend_Jpsip->AddEntry(Jpsip_mass_Ls2020_72,"#Lambda_{3}*(2020)");
	legend_Jpsip->AddEntry(Jpsip_mass_Ls2050_32,"#Lambda_{0}*(2050)");
	legend_Jpsip->AddEntry(Jpsip_mass_Ls2100_72,"#Lambda_{2}*(2100)");
	legend_Jpsip->AddEntry(Jpsip_mass_Ls2110_52,"#Lambda_{4}*(2110)");
    legend_Jpsip->AddEntry(Jpsip_mass_Ls2325_32,"#Lambda_{4}*(2325)");
    legend_Jpsip->AddEntry(Jpsip_mass_Ls2350_92,"#Lambda_{4}*(2350)");
    legend_Jpsip->AddEntry(Jpsip_mass_LsNR_12,"#Lambda non resonant");
    legend_Jpsip->AddEntry(Jpsip_mass_Pc1,"Pc1");
    legend_Jpsip->AddEntry(Jpsip_mass_Pc2,"Pc2");
	legend_Jpsip->Draw();
    t->DrawLatex(0.25,0.88, writetext00);
    t->DrawLatex(0.25,0.83, writetext01);
    pictures_path_cpp = pictures_path + "Bdwave_controlLb_Jpsip_2d_ProjectionX.C";
    pictures_path_png = pictures_path + "Bdwave_controlLb_Jpsip_2d_ProjectionX.png";
    c_Bdwave_controlLb_JpsipX->SaveAs(pictures_path_cpp.c_str());
    c_Bdwave_controlLb_JpsipX->SaveAs(pictures_path_png.c_str());
    
    TCanvas * c_Bdwave_controlLb_JpsipY = new TCanvas("Bdwave_controlLb_JpsipY");
    
    Jpsip_2d_controlLb_total->ProjectionY()->SetStats(kFALSE);
    Jpsip_2d_controlLb_total->ProjectionY()->Draw("hist");
	Jpsip_mass_Ls1800_12->ProjectionY()->Draw("hist same");
	Jpsip_mass_Ls1810_12->ProjectionY()->Draw("hist same");
	Jpsip_mass_Ls1830_52->ProjectionY()->Draw("hist same");
	Jpsip_mass_Ls1890_32->ProjectionY()->Draw("hist same");
	Jpsip_mass_Ls2020_72->ProjectionY()->Draw("hist same");
	Jpsip_mass_Ls2050_32->ProjectionY()->Draw("hist same");
	Jpsip_mass_Ls2100_72->ProjectionY()->Draw("hist same");
	Jpsip_mass_Ls2110_52->ProjectionY()->Draw("hist same");
    Jpsip_mass_Ls2325_32->ProjectionY()->Draw("hist same");
    Jpsip_mass_Ls2350_92->ProjectionY()->Draw("hist same");
	Jpsip_mass_LsNR_12->ProjectionY()->Draw("hist same");
	Jpsip_mass_Pc1->ProjectionY()->Draw("hist same");
    Jpsip_mass_Pc2->ProjectionY()->Draw("hist same");
    legend_Jpsip->Draw();
    t->DrawLatex(0.25,0.88, writetext00);
    t->DrawLatex(0.25,0.83, writetext01);
    pictures_path_cpp = pictures_path + "Bdwave_controlLb_Jpsip_2d_ProjectionY.C";
    pictures_path_png = pictures_path + "Bdwave_controlLb_Jpsip_2d_ProjectionY.png";
    c_Bdwave_controlLb_JpsipY->SaveAs(pictures_path_cpp.c_str());
    c_Bdwave_controlLb_JpsipY->SaveAs(pictures_path_png.c_str());
    
    norm_JpsiK = 1. / (JpsiK_mass_Ls1800_12->Integral() + JpsiK_mass_Ls1810_12->Integral() + JpsiK_mass_Ls1820_52->Integral() + JpsiK_mass_Ls1830_52->Integral() + JpsiK_mass_Ls1890_32->Integral() + JpsiK_mass_Ls2020_72->Integral() + JpsiK_mass_Ls2050_32->Integral() + JpsiK_mass_Ls2100_72->Integral() + JpsiK_mass_Ls2110_52->Integral() + JpsiK_mass_Ls2325_32->Integral() + JpsiK_mass_Ls2350_92->Integral() + JpsiK_mass_LsNR_12->Integral() + JpsiK_mass_Pc1->Integral() + JpsiK_mass_Pc2 ->Integral()) * n_controlLb_LAMBDA0B_to_JPSI_P_K;
    
    TCanvas * c_Bdwave_controlLb_JpsiKX = new TCanvas("Bdwave_controlLb_JpsiKX");
	//JpsiKi_2d_total->SetStats(kFALSE);
	JpsiK_2d_controlLb_total->SetLineColor(1);
	JpsiK_mass_Ls1800_12->SetLineColor(2);
	JpsiK_mass_Ls1810_12->SetLineColor(3);
	JpsiK_mass_Ls1830_52->SetLineColor(4);
	JpsiK_mass_Ls1890_32->SetLineColor(5);
	JpsiK_mass_Ls2020_72->SetLineColor(6);
	JpsiK_mass_Ls2050_32->SetLineColor(7);
	JpsiK_mass_Ls2100_72->SetLineColor(8);
	JpsiK_mass_Ls2110_52->SetLineColor(9);
    JpsiK_mass_Ls2325_32->SetLineColor(21);
    JpsiK_mass_Ls2350_92->SetLineColor(31);
	JpsiK_mass_LsNR_12->SetLineColor(41);
    JpsiK_mass_Pc1->SetLineColor(51);
    JpsiK_mass_Pc2->SetLineColor(61);

	JpsiK_2d_controlLb_total->SetLineWidth(3);
	JpsiK_mass_Ls1800_12->SetLineWidth(2);
	JpsiK_mass_Ls1810_12->SetLineWidth(2);
	JpsiK_mass_Ls1830_52->SetLineWidth(2);
	JpsiK_mass_Ls1890_32->SetLineWidth(2);
	JpsiK_mass_Ls2020_72->SetLineWidth(2);
	JpsiK_mass_Ls2050_32->SetLineWidth(2);
	JpsiK_mass_Ls2100_72->SetLineWidth(2);
	JpsiK_mass_Ls2110_52->SetLineWidth(2);
    JpsiK_mass_Ls2325_32->SetLineWidth(2);
    JpsiK_mass_Ls2350_92->SetLineWidth(2);
	JpsiK_mass_LsNR_12->SetLineWidth(2);
    JpsiK_mass_Pc1->SetLineWidth(2);
    JpsiK_mass_Pc2->SetLineWidth(2);

	JpsiK_mass_Ls1800_12->Scale(norm_JpsiK);
	JpsiK_mass_Ls1810_12->Scale(norm_JpsiK);
	JpsiK_mass_Ls1830_52->Scale(norm_JpsiK);
	JpsiK_mass_Ls1890_32->Scale(norm_JpsiK);
	JpsiK_mass_Ls2020_72->Scale(norm_JpsiK);
	JpsiK_mass_Ls2050_32->Scale(norm_JpsiK);
	JpsiK_mass_Ls2100_72->Scale(norm_JpsiK);
	JpsiK_mass_Ls2110_52->Scale(norm_JpsiK);
    JpsiK_mass_Ls2325_32->Scale(norm_JpsiK);
    JpsiK_mass_Ls2350_92->Scale(norm_JpsiK);
	JpsiK_mass_LsNR_12->Scale(norm_JpsiK);
    JpsiK_mass_Pc1->Scale(norm_JpsiK);
    JpsiK_mass_Pc2->Scale(norm_JpsiK);
	
	JpsiK_2d_controlLb_total->ProjectionX()->SetStats(kFALSE);
    JpsiK_2d_controlLb_total->ProjectionX()->Draw("hist");
	JpsiK_mass_Ls1800_12->ProjectionX()->Draw("hist same");
	JpsiK_mass_Ls1810_12->ProjectionX()->Draw("hist same");
	JpsiK_mass_Ls1830_52->ProjectionX()->Draw("hist same");
	JpsiK_mass_Ls1890_32->ProjectionX()->Draw("hist same");
	JpsiK_mass_Ls2020_72->ProjectionX()->Draw("hist same");
	JpsiK_mass_Ls2050_32->ProjectionX()->Draw("hist same");
	JpsiK_mass_Ls2100_72->ProjectionX()->Draw("hist same");
	JpsiK_mass_Ls2110_52->ProjectionX()->Draw("hist same");
    JpsiK_mass_Ls2325_32->ProjectionX()->Draw("hist same");
    JpsiK_mass_Ls2350_92->ProjectionX()->Draw("hist same");
	JpsiK_mass_LsNR_12->ProjectionX()->Draw("hist same");
	JpsiK_mass_Pc1->ProjectionX()->Draw("hist same");
    JpsiK_mass_Pc2->ProjectionX()->Draw("hist same");
	TLegend *legend_JpsiK = new TLegend(.60,.60,.90,.90);
    legend_JpsiK->AddEntry(JpsiK_2d_controlLb_total,"total");
	legend_JpsiK->AddEntry(JpsiK_mass_Ls1800_12,"#Lambda_{1}*(1800)");
	legend_JpsiK->AddEntry(JpsiK_mass_Ls1810_12,"#Lambda_{0}*(1810)");
	legend_JpsiK->AddEntry(JpsiK_mass_Ls1830_52,"#Lambda_{2}*(1830)");
	legend_JpsiK->AddEntry(JpsiK_mass_Ls1890_32,"#Lambda_{1}*(1890)");
	legend_JpsiK->AddEntry(JpsiK_mass_Ls2020_72,"#Lambda_{3}*(2020)");
	legend_JpsiK->AddEntry(JpsiK_mass_Ls2050_32,"#Lambda_{0}*(2050)");
	legend_JpsiK->AddEntry(JpsiK_mass_Ls2100_72,"#Lambda_{2}*(2100)");
	legend_JpsiK->AddEntry(JpsiK_mass_Ls2110_52,"#Lambda_{4}*(2110)");
    legend_JpsiK->AddEntry(JpsiK_mass_Ls2325_32,"#Lambda_{4}*(2325)");
    legend_JpsiK->AddEntry(JpsiK_mass_Ls2350_92,"#Lambda_{4}*(2350)");
    legend_JpsiK->AddEntry(JpsiK_mass_LsNR_12,"#Lambda non resonant");
    legend_JpsiK->AddEntry(JpsiK_mass_Pc1,"Pc1");
    legend_JpsiK->AddEntry(JpsiK_mass_Pc2,"Pc2");
	legend_JpsiK->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = pictures_path + "Bdwave_controlLb_JpsiK_2d_ProjectionX.C";
    pictures_path_png = pictures_path + "Bdwave_controlLb_JpsiK_2d_ProjectionX.png";
    c_Bdwave_controlLb_JpsiKX->SaveAs(pictures_path_cpp.c_str());
    c_Bdwave_controlLb_JpsiKX->SaveAs(pictures_path_png.c_str());
    
    TCanvas * c_Bdwave_controlLb_JpsiKY = new TCanvas("Bdwave_controlLb_JpsiKY");
    
    JpsiK_2d_controlLb_total->ProjectionY()->SetStats(kFALSE);
    JpsiK_2d_controlLb_total->ProjectionY()->Draw("hist");
	JpsiK_mass_Ls1800_12->ProjectionY()->Draw("hist same");
	JpsiK_mass_Ls1810_12->ProjectionY()->Draw("hist same");
	JpsiK_mass_Ls1830_52->ProjectionY()->Draw("hist same");
	JpsiK_mass_Ls1890_32->ProjectionY()->Draw("hist same");
	JpsiK_mass_Ls2020_72->ProjectionY()->Draw("hist same");
	JpsiK_mass_Ls2050_32->ProjectionY()->Draw("hist same");
	JpsiK_mass_Ls2100_72->ProjectionY()->Draw("hist same");
	JpsiK_mass_Ls2110_52->ProjectionY()->Draw("hist same");
    JpsiK_mass_Ls2325_32->ProjectionY()->Draw("hist same");
    JpsiK_mass_Ls2350_92->ProjectionY()->Draw("hist same");
	JpsiK_mass_LsNR_12->ProjectionY()->Draw("hist same");
	JpsiK_mass_Pc1->ProjectionY()->Draw("hist same");
    JpsiK_mass_Pc2->ProjectionY()->Draw("hist same");
    legend_JpsiK->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = pictures_path + "Bdwave_controlLb_JpsiK_2d_ProjectionY.C";
    pictures_path_png = pictures_path + "Bdwave_controlLb_JpsiK_2d_ProjectionY.png";
    c_Bdwave_controlLb_JpsiKY->SaveAs(pictures_path_cpp.c_str());
    c_Bdwave_controlLb_JpsiKY->SaveAs(pictures_path_png.c_str());
    
    float norm_pK = 1. / (pK_mass_Ls1800_12->Integral() + pK_mass_Ls1810_12->Integral() + pK_mass_Ls1820_52->Integral() + pK_mass_Ls1830_52->Integral() + pK_mass_Ls1890_32->Integral() + pK_mass_Ls2020_72->Integral() + pK_mass_Ls2050_32->Integral() + pK_mass_Ls2100_72->Integral() + pK_mass_Ls2110_52->Integral() + pK_mass_Ls2325_32->Integral() + pK_mass_Ls2350_92->Integral() + pK_mass_LsNR_12->Integral() + pK_mass_Pc1->Integral() + pK_mass_Pc2 ->Integral()) * n_controlLb_LAMBDA0B_to_JPSI_P_K;
    
    TCanvas * c_Bdwave_controlLb_pKX = new TCanvas("Bdwave_controlLb_pKX");
	//pKi_2d_total->SetStats(kFALSE);
	pK_2d_controlLb_total->SetLineColor(1);
	pK_mass_Ls1800_12->SetLineColor(2);
	pK_mass_Ls1810_12->SetLineColor(3);
	pK_mass_Ls1830_52->SetLineColor(4);
	pK_mass_Ls1890_32->SetLineColor(5);
	pK_mass_Ls2020_72->SetLineColor(6);
	pK_mass_Ls2050_32->SetLineColor(7);
	pK_mass_Ls2100_72->SetLineColor(8);
	pK_mass_Ls2110_52->SetLineColor(9);
    pK_mass_Ls2325_32->SetLineColor(21);
    pK_mass_Ls2350_92->SetLineColor(31);
	pK_mass_LsNR_12->SetLineColor(41);
    pK_mass_Pc1->SetLineColor(51);
    pK_mass_Pc2->SetLineColor(61);

	pK_2d_controlLb_total->SetLineWidth(3);
	pK_mass_Ls1800_12->SetLineWidth(2);
	pK_mass_Ls1810_12->SetLineWidth(2);
	pK_mass_Ls1830_52->SetLineWidth(2);
	pK_mass_Ls1890_32->SetLineWidth(2);
	pK_mass_Ls2020_72->SetLineWidth(2);
	pK_mass_Ls2050_32->SetLineWidth(2);
	pK_mass_Ls2100_72->SetLineWidth(2);
	pK_mass_Ls2110_52->SetLineWidth(2);
    pK_mass_Ls2325_32->SetLineWidth(2);
    pK_mass_Ls2350_92->SetLineWidth(2);
	pK_mass_LsNR_12->SetLineWidth(2);
    pK_mass_Pc1->SetLineWidth(2);
    pK_mass_Pc2->SetLineWidth(2);

	pK_mass_Ls1800_12->Scale(norm_pK);
	pK_mass_Ls1810_12->Scale(norm_pK);
	pK_mass_Ls1830_52->Scale(norm_pK);
	pK_mass_Ls1890_32->Scale(norm_pK);
	pK_mass_Ls2020_72->Scale(norm_pK);
	pK_mass_Ls2050_32->Scale(norm_pK);
	pK_mass_Ls2100_72->Scale(norm_pK);
	pK_mass_Ls2110_52->Scale(norm_pK);
    pK_mass_Ls2325_32->Scale(norm_pK);
    pK_mass_Ls2350_92->Scale(norm_pK);
	pK_mass_LsNR_12->Scale(norm_pK);
    pK_mass_Pc1->Scale(norm_pK);
    pK_mass_Pc2->Scale(norm_pK);
	
	pK_2d_controlLb_total->ProjectionX()->SetStats(kFALSE);
    pK_2d_controlLb_total->ProjectionX()->Draw("hist");
	pK_mass_Ls1800_12->ProjectionX()->Draw("hist same");
	pK_mass_Ls1810_12->ProjectionX()->Draw("hist same");
	pK_mass_Ls1830_52->ProjectionX()->Draw("hist same");
	pK_mass_Ls1890_32->ProjectionX()->Draw("hist same");
	pK_mass_Ls2020_72->ProjectionX()->Draw("hist same");
	pK_mass_Ls2050_32->ProjectionX()->Draw("hist same");
	pK_mass_Ls2100_72->ProjectionX()->Draw("hist same");
	pK_mass_Ls2110_52->ProjectionX()->Draw("hist same");
    pK_mass_Ls2325_32->ProjectionX()->Draw("hist same");
    pK_mass_Ls2350_92->ProjectionX()->Draw("hist same");
	pK_mass_LsNR_12->ProjectionX()->Draw("hist same");
	pK_mass_Pc1->ProjectionX()->Draw("hist same");
    pK_mass_Pc2->ProjectionX()->Draw("hist same");
	TLegend *legend_pK = new TLegend(.60,.60,.90,.90);
    legend_pK->AddEntry(pK_2d_controlLb_total,"total");
	legend_pK->AddEntry(pK_mass_Ls1800_12,"#Lambda_{1}*(1800)");
	legend_pK->AddEntry(pK_mass_Ls1810_12,"#Lambda_{0}*(1810)");
	legend_pK->AddEntry(pK_mass_Ls1830_52,"#Lambda_{2}*(1830)");
	legend_pK->AddEntry(pK_mass_Ls1890_32,"#Lambda_{1}*(1890)");
	legend_pK->AddEntry(pK_mass_Ls2020_72,"#Lambda_{3}*(2020)");
	legend_pK->AddEntry(pK_mass_Ls2050_32,"#Lambda_{0}*(2050)");
	legend_pK->AddEntry(pK_mass_Ls2100_72,"#Lambda_{2}*(2100)");
	legend_pK->AddEntry(pK_mass_Ls2110_52,"#Lambda_{4}*(2110)");
    legend_pK->AddEntry(pK_mass_Ls2325_32,"#Lambda_{4}*(2325)");
    legend_pK->AddEntry(pK_mass_Ls2350_92,"#Lambda_{4}*(2350)");
    legend_pK->AddEntry(pK_mass_LsNR_12,"#Lambda non resonant");
    legend_pK->AddEntry(pK_mass_Pc1,"Pc1");
    legend_pK->AddEntry(pK_mass_Pc2,"Pc2");
	legend_pK->Draw();
    t->DrawLatex(0.35,0.88, writetext00);
    t->DrawLatex(0.35,0.83, writetext01);
    pictures_path_cpp = pictures_path + "Bdwave_controlLb_pK_2d_ProjectionX.C";
    pictures_path_png = pictures_path + "Bdwave_controlLb_pK_2d_ProjectionX.png";
    c_Bdwave_controlLb_pKX->SaveAs(pictures_path_cpp.c_str());
    c_Bdwave_controlLb_pKX->SaveAs(pictures_path_png.c_str());
    
    TCanvas * c_Bdwave_controlLb_pKY = new TCanvas("Bdwave_controlLb_pKY");
    
    pK_2d_controlLb_total->ProjectionY()->SetStats(kFALSE);
    pK_2d_controlLb_total->ProjectionY()->Draw("hist");
	pK_mass_Ls1800_12->ProjectionY()->Draw("hist same");
	pK_mass_Ls1810_12->ProjectionY()->Draw("hist same");
	pK_mass_Ls1830_52->ProjectionY()->Draw("hist same");
	pK_mass_Ls1890_32->ProjectionY()->Draw("hist same");
	pK_mass_Ls2020_72->ProjectionY()->Draw("hist same");
	pK_mass_Ls2050_32->ProjectionY()->Draw("hist same");
	pK_mass_Ls2100_72->ProjectionY()->Draw("hist same");
	pK_mass_Ls2110_52->ProjectionY()->Draw("hist same");
    pK_mass_Ls2325_32->ProjectionY()->Draw("hist same");
    pK_mass_Ls2350_92->ProjectionY()->Draw("hist same");
	pK_mass_LsNR_12->ProjectionY()->Draw("hist same");
	pK_mass_Pc1->ProjectionY()->Draw("hist same");
    pK_mass_Pc2->ProjectionY()->Draw("hist same");
    legend_pK->Draw();
    t->DrawLatex(0.35,0.88, writetext00);
    t->DrawLatex(0.35,0.83, writetext01);
    pictures_path_cpp = pictures_path + "Bdwave_controlLb_pK_2d_ProjectionY.C";
    pictures_path_png = pictures_path + "Bdwave_controlLb_pK_2d_ProjectionY.png";
    c_Bdwave_controlLb_pKY->SaveAs(pictures_path_cpp.c_str());
    c_Bdwave_controlLb_pKY->SaveAs(pictures_path_png.c_str());
    
//____ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo____
// 4 track pictures
//____ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo________ooOO0OOoo____ 
    
    
    TCanvas *c_Min2_X_KpipiK = new TCanvas("c_Min2_X_KpipiK","fit #2_X",200,10,700,500);
    
    float n1 = 0., n2 = 0., n3 = 0., n4 = 0., n5 = 0., n6 = 0., n7 = 0., n8 = 0., n9 = 0., n10 = 0.;
    float n1_sa = 0., n2_sa = 0., n3_sa = 0., n4_sa = 0., n5_sa = 0., n6_sa = 0., n7_sa = 0., n8_sa = 0., n9_sa = 0., n10_sa = 0.;
    int n_bins = 0;
    
    Kpi_piK_2d_BS_to_JPSI_K_K->Scale(n_BS_KK / Kpi_piK_2d_BS_to_JPSI_K_K->Integral());
    Kpi_piK_2d_BD_to_JPSI_K_PI->Scale(n_BD_KPI / Kpi_piK_2d_BD_to_JPSI_K_PI->Integral());
    Kpi_piK_2d_BD_to_JPSI_PI_PI->Scale(n_BD_PIPI / Kpi_piK_2d_BD_to_JPSI_PI_PI->Integral());
    Kpi_piK_2d_BS_to_JPSI_PI_PI->Scale(n_BS_PIPI / Kpi_piK_2d_BS_to_JPSI_PI_PI->Integral());
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Scale(n_lambda / Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Integral());
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->Scale(n_LB_PPI / Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->Integral());
    Kpi_piK_2d_BD_to_JPSI_K_K->Scale(n_BD_KK / Kpi_piK_2d_BD_to_JPSI_K_K->Integral());
    Kpi_piK_2d_BS_to_JPSI_K_PI->Scale(n_BS_KPI / Kpi_piK_2d_BS_to_JPSI_K_PI->Integral());
    
    TFile *f_sas1 = new TFile("ROOT_files/signal_area_shape.root");
    TH2D *h_sas = (TH2D*)f_sas1->Get("area_hist");
    
    for(int i = 1; i <= Kpi_piK_2d_BD_to_JPSI_K_PI->ProjectionX()->GetNbinsX(); i++)
    {
        for(int j = 1; j <= Kpi_piK_2d_BD_to_JPSI_K_PI->ProjectionY()->GetNbinsX(); j++)
        {
            xx = Kpi_piK_2d_BD_to_JPSI_K_PI->ProjectionX()->GetBinCenter(i);
            yy = Kpi_piK_2d_BD_to_JPSI_K_PI->ProjectionY()->GetBinCenter(j);
            if ((5.25 < xx && xx < 5.31) || (5.25 < yy && yy < 5.31))
            {
                n1 += Kpi_piK_2d_BD_to_JPSI_K_PI->GetBinContent(i, j);
                n2 += Kpi_piK_2d_BS_to_JPSI_K_K->GetBinContent(i, j);
                n3 += Kpi_piK_2d_BS_to_JPSI_PI_PI->GetBinContent(i, j);
                n4 += Kpi_piK_2d_BD_to_JPSI_PI_PI->GetBinContent(i, j);
                n5 += Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, j);
                n6 += Kpi_piK_2d_bg->GetBinContent(i, j);
                n7 += Kpi_piK_2d_data->GetBinContent(i, j);
                n8 += Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, j);
                n9 += Kpi_piK_2d_BD_to_JPSI_K_K->GetBinContent(i, j);
                n10 += Kpi_piK_2d_BS_to_JPSI_K_PI->GetBinContent(i, j);
                n_bins++;
            }
            if (h_sas->GetBinContent(i, j) > 0.)
            {
                n1_sa += Kpi_piK_2d_BD_to_JPSI_K_PI->GetBinContent(i, j);
                n2_sa += Kpi_piK_2d_BS_to_JPSI_K_K->GetBinContent(i, j);
                n3_sa += Kpi_piK_2d_BS_to_JPSI_PI_PI->GetBinContent(i, j);
                n4_sa += Kpi_piK_2d_BD_to_JPSI_PI_PI->GetBinContent(i, j);
                n5_sa += Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, j);
                n6_sa += Kpi_piK_2d_bg->GetBinContent(i, j);
                n8_sa += Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, j);
                n9_sa += Kpi_piK_2d_BD_to_JPSI_K_K->GetBinContent(i, j);
                n10_sa += Kpi_piK_2d_BS_to_JPSI_K_PI->GetBinContent(i, j);
            }
        }
    }
    
    n_tot_test = n1 + n2 + n3 + n4 + n5 + n6 + n8 + n9 + n10;
    cout << "nbins \t" << n_bins << endl;
    cout << "\n" << n_tot_test << "\t" << n7 << endl;
    cout << "\nOLD:" << endl;
    cout << "\nOLD signal area:\n" << endl;
    cout << "n_BD_to_JPSI_K_PI\t" << n1 << "\t" << n1 / n_tot_test << endl;
    cout << "n_BS_to_JPSI_K_K\t" << n2 << "\t" << n2 / n_tot_test << endl;
    cout << "n_BS_to_JPSI_PI_PI\t" << n3 << "\t" << n3 / n_tot_test << endl;
    cout << "n_BD_to_JPSI_PI_PI\t" << n4 << "\t" << n4 / n_tot_test << endl;
    cout << "n_LAMBDA0B_to_JPSI_P_K\t" << n5 << "\t" << n5 / n_tot_test << endl;
    cout << "n_LAMBDA0B_to_JPSI_P_PI\t" << n8 << "\t" << n8 / n_tot_test << endl;
    cout << "n_BD_to_JPSI_K_K\t" << n9 << "\t" << n9 / n_tot_test << endl;
    cout << "n_BS_to_JPSI_K_PI\t" << n10 << "\t" << n10 / n_tot_test << endl;
    cout << "n_comb_bg\t" << n6 << "\t" << n6 / n_tot_test << endl; 
    cout << Kpi_piK_2d_data->ProjectionY()->GetNbinsX() << endl;
    cout << Kpi_piK_2d_data->ProjectionX()->GetNbinsX() << endl;
    
    n_tot_test = n1_sa + n2_sa + n3_sa + n4_sa + n5_sa + n6_sa + n8_sa + n9_sa + n10_sa;
    cout << "\nNEW:" << endl;
    cout << "\nNEW:\n" << endl;
    cout << "n_BD_to_JPSI_K_PI\t" << n1_sa << "\t" << n1_sa / n_tot_test << endl;
    cout << "n_BS_to_JPSI_K_K\t" << n2_sa << "\t" << n2_sa / n_tot_test << endl;
    cout << "n_BS_to_JPSI_PI_PI\t" << n3_sa << "\t" << n3_sa / n_tot_test << endl;
    cout << "n_BD_to_JPSI_PI_PI\t" << n4_sa << "\t" << n4_sa / n_tot_test << endl;
    cout << "n_LAMBDA0B_to_JPSI_P_K\t" << n5_sa << "\t" << n5_sa / n_tot_test << endl;
    cout << "n_LAMBDA0B_to_JPSI_P_PI\t" << n8_sa << "\t" << n8_sa / n_tot_test << endl;
    cout << "n_BD_to_JPSI_K_K\t" << n9_sa << "\t" << n9_sa / n_tot_test << endl;
    cout << "n_BS_to_JPSI_K_PI\t" << n10_sa << "\t" << n10_sa / n_tot_test << endl;
    cout << "n_comb_bg\t" << n6_sa << "\t" << n6_sa / n_tot_test << endl;
    
    //cout << "describing:\t" << (n_BD_to_JPSI_K_PI + n_BS_to_JPSI_K_K + n_BS_to_JPSI_PI_PI + n_BD_to_JPSI_PI_PI + n_LAMBDA0B_to_JPSI_P_K + n_comb_bg) / Jpsipi_2d_data->Integral() << endl;
    
    /*for(int i = 1; i <= pK_Kp_2d_test_data->ProjectionX()->GetNbinsX(); i++)
    {
        for(int j = 1; j <= pK_Kp_2d_test_data->ProjectionY()->GetNbinsX(); j++)
        {
            xx = pK_Kp_2d_test_data->ProjectionX()->GetBinCenter(i);
            yy = pK_Kp_2d_test_data->ProjectionY()->GetBinCenter(j);
            x = st * xx + st * yy - u0_pKKp;
            y = -st * xx + st * yy;
            if (xmin_pK < xx)
            {
                pK_Kp_2d_test_bg->SetBinContent(i, j, pow(abs(x - x0_pKKp), d0_pKKp) * exp(p0_pKKp + log(50./36.) + x * p1_pKKp + pow(x, 2.) * p2_pKKp + pow(x, 3.) * p3_pKKp) * exp(-1. * pow((y + a0_pKKp) / (b0_pKKp + x * b1_pKKp + pow(x, 2.) * b2_pKKp), 2.)));
            }
            else 
            {
                pK_Kp_2d_test_bg->SetBinContent(i, j, 0.);
            }
        }
    }*/
    
    Kpi_piK_2d_bg->SetFillColor(42);
    Kpi_piK_2d_BS_to_JPSI_K_K->SetFillColor(30);
    Kpi_piK_2d_BD_to_JPSI_K_PI->SetFillColor(7);
    Kpi_piK_2d_BD_to_JPSI_PI_PI->SetFillColor(8);
    Kpi_piK_2d_BS_to_JPSI_PI_PI->SetFillColor(46);
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->SetFillColor(95);
    Kpi_piK_2d_BD_to_JPSI_K_K->SetFillColor(12);
    Kpi_piK_2d_BS_to_JPSI_K_PI->SetFillColor(6);
    
    Kpi_piK_2d_data->SetMarkerStyle(kFullCircle);
    Kpi_piK_2d_data->SetMarkerSize(1.5);

    Kpi_piK_2d_BS_to_JPSI_K_K->Scale(n_BS_KK / Kpi_piK_2d_BS_to_JPSI_K_K->Integral());
    Kpi_piK_2d_BD_to_JPSI_K_PI->Scale(n_BD_KPI / Kpi_piK_2d_BD_to_JPSI_K_PI->Integral());
    Kpi_piK_2d_BD_to_JPSI_PI_PI->Scale(n_BD_PIPI / Kpi_piK_2d_BD_to_JPSI_PI_PI->Integral());
    Kpi_piK_2d_BS_to_JPSI_PI_PI->Scale(n_BS_PIPI / Kpi_piK_2d_BS_to_JPSI_PI_PI->Integral());
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Scale(n_lambda / Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Integral());
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->Scale(n_LB_PPI / Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->Integral());
    Kpi_piK_2d_BD_to_JPSI_K_K->Scale(n_BD_KK / Kpi_piK_2d_BD_to_JPSI_K_K->Integral());
    Kpi_piK_2d_BS_to_JPSI_K_PI->Scale(n_BS_KPI / Kpi_piK_2d_BS_to_JPSI_K_PI->Integral());
    
    THStack hs_Kpi("","");
    hs_Kpi.Add(Kpi_piK_2d_bg->ProjectionX());
    hs_Kpi.Add(Kpi_piK_2d_BS_to_JPSI_PI_PI->ProjectionX());
    hs_Kpi.Add(Kpi_piK_2d_BD_to_JPSI_PI_PI->ProjectionX());
    hs_Kpi.Add(Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->ProjectionX());
    hs_Kpi.Add(Kpi_piK_2d_BS_to_JPSI_K_K->ProjectionX());
    hs_Kpi.Add(Kpi_piK_2d_BD_to_JPSI_K_PI->ProjectionX());
    if (RARE_DECAYS)
    {   
        hs_Kpi.Add(Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionX());
        hs_Kpi.Add(Kpi_piK_2d_BD_to_JPSI_K_K->ProjectionX());
        hs_Kpi.Add(Kpi_piK_2d_BS_to_JPSI_K_PI->ProjectionX());
    }

    TLegend *l_Kpi = new TLegend(.60,.60,.90,.90);
    l_Kpi->AddEntry(Kpi_piK_2d_data, "data");
    l_Kpi->AddEntry(Kpi_piK_2d_BD_to_JPSI_K_PI, "B_{d} -> J/#psi K #pi");
    l_Kpi->AddEntry(Kpi_piK_2d_BS_to_JPSI_K_K, "B_{s} -> J/#psi K K");
    l_Kpi->AddEntry(Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K, "#Lambda_{b} -> J/#psi p K");
    l_Kpi->AddEntry(Kpi_piK_2d_BD_to_JPSI_PI_PI, "B_{d} -> J/#psi #pi #pi");
    l_Kpi->AddEntry(Kpi_piK_2d_BS_to_JPSI_PI_PI, "B_{s} -> J/#psi #pi #pi");
    if (RARE_DECAYS)
    {   
        l_Kpi->AddEntry(Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI, "#Lambda_{b} -> J/#psi p #pi");
        l_Kpi->AddEntry(Kpi_piK_2d_BD_to_JPSI_K_K, "B_{d} -> J/#psi K K");
        l_Kpi->AddEntry(Kpi_piK_2d_BS_to_JPSI_K_PI, "B_{s} -> J/#psi K #pi");
    }
    l_Kpi->AddEntry(Kpi_piK_2d_bg, "comb background");
    
    hist_buffer = (TH1D*)Kpi_piK_2d_data->ProjectionX()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("m(J/#psi h_{1}=K h_{2}=#pi), GeV");
    hist_buffer->GetYaxis()->SetTitleOffset(1.35);
    hist_buffer->GetYaxis()->SetTitle("Events / 20 MeV");
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    hist_buffer->Draw("");
    hs_Kpi.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    l_Kpi->Draw("same");
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = pictures_path + "MinAll_X_KpipiK.C";
    pictures_path_png = pictures_path + "MinAll_X_KpipiK.png";
    c_Min2_X_KpipiK->SaveAs(pictures_path_cpp.c_str());
    c_Min2_X_KpipiK->SaveAs(pictures_path_png.c_str());

    TCanvas *c_Min2_Y_KpipiK = new TCanvas("c_Min2_Y_KpipiK","fit #2_Y",200,10,700,500);

    THStack hs_piK("","");
    hs_piK.Add(Kpi_piK_2d_bg->ProjectionY());
    hs_piK.Add(Kpi_piK_2d_BS_to_JPSI_PI_PI->ProjectionY());
    hs_piK.Add(Kpi_piK_2d_BD_to_JPSI_PI_PI->ProjectionY());
    hs_piK.Add(Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->ProjectionY());
    hs_piK.Add(Kpi_piK_2d_BS_to_JPSI_K_K->ProjectionY());
    hs_piK.Add(Kpi_piK_2d_BD_to_JPSI_K_PI->ProjectionY());
    if (RARE_DECAYS)
    {   
        hs_piK.Add(Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionY());
        hs_piK.Add(Kpi_piK_2d_BD_to_JPSI_K_K->ProjectionY());
        hs_piK.Add(Kpi_piK_2d_BS_to_JPSI_K_PI->ProjectionY());
    }
    
    hist_buffer = (TH1D*)Kpi_piK_2d_data->ProjectionY()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("m(J/#psi h_{1}=#pi h_{2}=K), GeV");
    hist_buffer->GetYaxis()->SetTitleOffset(1.35);
    hist_buffer->GetYaxis()->SetTitle("Events / 20 MeV");
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_piK.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    l_Kpi->Draw("same");
    pictures_path_cpp = pictures_path + "MinAll_Y_KpipiK.C";
    pictures_path_png = pictures_path + "MinAll_Y_KpipiK.png";
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    c_Min2_Y_KpipiK->SaveAs(pictures_path_cpp.c_str());
    c_Min2_Y_KpipiK->SaveAs(pictures_path_png.c_str());
    
    TCanvas *c_Min2_KK = new TCanvas("c_Min2_KK","fit KK",200,10,700,500);
    
    KK_1d_bg->SetFillColor(42);
    KK_1d_BS_to_JPSI_K_K->SetFillColor(30);
    KK_1d_BD_to_JPSI_K_PI->SetFillColor(7);
    KK_1d_BD_to_JPSI_PI_PI->SetFillColor(8);
    KK_1d_BS_to_JPSI_PI_PI->SetFillColor(46);
    KK_1d_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    KK_1d_LAMBDA0B_to_JPSI_P_PI->SetFillColor(95);
    KK_1d_BD_to_JPSI_K_K->SetFillColor(12);
    KK_1d_BS_to_JPSI_K_PI->SetFillColor(6);
    KK_1d_data->SetMarkerStyle(kFullCircle);
    KK_1d_data->SetMarkerSize(0.5);

    KK_1d_BS_to_JPSI_K_K->Scale(n_BS_KK / KK_1d_BS_to_JPSI_K_K->Integral());
    KK_1d_BD_to_JPSI_K_PI->Scale(n_BD_KPI / KK_1d_BD_to_JPSI_K_PI->Integral());
    KK_1d_BD_to_JPSI_PI_PI->Scale(n_BD_PIPI / KK_1d_BD_to_JPSI_PI_PI->Integral());
    KK_1d_BS_to_JPSI_PI_PI->Scale(n_BS_PIPI / KK_1d_BS_to_JPSI_PI_PI->Integral());
    KK_1d_LAMBDA0B_to_JPSI_P_K->Scale(n_lambda / KK_1d_LAMBDA0B_to_JPSI_P_K->Integral());
    KK_1d_LAMBDA0B_to_JPSI_P_PI->Scale(n_LB_PPI / KK_1d_LAMBDA0B_to_JPSI_P_PI->Integral());
    KK_1d_BD_to_JPSI_K_K->Scale(n_BD_KK / KK_1d_BD_to_JPSI_K_K->Integral());
    KK_1d_BS_to_JPSI_K_PI->Scale(n_BS_KPI / KK_1d_BS_to_JPSI_K_PI->Integral());
    
    THStack hs_KK("","");
    hs_KK.Add(KK_1d_bg);
    hs_KK.Add(KK_1d_BS_to_JPSI_PI_PI);
    hs_KK.Add(KK_1d_BD_to_JPSI_PI_PI);
    hs_KK.Add(KK_1d_LAMBDA0B_to_JPSI_P_K);
    hs_KK.Add(KK_1d_BS_to_JPSI_K_K);
    hs_KK.Add(KK_1d_BD_to_JPSI_K_PI);
    if (RARE_DECAYS)
    {   
        hs_KK.Add(KK_1d_LAMBDA0B_to_JPSI_P_PI);
        hs_KK.Add(KK_1d_BD_to_JPSI_K_K);
        hs_KK.Add(KK_1d_BS_to_JPSI_K_PI);
    }
    
    KK_1d_data->SetStats(kFALSE);
    KK_1d_data->SetTitle("");
    KK_1d_data->GetXaxis()->SetTitle("m(J/#psi h_{1}=K h_{2}=K), GeV");
    KK_1d_data->GetYaxis()->SetTitle("Events / 10.3 MeV");
    KK_1d_data->GetYaxis()->SetRangeUser(0., KK_1d_data->GetMaximum() * 1.1);
    
    KK_1d_data->Draw("");
    hs_KK.Draw("hist same");
    KK_1d_data->Draw("same");
    KK_1d_data->Draw("same axis");
    l_Kpi->Draw("same");
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = pictures_path + "MinAll_KK.C";
    pictures_path_png = pictures_path + "MinAll_KK.png";
    c_Min2_KK->SaveAs(pictures_path_cpp.c_str());
    c_Min2_KK->SaveAs(pictures_path_png.c_str());
    
    TCanvas *c_Min2_pipi = new TCanvas("c_Min2_pipi","fit pipi",200,10,700,500);
    
    pipi_1d_bg->SetFillColor(42);
    pipi_1d_BS_to_JPSI_K_K->SetFillColor(30);
    pipi_1d_BD_to_JPSI_K_PI->SetFillColor(7);
    pipi_1d_BD_to_JPSI_PI_PI->SetFillColor(8);
    pipi_1d_BS_to_JPSI_PI_PI->SetFillColor(46);
    pipi_1d_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    pipi_1d_LAMBDA0B_to_JPSI_P_PI->SetFillColor(95);
    pipi_1d_BD_to_JPSI_K_K->SetFillColor(12);
    pipi_1d_BS_to_JPSI_K_PI->SetFillColor(6);
    pipi_1d_data->SetMarkerStyle(kFullCircle);
    pipi_1d_data->SetMarkerSize(0.5);
    pipi_1d_data->GetYaxis()->SetRangeUser(0., 5000.);

    pipi_1d_BS_to_JPSI_K_K->Scale(n_BS_KK / pipi_1d_BS_to_JPSI_K_K->Integral());
    pipi_1d_BD_to_JPSI_K_PI->Scale(n_BD_KPI / pipi_1d_BD_to_JPSI_K_PI->Integral());
    pipi_1d_BD_to_JPSI_PI_PI->Scale(n_BD_PIPI / pipi_1d_BD_to_JPSI_PI_PI->Integral());
    pipi_1d_BS_to_JPSI_PI_PI->Scale(n_BS_PIPI / pipi_1d_BS_to_JPSI_PI_PI->Integral());
    pipi_1d_LAMBDA0B_to_JPSI_P_K->Scale(n_lambda / pipi_1d_LAMBDA0B_to_JPSI_P_K->Integral());
    pipi_1d_LAMBDA0B_to_JPSI_P_PI->Scale(n_LB_PPI / pipi_1d_LAMBDA0B_to_JPSI_P_PI->Integral());
    pipi_1d_BD_to_JPSI_K_K->Scale(n_BD_KK / pipi_1d_BD_to_JPSI_K_K->Integral());
    pipi_1d_BS_to_JPSI_K_PI->Scale(n_BS_KPI / pipi_1d_BS_to_JPSI_K_PI->Integral());
    
    THStack hs_pipi("","");
    hs_pipi.Add(pipi_1d_bg);
    hs_pipi.Add(pipi_1d_BS_to_JPSI_PI_PI);
    hs_pipi.Add(pipi_1d_BD_to_JPSI_PI_PI);
    hs_pipi.Add(pipi_1d_LAMBDA0B_to_JPSI_P_K);
    hs_pipi.Add(pipi_1d_BS_to_JPSI_K_K);
    hs_pipi.Add(pipi_1d_BD_to_JPSI_K_PI);
    if (RARE_DECAYS)
    {   
        hs_pipi.Add(pipi_1d_LAMBDA0B_to_JPSI_P_PI);
        hs_pipi.Add(pipi_1d_BD_to_JPSI_K_K);
        hs_pipi.Add(pipi_1d_BS_to_JPSI_K_PI);
    }
    //hs_pipi.GetYaxis()->SetRangeUser(0., 5000.);
    
    pipi_1d_data->SetStats(kFALSE);
    pipi_1d_data->SetTitle("");
    pipi_1d_data->GetXaxis()->SetTitle("m(J/#psi h_{1}=#pi h_{2}=#pi), GeV");
    pipi_1d_data->GetYaxis()->SetTitle("Events / 11 MeV");
    pipi_1d_data->GetYaxis()->SetRangeUser(0., pipi_1d_data->GetMaximum() * 1.1);
    
    pipi_1d_data->Draw("");
    hs_pipi.Draw("hist same");
    pipi_1d_data->Draw("same");
    pipi_1d_data->Draw("same axis");
    l_Kpi->Draw("same");
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = pictures_path + "MinAll_pipi.C";
    pictures_path_png = pictures_path + "MinAll_pipi.png";
    c_Min2_pipi->SaveAs(pictures_path_cpp.c_str());
    c_Min2_pipi->SaveAs(pictures_path_png.c_str());
    
    pK_Kp_2d_bg->SetFillColor(42);
    pK_Kp_2d_BS_to_JPSI_K_K->SetFillColor(30);
    pK_Kp_2d_BD_to_JPSI_K_PI->SetFillColor(7);
    pK_Kp_2d_BD_to_JPSI_PI_PI->SetFillColor(8);
    pK_Kp_2d_BS_to_JPSI_PI_PI->SetFillColor(46);
    pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI->SetFillColor(95);
    pK_Kp_2d_BD_to_JPSI_K_K->SetFillColor(12);
    pK_Kp_2d_BS_to_JPSI_K_PI->SetFillColor(6);
    pK_Kp_2d_data->SetMarkerStyle(kFullCircle);
    pK_Kp_2d_data->SetMarkerSize(1.5);
    
    TCanvas *c_Min2_X_pKKp = new TCanvas("c_Min2_X_pKKp","fit #2_X",200,10,700,500);
    
    pK_Kp_2d_BS_to_JPSI_K_K->Scale(n_BS_KK / pK_Kp_2d_BS_to_JPSI_K_K->Integral());
    pK_Kp_2d_BD_to_JPSI_K_PI->Scale(n_BD_KPI / pK_Kp_2d_BD_to_JPSI_K_PI->Integral());
    pK_Kp_2d_BD_to_JPSI_PI_PI->Scale(n_BD_PIPI / pK_Kp_2d_BD_to_JPSI_PI_PI->Integral());
    pK_Kp_2d_BS_to_JPSI_PI_PI->Scale(n_BS_PIPI / pK_Kp_2d_BS_to_JPSI_PI_PI->Integral());
    pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->Scale(n_lambda / pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->Integral());
    pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI->Scale(n_LB_PPI / pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI->Integral());
    pK_Kp_2d_BD_to_JPSI_K_K->Scale(n_BD_KK / pK_Kp_2d_BD_to_JPSI_K_K->Integral());
    pK_Kp_2d_BS_to_JPSI_K_PI->Scale(n_BS_KPI / pK_Kp_2d_BS_to_JPSI_K_PI->Integral());
    
    THStack hs_pK("","");
    hs_pK.Add(pK_Kp_2d_bg->ProjectionX());
    hs_pK.Add(pK_Kp_2d_BS_to_JPSI_PI_PI->ProjectionX());
    hs_pK.Add(pK_Kp_2d_BD_to_JPSI_PI_PI->ProjectionX());
    hs_pK.Add(pK_Kp_2d_BS_to_JPSI_K_K->ProjectionX());
    hs_pK.Add(pK_Kp_2d_BD_to_JPSI_K_PI->ProjectionX());
    hs_pK.Add(pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->ProjectionX());
    if (RARE_DECAYS)
    {   
        hs_pK.Add(pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionX());
        hs_pK.Add(pK_Kp_2d_BD_to_JPSI_K_K->ProjectionX());
        hs_pK.Add(pK_Kp_2d_BS_to_JPSI_K_PI->ProjectionX());
    }
    
    hist_buffer = (TH1D*)pK_Kp_2d_data->ProjectionX()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("m(J/#psi h_{1}=p h_{2}=K), GeV");
    hist_buffer->GetYaxis()->SetTitle("Events / 25 MeV");
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_pK.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    l_Kpi->Draw("same");
    t->DrawLatex(0.6,0.55, writetext00);
    t->DrawLatex(0.6,0.5, writetext01);
    pictures_path_cpp = pictures_path + "MinAll_X_pKKp.C";
    pictures_path_png = pictures_path + "MinAll_X_pKKp.png";
    c_Min2_X_pKKp->SaveAs(pictures_path_cpp.c_str());
    c_Min2_X_pKKp->SaveAs(pictures_path_png.c_str());
    
    cout << "pK_Kp_2d_bg\t" << pK_Kp_2d_bg->Integral() << endl;
    cout << "pK_Kp_2d_BS_to_JPSI_PI_PI\t" << pK_Kp_2d_BS_to_JPSI_PI_PI->Integral() << endl;
    cout << "pK_Kp_2d_BD_to_JPSI_PI_PI\t" << pK_Kp_2d_BD_to_JPSI_PI_PI->Integral() << endl;
    cout << "pK_Kp_2d_BS_to_JPSI_K_K\t" << pK_Kp_2d_BS_to_JPSI_K_K->Integral() << endl;
    cout << "pK_Kp_2d_BD_to_JPSI_K_PI\t" << pK_Kp_2d_BD_to_JPSI_K_PI->Integral() << endl;
    cout << "pK_Kp_2d_LAMBDA0B_to_JPSI_P_K\t" << pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->Integral() << endl;

    TCanvas *c_Min2_Y_pKKp = new TCanvas("c_Min2_Y_pKKp","fit #2_Y",200,10,700,500);

    THStack hs_Kp("","");
    hs_Kp.Add(pK_Kp_2d_bg->ProjectionY());
    hs_Kp.Add(pK_Kp_2d_BS_to_JPSI_PI_PI->ProjectionY());
    hs_Kp.Add(pK_Kp_2d_BD_to_JPSI_PI_PI->ProjectionY());
    hs_Kp.Add(pK_Kp_2d_BS_to_JPSI_K_K->ProjectionY());
    hs_Kp.Add(pK_Kp_2d_BD_to_JPSI_K_PI->ProjectionY()); 
    hs_Kp.Add(pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->ProjectionY());
    if (RARE_DECAYS)
    {   
        hs_Kp.Add(pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionY());
        hs_Kp.Add(pK_Kp_2d_BD_to_JPSI_K_K->ProjectionY());
        hs_Kp.Add(pK_Kp_2d_BS_to_JPSI_K_PI->ProjectionY());
    }
    
    hist_buffer = (TH1D*)pK_Kp_2d_data->ProjectionY()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("m(J/#psi h_{1}=K h_{2}=p), GeV");
    hist_buffer->GetYaxis()->SetTitle("Events / 25 MeV");
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_Kp.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    l_Kpi->Draw("same");
    t->DrawLatex(0.6,0.55, writetext00);
    t->DrawLatex(0.6,0.5, writetext01);
    pictures_path_cpp = pictures_path + "MinAll_Y_pKKp.C";
    pictures_path_png = pictures_path + "MinAll_Y_pKKp.png";
    c_Min2_Y_pKKp->SaveAs(pictures_path_cpp.c_str());
    c_Min2_Y_pKKp->SaveAs(pictures_path_png.c_str());
    
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
    
    pK_Kp_2d_rotate_bg->SetFillColor(42);
    pK_Kp_2d_rotate_BS_to_JPSI_K_K->SetFillColor(30);
    pK_Kp_2d_rotate_BD_to_JPSI_K_PI->SetFillColor(7);
    pK_Kp_2d_rotate_BD_to_JPSI_PI_PI->SetFillColor(8);
    pK_Kp_2d_rotate_BS_to_JPSI_PI_PI->SetFillColor(46);
    pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_PI->SetFillColor(95);
    pK_Kp_2d_rotate_BD_to_JPSI_K_K->SetFillColor(12);
    pK_Kp_2d_rotate_BS_to_JPSI_K_PI->SetFillColor(6);
    pK_Kp_2d_rotate_data->SetMarkerStyle(kFullCircle);
    pK_Kp_2d_rotate_data->SetMarkerSize(1.5);
    
    pK_Kp_2d_rotate_BS_to_JPSI_K_K->Scale(n_BS_KK / pK_Kp_2d_rotate_BS_to_JPSI_K_K->Integral());
    pK_Kp_2d_rotate_BD_to_JPSI_K_PI->Scale(n_BD_KPI / pK_Kp_2d_rotate_BD_to_JPSI_K_PI->Integral());
    pK_Kp_2d_rotate_BD_to_JPSI_PI_PI->Scale(n_BD_PIPI / pK_Kp_2d_rotate_BD_to_JPSI_PI_PI->Integral());
    pK_Kp_2d_rotate_BS_to_JPSI_PI_PI->Scale(n_BS_PIPI / pK_Kp_2d_rotate_BS_to_JPSI_PI_PI->Integral());
    pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_K->Scale(n_lambda / pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_K->Integral());
    pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_PI->Scale(n_LB_PPI / pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_PI->Integral());
    pK_Kp_2d_rotate_BD_to_JPSI_K_K->Scale(n_BD_KK / pK_Kp_2d_rotate_BD_to_JPSI_K_K->Integral());
    pK_Kp_2d_rotate_BS_to_JPSI_K_PI->Scale(n_BS_KPI / pK_Kp_2d_rotate_BS_to_JPSI_K_PI->Integral());
    
    TCanvas *c_Min_X_pKKp_rotate = new TCanvas("c_Min_X_pKKp_rotate","fit #2_X");
    
    THStack hs_pK_rotate("","");
    hs_pK_rotate.Add(pK_Kp_2d_rotate_bg->ProjectionX());
    hs_pK_rotate.Add(pK_Kp_2d_rotate_BS_to_JPSI_PI_PI->ProjectionX());
    hs_pK_rotate.Add(pK_Kp_2d_rotate_BD_to_JPSI_PI_PI->ProjectionX());
    hs_pK_rotate.Add(pK_Kp_2d_rotate_BS_to_JPSI_K_K->ProjectionX());
    hs_pK_rotate.Add(pK_Kp_2d_rotate_BD_to_JPSI_K_PI->ProjectionX());
    hs_pK_rotate.Add(pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_K->ProjectionX());
	if (RARE_DECAYS)
    {   
        hs_pK_rotate.Add(pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_PI->ProjectionX());
        hs_pK_rotate.Add(pK_Kp_2d_rotate_BD_to_JPSI_K_K->ProjectionX());
        hs_pK_rotate.Add(pK_Kp_2d_rotate_BS_to_JPSI_K_PI->ProjectionX());
    }
    
    hist_buffer = (TH1D*)pK_Kp_2d_rotate_data->ProjectionX()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("m(J/#psi h_{1}=p h_{2}=K)_{rotate}, GeV");
    hist_buffer->GetYaxis()->SetTitle("Events / 42 MeV");
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_pK_rotate.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    l_Kpi->Draw("same");
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = pictures_path + "MinAll_X_pKKp_rotate.C";
    pictures_path_png = pictures_path + "MinAll_X_pKKp_rotate.png";
    c_Min_X_pKKp_rotate->SaveAs(pictures_path_cpp.c_str());
    c_Min_X_pKKp_rotate->SaveAs(pictures_path_png.c_str());

    TCanvas *c_Min_Y_pKKp_rotate = new TCanvas("c_Min_Y_pKKp_rotate","fit #2_Y",200,10,700,500);

    THStack hs_Kp_rotate("","");
    hs_Kp_rotate.Add(pK_Kp_2d_rotate_bg->ProjectionY());
    hs_Kp_rotate.Add(pK_Kp_2d_rotate_BS_to_JPSI_PI_PI->ProjectionY());
    hs_Kp_rotate.Add(pK_Kp_2d_rotate_BD_to_JPSI_PI_PI->ProjectionY());
    hs_Kp_rotate.Add(pK_Kp_2d_rotate_BS_to_JPSI_K_K->ProjectionY());
    hs_Kp_rotate.Add(pK_Kp_2d_rotate_BD_to_JPSI_K_PI->ProjectionY()); 
    hs_Kp_rotate.Add(pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_K->ProjectionY());
	if (RARE_DECAYS)
    {   
        hs_Kp_rotate.Add(pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_PI->ProjectionY());
        hs_Kp_rotate.Add(pK_Kp_2d_rotate_BD_to_JPSI_K_K->ProjectionY());
        hs_Kp_rotate.Add(pK_Kp_2d_rotate_BS_to_JPSI_K_PI->ProjectionY());
    }
    
    hist_buffer = (TH1D*)pK_Kp_2d_rotate_data->ProjectionY()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("m(J/#psi h_{1}=K h_{2}=p)_{rotate}, GeV");
    hist_buffer->GetYaxis()->SetTitleOffset(1.35);
    hist_buffer->GetYaxis()->SetTitle("Events / 38 MeV");
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_Kp_rotate.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    l_Kpi->Draw("same");
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = pictures_path + "MinAll_Y_pKKp_rotate.C";
    pictures_path_png = pictures_path + "MinAll_Y_pKKp_rotate.png";
    c_Min_Y_pKKp_rotate->SaveAs(pictures_path_cpp.c_str());
    c_Min_Y_pKKp_rotate->SaveAs(pictures_path_png.c_str());
    
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
    
    ppi_pip_2d_bg->SetFillColor(42);
    ppi_pip_2d_BS_to_JPSI_K_K->SetFillColor(30);
    ppi_pip_2d_BD_to_JPSI_K_PI->SetFillColor(7);
    ppi_pip_2d_BD_to_JPSI_PI_PI->SetFillColor(8);
    ppi_pip_2d_BS_to_JPSI_PI_PI->SetFillColor(46);
    ppi_pip_2d_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    ppi_pip_2d_LAMBDA0B_to_JPSI_P_PI->SetFillColor(95);
    ppi_pip_2d_BD_to_JPSI_K_K->SetFillColor(12);
    ppi_pip_2d_BS_to_JPSI_K_PI->SetFillColor(6);
    ppi_pip_2d_data->SetMarkerStyle(kFullCircle);
    ppi_pip_2d_data->SetMarkerSize(1.5);
    
    ppi_pip_2d_BS_to_JPSI_K_K->Scale(n_BS_KK / ppi_pip_2d_BS_to_JPSI_K_K->Integral());
    ppi_pip_2d_BD_to_JPSI_K_PI->Scale(n_BD_KPI / ppi_pip_2d_BD_to_JPSI_K_PI->Integral());
    ppi_pip_2d_BD_to_JPSI_PI_PI->Scale(n_BD_PIPI / ppi_pip_2d_BD_to_JPSI_PI_PI->Integral());
    ppi_pip_2d_BS_to_JPSI_PI_PI->Scale(n_BS_PIPI / ppi_pip_2d_BS_to_JPSI_PI_PI->Integral());
    ppi_pip_2d_LAMBDA0B_to_JPSI_P_K->Scale(n_lambda / ppi_pip_2d_LAMBDA0B_to_JPSI_P_K->Integral());
    ppi_pip_2d_LAMBDA0B_to_JPSI_P_PI->Scale(n_LB_PPI / ppi_pip_2d_LAMBDA0B_to_JPSI_P_PI->Integral());
    ppi_pip_2d_BD_to_JPSI_K_K->Scale(n_BD_KK / ppi_pip_2d_BD_to_JPSI_K_K->Integral());
    ppi_pip_2d_BS_to_JPSI_K_PI->Scale(n_BS_KPI / ppi_pip_2d_BS_to_JPSI_K_PI->Integral());
    
    TCanvas *c_Min_X_ppipip = new TCanvas("c_Min_X_ppipip","fit #2_X");
    
    THStack hs_ppi("","");
    hs_ppi.Add(ppi_pip_2d_bg->ProjectionX()); 
    hs_ppi.Add(ppi_pip_2d_BS_to_JPSI_PI_PI->ProjectionX());
    hs_ppi.Add(ppi_pip_2d_BD_to_JPSI_PI_PI->ProjectionX());
    hs_ppi.Add(ppi_pip_2d_BS_to_JPSI_K_K->ProjectionX());
    hs_ppi.Add(ppi_pip_2d_BD_to_JPSI_K_PI->ProjectionX());
    hs_ppi.Add(ppi_pip_2d_LAMBDA0B_to_JPSI_P_K->ProjectionX());
	if (RARE_DECAYS)
    {   
        hs_ppi.Add(ppi_pip_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionX());
        hs_ppi.Add(ppi_pip_2d_BD_to_JPSI_K_K->ProjectionX());
        hs_ppi.Add(ppi_pip_2d_BS_to_JPSI_K_PI->ProjectionX());
    }
    
    hist_buffer = (TH1D*)ppi_pip_2d_data->ProjectionX()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("m(J/#psi h_{1}=p h_{2}=#pi), GeV");
    hist_buffer->GetYaxis()->SetTitle("Events / 25 MeV");
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_ppi.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    l_Kpi->Draw("same");
    t->DrawLatex(0.6,0.55, writetext00);
    t->DrawLatex(0.6,0.5, writetext01);
    pictures_path_cpp = pictures_path + "MinAll_X_ppipip.C";
    pictures_path_png = pictures_path + "MinAll_X_ppipip.png";
    c_Min_X_ppipip->SaveAs(pictures_path_cpp.c_str());
    c_Min_X_ppipip->SaveAs(pictures_path_png.c_str());

    TCanvas *c_Min_Y_ppipip = new TCanvas("c_Min_Y_ppipip","fit #2_Y",200,10,700,500);

    THStack hs_pip("","");
    hs_pip.Add(ppi_pip_2d_bg->ProjectionY()); 
    hs_pip.Add(ppi_pip_2d_BS_to_JPSI_PI_PI->ProjectionY());
    hs_pip.Add(ppi_pip_2d_BD_to_JPSI_PI_PI->ProjectionY());
    hs_pip.Add(ppi_pip_2d_BS_to_JPSI_K_K->ProjectionY());
    hs_pip.Add(ppi_pip_2d_BD_to_JPSI_K_PI->ProjectionY()); 
    hs_pip.Add(ppi_pip_2d_LAMBDA0B_to_JPSI_P_K->ProjectionY());
	if (RARE_DECAYS)
    {   
        hs_pip.Add(ppi_pip_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionY());
        hs_pip.Add(ppi_pip_2d_BD_to_JPSI_K_K->ProjectionY());
        hs_pip.Add(ppi_pip_2d_BS_to_JPSI_K_PI->ProjectionY());
    }
    
    hist_buffer = (TH1D*)ppi_pip_2d_data->ProjectionY()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("m(J/#psi h_{1}=#pi h_{2}=p), GeV");
    hist_buffer->GetYaxis()->SetTitleOffset(1.35);
    hist_buffer->GetYaxis()->SetTitle("Events / 25 MeV");
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_pip.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    l_Kpi->Draw("same");
    t->DrawLatex(0.6,0.55, writetext00);
    t->DrawLatex(0.6,0.5, writetext01);
    pictures_path_cpp = pictures_path + "MinAll_Y_ppipip.C";
    pictures_path_png = pictures_path + "MinAll_Y_ppipip.png";
    c_Min_Y_ppipip->SaveAs(pictures_path_cpp.c_str());
    c_Min_Y_ppipip->SaveAs(pictures_path_png.c_str());
    
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
    
    /*TCanvas *c_Min2_X_pKKp_test = new TCanvas("c_Min2_X_pKKp_test","fit #2_X",200,10,700,500);
    
    pK_Kp_2d_test_BS_to_JPSI_K_K->Scale(n_BS_KK / pK_Kp_2d_test_BS_to_JPSI_K_K->Integral());
    pK_Kp_2d_test_BD_to_JPSI_K_PI->Scale(n_BD_KPI / pK_Kp_2d_test_BD_to_JPSI_K_PI->Integral());
    pK_Kp_2d_test_BD_to_JPSI_PI_PI->Scale(n_BD_PIPI / pK_Kp_2d_test_BD_to_JPSI_PI_PI->Integral());
    pK_Kp_2d_test_BS_to_JPSI_PI_PI->Scale(n_BS_PIPI / pK_Kp_2d_test_BS_to_JPSI_PI_PI->Integral());
    pK_Kp_2d_test_LAMBDA0B_to_JPSI_P_K->Scale(n_lambda / pK_Kp_2d_test_LAMBDA0B_to_JPSI_P_K->Integral());
    
    THStack hs_pK_test("","");
    hs_pK_test.Add(pK_Kp_2d_test_bg->ProjectionX());
    hs_pK_test.Add(pK_Kp_2d_test_BS_to_JPSI_PI_PI->ProjectionX());
    hs_pK_test.Add(pK_Kp_2d_test_BD_to_JPSI_PI_PI->ProjectionX());
    hs_pK_test.Add(pK_Kp_2d_test_BS_to_JPSI_K_K->ProjectionX());
    hs_pK_test.Add(pK_Kp_2d_test_BD_to_JPSI_K_PI->ProjectionX());
    hs_pK_test.Add(pK_Kp_2d_test_LAMBDA0B_to_JPSI_P_K->ProjectionX());

    hs_pK_test.Draw("hist");
    pK_Kp_2d_test_data->ProjectionX()->Draw("same");
    l_Kpi->Draw("same");
    c_Min2_X_pKKp_test->SaveAs("pictures/MinAll_X_pKKp_test.png");
    c_Min2_X_pKKp_test->SaveAs("pictures/MinAll_X_pKKp_test.C");

    TCanvas *c_Min2_Y_pKKp_test = new TCanvas("c_Min2_Y_pKKp_test","fit #2_Y",200,10,700,500);

    THStack hs_Kp_test("","");
    hs_Kp_test.Add(pK_Kp_2d_test_bg->ProjectionY());
    hs_Kp_test.Add(pK_Kp_2d_test_BS_to_JPSI_PI_PI->ProjectionY());
    hs_Kp_test.Add(pK_Kp_2d_test_BD_to_JPSI_PI_PI->ProjectionY());
    hs_Kp_test.Add(pK_Kp_2d_test_BS_to_JPSI_K_K->ProjectionY());
    hs_Kp_test.Add(pK_Kp_2d_test_BD_to_JPSI_K_PI->ProjectionY()); 
    hs_Kp_test.Add(pK_Kp_2d_test_LAMBDA0B_to_JPSI_P_K->ProjectionY());

    hs_Kp_test.Draw("hist");
    pK_Kp_2d_test_data->ProjectionY()->Draw("same");
    l_Kpi->Draw("same");
    c_Min2_Y_pKKp_test->SaveAs("pictures/MinAll_Y_pKKp_test.png");
    c_Min2_Y_pKKp_test->SaveAs("pictures/MinAll_Y_pKKp_test.C");*/
    
    //Kpi_piK_2d_data->ProjectionY()->Draw();
    
    TH2D* Kpi_piK_2d_BS_to_JPSI_K_K_for_ea = (TH2D*)Kpi_piK_2d_BS_to_JPSI_K_K->Clone("Kpi_piK_2d_BS_to_JPSI_K_K_for_ea");
    TH2D* Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea = (TH2D*)Kpi_piK_2d_BD_to_JPSI_K_PI->Clone("Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea");
    TH2D* Kpi_piK_2d_BD_to_JPSI_PI_PI_for_ea = (TH2D*)Kpi_piK_2d_BD_to_JPSI_PI_PI->Clone("Kpi_piK_2d_BD_to_JPSI_PI_PI_for_ea");
    TH2D* Kpi_piK_2d_BS_to_JPSI_PI_PI_for_ea = (TH2D*)Kpi_piK_2d_BS_to_JPSI_PI_PI->Clone("Kpi_piK_2d_BS_to_JPSI_PI_PI_for_ea");
    TH2D* Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K_for_ea = (TH2D*)Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Clone("Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K_for_ea");
    TH2D* Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI_for_ea = (TH2D*)Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->Clone("Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI_for_ea");
    TH2D* Kpi_piK_2d_BD_to_JPSI_K_K_for_ea = (TH2D*)Kpi_piK_2d_BD_to_JPSI_K_K->Clone("Kpi_piK_2d_BD_to_JPSI_K_K_for_ea");
    TH2D* Kpi_piK_2d_BS_to_JPSI_K_PI_for_ea = (TH2D*)Kpi_piK_2d_BS_to_JPSI_K_PI->Clone("Kpi_piK_2d_BS_to_JPSI_K_PI_for_ea");
    TH2D* Kpi_piK_2d_full_for_ea = (TH2D*)Kpi_piK_2d_BS_to_JPSI_K_K->Clone("Kpi_piK_2d_full_for_ea");
    TH2D* Kpi_piK_2d_bg_for_ea = (TH2D*)Kpi_piK_2d_bg->Clone("Kpi_piK_2d_bg_for_ea");
    
    Kpi_piK_2d_full_for_ea->Add(Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea);
    Kpi_piK_2d_full_for_ea->Add(Kpi_piK_2d_BD_to_JPSI_PI_PI_for_ea);
    Kpi_piK_2d_full_for_ea->Add(Kpi_piK_2d_BS_to_JPSI_PI_PI_for_ea);
    Kpi_piK_2d_full_for_ea->Add(Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K_for_ea);
    Kpi_piK_2d_full_for_ea->Add(Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI_for_ea);
    Kpi_piK_2d_full_for_ea->Add(Kpi_piK_2d_BD_to_JPSI_K_K_for_ea);
    Kpi_piK_2d_full_for_ea->Add(Kpi_piK_2d_BS_to_JPSI_K_PI_for_ea);
    Kpi_piK_2d_full_for_ea->Add(Kpi_piK_2d_bg_for_ea);
    
    Kpi_piK_2d_BS_to_JPSI_K_K_for_ea->SetTitle("Kpi_piK_2d_BS_to_JPSI_K_K_for_ea");
    Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea->SetTitle("Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea");
    Kpi_piK_2d_BD_to_JPSI_PI_PI_for_ea->SetTitle("Kpi_piK_2d_BD_to_JPSI_PI_PI_for_ea");
    Kpi_piK_2d_BS_to_JPSI_PI_PI_for_ea->SetTitle("Kpi_piK_2d_BS_to_JPSI_PI_PI_for_ea");
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K_for_ea->SetTitle("Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K_for_ea");
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI_for_ea->SetTitle("Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI_for_ea");
    Kpi_piK_2d_BD_to_JPSI_K_K_for_ea->SetTitle("Kpi_piK_2d_BD_to_JPSI_K_K_for_ea");
    Kpi_piK_2d_BS_to_JPSI_K_PI_for_ea->SetTitle("Kpi_piK_2d_BS_to_JPSI_K_PI_for_ea");
    Kpi_piK_2d_full_for_ea->SetTitle("Kpi_piK_2d_full_for_ea");
    
    if (MODEL == 0)
    {
        TFile *f_out_ea = new TFile("../../SignalAreaShapeSA/evo_alg.root", "recreate");
        Kpi_piK_2d_BS_to_JPSI_K_K_for_ea->Write();
        Kpi_piK_2d_BD_to_JPSI_K_PI_for_ea->Write();
        Kpi_piK_2d_BD_to_JPSI_PI_PI_for_ea->Write();
        Kpi_piK_2d_BS_to_JPSI_PI_PI_for_ea->Write();
        Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K_for_ea->Write();
        Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI_for_ea->Write();
        Kpi_piK_2d_BD_to_JPSI_K_K_for_ea->Write();
        Kpi_piK_2d_BS_to_JPSI_K_PI_for_ea->Write();
        Kpi_piK_2d_full_for_ea->Write();
        Kpi_piK_2d_bg_for_ea->Write();
        f_out_ea->Close();
    }
    
    TCanvas *c_test = new TCanvas("c_test","c_test");
    TH1D *h_test_Kpi_mc = (TH1D*)Kpi_piK_2d_bg->ProjectionX()->Clone("h_test_Kpi_mc");
    h_test_Kpi_mc->Add(Kpi_piK_2d_BS_to_JPSI_PI_PI->ProjectionX());
    h_test_Kpi_mc->Add(Kpi_piK_2d_BD_to_JPSI_PI_PI->ProjectionX());
    h_test_Kpi_mc->Add(Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->ProjectionX());
    h_test_Kpi_mc->Add(Kpi_piK_2d_BS_to_JPSI_K_K->ProjectionX());
    h_test_Kpi_mc->Add(Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionX());
    h_test_Kpi_mc->Add(Kpi_piK_2d_BD_to_JPSI_K_K->ProjectionX());
    h_test_Kpi_mc->Add(Kpi_piK_2d_BS_to_JPSI_K_PI->ProjectionX());
    //h_test_Kpi_mc->Add(Kpi_piK_2d_BD_to_JPSI_K_PI->ProjectionX());
    
    TH1D *h_test_Kpi_data = (TH1D*)Kpi_piK_2d_data->ProjectionX()->Clone("h_test_Kpi_data");
    
    h_test_Kpi_data->Add(h_test_Kpi_mc, -1.);
    
    h_test_Kpi_data->GetXaxis()->SetTitle("m(J/#psi K #pi), GeV");
    
    h_test_Kpi_data->Draw();
    Kpi_piK_2d_BD_to_JPSI_K_PI->ProjectionX()->Draw("same hist");
    h_test_Kpi_data->Draw("same");
    pictures_path_png = pictures_path + "Kpi_data_vs_mc_compare.png";
    c_test->SaveAs(pictures_path_png.c_str());
    
    /*h_test_Kpi_data->Add(h_test_Kpi_mc, 1.);
    h_test_Kpi_mc->Add(Kpi_piK_2d_BD_to_JPSI_K_PI->ProjectionX(), 1.);
    
    h_test_Kpi_data->Scale(1. / h_test_Kpi_data->Integral());
    
    h_test_Kpi_mc->Scale(1. / h_test_Kpi_mc->Integral());
    
    h_test_Kpi_mc->Add(h_test_Kpi_data, -1.);
    
    TFile *f_mc_minus_data = new TFile("../../../COVID19_time/mc_minus_data.root", "recreate");
    h_test_Kpi_mc->Write();
    f_mc_minus_data->Close();*/
    
    cout << "\n\n Comb_bg Integral KpipiK:\t" << Kpi_piK_2d_bg->Integral() << endl;
    cout << "\n\n Comb_bg Integral KK:\t" << KK_1d_bg->Integral() << endl;
    cout << "\n\n Comb_bg Integral pipi:\t" << pipi_1d_bg->Integral() << endl;
    cout << "\n\n Comb_bg Integral pKKp:\t" << pK_Kp_2d_bg->Integral() << endl;
    cout << "\n\n Comb_bg Integral pKKp rotate:\t" << pK_Kp_2d_rotate_bg->Integral() << endl;
    cout << "\n\n Comb_bg_true : \t" << Kpi_piK_2d_data->Integral() - n_BS_KK - n_BD_KPI - n_BD_PIPI - n_BS_PIPI - n_lambda << endl;
    
    cos_theta_Zc_bg->SetLineColor(1);
    cos_theta_Zc_bg->SetFillColor(42);
    cos_theta_Zc_BS_to_JPSI_K_K->SetFillColor(30);
    cos_theta_Zc_BD_to_JPSI_K_PI->SetFillColor(7);
    cos_theta_Zc_BD_to_JPSI_PI_PI->SetFillColor(8);
    cos_theta_Zc_BS_to_JPSI_PI_PI->SetFillColor(46);
    cos_theta_Zc_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    cos_theta_Zc_LAMBDA0B_to_JPSI_P_PI->SetFillColor(95);
    cos_theta_Zc_BD_to_JPSI_K_K->SetFillColor(12);
    cos_theta_Zc_BS_to_JPSI_K_PI->SetFillColor(6);
    cos_theta_Zc_data->SetMarkerStyle(kFullCircle);
    cos_theta_Zc_data->SetMarkerSize(0.7);
    
    phi_psi_bg->SetLineColor(1);
    phi_psi_bg->SetFillColor(42);
    phi_psi_BS_to_JPSI_K_K->SetFillColor(30);
    phi_psi_BD_to_JPSI_K_PI->SetFillColor(7);
    phi_psi_BD_to_JPSI_PI_PI->SetFillColor(8);
    phi_psi_BS_to_JPSI_PI_PI->SetFillColor(46);
    phi_psi_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    phi_psi_LAMBDA0B_to_JPSI_P_PI->SetFillColor(95);
    phi_psi_BD_to_JPSI_K_K->SetFillColor(12);
    phi_psi_BS_to_JPSI_K_PI->SetFillColor(6);
    phi_psi_data->SetMarkerStyle(kFullCircle);
    phi_psi_data->SetMarkerSize(0.7);
    
    cos_theta_B0_bg->SetLineColor(1);
    cos_theta_B0_bg->SetFillColor(42);
    cos_theta_B0_BS_to_JPSI_K_K->SetFillColor(30);
    cos_theta_B0_BD_to_JPSI_K_PI->SetFillColor(7);
    cos_theta_B0_BD_to_JPSI_PI_PI->SetFillColor(8);
    cos_theta_B0_BS_to_JPSI_PI_PI->SetFillColor(46);
    cos_theta_B0_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    cos_theta_B0_LAMBDA0B_to_JPSI_P_PI->SetFillColor(95);
    cos_theta_B0_BD_to_JPSI_K_K->SetFillColor(12);
    cos_theta_B0_BS_to_JPSI_K_PI->SetFillColor(6);
    cos_theta_B0_data->SetMarkerStyle(kFullCircle);
    cos_theta_B0_data->SetMarkerSize(0.7);
    
    phi_K_bg->SetLineColor(1);
    phi_K_bg->SetFillColor(42);
    phi_K_BS_to_JPSI_K_K->SetFillColor(30);
    phi_K_BD_to_JPSI_K_PI->SetFillColor(7);
    phi_K_BD_to_JPSI_PI_PI->SetFillColor(8);
    phi_K_BS_to_JPSI_PI_PI->SetFillColor(46);
    phi_K_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    phi_K_LAMBDA0B_to_JPSI_P_PI->SetFillColor(95);
    phi_K_BD_to_JPSI_K_K->SetFillColor(12);
    phi_K_BS_to_JPSI_K_PI->SetFillColor(6);
    phi_K_data->SetMarkerStyle(kFullCircle);
    phi_K_data->SetMarkerSize(0.7);
    
    alpha_mu_bg->SetLineColor(1);
    alpha_mu_bg->SetFillColor(42);
    alpha_mu_BS_to_JPSI_K_K->SetFillColor(30);
    alpha_mu_BD_to_JPSI_K_PI->SetFillColor(7);
    alpha_mu_BD_to_JPSI_PI_PI->SetFillColor(8);
    alpha_mu_BS_to_JPSI_PI_PI->SetFillColor(46);
    alpha_mu_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    alpha_mu_LAMBDA0B_to_JPSI_P_PI->SetFillColor(95);
    alpha_mu_BD_to_JPSI_K_K->SetFillColor(12);
    alpha_mu_BS_to_JPSI_K_PI->SetFillColor(6);
    alpha_mu_data->SetMarkerStyle(kFullCircle);
    alpha_mu_data->SetMarkerSize(0.7);
    
    phi_mu_Kstar_bg->SetLineColor(1);
    phi_mu_Kstar_bg->SetFillColor(42);
    phi_mu_Kstar_BS_to_JPSI_K_K->SetFillColor(30);
    phi_mu_Kstar_BD_to_JPSI_K_PI->SetFillColor(7);
    phi_mu_Kstar_BD_to_JPSI_PI_PI->SetFillColor(8);
    phi_mu_Kstar_BS_to_JPSI_PI_PI->SetFillColor(46);
    phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    phi_mu_Kstar_LAMBDA0B_to_JPSI_P_PI->SetFillColor(95);
    phi_mu_Kstar_BD_to_JPSI_K_K->SetFillColor(12);
    phi_mu_Kstar_BS_to_JPSI_K_PI->SetFillColor(6);
    phi_mu_Kstar_data->SetMarkerStyle(kFullCircle);
    phi_mu_Kstar_data->SetMarkerSize(0.7);
    
    phi_mu_X_bg->SetLineColor(1);
    phi_mu_X_bg->SetFillColor(42);
    phi_mu_X_BS_to_JPSI_K_K->SetFillColor(30);
    phi_mu_X_BD_to_JPSI_K_PI->SetFillColor(7);
    phi_mu_X_BD_to_JPSI_PI_PI->SetFillColor(8);
    phi_mu_X_BS_to_JPSI_PI_PI->SetFillColor(46);
    phi_mu_X_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    phi_mu_X_LAMBDA0B_to_JPSI_P_PI->SetFillColor(95);
    phi_mu_X_BD_to_JPSI_K_K->SetFillColor(12);
    phi_mu_X_BS_to_JPSI_K_PI->SetFillColor(6);
    phi_mu_X_data->SetMarkerStyle(kFullCircle);
    phi_mu_X_data->SetMarkerSize(0.7);
    
    cos_theta_psi_X_bg->SetLineColor(1);
    cos_theta_psi_X_bg->SetFillColor(42);
    cos_theta_psi_X_BS_to_JPSI_K_K->SetFillColor(30);
    cos_theta_psi_X_BD_to_JPSI_K_PI->SetFillColor(7);
    cos_theta_psi_X_BD_to_JPSI_PI_PI->SetFillColor(8);
    cos_theta_psi_X_BS_to_JPSI_PI_PI->SetFillColor(46);
    cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    cos_theta_psi_X_LAMBDA0B_to_JPSI_P_PI->SetFillColor(95);
    cos_theta_psi_X_BD_to_JPSI_K_K->SetFillColor(12);
    cos_theta_psi_X_BS_to_JPSI_K_PI->SetFillColor(6);
    cos_theta_psi_X_data->SetMarkerStyle(kFullCircle);
    cos_theta_psi_X_data->SetMarkerSize(0.7);
    
    cos_theta_Kstar_bg->SetLineColor(1);
    cos_theta_Kstar_bg->SetFillColor(42);
    cos_theta_Kstar_BS_to_JPSI_K_K->SetFillColor(30);
    cos_theta_Kstar_BD_to_JPSI_K_PI->SetFillColor(7);
    cos_theta_Kstar_BD_to_JPSI_PI_PI->SetFillColor(8);
    cos_theta_Kstar_BS_to_JPSI_PI_PI->SetFillColor(46);
    cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    cos_theta_Kstar_LAMBDA0B_to_JPSI_P_PI->SetFillColor(95);
    cos_theta_Kstar_BD_to_JPSI_K_K->SetFillColor(12);
    cos_theta_Kstar_BS_to_JPSI_K_PI->SetFillColor(6);
    cos_theta_Kstar_data->SetMarkerStyle(kFullCircle);
    cos_theta_Kstar_data->SetMarkerSize(0.7);
    
    cos_theta_psi_Kstar_bg->SetLineColor(1);
    cos_theta_psi_Kstar_bg->SetFillColor(42);
    cos_theta_psi_Kstar_BS_to_JPSI_K_K->SetFillColor(30);
    cos_theta_psi_Kstar_BD_to_JPSI_K_PI->SetFillColor(7);
    cos_theta_psi_Kstar_BD_to_JPSI_PI_PI->SetFillColor(8);
    cos_theta_psi_Kstar_BS_to_JPSI_PI_PI->SetFillColor(46);
    cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_PI->SetFillColor(95);
    cos_theta_psi_Kstar_BD_to_JPSI_K_K->SetFillColor(12);
    cos_theta_psi_Kstar_BS_to_JPSI_K_PI->SetFillColor(6);
    cos_theta_psi_Kstar_data->SetMarkerStyle(kFullCircle);
    cos_theta_psi_Kstar_data->SetMarkerSize(0.7);
    
    THStack hs_phi_psi_X("hs_phi_psi_X","phi_psi_X");
    
    hs_phi_psi_X.Add(phi_psi_bg->ProjectionX());
    hs_phi_psi_X.Add(phi_psi_BD_to_JPSI_PI_PI->ProjectionX());
    hs_phi_psi_X.Add(phi_psi_BS_to_JPSI_PI_PI->ProjectionX());
    hs_phi_psi_X.Add(phi_psi_BS_to_JPSI_K_K->ProjectionX());
    hs_phi_psi_X.Add(phi_psi_LAMBDA0B_to_JPSI_P_K->ProjectionX());
    hs_phi_psi_X.Add(phi_psi_BD_to_JPSI_K_PI->ProjectionX());
    if (RARE_DECAYS)
    {
        hs_phi_psi_X.Add(phi_psi_LAMBDA0B_to_JPSI_P_PI->ProjectionX());
        hs_phi_psi_X.Add(phi_psi_BD_to_JPSI_K_K->ProjectionX());
        hs_phi_psi_X.Add(phi_psi_BS_to_JPSI_K_PI->ProjectionX());
    }
    TCanvas * c_phi_psi_X = new TCanvas("phi_psi_X");
    
    hist_buffer = (TH1D*)phi_psi_data->ProjectionX()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("#phi_{#psi} (h_{1}=K, h_{2}=#pi)");
    hist_buffer->GetYaxis()->SetTitle("Events / (2#pi/10)");
    hist_buffer->GetYaxis()->SetTitleOffset(1.35);
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_phi_psi_X.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    //legend_Jpsipi_X->Draw();
    t->DrawLatex(0.4,0.88, writetext00);
    t->DrawLatex(0.4,0.83, writetext01);
    pictures_path_cpp = angles_path + "phi_psi_X.C";
    pictures_path_png = angles_path + "phi_psi_X.png";
    c_phi_psi_X->SaveAs(pictures_path_cpp.c_str());
    c_phi_psi_X->SaveAs(pictures_path_png.c_str());
    
    THStack hs_phi_psi_Y("hs_phi_psi_Y","phi_psi_Y");
    
    hs_phi_psi_Y.Add(phi_psi_bg->ProjectionY());
    hs_phi_psi_Y.Add(phi_psi_BD_to_JPSI_PI_PI->ProjectionY());
    hs_phi_psi_Y.Add(phi_psi_BS_to_JPSI_PI_PI->ProjectionY());
    hs_phi_psi_Y.Add(phi_psi_BS_to_JPSI_K_K->ProjectionY());
    hs_phi_psi_Y.Add(phi_psi_LAMBDA0B_to_JPSI_P_K->ProjectionY());
    hs_phi_psi_Y.Add(phi_psi_BD_to_JPSI_K_PI->ProjectionY());
    if (RARE_DECAYS)
    {
        hs_phi_psi_Y.Add(phi_psi_LAMBDA0B_to_JPSI_P_PI->ProjectionY());
        hs_phi_psi_Y.Add(phi_psi_BD_to_JPSI_K_K->ProjectionY());
        hs_phi_psi_Y.Add(phi_psi_BS_to_JPSI_K_PI->ProjectionY());
    }
    TCanvas * c_phi_psi_Y = new TCanvas("phi_psi_Y");
    
    hist_buffer = (TH1D*)phi_psi_data->ProjectionY()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("#phi_{#psi} (h_{1}=#pi, h_{2}=K)");
    hist_buffer->GetYaxis()->SetTitle("Events / (2#pi/10)");
    hist_buffer->GetYaxis()->SetTitleOffset(1.35);
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_phi_psi_Y.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    //legend_Jpsipi_X->Draw();
    t->DrawLatex(0.4,0.88, writetext00);
    t->DrawLatex(0.4,0.83, writetext01);
    pictures_path_cpp = angles_path + "phi_psi_Y.C";
    pictures_path_png = angles_path + "phi_psi_Y.png";
    c_phi_psi_Y->SaveAs(pictures_path_cpp.c_str());
    c_phi_psi_Y->SaveAs(pictures_path_png.c_str());
    
    THStack hs_cos_theta_B0_X("hs_cos_theta_B0_X","cos_theta_B0_X");
    
    hs_cos_theta_B0_X.Add(cos_theta_B0_bg->ProjectionX());
    hs_cos_theta_B0_X.Add(cos_theta_B0_BD_to_JPSI_PI_PI->ProjectionX());
    hs_cos_theta_B0_X.Add(cos_theta_B0_BS_to_JPSI_PI_PI->ProjectionX());
    hs_cos_theta_B0_X.Add(cos_theta_B0_BS_to_JPSI_K_K->ProjectionX());
    hs_cos_theta_B0_X.Add(cos_theta_B0_LAMBDA0B_to_JPSI_P_K->ProjectionX());
    hs_cos_theta_B0_X.Add(cos_theta_B0_BD_to_JPSI_K_PI->ProjectionX());
    if (RARE_DECAYS)
    {
        hs_cos_theta_B0_X.Add(cos_theta_B0_LAMBDA0B_to_JPSI_P_PI->ProjectionX());
        hs_cos_theta_B0_X.Add(cos_theta_B0_BD_to_JPSI_K_K->ProjectionX());
        hs_cos_theta_B0_X.Add(cos_theta_B0_BS_to_JPSI_K_PI->ProjectionX());
    }
    TCanvas * c_cos_theta_B0_X = new TCanvas("cos_theta_B0_X");
    
    hist_buffer = (TH1D*)cos_theta_B0_data->ProjectionX()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("|cos(#theta_{B^{0}})| (h_{1}=K, h_{2}=#pi)");
    hist_buffer->GetYaxis()->SetTitle("Events / 0.1");
    hist_buffer->GetYaxis()->SetTitleOffset(1.35);
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_cos_theta_B0_X.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    //legend_Jpsipi_X->Draw();
    t->DrawLatex(0.6,0.88, writetext00);
    t->DrawLatex(0.6,0.83, writetext01);
    pictures_path_cpp = angles_path + "cos_theta_B0_X.C";
    pictures_path_png = angles_path + "cos_theta_B0_X.png";
    c_cos_theta_B0_X->SaveAs(pictures_path_cpp.c_str());
    c_cos_theta_B0_X->SaveAs(pictures_path_png.c_str());
    
    THStack hs_cos_theta_B0_Y("hs_cos_theta_B0_Y","cos_theta_B0_Y");
    
    hs_cos_theta_B0_Y.Add(cos_theta_B0_bg->ProjectionY());
    hs_cos_theta_B0_Y.Add(cos_theta_B0_BD_to_JPSI_PI_PI->ProjectionY());
    hs_cos_theta_B0_Y.Add(cos_theta_B0_BS_to_JPSI_PI_PI->ProjectionY());
    hs_cos_theta_B0_Y.Add(cos_theta_B0_BS_to_JPSI_K_K->ProjectionY());
    hs_cos_theta_B0_Y.Add(cos_theta_B0_LAMBDA0B_to_JPSI_P_K->ProjectionY());
    hs_cos_theta_B0_Y.Add(cos_theta_B0_BD_to_JPSI_K_PI->ProjectionY());
    if (RARE_DECAYS)
    {
        hs_cos_theta_B0_Y.Add(cos_theta_B0_LAMBDA0B_to_JPSI_P_PI->ProjectionY());
        hs_cos_theta_B0_Y.Add(cos_theta_B0_BD_to_JPSI_K_K->ProjectionY());
        hs_cos_theta_B0_Y.Add(cos_theta_B0_BS_to_JPSI_K_PI->ProjectionY());
    }
    TCanvas * c_cos_theta_B0_Y = new TCanvas("cos_theta_B0_Y");
    
    hist_buffer = (TH1D*)cos_theta_B0_data->ProjectionX()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("|cos(#theta_{B^{0}})| (h_{1}=#pi, h_{2}=K)");
    hist_buffer->GetYaxis()->SetTitle("Events / 0.1");
    hist_buffer->GetYaxis()->SetTitleOffset(1.35);
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_cos_theta_B0_Y.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    //legend_Jpsipi_X->Draw();
    t->DrawLatex(0.6,0.88, writetext00);
    t->DrawLatex(0.6,0.83, writetext01);
    pictures_path_cpp = angles_path + "cos_theta_B0_Y.C";
    pictures_path_png = angles_path + "cos_theta_B0_Y.png";
    c_cos_theta_B0_Y->SaveAs(pictures_path_cpp.c_str());
    c_cos_theta_B0_Y->SaveAs(pictures_path_png.c_str());
    
    THStack hs_cos_theta_Zc_X("hs_cos_theta_Zc_X","cos_theta_Zc_X");
    
    hs_cos_theta_Zc_X.Add(cos_theta_Zc_bg->ProjectionX());
    hs_cos_theta_Zc_X.Add(cos_theta_Zc_BD_to_JPSI_PI_PI->ProjectionX());
    hs_cos_theta_Zc_X.Add(cos_theta_Zc_BS_to_JPSI_PI_PI->ProjectionX());
    hs_cos_theta_Zc_X.Add(cos_theta_Zc_BS_to_JPSI_K_K->ProjectionX());
    hs_cos_theta_Zc_X.Add(cos_theta_Zc_LAMBDA0B_to_JPSI_P_K->ProjectionX());
    hs_cos_theta_Zc_X.Add(cos_theta_Zc_BD_to_JPSI_K_PI->ProjectionX());
    if (RARE_DECAYS)
    {
        hs_cos_theta_Zc_X.Add(cos_theta_Zc_LAMBDA0B_to_JPSI_P_PI->ProjectionX());
        hs_cos_theta_Zc_X.Add(cos_theta_Zc_BD_to_JPSI_K_K->ProjectionX());
        hs_cos_theta_Zc_X.Add(cos_theta_Zc_BS_to_JPSI_K_PI->ProjectionX());
    }
    TCanvas * c_cos_theta_Zc_X = new TCanvas("cos_theta_Zc_X");
    
    hist_buffer = (TH1D*)cos_theta_Zc_data->ProjectionX()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("|cos(#theta_{Z_{c}})| (h_{1}=K, h_{2}=#pi)");
    hist_buffer->GetYaxis()->SetTitle("Events / 0.1");
    hist_buffer->GetYaxis()->SetTitleOffset(1.35);
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_cos_theta_Zc_X.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    legend_Jpsipi_X->Draw();
    t->DrawLatex(0.25,0.88, writetext00);
    t->DrawLatex(0.25,0.83, writetext01);
    pictures_path_cpp = angles_path + "cos_theta_Zc_X.C";
    pictures_path_png = angles_path + "cos_theta_Zc_X.png";
    c_cos_theta_Zc_X->SaveAs(pictures_path_cpp.c_str());
    c_cos_theta_Zc_X->SaveAs(pictures_path_png.c_str());
    
    THStack hs_cos_theta_Zc_Y("hs_cos_theta_Zc_Y","cos_theta_Zc_Y");
    
    hs_cos_theta_Zc_Y.Add(cos_theta_Zc_bg->ProjectionY());
    hs_cos_theta_Zc_Y.Add(cos_theta_Zc_BD_to_JPSI_PI_PI->ProjectionY());
    hs_cos_theta_Zc_Y.Add(cos_theta_Zc_BS_to_JPSI_PI_PI->ProjectionY());
    hs_cos_theta_Zc_Y.Add(cos_theta_Zc_BS_to_JPSI_K_K->ProjectionY());
    hs_cos_theta_Zc_Y.Add(cos_theta_Zc_LAMBDA0B_to_JPSI_P_K->ProjectionY());
    hs_cos_theta_Zc_Y.Add(cos_theta_Zc_BD_to_JPSI_K_PI->ProjectionY());
    if (RARE_DECAYS)
    {
        hs_cos_theta_Zc_Y.Add(cos_theta_Zc_LAMBDA0B_to_JPSI_P_PI->ProjectionY());
        hs_cos_theta_Zc_Y.Add(cos_theta_Zc_BD_to_JPSI_K_K->ProjectionY());
        hs_cos_theta_Zc_Y.Add(cos_theta_Zc_BS_to_JPSI_K_PI->ProjectionY());
    }
    TCanvas * c_cos_theta_Zc_Y = new TCanvas("cos_theta_Zc_Y");
    
    hist_buffer = (TH1D*)cos_theta_Zc_data->ProjectionX()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("|cos(#theta_{Z_{c}})| (h_{1}=#pi, h_{2}=K)");
    hist_buffer->GetYaxis()->SetTitle("Events / 0.1");
    hist_buffer->GetYaxis()->SetTitleOffset(1.35);
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_cos_theta_Zc_Y.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    legend_Jpsipi_X->Draw();
    t->DrawLatex(0.25,0.88, writetext00);
    t->DrawLatex(0.25,0.83, writetext01);
    pictures_path_cpp = angles_path + "cos_theta_Zc_Y.C";
    pictures_path_png = angles_path + "cos_theta_Zc_Y.png";
    c_cos_theta_Zc_Y->SaveAs(pictures_path_cpp.c_str());
    c_cos_theta_Zc_Y->SaveAs(pictures_path_png.c_str());
    
    THStack hs_phi_K_X("hs_phi_K_X","phi_K_X");
    
    hs_phi_K_X.Add(phi_K_bg->ProjectionX());
    hs_phi_K_X.Add(phi_K_BD_to_JPSI_PI_PI->ProjectionX());
    hs_phi_K_X.Add(phi_K_BS_to_JPSI_PI_PI->ProjectionX());
    hs_phi_K_X.Add(phi_K_BS_to_JPSI_K_K->ProjectionX());
    hs_phi_K_X.Add(phi_K_LAMBDA0B_to_JPSI_P_K->ProjectionX());
    hs_phi_K_X.Add(phi_K_BD_to_JPSI_K_PI->ProjectionX());
    if (RARE_DECAYS)
    {
        hs_phi_K_X.Add(phi_K_LAMBDA0B_to_JPSI_P_PI->ProjectionX());
        hs_phi_K_X.Add(phi_K_BD_to_JPSI_K_K->ProjectionX());
        hs_phi_K_X.Add(phi_K_BS_to_JPSI_K_PI->ProjectionX());
    }
    TCanvas * c_phi_K_X = new TCanvas("phi_K_X");
    
    hist_buffer = (TH1D*)phi_K_data->ProjectionX()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("#phi_{K} (h_{1}=K, h_{2}=#pi)");
    hist_buffer->GetYaxis()->SetTitle("Events / (2#pi/10)");
    hist_buffer->GetYaxis()->SetTitleOffset(1.35);
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_phi_K_X.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    //legend_Jpsipi_X->Draw();
    t->DrawLatex(0.4,0.88, writetext00);
    t->DrawLatex(0.4,0.83, writetext01);
    pictures_path_cpp = angles_path + "phi_K_X.C";
    pictures_path_png = angles_path + "phi_K_X.png";
    c_phi_K_X->SaveAs(pictures_path_cpp.c_str());
    c_phi_K_X->SaveAs(pictures_path_png.c_str());
    
    THStack hs_phi_K_Y("hs_phi_K_Y","phi_K_Y");
    
    hs_phi_K_Y.Add(phi_K_bg->ProjectionY());
    hs_phi_K_Y.Add(phi_K_BD_to_JPSI_PI_PI->ProjectionY());
    hs_phi_K_Y.Add(phi_K_BS_to_JPSI_PI_PI->ProjectionY());
    hs_phi_K_Y.Add(phi_K_BS_to_JPSI_K_K->ProjectionY());
    hs_phi_K_Y.Add(phi_K_LAMBDA0B_to_JPSI_P_K->ProjectionY());
    hs_phi_K_Y.Add(phi_K_BD_to_JPSI_K_PI->ProjectionY());
    if (RARE_DECAYS)
    {
        hs_phi_K_Y.Add(phi_K_LAMBDA0B_to_JPSI_P_PI->ProjectionY());
        hs_phi_K_Y.Add(phi_K_BD_to_JPSI_K_K->ProjectionY());
        hs_phi_K_Y.Add(phi_K_BS_to_JPSI_K_PI->ProjectionY());
    }
    TCanvas * c_phi_K_Y = new TCanvas("phi_K_Y");
    
    hist_buffer = (TH1D*)phi_K_data->ProjectionY()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("#phi_{K} (h_{1}=#pi, h_{2}=K)");
    hist_buffer->GetYaxis()->SetTitle("Events / (2#pi/10)");
    hist_buffer->GetYaxis()->SetTitleOffset(1.35);
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_phi_K_Y.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    //legend_Jpsipi_X->Draw();
    t->DrawLatex(0.4,0.88, writetext00);
    t->DrawLatex(0.4,0.83, writetext01);
    pictures_path_cpp = angles_path + "phi_K_Y.C";
    pictures_path_png = angles_path + "phi_K_Y.png";
    c_phi_K_Y->SaveAs(pictures_path_cpp.c_str());
    c_phi_K_Y->SaveAs(pictures_path_png.c_str());
    
    THStack hs_alpha_mu_X("hs_alpha_mu_X","alpha_mu_X");
    
    hs_alpha_mu_X.Add(alpha_mu_bg->ProjectionX());
    hs_alpha_mu_X.Add(alpha_mu_BD_to_JPSI_PI_PI->ProjectionX());
    hs_alpha_mu_X.Add(alpha_mu_BS_to_JPSI_PI_PI->ProjectionX());
    hs_alpha_mu_X.Add(alpha_mu_BS_to_JPSI_K_K->ProjectionX());
    hs_alpha_mu_X.Add(alpha_mu_LAMBDA0B_to_JPSI_P_K->ProjectionX());
    hs_alpha_mu_X.Add(alpha_mu_BD_to_JPSI_K_PI->ProjectionX());
    if (RARE_DECAYS)
    {
        hs_alpha_mu_X.Add(alpha_mu_LAMBDA0B_to_JPSI_P_PI->ProjectionX());
        hs_alpha_mu_X.Add(alpha_mu_BD_to_JPSI_K_K->ProjectionX());
        hs_alpha_mu_X.Add(alpha_mu_BS_to_JPSI_K_PI->ProjectionX());
    }
    TCanvas * c_alpha_mu_X = new TCanvas("alpha_mu_X");
    
    hist_buffer = (TH1D*)alpha_mu_data->ProjectionX()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("#alpha_{#mu} (h_{1}=K, h_{2}=#pi)");
    hist_buffer->GetYaxis()->SetTitle("Events / (2#pi/10)");
    hist_buffer->GetYaxis()->SetTitleOffset(1.35);
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_alpha_mu_X.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    //legend_Jpsipi_X->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = angles_path + "alpha_mu_X.C";
    pictures_path_png = angles_path + "alpha_mu_X.png";
    c_alpha_mu_X->SaveAs(pictures_path_cpp.c_str());
    c_alpha_mu_X->SaveAs(pictures_path_png.c_str());
    
    THStack hs_alpha_mu_Y("hs_alpha_mu_Y","alpha_mu_Y");
    
    hs_alpha_mu_Y.Add(alpha_mu_bg->ProjectionY());
    hs_alpha_mu_Y.Add(alpha_mu_BD_to_JPSI_PI_PI->ProjectionY());
    hs_alpha_mu_Y.Add(alpha_mu_BS_to_JPSI_PI_PI->ProjectionY());
    hs_alpha_mu_Y.Add(alpha_mu_BS_to_JPSI_K_K->ProjectionY());
    hs_alpha_mu_Y.Add(alpha_mu_LAMBDA0B_to_JPSI_P_K->ProjectionY());
    hs_alpha_mu_Y.Add(alpha_mu_BD_to_JPSI_K_PI->ProjectionY());
    if (RARE_DECAYS)
    {
        hs_alpha_mu_Y.Add(alpha_mu_LAMBDA0B_to_JPSI_P_PI->ProjectionY());
        hs_alpha_mu_Y.Add(alpha_mu_BD_to_JPSI_K_K->ProjectionY());
        hs_alpha_mu_Y.Add(alpha_mu_BS_to_JPSI_K_PI->ProjectionY());
    }
    TCanvas * c_alpha_mu_Y = new TCanvas("alpha_mu_Y");
    
    hist_buffer = (TH1D*)alpha_mu_data->ProjectionY()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("#alpha_{#mu} (h_{1}=#pi, h_{2}=K)");
    hist_buffer->GetYaxis()->SetTitle("Events / (2#pi/10)");
    hist_buffer->GetYaxis()->SetTitleOffset(1.35);
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_alpha_mu_Y.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    //legend_Jpsipi_X->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = angles_path + "alpha_mu_Y.C";
    pictures_path_png = angles_path + "alpha_mu_Y.png";
    c_alpha_mu_Y->SaveAs(pictures_path_cpp.c_str());
    c_alpha_mu_Y->SaveAs(pictures_path_png.c_str());
    
    THStack hs_phi_mu_Kstar_X("hs_phi_mu_Kstar_X","phi_mu_Kstar_X");
    
    hs_phi_mu_Kstar_X.Add(phi_mu_Kstar_bg->ProjectionX());
    hs_phi_mu_Kstar_X.Add(phi_mu_Kstar_BD_to_JPSI_PI_PI->ProjectionX());
    hs_phi_mu_Kstar_X.Add(phi_mu_Kstar_BS_to_JPSI_PI_PI->ProjectionX());
    hs_phi_mu_Kstar_X.Add(phi_mu_Kstar_BS_to_JPSI_K_K->ProjectionX());
    hs_phi_mu_Kstar_X.Add(phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K->ProjectionX());
    hs_phi_mu_Kstar_X.Add(phi_mu_Kstar_BD_to_JPSI_K_PI->ProjectionX());
    if (RARE_DECAYS)
    {
        hs_phi_mu_Kstar_X.Add(phi_mu_Kstar_LAMBDA0B_to_JPSI_P_PI->ProjectionX());
        hs_phi_mu_Kstar_X.Add(phi_mu_Kstar_BD_to_JPSI_K_K->ProjectionX());
        hs_phi_mu_Kstar_X.Add(phi_mu_Kstar_BS_to_JPSI_K_PI->ProjectionX());
    }
    TCanvas * c_phi_mu_Kstar_X = new TCanvas("phi_mu_Kstar_X");
    
    hist_buffer = (TH1D*)phi_mu_Kstar_data->ProjectionX()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("#phi_{#mu}^{K*} (h_{1}=K, h_{2}=#pi)");
    hist_buffer->GetYaxis()->SetTitle("Events / (2#pi/10)");
    hist_buffer->GetYaxis()->SetTitleOffset(1.35);
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_phi_mu_Kstar_X.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    //legend_Jpsipi_X->Draw();
    t->DrawLatex(0.4,0.88, writetext00);
    t->DrawLatex(0.4,0.83, writetext01);
    pictures_path_cpp = angles_path + "phi_mu_Kstar_X.C";
    pictures_path_png = angles_path + "phi_mu_Kstar_X.png";
    c_phi_mu_Kstar_X->SaveAs(pictures_path_cpp.c_str());
    c_phi_mu_Kstar_X->SaveAs(pictures_path_png.c_str());
    
    THStack hs_phi_mu_Kstar_Y("hs_phi_mu_Kstar_Y","phi_mu_Kstar_Y");
    
    hs_phi_mu_Kstar_Y.Add(phi_mu_Kstar_bg->ProjectionY());
    hs_phi_mu_Kstar_Y.Add(phi_mu_Kstar_BD_to_JPSI_PI_PI->ProjectionY());
    hs_phi_mu_Kstar_Y.Add(phi_mu_Kstar_BS_to_JPSI_PI_PI->ProjectionY());
    hs_phi_mu_Kstar_Y.Add(phi_mu_Kstar_BS_to_JPSI_K_K->ProjectionY());
    hs_phi_mu_Kstar_Y.Add(phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K->ProjectionY());
    hs_phi_mu_Kstar_Y.Add(phi_mu_Kstar_BD_to_JPSI_K_PI->ProjectionY());
    if (RARE_DECAYS)
    {
        hs_phi_mu_Kstar_Y.Add(phi_mu_Kstar_LAMBDA0B_to_JPSI_P_PI->ProjectionY());
        hs_phi_mu_Kstar_Y.Add(phi_mu_Kstar_BD_to_JPSI_K_K->ProjectionY());
        hs_phi_mu_Kstar_Y.Add(phi_mu_Kstar_BS_to_JPSI_K_PI->ProjectionY());
    }
    TCanvas * c_phi_mu_Kstar_Y = new TCanvas("phi_mu_Kstar_Y");
    
    hist_buffer = (TH1D*)phi_mu_Kstar_data->ProjectionY()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("#phi_{#mu}^{K*} (h_{1}=#pi, h_{2}=K)");
    hist_buffer->GetYaxis()->SetTitle("Events / (2#pi/10)");
    hist_buffer->GetYaxis()->SetTitleOffset(1.35);
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_phi_mu_Kstar_Y.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    //legend_Jpsipi_X->Draw();
    t->DrawLatex(0.4,0.88, writetext00);
    t->DrawLatex(0.4,0.83, writetext01);
    pictures_path_cpp = angles_path + "phi_mu_Kstar_Y.C";
    pictures_path_png = angles_path + "phi_mu_Kstar_Y.png";
    c_phi_mu_Kstar_Y->SaveAs(pictures_path_cpp.c_str());
    c_phi_mu_Kstar_Y->SaveAs(pictures_path_png.c_str());
    
    THStack hs_phi_mu_X_X("hs_phi_mu_X_X","phi_mu_X_X");
    
    hs_phi_mu_X_X.Add(phi_mu_X_bg->ProjectionX());
    hs_phi_mu_X_X.Add(phi_mu_X_BD_to_JPSI_PI_PI->ProjectionX());
    hs_phi_mu_X_X.Add(phi_mu_X_BS_to_JPSI_PI_PI->ProjectionX());
    hs_phi_mu_X_X.Add(phi_mu_X_BS_to_JPSI_K_K->ProjectionX());
    hs_phi_mu_X_X.Add(phi_mu_X_LAMBDA0B_to_JPSI_P_K->ProjectionX());
    hs_phi_mu_X_X.Add(phi_mu_X_BD_to_JPSI_K_PI->ProjectionX());
    if (RARE_DECAYS)
    {
        hs_phi_mu_X_X.Add(phi_mu_X_LAMBDA0B_to_JPSI_P_PI->ProjectionX());
        hs_phi_mu_X_X.Add(phi_mu_X_BD_to_JPSI_K_K->ProjectionX());
        hs_phi_mu_X_X.Add(phi_mu_X_BS_to_JPSI_K_PI->ProjectionX());
    }
    TCanvas * c_phi_mu_X_X = new TCanvas("phi_mu_X_X");
    
    hist_buffer = (TH1D*)phi_mu_X_data->ProjectionX()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("#phi_{#mu}^{X} (h_{1}=K, h_{2}=#pi)");
    hist_buffer->GetYaxis()->SetTitle("Events / (2#pi/10)");
    hist_buffer->GetYaxis()->SetTitleOffset(1.35);
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.2);
    
    hist_buffer->Draw("");
    hs_phi_mu_X_X.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    //legend_Jpsipi_X->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = angles_path + "phi_mu_X_X.C";
    pictures_path_png = angles_path + "phi_mu_X_X.png";
    c_phi_mu_X_X->SaveAs(pictures_path_cpp.c_str());
    c_phi_mu_X_X->SaveAs(pictures_path_png.c_str());
    
    THStack hs_phi_mu_X_Y("hs_phi_mu_X_Y","phi_mu_X_Y");
    
    hs_phi_mu_X_Y.Add(phi_mu_X_bg->ProjectionY());
    hs_phi_mu_X_Y.Add(phi_mu_X_BD_to_JPSI_PI_PI->ProjectionY());
    hs_phi_mu_X_Y.Add(phi_mu_X_BS_to_JPSI_PI_PI->ProjectionY());
    hs_phi_mu_X_Y.Add(phi_mu_X_BS_to_JPSI_K_K->ProjectionY());
    hs_phi_mu_X_Y.Add(phi_mu_X_LAMBDA0B_to_JPSI_P_K->ProjectionY());
    hs_phi_mu_X_Y.Add(phi_mu_X_BD_to_JPSI_K_PI->ProjectionY());
    if (RARE_DECAYS)
    {
        hs_phi_mu_X_Y.Add(phi_mu_X_LAMBDA0B_to_JPSI_P_PI->ProjectionY());
        hs_phi_mu_X_Y.Add(phi_mu_X_BD_to_JPSI_K_K->ProjectionY());
        hs_phi_mu_X_Y.Add(phi_mu_X_BS_to_JPSI_K_PI->ProjectionY());
    }
    TCanvas * c_phi_mu_X_Y = new TCanvas("phi_mu_X_Y");
    
    hist_buffer = (TH1D*)phi_mu_X_data->ProjectionY()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("#phi_{#mu}^{X} (h_{1}=#pi, h_{2}=K)");
    hist_buffer->GetYaxis()->SetTitle("Events / (2#pi/10)");
    hist_buffer->GetYaxis()->SetTitleOffset(1.35);
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.2);
    
    hist_buffer->Draw("");
    hs_phi_mu_X_Y.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    //legend_Jpsipi_X->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = angles_path + "phi_mu_X_Y.C";
    pictures_path_png = angles_path + "phi_mu_X_Y.png";
    c_phi_mu_X_Y->SaveAs(pictures_path_cpp.c_str());
    c_phi_mu_X_Y->SaveAs(pictures_path_png.c_str());
    
    THStack hs_cos_theta_psi_X_X("hs_cos_theta_psi_X_X","cos_theta_psi_X_X");
    
    hs_cos_theta_psi_X_X.Add(cos_theta_psi_X_bg->ProjectionX());
    hs_cos_theta_psi_X_X.Add(cos_theta_psi_X_BD_to_JPSI_PI_PI->ProjectionX());
    hs_cos_theta_psi_X_X.Add(cos_theta_psi_X_BS_to_JPSI_PI_PI->ProjectionX());
    hs_cos_theta_psi_X_X.Add(cos_theta_psi_X_BS_to_JPSI_K_K->ProjectionX());
    hs_cos_theta_psi_X_X.Add(cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K->ProjectionX());
    hs_cos_theta_psi_X_X.Add(cos_theta_psi_X_BD_to_JPSI_K_PI->ProjectionX());
    if (RARE_DECAYS)
    {
        hs_cos_theta_psi_X_X.Add(cos_theta_psi_X_LAMBDA0B_to_JPSI_P_PI->ProjectionX());
        hs_cos_theta_psi_X_X.Add(cos_theta_psi_X_BD_to_JPSI_K_K->ProjectionX());
        hs_cos_theta_psi_X_X.Add(cos_theta_psi_X_BS_to_JPSI_K_PI->ProjectionX());
    }
    TCanvas * c_cos_theta_psi_X_X = new TCanvas("cos_theta_psi_X_X");
    
    hist_buffer = (TH1D*)cos_theta_psi_X_data->ProjectionX()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("|cos(#theta_{#psi}^{X})| (h_{1}=K, h_{2}=#pi)");
    hist_buffer->GetYaxis()->SetTitle("Events / 0.1");
    hist_buffer->GetYaxis()->SetTitleOffset(1.35);
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_cos_theta_psi_X_X.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    //legend_Jpsipi_X->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = angles_path + "cos_theta_psi_X_X.C";
    pictures_path_png = angles_path + "cos_theta_psi_X_X.png";
    c_cos_theta_psi_X_X->SaveAs(pictures_path_cpp.c_str());
    c_cos_theta_psi_X_X->SaveAs(pictures_path_png.c_str());
    
    THStack hs_cos_theta_psi_X_Y("hs_cos_theta_psi_X_Y","cos_theta_psi_X_Y");
    
    hs_cos_theta_psi_X_Y.Add(cos_theta_psi_X_bg->ProjectionY());
    hs_cos_theta_psi_X_Y.Add(cos_theta_psi_X_BD_to_JPSI_PI_PI->ProjectionY());
    hs_cos_theta_psi_X_Y.Add(cos_theta_psi_X_BS_to_JPSI_PI_PI->ProjectionY());
    hs_cos_theta_psi_X_Y.Add(cos_theta_psi_X_BS_to_JPSI_K_K->ProjectionY());
    hs_cos_theta_psi_X_Y.Add(cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K->ProjectionY());
    hs_cos_theta_psi_X_Y.Add(cos_theta_psi_X_BD_to_JPSI_K_PI->ProjectionY());
    if (RARE_DECAYS)
    {
        hs_cos_theta_psi_X_Y.Add(cos_theta_psi_X_LAMBDA0B_to_JPSI_P_PI->ProjectionY());
        hs_cos_theta_psi_X_Y.Add(cos_theta_psi_X_BD_to_JPSI_K_K->ProjectionY());
        hs_cos_theta_psi_X_Y.Add(cos_theta_psi_X_BS_to_JPSI_K_PI->ProjectionY());
    }
    TCanvas * c_cos_theta_psi_X_Y = new TCanvas("cos_theta_psi_X_Y");
    
    hist_buffer = (TH1D*)cos_theta_psi_X_data->ProjectionY()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("|cos(#theta_{#psi}^{X})| (h_{1}=#pi, h_{2}=K)");
    hist_buffer->GetYaxis()->SetTitle("Events / 0.1");
    hist_buffer->GetYaxis()->SetTitleOffset(1.35);
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_cos_theta_psi_X_Y.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    //legend_Jpsipi_X->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = angles_path + "cos_theta_psi_X_Y.C";
    pictures_path_png = angles_path + "cos_theta_psi_X_Y.png";
    c_cos_theta_psi_X_Y->SaveAs(pictures_path_cpp.c_str());
    c_cos_theta_psi_X_Y->SaveAs(pictures_path_png.c_str());
    
    THStack hs_cos_theta_Kstar_X("hs_cos_theta_Kstar_X","cos_theta_Kstar_X");
    
    hs_cos_theta_Kstar_X.Add(cos_theta_Kstar_bg->ProjectionX());
    hs_cos_theta_Kstar_X.Add(cos_theta_Kstar_BD_to_JPSI_PI_PI->ProjectionX());
    hs_cos_theta_Kstar_X.Add(cos_theta_Kstar_BS_to_JPSI_PI_PI->ProjectionX());
    hs_cos_theta_Kstar_X.Add(cos_theta_Kstar_BS_to_JPSI_K_K->ProjectionX());
    hs_cos_theta_Kstar_X.Add(cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->ProjectionX());
    hs_cos_theta_Kstar_X.Add(cos_theta_Kstar_BD_to_JPSI_K_PI->ProjectionX());
    if (RARE_DECAYS)
    {
        hs_cos_theta_Kstar_X.Add(cos_theta_Kstar_LAMBDA0B_to_JPSI_P_PI->ProjectionX());
        hs_cos_theta_Kstar_X.Add(cos_theta_Kstar_BD_to_JPSI_K_K->ProjectionX());
        hs_cos_theta_Kstar_X.Add(cos_theta_Kstar_BS_to_JPSI_K_PI->ProjectionX());
    }
    TCanvas * c_cos_theta_Kstar_X = new TCanvas("cos_theta_Kstar_X");
    
    hist_buffer = (TH1D*)cos_theta_Kstar_data->ProjectionX()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("|cos(#theta_{K*})| (h_{1}=K, h_{2}=#pi)");
    hist_buffer->GetYaxis()->SetTitle("Events / 0.1");
    hist_buffer->GetYaxis()->SetTitleOffset(1.35);
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_cos_theta_Kstar_X.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    //legend_Jpsipi_X->Draw();
    t->DrawLatex(0.6,0.88, writetext00);
    t->DrawLatex(0.6,0.83, writetext01);
    pictures_path_cpp = angles_path + "cos_theta_Kstar_X.C";
    pictures_path_png = angles_path + "cos_theta_Kstar_X.png";
    c_cos_theta_Kstar_X->SaveAs(pictures_path_cpp.c_str());
    c_cos_theta_Kstar_X->SaveAs(pictures_path_png.c_str());
    
    THStack hs_cos_theta_Kstar_Y("hs_cos_theta_Kstar_Y","cos_theta_Kstar_Y");
    
    hs_cos_theta_Kstar_Y.Add(cos_theta_Kstar_bg->ProjectionY());
    hs_cos_theta_Kstar_Y.Add(cos_theta_Kstar_BD_to_JPSI_PI_PI->ProjectionY());
    hs_cos_theta_Kstar_Y.Add(cos_theta_Kstar_BS_to_JPSI_PI_PI->ProjectionY());
    hs_cos_theta_Kstar_Y.Add(cos_theta_Kstar_BS_to_JPSI_K_K->ProjectionY());
    hs_cos_theta_Kstar_Y.Add(cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->ProjectionY());
    hs_cos_theta_Kstar_Y.Add(cos_theta_Kstar_BD_to_JPSI_K_PI->ProjectionY());
    if (RARE_DECAYS)
    {
        hs_cos_theta_Kstar_Y.Add(cos_theta_Kstar_LAMBDA0B_to_JPSI_P_PI->ProjectionY());
        hs_cos_theta_Kstar_Y.Add(cos_theta_Kstar_BD_to_JPSI_K_K->ProjectionY());
        hs_cos_theta_Kstar_Y.Add(cos_theta_Kstar_BS_to_JPSI_K_PI->ProjectionY());
    }
    TCanvas * c_cos_theta_Kstar_Y = new TCanvas("cos_theta_Kstar_Y");
    
    hist_buffer = (TH1D*)cos_theta_Kstar_data->ProjectionY()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("|cos(#theta_{K*})| (h_{1}=#pi, h_{2}=K)");
    hist_buffer->GetYaxis()->SetTitle("Events / 0.1");
    hist_buffer->GetYaxis()->SetTitleOffset(1.35);
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_cos_theta_Kstar_Y.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    //legend_Jpsipi_X->Draw();
    t->DrawLatex(0.6,0.88, writetext00);
    t->DrawLatex(0.6,0.83, writetext01);
    pictures_path_cpp = angles_path + "cos_theta_Kstar_Y.C";
    pictures_path_png = angles_path + "cos_theta_Kstar_Y.png";
    c_cos_theta_Kstar_Y->SaveAs(pictures_path_cpp.c_str());
    c_cos_theta_Kstar_Y->SaveAs(pictures_path_png.c_str());
    
    THStack hs_cos_theta_psi_Kstar_X("hs_cos_theta_psi_Kstar_X","cos_theta_psi_Kstar_X");
    
    hs_cos_theta_psi_Kstar_X.Add(cos_theta_psi_Kstar_bg->ProjectionX());
    hs_cos_theta_psi_Kstar_X.Add(cos_theta_psi_Kstar_BD_to_JPSI_PI_PI->ProjectionX());
    hs_cos_theta_psi_Kstar_X.Add(cos_theta_psi_Kstar_BS_to_JPSI_PI_PI->ProjectionX());
    hs_cos_theta_psi_Kstar_X.Add(cos_theta_psi_Kstar_BS_to_JPSI_K_K->ProjectionX());
    hs_cos_theta_psi_Kstar_X.Add(cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K->ProjectionX());
    hs_cos_theta_psi_Kstar_X.Add(cos_theta_psi_Kstar_BD_to_JPSI_K_PI->ProjectionX());
    if (RARE_DECAYS)
    {
        hs_cos_theta_psi_Kstar_X.Add(cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_PI->ProjectionX());
        hs_cos_theta_psi_Kstar_X.Add(cos_theta_psi_Kstar_BD_to_JPSI_K_K->ProjectionX());
        hs_cos_theta_psi_Kstar_X.Add(cos_theta_psi_Kstar_BS_to_JPSI_K_PI->ProjectionX());
    }
    TCanvas * c_cos_theta_psi_Kstar_X = new TCanvas("cos_theta_psi_Kstar_X");
    
    hist_buffer = (TH1D*)cos_theta_psi_Kstar_data->ProjectionX()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("|cos(#theta_{#psi}^{K*})| (h_{1}=K, h_{2}=#pi)");
    hist_buffer->GetYaxis()->SetTitle("Events / 0.1");
    hist_buffer->GetYaxis()->SetTitleOffset(1.35);
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.2);
    
    hist_buffer->Draw("");
    hs_cos_theta_psi_Kstar_X.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    //legend_Jpsipi_X->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = angles_path + "cos_theta_psi_Kstar_X.C";
    pictures_path_png = angles_path + "cos_theta_psi_Kstar_X.png";
    c_cos_theta_psi_Kstar_X->SaveAs(pictures_path_cpp.c_str());
    c_cos_theta_psi_Kstar_X->SaveAs(pictures_path_png.c_str());
    
    THStack hs_cos_theta_psi_Kstar_Y("hs_cos_theta_psi_Kstar_Y","cos_theta_psi_Kstar_Y");
    
    hs_cos_theta_psi_Kstar_Y.Add(cos_theta_psi_Kstar_bg->ProjectionY());
    hs_cos_theta_psi_Kstar_Y.Add(cos_theta_psi_Kstar_BD_to_JPSI_PI_PI->ProjectionY());
    hs_cos_theta_psi_Kstar_Y.Add(cos_theta_psi_Kstar_BS_to_JPSI_PI_PI->ProjectionY());
    hs_cos_theta_psi_Kstar_Y.Add(cos_theta_psi_Kstar_BS_to_JPSI_K_K->ProjectionY());
    hs_cos_theta_psi_Kstar_Y.Add(cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K->ProjectionY());
    hs_cos_theta_psi_Kstar_Y.Add(cos_theta_psi_Kstar_BD_to_JPSI_K_PI->ProjectionY());
    if (RARE_DECAYS)
    {
        hs_cos_theta_psi_Kstar_Y.Add(cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_PI->ProjectionY());
        hs_cos_theta_psi_Kstar_Y.Add(cos_theta_psi_Kstar_BD_to_JPSI_K_K->ProjectionY());
        hs_cos_theta_psi_Kstar_Y.Add(cos_theta_psi_Kstar_BS_to_JPSI_K_PI->ProjectionY());
    }
    TCanvas * c_cos_theta_psi_Kstar_Y = new TCanvas("cos_theta_psi_Kstar_Y");
    
    hist_buffer = (TH1D*)cos_theta_psi_Kstar_data->ProjectionY()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("|cos(#theta_{#psi}^{K*})| (h_{1}=#pi, h_{2}=K)");
    hist_buffer->GetYaxis()->SetTitle("Events / 0.1");
    hist_buffer->GetYaxis()->SetTitleOffset(1.35);
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.2);
    
    hist_buffer->Draw("");
    hs_cos_theta_psi_Kstar_Y.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    //legend_Jpsipi_X->Draw();
    t->DrawLatex(0.14,0.88, writetext00);
    t->DrawLatex(0.14,0.83, writetext01);
    pictures_path_cpp = angles_path + "cos_theta_psi_Kstar_Y.C";
    pictures_path_png = angles_path + "cos_theta_psi_Kstar_Y.png";
    c_cos_theta_psi_Kstar_Y->SaveAs(pictures_path_cpp.c_str());
    c_cos_theta_psi_Kstar_Y->SaveAs(pictures_path_png.c_str());
    
    cos_theta_Zc_total->Reset();
    cos_theta_Zc_total->Add(cos_theta_Zc_bg);
    cos_theta_Zc_total->Add(cos_theta_Zc_BS_to_JPSI_K_K);
    cos_theta_Zc_total->Add(cos_theta_Zc_BD_to_JPSI_K_PI);
    cos_theta_Zc_total->Add(cos_theta_Zc_BD_to_JPSI_PI_PI);
    cos_theta_Zc_total->Add(cos_theta_Zc_BS_to_JPSI_PI_PI);
    cos_theta_Zc_total->Add(cos_theta_Zc_LAMBDA0B_to_JPSI_P_K);
    if (RARE_DECAYS)
    {
        cos_theta_Zc_total->Add(cos_theta_Zc_LAMBDA0B_to_JPSI_P_PI);
        cos_theta_Zc_total->Add(cos_theta_Zc_BD_to_JPSI_K_K);
        cos_theta_Zc_total->Add(cos_theta_Zc_BS_to_JPSI_K_PI);
    }
    std::string cos_hist_path = angles_path + "cos_hist.root";
    TFile *f_cos_out = new TFile(cos_hist_path.c_str(), "recreate");
    cos_theta_Zc_total->Write();
    f_cos_out->Close();
    
    cout << "\n\n_________________________________________________________\n" << endl;
    
    Double_t model_integral, model_integral_X, model_integral_Y;
    
    cout << "\nJpsipi_2d" << endl;
    printf("%-10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s\n", "name", "data", "model", "BD_KPI", "BS_KK", "BD_PIPI", "BS_PIPI", "LB_PK", "LB_PPI", "BD_KK", "BS_KPI", "comb_bg");
    model_integral = Jpsipi_2d_bg->Integral() + Jpsipi_2d_BD_to_JPSI_K_PI->Integral() + Jpsipi_2d_BS_to_JPSI_K_K->Integral() + Jpsipi_2d_BS_to_JPSI_PI_PI->Integral() + Jpsipi_2d_BD_to_JPSI_PI_PI->Integral() + Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->Integral() + Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->Integral() + Jpsipi_2d_BD_to_JPSI_K_K->Integral() + Jpsipi_2d_BS_to_JPSI_K_PI->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "2d", Jpsipi_2d_data->Integral(), model_integral, Jpsipi_2d_BD_to_JPSI_K_PI->Integral(), Jpsipi_2d_BS_to_JPSI_K_K->Integral(), Jpsipi_2d_BD_to_JPSI_PI_PI->Integral(), Jpsipi_2d_BS_to_JPSI_PI_PI->Integral(), Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->Integral(), Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->Integral(), Jpsipi_2d_BD_to_JPSI_K_K->Integral(), Jpsipi_2d_BS_to_JPSI_K_PI->Integral(), Jpsipi_2d_bg->Integral());
    model_integral_X = Jpsipi_2d_bg->ProjectionX()->Integral() + Jpsipi_2d_BD_to_JPSI_K_PI->ProjectionX()->Integral() + Jpsipi_2d_BS_to_JPSI_K_K->ProjectionX()->Integral() + Jpsipi_2d_BS_to_JPSI_PI_PI->ProjectionX()->Integral() + Jpsipi_2d_BD_to_JPSI_PI_PI->ProjectionX()->Integral() + Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral() + Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral() + Jpsipi_2d_BD_to_JPSI_K_K->ProjectionX()->Integral() + Jpsipi_2d_BS_to_JPSI_K_PI->ProjectionX()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "X", Jpsipi_2d_data->ProjectionX()->Integral(), model_integral_X, Jpsipi_2d_BD_to_JPSI_K_PI->ProjectionX()->Integral(), Jpsipi_2d_BS_to_JPSI_K_K->ProjectionX()->Integral(), Jpsipi_2d_BD_to_JPSI_PI_PI->ProjectionX()->Integral(), Jpsipi_2d_BS_to_JPSI_PI_PI->ProjectionX()->Integral(), Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral(), Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral(), Jpsipi_2d_BD_to_JPSI_K_K->ProjectionX()->Integral(), Jpsipi_2d_BS_to_JPSI_K_PI->ProjectionX()->Integral(), Jpsipi_2d_bg->ProjectionX()->Integral());
    model_integral_Y = Jpsipi_2d_bg->ProjectionY()->Integral() + Jpsipi_2d_BD_to_JPSI_K_PI->ProjectionY()->Integral() + Jpsipi_2d_BS_to_JPSI_K_K->ProjectionY()->Integral() + Jpsipi_2d_BS_to_JPSI_PI_PI->ProjectionY()->Integral() + Jpsipi_2d_BD_to_JPSI_PI_PI->ProjectionY()->Integral() + Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral() + Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral() + Jpsipi_2d_BD_to_JPSI_K_K->ProjectionY()->Integral() + Jpsipi_2d_BS_to_JPSI_K_PI->ProjectionY()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "Y", Jpsipi_2d_data->ProjectionY()->Integral(), model_integral_Y, Jpsipi_2d_BD_to_JPSI_K_PI->ProjectionY()->Integral(), Jpsipi_2d_BS_to_JPSI_K_K->ProjectionY()->Integral(), Jpsipi_2d_BD_to_JPSI_PI_PI->ProjectionY()->Integral(), Jpsipi_2d_BS_to_JPSI_PI_PI->ProjectionY()->Integral(), Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral(), Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral(), Jpsipi_2d_BD_to_JPSI_K_K->ProjectionY()->Integral(), Jpsipi_2d_BS_to_JPSI_K_PI->ProjectionY()->Integral(), Jpsipi_2d_bg->ProjectionY()->Integral());
    
	cout << "\nJpsiK_2d" << endl;
    printf("%-10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s\n", "name", "data", "model", "BD_KPI", "BS_KK", "BD_PIPI", "BS_PIPI", "LB_PK", "LB_PPI", "BD_KK", "BS_KPI", "comb_bg");
    model_integral = JpsiK_2d_bg->Integral() + JpsiK_2d_BD_to_JPSI_K_PI->Integral() + JpsiK_2d_BS_to_JPSI_K_K->Integral() + JpsiK_2d_BS_to_JPSI_PI_PI->Integral() + JpsiK_2d_BD_to_JPSI_PI_PI->Integral() + JpsiK_2d_LAMBDA0B_to_JPSI_P_K->Integral() + JpsiK_2d_LAMBDA0B_to_JPSI_P_PI->Integral() + JpsiK_2d_BD_to_JPSI_K_K->Integral() + JpsiK_2d_BS_to_JPSI_K_PI->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "2d", JpsiK_2d_data->Integral(), model_integral, JpsiK_2d_BD_to_JPSI_K_PI->Integral(), JpsiK_2d_BS_to_JPSI_K_K->Integral(), JpsiK_2d_BD_to_JPSI_PI_PI->Integral(), JpsiK_2d_BS_to_JPSI_PI_PI->Integral(), JpsiK_2d_LAMBDA0B_to_JPSI_P_K->Integral(), JpsiK_2d_LAMBDA0B_to_JPSI_P_PI->Integral(), JpsiK_2d_BD_to_JPSI_K_K->Integral(), JpsiK_2d_BS_to_JPSI_K_PI->Integral(), JpsiK_2d_bg->Integral());
    model_integral_X = JpsiK_2d_bg->ProjectionX()->Integral() + JpsiK_2d_BD_to_JPSI_K_PI->ProjectionX()->Integral() + JpsiK_2d_BS_to_JPSI_K_K->ProjectionX()->Integral() + JpsiK_2d_BS_to_JPSI_PI_PI->ProjectionX()->Integral() + JpsiK_2d_BD_to_JPSI_PI_PI->ProjectionX()->Integral() + JpsiK_2d_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral() + JpsiK_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral() + JpsiK_2d_BD_to_JPSI_K_K->ProjectionX()->Integral() + JpsiK_2d_BS_to_JPSI_K_PI->ProjectionX()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "X", JpsiK_2d_data->ProjectionX()->Integral(), model_integral_X, JpsiK_2d_BD_to_JPSI_K_PI->ProjectionX()->Integral(), JpsiK_2d_BS_to_JPSI_K_K->ProjectionX()->Integral(), JpsiK_2d_BD_to_JPSI_PI_PI->ProjectionX()->Integral(), JpsiK_2d_BS_to_JPSI_PI_PI->ProjectionX()->Integral(), JpsiK_2d_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral(), JpsiK_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral(), JpsiK_2d_BD_to_JPSI_K_K->ProjectionX()->Integral(), JpsiK_2d_BS_to_JPSI_K_PI->ProjectionX()->Integral(), JpsiK_2d_bg->ProjectionX()->Integral());
    model_integral_Y = JpsiK_2d_bg->ProjectionY()->Integral() + JpsiK_2d_BD_to_JPSI_K_PI->ProjectionY()->Integral() + JpsiK_2d_BS_to_JPSI_K_K->ProjectionY()->Integral() + JpsiK_2d_BS_to_JPSI_PI_PI->ProjectionY()->Integral() + JpsiK_2d_BD_to_JPSI_PI_PI->ProjectionY()->Integral() + JpsiK_2d_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral() + JpsiK_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral() + JpsiK_2d_BD_to_JPSI_K_K->ProjectionY()->Integral() + JpsiK_2d_BS_to_JPSI_K_PI->ProjectionY()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "Y", JpsiK_2d_data->ProjectionY()->Integral(), model_integral_Y, JpsiK_2d_BD_to_JPSI_K_PI->ProjectionY()->Integral(), JpsiK_2d_BS_to_JPSI_K_K->ProjectionY()->Integral(), JpsiK_2d_BD_to_JPSI_PI_PI->ProjectionY()->Integral(), JpsiK_2d_BS_to_JPSI_PI_PI->ProjectionY()->Integral(), JpsiK_2d_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral(), JpsiK_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral(), JpsiK_2d_BD_to_JPSI_K_K->ProjectionY()->Integral(), JpsiK_2d_BS_to_JPSI_K_PI->ProjectionY()->Integral(), JpsiK_2d_bg->ProjectionY()->Integral());
    
	cout << "\nKpi_2d" << endl;
    printf("%-10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s\n", "name", "data", "model", "BD_KPI", "BS_KK", "BD_PIPI", "BS_PIPI", "LB_PK", "LB_PPI", "BD_KK", "BS_KPI", "comb_bg");
    model_integral = Kpi_2d_bg->Integral() + Kpi_2d_BD_to_JPSI_K_PI->Integral() + Kpi_2d_BS_to_JPSI_K_K->Integral() + Kpi_2d_BS_to_JPSI_PI_PI->Integral() + Kpi_2d_BD_to_JPSI_PI_PI->Integral() + Kpi_2d_LAMBDA0B_to_JPSI_P_K->Integral() + Kpi_2d_LAMBDA0B_to_JPSI_P_PI->Integral() + Kpi_2d_BD_to_JPSI_K_K->Integral() + Kpi_2d_BS_to_JPSI_K_PI->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "2d", Kpi_2d_data->Integral(), model_integral, Kpi_2d_BD_to_JPSI_K_PI->Integral(), Kpi_2d_BS_to_JPSI_K_K->Integral(), Kpi_2d_BD_to_JPSI_PI_PI->Integral(), Kpi_2d_BS_to_JPSI_PI_PI->Integral(), Kpi_2d_LAMBDA0B_to_JPSI_P_K->Integral(), Kpi_2d_LAMBDA0B_to_JPSI_P_PI->Integral(), Kpi_2d_BD_to_JPSI_K_K->Integral(), Kpi_2d_BS_to_JPSI_K_PI->Integral(), Kpi_2d_bg->Integral());
    model_integral_X = Kpi_2d_bg->ProjectionX()->Integral() + Kpi_2d_BD_to_JPSI_K_PI->ProjectionX()->Integral() + Kpi_2d_BS_to_JPSI_K_K->ProjectionX()->Integral() + Kpi_2d_BS_to_JPSI_PI_PI->ProjectionX()->Integral() + Kpi_2d_BD_to_JPSI_PI_PI->ProjectionX()->Integral() + Kpi_2d_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral() + Kpi_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral() + Kpi_2d_BD_to_JPSI_K_K->ProjectionX()->Integral() + Kpi_2d_BS_to_JPSI_K_PI->ProjectionX()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "X", Kpi_2d_data->ProjectionX()->Integral(), model_integral_X, Kpi_2d_BD_to_JPSI_K_PI->ProjectionX()->Integral(), Kpi_2d_BS_to_JPSI_K_K->ProjectionX()->Integral(), Kpi_2d_BD_to_JPSI_PI_PI->ProjectionX()->Integral(), Kpi_2d_BS_to_JPSI_PI_PI->ProjectionX()->Integral(), Kpi_2d_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral(), Kpi_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral(), Kpi_2d_BD_to_JPSI_K_K->ProjectionX()->Integral(), Kpi_2d_BS_to_JPSI_K_PI->ProjectionX()->Integral(), Kpi_2d_bg->ProjectionX()->Integral());
    model_integral_Y = Kpi_2d_bg->ProjectionY()->Integral() + Kpi_2d_BD_to_JPSI_K_PI->ProjectionY()->Integral() + Kpi_2d_BS_to_JPSI_K_K->ProjectionY()->Integral() + Kpi_2d_BS_to_JPSI_PI_PI->ProjectionY()->Integral() + Kpi_2d_BD_to_JPSI_PI_PI->ProjectionY()->Integral() + Kpi_2d_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral() + Kpi_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral() + Kpi_2d_BD_to_JPSI_K_K->ProjectionY()->Integral() + Kpi_2d_BS_to_JPSI_K_PI->ProjectionY()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "Y", Kpi_2d_data->ProjectionY()->Integral(), model_integral_Y, Kpi_2d_BD_to_JPSI_K_PI->ProjectionY()->Integral(), Kpi_2d_BS_to_JPSI_K_K->ProjectionY()->Integral(), Kpi_2d_BD_to_JPSI_PI_PI->ProjectionY()->Integral(), Kpi_2d_BS_to_JPSI_PI_PI->ProjectionY()->Integral(), Kpi_2d_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral(), Kpi_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral(), Kpi_2d_BD_to_JPSI_K_K->ProjectionY()->Integral(), Kpi_2d_BS_to_JPSI_K_PI->ProjectionY()->Integral(), Kpi_2d_bg->ProjectionY()->Integral());
    
    cout << "\n\n_________________________________________________________" << endl;
    cout << "Angular variables historgrams" << endl;
    cout << "_________________________________________________________\n" << endl;
    
    cout << "\ncos_theta_Zc" << endl;
    printf("%-10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s\n", "name", "data", "model", "BD_KPI", "BS_KK", "BD_PIPI", "BS_PIPI", "LB_PK", "LB_PPI", "BD_KK", "BS_KPI", "comb_bg");
    model_integral = cos_theta_Zc_bg->Integral() + cos_theta_Zc_BD_to_JPSI_K_PI->Integral() + cos_theta_Zc_BS_to_JPSI_K_K->Integral() + cos_theta_Zc_BS_to_JPSI_PI_PI->Integral() + cos_theta_Zc_BD_to_JPSI_PI_PI->Integral() + cos_theta_Zc_LAMBDA0B_to_JPSI_P_K->Integral() + cos_theta_Zc_LAMBDA0B_to_JPSI_P_PI->Integral() + cos_theta_Zc_BD_to_JPSI_K_K->Integral() + cos_theta_Zc_BS_to_JPSI_K_PI->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "2d", cos_theta_Zc_data->Integral(), model_integral, cos_theta_Zc_BD_to_JPSI_K_PI->Integral(), cos_theta_Zc_BS_to_JPSI_K_K->Integral(), cos_theta_Zc_BD_to_JPSI_PI_PI->Integral(), cos_theta_Zc_BS_to_JPSI_PI_PI->Integral(), cos_theta_Zc_LAMBDA0B_to_JPSI_P_K->Integral(), cos_theta_Zc_LAMBDA0B_to_JPSI_P_PI->Integral(), cos_theta_Zc_BD_to_JPSI_K_K->Integral(), cos_theta_Zc_BS_to_JPSI_K_PI->Integral(), cos_theta_Zc_bg->Integral());
    model_integral_X = cos_theta_Zc_bg->ProjectionX()->Integral() + cos_theta_Zc_BD_to_JPSI_K_PI->ProjectionX()->Integral() + cos_theta_Zc_BS_to_JPSI_K_K->ProjectionX()->Integral() + cos_theta_Zc_BS_to_JPSI_PI_PI->ProjectionX()->Integral() + cos_theta_Zc_BD_to_JPSI_PI_PI->ProjectionX()->Integral() + cos_theta_Zc_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral() + cos_theta_Zc_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral() + cos_theta_Zc_BD_to_JPSI_K_K->ProjectionX()->Integral() + cos_theta_Zc_BS_to_JPSI_K_PI->ProjectionX()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "X", cos_theta_Zc_data->ProjectionX()->Integral(), model_integral_X, cos_theta_Zc_BD_to_JPSI_K_PI->ProjectionX()->Integral(), cos_theta_Zc_BS_to_JPSI_K_K->ProjectionX()->Integral(), cos_theta_Zc_BD_to_JPSI_PI_PI->ProjectionX()->Integral(), cos_theta_Zc_BS_to_JPSI_PI_PI->ProjectionX()->Integral(), cos_theta_Zc_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral(), cos_theta_Zc_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral(), cos_theta_Zc_BD_to_JPSI_K_K->ProjectionX()->Integral(), cos_theta_Zc_BS_to_JPSI_K_PI->ProjectionX()->Integral(), cos_theta_Zc_bg->ProjectionX()->Integral());
    model_integral_Y = cos_theta_Zc_bg->ProjectionY()->Integral() + cos_theta_Zc_BD_to_JPSI_K_PI->ProjectionY()->Integral() + cos_theta_Zc_BS_to_JPSI_K_K->ProjectionY()->Integral() + cos_theta_Zc_BS_to_JPSI_PI_PI->ProjectionY()->Integral() + cos_theta_Zc_BD_to_JPSI_PI_PI->ProjectionY()->Integral() + cos_theta_Zc_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral() + cos_theta_Zc_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral() + cos_theta_Zc_BD_to_JPSI_K_K->ProjectionY()->Integral() + cos_theta_Zc_BS_to_JPSI_K_PI->ProjectionY()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "Y", cos_theta_Zc_data->ProjectionY()->Integral(), model_integral_Y, cos_theta_Zc_BD_to_JPSI_K_PI->ProjectionY()->Integral(), cos_theta_Zc_BS_to_JPSI_K_K->ProjectionY()->Integral(), cos_theta_Zc_BD_to_JPSI_PI_PI->ProjectionY()->Integral(), cos_theta_Zc_BS_to_JPSI_PI_PI->ProjectionY()->Integral(), cos_theta_Zc_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral(), cos_theta_Zc_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral(), cos_theta_Zc_BD_to_JPSI_K_K->ProjectionY()->Integral(), cos_theta_Zc_BS_to_JPSI_K_PI->ProjectionY()->Integral(), cos_theta_Zc_bg->ProjectionY()->Integral());
    
    cout << "\ncos_theta_B0" << endl;
    printf("%-10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s\n", "name", "data", "model", "BD_KPI", "BS_KK", "BD_PIPI", "BS_PIPI", "LB_PK", "LB_PPI", "BD_KK", "BS_KPI", "comb_bg");
    model_integral = cos_theta_B0_bg->Integral() + cos_theta_B0_BD_to_JPSI_K_PI->Integral() + cos_theta_B0_BS_to_JPSI_K_K->Integral() + cos_theta_B0_BS_to_JPSI_PI_PI->Integral() + cos_theta_B0_BD_to_JPSI_PI_PI->Integral() + cos_theta_B0_LAMBDA0B_to_JPSI_P_K->Integral() + cos_theta_B0_LAMBDA0B_to_JPSI_P_PI->Integral() + cos_theta_B0_BD_to_JPSI_K_K->Integral() + cos_theta_B0_BS_to_JPSI_K_PI->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "2d", cos_theta_B0_data->Integral(), model_integral, cos_theta_B0_BD_to_JPSI_K_PI->Integral(), cos_theta_B0_BS_to_JPSI_K_K->Integral(), cos_theta_B0_BD_to_JPSI_PI_PI->Integral(), cos_theta_B0_BS_to_JPSI_PI_PI->Integral(), cos_theta_B0_LAMBDA0B_to_JPSI_P_K->Integral(), cos_theta_B0_LAMBDA0B_to_JPSI_P_PI->Integral(), cos_theta_B0_BD_to_JPSI_K_K->Integral(), cos_theta_B0_BS_to_JPSI_K_PI->Integral(), cos_theta_B0_bg->Integral());
    model_integral_X = cos_theta_B0_bg->ProjectionX()->Integral() + cos_theta_B0_BD_to_JPSI_K_PI->ProjectionX()->Integral() + cos_theta_B0_BS_to_JPSI_K_K->ProjectionX()->Integral() + cos_theta_B0_BS_to_JPSI_PI_PI->ProjectionX()->Integral() + cos_theta_B0_BD_to_JPSI_PI_PI->ProjectionX()->Integral() + cos_theta_B0_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral() + cos_theta_B0_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral() + cos_theta_B0_BD_to_JPSI_K_K->ProjectionX()->Integral() + cos_theta_B0_BS_to_JPSI_K_PI->ProjectionX()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "X", cos_theta_B0_data->ProjectionX()->Integral(), model_integral_X, cos_theta_B0_BD_to_JPSI_K_PI->ProjectionX()->Integral(), cos_theta_B0_BS_to_JPSI_K_K->ProjectionX()->Integral(), cos_theta_B0_BD_to_JPSI_PI_PI->ProjectionX()->Integral(), cos_theta_B0_BS_to_JPSI_PI_PI->ProjectionX()->Integral(), cos_theta_B0_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral(), cos_theta_B0_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral(), cos_theta_B0_BD_to_JPSI_K_K->ProjectionX()->Integral(), cos_theta_B0_BS_to_JPSI_K_PI->ProjectionX()->Integral(), cos_theta_B0_bg->ProjectionX()->Integral());
    model_integral_Y = cos_theta_B0_bg->ProjectionY()->Integral() + cos_theta_B0_BD_to_JPSI_K_PI->ProjectionY()->Integral() + cos_theta_B0_BS_to_JPSI_K_K->ProjectionY()->Integral() + cos_theta_B0_BS_to_JPSI_PI_PI->ProjectionY()->Integral() + cos_theta_B0_BD_to_JPSI_PI_PI->ProjectionY()->Integral() + cos_theta_B0_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral() + cos_theta_B0_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral() + cos_theta_B0_BD_to_JPSI_K_K->ProjectionY()->Integral() + cos_theta_B0_BS_to_JPSI_K_PI->ProjectionY()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "Y", cos_theta_B0_data->ProjectionY()->Integral(), model_integral_Y, cos_theta_B0_BD_to_JPSI_K_PI->ProjectionY()->Integral(), cos_theta_B0_BS_to_JPSI_K_K->ProjectionY()->Integral(), cos_theta_B0_BD_to_JPSI_PI_PI->ProjectionY()->Integral(), cos_theta_B0_BS_to_JPSI_PI_PI->ProjectionY()->Integral(), cos_theta_B0_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral(), cos_theta_B0_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral(), cos_theta_B0_BD_to_JPSI_K_K->ProjectionY()->Integral(), cos_theta_B0_BS_to_JPSI_K_PI->ProjectionY()->Integral(), cos_theta_B0_bg->ProjectionY()->Integral());
    
    cout << "\nphi_psi" << endl;
    printf("%-10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s\n", "name", "data", "model", "BD_KPI", "BS_KK", "BD_PIPI", "BS_PIPI", "LB_PK", "LB_PPI", "BD_KK", "BS_KPI", "comb_bg");
    model_integral = phi_psi_bg->Integral() + phi_psi_BD_to_JPSI_K_PI->Integral() + phi_psi_BS_to_JPSI_K_K->Integral() + phi_psi_BS_to_JPSI_PI_PI->Integral() + phi_psi_BD_to_JPSI_PI_PI->Integral() + phi_psi_LAMBDA0B_to_JPSI_P_K->Integral() + phi_psi_LAMBDA0B_to_JPSI_P_PI->Integral() + phi_psi_BD_to_JPSI_K_K->Integral() + phi_psi_BS_to_JPSI_K_PI->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "2d", phi_psi_data->Integral(), model_integral, phi_psi_BD_to_JPSI_K_PI->Integral(), phi_psi_BS_to_JPSI_K_K->Integral(), phi_psi_BD_to_JPSI_PI_PI->Integral(), phi_psi_BS_to_JPSI_PI_PI->Integral(), phi_psi_LAMBDA0B_to_JPSI_P_K->Integral(), phi_psi_LAMBDA0B_to_JPSI_P_PI->Integral(), phi_psi_BD_to_JPSI_K_K->Integral(), phi_psi_BS_to_JPSI_K_PI->Integral(), phi_psi_bg->Integral());
    model_integral_X = phi_psi_bg->ProjectionX()->Integral() + phi_psi_BD_to_JPSI_K_PI->ProjectionX()->Integral() + phi_psi_BS_to_JPSI_K_K->ProjectionX()->Integral() + phi_psi_BS_to_JPSI_PI_PI->ProjectionX()->Integral() + phi_psi_BD_to_JPSI_PI_PI->ProjectionX()->Integral() + phi_psi_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral() + phi_psi_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral() + phi_psi_BD_to_JPSI_K_K->ProjectionX()->Integral() + phi_psi_BS_to_JPSI_K_PI->ProjectionX()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "X", phi_psi_data->ProjectionX()->Integral(), model_integral_X, phi_psi_BD_to_JPSI_K_PI->ProjectionX()->Integral(), phi_psi_BS_to_JPSI_K_K->ProjectionX()->Integral(), phi_psi_BD_to_JPSI_PI_PI->ProjectionX()->Integral(), phi_psi_BS_to_JPSI_PI_PI->ProjectionX()->Integral(), phi_psi_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral(), phi_psi_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral(), phi_psi_BD_to_JPSI_K_K->ProjectionX()->Integral(), phi_psi_BS_to_JPSI_K_PI->ProjectionX()->Integral(), phi_psi_bg->ProjectionX()->Integral());
    model_integral_Y = phi_psi_bg->ProjectionY()->Integral() + phi_psi_BD_to_JPSI_K_PI->ProjectionY()->Integral() + phi_psi_BS_to_JPSI_K_K->ProjectionY()->Integral() + phi_psi_BS_to_JPSI_PI_PI->ProjectionY()->Integral() + phi_psi_BD_to_JPSI_PI_PI->ProjectionY()->Integral() + phi_psi_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral() + phi_psi_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral() + phi_psi_BD_to_JPSI_K_K->ProjectionY()->Integral() + phi_psi_BS_to_JPSI_K_PI->ProjectionY()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "Y", phi_psi_data->ProjectionY()->Integral(), model_integral_Y, phi_psi_BD_to_JPSI_K_PI->ProjectionY()->Integral(), phi_psi_BS_to_JPSI_K_K->ProjectionY()->Integral(), phi_psi_BD_to_JPSI_PI_PI->ProjectionY()->Integral(), phi_psi_BS_to_JPSI_PI_PI->ProjectionY()->Integral(), phi_psi_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral(), phi_psi_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral(), phi_psi_BD_to_JPSI_K_K->ProjectionY()->Integral(), phi_psi_BS_to_JPSI_K_PI->ProjectionY()->Integral(), phi_psi_bg->ProjectionY()->Integral());
    
    cout << "\nphi_K" << endl;
    printf("%-10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s\n", "name", "data", "model", "BD_KPI", "BS_KK", "BD_PIPI", "BS_PIPI", "LB_PK", "LB_PPI", "BD_KK", "BS_KPI", "comb_bg");
    model_integral = phi_K_bg->Integral() + phi_K_BD_to_JPSI_K_PI->Integral() + phi_K_BS_to_JPSI_K_K->Integral() + phi_K_BS_to_JPSI_PI_PI->Integral() + phi_K_BD_to_JPSI_PI_PI->Integral() + phi_K_LAMBDA0B_to_JPSI_P_K->Integral() + phi_K_LAMBDA0B_to_JPSI_P_PI->Integral() + phi_K_BD_to_JPSI_K_K->Integral() + phi_K_BS_to_JPSI_K_PI->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "2d", phi_K_data->Integral(), model_integral, phi_K_BD_to_JPSI_K_PI->Integral(), phi_K_BS_to_JPSI_K_K->Integral(), phi_K_BD_to_JPSI_PI_PI->Integral(), phi_K_BS_to_JPSI_PI_PI->Integral(), phi_K_LAMBDA0B_to_JPSI_P_K->Integral(), phi_K_LAMBDA0B_to_JPSI_P_PI->Integral(), phi_K_BD_to_JPSI_K_K->Integral(), phi_K_BS_to_JPSI_K_PI->Integral(), phi_K_bg->Integral());
    model_integral_X = phi_K_bg->ProjectionX()->Integral() + phi_K_BD_to_JPSI_K_PI->ProjectionX()->Integral() + phi_K_BS_to_JPSI_K_K->ProjectionX()->Integral() + phi_K_BS_to_JPSI_PI_PI->ProjectionX()->Integral() + phi_K_BD_to_JPSI_PI_PI->ProjectionX()->Integral() + phi_K_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral() + phi_K_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral() + phi_K_BD_to_JPSI_K_K->ProjectionX()->Integral() + phi_K_BS_to_JPSI_K_PI->ProjectionX()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "X", phi_K_data->ProjectionX()->Integral(), model_integral_X, phi_K_BD_to_JPSI_K_PI->ProjectionX()->Integral(), phi_K_BS_to_JPSI_K_K->ProjectionX()->Integral(), phi_K_BD_to_JPSI_PI_PI->ProjectionX()->Integral(), phi_K_BS_to_JPSI_PI_PI->ProjectionX()->Integral(), phi_K_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral(), phi_K_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral(), phi_K_BD_to_JPSI_K_K->ProjectionX()->Integral(), phi_K_BS_to_JPSI_K_PI->ProjectionX()->Integral(), phi_K_bg->ProjectionX()->Integral());
    model_integral_Y = phi_K_bg->ProjectionY()->Integral() + phi_K_BD_to_JPSI_K_PI->ProjectionY()->Integral() + phi_K_BS_to_JPSI_K_K->ProjectionY()->Integral() + phi_K_BS_to_JPSI_PI_PI->ProjectionY()->Integral() + phi_K_BD_to_JPSI_PI_PI->ProjectionY()->Integral() + phi_K_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral() + phi_K_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral() + phi_K_BD_to_JPSI_K_K->ProjectionY()->Integral() + phi_K_BS_to_JPSI_K_PI->ProjectionY()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "Y", phi_K_data->ProjectionY()->Integral(), model_integral_Y, phi_K_BD_to_JPSI_K_PI->ProjectionY()->Integral(), phi_K_BS_to_JPSI_K_K->ProjectionY()->Integral(), phi_K_BD_to_JPSI_PI_PI->ProjectionY()->Integral(), phi_K_BS_to_JPSI_PI_PI->ProjectionY()->Integral(), phi_K_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral(), phi_K_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral(), phi_K_BD_to_JPSI_K_K->ProjectionY()->Integral(), phi_K_BS_to_JPSI_K_PI->ProjectionY()->Integral(), phi_K_bg->ProjectionY()->Integral());
    
    cout << "\nalpha_mu" << endl;
    printf("%-10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s\n", "name", "data", "model", "BD_KPI", "BS_KK", "BD_PIPI", "BS_PIPI", "LB_PK", "LB_PPI", "BD_KK", "BS_KPI", "comb_bg");
    model_integral = alpha_mu_bg->Integral() + alpha_mu_BD_to_JPSI_K_PI->Integral() + alpha_mu_BS_to_JPSI_K_K->Integral() + alpha_mu_BS_to_JPSI_PI_PI->Integral() + alpha_mu_BD_to_JPSI_PI_PI->Integral() + alpha_mu_LAMBDA0B_to_JPSI_P_K->Integral() + alpha_mu_LAMBDA0B_to_JPSI_P_PI->Integral() + alpha_mu_BD_to_JPSI_K_K->Integral() + alpha_mu_BS_to_JPSI_K_PI->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "2d", alpha_mu_data->Integral(), model_integral, alpha_mu_BD_to_JPSI_K_PI->Integral(), alpha_mu_BS_to_JPSI_K_K->Integral(), alpha_mu_BD_to_JPSI_PI_PI->Integral(), alpha_mu_BS_to_JPSI_PI_PI->Integral(), alpha_mu_LAMBDA0B_to_JPSI_P_K->Integral(), alpha_mu_LAMBDA0B_to_JPSI_P_PI->Integral(), alpha_mu_BD_to_JPSI_K_K->Integral(), alpha_mu_BS_to_JPSI_K_PI->Integral(), alpha_mu_bg->Integral());
    model_integral_X = alpha_mu_bg->ProjectionX()->Integral() + alpha_mu_BD_to_JPSI_K_PI->ProjectionX()->Integral() + alpha_mu_BS_to_JPSI_K_K->ProjectionX()->Integral() + alpha_mu_BS_to_JPSI_PI_PI->ProjectionX()->Integral() + alpha_mu_BD_to_JPSI_PI_PI->ProjectionX()->Integral() + alpha_mu_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral() + alpha_mu_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral() + alpha_mu_BD_to_JPSI_K_K->ProjectionX()->Integral() + alpha_mu_BS_to_JPSI_K_PI->ProjectionX()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "X", alpha_mu_data->ProjectionX()->Integral(), model_integral_X, alpha_mu_BD_to_JPSI_K_PI->ProjectionX()->Integral(), alpha_mu_BS_to_JPSI_K_K->ProjectionX()->Integral(), alpha_mu_BD_to_JPSI_PI_PI->ProjectionX()->Integral(), alpha_mu_BS_to_JPSI_PI_PI->ProjectionX()->Integral(), alpha_mu_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral(), alpha_mu_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral(), alpha_mu_BD_to_JPSI_K_K->ProjectionX()->Integral(), alpha_mu_BS_to_JPSI_K_PI->ProjectionX()->Integral(), alpha_mu_bg->ProjectionX()->Integral());
    model_integral_Y = alpha_mu_bg->ProjectionY()->Integral() + alpha_mu_BD_to_JPSI_K_PI->ProjectionY()->Integral() + alpha_mu_BS_to_JPSI_K_K->ProjectionY()->Integral() + alpha_mu_BS_to_JPSI_PI_PI->ProjectionY()->Integral() + alpha_mu_BD_to_JPSI_PI_PI->ProjectionY()->Integral() + alpha_mu_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral() + alpha_mu_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral() + alpha_mu_BD_to_JPSI_K_K->ProjectionY()->Integral() + alpha_mu_BS_to_JPSI_K_PI->ProjectionY()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "Y", alpha_mu_data->ProjectionY()->Integral(), model_integral_Y, alpha_mu_BD_to_JPSI_K_PI->ProjectionY()->Integral(), alpha_mu_BS_to_JPSI_K_K->ProjectionY()->Integral(), alpha_mu_BD_to_JPSI_PI_PI->ProjectionY()->Integral(), alpha_mu_BS_to_JPSI_PI_PI->ProjectionY()->Integral(), alpha_mu_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral(), alpha_mu_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral(), alpha_mu_BD_to_JPSI_K_K->ProjectionY()->Integral(), alpha_mu_BS_to_JPSI_K_PI->ProjectionY()->Integral(), alpha_mu_bg->ProjectionY()->Integral());
    
    cout << "\nphi_mu_Kstar" << endl;
    printf("%-10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s\n", "name", "data", "model", "BD_KPI", "BS_KK", "BD_PIPI", "BS_PIPI", "LB_PK", "LB_PPI", "BD_KK", "BS_KPI", "comb_bg");
    model_integral = phi_mu_Kstar_bg->Integral() + phi_mu_Kstar_BD_to_JPSI_K_PI->Integral() + phi_mu_Kstar_BS_to_JPSI_K_K->Integral() + phi_mu_Kstar_BS_to_JPSI_PI_PI->Integral() + phi_mu_Kstar_BD_to_JPSI_PI_PI->Integral() + phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K->Integral() + phi_mu_Kstar_LAMBDA0B_to_JPSI_P_PI->Integral() + phi_mu_Kstar_BD_to_JPSI_K_K->Integral() + phi_mu_Kstar_BS_to_JPSI_K_PI->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "2d", phi_mu_Kstar_data->Integral(), model_integral, phi_mu_Kstar_BD_to_JPSI_K_PI->Integral(), phi_mu_Kstar_BS_to_JPSI_K_K->Integral(), phi_mu_Kstar_BD_to_JPSI_PI_PI->Integral(), phi_mu_Kstar_BS_to_JPSI_PI_PI->Integral(), phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K->Integral(), phi_mu_Kstar_LAMBDA0B_to_JPSI_P_PI->Integral(), phi_mu_Kstar_BD_to_JPSI_K_K->Integral(), phi_mu_Kstar_BS_to_JPSI_K_PI->Integral(), phi_mu_Kstar_bg->Integral());
    model_integral_X = phi_mu_Kstar_bg->ProjectionX()->Integral() + phi_mu_Kstar_BD_to_JPSI_K_PI->ProjectionX()->Integral() + phi_mu_Kstar_BS_to_JPSI_K_K->ProjectionX()->Integral() + phi_mu_Kstar_BS_to_JPSI_PI_PI->ProjectionX()->Integral() + phi_mu_Kstar_BD_to_JPSI_PI_PI->ProjectionX()->Integral() + phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral() + phi_mu_Kstar_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral() + phi_mu_Kstar_BD_to_JPSI_K_K->ProjectionX()->Integral() + phi_mu_Kstar_BS_to_JPSI_K_PI->ProjectionX()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "X", phi_mu_Kstar_data->ProjectionX()->Integral(), model_integral_X, phi_mu_Kstar_BD_to_JPSI_K_PI->ProjectionX()->Integral(), phi_mu_Kstar_BS_to_JPSI_K_K->ProjectionX()->Integral(), phi_mu_Kstar_BD_to_JPSI_PI_PI->ProjectionX()->Integral(), phi_mu_Kstar_BS_to_JPSI_PI_PI->ProjectionX()->Integral(), phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral(), phi_mu_Kstar_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral(), phi_mu_Kstar_BD_to_JPSI_K_K->ProjectionX()->Integral(), phi_mu_Kstar_BS_to_JPSI_K_PI->ProjectionX()->Integral(), phi_mu_Kstar_bg->ProjectionX()->Integral());
    model_integral_Y = phi_mu_Kstar_bg->ProjectionY()->Integral() + phi_mu_Kstar_BD_to_JPSI_K_PI->ProjectionY()->Integral() + phi_mu_Kstar_BS_to_JPSI_K_K->ProjectionY()->Integral() + phi_mu_Kstar_BS_to_JPSI_PI_PI->ProjectionY()->Integral() + phi_mu_Kstar_BD_to_JPSI_PI_PI->ProjectionY()->Integral() + phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral() + phi_mu_Kstar_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral() + phi_mu_Kstar_BD_to_JPSI_K_K->ProjectionY()->Integral() + phi_mu_Kstar_BS_to_JPSI_K_PI->ProjectionY()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "Y", phi_mu_Kstar_data->ProjectionY()->Integral(), model_integral_Y, phi_mu_Kstar_BD_to_JPSI_K_PI->ProjectionY()->Integral(), phi_mu_Kstar_BS_to_JPSI_K_K->ProjectionY()->Integral(), phi_mu_Kstar_BD_to_JPSI_PI_PI->ProjectionY()->Integral(), phi_mu_Kstar_BS_to_JPSI_PI_PI->ProjectionY()->Integral(), phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral(), phi_mu_Kstar_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral(), phi_mu_Kstar_BD_to_JPSI_K_K->ProjectionY()->Integral(), phi_mu_Kstar_BS_to_JPSI_K_PI->ProjectionY()->Integral(), phi_mu_Kstar_bg->ProjectionY()->Integral());
    
    cout << "\nphi_mu_X" << endl;
    printf("%-10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s\n", "name", "data", "model", "BD_KPI", "BS_KK", "BD_PIPI", "BS_PIPI", "LB_PK", "LB_PPI", "BD_KK", "BS_KPI", "comb_bg");
    model_integral = phi_mu_X_bg->Integral() + phi_mu_X_BD_to_JPSI_K_PI->Integral() + phi_mu_X_BS_to_JPSI_K_K->Integral() + phi_mu_X_BS_to_JPSI_PI_PI->Integral() + phi_mu_X_BD_to_JPSI_PI_PI->Integral() + phi_mu_X_LAMBDA0B_to_JPSI_P_K->Integral() + phi_mu_X_LAMBDA0B_to_JPSI_P_PI->Integral() + phi_mu_X_BD_to_JPSI_K_K->Integral() + phi_mu_X_BS_to_JPSI_K_PI->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "2d", phi_mu_X_data->Integral(), model_integral, phi_mu_X_BD_to_JPSI_K_PI->Integral(), phi_mu_X_BS_to_JPSI_K_K->Integral(), phi_mu_X_BD_to_JPSI_PI_PI->Integral(), phi_mu_X_BS_to_JPSI_PI_PI->Integral(), phi_mu_X_LAMBDA0B_to_JPSI_P_K->Integral(), phi_mu_X_LAMBDA0B_to_JPSI_P_PI->Integral(), phi_mu_X_BD_to_JPSI_K_K->Integral(), phi_mu_X_BS_to_JPSI_K_PI->Integral(), phi_mu_X_bg->Integral());
    model_integral_X = phi_mu_X_bg->ProjectionX()->Integral() + phi_mu_X_BD_to_JPSI_K_PI->ProjectionX()->Integral() + phi_mu_X_BS_to_JPSI_K_K->ProjectionX()->Integral() + phi_mu_X_BS_to_JPSI_PI_PI->ProjectionX()->Integral() + phi_mu_X_BD_to_JPSI_PI_PI->ProjectionX()->Integral() + phi_mu_X_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral() + phi_mu_X_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral() + phi_mu_X_BD_to_JPSI_K_K->ProjectionX()->Integral() + phi_mu_X_BS_to_JPSI_K_PI->ProjectionX()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "X", phi_mu_X_data->ProjectionX()->Integral(), model_integral_X, phi_mu_X_BD_to_JPSI_K_PI->ProjectionX()->Integral(), phi_mu_X_BS_to_JPSI_K_K->ProjectionX()->Integral(), phi_mu_X_BD_to_JPSI_PI_PI->ProjectionX()->Integral(), phi_mu_X_BS_to_JPSI_PI_PI->ProjectionX()->Integral(), phi_mu_X_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral(), phi_mu_X_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral(), phi_mu_X_BD_to_JPSI_K_K->ProjectionX()->Integral(), phi_mu_X_BS_to_JPSI_K_PI->ProjectionX()->Integral(), phi_mu_X_bg->ProjectionX()->Integral());
    model_integral_Y = phi_mu_X_bg->ProjectionY()->Integral() + phi_mu_X_BD_to_JPSI_K_PI->ProjectionY()->Integral() + phi_mu_X_BS_to_JPSI_K_K->ProjectionY()->Integral() + phi_mu_X_BS_to_JPSI_PI_PI->ProjectionY()->Integral() + phi_mu_X_BD_to_JPSI_PI_PI->ProjectionY()->Integral() + phi_mu_X_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral() + phi_mu_X_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral() + phi_mu_X_BD_to_JPSI_K_K->ProjectionY()->Integral() + phi_mu_X_BS_to_JPSI_K_PI->ProjectionY()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "Y", phi_mu_X_data->ProjectionY()->Integral(), model_integral_Y, phi_mu_X_BD_to_JPSI_K_PI->ProjectionY()->Integral(), phi_mu_X_BS_to_JPSI_K_K->ProjectionY()->Integral(), phi_mu_X_BD_to_JPSI_PI_PI->ProjectionY()->Integral(), phi_mu_X_BS_to_JPSI_PI_PI->ProjectionY()->Integral(), phi_mu_X_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral(), phi_mu_X_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral(), phi_mu_X_BD_to_JPSI_K_K->ProjectionY()->Integral(), phi_mu_X_BS_to_JPSI_K_PI->ProjectionY()->Integral(), phi_mu_X_bg->ProjectionY()->Integral());
    
    cout << "\ncos_theta_psi_X" << endl;
    printf("%-10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s\n", "name", "data", "model", "BD_KPI", "BS_KK", "BD_PIPI", "BS_PIPI", "LB_PK", "LB_PPI", "BD_KK", "BS_KPI", "comb_bg");
    model_integral = cos_theta_psi_X_bg->Integral() + cos_theta_psi_X_BD_to_JPSI_K_PI->Integral() + cos_theta_psi_X_BS_to_JPSI_K_K->Integral() + cos_theta_psi_X_BS_to_JPSI_PI_PI->Integral() + cos_theta_psi_X_BD_to_JPSI_PI_PI->Integral() + cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K->Integral() + cos_theta_psi_X_LAMBDA0B_to_JPSI_P_PI->Integral() + cos_theta_psi_X_BD_to_JPSI_K_K->Integral() + cos_theta_psi_X_BS_to_JPSI_K_PI->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "2d", cos_theta_psi_X_data->Integral(), model_integral, cos_theta_psi_X_BD_to_JPSI_K_PI->Integral(), cos_theta_psi_X_BS_to_JPSI_K_K->Integral(), cos_theta_psi_X_BD_to_JPSI_PI_PI->Integral(), cos_theta_psi_X_BS_to_JPSI_PI_PI->Integral(), cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K->Integral(), cos_theta_psi_X_LAMBDA0B_to_JPSI_P_PI->Integral(), cos_theta_psi_X_BD_to_JPSI_K_K->Integral(), cos_theta_psi_X_BS_to_JPSI_K_PI->Integral(), cos_theta_psi_X_bg->Integral());
    model_integral_X = cos_theta_psi_X_bg->ProjectionX()->Integral() + cos_theta_psi_X_BD_to_JPSI_K_PI->ProjectionX()->Integral() + cos_theta_psi_X_BS_to_JPSI_K_K->ProjectionX()->Integral() + cos_theta_psi_X_BS_to_JPSI_PI_PI->ProjectionX()->Integral() + cos_theta_psi_X_BD_to_JPSI_PI_PI->ProjectionX()->Integral() + cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral() + cos_theta_psi_X_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral() + cos_theta_psi_X_BD_to_JPSI_K_K->ProjectionX()->Integral() + cos_theta_psi_X_BS_to_JPSI_K_PI->ProjectionX()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "X", cos_theta_psi_X_data->ProjectionX()->Integral(), model_integral_X, cos_theta_psi_X_BD_to_JPSI_K_PI->ProjectionX()->Integral(), cos_theta_psi_X_BS_to_JPSI_K_K->ProjectionX()->Integral(), cos_theta_psi_X_BD_to_JPSI_PI_PI->ProjectionX()->Integral(), cos_theta_psi_X_BS_to_JPSI_PI_PI->ProjectionX()->Integral(), cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral(), cos_theta_psi_X_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral(), cos_theta_psi_X_BD_to_JPSI_K_K->ProjectionX()->Integral(), cos_theta_psi_X_BS_to_JPSI_K_PI->ProjectionX()->Integral(), cos_theta_psi_X_bg->ProjectionX()->Integral());
    model_integral_Y = cos_theta_psi_X_bg->ProjectionY()->Integral() + cos_theta_psi_X_BD_to_JPSI_K_PI->ProjectionY()->Integral() + cos_theta_psi_X_BS_to_JPSI_K_K->ProjectionY()->Integral() + cos_theta_psi_X_BS_to_JPSI_PI_PI->ProjectionY()->Integral() + cos_theta_psi_X_BD_to_JPSI_PI_PI->ProjectionY()->Integral() + cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral() + cos_theta_psi_X_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral() + cos_theta_psi_X_BD_to_JPSI_K_K->ProjectionY()->Integral() + cos_theta_psi_X_BS_to_JPSI_K_PI->ProjectionY()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "Y", cos_theta_psi_X_data->ProjectionY()->Integral(), model_integral_Y, cos_theta_psi_X_BD_to_JPSI_K_PI->ProjectionY()->Integral(), cos_theta_psi_X_BS_to_JPSI_K_K->ProjectionY()->Integral(), cos_theta_psi_X_BD_to_JPSI_PI_PI->ProjectionY()->Integral(), cos_theta_psi_X_BS_to_JPSI_PI_PI->ProjectionY()->Integral(), cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral(), cos_theta_psi_X_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral(), cos_theta_psi_X_BD_to_JPSI_K_K->ProjectionY()->Integral(), cos_theta_psi_X_BS_to_JPSI_K_PI->ProjectionY()->Integral(), cos_theta_psi_X_bg->ProjectionY()->Integral());
    
    cout << "\ncos_theta_Kstar" << endl;
    printf("%-10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s\n", "name", "data", "model", "BD_KPI", "BS_KK", "BD_PIPI", "BS_PIPI", "LB_PK", "LB_PPI", "BD_KK", "BS_KPI", "comb_bg");
    model_integral = cos_theta_Kstar_bg->Integral() + cos_theta_Kstar_BD_to_JPSI_K_PI->Integral() + cos_theta_Kstar_BS_to_JPSI_K_K->Integral() + cos_theta_Kstar_BS_to_JPSI_PI_PI->Integral() + cos_theta_Kstar_BD_to_JPSI_PI_PI->Integral() + cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->Integral() + cos_theta_Kstar_LAMBDA0B_to_JPSI_P_PI->Integral() + cos_theta_Kstar_BD_to_JPSI_K_K->Integral() + cos_theta_Kstar_BS_to_JPSI_K_PI->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "2d", cos_theta_Kstar_data->Integral(), model_integral, cos_theta_Kstar_BD_to_JPSI_K_PI->Integral(), cos_theta_Kstar_BS_to_JPSI_K_K->Integral(), cos_theta_Kstar_BD_to_JPSI_PI_PI->Integral(), cos_theta_Kstar_BS_to_JPSI_PI_PI->Integral(), cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->Integral(), cos_theta_Kstar_LAMBDA0B_to_JPSI_P_PI->Integral(), cos_theta_Kstar_BD_to_JPSI_K_K->Integral(), cos_theta_Kstar_BS_to_JPSI_K_PI->Integral(), cos_theta_Kstar_bg->Integral());
    model_integral_X = cos_theta_Kstar_bg->ProjectionX()->Integral() + cos_theta_Kstar_BD_to_JPSI_K_PI->ProjectionX()->Integral() + cos_theta_Kstar_BS_to_JPSI_K_K->ProjectionX()->Integral() + cos_theta_Kstar_BS_to_JPSI_PI_PI->ProjectionX()->Integral() + cos_theta_Kstar_BD_to_JPSI_PI_PI->ProjectionX()->Integral() + cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral() + cos_theta_Kstar_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral() + cos_theta_Kstar_BD_to_JPSI_K_K->ProjectionX()->Integral() + cos_theta_Kstar_BS_to_JPSI_K_PI->ProjectionX()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "X", cos_theta_Kstar_data->ProjectionX()->Integral(), model_integral_X, cos_theta_Kstar_BD_to_JPSI_K_PI->ProjectionX()->Integral(), cos_theta_Kstar_BS_to_JPSI_K_K->ProjectionX()->Integral(), cos_theta_Kstar_BD_to_JPSI_PI_PI->ProjectionX()->Integral(), cos_theta_Kstar_BS_to_JPSI_PI_PI->ProjectionX()->Integral(), cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral(), cos_theta_Kstar_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral(), cos_theta_Kstar_BD_to_JPSI_K_K->ProjectionX()->Integral(), cos_theta_Kstar_BS_to_JPSI_K_PI->ProjectionX()->Integral(), cos_theta_Kstar_bg->ProjectionX()->Integral());
    model_integral_Y = cos_theta_Kstar_bg->ProjectionY()->Integral() + cos_theta_Kstar_BD_to_JPSI_K_PI->ProjectionY()->Integral() + cos_theta_Kstar_BS_to_JPSI_K_K->ProjectionY()->Integral() + cos_theta_Kstar_BS_to_JPSI_PI_PI->ProjectionY()->Integral() + cos_theta_Kstar_BD_to_JPSI_PI_PI->ProjectionY()->Integral() + cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral() + cos_theta_Kstar_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral() + cos_theta_Kstar_BD_to_JPSI_K_K->ProjectionY()->Integral() + cos_theta_Kstar_BS_to_JPSI_K_PI->ProjectionY()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "Y", cos_theta_Kstar_data->ProjectionY()->Integral(), model_integral_Y, cos_theta_Kstar_BD_to_JPSI_K_PI->ProjectionY()->Integral(), cos_theta_Kstar_BS_to_JPSI_K_K->ProjectionY()->Integral(), cos_theta_Kstar_BD_to_JPSI_PI_PI->ProjectionY()->Integral(), cos_theta_Kstar_BS_to_JPSI_PI_PI->ProjectionY()->Integral(), cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral(), cos_theta_Kstar_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral(), cos_theta_Kstar_BD_to_JPSI_K_K->ProjectionY()->Integral(), cos_theta_Kstar_BS_to_JPSI_K_PI->ProjectionY()->Integral(), cos_theta_Kstar_bg->ProjectionY()->Integral());
    
    cout << "\ncos_theta_psi_Kstar" << endl;
    printf("%-10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s\n", "name", "data", "model", "BD_KPI", "BS_KK", "BD_PIPI", "BS_PIPI", "LB_PK", "LB_PPI", "BD_KK", "BS_KPI", "comb_bg");
    model_integral = cos_theta_psi_Kstar_bg->Integral() + cos_theta_psi_Kstar_BD_to_JPSI_K_PI->Integral() + cos_theta_psi_Kstar_BS_to_JPSI_K_K->Integral() + cos_theta_psi_Kstar_BS_to_JPSI_PI_PI->Integral() + cos_theta_psi_Kstar_BD_to_JPSI_PI_PI->Integral() + cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K->Integral() + cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_PI->Integral() + cos_theta_psi_Kstar_BD_to_JPSI_K_K->Integral() + cos_theta_psi_Kstar_BS_to_JPSI_K_PI->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "2d", cos_theta_psi_Kstar_data->Integral(), model_integral, cos_theta_psi_Kstar_BD_to_JPSI_K_PI->Integral(), cos_theta_psi_Kstar_BS_to_JPSI_K_K->Integral(), cos_theta_psi_Kstar_BD_to_JPSI_PI_PI->Integral(), cos_theta_psi_Kstar_BS_to_JPSI_PI_PI->Integral(), cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K->Integral(), cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_PI->Integral(), cos_theta_psi_Kstar_BD_to_JPSI_K_K->Integral(), cos_theta_psi_Kstar_BS_to_JPSI_K_PI->Integral(), cos_theta_psi_Kstar_bg->Integral());
    model_integral_X = cos_theta_psi_Kstar_bg->ProjectionX()->Integral() + cos_theta_psi_Kstar_BD_to_JPSI_K_PI->ProjectionX()->Integral() + cos_theta_psi_Kstar_BS_to_JPSI_K_K->ProjectionX()->Integral() + cos_theta_psi_Kstar_BS_to_JPSI_PI_PI->ProjectionX()->Integral() + cos_theta_psi_Kstar_BD_to_JPSI_PI_PI->ProjectionX()->Integral() + cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral() + cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral() + cos_theta_psi_Kstar_BD_to_JPSI_K_K->ProjectionX()->Integral() + cos_theta_psi_Kstar_BS_to_JPSI_K_PI->ProjectionX()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "X", cos_theta_psi_Kstar_data->ProjectionX()->Integral(), model_integral_X, cos_theta_psi_Kstar_BD_to_JPSI_K_PI->ProjectionX()->Integral(), cos_theta_psi_Kstar_BS_to_JPSI_K_K->ProjectionX()->Integral(), cos_theta_psi_Kstar_BD_to_JPSI_PI_PI->ProjectionX()->Integral(), cos_theta_psi_Kstar_BS_to_JPSI_PI_PI->ProjectionX()->Integral(), cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral(), cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral(), cos_theta_psi_Kstar_BD_to_JPSI_K_K->ProjectionX()->Integral(), cos_theta_psi_Kstar_BS_to_JPSI_K_PI->ProjectionX()->Integral(), cos_theta_psi_Kstar_bg->ProjectionX()->Integral());
    model_integral_Y = cos_theta_psi_Kstar_bg->ProjectionY()->Integral() + cos_theta_psi_Kstar_BD_to_JPSI_K_PI->ProjectionY()->Integral() + cos_theta_psi_Kstar_BS_to_JPSI_K_K->ProjectionY()->Integral() + cos_theta_psi_Kstar_BS_to_JPSI_PI_PI->ProjectionY()->Integral() + cos_theta_psi_Kstar_BD_to_JPSI_PI_PI->ProjectionY()->Integral() + cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral() + cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral() + cos_theta_psi_Kstar_BD_to_JPSI_K_K->ProjectionY()->Integral() + cos_theta_psi_Kstar_BS_to_JPSI_K_PI->ProjectionY()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "Y", cos_theta_psi_Kstar_data->ProjectionY()->Integral(), model_integral_Y, cos_theta_psi_Kstar_BD_to_JPSI_K_PI->ProjectionY()->Integral(), cos_theta_psi_Kstar_BS_to_JPSI_K_K->ProjectionY()->Integral(), cos_theta_psi_Kstar_BD_to_JPSI_PI_PI->ProjectionY()->Integral(), cos_theta_psi_Kstar_BS_to_JPSI_PI_PI->ProjectionY()->Integral(), cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral(), cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral(), cos_theta_psi_Kstar_BD_to_JPSI_K_K->ProjectionY()->Integral(), cos_theta_psi_Kstar_BS_to_JPSI_K_PI->ProjectionY()->Integral(), cos_theta_psi_Kstar_bg->ProjectionY()->Integral());
    
    cout << "\n\n_________________________________________________________" << endl;
    cout << "Control area of Lb decays:" << endl;
    cout << "_________________________________________________________\n" << endl;
    
    cout << "\nJpsip_2d_controlLb" << endl; 
    printf("%-10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s\n", "name", "data", "model", "BD_KPI", "BS_KK", "BD_PIPI", "BS_PIPI", "LB_PK", "LB_PPI", "BD_KK", "BS_KPI", "comb_bg");
    model_integral = Jpsip_2d_controlLb_bg->Integral() + Jpsip_2d_controlLb_BD_to_JPSI_K_PI->Integral() + Jpsip_2d_controlLb_BS_to_JPSI_K_K->Integral() + Jpsip_2d_controlLb_BS_to_JPSI_PI_PI->Integral() + Jpsip_2d_controlLb_BD_to_JPSI_PI_PI->Integral() + Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Integral() + Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->Integral() + Jpsip_2d_controlLb_BD_to_JPSI_K_K->Integral() + Jpsip_2d_controlLb_BS_to_JPSI_K_PI->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "2d", Jpsip_2d_controlLb_data->Integral(), model_integral, Jpsip_2d_controlLb_BD_to_JPSI_K_PI->Integral(), Jpsip_2d_controlLb_BS_to_JPSI_K_K->Integral(), Jpsip_2d_controlLb_BD_to_JPSI_PI_PI->Integral(), Jpsip_2d_controlLb_BS_to_JPSI_PI_PI->Integral(), Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Integral(), Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->Integral(), Jpsip_2d_controlLb_BD_to_JPSI_K_K->Integral(), Jpsip_2d_controlLb_BS_to_JPSI_K_PI->Integral(), Jpsip_2d_controlLb_bg->Integral());
    model_integral_X = Jpsip_2d_controlLb_bg->ProjectionX()->Integral() + Jpsip_2d_controlLb_BD_to_JPSI_K_PI->ProjectionX()->Integral() + Jpsip_2d_controlLb_BS_to_JPSI_K_K->ProjectionX()->Integral() + Jpsip_2d_controlLb_BS_to_JPSI_PI_PI->ProjectionX()->Integral() + Jpsip_2d_controlLb_BD_to_JPSI_PI_PI->ProjectionX()->Integral() + Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral() + Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral() + Jpsip_2d_controlLb_BD_to_JPSI_K_K->ProjectionX()->Integral() + Jpsip_2d_controlLb_BS_to_JPSI_K_PI->ProjectionX()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "X", Jpsip_2d_controlLb_data->ProjectionX()->Integral(), model_integral_X, Jpsip_2d_controlLb_BD_to_JPSI_K_PI->ProjectionX()->Integral(), Jpsip_2d_controlLb_BS_to_JPSI_K_K->ProjectionX()->Integral(), Jpsip_2d_controlLb_BD_to_JPSI_PI_PI->ProjectionX()->Integral(), Jpsip_2d_controlLb_BS_to_JPSI_PI_PI->ProjectionX()->Integral(), Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral(), Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral(), Jpsip_2d_controlLb_BD_to_JPSI_K_K->ProjectionX()->Integral(), Jpsip_2d_controlLb_BS_to_JPSI_K_PI->ProjectionX()->Integral(), Jpsip_2d_controlLb_bg->ProjectionX()->Integral());
    model_integral_Y = Jpsip_2d_controlLb_bg->ProjectionY()->Integral() + Jpsip_2d_controlLb_BD_to_JPSI_K_PI->ProjectionY()->Integral() + Jpsip_2d_controlLb_BS_to_JPSI_K_K->ProjectionY()->Integral() + Jpsip_2d_controlLb_BS_to_JPSI_PI_PI->ProjectionY()->Integral() + Jpsip_2d_controlLb_BD_to_JPSI_PI_PI->ProjectionY()->Integral() + Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral() + Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral() + Jpsip_2d_controlLb_BD_to_JPSI_K_K->ProjectionY()->Integral() + Jpsip_2d_controlLb_BS_to_JPSI_K_PI->ProjectionY()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "Y", Jpsip_2d_controlLb_data->ProjectionY()->Integral(), model_integral_Y, Jpsip_2d_controlLb_BD_to_JPSI_K_PI->ProjectionY()->Integral(), Jpsip_2d_controlLb_BS_to_JPSI_K_K->ProjectionY()->Integral(), Jpsip_2d_controlLb_BD_to_JPSI_PI_PI->ProjectionY()->Integral(), Jpsip_2d_controlLb_BS_to_JPSI_PI_PI->ProjectionY()->Integral(), Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral(), Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral(), Jpsip_2d_controlLb_BD_to_JPSI_K_K->ProjectionY()->Integral(), Jpsip_2d_controlLb_BS_to_JPSI_K_PI->ProjectionY()->Integral(), Jpsip_2d_controlLb_bg->ProjectionY()->Integral());
    
    cout << "\nJpsiK_2d_controlLb" << endl; 
    printf("%-10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s\n", "name", "data", "model", "BD_KPI", "BS_KK", "BD_PIPI", "BS_PIPI", "LB_PK", "LB_PPI", "BD_KK", "BS_KPI", "comb_bg");
    model_integral = JpsiK_2d_controlLb_bg->Integral() + JpsiK_2d_controlLb_BD_to_JPSI_K_PI->Integral() + JpsiK_2d_controlLb_BS_to_JPSI_K_K->Integral() + JpsiK_2d_controlLb_BS_to_JPSI_PI_PI->Integral() + JpsiK_2d_controlLb_BD_to_JPSI_PI_PI->Integral() + JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Integral() + JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->Integral() + JpsiK_2d_controlLb_BD_to_JPSI_K_K->Integral() + JpsiK_2d_controlLb_BS_to_JPSI_K_PI->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "2d", JpsiK_2d_controlLb_data->Integral(), model_integral, JpsiK_2d_controlLb_BD_to_JPSI_K_PI->Integral(), JpsiK_2d_controlLb_BS_to_JPSI_K_K->Integral(), JpsiK_2d_controlLb_BD_to_JPSI_PI_PI->Integral(), JpsiK_2d_controlLb_BS_to_JPSI_PI_PI->Integral(), JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Integral(), JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->Integral(), JpsiK_2d_controlLb_BD_to_JPSI_K_K->Integral(), JpsiK_2d_controlLb_BS_to_JPSI_K_PI->Integral(), JpsiK_2d_controlLb_bg->Integral());
    model_integral_X = JpsiK_2d_controlLb_bg->ProjectionX()->Integral() + JpsiK_2d_controlLb_BD_to_JPSI_K_PI->ProjectionX()->Integral() + JpsiK_2d_controlLb_BS_to_JPSI_K_K->ProjectionX()->Integral() + JpsiK_2d_controlLb_BS_to_JPSI_PI_PI->ProjectionX()->Integral() + JpsiK_2d_controlLb_BD_to_JPSI_PI_PI->ProjectionX()->Integral() + JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral() + JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral() + JpsiK_2d_controlLb_BD_to_JPSI_K_K->ProjectionX()->Integral() + JpsiK_2d_controlLb_BS_to_JPSI_K_PI->ProjectionX()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "X", JpsiK_2d_controlLb_data->ProjectionX()->Integral(), model_integral_X, JpsiK_2d_controlLb_BD_to_JPSI_K_PI->ProjectionX()->Integral(), JpsiK_2d_controlLb_BS_to_JPSI_K_K->ProjectionX()->Integral(), JpsiK_2d_controlLb_BD_to_JPSI_PI_PI->ProjectionX()->Integral(), JpsiK_2d_controlLb_BS_to_JPSI_PI_PI->ProjectionX()->Integral(), JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral(), JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral(), JpsiK_2d_controlLb_BD_to_JPSI_K_K->ProjectionX()->Integral(), JpsiK_2d_controlLb_BS_to_JPSI_K_PI->ProjectionX()->Integral(), JpsiK_2d_controlLb_bg->ProjectionX()->Integral());
    model_integral_Y = JpsiK_2d_controlLb_bg->ProjectionY()->Integral() + JpsiK_2d_controlLb_BD_to_JPSI_K_PI->ProjectionY()->Integral() + JpsiK_2d_controlLb_BS_to_JPSI_K_K->ProjectionY()->Integral() + JpsiK_2d_controlLb_BS_to_JPSI_PI_PI->ProjectionY()->Integral() + JpsiK_2d_controlLb_BD_to_JPSI_PI_PI->ProjectionY()->Integral() + JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral() + JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral() + JpsiK_2d_controlLb_BD_to_JPSI_K_K->ProjectionY()->Integral() + JpsiK_2d_controlLb_BS_to_JPSI_K_PI->ProjectionY()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "Y", JpsiK_2d_controlLb_data->ProjectionY()->Integral(), model_integral_Y, JpsiK_2d_controlLb_BD_to_JPSI_K_PI->ProjectionY()->Integral(), JpsiK_2d_controlLb_BS_to_JPSI_K_K->ProjectionY()->Integral(), JpsiK_2d_controlLb_BD_to_JPSI_PI_PI->ProjectionY()->Integral(), JpsiK_2d_controlLb_BS_to_JPSI_PI_PI->ProjectionY()->Integral(), JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral(), JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral(), JpsiK_2d_controlLb_BD_to_JPSI_K_K->ProjectionY()->Integral(), JpsiK_2d_controlLb_BS_to_JPSI_K_PI->ProjectionY()->Integral(), JpsiK_2d_controlLb_bg->ProjectionY()->Integral());
    
    cout << "\npK_2d_controlLb" << endl; 
    printf("%-10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s\n", "name", "data", "model", "BD_KPI", "BS_KK", "BD_PIPI", "BS_PIPI", "LB_PK", "LB_PPI", "BD_KK", "BS_KPI", "comb_bg");
    model_integral = pK_2d_controlLb_bg->Integral() + pK_2d_controlLb_BD_to_JPSI_K_PI->Integral() + pK_2d_controlLb_BS_to_JPSI_K_K->Integral() + pK_2d_controlLb_BS_to_JPSI_PI_PI->Integral() + pK_2d_controlLb_BD_to_JPSI_PI_PI->Integral() + pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Integral() + pK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->Integral() + pK_2d_controlLb_BD_to_JPSI_K_K->Integral() + pK_2d_controlLb_BS_to_JPSI_K_PI->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "2d", pK_2d_controlLb_data->Integral(), model_integral, pK_2d_controlLb_BD_to_JPSI_K_PI->Integral(), pK_2d_controlLb_BS_to_JPSI_K_K->Integral(), pK_2d_controlLb_BD_to_JPSI_PI_PI->Integral(), pK_2d_controlLb_BS_to_JPSI_PI_PI->Integral(), pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->Integral(), pK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->Integral(), pK_2d_controlLb_BD_to_JPSI_K_K->Integral(), pK_2d_controlLb_BS_to_JPSI_K_PI->Integral(), pK_2d_controlLb_bg->Integral());
    model_integral_X = pK_2d_controlLb_bg->ProjectionX()->Integral() + pK_2d_controlLb_BD_to_JPSI_K_PI->ProjectionX()->Integral() + pK_2d_controlLb_BS_to_JPSI_K_K->ProjectionX()->Integral() + pK_2d_controlLb_BS_to_JPSI_PI_PI->ProjectionX()->Integral() + pK_2d_controlLb_BD_to_JPSI_PI_PI->ProjectionX()->Integral() + pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral() + pK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral() + pK_2d_controlLb_BD_to_JPSI_K_K->ProjectionX()->Integral() + pK_2d_controlLb_BS_to_JPSI_K_PI->ProjectionX()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "X", pK_2d_controlLb_data->ProjectionX()->Integral(), model_integral_X, pK_2d_controlLb_BD_to_JPSI_K_PI->ProjectionX()->Integral(), pK_2d_controlLb_BS_to_JPSI_K_K->ProjectionX()->Integral(), pK_2d_controlLb_BD_to_JPSI_PI_PI->ProjectionX()->Integral(), pK_2d_controlLb_BS_to_JPSI_PI_PI->ProjectionX()->Integral(), pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral(), pK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral(), pK_2d_controlLb_BD_to_JPSI_K_K->ProjectionX()->Integral(), pK_2d_controlLb_BS_to_JPSI_K_PI->ProjectionX()->Integral(), pK_2d_controlLb_bg->ProjectionX()->Integral());
    model_integral_Y = pK_2d_controlLb_bg->ProjectionY()->Integral() + pK_2d_controlLb_BD_to_JPSI_K_PI->ProjectionY()->Integral() + pK_2d_controlLb_BS_to_JPSI_K_K->ProjectionY()->Integral() + pK_2d_controlLb_BS_to_JPSI_PI_PI->ProjectionY()->Integral() + pK_2d_controlLb_BD_to_JPSI_PI_PI->ProjectionY()->Integral() + pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral() + pK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral() + pK_2d_controlLb_BD_to_JPSI_K_K->ProjectionY()->Integral() + pK_2d_controlLb_BS_to_JPSI_K_PI->ProjectionY()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "Y", pK_2d_controlLb_data->ProjectionY()->Integral(), model_integral_Y, pK_2d_controlLb_BD_to_JPSI_K_PI->ProjectionY()->Integral(), pK_2d_controlLb_BS_to_JPSI_K_K->ProjectionY()->Integral(), pK_2d_controlLb_BD_to_JPSI_PI_PI->ProjectionY()->Integral(), pK_2d_controlLb_BS_to_JPSI_PI_PI->ProjectionY()->Integral(), pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral(), pK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral(), pK_2d_controlLb_BD_to_JPSI_K_K->ProjectionY()->Integral(), pK_2d_controlLb_BS_to_JPSI_K_PI->ProjectionY()->Integral(), pK_2d_controlLb_bg->ProjectionY()->Integral());
    
	cout << "\n\n_________________________________________________________" << endl;
    cout << "Control area of Bs decays:" << endl;
    cout << "_________________________________________________________\n" << endl;
    
    cout << "\nJpsiK_2d_control" << endl; 
    printf("%-10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s\n", "name", "data", "model", "BD_KPI", "BS_KK", "BD_PIPI", "BS_PIPI", "LB_PK", "LB_PPI", "BD_KK", "BS_KPI", "comb_bg");
    model_integral = JpsiK_2d_control_bg->Integral() + JpsiK_2d_control_BD_to_JPSI_K_PI->Integral() + JpsiK_2d_control_BS_to_JPSI_K_K->Integral() + JpsiK_2d_control_BS_to_JPSI_PI_PI->Integral() + JpsiK_2d_control_BD_to_JPSI_PI_PI->Integral() + JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K->Integral() + JpsiK_2d_control_LAMBDA0B_to_JPSI_P_PI->Integral() + JpsiK_2d_control_BD_to_JPSI_K_K->Integral() + JpsiK_2d_control_BS_to_JPSI_K_PI->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "2d", JpsiK_2d_control_data->Integral(), model_integral, JpsiK_2d_control_BD_to_JPSI_K_PI->Integral(), JpsiK_2d_control_BS_to_JPSI_K_K->Integral(), JpsiK_2d_control_BD_to_JPSI_PI_PI->Integral(), JpsiK_2d_control_BS_to_JPSI_PI_PI->Integral(), JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K->Integral(), JpsiK_2d_control_LAMBDA0B_to_JPSI_P_PI->Integral(), JpsiK_2d_control_BD_to_JPSI_K_K->Integral(), JpsiK_2d_control_BS_to_JPSI_K_PI->Integral(), JpsiK_2d_control_bg->Integral());
    model_integral_X = JpsiK_2d_control_bg->ProjectionX()->Integral() + JpsiK_2d_control_BD_to_JPSI_K_PI->ProjectionX()->Integral() + JpsiK_2d_control_BS_to_JPSI_K_K->ProjectionX()->Integral() + JpsiK_2d_control_BS_to_JPSI_PI_PI->ProjectionX()->Integral() + JpsiK_2d_control_BD_to_JPSI_PI_PI->ProjectionX()->Integral() + JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral() + JpsiK_2d_control_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral() + JpsiK_2d_control_BD_to_JPSI_K_K->ProjectionX()->Integral() + JpsiK_2d_control_BS_to_JPSI_K_PI->ProjectionX()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "X", JpsiK_2d_control_data->ProjectionX()->Integral(), model_integral_X, JpsiK_2d_control_BD_to_JPSI_K_PI->ProjectionX()->Integral(), JpsiK_2d_control_BS_to_JPSI_K_K->ProjectionX()->Integral(), JpsiK_2d_control_BD_to_JPSI_PI_PI->ProjectionX()->Integral(), JpsiK_2d_control_BS_to_JPSI_PI_PI->ProjectionX()->Integral(), JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral(), JpsiK_2d_control_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral(), JpsiK_2d_control_BD_to_JPSI_K_K->ProjectionX()->Integral(), JpsiK_2d_control_BS_to_JPSI_K_PI->ProjectionX()->Integral(), JpsiK_2d_control_bg->ProjectionX()->Integral());
    model_integral_Y = JpsiK_2d_control_bg->ProjectionY()->Integral() + JpsiK_2d_control_BD_to_JPSI_K_PI->ProjectionY()->Integral() + JpsiK_2d_control_BS_to_JPSI_K_K->ProjectionY()->Integral() + JpsiK_2d_control_BS_to_JPSI_PI_PI->ProjectionY()->Integral() + JpsiK_2d_control_BD_to_JPSI_PI_PI->ProjectionY()->Integral() + JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral() + JpsiK_2d_control_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral() + JpsiK_2d_control_BD_to_JPSI_K_K->ProjectionY()->Integral() + JpsiK_2d_control_BS_to_JPSI_K_PI->ProjectionY()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "Y", JpsiK_2d_control_data->ProjectionY()->Integral(), model_integral_Y, JpsiK_2d_control_BD_to_JPSI_K_PI->ProjectionY()->Integral(), JpsiK_2d_control_BS_to_JPSI_K_K->ProjectionY()->Integral(), JpsiK_2d_control_BD_to_JPSI_PI_PI->ProjectionY()->Integral(), JpsiK_2d_control_BS_to_JPSI_PI_PI->ProjectionY()->Integral(), JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral(), JpsiK_2d_control_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral(), JpsiK_2d_control_BD_to_JPSI_K_K->ProjectionY()->Integral(), JpsiK_2d_control_BS_to_JPSI_K_PI->ProjectionY()->Integral(), JpsiK_2d_control_bg->ProjectionY()->Integral());
    
    cout << "\nKK_1d_control" << endl; 
    printf("%-10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s\n", "name", "data", "model", "BD_KPI", "BS_KK", "BD_PIPI", "BS_PIPI", "LB_PK", "LB_PPI", "BD_KK", "BS_KPI", "comb_bg");
    model_integral = KK_1d_control_bg->Integral() + KK_1d_control_BD_to_JPSI_K_PI->Integral() + KK_1d_control_BS_to_JPSI_K_K->Integral() + KK_1d_control_BS_to_JPSI_PI_PI->Integral() + KK_1d_control_BD_to_JPSI_PI_PI->Integral() + KK_1d_control_LAMBDA0B_to_JPSI_P_K->Integral() + KK_1d_control_LAMBDA0B_to_JPSI_P_PI->Integral() + KK_1d_control_BD_to_JPSI_K_K->Integral() + KK_1d_control_BS_to_JPSI_K_PI->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "2d", KK_1d_control_data->Integral(), model_integral, KK_1d_control_BD_to_JPSI_K_PI->Integral(), KK_1d_control_BS_to_JPSI_K_K->Integral(), KK_1d_control_BD_to_JPSI_PI_PI->Integral(), KK_1d_control_BS_to_JPSI_PI_PI->Integral(), KK_1d_control_LAMBDA0B_to_JPSI_P_K->Integral(), KK_1d_control_LAMBDA0B_to_JPSI_P_PI->Integral(), KK_1d_control_BD_to_JPSI_K_K->Integral(), KK_1d_control_BS_to_JPSI_K_PI->Integral(), KK_1d_control_bg->Integral());
    
    cout << "\n\n_________________________________________________________" << endl;
    cout << "4 track mass distributions:" << endl;
    cout << "_________________________________________________________\n" << endl;
	
	cout << "\nKpi_piK_2d" << endl; 
    printf("%-10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s\n", "name", "data", "model", "BD_KPI", "BS_KK", "BD_PIPI", "BS_PIPI", "LB_PK", "LB_PPI", "BD_KK", "BS_KPI", "comb_bg");
    model_integral = Kpi_piK_2d_bg->Integral() + Kpi_piK_2d_BD_to_JPSI_K_PI->Integral() + Kpi_piK_2d_BS_to_JPSI_K_K->Integral() + Kpi_piK_2d_BS_to_JPSI_PI_PI->Integral() + Kpi_piK_2d_BD_to_JPSI_PI_PI->Integral() + Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Integral() + Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->Integral() + Kpi_piK_2d_BD_to_JPSI_K_K->Integral() + Kpi_piK_2d_BS_to_JPSI_K_PI->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "2d", Kpi_piK_2d_data->Integral(), model_integral, Kpi_piK_2d_BD_to_JPSI_K_PI->Integral(), Kpi_piK_2d_BS_to_JPSI_K_K->Integral(), Kpi_piK_2d_BD_to_JPSI_PI_PI->Integral(), Kpi_piK_2d_BS_to_JPSI_PI_PI->Integral(), Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Integral(), Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->Integral(), Kpi_piK_2d_BD_to_JPSI_K_K->Integral(), Kpi_piK_2d_BS_to_JPSI_K_PI->Integral(), Kpi_piK_2d_bg->Integral());
    model_integral_X = Kpi_piK_2d_bg->ProjectionX()->Integral() + Kpi_piK_2d_BD_to_JPSI_K_PI->ProjectionX()->Integral() + Kpi_piK_2d_BS_to_JPSI_K_K->ProjectionX()->Integral() + Kpi_piK_2d_BS_to_JPSI_PI_PI->ProjectionX()->Integral() + Kpi_piK_2d_BD_to_JPSI_PI_PI->ProjectionX()->Integral() + Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral() + Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral() + Kpi_piK_2d_BD_to_JPSI_K_K->ProjectionX()->Integral() + Kpi_piK_2d_BS_to_JPSI_K_PI->ProjectionX()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "X", Kpi_piK_2d_data->ProjectionX()->Integral(), model_integral_X, Kpi_piK_2d_BD_to_JPSI_K_PI->ProjectionX()->Integral(), Kpi_piK_2d_BS_to_JPSI_K_K->ProjectionX()->Integral(), Kpi_piK_2d_BD_to_JPSI_PI_PI->ProjectionX()->Integral(), Kpi_piK_2d_BS_to_JPSI_PI_PI->ProjectionX()->Integral(), Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral(), Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral(), Kpi_piK_2d_BD_to_JPSI_K_K->ProjectionX()->Integral(), Kpi_piK_2d_BS_to_JPSI_K_PI->ProjectionX()->Integral(), Kpi_piK_2d_bg->ProjectionX()->Integral());
    model_integral_Y = Kpi_piK_2d_bg->ProjectionY()->Integral() + Kpi_piK_2d_BD_to_JPSI_K_PI->ProjectionY()->Integral() + Kpi_piK_2d_BS_to_JPSI_K_K->ProjectionY()->Integral() + Kpi_piK_2d_BS_to_JPSI_PI_PI->ProjectionY()->Integral() + Kpi_piK_2d_BD_to_JPSI_PI_PI->ProjectionY()->Integral() + Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral() + Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral() + Kpi_piK_2d_BD_to_JPSI_K_K->ProjectionY()->Integral() + Kpi_piK_2d_BS_to_JPSI_K_PI->ProjectionY()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "Y", Kpi_piK_2d_data->ProjectionY()->Integral(), model_integral_Y, Kpi_piK_2d_BD_to_JPSI_K_PI->ProjectionY()->Integral(), Kpi_piK_2d_BS_to_JPSI_K_K->ProjectionY()->Integral(), Kpi_piK_2d_BD_to_JPSI_PI_PI->ProjectionY()->Integral(), Kpi_piK_2d_BS_to_JPSI_PI_PI->ProjectionY()->Integral(), Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral(), Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral(), Kpi_piK_2d_BD_to_JPSI_K_K->ProjectionY()->Integral(), Kpi_piK_2d_BS_to_JPSI_K_PI->ProjectionY()->Integral(), Kpi_piK_2d_bg->ProjectionY()->Integral());
    
	cout << "\npK_Kp_2d" << endl; 
    printf("%-10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s\n", "name", "data", "model", "BD_KPI", "BS_KK", "BD_PIPI", "BS_PIPI", "LB_PK", "LB_PPI", "BD_KK", "BS_KPI", "comb_bg");
    model_integral = pK_Kp_2d_bg->Integral() + pK_Kp_2d_BD_to_JPSI_K_PI->Integral() + pK_Kp_2d_BS_to_JPSI_K_K->Integral() + pK_Kp_2d_BS_to_JPSI_PI_PI->Integral() + pK_Kp_2d_BD_to_JPSI_PI_PI->Integral() + pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->Integral() + pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI->Integral() + pK_Kp_2d_BD_to_JPSI_K_K->Integral() + pK_Kp_2d_BS_to_JPSI_K_PI->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "2d", pK_Kp_2d_data->Integral(), model_integral, pK_Kp_2d_BD_to_JPSI_K_PI->Integral(), pK_Kp_2d_BS_to_JPSI_K_K->Integral(), pK_Kp_2d_BD_to_JPSI_PI_PI->Integral(), pK_Kp_2d_BS_to_JPSI_PI_PI->Integral(), pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->Integral(), pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI->Integral(), pK_Kp_2d_BD_to_JPSI_K_K->Integral(), pK_Kp_2d_BS_to_JPSI_K_PI->Integral(), pK_Kp_2d_bg->Integral());
    model_integral_X = pK_Kp_2d_bg->ProjectionX()->Integral() + pK_Kp_2d_BD_to_JPSI_K_PI->ProjectionX()->Integral() + pK_Kp_2d_BS_to_JPSI_K_K->ProjectionX()->Integral() + pK_Kp_2d_BS_to_JPSI_PI_PI->ProjectionX()->Integral() + pK_Kp_2d_BD_to_JPSI_PI_PI->ProjectionX()->Integral() + pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral() + pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral() + pK_Kp_2d_BD_to_JPSI_K_K->ProjectionX()->Integral() + pK_Kp_2d_BS_to_JPSI_K_PI->ProjectionX()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "X", pK_Kp_2d_data->ProjectionX()->Integral(), model_integral_X, pK_Kp_2d_BD_to_JPSI_K_PI->ProjectionX()->Integral(), pK_Kp_2d_BS_to_JPSI_K_K->ProjectionX()->Integral(), pK_Kp_2d_BD_to_JPSI_PI_PI->ProjectionX()->Integral(), pK_Kp_2d_BS_to_JPSI_PI_PI->ProjectionX()->Integral(), pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral(), pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral(), pK_Kp_2d_BD_to_JPSI_K_K->ProjectionX()->Integral(), pK_Kp_2d_BS_to_JPSI_K_PI->ProjectionX()->Integral(), pK_Kp_2d_bg->ProjectionX()->Integral());
    model_integral_Y = pK_Kp_2d_bg->ProjectionY()->Integral() + pK_Kp_2d_BD_to_JPSI_K_PI->ProjectionY()->Integral() + pK_Kp_2d_BS_to_JPSI_K_K->ProjectionY()->Integral() + pK_Kp_2d_BS_to_JPSI_PI_PI->ProjectionY()->Integral() + pK_Kp_2d_BD_to_JPSI_PI_PI->ProjectionY()->Integral() + pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral() + pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral() + pK_Kp_2d_BD_to_JPSI_K_K->ProjectionY()->Integral() + pK_Kp_2d_BS_to_JPSI_K_PI->ProjectionY()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "Y", pK_Kp_2d_data->ProjectionY()->Integral(), model_integral_Y, pK_Kp_2d_BD_to_JPSI_K_PI->ProjectionY()->Integral(), pK_Kp_2d_BS_to_JPSI_K_K->ProjectionY()->Integral(), pK_Kp_2d_BD_to_JPSI_PI_PI->ProjectionY()->Integral(), pK_Kp_2d_BS_to_JPSI_PI_PI->ProjectionY()->Integral(), pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral(), pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral(), pK_Kp_2d_BD_to_JPSI_K_K->ProjectionY()->Integral(), pK_Kp_2d_BS_to_JPSI_K_PI->ProjectionY()->Integral(), pK_Kp_2d_bg->ProjectionY()->Integral());
    
	cout << "\npK_Kp_2d_rotate" << endl; 
    printf("%-10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s\n", "name", "data", "model", "BD_KPI", "BS_KK", "BD_PIPI", "BS_PIPI", "LB_PK", "LB_PPI", "BD_KK", "BS_KPI", "comb_bg");
    model_integral = pK_Kp_2d_rotate_bg->Integral() + pK_Kp_2d_rotate_BD_to_JPSI_K_PI->Integral() + pK_Kp_2d_rotate_BS_to_JPSI_K_K->Integral() + pK_Kp_2d_rotate_BS_to_JPSI_PI_PI->Integral() + pK_Kp_2d_rotate_BD_to_JPSI_PI_PI->Integral() + pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_K->Integral() + pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_PI->Integral() + pK_Kp_2d_rotate_BD_to_JPSI_K_K->Integral() + pK_Kp_2d_rotate_BS_to_JPSI_K_PI->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "2d", pK_Kp_2d_rotate_data->Integral(), model_integral, pK_Kp_2d_rotate_BD_to_JPSI_K_PI->Integral(), pK_Kp_2d_rotate_BS_to_JPSI_K_K->Integral(), pK_Kp_2d_rotate_BD_to_JPSI_PI_PI->Integral(), pK_Kp_2d_rotate_BS_to_JPSI_PI_PI->Integral(), pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_K->Integral(), pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_PI->Integral(), pK_Kp_2d_rotate_BD_to_JPSI_K_K->Integral(), pK_Kp_2d_rotate_BS_to_JPSI_K_PI->Integral(), pK_Kp_2d_rotate_bg->Integral());
    model_integral_X = pK_Kp_2d_rotate_bg->ProjectionX()->Integral() + pK_Kp_2d_rotate_BD_to_JPSI_K_PI->ProjectionX()->Integral() + pK_Kp_2d_rotate_BS_to_JPSI_K_K->ProjectionX()->Integral() + pK_Kp_2d_rotate_BS_to_JPSI_PI_PI->ProjectionX()->Integral() + pK_Kp_2d_rotate_BD_to_JPSI_PI_PI->ProjectionX()->Integral() + pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral() + pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral() + pK_Kp_2d_rotate_BD_to_JPSI_K_K->ProjectionX()->Integral() + pK_Kp_2d_rotate_BS_to_JPSI_K_PI->ProjectionX()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "X", pK_Kp_2d_rotate_data->ProjectionX()->Integral(), model_integral_X, pK_Kp_2d_rotate_BD_to_JPSI_K_PI->ProjectionX()->Integral(), pK_Kp_2d_rotate_BS_to_JPSI_K_K->ProjectionX()->Integral(), pK_Kp_2d_rotate_BD_to_JPSI_PI_PI->ProjectionX()->Integral(), pK_Kp_2d_rotate_BS_to_JPSI_PI_PI->ProjectionX()->Integral(), pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral(), pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral(), pK_Kp_2d_rotate_BD_to_JPSI_K_K->ProjectionX()->Integral(), pK_Kp_2d_rotate_BS_to_JPSI_K_PI->ProjectionX()->Integral(), pK_Kp_2d_rotate_bg->ProjectionX()->Integral());
    model_integral_Y = pK_Kp_2d_rotate_bg->ProjectionY()->Integral() + pK_Kp_2d_rotate_BD_to_JPSI_K_PI->ProjectionY()->Integral() + pK_Kp_2d_rotate_BS_to_JPSI_K_K->ProjectionY()->Integral() + pK_Kp_2d_rotate_BS_to_JPSI_PI_PI->ProjectionY()->Integral() + pK_Kp_2d_rotate_BD_to_JPSI_PI_PI->ProjectionY()->Integral() + pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral() + pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral() + pK_Kp_2d_rotate_BD_to_JPSI_K_K->ProjectionY()->Integral() + pK_Kp_2d_rotate_BS_to_JPSI_K_PI->ProjectionY()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "Y", pK_Kp_2d_rotate_data->ProjectionY()->Integral(), model_integral_Y, pK_Kp_2d_rotate_BD_to_JPSI_K_PI->ProjectionY()->Integral(), pK_Kp_2d_rotate_BS_to_JPSI_K_K->ProjectionY()->Integral(), pK_Kp_2d_rotate_BD_to_JPSI_PI_PI->ProjectionY()->Integral(), pK_Kp_2d_rotate_BS_to_JPSI_PI_PI->ProjectionY()->Integral(), pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral(), pK_Kp_2d_rotate_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral(), pK_Kp_2d_rotate_BD_to_JPSI_K_K->ProjectionY()->Integral(), pK_Kp_2d_rotate_BS_to_JPSI_K_PI->ProjectionY()->Integral(), pK_Kp_2d_rotate_bg->ProjectionY()->Integral());
    
    cout << "\nppi_pip_2d" << endl; 
    printf("%-10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s\n", "name", "data", "model", "BD_KPI", "BS_KK", "BD_PIPI", "BS_PIPI", "LB_PK", "LB_PPI", "BD_KK", "BS_KPI", "comb_bg");
    model_integral = ppi_pip_2d_bg->Integral() + ppi_pip_2d_BD_to_JPSI_K_PI->Integral() + ppi_pip_2d_BS_to_JPSI_K_K->Integral() + ppi_pip_2d_BS_to_JPSI_PI_PI->Integral() + ppi_pip_2d_BD_to_JPSI_PI_PI->Integral() + ppi_pip_2d_LAMBDA0B_to_JPSI_P_K->Integral() + ppi_pip_2d_LAMBDA0B_to_JPSI_P_PI->Integral() + ppi_pip_2d_BD_to_JPSI_K_K->Integral() + ppi_pip_2d_BS_to_JPSI_K_PI->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "2d", ppi_pip_2d_data->Integral(), model_integral, ppi_pip_2d_BD_to_JPSI_K_PI->Integral(), ppi_pip_2d_BS_to_JPSI_K_K->Integral(), ppi_pip_2d_BD_to_JPSI_PI_PI->Integral(), ppi_pip_2d_BS_to_JPSI_PI_PI->Integral(), ppi_pip_2d_LAMBDA0B_to_JPSI_P_K->Integral(), ppi_pip_2d_LAMBDA0B_to_JPSI_P_PI->Integral(), ppi_pip_2d_BD_to_JPSI_K_K->Integral(), ppi_pip_2d_BS_to_JPSI_K_PI->Integral(), ppi_pip_2d_bg->Integral());
    model_integral_X = ppi_pip_2d_bg->ProjectionX()->Integral() + ppi_pip_2d_BD_to_JPSI_K_PI->ProjectionX()->Integral() + ppi_pip_2d_BS_to_JPSI_K_K->ProjectionX()->Integral() + ppi_pip_2d_BS_to_JPSI_PI_PI->ProjectionX()->Integral() + ppi_pip_2d_BD_to_JPSI_PI_PI->ProjectionX()->Integral() + ppi_pip_2d_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral() + ppi_pip_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral() + ppi_pip_2d_BD_to_JPSI_K_K->ProjectionX()->Integral() + ppi_pip_2d_BS_to_JPSI_K_PI->ProjectionX()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "X", ppi_pip_2d_data->ProjectionX()->Integral(), model_integral_X, ppi_pip_2d_BD_to_JPSI_K_PI->ProjectionX()->Integral(), ppi_pip_2d_BS_to_JPSI_K_K->ProjectionX()->Integral(), ppi_pip_2d_BD_to_JPSI_PI_PI->ProjectionX()->Integral(), ppi_pip_2d_BS_to_JPSI_PI_PI->ProjectionX()->Integral(), ppi_pip_2d_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral(), ppi_pip_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral(), ppi_pip_2d_BD_to_JPSI_K_K->ProjectionX()->Integral(), ppi_pip_2d_BS_to_JPSI_K_PI->ProjectionX()->Integral(), ppi_pip_2d_bg->ProjectionX()->Integral());
    model_integral_Y = ppi_pip_2d_bg->ProjectionY()->Integral() + ppi_pip_2d_BD_to_JPSI_K_PI->ProjectionY()->Integral() + ppi_pip_2d_BS_to_JPSI_K_K->ProjectionY()->Integral() + ppi_pip_2d_BS_to_JPSI_PI_PI->ProjectionY()->Integral() + ppi_pip_2d_BD_to_JPSI_PI_PI->ProjectionY()->Integral() + ppi_pip_2d_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral() + ppi_pip_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral() + ppi_pip_2d_BD_to_JPSI_K_K->ProjectionY()->Integral() + ppi_pip_2d_BS_to_JPSI_K_PI->ProjectionY()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "Y", ppi_pip_2d_data->ProjectionY()->Integral(), model_integral_Y, ppi_pip_2d_BD_to_JPSI_K_PI->ProjectionY()->Integral(), ppi_pip_2d_BS_to_JPSI_K_K->ProjectionY()->Integral(), ppi_pip_2d_BD_to_JPSI_PI_PI->ProjectionY()->Integral(), ppi_pip_2d_BS_to_JPSI_PI_PI->ProjectionY()->Integral(), ppi_pip_2d_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral(), ppi_pip_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral(), ppi_pip_2d_BD_to_JPSI_K_K->ProjectionY()->Integral(), ppi_pip_2d_BS_to_JPSI_K_PI->ProjectionY()->Integral(), ppi_pip_2d_bg->ProjectionY()->Integral());
    
	cout << "\nKK_1d" << endl; 
    printf("%-10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s\n", "name", "data", "model", "BD_KPI", "BS_KK", "BD_PIPI", "BS_PIPI", "LB_PK", "LB_PPI", "BD_KK", "BS_KPI", "comb_bg");
    model_integral = KK_1d_bg->Integral() + KK_1d_BD_to_JPSI_K_PI->Integral() + KK_1d_BS_to_JPSI_K_K->Integral() + KK_1d_BS_to_JPSI_PI_PI->Integral() + KK_1d_BD_to_JPSI_PI_PI->Integral() + KK_1d_LAMBDA0B_to_JPSI_P_K->Integral() + KK_1d_LAMBDA0B_to_JPSI_P_PI->Integral() + KK_1d_BD_to_JPSI_K_K->Integral() + KK_1d_BS_to_JPSI_K_PI->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "2d", KK_1d_data->Integral(), model_integral, KK_1d_BD_to_JPSI_K_PI->Integral(), KK_1d_BS_to_JPSI_K_K->Integral(), KK_1d_BD_to_JPSI_PI_PI->Integral(), KK_1d_BS_to_JPSI_PI_PI->Integral(), KK_1d_LAMBDA0B_to_JPSI_P_K->Integral(), KK_1d_LAMBDA0B_to_JPSI_P_PI->Integral(), KK_1d_BD_to_JPSI_K_K->Integral(), KK_1d_BS_to_JPSI_K_PI->Integral(), KK_1d_bg->Integral());
    
	cout << "\npipi_1d" << endl; 
    printf("%-10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s\n", "name", "data", "model", "BD_KPI", "BS_KK", "BD_PIPI", "BS_PIPI", "LB_PK", "LB_PPI", "BD_KK", "BS_KPI", "comb_bg");
    model_integral = pipi_1d_bg->Integral() + pipi_1d_BD_to_JPSI_K_PI->Integral() + pipi_1d_BS_to_JPSI_K_K->Integral() + pipi_1d_BS_to_JPSI_PI_PI->Integral() + pipi_1d_BD_to_JPSI_PI_PI->Integral() + pipi_1d_LAMBDA0B_to_JPSI_P_K->Integral() + pipi_1d_LAMBDA0B_to_JPSI_P_PI->Integral() + pipi_1d_BD_to_JPSI_K_K->Integral() + pipi_1d_BS_to_JPSI_K_PI->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "2d", pipi_1d_data->Integral(), model_integral, pipi_1d_BD_to_JPSI_K_PI->Integral(), pipi_1d_BS_to_JPSI_K_K->Integral(), pipi_1d_BD_to_JPSI_PI_PI->Integral(), pipi_1d_BS_to_JPSI_PI_PI->Integral(), pipi_1d_LAMBDA0B_to_JPSI_P_K->Integral(), pipi_1d_LAMBDA0B_to_JPSI_P_PI->Integral(), pipi_1d_BD_to_JPSI_K_K->Integral(), pipi_1d_BS_to_JPSI_K_PI->Integral(), pipi_1d_bg->Integral());
    
	
    printf("\n%-20s%10.2f\n\n", "Minimized function: ", amin);
    
    printf("%-20s\n\n", "signal area likelyhood:");
    
	Double_t Jpsipi_2d_likelyhood = 0.;
    
    for(int i = 1; i <= Jpsipi_2d_data->ProjectionX()->GetNbinsX(); i++)
    {
        for(int k = 1; k <= Jpsipi_2d_data->ProjectionY()->GetNbinsX(); k++)
        {
            //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
            //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            fb += Jpsipi_2d_bg->GetBinContent(i, k);
            fb += Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
            fb += Jpsipi_2d_BD_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += Jpsipi_2d_BS_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += Jpsipi_2d_BS_to_JPSI_K_K->GetBinContent(i, k);
            fb += Jpsipi_2d_BD_to_JPSI_K_PI->GetBinContent(i, k);
            if (RARE_DECAYS)
            {
                fb += Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                fb += Jpsipi_2d_BD_to_JPSI_K_K->GetBinContent(i, k);
                fb += Jpsipi_2d_BS_to_JPSI_K_PI->GetBinContent(i, k);
            }
            //cout << bglevel0_psipi << endl;
            nb = Jpsipi_2d_data->GetBinContent(i, k);
            if( fb > 0.0 ) Jpsipi_2d_likelyhood += -nb*log(fb) + fb;
            //if ((-nb*log(fb) + fb) > 0.) {cout << i << "\t" << k << endl;}
        }
    }
    
    printf("%-20s%10.2f\n", "Likelyhood Jpsipi_2d: ", Jpsipi_2d_likelyhood);
    
    Double_t JpsiK_2d_likelyhood = 0.;
    
    for(int i = 1; i <= JpsiK_2d_data->ProjectionX()->GetNbinsX(); i++)
    {
        for(int k = 1; k <= JpsiK_2d_data->ProjectionY()->GetNbinsX(); k++)
        {
            //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
            //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            fb += JpsiK_2d_bg->GetBinContent(i, k);
            fb += JpsiK_2d_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
            fb += JpsiK_2d_BD_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += JpsiK_2d_BS_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += JpsiK_2d_BS_to_JPSI_K_K->GetBinContent(i, k);
            fb += JpsiK_2d_BD_to_JPSI_K_PI->GetBinContent(i, k);
            if (RARE_DECAYS)
            {
                fb += JpsiK_2d_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                fb += JpsiK_2d_BD_to_JPSI_K_K->GetBinContent(i, k);
                fb += JpsiK_2d_BS_to_JPSI_K_PI->GetBinContent(i, k);
            }
            //cout << bglevel0_psipi << endl;
            nb = JpsiK_2d_data->GetBinContent(i, k);
            if( fb > 0.0 ) JpsiK_2d_likelyhood += -nb*log(fb) + fb;
            //if ((-nb*log(fb) + fb) > 0.) {cout << i << "\t" << k << endl;}
        }
    }
    
    printf("%-20s%10.2f\n", "Likelyhood JpsiK_2d: ", JpsiK_2d_likelyhood);
    
    Double_t Kpi_2d_likelyhood = 0.;
    
    for(int i = 1; i <= Kpi_2d_data->ProjectionX()->GetNbinsX(); i++)
    {
        for(int k = 1; k <= Kpi_2d_data->ProjectionY()->GetNbinsX(); k++)
        {
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            fb += Kpi_2d_bg->GetBinContent(i, k);
            fb += Kpi_2d_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
            fb += Kpi_2d_BD_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += Kpi_2d_BS_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += Kpi_2d_BS_to_JPSI_K_K->GetBinContent(i, k);
            fb += Kpi_2d_BD_to_JPSI_K_PI->GetBinContent(i, k);
            if (RARE_DECAYS)
            {
                fb += Kpi_2d_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                fb += Kpi_2d_BD_to_JPSI_K_K->GetBinContent(i, k);
                fb += Kpi_2d_BS_to_JPSI_K_PI->GetBinContent(i, k);
            }
            //cout << bglevel0_psipi << endl;
            nb = Kpi_2d_data->GetBinContent(i, k);
            if( fb > 0.0 ) Kpi_2d_likelyhood += -nb*log(fb) + fb;
            //if ((-nb*log(fb) + fb) > 0.) {cout << i << "\t" << k << endl;}
        }
    }
    
    printf("%-20s%10.2f\n", "Likelyhood Kpi_2d: ", Kpi_2d_likelyhood);
    
    Double_t cos_theta_Zc_likelyhood = 0.;
    
    for(int i = 1; i <= cos_theta_Zc_data->ProjectionX()->GetNbinsX(); i++)
    {
        for(int k = 1; k <= cos_theta_Zc_data->ProjectionY()->GetNbinsX(); k++)
        {
            //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
            //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            fb += cos_theta_Zc_bg->GetBinContent(i, k);
            fb += cos_theta_Zc_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
            fb += cos_theta_Zc_BD_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += cos_theta_Zc_BS_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += cos_theta_Zc_BS_to_JPSI_K_K->GetBinContent(i, k);
            fb += cos_theta_Zc_BD_to_JPSI_K_PI->GetBinContent(i, k);
            if (RARE_DECAYS)
            {
                fb += cos_theta_Zc_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                fb += cos_theta_Zc_BD_to_JPSI_K_K->GetBinContent(i, k);
                fb += cos_theta_Zc_BS_to_JPSI_K_PI->GetBinContent(i, k);
            }
            //cout << bglevel0_psipi << endl;
            nb = cos_theta_Zc_data->GetBinContent(i, k);
            if( fb > 0.0 ) cos_theta_Zc_likelyhood += -nb*log(fb) + fb;
            //if ((-nb*log(fb) + fb) > 0.) {cout << i << "\t" << k << endl;}
        }
    }
    
    printf("%-20s%10.2f\n", "Likelyhood cos_theta_Zc: ", cos_theta_Zc_likelyhood);
    
    Double_t cos_theta_B0_likelyhood = 0.;
    
    for(int i = 1; i <= cos_theta_B0_data->ProjectionX()->GetNbinsX(); i++)
    {
        for(int k = 1; k <= cos_theta_B0_data->ProjectionY()->GetNbinsX(); k++)
        {
            //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
            //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            fb += cos_theta_B0_bg->GetBinContent(i, k);
            fb += cos_theta_B0_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
            fb += cos_theta_B0_BD_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += cos_theta_B0_BS_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += cos_theta_B0_BS_to_JPSI_K_K->GetBinContent(i, k);
            fb += cos_theta_B0_BD_to_JPSI_K_PI->GetBinContent(i, k);
            if (RARE_DECAYS)
            {
                fb += cos_theta_B0_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                fb += cos_theta_B0_BD_to_JPSI_K_K->GetBinContent(i, k);
                fb += cos_theta_B0_BS_to_JPSI_K_PI->GetBinContent(i, k);
            }
            //cout << bglevel0_psipi << endl;
            nb = cos_theta_B0_data->GetBinContent(i, k);
            if( fb > 0.0 ) cos_theta_B0_likelyhood += -nb*log(fb) + fb;
            //if ((-nb*log(fb) + fb) > 0.) {cout << i << "\t" << k << endl;}
        }
    }
    
    printf("%-20s%10.2f\n", "Likelyhood cos_theta_B0: ", cos_theta_B0_likelyhood);
    
    Double_t phi_psi_likelyhood = 0.;
    
    for(int i = 1; i <= phi_psi_data->ProjectionX()->GetNbinsX(); i++)
    {
        for(int k = 1; k <= phi_psi_data->ProjectionY()->GetNbinsX(); k++)
        {
            //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
            //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            fb += phi_psi_bg->GetBinContent(i, k);
            fb += phi_psi_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
            fb += phi_psi_BD_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += phi_psi_BS_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += phi_psi_BS_to_JPSI_K_K->GetBinContent(i, k);
            fb += phi_psi_BD_to_JPSI_K_PI->GetBinContent(i, k);
            if (RARE_DECAYS)
            {
                fb += phi_psi_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                fb += phi_psi_BD_to_JPSI_K_K->GetBinContent(i, k);
                fb += phi_psi_BS_to_JPSI_K_PI->GetBinContent(i, k);
            }
            //cout << bglevel0_psipi << endl;
            nb = phi_psi_data->GetBinContent(i, k);
            if( fb > 0.0 ) phi_psi_likelyhood += -nb*log(fb) + fb;
            //if ((-nb*log(fb) + fb) > 0.) {cout << i << "\t" << k << endl;}
        }
    }
    
    printf("%-20s%10.2f\n", "Likelyhood phi_psi: ", phi_psi_likelyhood);
    
    Double_t phi_K_likelyhood = 0.;
    
    for(int i = 1; i <= phi_K_data->ProjectionX()->GetNbinsX(); i++)
    {
        for(int k = 1; k <= phi_K_data->ProjectionY()->GetNbinsX(); k++)
        {
            //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
            //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            fb += phi_K_bg->GetBinContent(i, k);
            fb += phi_K_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
            fb += phi_K_BD_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += phi_K_BS_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += phi_K_BS_to_JPSI_K_K->GetBinContent(i, k);
            fb += phi_K_BD_to_JPSI_K_PI->GetBinContent(i, k);
            if (RARE_DECAYS)
            {
                fb += phi_K_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                fb += phi_K_BD_to_JPSI_K_K->GetBinContent(i, k);
                fb += phi_K_BS_to_JPSI_K_PI->GetBinContent(i, k);
            }
            //cout << bglevel0_psipi << endl;
            nb = phi_K_data->GetBinContent(i, k);
            if( fb > 0.0 ) phi_K_likelyhood += -nb*log(fb) + fb;
            //if ((-nb*log(fb) + fb) > 0.) {cout << i << "\t" << k << endl;}
        }
    }
    
    printf("%-20s%10.2f\n", "Likelyhood phi_K: ", phi_K_likelyhood);
    
    Double_t alpha_mu_likelyhood = 0.;
    
    for(int i = 1; i <= alpha_mu_data->ProjectionX()->GetNbinsX(); i++)
    {
        for(int k = 1; k <= alpha_mu_data->ProjectionY()->GetNbinsX(); k++)
        {
            //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
            //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            fb += alpha_mu_bg->GetBinContent(i, k);
            fb += alpha_mu_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
            fb += alpha_mu_BD_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += alpha_mu_BS_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += alpha_mu_BS_to_JPSI_K_K->GetBinContent(i, k);
            fb += alpha_mu_BD_to_JPSI_K_PI->GetBinContent(i, k);
            if (RARE_DECAYS)
            {
                fb += alpha_mu_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                fb += alpha_mu_BD_to_JPSI_K_K->GetBinContent(i, k);
                fb += alpha_mu_BS_to_JPSI_K_PI->GetBinContent(i, k);
            }
            //cout << bglevel0_psipi << endl;
            nb = alpha_mu_data->GetBinContent(i, k);
            if( fb > 0.0 ) alpha_mu_likelyhood += -nb*log(fb) + fb;
            //if ((-nb*log(fb) + fb) > 0.) {cout << i << "\t" << k << endl;}
        }
    }
    
    printf("%-20s%10.2f\n", "Likelyhood alpha_mu: ", alpha_mu_likelyhood);
    
    Double_t phi_mu_Kstar_likelyhood = 0.;
    
    for(int i = 1; i <= phi_mu_Kstar_data->ProjectionX()->GetNbinsX(); i++)
    {
        for(int k = 1; k <= phi_mu_Kstar_data->ProjectionY()->GetNbinsX(); k++)
        {
            //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
            //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            fb += phi_mu_Kstar_bg->GetBinContent(i, k);
            fb += phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
            fb += phi_mu_Kstar_BD_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += phi_mu_Kstar_BS_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += phi_mu_Kstar_BS_to_JPSI_K_K->GetBinContent(i, k);
            fb += phi_mu_Kstar_BD_to_JPSI_K_PI->GetBinContent(i, k);
            if (RARE_DECAYS)
            {
                fb += phi_mu_Kstar_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                fb += phi_mu_Kstar_BD_to_JPSI_K_K->GetBinContent(i, k);
                fb += phi_mu_Kstar_BS_to_JPSI_K_PI->GetBinContent(i, k);
            }
            //cout << bglevel0_psipi << endl;
            nb = phi_mu_Kstar_data->GetBinContent(i, k);
            if( fb > 0.0 ) phi_mu_Kstar_likelyhood += -nb*log(fb) + fb;
            //if ((-nb*log(fb) + fb) > 0.) {cout << i << "\t" << k << endl;}
        }
    }
    
    printf("%-20s%10.2f\n", "Likelyhood phi_mu_Kstar: ", phi_mu_Kstar_likelyhood);
    
    Double_t phi_mu_X_likelyhood = 0.;
    
    for(int i = 1; i <= phi_mu_X_data->ProjectionX()->GetNbinsX(); i++)
    {
        for(int k = 1; k <= phi_mu_X_data->ProjectionY()->GetNbinsX(); k++)
        {
            //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
            //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            fb += phi_mu_X_bg->GetBinContent(i, k);
            fb += phi_mu_X_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
            fb += phi_mu_X_BD_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += phi_mu_X_BS_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += phi_mu_X_BS_to_JPSI_K_K->GetBinContent(i, k);
            fb += phi_mu_X_BD_to_JPSI_K_PI->GetBinContent(i, k);
            if (RARE_DECAYS)
            {
                fb += phi_mu_X_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                fb += phi_mu_X_BD_to_JPSI_K_K->GetBinContent(i, k);
                fb += phi_mu_X_BS_to_JPSI_K_PI->GetBinContent(i, k);
            }
            //cout << bglevel0_psipi << endl;
            nb = phi_mu_X_data->GetBinContent(i, k);
            if( fb > 0.0 ) phi_mu_X_likelyhood += -nb*log(fb) + fb;
            //if ((-nb*log(fb) + fb) > 0.) {cout << i << "\t" << k << endl;}
        }
    }
    
    printf("%-20s%10.2f\n", "Likelyhood phi_mu_X: ", phi_mu_X_likelyhood);
    
    Double_t cos_theta_psi_X_likelyhood = 0.;
    
    for(int i = 1; i <= cos_theta_psi_X_data->ProjectionX()->GetNbinsX(); i++)
    {
        for(int k = 1; k <= cos_theta_psi_X_data->ProjectionY()->GetNbinsX(); k++)
        {
            //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
            //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            fb += cos_theta_psi_X_bg->GetBinContent(i, k);
            fb += cos_theta_psi_X_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
            fb += cos_theta_psi_X_BD_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += cos_theta_psi_X_BS_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += cos_theta_psi_X_BS_to_JPSI_K_K->GetBinContent(i, k);
            fb += cos_theta_psi_X_BD_to_JPSI_K_PI->GetBinContent(i, k);
            if (RARE_DECAYS)
            {
                fb += cos_theta_psi_X_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                fb += cos_theta_psi_X_BD_to_JPSI_K_K->GetBinContent(i, k);
                fb += cos_theta_psi_X_BS_to_JPSI_K_PI->GetBinContent(i, k);
            }
            //cout << bglevel0_psipi << endl;
            nb = cos_theta_psi_X_data->GetBinContent(i, k);
            if( fb > 0.0 ) cos_theta_psi_X_likelyhood += -nb*log(fb) + fb;
            //if ((-nb*log(fb) + fb) > 0.) {cout << i << "\t" << k << endl;}
        }
    }
    
    printf("%-20s%10.2f\n", "Likelyhood cos_theta_psi_X: ", cos_theta_psi_X_likelyhood);
    
    Double_t cos_theta_Kstar_likelyhood = 0.;
    
    for(int i = 1; i <= cos_theta_Kstar_data->ProjectionX()->GetNbinsX(); i++)
    {
        for(int k = 1; k <= cos_theta_Kstar_data->ProjectionY()->GetNbinsX(); k++)
        {
            //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
            //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            fb += cos_theta_Kstar_bg->GetBinContent(i, k);
            fb += cos_theta_Kstar_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
            fb += cos_theta_Kstar_BD_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += cos_theta_Kstar_BS_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += cos_theta_Kstar_BS_to_JPSI_K_K->GetBinContent(i, k);
            fb += cos_theta_Kstar_BD_to_JPSI_K_PI->GetBinContent(i, k);
            if (RARE_DECAYS)
            {
                fb += cos_theta_Kstar_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                fb += cos_theta_Kstar_BD_to_JPSI_K_K->GetBinContent(i, k);
                fb += cos_theta_Kstar_BS_to_JPSI_K_PI->GetBinContent(i, k);
            }
            //cout << bglevel0_psipi << endl;
            nb = cos_theta_Kstar_data->GetBinContent(i, k);
            if( fb > 0.0 ) cos_theta_Kstar_likelyhood += -nb*log(fb) + fb;
            //if ((-nb*log(fb) + fb) > 0.) {cout << i << "\t" << k << endl;}
        }
    }
    
    printf("%-20s%10.2f\n", "Likelyhood cos_theta_Kstar: ", cos_theta_Kstar_likelyhood);
    
    Double_t cos_theta_psi_Kstar_likelyhood = 0.;
    
    for(int i = 1; i <= cos_theta_psi_Kstar_data->ProjectionX()->GetNbinsX(); i++)
    {
        for(int k = 1; k <= cos_theta_psi_Kstar_data->ProjectionY()->GetNbinsX(); k++)
        {
            //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
            //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            fb += cos_theta_psi_Kstar_bg->GetBinContent(i, k);
            fb += cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
            fb += cos_theta_psi_Kstar_BD_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += cos_theta_psi_Kstar_BS_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += cos_theta_psi_Kstar_BS_to_JPSI_K_K->GetBinContent(i, k);
            fb += cos_theta_psi_Kstar_BD_to_JPSI_K_PI->GetBinContent(i, k);
            if (RARE_DECAYS)
            {
                fb += cos_theta_psi_Kstar_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                fb += cos_theta_psi_Kstar_BD_to_JPSI_K_K->GetBinContent(i, k);
                fb += cos_theta_psi_Kstar_BS_to_JPSI_K_PI->GetBinContent(i, k);
            }
            //cout << bglevel0_psipi << endl;
            nb = cos_theta_psi_Kstar_data->GetBinContent(i, k);
            if( fb > 0.0 ) cos_theta_psi_Kstar_likelyhood += -nb*log(fb) + fb;
            //if ((-nb*log(fb) + fb) > 0.) {cout << i << "\t" << k << endl;}
        }
    }
    
    printf("%-20s%10.2f\n\n", "Likelyhood cos_theta_psi_Kstar: ", cos_theta_psi_Kstar_likelyhood);
	
    printf("%-20s\n\n", "control area BsKK likelyhood:");
    
    Double_t JpsiK_2d_control_likelyhood = 0.;
    
    for(int i = 1; i <= JpsiK_2d_control_data->ProjectionX()->GetNbinsX(); i++)
    {
        for(int k = 1; k <= JpsiK_2d_control_data->ProjectionY()->GetNbinsX(); k++)
        {
            //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
            //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            fb += JpsiK_2d_control_bg->GetBinContent(i, k);
            fb += JpsiK_2d_control_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
            fb += JpsiK_2d_control_BD_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += JpsiK_2d_control_BS_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += JpsiK_2d_control_BS_to_JPSI_K_K->GetBinContent(i, k);
            fb += JpsiK_2d_control_BD_to_JPSI_K_PI->GetBinContent(i, k);
            if (RARE_DECAYS)
            {
                fb += JpsiK_2d_control_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                fb += JpsiK_2d_control_BD_to_JPSI_K_K->GetBinContent(i, k);
                fb += JpsiK_2d_control_BS_to_JPSI_K_PI->GetBinContent(i, k);
            }
            //cout << bglevel0_psipi << endl;
            nb = JpsiK_2d_control_data->GetBinContent(i, k);
            if( fb > 0.0 ) JpsiK_2d_control_likelyhood += -nb*log(fb) + fb;
            //if ((-nb*log(fb) + fb) > 0.) {cout << i << "\t" << k << endl;}
        }
    }
    
    printf("%-20s%10.2f\n", "Likelyhood JpsiK_2d_control: ", JpsiK_2d_control_likelyhood);
    
    Double_t KK_1d_control_likelyhood = 0.;
    
    for(int i = 1; i <= KK_1d_control_data->GetNbinsX(); i++)
    {
        double nb = 0.0; double nberr = 0.0; double fb = 0.0;
        fb += KK_1d_control_bg->GetBinContent(i);
        fb += KK_1d_control_LAMBDA0B_to_JPSI_P_K->GetBinContent(i);
        fb += KK_1d_control_BD_to_JPSI_PI_PI->GetBinContent(i);
        fb += KK_1d_control_BS_to_JPSI_PI_PI->GetBinContent(i);
        fb += KK_1d_control_BS_to_JPSI_K_K->GetBinContent(i);
        fb += KK_1d_control_BD_to_JPSI_K_PI->GetBinContent(i);
        if (RARE_DECAYS)
        {
            fb += KK_1d_control_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i);
            fb += KK_1d_control_BD_to_JPSI_K_K->GetBinContent(i);
            fb += KK_1d_control_BS_to_JPSI_K_PI->GetBinContent(i);
        }
        nb = KK_1d_control_data->GetBinContent(i);
        if( fb > 0.0 ) KK_1d_control_likelyhood += -nb*log(fb) + fb;
    }
    
    printf("%-20s%10.2f\n", "Likelyhood KK_1d_control: ", KK_1d_control_likelyhood);
    
    printf("%-20s\n\n", "control area LbpK likelyhood:");
    
    Double_t Jpsip_2d_controlLb_likelyhood = 0.;
    
    for(int i = 1; i <= Jpsip_2d_controlLb_data->ProjectionX()->GetNbinsX(); i++)
    {
        for(int k = 1; k <= Jpsip_2d_controlLb_data->ProjectionY()->GetNbinsX(); k++)
        {
            //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
            //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            fb += Jpsip_2d_controlLb_bg->GetBinContent(i, k);
            fb += Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
            fb += Jpsip_2d_controlLb_BD_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += Jpsip_2d_controlLb_BS_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += Jpsip_2d_controlLb_BS_to_JPSI_K_K->GetBinContent(i, k);
            fb += Jpsip_2d_controlLb_BD_to_JPSI_K_PI->GetBinContent(i, k);
            if (RARE_DECAYS)
            {
                fb += Jpsip_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                fb += Jpsip_2d_controlLb_BD_to_JPSI_K_K->GetBinContent(i, k);
                fb += Jpsip_2d_controlLb_BS_to_JPSI_K_PI->GetBinContent(i, k);
            }
            //cout << bglevel0_psipi << endl;
            nb = Jpsip_2d_controlLb_data->GetBinContent(i, k);
            if( fb > 0.0 ) Jpsip_2d_controlLb_likelyhood += -nb*log(fb) + fb;
            //if ((-nb*log(fb) + fb) > 0.) {cout << i << "\t" << k << endl;}
        }
    }
    
    printf("%-20s%10.2f\n", "Likelyhood Jpsip_2d_controlLb: ", Jpsip_2d_controlLb_likelyhood);
    
    Double_t JpsiK_2d_controlLb_likelyhood = 0.;
    
    for(int i = 1; i <= JpsiK_2d_controlLb_data->ProjectionX()->GetNbinsX(); i++)
    {
        for(int k = 1; k <= JpsiK_2d_controlLb_data->ProjectionY()->GetNbinsX(); k++)
        {
            //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
            //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            fb += JpsiK_2d_controlLb_bg->GetBinContent(i, k);
            fb += JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
            fb += JpsiK_2d_controlLb_BD_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += JpsiK_2d_controlLb_BS_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += JpsiK_2d_controlLb_BS_to_JPSI_K_K->GetBinContent(i, k);
            fb += JpsiK_2d_controlLb_BD_to_JPSI_K_PI->GetBinContent(i, k);
            if (RARE_DECAYS)
            {
                fb += JpsiK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                fb += JpsiK_2d_controlLb_BD_to_JPSI_K_K->GetBinContent(i, k);
                fb += JpsiK_2d_controlLb_BS_to_JPSI_K_PI->GetBinContent(i, k);
            }
            //cout << bglevel0_psipi << endl;
            nb = JpsiK_2d_controlLb_data->GetBinContent(i, k);
            if( fb > 0.0 ) JpsiK_2d_controlLb_likelyhood += -nb*log(fb) + fb;
            //if ((-nb*log(fb) + fb) > 0.) {cout << i << "\t" << k << endl;}
        }
    }
    
    printf("%-20s%10.2f\n", "Likelyhood JpsiK_2d_controlLb: ", JpsiK_2d_controlLb_likelyhood);
    
    Double_t pK_2d_controlLb_likelyhood = 0.;
    
    for(int i = 1; i <= pK_2d_controlLb_data->ProjectionX()->GetNbinsX(); i++)
    {
        for(int k = 1; k <= pK_2d_controlLb_data->ProjectionY()->GetNbinsX(); k++)
        {
            //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
            //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            fb += pK_2d_controlLb_bg->GetBinContent(i, k);
            fb += pK_2d_controlLb_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
            fb += pK_2d_controlLb_BD_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += pK_2d_controlLb_BS_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += pK_2d_controlLb_BS_to_JPSI_K_K->GetBinContent(i, k);
            fb += pK_2d_controlLb_BD_to_JPSI_K_PI->GetBinContent(i, k);
            if (RARE_DECAYS)
            {
                fb += pK_2d_controlLb_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                fb += pK_2d_controlLb_BD_to_JPSI_K_K->GetBinContent(i, k);
                fb += pK_2d_controlLb_BS_to_JPSI_K_PI->GetBinContent(i, k);
            }
            //cout << bglevel0_psipi << endl;
            nb = pK_2d_controlLb_data->GetBinContent(i, k);
            if( fb > 0.0 ) pK_2d_controlLb_likelyhood += -nb*log(fb) + fb;
            //if ((-nb*log(fb) + fb) > 0.) {cout << i << "\t" << k << endl;}
        }
    }
    
    printf("%-20s%10.2f\n", "Likelyhood pK_2d_controlLb: ", pK_2d_controlLb_likelyhood);
    
    printf("%-20s\n\n", "4 track distributions likelyhood:");
    
    Double_t pK_Kp_2d_likelyhood = 0.;
    
    for(int i = 1; i <= pK_Kp_2d_data->ProjectionX()->GetNbinsX(); i++)
    {
        for(int k = 1; k <= pK_Kp_2d_data->ProjectionY()->GetNbinsX(); k++)
        {
            //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
            //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            fb += pK_Kp_2d_bg->GetBinContent(i, k);
            fb += pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
            fb += pK_Kp_2d_BD_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += pK_Kp_2d_BS_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += pK_Kp_2d_BS_to_JPSI_K_K->GetBinContent(i, k);
            fb += pK_Kp_2d_BD_to_JPSI_K_PI->GetBinContent(i, k);
            if (RARE_DECAYS)
            {
                fb += pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                fb += pK_Kp_2d_BD_to_JPSI_K_K->GetBinContent(i, k);
                fb += pK_Kp_2d_BS_to_JPSI_K_PI->GetBinContent(i, k);
            }
            //cout << bglevel0_psipi << endl;
            nb = pK_Kp_2d_data->GetBinContent(i, k);
            if( fb > 0.0 ) pK_Kp_2d_likelyhood += -nb*log(fb) + fb;
            //if ((-nb*log(fb) + fb) > 0.) {cout << i << "\t" << k << endl;}
        }
    }
    
    printf("%-20s%10.2f\n", "Likelyhood pK_Kp_2d: ", pK_Kp_2d_likelyhood);
    
    Double_t Kpi_piK_2d_likelyhood = 0.;
    
    for(int i = 1; i <= Kpi_piK_2d_data->ProjectionX()->GetNbinsX(); i++)
    {
        for(int k = 1; k <= Kpi_piK_2d_data->ProjectionY()->GetNbinsX(); k++)
        {
            //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
            //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            fb += Kpi_piK_2d_bg->GetBinContent(i, k);
            fb += Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
            fb += Kpi_piK_2d_BD_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += Kpi_piK_2d_BS_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += Kpi_piK_2d_BS_to_JPSI_K_K->GetBinContent(i, k);
            fb += Kpi_piK_2d_BD_to_JPSI_K_PI->GetBinContent(i, k);
            if (RARE_DECAYS)
            {
                fb += Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                fb += Kpi_piK_2d_BD_to_JPSI_K_K->GetBinContent(i, k);
                fb += Kpi_piK_2d_BS_to_JPSI_K_PI->GetBinContent(i, k);
            }
            //cout << bglevel0_psipi << endl;
            nb = Kpi_piK_2d_data->GetBinContent(i, k);
            if( fb > 0.0 ) Kpi_piK_2d_likelyhood += -nb*log(fb) + fb;
            //if ((-nb*log(fb) + fb) > 0.) {cout << i << "\t" << k << endl;}
        }
    }
    
    printf("%-20s%10.2f\n", "Likelyhood Kpi_piK_2d: ", Kpi_piK_2d_likelyhood);
    
    Double_t ppi_pip_2d_likelyhood = 0.;
    
    for(int i = 1; i <= ppi_pip_2d_data->ProjectionX()->GetNbinsX(); i++)
    {
        for(int k = 1; k <= ppi_pip_2d_data->ProjectionY()->GetNbinsX(); k++)
        {
            //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
            //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            fb += ppi_pip_2d_bg->GetBinContent(i, k);
            fb += ppi_pip_2d_LAMBDA0B_to_JPSI_P_K->GetBinContent(i, k);
            fb += ppi_pip_2d_BD_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += ppi_pip_2d_BS_to_JPSI_PI_PI->GetBinContent(i, k);
            fb += ppi_pip_2d_BS_to_JPSI_K_K->GetBinContent(i, k);
            fb += ppi_pip_2d_BD_to_JPSI_K_PI->GetBinContent(i, k);
            if (RARE_DECAYS)
            {
                fb += ppi_pip_2d_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i, k);
                fb += ppi_pip_2d_BD_to_JPSI_K_K->GetBinContent(i, k);
                fb += ppi_pip_2d_BS_to_JPSI_K_PI->GetBinContent(i, k);
            }
            //cout << bglevel0_psipi << endl;
            nb = ppi_pip_2d_data->GetBinContent(i, k);
            if( fb > 0.0 ) ppi_pip_2d_likelyhood += -nb*log(fb) + fb;
            //if ((-nb*log(fb) + fb) > 0.) {cout << i << "\t" << k << endl;}
        }
    }
    
    printf("%-20s%10.2f\n", "Likelyhood ppi_pip_2d: ", ppi_pip_2d_likelyhood);
    
    Double_t pipi_1d_likelyhood = 0.;
    
    for(int i = 1; i <= pipi_1d_data->GetNbinsX(); i++)
    {
        double nb = 0.0; double nberr = 0.0; double fb = 0.0;
        fb += pipi_1d_bg->GetBinContent(i);
        fb += pipi_1d_LAMBDA0B_to_JPSI_P_K->GetBinContent(i);
        fb += pipi_1d_BD_to_JPSI_PI_PI->GetBinContent(i);
        fb += pipi_1d_BS_to_JPSI_PI_PI->GetBinContent(i);
        fb += pipi_1d_BS_to_JPSI_K_K->GetBinContent(i);
        fb += pipi_1d_BD_to_JPSI_K_PI->GetBinContent(i);
        if (RARE_DECAYS)
        {
            fb += pipi_1d_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i);
            fb += pipi_1d_BD_to_JPSI_K_K->GetBinContent(i);
            fb += pipi_1d_BS_to_JPSI_K_PI->GetBinContent(i);
        }
        nb = pipi_1d_data->GetBinContent(i);
        if( fb > 0.0 ) pipi_1d_likelyhood += -nb*log(fb) + fb;
    }
    
    printf("%-20s%10.2f\n", "Likelyhood pipi_1d: ", pipi_1d_likelyhood);
    
    Double_t KK_1d_likelyhood = 0.;
    
    for(int i = 1; i <= KK_1d_data->GetNbinsX(); i++)
    {
        double nb = 0.0; double nberr = 0.0; double fb = 0.0;
        fb += KK_1d_bg->GetBinContent(i);
        fb += KK_1d_LAMBDA0B_to_JPSI_P_K->GetBinContent(i);
        fb += KK_1d_BD_to_JPSI_PI_PI->GetBinContent(i);
        fb += KK_1d_BS_to_JPSI_PI_PI->GetBinContent(i);
        fb += KK_1d_BS_to_JPSI_K_K->GetBinContent(i);
        fb += KK_1d_BD_to_JPSI_K_PI->GetBinContent(i);
        if (RARE_DECAYS)
        {
            fb += KK_1d_LAMBDA0B_to_JPSI_P_PI->GetBinContent(i);
            fb += KK_1d_BD_to_JPSI_K_K->GetBinContent(i);
            fb += KK_1d_BS_to_JPSI_K_PI->GetBinContent(i);
        }
        nb = KK_1d_data->GetBinContent(i);
        if( fb > 0.0 ) KK_1d_likelyhood += -nb*log(fb) + fb;
    }
    
    printf("%-20s%10.2f\n\n", "Likelyhood KK_1d: ", KK_1d_likelyhood);
    
    Double_t total_likelyhood = 0.;
    
    switch (fit_style)
    {
        case 0:
            cout << "\nThe mode JpsipK_fit_mode_plus_JpsiKpi was chosen...\n" << endl;
            total_likelyhood += Kpi_piK_2d_likelyhood + pK_Kp_2d_likelyhood;
            break;
        case 1:
            cout << "\nThe mode Jpsipipi_fit_mode_plus_JpsiKpi was chosen...\n" << endl;
            total_likelyhood += pipi_1d_likelyhood + Kpi_piK_2d_likelyhood;
            break;
        case 2:
            cout << "\nThe mode fit JpsiKK_fit_mode_plus_JpsiKpi was chosen...\n" << endl;
            total_likelyhood += KK_1d_likelyhood + KK_1d_control_likelyhood + JpsiK_2d_control_likelyhood;
            break;
        case 3:
            cout << "\nThe mode JpsiKpi_fit_mode_no_signal was chosen...\n" << endl;
            total_likelyhood += Kpi_piK_2d_likelyhood;
            break;
        case 4:
            cout << "\nThe mode fit signal_area_fit_mode was chosen...\n" << endl;
            total_likelyhood += Jpsipi_2d_likelyhood + JpsiK_2d_likelyhood + Kpi_2d_likelyhood + cos_theta_Zc_likelyhood + cos_theta_B0_likelyhood + cos_theta_Kstar_likelyhood + cos_theta_psi_Kstar_likelyhood + phi_mu_X_likelyhood + phi_K_likelyhood;
            break;
        case 5:
            cout << "\nThe mode Jpsipipi_fit_mode_plus_JpsiKpi_lbfix was chosen...\n" << endl;
            total_likelyhood += pipi_1d_likelyhood + Kpi_piK_2d_likelyhood;
            break;
        case 6:
            cout << "\nThe mode JpsiKK_fit_mode_plus_JpsiKpi_lbpipi_fix was chosen...\n" << endl;
            total_likelyhood += KK_1d_likelyhood + KK_1d_control_likelyhood + JpsiK_2d_control_likelyhood;
            break;
        case 7:
            cout << "\nThe mode JpsiKK_fit_mode_plus_JpsiKpi_no_control_lbpipi_fix was chosen...\n" << endl;
            total_likelyhood += KK_1d_likelyhood + Kpi_piK_2d_likelyhood;
            break;
        case 8:
            cout << "\nThe mode signal_area_fit_mode_short was chosen...\n" << endl;
            total_likelyhood += Jpsipi_2d_likelyhood + JpsiK_2d_likelyhood + Kpi_2d_likelyhood + cos_theta_Zc_likelyhood;
            break;
        case 9:
            cout << "\nThe mode JpsipK_fit_mode_comb_only was chosen...\n" << endl;
            total_likelyhood += pK_Kp_2d_likelyhood;
            break;
        case 10:
            cout << "\nThe mode global_area_no_comb_form_4tr was chosen...\n" << endl;
            total_likelyhood += KK_1d_likelyhood + pipi_1d_likelyhood + Kpi_piK_2d_likelyhood + pK_Kp_2d_likelyhood;
            break; 
        case 11:
            cout << "\nThe mode Jpsipipi_fit_mode_comb_only was chosen...\n" << endl;
            total_likelyhood += pipi_1d_likelyhood;
            break;
        case 12:
            cout << "\nThe mode JpsiKK_fit_mode_comb_only was chosen...\n" << endl;
            total_likelyhood += KK_1d_likelyhood;
            break;
        case 13:
            cout << "\nThe mode JpsiKpi_fit_mode_comb_only was chosen...\n" << endl;
            total_likelyhood += Kpi_piK_2d_likelyhood;
            break;
        case 14:
            cout << "\nThe mode signal_area_fit_mode_short_noZc was chosen...\n" << endl;
            total_likelyhood += Jpsipi_2d_likelyhood + JpsiK_2d_likelyhood + Kpi_2d_likelyhood + cos_theta_Zc_likelyhood;
            break;
        case 15:
            cout << "\nThe mode signal_area_fit_mode_noZc was chosen...\n" << endl;
            total_likelyhood += Jpsipi_2d_likelyhood + JpsiK_2d_likelyhood + Kpi_2d_likelyhood + cos_theta_Zc_likelyhood + cos_theta_B0_likelyhood + cos_theta_Kstar_likelyhood + cos_theta_psi_Kstar_likelyhood + phi_mu_X_likelyhood + phi_K_likelyhood;
            break;
        case 16:
            cout << "\nThe mode fit signal_area_fit_mode_onlyZc was chosen...\n" << endl;
            total_likelyhood += Jpsipi_2d_likelyhood + Kpi_2d_likelyhood;
            break;
        case 17:
            cout << "\nThe mode fit Jpsippi_fit_mode_comb_only was chosen...\n" << endl;
            total_likelyhood += ppi_pip_2d_likelyhood;
            break;
        case 101:
            cout << "\nThe mode only drawing pictures was chosen\n" << endl;
            break;
        default:
            cout << "\n!!! Choose proper fit_style (0-9 & 101) !!!\n" << endl;
            return 1;
            break;
    }
    
    printf("\n%-20s%10.2f\n", "Likelyhood total: ", total_likelyhood);
    printf("%-20s%10.2f\n\n", "Minimized function: ", amin);
    
    
	cout << pK_Kp_2d_data->ProjectionX()->GetNbinsX() << "\t" << pK_Kp_2d_data->ProjectionY()->GetNbinsX() << endl;
	cout << pK_Kp_2d_bg->ProjectionX()->GetNbinsX() << "\t" << pK_Kp_2d_bg->ProjectionY()->GetNbinsX() << endl;
	cout << pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->ProjectionX()->GetNbinsX() << "\t" << pK_Kp_2d_LAMBDA0B_to_JPSI_P_K->ProjectionY()->GetNbinsX() << endl;
	cout << pK_Kp_2d_BD_to_JPSI_PI_PI->ProjectionX()->GetNbinsX() << "\t" << pK_Kp_2d_BD_to_JPSI_PI_PI->ProjectionY()->GetNbinsX() << endl;
	cout << pK_Kp_2d_BS_to_JPSI_PI_PI->ProjectionX()->GetNbinsX() << "\t" << pK_Kp_2d_BS_to_JPSI_PI_PI->ProjectionY()->GetNbinsX() << endl;
	cout << pK_Kp_2d_BS_to_JPSI_K_K->ProjectionX()->GetNbinsX() << "\t" << pK_Kp_2d_BS_to_JPSI_K_K->ProjectionY()->GetNbinsX() << endl;
	cout << pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->GetNbinsX() << "\t" << pK_Kp_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->GetNbinsX() << endl;
	cout << pK_Kp_2d_BD_to_JPSI_K_K->ProjectionX()->GetNbinsX() << "\t" << pK_Kp_2d_BD_to_JPSI_K_K->ProjectionY()->GetNbinsX() << endl;
	cout << pK_Kp_2d_BS_to_JPSI_K_PI->ProjectionX()->GetNbinsX() << "\t" << pK_Kp_2d_BS_to_JPSI_K_PI->ProjectionY()->GetNbinsX() << endl;
    
    Double_t seconds = (Double_t)(end - start) / CLOCKS_PER_SEC;
    printf("\n%-20s%10.2f\n\n", "time:\t", seconds);
    
    if (n_fit_step > 0)
    {
        printf("\n%-20s%10.2f\n\n", "1 fit step:\t", seconds / (Double_t)n_fit_step);
    }
    
    
   
     
    return 0;
}
