void phi_mu_Kstar_Y()
{
//=========Macro generated from canvas: phi_mu_Kstar_Y/
//=========  (Sat Nov 14 04:46:30 2020) by ROOT version 6.22/02
   TCanvas *phi_mu_Kstar_Y = new TCanvas("phi_mu_Kstar_Y", "",0,0,700,500);
   phi_mu_Kstar_Y->Range(0,0,1,1);
   phi_mu_Kstar_Y->SetFillColor(0);
   phi_mu_Kstar_Y->SetBorderMode(0);
   phi_mu_Kstar_Y->SetBorderSize(2);
   phi_mu_Kstar_Y->SetFrameBorderMode(0);
   
   TH1D *hist_buffer__256 = new TH1D("hist_buffer__256","",10,-3.15,3.15);
   hist_buffer__256->SetBinContent(1,2772);
   hist_buffer__256->SetBinContent(2,5545);
   hist_buffer__256->SetBinContent(3,9726);
   hist_buffer__256->SetBinContent(4,5616);
   hist_buffer__256->SetBinContent(5,3009);
   hist_buffer__256->SetBinContent(6,2904);
   hist_buffer__256->SetBinContent(7,5671);
   hist_buffer__256->SetBinContent(8,9831);
   hist_buffer__256->SetBinContent(9,5647);
   hist_buffer__256->SetBinContent(10,2901);
   hist_buffer__256->SetBinError(1,58.10336);
   hist_buffer__256->SetBinError(2,82.04267);
   hist_buffer__256->SetBinError(3,108.8853);
   hist_buffer__256->SetBinError(4,82.03658);
   hist_buffer__256->SetBinError(5,59.75784);
   hist_buffer__256->SetBinError(6,58.97457);
   hist_buffer__256->SetBinError(7,82.41966);
   hist_buffer__256->SetBinError(8,108.9908);
   hist_buffer__256->SetBinError(9,82.45605);
   hist_buffer__256->SetBinError(10,58.59181);
   hist_buffer__256->SetMinimum(0);
   hist_buffer__256->SetMaximum(10814.1);
   hist_buffer__256->SetEntries(64684);
   hist_buffer__256->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   hist_buffer__256->SetLineColor(ci);
   hist_buffer__256->SetMarkerStyle(20);
   hist_buffer__256->GetXaxis()->SetTitle("#phi_{#mu}^{K*} (h_{1}=#pi, h_{2}=K)");
   hist_buffer__256->GetXaxis()->SetLabelFont(42);
   hist_buffer__256->GetXaxis()->SetTitleFont(42);
   hist_buffer__256->GetYaxis()->SetTitle("Events / (2#pi/10)");
   hist_buffer__256->GetYaxis()->SetLabelFont(42);
   hist_buffer__256->GetYaxis()->SetTitleOffset(1.35);
   hist_buffer__256->GetYaxis()->SetTitleFont(42);
   hist_buffer__256->GetZaxis()->SetLabelFont(42);
   hist_buffer__256->GetZaxis()->SetTitleOffset(1);
   hist_buffer__256->GetZaxis()->SetTitleFont(42);
   hist_buffer__256->Draw("");
   
   THStack *hs_phi_mu_Kstar_Y = new THStack();
   hs_phi_mu_Kstar_Y->SetName("hs_phi_mu_Kstar_Y");
   hs_phi_mu_Kstar_Y->SetTitle("phi_mu_Kstar_Y");
   
   TH1D *phi_mu_Kstar_bg_py_stack_1 = new TH1D("phi_mu_Kstar_bg_py_stack_1","phi_mu_Kstar_bg",10,-3.15,3.15);
   phi_mu_Kstar_bg_py_stack_1->SetBinContent(1,200.4206);
   phi_mu_Kstar_bg_py_stack_1->SetBinContent(2,385.2769);
   phi_mu_Kstar_bg_py_stack_1->SetBinContent(3,588.9816);
   phi_mu_Kstar_bg_py_stack_1->SetBinContent(4,385.2919);
   phi_mu_Kstar_bg_py_stack_1->SetBinContent(5,237.895);
   phi_mu_Kstar_bg_py_stack_1->SetBinContent(6,187.1931);
   phi_mu_Kstar_bg_py_stack_1->SetBinContent(7,270.7985);
   phi_mu_Kstar_bg_py_stack_1->SetBinContent(8,629.4126);
   phi_mu_Kstar_bg_py_stack_1->SetBinContent(9,369.7271);
   phi_mu_Kstar_bg_py_stack_1->SetBinContent(10,198.0275);
   phi_mu_Kstar_bg_py_stack_1->SetBinError(1,16.94449);
   phi_mu_Kstar_bg_py_stack_1->SetBinError(2,43.61674);
   phi_mu_Kstar_bg_py_stack_1->SetBinError(3,35.54124);
   phi_mu_Kstar_bg_py_stack_1->SetBinError(4,37.26193);
   phi_mu_Kstar_bg_py_stack_1->SetBinError(5,52.38436);
   phi_mu_Kstar_bg_py_stack_1->SetBinError(6,34.34258);
   phi_mu_Kstar_bg_py_stack_1->SetBinError(7,41.03807);
   phi_mu_Kstar_bg_py_stack_1->SetBinError(8,35.79814);
   phi_mu_Kstar_bg_py_stack_1->SetBinError(9,25.10079);
   phi_mu_Kstar_bg_py_stack_1->SetBinError(10,49.47163);
   phi_mu_Kstar_bg_py_stack_1->SetEntries(200539);
   phi_mu_Kstar_bg_py_stack_1->SetFillColor(42);
   phi_mu_Kstar_bg_py_stack_1->GetXaxis()->SetLabelFont(42);
   phi_mu_Kstar_bg_py_stack_1->GetXaxis()->SetTitleFont(42);
   phi_mu_Kstar_bg_py_stack_1->GetYaxis()->SetLabelFont(42);
   phi_mu_Kstar_bg_py_stack_1->GetYaxis()->SetTitleFont(42);
   phi_mu_Kstar_bg_py_stack_1->GetZaxis()->SetLabelFont(42);
   phi_mu_Kstar_bg_py_stack_1->GetZaxis()->SetTitleOffset(1);
   phi_mu_Kstar_bg_py_stack_1->GetZaxis()->SetTitleFont(42);
   hs_phi_mu_Kstar_Y->Add(phi_mu_Kstar_bg_py_stack_1,"");
   
   TH1D *phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2 = new TH1D("phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2","phi_mu_Kstar_BD_to_JPSI_PI_PI",10,-3.15,3.15);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->SetBinContent(1,16.8961);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->SetBinContent(2,15.40945);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->SetBinContent(3,44.22851);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->SetBinContent(4,13.50637);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->SetBinContent(5,11.41276);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->SetBinContent(6,12.65972);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->SetBinContent(7,21.61819);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->SetBinContent(8,28.77459);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->SetBinContent(9,14.23169);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->SetBinContent(10,8.981829);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->SetBinError(1,4.126654);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->SetBinError(2,3.742757);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->SetBinError(3,6.487807);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->SetBinError(4,3.623071);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->SetBinError(5,3.316454);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->SetBinError(6,3.543262);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->SetBinError(7,4.525774);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->SetBinError(8,5.186653);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->SetBinError(9,3.697378);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->SetBinError(10,3.01581);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->SetEntries(198);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->SetFillColor(8);

   ci = TColor::GetColor("#000099");
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->SetLineColor(ci);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->GetXaxis()->SetLabelFont(42);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->GetXaxis()->SetTitleFont(42);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->GetYaxis()->SetLabelFont(42);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->GetYaxis()->SetTitleFont(42);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->GetZaxis()->SetLabelFont(42);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->GetZaxis()->SetTitleOffset(1);
   phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2->GetZaxis()->SetTitleFont(42);
   hs_phi_mu_Kstar_Y->Add(phi_mu_Kstar_BD_to_JPSI_PI_PI_py_stack_2,"");
   
   TH1D *phi_mu_Kstar_BS_to_JPSI_PI_PI_py_stack_3 = new TH1D("phi_mu_Kstar_BS_to_JPSI_PI_PI_py_stack_3","phi_mu_Kstar_BS_to_JPSI_PI_PI",10,-3.15,3.15);
   phi_mu_Kstar_BS_to_JPSI_PI_PI_py_stack_3->SetBinContent(2,0.870277);
   phi_mu_Kstar_BS_to_JPSI_PI_PI_py_stack_3->SetBinContent(3,2.973929);
   phi_mu_Kstar_BS_to_JPSI_PI_PI_py_stack_3->SetBinContent(4,0.8803192);
   phi_mu_Kstar_BS_to_JPSI_PI_PI_py_stack_3->SetBinContent(5,1.796164);
   phi_mu_Kstar_BS_to_JPSI_PI_PI_py_stack_3->SetBinContent(6,0.9626446);
   phi_mu_Kstar_BS_to_JPSI_PI_PI_py_stack_3->SetBinContent(8,1.98372);
   phi_mu_Kstar_BS_to_JPSI_PI_PI_py_stack_3->SetBinContent(9,1.020379);
   phi_mu_Kstar_BS_to_JPSI_PI_PI_py_stack_3->SetBinError(2,0.870277);
   phi_mu_Kstar_BS_to_JPSI_PI_PI_py_stack_3->SetBinError(3,1.719775);
   phi_mu_Kstar_BS_to_JPSI_PI_PI_py_stack_3->SetBinError(4,0.8803192);
   phi_mu_Kstar_BS_to_JPSI_PI_PI_py_stack_3->SetBinError(5,1.270688);
   phi_mu_Kstar_BS_to_JPSI_PI_PI_py_stack_3->SetBinError(6,0.9626446);
   phi_mu_Kstar_BS_to_JPSI_PI_PI_py_stack_3->SetBinError(8,1.402705);
   phi_mu_Kstar_BS_to_JPSI_PI_PI_py_stack_3->SetBinError(9,1.020379);
   phi_mu_Kstar_BS_to_JPSI_PI_PI_py_stack_3->SetEntries(11);
   phi_mu_Kstar_BS_to_JPSI_PI_PI_py_stack_3->SetFillColor(46);

   ci = TColor::GetColor("#000099");
   phi_mu_Kstar_BS_to_JPSI_PI_PI_py_stack_3->SetLineColor(ci);
   phi_mu_Kstar_BS_to_JPSI_PI_PI_py_stack_3->GetXaxis()->SetLabelFont(42);
   phi_mu_Kstar_BS_to_JPSI_PI_PI_py_stack_3->GetXaxis()->SetTitleFont(42);
   phi_mu_Kstar_BS_to_JPSI_PI_PI_py_stack_3->GetYaxis()->SetLabelFont(42);
   phi_mu_Kstar_BS_to_JPSI_PI_PI_py_stack_3->GetYaxis()->SetTitleFont(42);
   phi_mu_Kstar_BS_to_JPSI_PI_PI_py_stack_3->GetZaxis()->SetLabelFont(42);
   phi_mu_Kstar_BS_to_JPSI_PI_PI_py_stack_3->GetZaxis()->SetTitleOffset(1);
   phi_mu_Kstar_BS_to_JPSI_PI_PI_py_stack_3->GetZaxis()->SetTitleFont(42);
   hs_phi_mu_Kstar_Y->Add(phi_mu_Kstar_BS_to_JPSI_PI_PI_py_stack_3,"");
   
   TH1D *phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4 = new TH1D("phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4","phi_mu_Kstar_BS_to_JPSI_K_K",10,-3.15,3.15);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->SetBinContent(1,350.2105);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->SetBinContent(2,809.2855);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->SetBinContent(3,1484.786);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->SetBinContent(4,772.957);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->SetBinContent(5,337.026);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->SetBinContent(6,393.856);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->SetBinContent(7,809.6929);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->SetBinContent(8,1521.21);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->SetBinContent(9,762.6202);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->SetBinContent(10,350.3121);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->SetBinError(1,17.47377);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->SetBinError(2,32.33206);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->SetBinError(3,45.93235);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->SetBinError(4,35.13699);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->SetBinError(5,17.88316);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->SetBinError(6,20.55426);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->SetBinError(7,32.88495);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->SetBinError(8,45.69632);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->SetBinError(9,31.12666);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->SetBinError(10,18.35487);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->SetEntries(21319);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->SetFillColor(30);

   ci = TColor::GetColor("#000099");
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->SetLineColor(ci);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->GetXaxis()->SetLabelFont(42);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->GetXaxis()->SetTitleFont(42);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->GetYaxis()->SetLabelFont(42);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->GetYaxis()->SetTitleFont(42);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->GetZaxis()->SetLabelFont(42);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->GetZaxis()->SetTitleOffset(1);
   phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4->GetZaxis()->SetTitleFont(42);
   hs_phi_mu_Kstar_Y->Add(phi_mu_Kstar_BS_to_JPSI_K_K_py_stack_4,"");
   
   TH1D *phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5 = new TH1D("phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5","phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K",10,-3.15,3.15);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->SetBinContent(1,222.9619);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->SetBinContent(2,416.249);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->SetBinContent(3,760.3161);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->SetBinContent(4,420.2591);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->SetBinContent(5,226.972);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->SetBinContent(6,211.7336);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->SetBinContent(7,406.6247);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->SetBinContent(8,782.7727);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->SetBinContent(9,440.3096);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->SetBinContent(10,245.4185);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->SetBinError(1,13.37237);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->SetBinError(2,18.2713);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->SetBinError(3,24.69392);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->SetBinError(4,18.3591);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->SetBinError(5,13.49208);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->SetBinError(6,13.0313);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->SetBinError(7,18.05884);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->SetBinError(8,25.05594);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->SetBinError(9,18.79196);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->SetBinError(10,14.02964);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->SetEntries(5154);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->SetFillColor(3);

   ci = TColor::GetColor("#000099");
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->SetLineColor(ci);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->GetXaxis()->SetLabelFont(42);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->GetXaxis()->SetTitleFont(42);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->GetYaxis()->SetLabelFont(42);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->GetYaxis()->SetTitleFont(42);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->GetZaxis()->SetLabelFont(42);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->GetZaxis()->SetTitleOffset(1);
   phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5->GetZaxis()->SetTitleFont(42);
   hs_phi_mu_Kstar_Y->Add(phi_mu_Kstar_LAMBDA0B_to_JPSI_P_K_py_stack_5,"");
   
   TH1D *phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6 = new TH1D("phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6","phi_mu_Kstar_BD_to_JPSI_K_PI",10,-3.15,3.15);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->SetBinContent(1,1890.69);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->SetBinContent(2,3883.068);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->SetBinContent(3,7212.848);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->SetBinContent(4,3826.864);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->SetBinContent(5,1866.917);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->SetBinContent(6,1954.191);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->SetBinContent(7,3880.266);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->SetBinContent(8,7150.68);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->SetBinContent(9,3715.417);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->SetBinContent(10,1788.83);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->SetBinError(1,41.91326);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->SetBinError(2,62.39115);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->SetBinError(3,88.51862);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->SetBinError(4,62.59016);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->SetBinError(5,43.21365);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->SetBinError(6,44.15113);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->SetBinError(7,62.21208);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->SetBinError(8,88.37185);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->SetBinError(9,61.98182);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->SetBinError(10,41.01786);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->SetEntries(106314);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->SetFillColor(7);

   ci = TColor::GetColor("#000099");
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->SetLineColor(ci);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->GetXaxis()->SetLabelFont(42);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->GetXaxis()->SetTitleFont(42);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->GetYaxis()->SetLabelFont(42);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->GetYaxis()->SetTitleFont(42);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->GetZaxis()->SetLabelFont(42);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->GetZaxis()->SetTitleOffset(1);
   phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6->GetZaxis()->SetTitleFont(42);
   hs_phi_mu_Kstar_Y->Add(phi_mu_Kstar_BD_to_JPSI_K_PI_py_stack_6,"");
   hs_phi_mu_Kstar_Y->Draw("hist same");
   
   TH1D *hist_buffer__257 = new TH1D("hist_buffer__257","",10,-3.15,3.15);
   hist_buffer__257->SetBinContent(1,2772);
   hist_buffer__257->SetBinContent(2,5545);
   hist_buffer__257->SetBinContent(3,9726);
   hist_buffer__257->SetBinContent(4,5616);
   hist_buffer__257->SetBinContent(5,3009);
   hist_buffer__257->SetBinContent(6,2904);
   hist_buffer__257->SetBinContent(7,5671);
   hist_buffer__257->SetBinContent(8,9831);
   hist_buffer__257->SetBinContent(9,5647);
   hist_buffer__257->SetBinContent(10,2901);
   hist_buffer__257->SetBinError(1,58.10336);
   hist_buffer__257->SetBinError(2,82.04267);
   hist_buffer__257->SetBinError(3,108.8853);
   hist_buffer__257->SetBinError(4,82.03658);
   hist_buffer__257->SetBinError(5,59.75784);
   hist_buffer__257->SetBinError(6,58.97457);
   hist_buffer__257->SetBinError(7,82.41966);
   hist_buffer__257->SetBinError(8,108.9908);
   hist_buffer__257->SetBinError(9,82.45605);
   hist_buffer__257->SetBinError(10,58.59181);
   hist_buffer__257->SetMinimum(0);
   hist_buffer__257->SetMaximum(10814.1);
   hist_buffer__257->SetEntries(64684);
   hist_buffer__257->SetStats(0);

   ci = TColor::GetColor("#000099");
   hist_buffer__257->SetLineColor(ci);
   hist_buffer__257->SetMarkerStyle(20);
   hist_buffer__257->GetXaxis()->SetTitle("#phi_{#mu}^{K*} (h_{1}=#pi, h_{2}=K)");
   hist_buffer__257->GetXaxis()->SetLabelFont(42);
   hist_buffer__257->GetXaxis()->SetTitleFont(42);
   hist_buffer__257->GetYaxis()->SetTitle("Events / (2#pi/10)");
   hist_buffer__257->GetYaxis()->SetLabelFont(42);
   hist_buffer__257->GetYaxis()->SetTitleOffset(1.35);
   hist_buffer__257->GetYaxis()->SetTitleFont(42);
   hist_buffer__257->GetZaxis()->SetLabelFont(42);
   hist_buffer__257->GetZaxis()->SetTitleOffset(1);
   hist_buffer__257->GetZaxis()->SetTitleFont(42);
   hist_buffer__257->Draw("same");
   
   TH1D *hist_buffer__258 = new TH1D("hist_buffer__258","",10,-3.15,3.15);
   hist_buffer__258->SetBinContent(1,2772);
   hist_buffer__258->SetBinContent(2,5545);
   hist_buffer__258->SetBinContent(3,9726);
   hist_buffer__258->SetBinContent(4,5616);
   hist_buffer__258->SetBinContent(5,3009);
   hist_buffer__258->SetBinContent(6,2904);
   hist_buffer__258->SetBinContent(7,5671);
   hist_buffer__258->SetBinContent(8,9831);
   hist_buffer__258->SetBinContent(9,5647);
   hist_buffer__258->SetBinContent(10,2901);
   hist_buffer__258->SetBinError(1,58.10336);
   hist_buffer__258->SetBinError(2,82.04267);
   hist_buffer__258->SetBinError(3,108.8853);
   hist_buffer__258->SetBinError(4,82.03658);
   hist_buffer__258->SetBinError(5,59.75784);
   hist_buffer__258->SetBinError(6,58.97457);
   hist_buffer__258->SetBinError(7,82.41966);
   hist_buffer__258->SetBinError(8,108.9908);
   hist_buffer__258->SetBinError(9,82.45605);
   hist_buffer__258->SetBinError(10,58.59181);
   hist_buffer__258->SetMinimum(0);
   hist_buffer__258->SetMaximum(10814.1);
   hist_buffer__258->SetEntries(64684);
   hist_buffer__258->SetStats(0);

   ci = TColor::GetColor("#000099");
   hist_buffer__258->SetLineColor(ci);
   hist_buffer__258->SetMarkerStyle(20);
   hist_buffer__258->GetXaxis()->SetTitle("#phi_{#mu}^{K*} (h_{1}=#pi, h_{2}=K)");
   hist_buffer__258->GetXaxis()->SetLabelFont(42);
   hist_buffer__258->GetXaxis()->SetTitleFont(42);
   hist_buffer__258->GetYaxis()->SetTitle("Events / (2#pi/10)");
   hist_buffer__258->GetYaxis()->SetLabelFont(42);
   hist_buffer__258->GetYaxis()->SetTitleOffset(1.35);
   hist_buffer__258->GetYaxis()->SetTitleFont(42);
   hist_buffer__258->GetZaxis()->SetLabelFont(42);
   hist_buffer__258->GetZaxis()->SetTitleOffset(1);
   hist_buffer__258->GetZaxis()->SetTitleFont(42);
   hist_buffer__258->Draw("same axis");
   phi_mu_Kstar_Y->Modified();
   phi_mu_Kstar_Y->cd();
   phi_mu_Kstar_Y->SetSelected(phi_mu_Kstar_Y);
}
