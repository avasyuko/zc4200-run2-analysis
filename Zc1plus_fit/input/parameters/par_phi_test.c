{
	Double_t PI = TMath::Pi();
	
	TFile* inpFile = TFile::Open("parameters_signal_noZc_v16_it2_n3.root");
	TVectorD *vpar = (TVectorD*)inpFile->Get("parameters");
	//printf("Printing start values of parameters");
	for(int j=0; j<214; j++){
	
	if( (j >= 0 && j <= 43)   ){
	  if( j % 2 == 1){  
		cout << j << "\t" << (*vpar)(j) << "\t";
		int n = (int)((*vpar)(j)/2/PI);   (*vpar)(j) =  (*vpar)(j) - n*2*PI;  // force phase to 0..2PI range
		if((*vpar)(j-1) < 0){(*vpar)(j-1) *= -1.0; (*vpar)(j) += PI; }        // force amplitude to be positive, phase gets additional PI
		if(j > 1) (*vpar)(j) = (*vpar)(j) - (*vpar)(1);
		cout << (*vpar)(j) << endl;
		//subtraction of the K1410 phase
		}//ifj%2

	}//if 0..43



	if( (j >= 47 && j <= 78)   ){
	  if( j % 2 == 0){  
		cout << j << "\t" << (*vpar)(j) << "\t";
		int n = (int)((*vpar)(j)/2/PI);   (*vpar)(j) =  (*vpar)(j) - n*2*PI;  // force phase to 0..2PI range
		if((*vpar)(j-1) < 0){(*vpar)(j-1) *= -1.0; (*vpar)(j) += PI; }  // force amplitude to be positive, phase gets additional PI 
		if(j > 48) (*vpar)(j) = (*vpar)(j) - (*vpar)(48);
		cout << (*vpar)(j) << endl;
		}
	}// if 47..78

    } //for
    (*vpar)(1) = 0.0;
    (*vpar)(48) = 0.0;
}