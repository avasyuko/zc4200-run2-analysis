{
    TFile *f_in_par = new TFile("parameters/parameters_signal_Zc1plus_v16_it2_n2.root", "open");
    TVectorD *vpar = (TVectorD*)f_in_par->Get("parameters");
    
    TFile *f_in_par1 = new TFile("parameters/parameters_from_IV_to_Artem_2Pc.root", "open");
    TVectorD *vpar1 = (TVectorD*)f_in_par1->Get("parameters");
    
    TFile *f_out = new TFile("parameters/parameters_signal_Zc1plus_v16_2Pc_syst_0.root", "recreate");
    TVectorD parameters(217);
    
    for(int i = 0; i < 217; i++)
    {
        parameters[i] = (*vpar)(i);
    }
    
    cout << "Lb parameters from RUN1 analysis" << endl;
    
    for(int i = 121; i <= 136; i++)
    {
        parameters[i] = (*vpar1)(i - 81);
    }
    
    for(int i = 137; i <= 140; i++)
    {
        parameters[i] = 0.;
    }
    
    for(int i = 141; i <= 142; i++)
    {
        parameters[i] = (*vpar1)(i - 85);
    }
    
    for(int i = 143; i <= 146; i++)
    {
        parameters[i] = 0.;
    }
    
    for(int i = 147; i <= 154; i++)
    {
        parameters[i] = (*vpar1)(i - 89);
    }
    
    for(int i = 155; i <= 158; i++)
    {
        parameters[i] = 0.;
    }
    
    for(int i = 159; i <= 160; i++)
    {
        parameters[i] = (*vpar1)(i - 93);
    }
    
    for(int i = 161; i <= 164; i++)
    {
        parameters[i] = 0.;
    }
    
    for(int i = 165; i <= 166; i++)
    {
        parameters[i] = (*vpar1)(i - 97);
    }
    
    for(int i = 167; i <= 170; i++)
    {
        parameters[i] = 0.;
    }
    
    for(int i = 171; i <= 172; i++)
    {
        parameters[i] = (*vpar1)(i - 101);
    }
    
    for(int i = 173; i <= 176; i++)
    {
        parameters[i] = 0.;
    }
    
    for(int i = 177; i <= 178; i++)
    {
        parameters[i] = (*vpar1)(i - 105);
    }
    
    for(int i = 179; i <= 182; i++)
    {
        parameters[i] = 0.;
    }
    
    for(int i = 183; i <= 184; i++)
    {
        parameters[i] = (*vpar1)(i - 109);
    }
    
    for(int i = 185; i <= 186; i++)
    {
        parameters[i] = 0.;
    }
    
    for(int i = 187; i <= 188; i++)
    {
        //parameters[i] = (*vpar1)(i - 156);
        parameters[i] = 0.;
    }
    
    for(int i = 189; i <= 208; i++)
    {
        parameters[i] = (*vpar1)(i - 189);
        //parameters[i] = 0.;
    }
    
    parameters.Write("parameters");
    
    f_out->Close();    
}
