#!/bin/zsh

##source ~/set_panda.sh
suffix=B0
today=d_$(date +%y%m%d)_$(date +%H%M%S)$1



if [ -f incFiles.tar.gz ]
then
   echo "Using existing incFiles.tar"
else 
   echo "Creating incFiles.tar containing:"
   tar -zcvf incFiles.tar.gz input
   ###bzip2 incFiles.tar.gz
   ###rm incFiles.tar.gz

fi

echo "submitting job..."

prun --rootVer=6.14/04 --cmtConfig=x86_64-slc6-gcc62-opt --exec "tar -zxvf incFiles.tar.gz; mv input/* .; mkdir outputs; . run_test_v16_signal.sh; mv *.dat outputs/.; mv angles_Zc1plus outputs/.; mv pictures_Zc1plus outputs/.; mv parameters/*.root outputs/.; tar -cvf outputs.tar ./outputs;" --outDS user.avasyuko.Zc1plus_signal_it2_n8.$suffix.$today --outputs "outputs.tar" --extFile "incFiles.tar.gz" --nCore=8 #--maxFileSize 20000000 ###--maxCpuCount=259200 --site ANALY_MWT2_MCORE_SL7 ####--site=ANALY_FZK_UCORE ####

rm incFiles.tar.gz

###--athenaTag=21.1.0
## --athenaTag=20.1.0  ## this says support for c++11 needed
##--athenaTag=21.1.0  
