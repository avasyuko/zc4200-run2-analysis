#!/bin/zsh

##source ~/set_panda.sh
inum=iteration1
suffix=Zc1plus_global_$inum
today=d_$(date +%y%m%d)_$(date +%H%M%S)$1

mkdir $inum
touch ${inum}/taskid_${inum}.txt
touch ${inum}/dataset_${inum}.txt
touch ${inum}/buf.txt

if [ -f incFiles.tar.gz ]
then
   echo "Using existing incFiles.tar"
else
   echo "Creating incFiles.tar containing:"
   tar -zcvf incFiles.tar.gz input
   ###bzip2 incFiles.tar.gz

fi

echo "submitting job..."

i=0
while [[ $i -lt "10" ]]
	do prun --maxCpuCount=259200 --rootVer=6.14/04 --cmtConfig=x86_64-slc6-gcc62-opt --osMatching --exec "tar -zxvf incFiles.tar.gz; mv input/*.* .; mkdir outputs; . run.sh; mv *.dat outputs/.; mv *.png outputs/.; mv parameters*.root outputs/.; tar -cvf outputs.tar ./outputs; rm *.root" --outDS user.avasyuko.first_test_run.$suffix.$today --outputs "outputs.tar" --extFile "incFiles.tar.gz" --nCore=8 2>> ${inum}/buf.txt
let "i = i + 1"
echo user.first_avasyuko._test_run.$suffix.$today >> ${inum}/dataset_${inum}.txt
today=d_$(date +%y%m%d)_$(date +%H%M%S)$1
done

while read LINE
    do if [[ ${LINE:0:16} == "INFO : succeeded" ]]
then
echo ${LINE:33:8} >> ${inum}/taskid_${inum}.txt
fi
done < ${inum}/buf.txt

rm ${inum}/buf.txt

rm incFiles.tar.gz
