That folder contains code to run fit (Zc1plus hypothesis). At that moment it isn`t optimized to run in grid (will be fixed later).

At that moment the latest version of fit is 16. I decided to leave that notation in order to avoid confision.  

Instructions to run fitting code:

[0] cd input
[1] . run_test_v16_<version>.sh

Let`s consider fit function:

signal_area_fit_Zc1plus_v16(int threads, std::string par_in, std::string par_out, Int_t fit_style, Int_t decay_model)

par_in - input vector of parameters;
par_out - output vector of parameters;
fit_style - can be 0-15 and 101. 101 - only drawing pictures;
decay_model - can be 0 (Zc1plus), 1 (noZc), 2 (Zc1minus), 3 (Zc0minus), 4 (Zc2minus), 5 (Zc2plus), 6 (drawing pictures for test);

Pictures will be saved in pictures_<decay model> folder, vectors of parameters - in parameters folder.

Code hasn`t been optimized to run in grid yet.
