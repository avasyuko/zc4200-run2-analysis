enum class Dataset : std::size_t { kData15, kData16p1, kData16p2, kData17, kData18, kMC16a, kMC16d, kMC16e };

UInt_t m_HLT_isPassed = 0;
uint32_t m_runNumber;

std::vector< std::string > m_HLT;



UInt_t triggerBits() { return m_HLT_isPassed; }

std::vector< std::string >& trigger() { return m_HLT; }

void addTrigger( const std::string& trigger ) { m_HLT.push_back( trigger ); }

Dataset dataset() {

  // mc16e.private
  //if ( mcChannelNumber() >= 999000 ) return Dataset::kMC16e;

  if ( m_runNumber == 284500 )
    return Dataset::kMC16a;
  else if ( m_runNumber == 300000 )
    return Dataset::kMC16d;
  else if ( m_runNumber == 310000 )
    return Dataset::kMC16e;
  else
    throw std::out_of_range( "run number is out of range" );
}



void setTriggerBits() {

  static const std::vector< std::string > triggerName = {            //  15m 16m1 16m2  16b  17b  18b
    "HLT_mu6_mu4_bJpsimumu_noL2",                                    //    1    0    0    0    0    0
    "HLT_2mu4_bJpsimumu_noL2",                                       //    1    0    0    0    0    0
    "HLT_mu6_mu4_bJpsimumu",                                         //    0    1    0    0    0    0
    "HLT_mu10_mu6_bJpsimumu",                                        //    0    1    0    0    0    0
    "HLT_2mu6_bJpsimumu",                                            //    0    1    0    0    0    0
    "HLT_mu20_2mu0noL1_JpsimumuFS",                                  //    0    1    1    0    0    0
    "HLT_mu6_2mu4_bJpsi",                                            //    0    1    0    0    0    0
    "HLT_mu20_nomucomb_mu6noL1_nscan03",                             //    0    0    1    0    0    0
    "HLT_2mu6_bJpsimumu_delayed",                                    //    0    0    0    1    0    0
    "HLT_mu6_mu4_bJpsimumu_Lxy0_delayed",                            //    0    0    0    1    0    0
    "HLT_mu6_nomucomb_2mu4_nomucomb_delayed_L1MU6_3MU4",             //    0    0    0    1    0    0
    "HLT_3mu4_bJpsi_delayed",                                        //    0    0    0    1    0    0
    "HLT_2mu6_bJpsimumu_L1BPH-2M9-2MU6_BPH-2DR15-2MU6",              //    0    0    0    0    1    1
    "HLT_mu6_mu4_bJpsimumu_Lxy0_L1BPH-2M9-MU6MU4_BPH-0DR15-MU6MU4",  //    0    0    0    0    1    1
    "HLT_mu11_mu6_bDimu",                                            //    0    0    0    0    1    1
    "HLT_3mu4_bJpsi",                                                //    0    0    0    0    1    1
    "HLT_mu11_2mu4noL1_bNocut_L1MU11_2MU6",                          //    0    0    0    0    1    1
    "HLT_2mu4_bJpsimumu_Lxy0_L1BPH-2M9-2MU4_BPH-0DR15-2MU4"          //    0    0    0    0    0    1
  };

  m_HLT_isPassed = 0;
  for ( size_t i = 0; i < triggerName.size(); ++i ) {
    if ( std::find( trigger().begin(), trigger().end(), triggerName[ i ] ) != trigger().end() ) m_HLT_isPassed += ( 1 << i );
  }

}

Float_t triggerWeight() 
{

  static const std::vector< UInt_t > mask = { 0x3u, 0x7Cu, 0xFA0u, 0x1F000u, 0x3F000u };
  static const std::vector< std::vector< Dataset > > data = {
    {}, {}, {}, {}, {},                                            // should not be used for DATA
    { Dataset::kData15, Dataset::kData16p1, Dataset::kData16p2 },  // MC16a
    { Dataset::kData17 },                                          // MC16d
    { Dataset::kData18 }                                           // MC16e
  };

  static std::vector< std::vector< UInt_t >* > triggers( 5, nullptr );
  static std::vector< std::vector< Float_t >* > luminosity( 5, nullptr );

  static bool init = false;
  if ( !init ) {
    const std::vector< TString > name = { "data15", "data16p1", "data16p2", "data17", "data18" };
    TDirectory* currentDirectory = gDirectory->CurrentDirectory();
    std::string inputFileName = "BPhysTriggerWeights.DAT.root";
    TFile* inputFile = new TFile( inputFileName.c_str() );
    for ( std::size_t i = 0; i < triggers.size(); ++i ) {
      inputFile->GetObject( "trigger_" + name[ i ], triggers[ i ] );
      inputFile->GetObject( "luminosity_" + name[ i ], luminosity[ i ] );
    }
    inputFile->Close();
    delete inputFile;
    currentDirectory->cd();
    init = true;
  }

  Float_t weight = 0.;
  Float_t intLuminosity = 0.;
  for ( const auto& k : data[ static_cast< std::underlying_type< Dataset >::type >( dataset() ) ] ) {
    auto i = static_cast< std::underlying_type< Dataset >::type >( k );
    auto key = triggerBits() & mask[ i ];
    if ( key ) {
      auto p = std::equal_range( triggers[ i ]->begin(), triggers[ i ]->end(), key );
      auto idx = std::distance( triggers[ i ]->begin(), p.first );
      weight += luminosity[ i ]->at( idx );
    }
    intLuminosity += luminosity[ i ]->at( 0 );
  }
  weight /= intLuminosity;
  return weight;
}

