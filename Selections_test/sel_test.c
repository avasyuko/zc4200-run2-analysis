{
    TH1 *trk_pt_data = new TH1F("trk_pt_data", "trk_pt_data", 100, 1200., 5000.);
    TH1 *Bhad_pt_data = new TH1F("Bhad_pt_data", "Bhad_pt_data", 100, 10000., 50000.);
    TH1 *Bhad_chi2_data = new TH1F("Bhad_chi2_data", "Bhad_chi2_data", 100, 0., 3.5);
    TH1 *Lxy_data = new TH1F("Lxy_data", "Lxy_data", 100, 0.6, 3.);
    TH1 *Jpsi_chi2_data = new TH1F("Jpsi_chi2_data", "Jpsi_chi2_data", 100, 0., 10.);
    TH1 *m_Kpi_data = new TH1F("m_Kpi_data", "m_Kpi_data", 100, 1450., 3000.);
    TH1 *devision_data = new TH1F("devision_data", "devision_data", 100, 0., 1.);
    TH1 *Lxy_minA0_data = new TH1F("Lxy_minA0_data", "Lxy_minA0_data", 100, 0.6, 3.);
    TH1 *devision_minA0_data = new TH1F("devision_minA0_data", "devision_minA0_data", 100, 0., 1.);

    TH1 *trk_pt_data_percent = new TH1F("trk_pt_data_percent", "trk_pt_data_percent", 100, 1200., 5000.);
    TH1 *Bhad_pt_data_percent = new TH1F("Bhad_pt_data_percent", "Bhad_pt_data_percent", 100, 10000., 50000.);
    TH1 *Bhad_chi2_data_percent = new TH1F("Bhad_chi2_data_percent", "Bhad_chi2_data_percent", 100, 0., 3.5);
    TH1 *Lxy_data_percent = new TH1F("Lxy_data_percent", "Lxy_data_percent", 100, 0.6, 3.);
    TH1 *Jpsi_chi2_data_percent = new TH1F("Jpsi_chi2_data_percent", "Jpsi_chi2_data_percent", 100, 0., 10.);
    TH1 *m_Kpi_data_percent = new TH1F("m_Kpi_data_percent", "m_Kpi_data_percent", 100, 1450., 3000.);
    TH1 *devision_data_percent = new TH1F("devision_data_percent", "devision_data_percent", 100, 0., 1.);
    TH1 *Lxy_minA0_data_percent = new TH1F("Lxy_minA0_data_percent", "Lxy_minA0_data_percent", 100, 0.6, 3.);
    TH1 *devision_minA0_data_percent = new TH1F("devision_minA0_data_percent", "devision_minA0_data_percent", 100, 0., 1.);
     
    TH1 *trk_pt_data_w = new TH1F("trk_pt_data_w", "trk_pt_data_w", 100, 1200., 5000.);
    TH1 *Bhad_pt_data_w = new TH1F("Bhad_pt_data_w", "Bhad_pt_data_w", 100, 10000., 50000.);
    TH1 *Bhad_chi2_data_w = new TH1F("Bhad_chi2_data_w", "Bhad_chi2_data_w", 100, 0., 3.5);
    TH1 *Lxy_data_w = new TH1F("Lxy_data_w", "Lxy_data_w", 100, 0.6, 3.);
    TH1 *Jpsi_chi2_data_w = new TH1F("Jpsi_chi2_data_w", "Jpsi_chi2_data_w", 100, 0., 10.);
    TH1 *m_Kpi_data_w = new TH1F("m_Kpi_data_w", "m_Kpi_data_w", 100, 1450., 3000.);
    TH1 *devision_data_w = new TH1F("devision_data_w", "devision_data_w", 100, 0., 1.);
    TH1 *Lxy_minA0_data_w = new TH1F("Lxy_minA0_data_w", "Lxy_minA0_data_w", 100, 0.6, 3.);
    TH1 *devision_minA0_data_w = new TH1F("devision_minA0_data_w", "devision_minA0_data_w", 100, 0., 1.);

    TH1 *trk_pt_data_w_percent = new TH1F("trk_pt_data_w_percent", "trk_pt_data_w_percent", 100, 1200., 5000.);
    TH1 *Bhad_pt_data_w_percent = new TH1F("Bhad_pt_data_w_percent", "Bhad_pt_data_w_percent", 100, 10000., 50000.);
    TH1 *Bhad_chi2_data_w_percent = new TH1F("Bhad_chi2_data_w_percent", "Bhad_chi2_data_w_percent", 100, 0., 3.5);
    TH1 *Lxy_data_w_percent = new TH1F("Lxy_data_w_percent", "Lxy_data_w_percent", 100, 0.6, 3.);
    TH1 *Jpsi_chi2_data_w_percent = new TH1F("Jpsi_chi2_data_w_percent", "Jpsi_chi2_data_w_percent", 100, 0., 10.);
    TH1 *m_Kpi_data_w_percent = new TH1F("m_Kpi_data_w_percent", "m_Kpi_data_w_percent", 100, 1450., 3000.);
    TH1 *devision_data_w_percent = new TH1F("devision_data_w_percent", "devision_data_w_percent", 100, 0., 1.);
    TH1 *Lxy_minA0_data_w_percent = new TH1F("Lxy_minA0_data_w_percent", "Lxy_minA0_data_w_percent", 100, 0.6, 3.);
    TH1 *devision_minA0_data_w_percent = new TH1F("devision_minA0_data_w_percent", "devision_minA0_data_w_percent", 100, 0., 1.);

    TH1 *trk_pt_mc = new TH1F("trk_pt_mc", "trk_pt_mc", 100, 1200., 5000.);
    TH1 *Bhad_pt_mc = new TH1F("Bhad_pt_mc", "Bhad_pt_mc", 100, 10000., 50000.);
    TH1 *Bhad_chi2_mc = new TH1F("Bhad_chi2_mc", "Bhad_chi2_mc", 100, 0., 3.5);
    TH1 *Lxy_mc = new TH1F("Lxy_mc", "Lxy_mc", 100, 0.6, 3.);
    TH1 *Jpsi_chi2_mc = new TH1F("Jpsi_chi2_mc", "Jpsi_chi2_mc", 100, 0., 10.);
    TH1 *m_Kpi_mc = new TH1F("m_Kpi_mc", "m_Kpi_mc", 100, 1450., 3000.);
    TH1 *devision_mc = new TH1F("devision_mc", "devision_mc", 100, 0., 1.);
    TH1 *Lxy_minA0_mc = new TH1F("Lxy_minA0_mc", "Lxy_minA0_mc", 100, 0.6, 3.);
    TH1 *devision_minA0_mc = new TH1F("devision_minA0_mc", "devision_minA0_mc", 100, 0., 1.);

    TH1 *trk_pt_mc_percent = new TH1F("trk_pt_mc_percent", "trk_pt_mc_percent", 100, 1200., 5000.);
    TH1 *Bhad_pt_mc_percent = new TH1F("Bhad_pt_mc_percent", "Bhad_pt_mc_percent", 100, 10000., 50000.);
    TH1 *Bhad_chi2_mc_percent = new TH1F("Bhad_chi2_mc_percent", "Bhad_chi2_mc_percent", 100, 0., 3.5);
    TH1 *Lxy_mc_percent = new TH1F("Lxy_mc_percent", "Lxy_mc_percent", 100, 0.6, 3.);
    TH1 *Jpsi_chi2_mc_percent = new TH1F("Jpsi_chi2_mc_percent", "Jpsi_chi2_mc_percent", 100, 0., 10.);
    TH1 *m_Kpi_mc_percent = new TH1F("m_Kpi_mc_percent", "m_Kpi_mc_percent", 100, 1450., 3000.);
    TH1 *devision_mc_percent = new TH1F("devision_mc_percent", "devision_mc_percent", 100, 0., 1.);
    TH1 *Lxy_minA0_mc_percent = new TH1F("Lxy_minA0_mc_percent", "Lxy_minA0_mc_percent", 100, 0.6, 3.);
    TH1 *devision_minA0_mc_percent = new TH1F("devision_minA0_mc_percent", "devision_minA0_mc_percent", 100, 0., 1.);
    
    TH1 *trk_pt_mc_w = new TH1F("trk_pt_mc_w", "trk_pt_mc_w", 100, 1200., 5000.);
    TH1 *Bhad_pt_mc_w = new TH1F("Bhad_pt_mc_w", "Bhad_pt_mc_w", 100, 10000., 50000.);
    TH1 *Bhad_chi2_mc_w = new TH1F("Bhad_chi2_mc_w", "Bhad_chi2_mc_w", 100, 0., 3.5);
    TH1 *Lxy_mc_w = new TH1F("Lxy_mc_w", "Lxy_mc_w", 100, 0.6, 3.);
    TH1 *Jpsi_chi2_mc_w = new TH1F("Jpsi_chi2_mc_w", "Jpsi_chi2_mc_w", 100, 0., 10.);
    TH1 *m_Kpi_mc_w = new TH1F("m_Kpi_mc_w", "m_Kpi_mc_w", 100, 1450., 3000.);
    TH1 *devision_mc_w = new TH1F("devision_mc_w", "devision_mc_w", 100, 0., 1.);
    TH1 *Lxy_minA0_mc_w = new TH1F("Lxy_minA0_mc_w", "Lxy_minA0_mc_w", 100, 0.6, 3.);
    TH1 *devision_minA0_mc_w = new TH1F("devision_minA0_mc_w", "devision_minA0_mc_w", 100, 0., 1.);

    TH1 *trk_pt_mc_w_percent = new TH1F("trk_pt_mc_w_percent", "trk_pt_mc_w_percent", 100, 1200., 5000.);
    TH1 *Bhad_pt_mc_w_percent = new TH1F("Bhad_pt_mc_w_percent", "Bhad_pt_mc_w_percent", 100, 10000., 50000.);
    TH1 *Bhad_chi2_mc_w_percent = new TH1F("Bhad_chi2_mc_w_percent", "Bhad_chi2_mc_w_percent", 100, 0., 3.5);
    TH1 *Lxy_mc_w_percent = new TH1F("Lxy_mc_w_percent", "Lxy_mc_w_percent", 100, 0.6, 3.);
    TH1 *Jpsi_chi2_mc_w_percent = new TH1F("Jpsi_chi2_mc_w_percent", "Jpsi_chi2_mc_w_percent", 100, 0., 10.);
    TH1 *m_Kpi_mc_w_percent = new TH1F("m_Kpi_mc_w_percent", "m_Kpi_mc_w_percent", 100, 1450., 3000.);
    TH1 *devision_mc_w_percent = new TH1F("devision_mc_w_percent", "devision_mc_w_percent", 100, 0., 1.);
    TH1 *Lxy_minA0_mc_w_percent = new TH1F("Lxy_minA0_mc_w_percent", "Lxy_minA0_mc_w_percent", 100, 0.6, 3.);
    TH1 *devision_minA0_mc_w_percent = new TH1F("devision_minA0_mc_w_percent", "devision_minA0_mc_w_percent", 100, 0., 1.);

    float fn_sc = 0., fn_w = 0., fn_mc = 0., fn_mc_w = 0.;

    TChain stree18("stree");
    stree18.AddFile("../datasets/data15_slim.root");
    //stree18.AddFile("../datasets/data16Main_for_comb.root");
    //stree18.AddFile("../datasets/data16delayed_for_comb.root");
    //stree18.AddFile("../datasets/data17_for_comb.root");
    //stree18.AddFile("../datasets/data18_for_comb.root");
    
    Long64_t nentries18 = stree18.GetEntries();
    cout << "Number of runs data\t" << nentries18 << endl;
    float sB_mu1_px, sB_mu1_py, sB_mu1_pz, sB_mu2_px, sB_mu2_py, sB_mu2_pz, sB_trk1_px, sB_trk1_py, sB_trk1_pz, sB_trk2_px, sB_trk2_py, sB_trk2_pz, sB_trk1_charge, sB_trk2_charge, sB_Lxy_MaxSumPt, sB_Bs_pt, sB_mu1_pt, sB_mu2_pt, sB_trk1_pt, sB_trk2_pt, sB_Bs_chi2_ndof, sB_MaxSumPt, sB_mu1_eta, sB_mu2_eta, sB_trk1_eta, sB_trk2_eta, sB_Jpsi_mass, sB_Jpsi_chi2, sB_Lxy_MinA0, sB_MinA0SumPt;
    float w;
    
    stree18.SetBranchAddress("sB_Lxy_MinA0", &sB_Lxy_MinA0);
    stree18.SetBranchAddress("sB_MinA0SumPt", &sB_MinA0SumPt);
    stree18.SetBranchAddress("sB_mu1_px", &sB_mu1_px);
    stree18.SetBranchAddress("sB_mu1_py", &sB_mu1_py);
    stree18.SetBranchAddress("sB_mu1_pz", &sB_mu1_pz);
    stree18.SetBranchAddress("sB_mu2_px", &sB_mu2_px);
    stree18.SetBranchAddress("sB_mu2_py", &sB_mu2_py);
    stree18.SetBranchAddress("sB_mu2_pz", &sB_mu2_pz);
    stree18.SetBranchAddress("sB_trk1_px", &sB_trk1_px);
    stree18.SetBranchAddress("sB_trk1_py", &sB_trk1_py);
    stree18.SetBranchAddress("sB_trk1_pz", &sB_trk1_pz);
    stree18.SetBranchAddress("sB_trk2_px", &sB_trk2_px);
    stree18.SetBranchAddress("sB_trk2_py", &sB_trk2_py);
    stree18.SetBranchAddress("sB_trk2_pz", &sB_trk2_pz);
    stree18.SetBranchAddress("sB_trk1_charge", &sB_trk1_charge);
    stree18.SetBranchAddress("sB_trk2_charge", &sB_trk2_charge);
    stree18.SetBranchAddress("sB_Lxy_MaxSumPt", &sB_Lxy_MaxSumPt);
    stree18.SetBranchAddress("sB_Bs_pt", &sB_Bs_pt);
    stree18.SetBranchAddress("sB_mu1_pt", &sB_mu1_pt);
    stree18.SetBranchAddress("sB_mu2_pt", &sB_mu2_pt);
    stree18.SetBranchAddress("sB_trk1_pt", &sB_trk1_pt);
    stree18.SetBranchAddress("sB_trk2_pt", &sB_trk2_pt);
    stree18.SetBranchAddress("sB_Bs_chi2_ndof", &sB_Bs_chi2_ndof);
    stree18.SetBranchAddress("sB_MaxSumPt", &sB_MaxSumPt);
    stree18.SetBranchAddress("sB_mu1_eta", &sB_mu1_eta);
    stree18.SetBranchAddress("sB_mu2_eta", &sB_mu2_eta);
    stree18.SetBranchAddress("sB_trk1_eta", &sB_trk1_eta);
    stree18.SetBranchAddress("sB_trk2_eta", &sB_trk2_eta);
    stree18.SetBranchAddress("sB_Jpsi_mass", &sB_Jpsi_mass);
    stree18.SetBranchAddress("sB_Jpsi_chi2", &sB_Jpsi_chi2);
    for(int i = 1; i <= nentries18; i++)
    {
        stree18.GetEntry(i);
        TLorentzVector mu1, mu2, pi1, pi2, K1, K2, p1, p2, jpsi;
        w = 1.;
        mu1.SetXYZM(sB_mu1_px, sB_mu1_py, sB_mu1_pz, 105.65837);
        mu2.SetXYZM(sB_mu2_px, sB_mu2_py, sB_mu2_pz, 105.65837);
        pi1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 139.57);
        pi2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 139.57);
        K1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 493.677);
        K2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 493.677);
        p1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 938.272);
        p2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 938.272);
        jpsi.SetXYZM(sB_mu1_px + sB_mu2_px, sB_mu1_py + sB_mu2_py, sB_mu1_pz + sB_mu2_pz, 3096.);
        TLorentzVector bs = jpsi + pi1 + pi2;  
        if (sB_trk1_charge * sB_trk2_charge > 0) {w = -1;
        trk_pt_data->Fill(sB_trk1_pt);
        trk_pt_data->Fill(sB_trk2_pt);
        Bhad_pt_data->Fill(sB_Bs_pt);
        Bhad_chi2_data->Fill(sB_Bs_chi2_ndof);
        Lxy_data->Fill(sB_Lxy_MaxSumPt);
        Jpsi_chi2_data->Fill(sB_Jpsi_chi2);
        m_Kpi_data->Fill((pi1 + K2).M());
        m_Kpi_data->Fill((K1 + pi2).M());
        devision_data->Fill(sB_Bs_pt / sB_MaxSumPt);
        Lxy_minA0_data->Fill(sB_Lxy_MinA0);
        devision_minA0_data->Fill(sB_Bs_pt / sB_MinA0SumPt);
        fn_sc += 1.;}

        trk_pt_data_w->Fill(sB_trk1_pt, w);
        trk_pt_data_w->Fill(sB_trk2_pt, w);
        Bhad_pt_data_w->Fill(sB_Bs_pt, w);
        Bhad_chi2_data_w->Fill(sB_Bs_chi2_ndof, w);
        Lxy_data_w->Fill(sB_Lxy_MaxSumPt, w);
        Jpsi_chi2_data_w->Fill(sB_Jpsi_chi2, w);
        m_Kpi_data_w->Fill((pi1 + K2).M(), w);
        m_Kpi_data_w->Fill((K1 + pi2).M(), w);
        devision_data_w->Fill(sB_Bs_pt / sB_MaxSumPt, w);
        Lxy_minA0_data_w->Fill(sB_Lxy_MinA0, w);
        devision_minA0_data_w->Fill(sB_Bs_pt / sB_MinA0SumPt, w);
        fn_w += w;  
    }

    TFile *fmc=new TFile("../datasets/mc_BD_to_JPSI_K_PI_slim.root"); 
    TTree *streemc = (TTree*)fmc->Get("BsAllCandidates");
    Long64_t nentriesmc = streemc->GetEntries();
    cout << "Number of runs datamc\t" << nentriesmc << endl;
    
    streemc->SetBranchAddress("sB_Lxy_MinA0", &sB_Lxy_MinA0);
    streemc->SetBranchAddress("sB_MinA0SumPt", &sB_MinA0SumPt);    
    streemc->SetBranchAddress("sB_mu1_px", &sB_mu1_px);
    streemc->SetBranchAddress("sB_mu1_py", &sB_mu1_py);
    streemc->SetBranchAddress("sB_mu1_pz", &sB_mu1_pz);
    streemc->SetBranchAddress("sB_mu2_px", &sB_mu2_px);
    streemc->SetBranchAddress("sB_mu2_py", &sB_mu2_py);
    streemc->SetBranchAddress("sB_mu2_pz", &sB_mu2_pz);
    streemc->SetBranchAddress("sB_trk1_px", &sB_trk1_px);
    streemc->SetBranchAddress("sB_trk1_py", &sB_trk1_py);
    streemc->SetBranchAddress("sB_trk1_pz", &sB_trk1_pz);
    streemc->SetBranchAddress("sB_trk2_px", &sB_trk2_px);
    streemc->SetBranchAddress("sB_trk2_py", &sB_trk2_py);
    streemc->SetBranchAddress("sB_trk2_pz", &sB_trk2_pz);
    streemc->SetBranchAddress("sB_trk1_charge", &sB_trk1_charge);
    streemc->SetBranchAddress("sB_trk2_charge", &sB_trk2_charge);
    streemc->SetBranchAddress("sB_Lxy_MaxSumPt", &sB_Lxy_MaxSumPt);
    streemc->SetBranchAddress("sB_Bs_pt", &sB_Bs_pt);
    streemc->SetBranchAddress("sB_mu1_pt", &sB_mu1_pt);
    streemc->SetBranchAddress("sB_mu2_pt", &sB_mu2_pt);
    streemc->SetBranchAddress("sB_trk1_pt", &sB_trk1_pt);
    streemc->SetBranchAddress("sB_trk2_pt", &sB_trk2_pt);
    streemc->SetBranchAddress("sB_Bs_chi2_ndof", &sB_Bs_chi2_ndof);
    streemc->SetBranchAddress("sB_MaxSumPt", &sB_MaxSumPt);
    streemc->SetBranchAddress("sB_mu1_eta", &sB_mu1_eta);
    streemc->SetBranchAddress("sB_mu2_eta", &sB_mu2_eta);
    streemc->SetBranchAddress("sB_trk1_eta", &sB_trk1_eta);
    streemc->SetBranchAddress("sB_trk2_eta", &sB_trk2_eta);
    streemc->SetBranchAddress("sB_Jpsi_mass", &sB_Jpsi_mass);
    streemc->SetBranchAddress("sB_Jpsi_chi2", &sB_Jpsi_chi2);
    
    Float_t w_trigger, w_SF_mu1, w_SF_mu2;
    
    streemc->SetBranchAddress("sB_w_trigger", &w_trigger);
    streemc->SetBranchAddress("sB_w_SF_mu1", &w_SF_mu1);
    streemc->SetBranchAddress("sB_w_SF_mu2", &w_SF_mu2);
    for(int i = 1; i <= nentriesmc; i++)
    {
        streemc->GetEntry(i);
        TLorentzVector mu1, mu2, pi1, pi2, K1, K2, p1, p2, jpsi;
        w = w_trigger * w_SF_mu1 * w_SF_mu2;
        mu1.SetXYZM(sB_mu1_px, sB_mu1_py, sB_mu1_pz, 105.65837);
        mu2.SetXYZM(sB_mu2_px, sB_mu2_py, sB_mu2_pz, 105.65837);
        pi1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 139.57);
        pi2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 139.57);
        K1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 493.677);
        K2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 493.677);
        p1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 938.272);
        p2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 938.272);
        jpsi.SetXYZM(sB_mu1_px + sB_mu2_px, sB_mu1_py + sB_mu2_py, sB_mu1_pz + sB_mu2_pz, 3096.);
        TLorentzVector bs = jpsi + pi1 + pi2;
        trk_pt_mc->Fill(sB_trk1_pt);
        trk_pt_mc->Fill(sB_trk2_pt);
        Bhad_pt_mc->Fill(sB_Bs_pt);
        Bhad_chi2_mc->Fill(sB_Bs_chi2_ndof);
        Lxy_mc->Fill(sB_Lxy_MaxSumPt);
        Jpsi_chi2_mc->Fill(sB_Jpsi_chi2);
        m_Kpi_mc->Fill((pi1 + K2).M());
        m_Kpi_mc->Fill((K1 + pi2).M());
        devision_mc->Fill(sB_Bs_pt / sB_MaxSumPt);
        Lxy_minA0_mc->Fill(sB_Lxy_MinA0);
        devision_minA0_mc->Fill(sB_Bs_pt / sB_MinA0SumPt);
        
        
        trk_pt_mc_w->Fill(sB_trk1_pt, w);
        trk_pt_mc_w->Fill(sB_trk2_pt, w);
        Bhad_pt_mc_w->Fill(sB_Bs_pt, w);
        Bhad_chi2_mc_w->Fill(sB_Bs_chi2_ndof, w);
        Lxy_mc_w->Fill(sB_Lxy_MaxSumPt, w);
        Jpsi_chi2_mc_w->Fill(sB_Jpsi_chi2, w);
        m_Kpi_mc_w->Fill((pi1 + K2).M(), w);
        m_Kpi_mc_w->Fill((K1 + pi2).M(), w);
        devision_mc_w->Fill(sB_Bs_pt / sB_MaxSumPt, w);
        Lxy_minA0_mc_w->Fill(sB_Lxy_MinA0, w);
        devision_minA0_mc_w->Fill(sB_Bs_pt / sB_MinA0SumPt, w);
        
        fn_mc += 1;
        fn_mc_w += w;
    }

    float sum_data = 0., sum_data_w = 0., sum_mc = 0., sum_mc_w = 0.;    
    for(int i = 1; i <= trk_pt_data->GetNbinsX(); i++)
    {
        trk_pt_data_percent->SetBinContent(i, (2 * fn_sc - sum_data) / fn_sc / 2.);
        trk_pt_data_w_percent->SetBinContent(i, (2 * fn_w - sum_data_w) / fn_w / 2.);
        trk_pt_mc_percent->SetBinContent(i, (2 * fn_mc - sum_mc) / fn_mc / 2.);
        trk_pt_mc_w_percent->SetBinContent(i, (2 * fn_mc_w - sum_mc_w) / fn_mc_w / 2.);
        sum_data += trk_pt_data->GetBinContent(i);
        sum_data_w += trk_pt_data_w->GetBinContent(i);
        sum_mc += trk_pt_mc->GetBinContent(i);
        sum_mc_w += trk_pt_mc_w->GetBinContent(i);
    }

    sum_data = 0.; sum_data_w = 0.; sum_mc = 0., sum_mc_w = 0.;    
    for(int i = 1; i <= Bhad_pt_data->GetNbinsX(); i++)
    {
        Bhad_pt_data_percent->SetBinContent(i, (fn_sc - sum_data) / fn_sc);
        Bhad_pt_data_w_percent->SetBinContent(i, (fn_w - sum_data_w) / fn_w);
        Bhad_pt_mc_percent->SetBinContent(i, (fn_mc - sum_mc) / fn_mc);
        Bhad_pt_mc_w_percent->SetBinContent(i, (fn_mc_w - sum_mc_w) / fn_mc_w);
        sum_data += Bhad_pt_data->GetBinContent(i);
        sum_data_w += Bhad_pt_data_w->GetBinContent(i);
        sum_mc += Bhad_pt_mc->GetBinContent(i);
        sum_mc_w += Bhad_pt_mc_w->GetBinContent(i);
    }

    sum_data = 0.; sum_data_w = 0.; sum_mc = 0., sum_mc_w = 0.;    
    for(int i = 1; i <= Lxy_data->GetNbinsX(); i++)
    {
        Lxy_data_percent->SetBinContent(i, (fn_sc - sum_data) / fn_sc);
        Lxy_data_w_percent->SetBinContent(i, (fn_w - sum_data_w) / fn_w);
        Lxy_mc_percent->SetBinContent(i, (fn_mc - sum_mc) / fn_mc);
        Lxy_mc_w_percent->SetBinContent(i, (fn_mc_w - sum_mc_w) / fn_mc_w);
        sum_data += Lxy_data->GetBinContent(i);
        sum_data_w += Lxy_data_w->GetBinContent(i);
        sum_mc += Lxy_mc->GetBinContent(i);
        sum_mc_w += Lxy_mc_w->GetBinContent(i);
    }
    
    sum_data = 0.; sum_data_w = 0.; sum_mc = 0., sum_mc_w = 0.;    
    for(int i = 1; i <= Lxy_minA0_data->GetNbinsX(); i++)
    {
        Lxy_minA0_data_percent->SetBinContent(i, (fn_sc - sum_data) / fn_sc);
        Lxy_minA0_data_w_percent->SetBinContent(i, (fn_w - sum_data_w) / fn_w);
        Lxy_minA0_mc_percent->SetBinContent(i, (fn_mc - sum_mc) / fn_mc);
        Lxy_minA0_mc_w_percent->SetBinContent(i, (fn_mc_w - sum_mc_w) / fn_mc_w);
        sum_data += Lxy_minA0_data->GetBinContent(i);
        sum_data_w += Lxy_minA0_data_w->GetBinContent(i);
        sum_mc += Lxy_minA0_mc->GetBinContent(i);
        sum_mc_w += Lxy_minA0_mc_w->GetBinContent(i);
    }

    sum_data = 0.; sum_data_w = 0.; sum_mc = 0., sum_mc_w = 0.;    
    for(int i = 1; i <= devision_minA0_data->GetNbinsX(); i++)
    {
        devision_minA0_data_percent->SetBinContent(i, (fn_sc - sum_data) / fn_sc);
        devision_minA0_data_w_percent->SetBinContent(i, (fn_w - sum_data_w) / fn_w);
        devision_minA0_mc_percent->SetBinContent(i, (fn_mc - sum_mc) / fn_mc);
        devision_minA0_mc_w_percent->SetBinContent(i, (fn_mc_w - sum_mc_w) / fn_mc_w);
        sum_data += devision_minA0_data->GetBinContent(i);
        sum_data_w += devision_minA0_data_w->GetBinContent(i);
        sum_mc += devision_minA0_mc->GetBinContent(i);
        sum_mc_w += devision_minA0_mc_w->GetBinContent(i);
    }
    
    sum_data = 0.; sum_data_w = 0.; sum_mc = 0., sum_mc_w = 0.;    
    for(int i = 1; i <= devision_data->GetNbinsX(); i++)
    {
        devision_data_percent->SetBinContent(i, (fn_sc - sum_data) / fn_sc);
        devision_data_w_percent->SetBinContent(i, (fn_w - sum_data_w) / fn_w);
        devision_mc_percent->SetBinContent(i, (fn_mc - sum_mc) / fn_mc);
        devision_mc_w_percent->SetBinContent(i, (fn_mc_w - sum_mc_w) / fn_mc_w);
        sum_data += devision_data->GetBinContent(i);
        sum_data_w += devision_data_w->GetBinContent(i);
        sum_mc += devision_mc->GetBinContent(i);
        sum_mc_w += devision_mc_w->GetBinContent(i);
    }

    sum_data = 0.; sum_data_w = 0.; sum_mc = 0., sum_mc_w = 0.;    
    for(int i = 1; i <= Bhad_chi2_data->GetNbinsX(); i++)
    {
        Bhad_chi2_data_percent->SetBinContent(i, sum_data / fn_sc);
        Bhad_chi2_data_w_percent->SetBinContent(i, sum_data_w / fn_w);
        Bhad_chi2_mc_percent->SetBinContent(i, sum_mc / fn_mc);
        Bhad_chi2_mc_w_percent->SetBinContent(i, sum_mc_w / fn_mc_w);
        sum_data += Bhad_chi2_data->GetBinContent(i);
        sum_data_w += Bhad_chi2_data_w->GetBinContent(i);
        sum_mc += Bhad_chi2_mc->GetBinContent(i);
        sum_mc_w += Bhad_chi2_mc_w->GetBinContent(i);
    }

    sum_data = 0.; sum_data_w = 0.; sum_mc = 0., sum_mc_w = 0.;    
    for(int i = 1; i <= Jpsi_chi2_data->GetNbinsX(); i++)
    {
        Jpsi_chi2_data_percent->SetBinContent(i, sum_data / fn_sc);
        Jpsi_chi2_data_w_percent->SetBinContent(i, sum_data_w / fn_w);
        Jpsi_chi2_mc_percent->SetBinContent(i, sum_mc / fn_mc);
        Jpsi_chi2_mc_w_percent->SetBinContent(i, sum_mc_w / fn_mc_w);
        sum_data += Jpsi_chi2_data->GetBinContent(i);
        sum_data_w += Jpsi_chi2_data_w->GetBinContent(i);
        sum_mc += Jpsi_chi2_mc->GetBinContent(i);
        sum_mc_w += Jpsi_chi2_mc_w->GetBinContent(i);
    }

    trk_pt_data->Scale(1. / trk_pt_data->Integral());
    Bhad_pt_data->Scale(1. / Bhad_pt_data->Integral());
    Bhad_chi2_data->Scale(1. / Bhad_chi2_data->Integral());
    Lxy_data->Scale(1. / Lxy_data->Integral());
    Jpsi_chi2_data->Scale(1. / Jpsi_chi2_data->Integral());
    m_Kpi_data->Scale(1. / m_Kpi_data->Integral());
    devision_data->Scale(1. / devision_data->Integral());
    Lxy_minA0_data->Scale(1. / Lxy_minA0_data->Integral());
    devision_minA0_data->Scale(1. / devision_minA0_data->Integral());

    trk_pt_data_w->Scale(1. / trk_pt_data_w->Integral());
    Bhad_pt_data_w->Scale(1. / Bhad_pt_data_w->Integral());
    Bhad_chi2_data_w->Scale(1. / Bhad_chi2_data_w->Integral());
    Lxy_data_w->Scale(1. / Lxy_data_w->Integral());
    Jpsi_chi2_data_w->Scale(1. / Jpsi_chi2_data_w->Integral());
    m_Kpi_data_w->Scale(1. / m_Kpi_data_w->Integral());
    devision_data_w->Scale(1. / devision_data_w->Integral());
    Lxy_minA0_data_w->Scale(1. / Lxy_minA0_data_w->Integral());
    devision_minA0_data_w->Scale(1. / devision_minA0_data_w->Integral());

    trk_pt_mc->Scale(1. / trk_pt_mc->Integral());
    Bhad_pt_mc->Scale(1. / Bhad_pt_mc->Integral());
    Bhad_chi2_mc->Scale(1. / Bhad_chi2_mc->Integral());
    Lxy_mc->Scale(1. / Lxy_mc->Integral());
    Jpsi_chi2_mc->Scale(1. / Jpsi_chi2_mc->Integral());
    m_Kpi_mc->Scale(1. / m_Kpi_mc->Integral());
    devision_mc->Scale(1. / devision_mc->Integral());
    Lxy_minA0_mc->Scale(1. / Lxy_minA0_mc->Integral());
    devision_minA0_mc->Scale(1. / devision_minA0_mc->Integral());
    
    trk_pt_mc_w->Scale(1. / trk_pt_mc_w->Integral());
    Bhad_pt_mc_w->Scale(1. / Bhad_pt_mc_w->Integral());
    Bhad_chi2_mc_w->Scale(1. / Bhad_chi2_mc_w->Integral());
    Lxy_mc_w->Scale(1. / Lxy_mc_w->Integral());
    Jpsi_chi2_mc_w->Scale(1. / Jpsi_chi2_mc_w->Integral());
    m_Kpi_mc_w->Scale(1. / m_Kpi_mc_w->Integral());
    devision_mc_w->Scale(1. / devision_mc_w->Integral());
    Lxy_minA0_mc_w->Scale(1. / Lxy_minA0_mc_w->Integral());
    devision_minA0_mc_w->Scale(1. / devision_minA0_mc_w->Integral());

    TCanvas *c_trk_pt = new TCanvas("c_trk_pt");
    trk_pt_data->SetLineWidth(2.);
    trk_pt_data->SetStats(kFALSE);
    trk_pt_data->GetXaxis()->SetTitle("P_{T}(had), MeV");
    trk_pt_data->SetTitle("");
    trk_pt_data->Draw("hist");
    trk_pt_data_w->SetLineColor(kRed);
    trk_pt_data_w->SetLineWidth(2.);
    trk_pt_data_w->Draw("hist same");
    trk_pt_mc->SetLineColor(kGreen);
    trk_pt_mc->SetFillStyle(3353);
    trk_pt_mc->SetFillColor(kGreen);
    trk_pt_mc->Draw("hist same");
    trk_pt_mc_w->SetLineColor(6);
    trk_pt_mc_w->SetFillStyle(3335);
    trk_pt_mc_w->SetFillColor(6);
    trk_pt_mc_w->Draw("hist same");
    TLegend *l_trk_pt = new TLegend(.75, .80, .95, .95);
    l_trk_pt->AddEntry(trk_pt_data, "data same charge");
    l_trk_pt->AddEntry(trk_pt_data_w, "data after substraction");
    l_trk_pt->AddEntry(trk_pt_mc, "B_d -> J/#psi K #pi (mc)");
    l_trk_pt->AddEntry(trk_pt_mc_w, "mc weighted");
    l_trk_pt->Draw("same");
    c_trk_pt->SaveAs("trk_pt.png");

    TCanvas *c_Bhad_pt = new TCanvas("c_Bhad_pt");
    Bhad_pt_data->SetLineWidth(2.);
    Bhad_pt_data->SetStats(kFALSE);
    Bhad_pt_data->GetXaxis()->SetTitle("P_{T}(B-had), MeV");
    Bhad_pt_data->SetTitle("");
    Bhad_pt_data->Draw("hist");
    Bhad_pt_data_w->SetLineColor(kRed);
    Bhad_pt_data_w->SetLineWidth(2.);
    Bhad_pt_data_w->Draw("hist same");
    Bhad_pt_mc->SetLineColor(kGreen);
    Bhad_pt_mc->SetFillStyle(3353);
    Bhad_pt_mc->SetFillColor(kGreen);
    Bhad_pt_mc->Draw("hist same");
    Bhad_pt_mc_w->SetLineColor(6);
    Bhad_pt_mc_w->SetFillStyle(3335);
    Bhad_pt_mc_w->SetFillColor(6);
    Bhad_pt_mc_w->Draw("hist same");
    TLegend *l_Bhad_pt = new TLegend(.75, .80, .95, .95);
    l_Bhad_pt->AddEntry(Bhad_pt_data, "data same charge");
    l_Bhad_pt->AddEntry(Bhad_pt_data_w, "data after substraction");
    l_Bhad_pt->AddEntry(Bhad_pt_mc, "B_d -> J/#psi K #pi (mc)");
    l_Bhad_pt->AddEntry(Bhad_pt_mc_w, "mc weighted");
    l_Bhad_pt->Draw("same");
    c_Bhad_pt->SaveAs("Bhad_pt.png");

    TCanvas *c_Bhad_chi2 = new TCanvas("c_Bhad_chi2");
    Bhad_chi2_data->SetLineWidth(2.);
    Bhad_chi2_data->SetStats(kFALSE);
    Bhad_chi2_data->GetYaxis()->SetRangeUser(0., 0.04);
    Bhad_chi2_data->GetXaxis()->SetTitle("#chi^{2}(B-had) / ndof");
    Bhad_chi2_data->SetTitle("");
    Bhad_chi2_data->Draw("hist");
    Bhad_chi2_data_w->SetLineColor(kRed);
    Bhad_chi2_data_w->SetLineWidth(2.);
    Bhad_chi2_data_w->Draw("hist same");
    Bhad_chi2_mc->SetLineColor(kGreen);
    Bhad_chi2_mc->SetFillStyle(3353);
    Bhad_chi2_mc->SetFillColor(kGreen);
    Bhad_chi2_mc->Draw("hist same");
    Bhad_chi2_mc_w->SetLineColor(6);
    Bhad_chi2_mc_w->SetFillStyle(3335);
    Bhad_chi2_mc_w->SetFillColor(6);
    Bhad_chi2_mc_w->Draw("hist same");
    TLegend *l_Bhad_chi2 = new TLegend(.75, .80, .95, .95);
    l_Bhad_chi2->AddEntry(Bhad_chi2_data, "data same charge");
    l_Bhad_chi2->AddEntry(Bhad_chi2_data_w, "data after substraction");
    l_Bhad_chi2->AddEntry(Bhad_chi2_mc, "B_d -> J/#psi K #pi (mc)");
    l_Bhad_chi2->AddEntry(Bhad_chi2_mc_w, "mc weighted");
    l_Bhad_chi2->Draw("same");
    c_Bhad_chi2->SaveAs("Bhad_chi2.png");

    TCanvas *c_Lxy = new TCanvas("c_Lxy");
    Lxy_data->SetLineWidth(2.);
    Lxy_data->SetStats(kFALSE);
    Lxy_data->GetXaxis()->SetTitle("L_{xy}, mm");
    Lxy_data->SetTitle("");
    Lxy_data->Draw("hist");
    Lxy_data_w->SetLineColor(kRed);
    Lxy_data_w->SetLineWidth(2.);
    Lxy_data_w->Draw("hist same");
    Lxy_mc->SetLineColor(kGreen);
    Lxy_mc->SetFillStyle(3353);
    Lxy_mc->SetFillColor(kGreen);
    Lxy_mc->Draw("hist same");
    Lxy_mc_w->SetLineColor(6);
    Lxy_mc_w->SetFillStyle(3335);
    Lxy_mc_w->SetFillColor(6);
    Lxy_mc_w->Draw("hist same");
    TLegend *l_Lxy = new TLegend(.75, .80, .95, .95);
    l_Lxy->AddEntry(Lxy_data, "data same charge");
    l_Lxy->AddEntry(Lxy_data_w, "data after substraction");
    l_Lxy->AddEntry(Lxy_mc, "B_d -> J/#psi K #pi (mc)");
    l_Lxy->AddEntry(Lxy_mc_w, "mc weighted");
    l_Lxy->Draw("same");
    c_Lxy->SaveAs("Lxy.png");
    
    TCanvas *c_Lxy_minA0 = new TCanvas("c_Lxy_minA0");
    Lxy_minA0_data->SetLineWidth(2.);
    Lxy_minA0_data->SetStats(kFALSE);
    Lxy_minA0_data->GetXaxis()->SetTitle("L_{xy}, mm");
    Lxy_minA0_data->SetTitle("");
    Lxy_minA0_data->Draw("hist");
    Lxy_minA0_data_w->SetLineColor(kRed);
    Lxy_minA0_data_w->SetLineWidth(2.);
    Lxy_minA0_data_w->Draw("hist same");
    Lxy_minA0_mc->SetLineColor(kGreen);
    Lxy_minA0_mc->SetFillStyle(3353);
    Lxy_minA0_mc->SetFillColor(kGreen);
    Lxy_minA0_mc->Draw("hist same");
    Lxy_minA0_mc_w->SetLineColor(6);
    Lxy_minA0_mc_w->SetFillStyle(3335);
    Lxy_minA0_mc_w->SetFillColor(6);
    Lxy_minA0_mc_w->Draw("hist same");
    TLegend *l_Lxy_minA0 = new TLegend(.75, .80, .95, .95);
    l_Lxy_minA0->AddEntry(Lxy_minA0_data, "data same charge");
    l_Lxy_minA0->AddEntry(Lxy_minA0_data_w, "data after substraction");
    l_Lxy_minA0->AddEntry(Lxy_minA0_mc, "B_d -> J/#psi K #pi (mc)");
    l_Lxy_minA0->AddEntry(Lxy_minA0_mc_w, "mc weighted");
    l_Lxy_minA0->Draw("same");
    c_Lxy_minA0->SaveAs("Lxy_minA0.png");

    TCanvas *c_Jpsi_chi2 = new TCanvas("c_Jpsi_chi2");
    Jpsi_chi2_data->SetLineWidth(2.);
    Jpsi_chi2_data->SetStats(kFALSE);
    Jpsi_chi2_data->GetYaxis()->SetRangeUser(0., 0.3);
    Jpsi_chi2_data->GetXaxis()->SetTitle("#chi^{2}(J/#psi) / ndof");
    Jpsi_chi2_data->SetTitle("");
    Jpsi_chi2_data->Draw("hist");
    Jpsi_chi2_data_w->SetLineColor(kRed);
    Jpsi_chi2_data_w->SetLineWidth(2.);
    Jpsi_chi2_data_w->Draw("hist same");
    Jpsi_chi2_mc->SetLineColor(kGreen);
    Jpsi_chi2_mc->SetFillStyle(3353);
    Jpsi_chi2_mc->SetFillColor(kGreen);
    Jpsi_chi2_mc->Draw("hist same");
    Jpsi_chi2_mc_w->SetLineColor(6);
    Jpsi_chi2_mc_w->SetFillStyle(3335);
    Jpsi_chi2_mc_w->SetFillColor(6);
    Jpsi_chi2_mc_w->Draw("hist same");
    TLegend *l_Jpsi_chi2 = new TLegend(.75, .80, .95, .95);
    l_Jpsi_chi2->AddEntry(Jpsi_chi2_data, "data same charge");
    l_Jpsi_chi2->AddEntry(Jpsi_chi2_data_w, "data after substraction");
    l_Jpsi_chi2->AddEntry(Jpsi_chi2_mc, "B_d -> J/#psi K #pi (mc)");
    l_Jpsi_chi2->AddEntry(Jpsi_chi2_mc_w, "mc weighted");
    l_Jpsi_chi2->Draw("same");
    c_Jpsi_chi2->SaveAs("Jpsi_chi2.png");

    TCanvas *c_m_Kpi = new TCanvas("c_m_Kpi");
    m_Kpi_data->SetLineWidth(2.);
    m_Kpi_data->SetStats(kFALSE);
    m_Kpi_data->GetXaxis()->SetTitle("m(K#pi), MeV");
    m_Kpi_data->SetTitle("");
    m_Kpi_data->Draw("hist");
    m_Kpi_data_w->SetLineColor(kRed);
    m_Kpi_data_w->SetLineWidth(2.);
    m_Kpi_data_w->Draw("hist same");
    m_Kpi_mc->SetLineColor(kGreen);
    m_Kpi_mc->SetFillStyle(3353);
    m_Kpi_mc->SetFillColor(kGreen);
    m_Kpi_mc->Draw("hist same");
    m_Kpi_mc_w->SetLineColor(6);
    m_Kpi_mc_w->SetFillStyle(3335);
    m_Kpi_mc_w->SetFillColor(6);
    m_Kpi_mc_w->Draw("hist same");
    TLegend *l_m_Kpi = new TLegend(.75, .80, .95, .95);
    l_m_Kpi->AddEntry(m_Kpi_data, "data same charge");
    l_m_Kpi->AddEntry(m_Kpi_data_w, "data after substraction");
    l_m_Kpi->AddEntry(m_Kpi_mc, "B_d -> J/#psi K #pi (mc)");
    l_m_Kpi->AddEntry(m_Kpi_mc_w, "mc weighted");
    l_m_Kpi->Draw("same");
    c_m_Kpi->SaveAs("m_Kpi.png");

    TCanvas *c_devision = new TCanvas("c_devision");
    devision_data->SetLineWidth(2.);
    devision_data->SetStats(kFALSE);
    devision_data->GetXaxis()->SetTitle("P_{T}(B-had) / P_{T}(primary vertex)");
    devision_data->SetTitle("");
    devision_data->Draw("hist");
    devision_data_w->SetLineColor(kRed);
    devision_data_w->SetLineWidth(2.);
    devision_data_w->Draw("hist same");
    devision_mc->SetLineColor(kGreen);
    devision_mc->SetFillStyle(3353);
    devision_mc->SetFillColor(kGreen);
    devision_mc->Draw("hist same");
    devision_mc_w->SetLineColor(6);
    devision_mc_w->SetFillStyle(3335);
    devision_mc_w->SetFillColor(6);
    devision_mc_w->Draw("hist same");
    TLegend *l_devision = new TLegend(.75, .80, .95, .95);
    l_devision->AddEntry(devision_data, "data same charge");
    l_devision->AddEntry(devision_data_w, "data after substraction");
    l_devision->AddEntry(devision_mc, "B_d -> J/#psi K #pi (mc)");
    l_devision->AddEntry(devision_mc_w, "mc weighted");
    l_devision->Draw("same");
    c_devision->SaveAs("devision.png");
    
    TCanvas *c_devision_minA0 = new TCanvas("c_devision_minA0");
    devision_minA0_data->SetLineWidth(2.);
    devision_minA0_data->SetStats(kFALSE);
    devision_minA0_data->GetXaxis()->SetTitle("P_{T}(B-had) / P_{T}(primary vertex)");
    devision_minA0_data->SetTitle("");
    devision_minA0_data->Draw("hist");
    devision_minA0_data_w->SetLineColor(kRed);
    devision_minA0_data_w->SetLineWidth(2.);
    devision_minA0_data_w->Draw("hist same");
    devision_minA0_mc->SetLineColor(kGreen);
    devision_minA0_mc->SetFillStyle(3353);
    devision_minA0_mc->SetFillColor(kGreen);
    devision_minA0_mc->Draw("hist same");
    devision_minA0_mc_w->SetLineColor(6);
    devision_minA0_mc_w->SetFillStyle(3335);
    devision_minA0_mc_w->SetFillColor(6);
    devision_minA0_mc_w->Draw("hist same");
    TLegend *l_devision_minA0 = new TLegend(.75, .80, .95, .95);
    l_devision_minA0->AddEntry(devision_minA0_data, "data same charge");
    l_devision_minA0->AddEntry(devision_minA0_data_w, "data after substraction");
    l_devision_minA0->AddEntry(devision_minA0_mc, "B_d -> J/#psi K #pi (mc)");
    l_devision_minA0->AddEntry(devision_minA0_mc_w, "mc weighted");
    l_devision_minA0->Draw("same");
    c_devision_minA0->SaveAs("devision_minA0.png");

    TCanvas *c_trk_pt_percent = new TCanvas("c_trk_pt_percent");
    trk_pt_data_percent->SetLineWidth(2.);
    trk_pt_data_percent->GetYaxis()->SetRangeUser(0., 1.);
    trk_pt_data_w_percent->SetLineWidth(2.);
    trk_pt_data_w_percent->SetLineColor(kRed);
    trk_pt_mc_percent->SetLineWidth(2.);
    trk_pt_mc_percent->SetLineColor(kGreen);
    trk_pt_data_percent->SetStats(kFALSE);
    trk_pt_data_percent->GetXaxis()->SetTitle("P_{T}(had), MeV");
    trk_pt_data_percent->SetTitle("");
    trk_pt_data_percent->Draw("hist");
    trk_pt_data_w_percent->Draw("hist same");
    trk_pt_mc_percent->Draw("hist same");
    trk_pt_mc_w_percent->SetLineWidth(2.);
    trk_pt_mc_w_percent->SetLineColor(6);
    trk_pt_mc_w_percent->Draw("hist same");
    TLegend *l_trk_pt_percent = new TLegend(.75, .80, .95, .95);
    l_trk_pt_percent->AddEntry(trk_pt_data_percent, "data same charge");
    l_trk_pt_percent->AddEntry(trk_pt_data_w_percent, "data after substraction");
    l_trk_pt_percent->AddEntry(trk_pt_mc_percent, "B_d -> J/#psi K #pi (mc)");
    l_trk_pt_percent->AddEntry(trk_pt_mc_w_percent, "mc weighted");
    l_trk_pt_percent->Draw("same");
    c_trk_pt_percent->SaveAs("trk_pt_percent.png");

    TCanvas *c_Bhad_pt_percent = new TCanvas("c_Bhad_pt_percent");
    Bhad_pt_data_percent->SetLineWidth(2.);
    Bhad_pt_data_percent->GetYaxis()->SetRangeUser(0., 1.);
    Bhad_pt_data_w_percent->SetLineWidth(2.);
    Bhad_pt_data_w_percent->SetLineColor(kRed);
    Bhad_pt_mc_percent->SetLineWidth(2.);
    Bhad_pt_mc_percent->SetLineColor(kGreen);
    Bhad_pt_data_percent->SetStats(kFALSE);
    Bhad_pt_data_percent->GetXaxis()->SetTitle("P_{T}(B-had), MeV");
    Bhad_pt_data_percent->SetTitle("");
    Bhad_pt_data_percent->Draw("hist");
    Bhad_pt_data_w_percent->Draw("hist same");
    Bhad_pt_mc_percent->Draw("hist same");
    Bhad_pt_mc_w_percent->SetLineWidth(2.);
    Bhad_pt_mc_w_percent->SetLineColor(6);
    Bhad_pt_mc_w_percent->Draw("hist same");
    TLegend *l_Bhad_pt_percent = new TLegend(.75, .80, .95, .95);
    l_Bhad_pt_percent->AddEntry(Bhad_pt_data_percent, "data same charge");
    l_Bhad_pt_percent->AddEntry(Bhad_pt_data_w_percent, "data after substraction");
    l_Bhad_pt_percent->AddEntry(Bhad_pt_mc_percent, "B_d -> J/#psi K #pi (mc)");
    l_Bhad_pt_percent->AddEntry(Bhad_pt_mc_w_percent, "mc weighted");
    l_Bhad_pt_percent->Draw("same");
    c_Bhad_pt_percent->SaveAs("Bhad_pt_percent.png");

    TCanvas *c_Bhad_chi2_percent = new TCanvas("c_Bhad_chi2_percent");
    Bhad_chi2_data_percent->SetLineWidth(2.);
    Bhad_chi2_data_percent->GetYaxis()->SetRangeUser(0., 1.);
    Bhad_chi2_data_w_percent->SetLineWidth(2.);
    Bhad_chi2_data_w_percent->SetLineColor(kRed);
    Bhad_chi2_mc_percent->SetLineWidth(2.);
    Bhad_chi2_mc_percent->SetLineColor(kGreen);
    Bhad_chi2_data_percent->SetStats(kFALSE);
    Bhad_chi2_data_percent->GetXaxis()->SetTitle("#chi^{2}(B-had) / ndof");
    Bhad_chi2_data_percent->SetTitle("");
    Bhad_chi2_data_percent->Draw("hist");
    Bhad_chi2_data_w_percent->Draw("hist same");
    Bhad_chi2_mc_percent->Draw("hist same");
    Bhad_chi2_mc_w_percent->SetLineWidth(2.);
    Bhad_chi2_mc_w_percent->SetLineColor(6);
    Bhad_chi2_mc_w_percent->Draw("hist same");
    TLegend *l_Bhad_chi2_percent = new TLegend(.75, .80, .95, .95);
    l_Bhad_chi2_percent->AddEntry(Bhad_chi2_data_percent, "data same charge");
    l_Bhad_chi2_percent->AddEntry(Bhad_chi2_data_w_percent, "data after substraction");
    l_Bhad_chi2_percent->AddEntry(Bhad_chi2_mc_percent, "B_d -> J/#psi K #pi (mc)");
    l_Bhad_chi2_percent->AddEntry(Bhad_chi2_mc_w_percent, "mc weighted");
    l_Bhad_chi2_percent->Draw("same");
    c_Bhad_chi2_percent->SaveAs("Bhad_chi2_percent.png");

    TCanvas *c_Lxy_percent = new TCanvas("c_Lxy_percent");
    Lxy_data_percent->SetLineWidth(2.);
    Lxy_data_percent->GetYaxis()->SetRangeUser(0., 1.);
    Lxy_data_w_percent->SetLineWidth(2.);
    Lxy_data_w_percent->SetLineColor(kRed);
    Lxy_mc_percent->SetLineWidth(2.);
    Lxy_mc_percent->SetLineColor(kGreen);
    Lxy_data_percent->SetStats(kFALSE);
    Lxy_data_percent->GetXaxis()->SetTitle("L_{xy}, mm");
    Lxy_data_percent->SetTitle("");
    Lxy_data_percent->Draw("hist");
    Lxy_data_w_percent->Draw("hist same");
    Lxy_mc_percent->Draw("hist same");
    Lxy_mc_w_percent->SetLineWidth(2.);
    Lxy_mc_w_percent->SetLineColor(6);
    Lxy_mc_w_percent->Draw("hist same");
    TLegend *l_Lxy_percent = new TLegend(.75, .80, .95, .95);
    l_Lxy_percent->AddEntry(Lxy_data_percent, "data same charge");
    l_Lxy_percent->AddEntry(Lxy_data_w_percent, "data after substraction");
    l_Lxy_percent->AddEntry(Lxy_mc_percent, "B_d -> J/#psi K #pi (mc)");
    l_Lxy_percent->AddEntry(Lxy_mc_w_percent, "mc weighted");
    l_Lxy_percent->Draw("same");
    c_Lxy_percent->SaveAs("Lxy_percent.png");
    
    TCanvas *c_Lxy_minA0_percent = new TCanvas("c_Lxy_minA0_percent");
    Lxy_minA0_data_percent->SetLineWidth(2.);
    Lxy_minA0_data_percent->GetYaxis()->SetRangeUser(0., 1.);
    Lxy_minA0_data_w_percent->SetLineWidth(2.);
    Lxy_minA0_data_w_percent->SetLineColor(kRed);
    Lxy_minA0_mc_percent->SetLineWidth(2.);
    Lxy_minA0_mc_percent->SetLineColor(kGreen);
    Lxy_minA0_data_percent->SetStats(kFALSE);
    Lxy_minA0_data_percent->GetXaxis()->SetTitle("L_{xy}, mm");
    Lxy_minA0_data_percent->SetTitle("");
    Lxy_minA0_data_percent->Draw("hist");
    Lxy_minA0_data_w_percent->Draw("hist same");
    Lxy_minA0_mc_percent->Draw("hist same");
    Lxy_minA0_mc_w_percent->SetLineWidth(2.);
    Lxy_minA0_mc_w_percent->SetLineColor(6);
    Lxy_minA0_mc_w_percent->Draw("hist same");
    TLegend *l_Lxy_minA0_percent = new TLegend(.75, .80, .95, .95);
    l_Lxy_minA0_percent->AddEntry(Lxy_minA0_data_percent, "data same charge");
    l_Lxy_minA0_percent->AddEntry(Lxy_minA0_data_w_percent, "data after substraction");
    l_Lxy_minA0_percent->AddEntry(Lxy_minA0_mc_percent, "B_d -> J/#psi K #pi (mc)");
    l_Lxy_minA0_percent->AddEntry(Lxy_minA0_mc_w_percent, "mc weighted");
    l_Lxy_minA0_percent->Draw("same");
    c_Lxy_minA0_percent->SaveAs("Lxy_minA0_percent.png");

    TCanvas *c_Jpsi_chi2_percent = new TCanvas("c_Jpsi_chi2_percent");
    Jpsi_chi2_data_percent->SetLineWidth(2.);
    Jpsi_chi2_data_percent->GetYaxis()->SetRangeUser(0., 1.);
    Jpsi_chi2_data_w_percent->SetLineWidth(2.);
    Jpsi_chi2_data_w_percent->SetLineColor(kRed);
    Jpsi_chi2_mc_percent->SetLineWidth(2.);
    Jpsi_chi2_mc_percent->SetLineColor(kGreen);
    Jpsi_chi2_data_percent->SetStats(kFALSE);
    Jpsi_chi2_data_percent->GetXaxis()->SetTitle("#chi^{2}(J/#psi) / ndof");
    Jpsi_chi2_data_percent->SetTitle("");
    Jpsi_chi2_data_percent->Draw("hist");
    Jpsi_chi2_data_w_percent->Draw("hist same");
    Jpsi_chi2_mc_percent->Draw("hist same");
    Jpsi_chi2_mc_w_percent->SetLineWidth(2.);
    Jpsi_chi2_mc_w_percent->SetLineColor(6);
    Jpsi_chi2_mc_w_percent->Draw("hist same");
    TLegend *l_Jpsi_chi2_percent = new TLegend(.75, .80, .95, .95);
    l_Jpsi_chi2_percent->AddEntry(Jpsi_chi2_data_percent, "data same charge");
    l_Jpsi_chi2_percent->AddEntry(Jpsi_chi2_data_w_percent, "data after substraction");
    l_Jpsi_chi2_percent->AddEntry(Jpsi_chi2_mc_percent, "B_d -> J/#psi K #pi (mc)");
    l_Jpsi_chi2_percent->AddEntry(Jpsi_chi2_mc_w_percent, "mc weighted");
    l_Jpsi_chi2_percent->Draw("same");
    c_Jpsi_chi2_percent->SaveAs("Jpsi_chi2_percent.png");

    TCanvas *c_devision_percent = new TCanvas("c_devision_percent");
    devision_data_percent->SetLineWidth(2.);
    devision_data_percent->GetYaxis()->SetRangeUser(0., 1.);
    devision_data_w_percent->SetLineWidth(2.);
    devision_data_w_percent->SetLineColor(kRed);
    devision_mc_percent->SetLineWidth(2.);
    devision_mc_percent->SetLineColor(kGreen);
    devision_data_percent->SetStats(kFALSE);
    devision_data_percent->GetXaxis()->SetTitle("P_{T}(B-had) / P_{T}(primary vertex)");
    devision_data_percent->SetTitle("");
    devision_data_percent->Draw("hist");
    devision_data_w_percent->Draw("hist same");
    devision_mc_percent->Draw("hist same");
    devision_mc_w_percent->SetLineWidth(2.);
    devision_mc_w_percent->SetLineColor(6);
    devision_mc_w_percent->Draw("hist same");
    TLegend *l_devision_percent = new TLegend(.75, .80, .95, .95);
    l_devision_percent->AddEntry(Jpsi_chi2_data_percent, "data same charge");
    l_devision_percent->AddEntry(Jpsi_chi2_data_w_percent, "data after substraction");
    l_devision_percent->AddEntry(Jpsi_chi2_mc_percent, "B_d -> J/#psi K #pi (mc)");
    l_devision_percent->AddEntry(Jpsi_chi2_mc_w_percent, "mc weighted");
    l_devision_percent->Draw("same");
    c_devision_percent->SaveAs("devision_percent.png");
    
    TCanvas *c_devision_minA0_percent = new TCanvas("c_devision_minA0_percent");
    devision_minA0_data_percent->SetLineWidth(2.);
    devision_minA0_data_percent->GetYaxis()->SetRangeUser(0., 1.);
    devision_minA0_data_w_percent->SetLineWidth(2.);
    devision_minA0_data_w_percent->SetLineColor(kRed);
    devision_minA0_mc_percent->SetLineWidth(2.);
    devision_minA0_mc_percent->SetLineColor(kGreen);
    devision_minA0_data_percent->SetStats(kFALSE);
    devision_minA0_data_percent->GetXaxis()->SetTitle("P_{T}(B-had) / P_{T}(primary vertex)");
    devision_minA0_data_percent->SetTitle("");
    devision_minA0_data_percent->Draw("hist");
    devision_minA0_data_w_percent->Draw("hist same");
    devision_minA0_mc_percent->Draw("hist same");
    devision_minA0_mc_w_percent->SetLineWidth(2.);
    devision_minA0_mc_w_percent->SetLineColor(6);
    devision_minA0_mc_w_percent->Draw("hist same");
    TLegend *l_devision_minA0_percent = new TLegend(.75, .80, .95, .95);
    l_devision_minA0_percent->AddEntry(Jpsi_chi2_data_percent, "data same charge");
    l_devision_minA0_percent->AddEntry(Jpsi_chi2_data_w_percent, "data after substraction");
    l_devision_minA0_percent->AddEntry(Jpsi_chi2_mc_percent, "B_d -> J/#psi K #pi (mc)");
    l_devision_minA0_percent->AddEntry(Jpsi_chi2_mc_w_percent, "mc weighted");
    l_devision_minA0_percent->Draw("same");
    c_devision_minA0_percent->SaveAs("devision_minA0_percent.png");
}
