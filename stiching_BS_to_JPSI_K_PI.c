{
    // we have MCs with Bhadron and antiBhadron decays 
    // for each run we need to choose one of it
    // Possible variants:
    // "Bhad"
    // "antiBhad"
    
    std::string decay_type = "antiBhad";
    
    // Possible modes:
    // - "comb_bg"
    // That is selection for obtaining
    // comb_bg in signal and control areas
    // - "default"
    // That is default selection for slimming
    // ntuples (if you want to vary selection later)
    
    std::string selection_mode = "default";
    
    float mu1_pt, mu2_pt, trk1_pt, trk2_pt, mu1_px, mu1_py, mu1_pz, mu2_px, mu2_py, mu2_pz, trk1_px, trk1_py, trk1_pz, trk2_px, trk2_py, trk2_pz, trk1_charge, trk2_charge, Lxy_MaxSumPt, Bs_pt, Bs_chi2_ndof, MaxSumPt, mu1_eta, mu2_eta, trk1_eta, trk2_eta, Jpsi_mass, Jpsi_chi2, mu1_px_ReFit_pK, mu1_py_ReFit_pK, mu1_pz_ReFit_pK, mu2_px_ReFit_pK, mu2_py_ReFit_pK, mu2_pz_ReFit_pK, trk1_px_ReFit_pK, trk1_py_ReFit_pK, trk1_pz_ReFit_pK, trk2_px_ReFit_pK, trk2_py_ReFit_pK, trk2_pz_ReFit_pK, mu1_px_ReFit_Kp, mu1_py_ReFit_Kp, mu1_pz_ReFit_Kp, mu2_px_ReFit_Kp, mu2_py_ReFit_Kp, mu2_pz_ReFit_Kp, trk1_px_ReFit_Kp, trk1_py_ReFit_Kp, trk1_pz_ReFit_Kp, trk2_px_ReFit_Kp, trk2_py_ReFit_Kp, trk2_pz_ReFit_Kp, w_trigger, w_SF_mu1, w_SF_mu2, Bs_B_pt, MinA0SumPt, Lxy_MinA0;
    Int_t B_PDG, Kplus_PDG, Piminus_PDG;
    
    int n = 0, nk = 0;
    
    std::string sf_name = "";
    
    if (selection_mode == "comb_bg")
    {
         sf_name = "datasets/mc_BS_to_JPSI_K_PI_" + decay_type + "_for_comb.root";
    }
    else
    {
         sf_name = "datasets/mc_BS_to_JPSI_K_PI_" + decay_type + ".root";
    }
    
    TFile *sf = new TFile(sf_name.c_str(), "recreate");
    
    TTree *streemc = new TTree("BsAllCandidates", "BsAllCandidates");
    
    TBranch *sB_MinA0SumPt = streemc->Branch("sB_MinA0SumPt", &MinA0SumPt);
    TBranch *sB_Lxy_MinA0 = streemc->Branch("sB_Lxy_MinA0", &Lxy_MinA0);
    TBranch *sB_B_pt = streemc->Branch("sB_B_pt", &Bs_B_pt);
    TBranch *sB_mu1_pt = streemc->Branch("sB_mu1_pt", &mu1_pt);
    TBranch *sB_mu2_pt = streemc->Branch("sB_mu2_pt", &mu2_pt);
    TBranch *sB_trk1_pt = streemc->Branch("sB_trk1_pt", &trk1_pt);
    TBranch *sB_trk2_pt = streemc->Branch("sB_trk2_pt", &trk2_pt);
    TBranch *sB_mu1_px = streemc->Branch("sB_mu1_px", &mu1_px);
    TBranch *sB_mu1_py = streemc->Branch("sB_mu1_py", &mu1_py);
    TBranch *sB_mu1_pz = streemc->Branch("sB_mu1_pz", &mu1_pz);
    TBranch *sB_mu2_px = streemc->Branch("sB_mu2_px", &mu2_px);
    TBranch *sB_mu2_py = streemc->Branch("sB_mu2_py", &mu2_py);
    TBranch *sB_mu2_pz = streemc->Branch("sB_mu2_pz", &mu2_pz);
    TBranch *sB_trk1_px = streemc->Branch("sB_trk1_px", &trk1_px);
    TBranch *sB_trk1_py = streemc->Branch("sB_trk1_py", &trk1_py);
    TBranch *sB_trk1_pz = streemc->Branch("sB_trk1_pz", &trk1_pz);
    TBranch *sB_trk2_px = streemc->Branch("sB_trk2_px", &trk2_px);
    TBranch *sB_trk2_py = streemc->Branch("sB_trk2_py", &trk2_py);
    TBranch *sB_trk2_pz = streemc->Branch("sB_trk2_pz", &trk2_pz);
    TBranch *sB_trk1_charge = streemc->Branch("sB_trk1_charge", &trk1_charge);
    TBranch *sB_trk2_charge = streemc->Branch("sB_trk2_charge", &trk2_charge);
    TBranch *sB_MaxSumPt = streemc->Branch("sB_MaxSumPt", &MaxSumPt);                
    TBranch *sB_mu1_eta = streemc->Branch("sB_mu1_eta", &mu1_eta);
    TBranch *sB_mu2_eta = streemc->Branch("sB_mu2_eta", &mu2_eta);
    TBranch *sB_trk1_eta = streemc->Branch("sB_trk1_eta", &trk1_eta);
    TBranch *sB_trk2_eta = streemc->Branch("sB_trk2_eta", &trk2_eta);
    TBranch *sB_Bs_pt = streemc->Branch("sB_Bs_pt", &Bs_pt);
    TBranch *sB_Bs_chi2_ndof = streemc->Branch("sB_Bs_chi2_ndof", &Bs_chi2_ndof);
    TBranch *sB_Lxy_MaxSumPt = streemc->Branch("sB_Lxy_MaxSumPt", &Lxy_MaxSumPt);
    TBranch *sB_Jpsi_mass = streemc->Branch("sB_Jpsi_mass", &Jpsi_mass);
    TBranch *sB_Jpsi_chi2 = streemc->Branch("sB_Jpsi_chi2", &Jpsi_chi2);
    TBranch *sB_w_trigger = streemc->Branch("sB_w_trigger", &w_trigger);
    TBranch *sB_Bhad_PDG = streemc->Branch("sB_Bhad_PDG", &B_PDG);
    TBranch *sB_Kplus_PDG = streemc->Branch("sB_Kplus_PDG", &Kplus_PDG);
    TBranch *sB_Piminus_PDG = streemc->Branch("sB_Piminus_PDG", &Piminus_PDG);
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    
    TChain BsAllCandidates("BsAllCandidates");
    
    // Don`t forget to write proper ntuples_path to mc_BS_to_JPSI_K_PI
    
    /*if (decay_type == "antiBhad")
    {
        BsAllCandidates.AddFile("<ntuples_path>/user.avasyuko.mc16old300462_same_charge_rerun_EXT0.root ");
    }
    else
    {   
        BsAllCandidates.AddFile("<ntuples_path>/user.avasyuko.mc16old300463_same_charge_rerun_EXT0.root"); 
    }*/
    
    if (decay_type == "antiBhad")
    {
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/mc_BS_to_JPSI_K_PI/user.avasyuko.mc16old300462_same_charge_rerun_EXT0.root ");
    }
    else
    {   
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/mc_BS_to_JPSI_K_PI/user.avasyuko.mc16old300463_same_charge_rerun_EXT0.root"); 
    }
    
    Long64_t nentries = BsAllCandidates.GetEntries();
    vector<float> *B_mu1_px, *B_mu1_py, *B_mu1_pz, *B_mu2_px, *B_mu2_py, *B_mu2_pz, *B_trk1_px, *B_trk1_py, *B_trk1_pz, *B_trk2_px, *B_trk2_py, *B_trk2_pz, *B_trk1_charge, *B_trk2_charge, *B_Lxy_MaxSumPt, *B_pT, *B_mu1_pT, *B_mu2_pT, *B_trk1_pT, *B_trk2_pT, *B_chi2_ndof, *MaxSumPt_SumPt, *B_mu1_eta, *B_mu2_eta, *B_trk1_eta, *B_trk2_eta, *B_Jpsi_mass, *B_Jpsi_chi2, *B_mu1_px_ReFit_pK, *B_mu1_py_ReFit_pK, *B_mu1_pz_ReFit_pK, *B_mu2_px_ReFit_pK, *B_mu2_py_ReFit_pK, *B_mu2_pz_ReFit_pK, *B_trk1_px_ReFit_pK, *B_trk1_py_ReFit_pK, *B_trk1_pz_ReFit_pK, *B_trk2_px_ReFit_pK, *B_trk2_py_ReFit_pK, *B_trk2_pz_ReFit_pK, *B_mu1_px_ReFit_Kp, *B_mu1_py_ReFit_Kp, *B_mu1_pz_ReFit_Kp, *B_mu2_px_ReFit_Kp, *B_mu2_py_ReFit_Kp, *B_mu2_pz_ReFit_Kp, *B_trk1_px_ReFit_Kp, *B_trk1_py_ReFit_Kp, *B_trk1_pz_ReFit_Kp, *B_trk2_px_ReFit_Kp, *B_trk2_py_ReFit_Kp, *B_trk2_pz_ReFit_Kp, *w_rSF_mu1, *w_rSF_mu2, *MinA0_SumPt, *B_Lxy_MinA0;
    vector<int> *B_ReFit_result;
    UInt_t B_RunNumber;
    vector<bool> *HLT_mu6_mu4_bJpsimumu_noL2_isPassed, *HLT_2mu4_bJpsimumu_noL2_isPassed,
    *HLT_mu6_mu4_bJpsimumu_isPassed, 
    *HLT_mu10_mu6_bJpsimumu_isPassed,
    *HLT_2mu6_bJpsimumu_isPassed, 
    *HLT_mu20_2mu0noL1_JpsimumuFS_isPassed,
    *HLT_mu6_2mu4_bJpsi_isPassed,
    *HLT_mu20_nomucomb_mu6noL1_nscan03_isPassed,
    *HLT_2mu6_bJpsimumu_delayed_isPassed,
    *HLT_mu6_mu4_bJpsimumu_Lxy0_delayed_isPassed,
    *HLT_mu6_nomucomb_2mu4_nomucomb_delayed_L1MU6_3MU4_isPassed,
    *HLT_3mu4_bJpsi_delayed_isPassed,
    *HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6_isPassed,
    *HLT_mu6_mu4_bJpsimumu_Lxy0_L1BPH_2M9_MU6MU4_BPH_0DR15_MU6MU4_isPassed,
    *HLT_mu11_mu6_bDimu_isPassed,
    *HLT_3mu4_bJpsi_isPassed,
    *HLT_mu11_2mu4noL1_bNocut_L1MU11_2MU6_isPassed,
    *HLT_2mu4_bJpsimumu_Lxy0_L1BPH_2M9_2MU4_BPH_0DR15_2MU4_isPassed;
    
    vector<Int_t> *quality_mu1, *quality_mu2, *Pixelhits_had1, *Pixelhits_had2, *PixelShared_had1, *PixelShared_had2, *PixelDeadSensors_had1, *PixelDeadSensors_had2, *SCThitshad1, *SCThitshad2, *SCTShared_had1, *SCTShared_had2, *SCTDeadSensors_had1, *SCTDeadSensors_had2, *SCTHoles_had1, *SCTHoles_had2, *PixelHoles_had1, *PixelHoles_had2;
    
    BsAllCandidates.SetBranchAddress("B_Lxy_MinA0", &B_Lxy_MinA0);
    BsAllCandidates.SetBranchAddress("MinA0_SumPt", &MinA0_SumPt);
    
    BsAllCandidates.SetBranchAddress("B_mu1_px", &B_mu1_px);
    BsAllCandidates.SetBranchAddress("B_mu1_py", &B_mu1_py);
    BsAllCandidates.SetBranchAddress("B_mu1_pz", &B_mu1_pz);
    BsAllCandidates.SetBranchAddress("B_mu2_px", &B_mu2_px);
    BsAllCandidates.SetBranchAddress("B_mu2_py", &B_mu2_py);
    BsAllCandidates.SetBranchAddress("B_mu2_pz", &B_mu2_pz);
    BsAllCandidates.SetBranchAddress("B_trk1_px", &B_trk1_px);
    BsAllCandidates.SetBranchAddress("B_trk1_py", &B_trk1_py);
    BsAllCandidates.SetBranchAddress("B_trk1_pz", &B_trk1_pz);
    BsAllCandidates.SetBranchAddress("B_trk2_px", &B_trk2_px);
    BsAllCandidates.SetBranchAddress("B_trk2_py", &B_trk2_py);
    BsAllCandidates.SetBranchAddress("B_trk2_pz", &B_trk2_pz);
    BsAllCandidates.SetBranchAddress("B_trk1_charge", &B_trk1_charge);
    BsAllCandidates.SetBranchAddress("B_trk2_charge", &B_trk2_charge);
    BsAllCandidates.SetBranchAddress("B_Lxy_MaxSumPt", &B_Lxy_MaxSumPt);
    BsAllCandidates.SetBranchAddress("B_pT", &B_pT);
    BsAllCandidates.SetBranchAddress("B_mu1_pT", &B_mu1_pT);
    BsAllCandidates.SetBranchAddress("B_mu2_pT", &B_mu2_pT);
    BsAllCandidates.SetBranchAddress("B_trk1_pT", &B_trk1_pT);
    BsAllCandidates.SetBranchAddress("B_trk2_pT", &B_trk2_pT);
    BsAllCandidates.SetBranchAddress("B_chi2_ndof", &B_chi2_ndof);
    BsAllCandidates.SetBranchAddress("MaxSumPt_SumPt", &MaxSumPt_SumPt);
    BsAllCandidates.SetBranchAddress("B_mu1_eta", &B_mu1_eta);
    BsAllCandidates.SetBranchAddress("B_mu2_eta", &B_mu2_eta);
    BsAllCandidates.SetBranchAddress("B_trk1_eta", &B_trk1_eta);
    BsAllCandidates.SetBranchAddress("B_trk2_eta", &B_trk2_eta);
    BsAllCandidates.SetBranchAddress("B_Jpsi_mass", &B_Jpsi_mass);
    BsAllCandidates.SetBranchAddress("Jpsi_chi2", &B_Jpsi_chi2);
    BsAllCandidates.SetBranchAddress("run_number", &B_RunNumber);
    BsAllCandidates.SetBranchAddress("HLT_mu6_mu4_bJpsimumu_noL2_isPassed", &HLT_mu6_mu4_bJpsimumu_noL2_isPassed);
    BsAllCandidates.SetBranchAddress("HLT_2mu4_bJpsimumu_noL2_isPassed", &HLT_2mu4_bJpsimumu_noL2_isPassed);
    BsAllCandidates.SetBranchAddress("HLT_mu6_mu4_bJpsimumu_isPassed", &HLT_mu6_mu4_bJpsimumu_isPassed);
    BsAllCandidates.SetBranchAddress("HLT_mu10_mu6_bJpsimumu_isPassed", &HLT_mu10_mu6_bJpsimumu_isPassed);
    BsAllCandidates.SetBranchAddress("HLT_2mu6_bJpsimumu_isPassed", &HLT_2mu6_bJpsimumu_isPassed);
    BsAllCandidates.SetBranchAddress("HLT_mu20_2mu0noL1_JpsimumuFS_isPassed", &HLT_mu20_2mu0noL1_JpsimumuFS_isPassed);
    BsAllCandidates.SetBranchAddress("HLT_mu6_2mu4_bJpsi_isPassed", &HLT_mu6_2mu4_bJpsi_isPassed);
    BsAllCandidates.SetBranchAddress("HLT_mu20_nomucomb_mu6noL1_nscan03_isPassed", &HLT_mu20_nomucomb_mu6noL1_nscan03_isPassed);
    BsAllCandidates.SetBranchAddress("HLT_2mu6_bJpsimumu_delayed_isPassed", &HLT_2mu6_bJpsimumu_delayed_isPassed);
    BsAllCandidates.SetBranchAddress("HLT_mu6_mu4_bJpsimumu_Lxy0_delayed_isPassed", &HLT_mu6_mu4_bJpsimumu_Lxy0_delayed_isPassed);
    BsAllCandidates.SetBranchAddress("HLT_mu6_nomucomb_2mu4_nomucomb_delayed_L1MU6_3MU4_isPassed", &HLT_mu6_nomucomb_2mu4_nomucomb_delayed_L1MU6_3MU4_isPassed);
    BsAllCandidates.SetBranchAddress("HLT_3mu4_bJpsi_delayed_isPassed", &HLT_3mu4_bJpsi_delayed_isPassed);
    BsAllCandidates.SetBranchAddress("HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6_isPassed", &HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6_isPassed);
    BsAllCandidates.SetBranchAddress("HLT_mu6_mu4_bJpsimumu_Lxy0_L1BPH_2M9_MU6MU4_BPH_0DR15_MU6MU4_isPassed", &HLT_mu6_mu4_bJpsimumu_Lxy0_L1BPH_2M9_MU6MU4_BPH_0DR15_MU6MU4_isPassed);
    BsAllCandidates.SetBranchAddress("HLT_mu11_mu6_bDimu_isPassed", &HLT_mu11_mu6_bDimu_isPassed);
    BsAllCandidates.SetBranchAddress("HLT_3mu4_bJpsi_isPassed", &HLT_3mu4_bJpsi_isPassed);
    BsAllCandidates.SetBranchAddress("HLT_mu11_2mu4noL1_bNocut_L1MU11_2MU6_isPassed", &HLT_mu11_2mu4noL1_bNocut_L1MU11_2MU6_isPassed);
    BsAllCandidates.SetBranchAddress("HLT_2mu4_bJpsimumu_Lxy0_L1BPH_2M9_2MU4_BPH_0DR15_2MU4_isPassed", &HLT_2mu4_bJpsimumu_Lxy0_L1BPH_2M9_2MU4_BPH_0DR15_2MU4_isPassed);
    
    BsAllCandidates.SetBranchAddress("quality_mu1", &quality_mu1);
    BsAllCandidates.SetBranchAddress("quality_mu2", &quality_mu2);
    
    TChain BsTruth("BsTruth");
    
    // Don`t forget to write proper ntuples_path to mc_BS_to_JPSI_K_PI
    
    /*if (decay_type == "antiBhad")
    {
        BsTruth.AddFile("<ntuples_path>/user.avasyuko.mc16old300462_same_charge_rerun_EXT0.root ");
    }
    else
    {   
        BsTruth.AddFile("<ntuples_path>/user.avasyuko.mc16old300463_same_charge_rerun_EXT0.root"); 
    }*/
    
    if (decay_type == "antiBhad")
    {
        BsTruth.AddFile("~/BPHY11_15/datasets/mc_BS_to_JPSI_K_PI/user.avasyuko.mc16old300462_same_charge_rerun_EXT0.root ");
    }
    else
    {   
        BsTruth.AddFile("~/BPHY11_15/datasets/mc_BS_to_JPSI_K_PI/user.avasyuko.mc16old300463_same_charge_rerun_EXT0.root"); 
    }
    
    vector<float> *Truth_B_pt;
    vector<Int_t> *Bhad_PDG, *kaon_PDG, *pion_PDG;
    
    BsTruth.SetBranchAddress("Bs_pt", &Truth_B_pt);
    BsTruth.SetBranchAddress("Bs_PDG", &Bhad_PDG);
    BsTruth.SetBranchAddress("Bs_Kplus_PDG", &kaon_PDG);
    BsTruth.SetBranchAddress("Bs_Piminus_PDG", &pion_PDG);
    
    cout << "Processing mc dataset -----> number of \"dirty\" entries " << nentries << endl;
    for(int i = 1; i <= nentries; i++)
    {
            BsAllCandidates.GetEntry(i);
            BsTruth.GetEntry(i);
            m_runNumber = B_RunNumber;
            
            for(int k = 0; k < B_mu1_px->size(); k++)
            {
                nk++;
                
                TLorentzVector mu1, mu2, pi1, pi2, K1, K2, p1, p2, jpsi;
                mu1.SetXYZM(B_mu1_px->at(k), B_mu1_py->at(k), B_mu1_pz->at(k), 105.65837);
                mu2.SetXYZM(B_mu2_px->at(k), B_mu2_py->at(k), B_mu2_pz->at(k), 105.65837);
                pi1.SetXYZM(B_trk1_px->at(k), B_trk1_py->at(k), B_trk1_pz->at(k), 139.57);
                pi2.SetXYZM(B_trk2_px->at(k), B_trk2_py->at(k), B_trk2_pz->at(k), 139.57);
                K1.SetXYZM(B_trk1_px->at(k), B_trk1_py->at(k), B_trk1_pz->at(k), 493.677);
                K2.SetXYZM(B_trk2_px->at(k), B_trk2_py->at(k), B_trk2_pz->at(k), 493.677);
                p1.SetXYZM(B_trk1_px->at(k), B_trk1_py->at(k), B_trk1_pz->at(k), 938.272);
                p2.SetXYZM(B_trk2_px->at(k), B_trk2_py->at(k), B_trk2_pz->at(k), 938.272);
                jpsi.SetXYZM(B_mu1_px->at(k) + B_mu2_px->at(k), B_mu1_py->at(k) + B_mu2_py->at(k), B_mu1_pz->at(k) + B_mu2_pz->at(k), 3096.);
                TLorentzVector bs = jpsi + pi1 + pi2;
                
                //bool had1_WP = ((B_trk1_eta->at(k) < 2.5) && (B_trk1_eta->at(k) > -2.5) && (B_trk1_pT->at(k) > 500.) && ((Pixelhits_had1->at(k) + SCThitshad1->at(k) + SCTDeadSensors_had1->at(k) + PixelDeadSensors_had1->at(k)) >= 7) && (((float)PixelShared_had1->at(k) + SCTShared_had1->at(k) / 2.) <= 1.) && ((SCTHoles_had1->at(k) + PixelHoles_had1->at(k)) <= 2) && (PixelHoles_had1->at(k) <= 1));
                //bool had2_WP = ((B_trk2_eta->at(k) < 2.5) && (B_trk2_eta->at(k) > -2.5) && (B_trk2_pT->at(k) > 500.) && ((Pixelhits_had2->at(k) + SCThitshad2->at(k) + SCTDeadSensors_had2->at(k) + PixelDeadSensors_had2->at(k)) >= 7) && (((float)PixelShared_had2->at(k) + SCTShared_had2->at(k) / 2.) <= 1.) && ((SCTHoles_had2->at(k) + PixelHoles_had2->at(k)) <= 2) && (PixelHoles_had2->at(k) <= 1));
                
                 bool selection_test;
                
                if (selection_mode == "comb_bg") 
                {
                     selection_test = (B_Lxy_MaxSumPt->at(k) > 0.7 && B_chi2_ndof->at(k) < 2. && B_pT->at(k) > 0.15 * MaxSumPt_SumPt->at(k) && quality_mu1->at(k) == 0 && quality_mu2->at(k) == 0);
                }
                    
                if (selection_mode == "default") 
                {
                     selection_test = ((K1 + pi2).M() > 1500. && (pi1 + K2).M() > 1500. && B_Lxy_MaxSumPt->at(k) > 0.7 && B_mu1_pT->at(k) > 4000. && B_mu2_pT->at(k) > 4000. && B_trk1_pT->at(k) > 1500. && B_trk2_pT->at(k) > 1500. && B_chi2_ndof->at(k) < 2. && B_pT->at(k) > 0.15 * MaxSumPt_SumPt->at(k) && quality_mu1->at(k) == 0 && quality_mu2->at(k) == 0); 
                }
                
                selection_test = selection_test && (B_trk1_charge->at(k) * B_trk2_charge->at(k) < 0.);
                
                if (decay_type == "Bhad")
                {
                    selection_test = selection_test && (Bhad_PDG->at(k) > 0);
                }
                else
                {
                    selection_test = selection_test && (Bhad_PDG->at(k) < 0);
                }
                
                if (selection_test)  
                {
                     bool mc16a = (HLT_mu6_mu4_bJpsimumu_noL2_isPassed->at(k) || HLT_2mu4_bJpsimumu_noL2_isPassed->at(k) || HLT_2mu6_bJpsimumu_delayed_isPassed->at(k) || HLT_mu6_mu4_bJpsimumu_Lxy0_delayed_isPassed->at(k) || HLT_3mu4_bJpsi_delayed_isPassed->at(k) || HLT_mu6_mu4_bJpsimumu_isPassed->at(k) || HLT_mu10_mu6_bJpsimumu_isPassed->at(k) || HLT_2mu6_bJpsimumu_isPassed->at(k) || HLT_mu6_2mu4_bJpsi_isPassed->at(k)) || HLT_mu6_nomucomb_2mu4_nomucomb_delayed_L1MU6_3MU4_isPassed->at(k);
                    
                    bool mc16c = (HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6_isPassed->at(k) || HLT_mu6_mu4_bJpsimumu_Lxy0_L1BPH_2M9_MU6MU4_BPH_0DR15_MU6MU4_isPassed->at(k)  || HLT_3mu4_bJpsi_isPassed->at(k) || HLT_mu11_mu6_bDimu_isPassed->at(k));
                    
                    bool mc16e = (HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6_isPassed->at(k) || HLT_mu6_mu4_bJpsimumu_Lxy0_L1BPH_2M9_MU6MU4_BPH_0DR15_MU6MU4_isPassed->at(k) || HLT_3mu4_bJpsi_isPassed->at(k) || HLT_2mu4_bJpsimumu_Lxy0_L1BPH_2M9_2MU4_BPH_0DR15_2MU4_isPassed->at(k) || HLT_mu11_mu6_bDimu_isPassed);
                    
                    if ( m_runNumber == 284500 )
                    {if (!mc16a) {continue;}}
                    else if ( m_runNumber == 300000 )
                    {if (!mc16c) {continue;}}
                    else if ( m_runNumber == 310000 )
                    {if (!mc16e) {continue;}}
                    
                    n++;
                    if (n % 50000 == 0) {cout << "number of entries " << n << endl;}
                    
                    m_HLT.clear();
                    if (HLT_mu6_mu4_bJpsimumu_noL2_isPassed->at(k) == 1) {
                        m_HLT.push_back("HLT_mu6_mu4_bJpsimumu_noL2");
                    }
                    if (HLT_2mu4_bJpsimumu_noL2_isPassed->at(k) == 1) {
                        m_HLT.push_back("HLT_2mu4_bJpsimumu_noL2");
                    }
                    if (HLT_mu6_mu4_bJpsimumu_isPassed->at(k) == 1) {
                        m_HLT.push_back("HLT_mu6_mu4_bJpsimumu");
                    }
                    if (HLT_mu10_mu6_bJpsimumu_isPassed->at(k) == 1) {
                        m_HLT.push_back("HLT_mu10_mu6_bJpsimumu");
                    }
                    if (HLT_2mu6_bJpsimumu_isPassed->at(k) == 1) {
                        m_HLT.push_back("HLT_2mu6_bJpsimumu");
                    }
                    if (HLT_mu6_2mu4_bJpsi_isPassed->at(k) == 1) {
                        m_HLT.push_back("HLT_mu6_2mu4_bJpsi");
                    }
                    if (HLT_2mu6_bJpsimumu_delayed_isPassed->at(k) == 1) {
                        m_HLT.push_back("HLT_2mu6_bJpsimumu_delayed");
                    }
                    if (HLT_mu6_mu4_bJpsimumu_Lxy0_delayed_isPassed->at(k) == 1) {
                        m_HLT.push_back("HLT_mu6_mu4_bJpsimumu_Lxy0_delayed");
                    }
                    if (HLT_mu6_nomucomb_2mu4_nomucomb_delayed_L1MU6_3MU4_isPassed->at(k) == 1) {
                        m_HLT.push_back("HLT_mu6_nomucomb_2mu4_nomucomb_delayed_L1MU6_3MU4");
                    }
                    if (HLT_3mu4_bJpsi_delayed_isPassed->at(k) == 1) {
                        m_HLT.push_back("HLT_3mu4_bJpsi_delayed");
                    }
                    if (HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6_isPassed->at(k) == 1) {
                        m_HLT.push_back("HLT_2mu6_bJpsimumu_L1BPH-2M9-2MU6_BPH-2DR15-2MU6");
                    }
                    if (HLT_mu6_mu4_bJpsimumu_Lxy0_L1BPH_2M9_MU6MU4_BPH_0DR15_MU6MU4_isPassed->at(k) == 1) {
                        m_HLT.push_back("HLT_mu6_mu4_bJpsimumu_Lxy0_L1BPH-2M9-MU6MU4_BPH-0DR15-MU6MU4");
                    }
                    if (HLT_mu11_mu6_bDimu_isPassed->at(k) == 1) {
                        m_HLT.push_back("HLT_mu11_mu6_bDimu");
                    }
                    if (HLT_3mu4_bJpsi_isPassed->at(k) == 1) {
                        m_HLT.push_back("HLT_3mu4_bJpsi");
                    }
                    if (HLT_2mu4_bJpsimumu_Lxy0_L1BPH_2M9_2MU4_BPH_0DR15_2MU4_isPassed->at(k) == 1) {
                        m_HLT.push_back("HLT_2mu4_bJpsimumu_Lxy0_L1BPH-2M9-2MU4_BPH-0DR15-2MU4");
                    }
                    
                    if (B_trk1_charge->at(k) * B_trk2_charge->at(k) > 0.) 
                    {
                        cout << "same charge error!!!\t" << endl;
                    }
                    
                    setTriggerBits();
                    w_trigger = triggerWeight();
                    
                    mu1_pt = B_mu1_pT->at(k);
                    mu2_pt = B_mu2_pT->at(k);
                    trk1_pt = B_trk1_pT->at(k);
                    trk2_pt = B_trk2_pT->at(k);
                    mu1_px = B_mu1_px->at(k);
                    mu1_py = B_mu1_py->at(k);
                    mu1_pz = B_mu1_pz->at(k);
                    mu2_px = B_mu2_px->at(k);
                    mu2_py = B_mu2_py->at(k);
                    mu2_pz = B_mu2_pz->at(k);
                    trk1_px = B_trk1_px->at(k);
                    trk1_py = B_trk1_py->at(k);
                    trk1_pz = B_trk1_pz->at(k);
                    trk2_px = B_trk2_px->at(k);
                    trk2_py = B_trk2_py->at(k);
                    trk2_pz = B_trk2_pz->at(k);
                    trk1_charge = B_trk1_charge->at(k);
                    trk2_charge = B_trk2_charge->at(k);
                    MaxSumPt = MaxSumPt_SumPt->at(k);
                    mu1_eta = B_mu1_eta->at(k);
                    mu2_eta = B_mu2_eta->at(k);
                    trk1_eta = B_trk1_eta->at(k);
                    trk2_eta = B_trk2_eta->at(k);
                    Bs_pt = B_pT->at(k);
                    Bs_chi2_ndof = B_chi2_ndof->at(k);
                    Lxy_MaxSumPt = B_Lxy_MaxSumPt->at(k);
                    Jpsi_mass = B_Jpsi_mass->at(k);
                    Jpsi_chi2 = B_Jpsi_chi2->at(0);
                    MinA0SumPt = MinA0_SumPt->at(k);
                    Lxy_MinA0 = B_Lxy_MinA0->at(k);
                    
                    Bs_B_pt = Truth_B_pt->at(k);
                    
                    B_PDG = Bhad_PDG->at(k);
                    Kplus_PDG = kaon_PDG->at(k);
                    Piminus_PDG = pion_PDG->at(k);
                    streemc->Fill();
                    
                }
            }
    }
    
    cout << "_____________________________________________________________________" << endl;
    cout << "Total number of events:  " << n << endl;
    
    sf->cd();
    streemc->Write();
    sf->Close();
}
