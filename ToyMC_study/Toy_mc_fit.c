#include <stdio.h>
#include <time.h>
#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <TMath.h>
#include <vector>

#include <TROOT.h> 

#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TMinuit.h>
#include <TLatex.h>
#include <TRandom3.h> 
#include <thread>
#include <algorithm>
#include <THStack.h>
#include <TRandom.h>

#include <complex>
//#include "AtlasStyle.png"

#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TF1.h>

#include "TLorentzVector.h"
#include "TVector3.h"
#include "TVectorD.h"

#include "TMath.h"

#include "RooRealVar.h"
#include "RooHistPdf.h"
#include "RooDataHist.h"
#include "RooDataSet.h"
#include "RooGlobalFunc.h"
#include "RooGaussian.h"
#include "RooConstVar.h"
#include "RooPolyVar.h"
#include "RooProdPdf.h"
#include "RooPlot.h"
#include "RooRandom.h"

using namespace RooFit ;

//#include "Math/Vector3D.h"
//#include "Math/Vector4D.h"

#define MASS_JPSI   3.096900
#define MASS_MUON   0.1056583745
#define MASS_PION   0.13957061
#define MASS_KAON   0.493677
#define MASS_PROTON 0.938272081
#define MASS_LB     5.61960
#define MASS_BD     5.27963
#define MASS_BS     5.36689
#define MEV         1000.0
float GEV = 0.001;

Double_t PI = TMath::Pi();

int includeLs1810 = 1;   int extended1810 = 0;
	int includeLs1820 = 0;
	int includeLs1830 = 0;
	int includeLs1890 = 1;   int extended1890 = 0;
	int includeLs2020 = 0;
	int includeLs2050 = 0;
	int includeLs2100 = 1;
	int includeLs2110 = 1;
	int includeLs2325 = 0;
	int includeLs2350 = 0;
	int includeLs2385 = 0;

#define BW_nom_Lstar 0
#define BW_nom_Pc 0
#define Pc_32minus_52plus 1
#define Pc_32plus_52minus 0
#define Pc4_12minus_32minus_12minus_32minus 0
#define includeLsNRsimple 0
#define includeLsNR 0
#define Pc1_interference_off 0
#define Pc2_interference_off 0
#define Pc3_interference_off 0
#define Pc4_interference_off 0
#define includeKsNR 1

#define phs_Bd 148372
#define phs_Bs 35526
#define phs_Lb 12823

#define includeFNRsimple 1

//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__| fit modes START  |__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//

bool RARE_DECAYS = 1;
    
bool fit_mode = 1;
bool JpsipK_fit_mode = 0;
bool JpsipK_fit_mode_plus_JpsiKpi = 0;
bool JpsipK_fit_mode_comb_only = 0;
bool Jpsipipi_fit_mode = 0;
bool Jpsipipi_fit_mode_plus_JpsiKpi = 0;
bool Jpsipipi_fit_mode_plus_JpsiKpi_lbfix = 0;
bool Jpsipipi_fit_mode_comb_only = 0;
bool JpsiKK_fit_mode = 0;
bool JpsiKK_fit_mode_plus_JpsiKpi = 0;
bool JpsiKK_fit_mode_plus_JpsiKpi_lbpipi_fix = 0;
bool JpsiKK_fit_mode_no_control = 0;
bool JpsiKK_fit_mode_plus_JpsiKpi_no_control_lbpipi_fix = 0;
bool JpsiKK_fit_mode_comb_only = 0;
bool JpsiKpi_fit_mode = 0;
bool JpsiKpi_fit_mode_no_signal = 0;
bool JpsiKpi_fit_mode_comb_only = 0;
bool signal_area_fit_mode = 0;
bool signal_area_fit_mode_short = 0;
bool global_area_no_comb_form_4tr = 0;
bool signal_area_fit_mode_short_noZc = 0;
bool signal_area_fit_mode_noZc = 0;
bool signal_area_fit_mode_onlyZc = 0;
Int_t MODEL = 1;

//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__| fit modes FINISH |__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//



//#define THREADS 4

//#define GEV 0.001

int THREADS;

using namespace std;

void ov_uf_bins_Clear(TH2 *hist)
{
    Int_t hist_nbinsX = hist->ProjectionX()->GetNbinsX();
    Int_t hist_nbinsY = hist->ProjectionY()->GetNbinsX();
    
    for(int i = 0; i <= hist_nbinsX + 1; i++)
    {
        hist->SetBinContent(i, 0, 0.);
        hist->SetBinContent(i, hist_nbinsY + 1, 0.);
    }
    
    for(int i = 0; i <= hist_nbinsY + 1; i++)
    {
        hist->SetBinContent(0, i, 0.);
        hist->SetBinContent(hist_nbinsX + 1, i, 0.);
    }
}

/*std::complex<float> breit_wigner(float m0, float m, float g0, int l, float d, float p, float p0) //counts relativistic Breit-Wigner amplitude
{
	std::complex<float> z1(m * sqrt(m0 * G_width(p, p0, d, l, m0, m, g0)), 0);
	std::complex<float> z2(m0 * m0 - m * m, -1. * G_width(p, p0, d, l, m0, m, g0) * m0);
	std::complex<float> r;
	r = z1 / z2;
	//cout << "z2 = " << z2 << endl;
	return r;
}*/

float Wigner_matrix2(float a, float  b, float c, double fi) //counts Wigner d-matrix(0.5; 1; 1.5; 2.5 (not full b = 1.5|0.5|-0.5|-1.5); 3.5(not full b = 1.5|0.5|-0.5|-1.5))
{
	int t;
	float f = 0.;
	switch (t = (int)nearbyint(a * 2))
	{
	case 0:
	{	
		switch (t = (int)nearbyint(2 * b))
			  {
			  case 0:
			  {
						switch (t = (int)nearbyint(c * 2))
						{
						case 0:
						{
							f = 1.;
								   break;
						}
						}
						break;
			  }
			  }
			break;	
	}
	case 1:
	{
			  switch (t = (int)nearbyint(2 * b))
			  {
			  case 1:
			  {
						switch (t = (int)nearbyint(c * 2))
						{
						case -1:
						{
							f = -sin(fi / 2.);
								   break;
						}
						case 1:
						{
							f = cos(fi / 2.);
								  break;
						}
						default:
						{
													
							if ((f == 0) && (b != c)) { f = (float)pow(-1, b - c) * Wigner_matrix2(a, c, b, fi); }
							if (f == 0) { f = (float)pow(-1, b - c) * Wigner_matrix2(a, -b, -c, fi); }
							if (f == 0) { f = Wigner_matrix2(a, -c, -b, fi); }	
						}
						}
						break;
			  }
			  default:
			  {					
					if ((f == 0) && (b != c)) { f = (float)pow(-1, b - c) * Wigner_matrix2(a, c, b, fi); }
					if (f == 0) { f = (float)pow(-1, b - c) * Wigner_matrix2(a, -b, -c, fi); }
					if (f == 0) { f = Wigner_matrix2(a, -c, -b, fi); }		
			  }
			  }
			  break;
	}
	case 2:
	{
			  switch (t = (int)nearbyint(b))
			  {
			  case -1:
			  {
						switch (t = (int)nearbyint(c * 2))
						{
						case -2:
						{
								  f = (1. + cos(fi)) / 2.;
								  break;
						}
						case 0:
						{
								  f = -sin(fi) / sqrt(2.);
								  break;
						}
						case 2:
						{
								  f = (1. - cos(fi)) / 2.;
								  break;
						}
						default:
						{
													
							if ((f == 0) && (b != c)) { f = (float)pow(-1, b - c) * Wigner_matrix2(a, c, b, fi); }
							if (f == 0) { f = (float)pow(-1, b - c) * Wigner_matrix2(a, -b, -c, fi); }
							if (f == 0) { f = Wigner_matrix2(a, -c, -b, fi); }	
						}
						}
						break;

			  }
			  case 0:
			  {
						switch (t = (int)nearbyint(c * 2))
						{
						case -2:
						{
								  f = -sin(fi) / sqrt(2.);
								  break;
						}
						case 0:
						{
								  f = cos(fi);
								  break;
						}
						case 2:
						{
								  f = sin(fi) / sqrt(2.);
								  break;
						}
						default:
						{
													
							if ((f == 0) && (b != c)) { f = (float)pow(-1, b - c) * Wigner_matrix2(a, c, b, fi); }
							if (f == 0) { f = (float)pow(-1, b - c) * Wigner_matrix2(a, -b, -c, fi); }
							if (f == 0) { f = Wigner_matrix2(a, -c, -b, fi); }	
						}
						}
						break;
			  }
			  case 1:
			  {
						switch (t = (int)nearbyint(c * 2))
						{
						case -2:
						{
								   f = (1. - cos(fi)) / 2.;
								   break;
						}
						case 0:
						{
								  f = -sin(fi) / sqrt(2.);
								  break;
						}
						case 2:
						{
								  f = (1. + cos(fi)) / 2.;
								  break;
						}
						default:
						{
													
							if ((f == 0) && (b != c)) { f = (float)pow(-1, b - c) * Wigner_matrix2(a, c, b, fi); }
							if (f == 0) { f = (float)pow(-1, b - c) * Wigner_matrix2(a, -b, -c, fi); }
							if (f == 0) { f = Wigner_matrix2(a, -c, -b, fi); }	
						}
						}
						break;
			  }
				default:
						{
													
							if ((f == 0) && (b != c)) { f = (float)pow(-1, b - c) * Wigner_matrix2(a, c, b, fi); }
							if (f == 0) { f = (float)pow(-1, b - c) * Wigner_matrix2(a, -b, -c, fi); }
							if (f == 0) { f = Wigner_matrix2(a, -c, -b, fi); }	
						}
			  }
			  break;
	}
	case 4:
	{
			switch (t = (int)nearbyint(b))
			  {
			  case 0:
			  {
						switch (t = (int)nearbyint(c * 2))
						{
						case -2:
						{
								  f = -sqrt(6.) / 3. * pow(cos(fi / 2.), 5.) * pow(sin(fi / 2.), 1.) + sqrt(6.) / 2. * pow(cos(fi / 2.), 3.) * pow(sin(fi / 2.), 3.);
								  break;
						}
						case 0:
						{
								  f = pow(cos(fi / 2.), 4.);
								  break;
						}
						case 2:
						{
								  f = sqrt(6.) / 3. * pow(cos(fi / 2.), 5.) * pow(sin(fi / 2.), 1.) - sqrt(6.) / 2. * pow(cos(fi / 2.), 3.) * pow(sin(fi / 2.), 3.);
								  break;
						}
						}
						break;
			  }
			}
			break;	
	}
	case 6:
	{
			switch (t = (int)nearbyint(b))
			  {
			  case 0:
			  {
						switch (t = (int)nearbyint(c * 2))
						{
						case -2:
						{
								  f = - 2. * sqrt(3.) * pow(cos(fi / 2.), 7.) * pow(sin(fi / 2.), 1.) + 6 * sqrt(3.) * pow(cos(fi / 2.), 5.) * pow(sin(fi / 2.), 3.) - 2. * sqrt(3.) * pow(cos(fi / 2.), 3.) * pow(sin(fi / 2.), 5.);
								  break;
						}
						case 0:
						{
								  f = pow(cos(fi / 2.), 6.);
								  break;
						}
						case 2:
						{
								  f = 2. * sqrt(3.) * pow(cos(fi / 2.), 7.) * pow(sin(fi / 2.), 1.) - 6 * sqrt(3.) * pow(cos(fi / 2.), 5.) * pow(sin(fi / 2.), 3.) + 2. * sqrt(3.) * pow(cos(fi / 2.), 3.) * pow(sin(fi / 2.), 5.);
								  break;
						}
						}
						break;
			  }
			}
			break;	
	}

	default:
	{
		//cout << "forbidden value a = " << a << endl;;
	}
	}
	//cout << "Wigner_matrix = " << f << endl;
	return f;
}

float Wigner_matrix(float J, float m1, float m2, float theta)
{

//// J is spin*2
//// m1 is m*2
//// m2 is m'*2

float costheta = cos(theta);
float sintheta = sin(theta);

float costheta_2 = cos(theta/2.0);
float sintheta_2 = sin(theta/2.0);

float cos2theta = cos(2.0*theta);
float sin2theta = sin(2.0*theta);


if(J == 0.0) ////
{
	if((m1 == 0.0 && m2 == 0.0))
		return costheta_2;
}
else
if(J == 0.5) ////
{
	if((m1 == 0.5 && m2 == 0.5) || (m1 == -0.5 && m2 == -0.5))
		return costheta_2;
	if( m1 == 0.5 && m2 == -0.5 )
		return -sintheta_2;
	if( m1 == -0.5 && m2 == 0.5 )
		return sintheta_2;
}
else
if(J == 1.0) ////
{
	if((m1 == 1.0 && m2 == 1.0) || (m1 == -1.0 && m2 == -1.0))
		return (1 + costheta)/2.0;
	if((m1 == 1.0 && m2 == -1.0) || (m1 == -1.0 && m2 == 1.0))
		return (1 - costheta)/2.0;
	if((m1 == 1.0 && m2 == 0.0) || (m1 == 0.0 && m2 == -1.0))
		return -sintheta/sqrt(2.0);
	if((m1 == -1.0 && m2 == 0.0) || (m1 == 0.0 && m2 == 1.0))
		return sintheta/sqrt(2.0);
	if((m1 == 0.0 && m2 == 0.0))
		return costheta;
}
else
if(J == 1.5) ////
{
	if((m1 == 0.5 && m2 == 0.5) || (m1 == -0.5 && m2 == -0.5))
		return (3.0*costheta - 1)*costheta_2/2.0;
	if((m1 == 0.5 && m2 == -0.5))
		return -(3.0*costheta + 1)*sintheta_2/2.0;
	if((m1 == -0.5 && m2 == 0.5))
		return (3.0*costheta + 1)*sintheta_2/2.0;
	if((m1 == 0.5 && m2 == 1.5) || (m1 == -1.5 && m2 == -0.5))
		return sqrt(3.0)*(costheta + 1)*sintheta_2/2.0;
	if((m1 == -0.5 && m2 == -1.5) )
		return -sqrt(3.0)*(costheta + 1)*sintheta_2/2.0;
	if((m1 == 0.5 && m2 == -1.5) || (m1 == 1.5 && m2 == -0.5) || (m1 == -0.5 && m2 == 1.5) )
		return sqrt(3.0)*(1 - costheta)*costheta_2/2.0;
}
else
if(J == 2.5) ////
{
	if((m1 == 0.5 && m2 == 1.5))
		return sqrt(2.0)*(-0.25 + 1.25*costheta*costheta + costheta)*sintheta_2;
	if((m1 == -0.5 && m2 == -1.5))
		return -sqrt(2.0)*(-0.25 + 1.25*costheta*costheta + costheta)*sintheta_2;
	if((m1 == 0.5 && m2 == -1.5) || (m1 == -0.5 && m2 == 1.5))
		return sqrt(2.0)*(0.25 - 1.25*costheta*costheta + costheta)*costheta_2;
	if((m1 == 0.5 && m2 == 0.5) || (m1 == -0.5 && m2 == -0.5))
		return pow(costheta_2, 5.0) - 6.0*pow(costheta_2, 3.0)*pow(sintheta_2, 2.0) + 3.0*pow(sintheta_2, 4.0)*costheta_2;
	if((m1 == 0.5 && m2 == -0.5))
		return -3.0*pow(costheta_2, 4.0)*sintheta_2 + 6.0*pow(costheta_2, 2.0)*pow(sintheta_2, 3.0) - pow(sintheta_2, 5.0);
	if((m1 == -0.5 && m2 == 0.5))
		return 3.0*pow(costheta_2, 4.0)*sintheta_2 - 6.0*pow(costheta_2, 2.0)*pow(sintheta_2, 3.0) + pow(sintheta_2, 5.0);
}
else
if(J == 2.0) //// 
{
	if((m1 == 0.0 && m2 == 0.0))
		return 1.0/2.0*(3.0*costheta*costheta - 1.0);
	if((m1 == 1.0 && m2 == 0.0) || (m1 == 0.0 && m2 == -1.0))
		return -sqrt(3.0/8.0)*sin2theta;
	if((m1 == 0.0 && m2 == 1.0) || (m1 == -1.0 && m2 == 0.0))
		return sqrt(3.0/8.0)*sin2theta;
	if((m1 == 1.0 && m2 == -1.0) || (m1 == -1.0 && m2 == 1.0))
		return 1.0/2.0*(-2.0*costheta*costheta - costheta + 1.0);


}
else
if(J == 3.0) ////
{
	if((m1 == 0.0 && m2 == 0.0))
		return -1.5*costheta + 2.5*costheta*costheta*costheta;
	if((m1 == 1.0 && m2 == 0.0) || (m1 == 0.0 && m2 == -1.0))
		return sqrt(3.0)/4.0*sintheta - 5.0/4.0*sqrt(3.0)*sintheta*costheta*costheta;
	if((m1 == 0.0 && m2 == 1.0) || (m1 == -1.0 && m2 == 0.0))
		return -sqrt(3.0)/4.0*sintheta + 5.0/4.0*sqrt(3.0)*sintheta*costheta*costheta;
}
else
if(J == 3.5) ////
{
	if((m1 == 0.5 && m2 == 0.5) || (m1 == -0.5 && m2 == -0.5) )
		return pow(costheta_2, 7.0) - 12.0*pow(costheta_2, 5.0)*pow(sintheta_2, 2.0) + 18.0*pow(costheta_2, 3.0)*pow(sintheta_2, 4.0) - 4.0*costheta_2*pow(sintheta_2, 6.0);
	if((m1 == 0.5 && m2 == -0.5) )
		return -pow(costheta_2, 6.0)*sintheta_2 + 6.0*pow(costheta_2, 4.0)*pow(sintheta_2, 3.0) - 6.0*pow(costheta_2, 2.0)*pow(sintheta_2, 5.0) + pow(sintheta_2, 7.0);
	if((m1 == -0.5 && m2 == 0.5) )
		return pow(costheta_2, 6.0)*sintheta_2 - 6.0*pow(costheta_2, 4.0)*pow(sintheta_2, 3.0) + 6.0*pow(costheta_2, 2.0)*pow(sintheta_2, 5.0) - pow(sintheta_2, 7.0);


	if((m1 == 1.5 && m2 == 0.5))
		return sqrt(15.0)*( -pow(costheta_2, 6.0)*sintheta_2 + 4.0*pow(costheta_2, 4.0)*pow(sintheta_2, 3.0) - 2.0*pow(costheta_2, 2.0)*pow(sintheta_2, 5.0) );
	if((m1 == -1.5 && m2 == -0.5))
		return -sqrt(15.0)*( -pow(costheta_2, 6.0)*sintheta_2 + 4.0*pow(costheta_2, 4.0)*pow(sintheta_2, 3.0) - 2.0*pow(costheta_2, 2.0)*pow(sintheta_2, 5.0) );

	if((m1 == 1.5 && m2 == -0.5) || (m1 == -1.5 && m2 == 0.5))
		return sqrt(15.0)*( 2.0*pow(costheta_2, 5.0)*pow(sintheta_2, 2.0) - 4.0*pow(costheta_2, 3.0)*pow(sintheta_2, 4.0) + costheta_2*pow(sintheta_2, 6.0) );
}
else
if(J == 4.0) ////
{
	if((m1 == 0.0 && m2 == 0.0))
		return pow((1.0+costheta)/2.0, 4.0) + pow((1.0-costheta)/2.0, 4.0) - 4.0*sintheta*sintheta*pow((1.0+costheta)/2.0, 2.0)
			+9.0/4.0*pow(sintheta, 4.0) - 4.0*sintheta*sintheta*pow((1.0-costheta)/2.0, 2.0);
	

	if((m1 == 1.0 && m2 == 0.0) || (m1 == 0.0 && m2 == -1.0))
		return -1.0/sqrt(5.0)*sintheta*pow((1.0+costheta)/2.0, 3.0) + 3.0*sqrt(5.0)/8.0*sintheta*sintheta*sintheta*(1.0+costheta)/2.0
			-sqrt(5.0)/2.0*sintheta*sintheta*sintheta*(1.0-costheta)/2.0 + sqrt(5.0)/2.0*sintheta*pow((1.0-costheta)/2.0, 3.0);
	if((m1 == 0.0 && m2 == 1.0) || (m1 == -1.0 && m2 == 0.0))
		return 1.0/sqrt(5.0)*sintheta*pow((1.0+costheta)/2.0, 3.0) - 3.0*sqrt(5.0)/8.0*sintheta*sintheta*sintheta*(1.0+costheta)/2.0
			+sqrt(5.0)/2.0*sintheta*sintheta*sintheta*(1.0-costheta)/2.0 - sqrt(5.0)/2.0*sintheta*pow((1.0-costheta)/2.0, 3.0);
	
}
if(J == 4.5) ////
{
	if((m1 == 0.5 && m2 == 0.5) || (m1 == -0.5 && m2 == -0.5) )
		return pow(costheta_2, 9.0) - 20.0*pow(costheta_2, 7.0)*pow(sintheta_2, 2.0) + 60.0*pow(costheta_2, 5.0)*pow(sintheta_2, 4.0) - 120.0*pow(costheta_2, 3.0)*pow(sintheta_2, 6.0) + 5.0*costheta_2*pow(sintheta_2, 8.0);
	if((m1 == 0.5 && m2 == -0.5) )
		return -5.0*pow(costheta_2, 8.0)*sintheta_2 + 120.0*pow(costheta_2, 6.0)*pow(sintheta_2, 3.0) - 60.0*pow(costheta_2, 4.0)*pow(sintheta_2, 5.0) + 20.0*pow(costheta_2, 2.0)*pow(sintheta_2, 7.0) - pow(sintheta_2, 9.0);
	if((m1 == -0.5 && m2 == 0.5) )
		return 5.0*pow(costheta_2, 8.0)*sintheta_2 - 120.0*pow(costheta_2, 6.0)*pow(sintheta_2, 3.0) + 60.0*pow(costheta_2, 4.0)*pow(sintheta_2, 5.0) - 20.0*pow(costheta_2, 2.0)*pow(sintheta_2, 7.0) + pow(sintheta_2, 9.0);


	if((m1 == 1.5 && m2 == 0.5) )
		return sqrt(6.0)*( -2.0*pow(costheta_2, 8.0)*sintheta_2 + 7.5*pow(costheta_2, 6.0)*pow(sintheta_2, 3.0) - 20.0*pow(costheta_2, 4.0)*pow(sintheta_2, 5.0) + 5.0*pow(costheta_2, 2.0)*pow(sintheta_2, 7.0)  );
	if((m1 == -1.5 && m2 == -0.5) )
		return sqrt(6.0)*( 2.0*pow(costheta_2, 8.0)*sintheta_2 - 7.5*pow(costheta_2, 6.0)*pow(sintheta_2, 3.0) + 20.0*pow(costheta_2, 4.0)*pow(sintheta_2, 5.0) - 5.0*pow(costheta_2, 2.0)*pow(sintheta_2, 7.0)  );

	if((m1 == 1.5 && m2 == -0.5) || (m1 == 1.5 && m2 == -0.5) )
		return sqrt(6.0)*( 5.0*pow(costheta_2, 7.0)*pow(sintheta_2, 2.0) -20*pow(costheta_2, 5.0)*pow(sintheta_2, 4.0) +7.5*pow(costheta_2, 3.0)*pow(sintheta_2, 6.0) - 2.0*costheta_2*pow(sintheta_2, 8.0)  );


}



return 0.0;

}

std::complex<float> make_complex(float a, float p) //this function construct complex type using amplitude and phase
{
    return std::complex<float>(a * cos(p), a * sin(p));
}

double BlattWeisskopf(double p0, double p1, double d, int L)
{

	if(L==0) return 1.0;

	float c1 = p0*d*p0*d;
	float c2 = p1*d*p1*d;
	
	if(L==1) return sqrt((1 + c1)/(1 + c2));
	
	if(L==2) return sqrt((9.0 + 3.0*c1 + c1*c1)/(9.0 + 3.0*c2 + c2*c2));
	
	if(L==3) return sqrt((225.0 + 45.0*c1 + 6.0*c1*c1 + c1*c1*c1)/(225.0 + 45.0*c2 + 6.0*c2*c2 + c2*c2*c2));
	
	float c14 = c1*c1*c1*c1;
	float c24 = c2*c2*c2*c2;
		
	if(L==4) return sqrt(   ( 11025.0 + 1575.0*c1 + 135.0*c1*c1 + 10.0*c1*c1*c1 + c14 )/
				( 11025.0 + 1575.0*c2 + 135.0*c2*c2 + 10.0*c1*c1*c1 + c24 ) );
	
	if(L==5) return sqrt(   (893025.0 + 99225.0*c1 + 6300.0*c1*c1 + 315.0*c1*c1*c1 + 15.0*c14 + c14*c1)/
			        (893025.0 + 99225.0*c2 + 6300.0*c2*c2 + 315.0*c2*c2*c2 + 15.0*c24 + c24*c2) );
	return 0.0;


}

std::complex<float> BreitWignerX( float MX, float massB, float M0X, float gamma0, int LX )
{
std::complex<float> ix(0,1);
const float massProton = 0.938272;
const float massJpsi = 3.096916;
const float massKaon = 0.493667;
const float massPion = 0.13957;
//const float massLambdaB = MASS_LB;
//massB = MASS_BD;
float mplusX = massPion + massJpsi; /// mass JPsi + mass proton
float spinX = 1.;
//if(M0X < mplusX) return 0.0;
//if(MX < mplusX)  return 0.0;
//float q0 = sqrt(M0X*M0X - mplusX*mplusX); 
//float q1 = sqrt(MX*MX - mplusX*mplusX);


float argq0 = pow( M0X*M0X - massJpsi*massJpsi - massPion*massPion, 2.0 ) - 4.0 * massJpsi*massJpsi*massPion*massPion;
float argq1 = pow( MX*MX - massJpsi*massJpsi - massPion*massPion, 2.0 ) - 4.0 * massJpsi*massJpsi*massPion*massPion;
if(argq0 < 0.0 || argq1 < 0.0) return 0.0;
float q0 = sqrt(argq0)/2.0/M0X;
float q1 = sqrt(argq1)/2.0/MX;


float argp0 = pow( massB*massB - M0X*M0X - massKaon*massKaon, 2.0 ) - 4.0 * M0X*M0X*massKaon*massKaon;
float argp1 = pow( massB*massB - MX*MX - massKaon*massKaon, 2.0 ) - 4.0 * MX*MX*massKaon*massKaon;
if(argp0 < 0.0 || argp1 < 0.0) return 0.0;
float p0 = sqrt(argp0)/2.0/massB;
float p1 = sqrt(argp1)/2.0/massB;

float gamma = gamma0 * pow( q1/q0, 2*LX + 1) * M0X / MX * powf(BlattWeisskopf(q0, q1, 3.0, LX), 2.0);

float nom = 1.0;
//if(BW_nom_X==1) nom = MX*sqrt(M0X*gamma); //commented
std::complex<float> denom = nom/( M0X*M0X - MX*MX - ix*M0X*gamma );
///std::complex<float> Rm = denom * BlattWeisskopf(q0, q1, 3.0, LX) * pow( q1/M0X, LX );

const int LB = spinX;
std::complex<float> Rm = (float)(BlattWeisskopf(p0, p1, 3.0, LB) * pow( p1/massB, LB )) * denom * (float)(BlattWeisskopf(q0, q1, 3.0, LX) * pow( q1/M0X, LX ));

return Rm;
}

std::complex<float> BreitWignerX1( float MX, float massB, float M0X, float gamma0, int LX )
{
std::complex<float> ix(0,1);
const float massProton = 0.938272;
const float massJpsi = 3.096916;
const float massKaon = 0.493667;
const float massPion = 0.13957;
//const float massLambdaB = MASS_LB;
//massB = MASS_BD;
float mplusX = massPion + massJpsi; /// mass JPsi + mass proton
float spinX = 1.;
//if(M0X < mplusX) return 0.0;
//if(MX < mplusX)  return 0.0;
//float q0 = sqrt(M0X*M0X - mplusX*mplusX); 
//float q1 = sqrt(MX*MX - mplusX*mplusX);


float argq0 = pow( M0X*M0X - massJpsi*massJpsi - massPion*massPion, 2.0 ) - 4.0 * massJpsi*massJpsi*massPion*massPion;
float argq1 = pow( MX*MX - massJpsi*massJpsi - massPion*massPion, 2.0 ) - 4.0 * massJpsi*massJpsi*massPion*massPion;
if(argq0 < 0.0 || argq1 < 0.0) return 0.0;
float q0 = sqrt(argq0)/2.0/M0X;
float q1 = sqrt(argq1)/2.0/MX;


float argp0 = pow( massB*massB - M0X*M0X - massKaon*massKaon, 2.0 ) - 4.0 * M0X*M0X*massKaon*massKaon;
float argp1 = pow( massB*massB - MX*MX - massKaon*massKaon, 2.0 ) - 4.0 * MX*MX*massKaon*massKaon;
if(argp0 < 0.0 || argp1 < 0.0) return 0.0;
float p0 = sqrt(argp0)/2.0/massB;
float p1 = sqrt(argp1)/2.0/massB;

float gamma = gamma0 * pow( q1/q0, 2*LX + 1) * M0X / MX * powf(BlattWeisskopf(q0, q1, 3.0, LX), 2.0);

float nom = 1.0;
//if(BW_nom_X==1) nom = MX*sqrt(M0X*gamma); //commented
std::complex<float> denom = nom/( M0X*M0X - MX*MX - ix*M0X*gamma );
///std::complex<float> Rm = denom * BlattWeisskopf(q0, q1, 3.0, LX) * pow( q1/M0X, LX );

const int LB = spinX;
std::complex<float> Rm = (float)(BlattWeisskopf(p0, p1, 3.0, LB) * pow( p1/massB, LB )) * denom * (float)(BlattWeisskopf(q0, q1, 3.0, LX) * pow( q1/M0X, LX ));

return Rm;
}

std::complex<float> BreitWignerX2( float MX, float massB, float M0X, float gamma0, int LX )
{
std::complex<float> ix(0,1);
const float massProton = 0.938272;
const float massJpsi = 3.096916;
const float massKaon = 0.493667;
const float massPion = 0.13957;
//const float massLambdaB = MASS_LB;
//massB = MASS_BD;
float mplusX = massPion + massJpsi; /// mass JPsi + mass proton
float spinX = 2.;
//if(M0X < mplusX) return 0.0;
//if(MX < mplusX)  return 0.0;
//float q0 = sqrt(M0X*M0X - mplusX*mplusX); 
//float q1 = sqrt(MX*MX - mplusX*mplusX);


float argq0 = pow( M0X*M0X - massJpsi*massJpsi - massPion*massPion, 2.0 ) - 4.0 * massJpsi*massJpsi*massPion*massPion;
float argq1 = pow( MX*MX - massJpsi*massJpsi - massPion*massPion, 2.0 ) - 4.0 * massJpsi*massJpsi*massPion*massPion;
if(argq0 < 0.0 || argq1 < 0.0) return 0.0;
float q0 = sqrt(argq0)/2.0/M0X;
float q1 = sqrt(argq1)/2.0/MX;


float argp0 = pow( massB*massB - M0X*M0X - massKaon*massKaon, 2.0 ) - 4.0 * M0X*M0X*massKaon*massKaon;
float argp1 = pow( massB*massB - MX*MX - massKaon*massKaon, 2.0 ) - 4.0 * MX*MX*massKaon*massKaon;
if(argp0 < 0.0 || argp1 < 0.0) return 0.0;
float p0 = sqrt(argp0)/2.0/massB;
float p1 = sqrt(argp1)/2.0/massB;

float gamma = gamma0 * pow( q1/q0, 2*LX + 1) * M0X / MX * powf(BlattWeisskopf(q0, q1, 3.0, LX), 2.0);

float nom = 1.0;
//if(BW_nom_X==1) nom = MX*sqrt(M0X*gamma); //commented
std::complex<float> denom = nom/( M0X*M0X - MX*MX - ix*M0X*gamma );
///std::complex<float> Rm = denom * BlattWeisskopf(q0, q1, 3.0, LX) * pow( q1/M0X, LX );

const int LB = spinX;
std::complex<float> Rm = (float)(BlattWeisskopf(p0, p1, 3.0, LB) * pow( p1/massB, LB )) * denom * (float)(BlattWeisskopf(q0, q1, 3.0, LX) * pow( q1/M0X, LX ));

return Rm;
}

std::complex<float> BreitWignerKstar( float MK, float M0K, float massB, float gamma0, int LK, int LB )
{
std::complex<float> ix(0,1);
const float massProton = 0.938272;
const float massJpsi = 3.096916;
const float massKaon = 0.493667;
const float massPion = 0.13957;
//const float massLambdaB = MASS_LB;
//const float massB = MASS_BD;
const float mplusK = massPion + massKaon; /// mass JPsi + mass proton

//massB = MASS_BD;

float argq0 = pow( M0K*M0K - massPion*massPion - massKaon*massKaon, 2.0 ) - 4.0 * massPion*massPion*massKaon*massKaon;
float argq1 = pow( MK*MK - massPion*massPion - massKaon*massKaon, 2.0 ) - 4.0 * massPion*massPion*massKaon*massKaon;
if(argq0 < 0.0 || argq1 < 0.0) return 0.0;
float q0 = sqrt(argq0)/2.0/M0K;
float q1 = sqrt(argq1)/2.0/MK;


float argp0 = pow( massB*massB - M0K*M0K - massJpsi*massJpsi, 2.0 ) - 4.0 * M0K*M0K*massJpsi*massJpsi;
float argp1 = pow( massB*massB - MK*MK - massJpsi*massJpsi, 2.0 ) - 4.0 * MK*MK*massJpsi*massJpsi;
if(argp0 < 0.0 || argp1 < 0.0) return 0.0;
float p0 = sqrt(argp0)/2.0/massB;
float p1 = sqrt(argp1)/2.0/massB;


float gamma = gamma0 * pow( q1/q0, 2*LK + 1) * M0K / MK * powf(BlattWeisskopf(q0, q1, 3.0, LK), 2.0);

if(gamma != gamma ){printf("in BWkstar gamma is nan"); getchar();}

float nom = 1.0;
//if(BW_nom_Kstar==1) nom = MK*sqrt(M0K*gamma); //commented
std::complex<float> denom = nom/( M0K*M0K - MK*MK - ix*M0K*gamma );

float BlWsLB = BlattWeisskopf(p0, p1, 3.0, LB);
float BlWsLK = BlattWeisskopf(q0, q1, 3.0, LK);

if(BlWsLB != BlWsLB) { printf("blatt LB is NaN, p0=%10.2e\tp1=%10.2e\tLB=%d", p0, p1, LB); getchar(); }
if(BlWsLK != BlWsLK) { printf("blatt LK is NaN, q0=%10.2e\tq1=%10.2e\tLK=%d", q0, q1, LK); getchar(); }

std::complex<float> Rm = (float)(BlWsLB*pow( p1/massB, LB ))*denom*(float)(BlWsLK*pow( q1/M0K, LK ));

/*
if(Rm != Rm){
printf("\nBW: %10.2e ", abs(denom));
printf("%10.1e ", BlattWeisskopf(p0, p1, 3.0, LB));
printf("%10.1e ", BlattWeisskopf(q0, q1, 3.0, LK));
printf("%10.1e ", pow( p1/massB, LB ) );
printf("%10.1e ", pow( q1/M0K, LK ) );
printf("%10.4e ", massB );
printf("%10.4e ", mplusB1 );
printf("%10.4e ", MK );
getchar();
}*/

return Rm;
}

std::complex<float> HamplitudeX0minus(float M, float massB, float M0, float gamma, std::complex<float> Bs1, std::complex<float> Bs2, float hmu, float thetaX, float thetaPsi, float phimu, float alpha_mu )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);

std::complex<float> ealpha = cosf(hmu*alpha_mu) + ix * sinf(hmu*alpha_mu);

std::complex<float> Hs1, Hs2, Hs3;

std::complex<float> Ampl1;
Ampl1 = 0.0;

float spinX = 0.;
int parityX = -1;

/////spinX 2
	if(spinX == 0 && parityX == -1){
	Hs1 = -Bs1*BreitWignerX( M, massB, M0, gamma, 1);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi);		

	}

	if(spinX == 1 && parityX == 1){
	Hs1 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerX1( M, massB, M0, gamma, 0) - (float)(sqrt(2.0/3.0))*Bs2*BreitWignerX1( M, massB, M0, gamma, 2);
	Hs2 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerX1( M, massB, M0, gamma, 0) + (float)(sqrt(1.0/6.0))*Bs2*BreitWignerX1( M, massB, M0, gamma, 2);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0,-1.0, thetaX) * emu2;

	}
	if(spinX == 2 && parityX == -1){
	Hs1 = (float)(sqrt(2.0/5.0))*Bs1*BreitWignerX2( M, massB, M0, gamma, 1) - (float)(sqrt(3.0/5.0))*Bs2*BreitWignerX2( M, massB, M0, gamma, 3);
	Hs2 = (float)(sqrt(3.0/10.0))*Bs1*BreitWignerX2( M, massB, M0, gamma, 1) + (float)(sqrt(1.0/5.0))*Bs2*BreitWignerX2( M, massB, M0, gamma, 3);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}
	
	if(spinX == 2 && parityX == 1){
	Hs2 = (float)(-sqrt(0.5))*Bs1*BreitWignerX2( M, massB, M0, gamma, 2);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 -= Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}

	////Ampl1 *= (float)B0Factor( M, M0, spinX );


return Ampl1*ealpha;
}

std::complex<float> HamplitudeX1minus(float M, float massB, float M0, float gamma, std::complex<float> Bs1, std::complex<float> Bs2, float hmu, float thetaX, float thetaPsi, float phimu, float alpha_mu )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);

std::complex<float> ealpha = cosf(hmu*alpha_mu) + ix * sinf(hmu*alpha_mu);

std::complex<float> Hs1, Hs2, Hs3;

std::complex<float> Ampl1;
Ampl1 = 0.0;

float spinX = 1.;
int parityX = -1;

/////spinX 2
	if(spinX == 0 && parityX == -1){
	Hs1 = -Bs1*BreitWignerX( M, massB, M0, gamma, 1);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi);		

	}

	if(spinX == 1 && parityX == 1){
	Hs1 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerX1( M, massB, M0, gamma, 0) - (float)(sqrt(2.0/3.0))*Bs2*BreitWignerX1( M, massB, M0, gamma, 2);
	Hs2 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerX1( M, massB, M0, gamma, 0) + (float)(sqrt(1.0/6.0))*Bs2*BreitWignerX1( M, massB, M0, gamma, 2);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0,-1.0, thetaX) * emu2;

	}

    if(spinX == 1 && parityX == -1){	
    Hs1 = Bs1*BreitWignerX1( M, massB, M0, gamma, 1);	    
        Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 0.0, thetaX); 	}
	if(spinX == 2 && parityX == -1){
	Hs1 = (float)(sqrt(2.0/5.0))*Bs1*BreitWignerX( M, massB, M0, gamma, 1) - (float)(sqrt(3.0/5.0))*Bs2*BreitWignerX( M, massB, M0, gamma, 3);
	Hs2 = (float)(sqrt(3.0/10.0))*Bs1*BreitWignerX( M, massB, M0, gamma, 1) + (float)(sqrt(1.0/5.0))*Bs2*BreitWignerX( M, massB, M0, gamma, 3);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}
	
	if(spinX == 2 && parityX == 1){
	Hs2 = (float)(-sqrt(0.5))*Bs1*BreitWignerX( M, massB, M0, gamma, 2);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 -= Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}

	////Ampl1 *= (float)B0Factor( M, M0, spinX );


return Ampl1*ealpha;
}

std::complex<float> HamplitudeX1plus(float M, float massB, float M0, float gamma, std::complex<float> Bs1, std::complex<float> Bs2, float hmu, float thetaX, float thetaPsi, float phimu, float alpha_mu )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);

std::complex<float> ealpha = cosf(hmu*alpha_mu) + ix * sinf(hmu*alpha_mu);

std::complex<float> Hs1, Hs2, Hs3;

std::complex<float> Ampl1;
Ampl1 = 0.0;

float spinX = 1.;
int parityX = 1;

/////spinX 2
	if(spinX == 0 && parityX == -1){
	Hs1 = -Bs1*BreitWignerX( M, massB, M0, gamma, 1);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi);		

	}

	if(spinX == 1 && parityX == 1){
	Hs1 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerX1( M, massB, M0, gamma, 0) - (float)(sqrt(2.0/3.0))*Bs2*BreitWignerX1( M, massB, M0, gamma, 2);
	Hs2 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerX1( M, massB, M0, gamma, 0) + (float)(sqrt(1.0/6.0))*Bs2*BreitWignerX1( M, massB, M0, gamma, 2);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0,-1.0, thetaX) * emu2;

	}
	if(spinX == 2 && parityX == -1){
	Hs1 = (float)(sqrt(2.0/5.0))*Bs1*BreitWignerX( M, massB, M0, gamma, 1) - (float)(sqrt(3.0/5.0))*Bs2*BreitWignerX( M, massB, M0, gamma, 3);
	Hs2 = (float)(sqrt(3.0/10.0))*Bs1*BreitWignerX( M, massB, M0, gamma, 1) + (float)(sqrt(1.0/5.0))*Bs2*BreitWignerX( M, massB, M0, gamma, 3);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}
	
	if(spinX == 2 && parityX == 1){
	Hs2 = (float)(-sqrt(0.5))*Bs1*BreitWignerX( M, massB, M0, gamma, 2);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 -= Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}

	////Ampl1 *= (float)B0Factor( M, M0, spinX );


return Ampl1*ealpha;
}

std::complex<float> HamplitudeX2minus(float M, float massB, float M0, float gamma, std::complex<float> Bs1, std::complex<float> Bs2, float hmu, float thetaX, float thetaPsi, float phimu, float alpha_mu )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);

std::complex<float> ealpha = cosf(hmu*alpha_mu) + ix * sinf(hmu*alpha_mu);

std::complex<float> Hs1, Hs2, Hs3;

std::complex<float> Ampl1;
Ampl1 = 0.0;

float spinX = 2;
int parityX = -1;

/////spinX 2
	if(spinX == 0 && parityX == -1){
	Hs1 = -Bs1*BreitWignerX( M, massB, M0, gamma, 1);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi);		

	}

	if(spinX == 1 && parityX == 1){
	Hs1 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerX1( M, massB, M0, gamma, 0) - (float)(sqrt(2.0/3.0))*Bs2*BreitWignerX1( M, massB, M0, gamma, 2);
	Hs2 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerX1( M, massB, M0, gamma, 0) + (float)(sqrt(1.0/6.0))*Bs2*BreitWignerX1( M, massB, M0, gamma, 2);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0,-1.0, thetaX) * emu2;

	}
	if(spinX == 2 && parityX == -1){
	Hs1 = (float)(sqrt(2.0/5.0))*Bs1*BreitWignerX2( M, massB, M0, gamma, 1) - (float)(sqrt(3.0/5.0))*Bs2*BreitWignerX2( M, massB, M0, gamma, 3);
	Hs2 = (float)(sqrt(3.0/10.0))*Bs1*BreitWignerX2( M, massB, M0, gamma, 1) + (float)(sqrt(1.0/5.0))*Bs2*BreitWignerX2( M, massB, M0, gamma, 3);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}
	
	if(spinX == 2 && parityX == 1){
	Hs2 = (float)(-sqrt(0.5))*Bs1*BreitWignerX2( M, massB, M0, gamma, 2);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 -= Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}

	////Ampl1 *= (float)B0Factor( M, M0, spinX );


return Ampl1*ealpha;
}

std::complex<float> HamplitudeX2plus(float M, float massB, float M0, float gamma, std::complex<float> Bs1, std::complex<float> Bs2, float hmu, float thetaX, float thetaPsi, float phimu, float alpha_mu )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);

std::complex<float> ealpha = cosf(hmu*alpha_mu) + ix * sinf(hmu*alpha_mu);

std::complex<float> Hs1, Hs2, Hs3;

std::complex<float> Ampl1;
Ampl1 = 0.0;

float spinX = 2.;
int parityX = 1;

/////spinX 2
	if(spinX == 0 && parityX == -1){
	Hs1 = -Bs1*BreitWignerX( M, massB, M0, gamma, 1);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi);		

	}

	if(spinX == 1 && parityX == 1){
	Hs1 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerX1( M, massB, M0, gamma, 0) - (float)(sqrt(2.0/3.0))*Bs2*BreitWignerX1( M, massB, M0, gamma, 2);
	Hs2 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerX1( M, massB, M0, gamma, 0) + (float)(sqrt(1.0/6.0))*Bs2*BreitWignerX1( M, massB, M0, gamma, 2);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0,-1.0, thetaX) * emu2;

	}
	if(spinX == 2 && parityX == -1){
	Hs1 = (float)(sqrt(2.0/5.0))*Bs1*BreitWignerX2( M, massB, M0, gamma, 1) - (float)(sqrt(3.0/5.0))*Bs2*BreitWignerX2( M, massB, M0, gamma, 3);
	Hs2 = (float)(sqrt(3.0/10.0))*Bs1*BreitWignerX2( M, massB, M0, gamma, 1) + (float)(sqrt(1.0/5.0))*Bs2*BreitWignerX2( M, massB, M0, gamma, 3);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}
	
	if(spinX == 2 && parityX == 1){
	Hs2 = (float)(-sqrt(0.5))*Bs1*BreitWignerX2( M, massB, M0, gamma, 2);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 -= Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}

	////Ampl1 *= (float)B0Factor( M, M0, spinX );


return Ampl1*ealpha;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::complex<float> HamplitudeKstar0(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);

std::complex<float> BrWig1 = BreitWignerKstar( M, M0, MB, gamma, 1, 0);

if(BrWig1 != BrWig1){
printf("BrWig1 Kstar0 is nan");
printf("%10.2e %10.2e %10.2e %10.2e", M, M0, MB, gamma);
getchar();
}

std::complex<float>	Hs1 = -Bw1*BrWig1;

std::complex<float> Ampl1 = 0.0;

Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi);

return Ampl1;
}

std::complex<float> BreitWignerKstarNR( float MK, float M0K, float massB, float gamma0, int LK, int LB )
{
std::complex<float> ix(0,1);
const float massProton = MASS_PROTON;
const float massJpsi = MASS_JPSI;
const float massKaon = MASS_KAON;
const float massPion = MASS_PION;
const float massLambdaB = MASS_LB;
//const float massB = MASS_BD;
const float mplusK = massPion + massKaon; /// mass JPsi + mass proton

massB = MASS_BD;

float argq0 = pow( M0K*M0K - massPion*massPion - massKaon*massKaon, 2.0 ) - 4.0 * massPion*massPion*massKaon*massKaon;
float argq1 = pow( MK*MK - massPion*massPion - massKaon*massKaon, 2.0 ) - 4.0 * massPion*massPion*massKaon*massKaon;
if(argq0 < 0.0 || argq1 < 0.0) return 0.0;
float q0 = sqrt(argq0)/2.0/M0K;
float q1 = sqrt(argq1)/2.0/MK;


float argp0 = pow( massB*massB - M0K*M0K - massJpsi*massJpsi, 2.0 ) - 4.0 * M0K*M0K*massJpsi*massJpsi;
float argp1 = pow( massB*massB - MK*MK - massJpsi*massJpsi, 2.0 ) - 4.0 * MK*MK*massJpsi*massJpsi;
if(argp0 < 0.0 || argp1 < 0.0) return 0.0;
float p0 = sqrt(argp0)/2.0/massB;
float p1 = sqrt(argp1)/2.0/massB;


float gamma = gamma0 * pow( q1/q0, 2*LK + 1) * M0K / MK * powf(BlattWeisskopf(q0, q1, 3.0, LK), 2.0);

if(gamma != gamma ){printf("in BWkstar gamma is nan"); getchar();}

float BlWsLB = BlattWeisskopf(p0, p1, 3.0, LB);
float BlWsLK = BlattWeisskopf(q0, q1, 3.0, LK);

if(BlWsLB != BlWsLB) { printf("blatt LB is NaN, p0=%10.2e\tp1=%10.2e\tLB=%d", p0, p1, LB); getchar(); }
if(BlWsLK != BlWsLK) { printf("blatt LK is NaN, q0=%10.2e\tq1=%10.2e\tLK=%d", q0, q1, LK); getchar(); }

std::complex<float> Rm = (float)(BlWsLB*pow( p1/massB, LB ))*(float)(BlWsLK*pow( q1/M0K, LK ));

/*
if(Rm != Rm){
printf("\nBW: %10.2e ", abs(denom));
printf("%10.1e ", BlattWeisskopf(p0, p1, 3.0, LB));
printf("%10.1e ", BlattWeisskopf(q0, q1, 3.0, LK));
printf("%10.1e ", pow( p1/massB, LB ) );
printf("%10.1e ", pow( q1/M0K, LK ) );
printf("%10.4e ", massB );
printf("%10.4e ", mplusB1 );
printf("%10.4e ", MK );
getchar();
}*/

return Rm;
}
/////////////==============================================================================


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
std::complex<float> HamplitudeKstarNR0(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);

std::complex<float> BrWig1 = BreitWignerKstarNR( M, M0, MB, gamma, 1, 0);

if(BrWig1 != BrWig1){
printf("BrWig1 Kstar0 is nan");
printf("%10.2e %10.2e %10.2e %10.2e", M, M0, MB, gamma);
getchar();
}

std::complex<float>	Hs1 = -Bw1*BrWig1;

std::complex<float> Ampl1 = 0.0;

Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi);

return Ampl1;
}



std::complex<float> HamplitudeKstar1(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK )
{


if(hmu != hmu || thetaKstar != thetaKstar || thetaPsi != thetaPsi || phimu != phimu || phiK != phiK)
{printf("input is nan"); getchar(); }

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);
std::complex<float> eK1 = cos(phiK) + ix*sin(phiK);//exp(ix*phiK);
std::complex<float> eK2 = cos(phiK) - ix*sin(phiK);

std::complex<float> BrWig1 = BreitWignerKstar( M, M0, MB, gamma, 0, 1);
std::complex<float> BrWig2 = BreitWignerKstar( M, M0, MB, gamma, 1, 1);
std::complex<float> BrWig3 = BreitWignerKstar( M, M0, MB, gamma, 2, 1);

if(BrWig1 != BrWig1 || BrWig2 != BrWig2 || BrWig3 != BrWig3){
printf("BrWig123 Kstar1 is nan");
printf("%10.2e %10.2e %10.2e %10.2e", M, M0, MB, gamma);
getchar();
}

std::complex<float>	Hs1 = -(float)(sqrt(1.0/3.0))*Bw1*BrWig1 + (float)(sqrt(2.0/3.0))*Bw3*BrWig3;
std::complex<float>	Hs2 = (float)(sqrt(1.0/3.0))*Bw1*BrWig1 - (float)(sqrt(1.0/2.0))*Bw2*BrWig2 + (float)(sqrt(1.0/6.0))*Bw3*BrWig3;
std::complex<float>	Hs3 = (float)(sqrt(1.0/3.0))*Bw1*BrWig1 + (float)(sqrt(1.0/2.0))*Bw2*BrWig2 + (float)(sqrt(1.0/6.0))*Bw3*BrWig3;


if(Bw1 != Bw1 || Bw2 != Bw2 || Bw3 != Bw3)
{
printf("Bw is nan"); getchar();
}

if(Hs1 != Hs1 || Hs2 != Hs2 || Hs3 != Hs3)
{
printf("Hs is nan"); getchar();
}

float wmpsi1 = (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi);
float wmpsi2 = (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi);
float wmpsi3 = (float)Wigner_matrix(1.0, -1.0, hmu, thetaPsi);
float wmkst1 = (float)Wigner_matrix(1.0, 0.0, 0.0, thetaKstar);
float wmkst2 = (float)Wigner_matrix(1.0, 1.0, 0.0, thetaKstar);
float wmkst3 = (float)Wigner_matrix(1.0, -1.0, 0.0, thetaKstar);

if(wmpsi1 != wmpsi1 || wmpsi2 != wmpsi2 || wmpsi3 != wmpsi3)
{printf("wmpsi is nan"); getchar(); }
if(wmkst1 != wmkst1 || wmkst2 != wmkst2 || wmkst3 != wmkst3)
{printf("wmkst is nan"); getchar(); }


std::complex<float> Ampl1 = 0.0;
Ampl1 += Hs1 * wmpsi1 * wmkst1;
Ampl1 += Hs2 * wmpsi2 * wmkst2 * emu1 * eK1;
Ampl1 += Hs3 * wmpsi3 * wmkst3 * emu2 * eK2;


return Ampl1;
}

std::complex<float> HamplitudeKstar2(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);
std::complex<float> eK1 = cos(phiK) + ix*sin(phiK);//exp(ix*phiK);
std::complex<float> eK2 = cos(phiK) - ix*sin(phiK);

std::complex<float> BrWig1 = BreitWignerKstar( M, M0, MB, gamma, 1, 2);
std::complex<float> BrWig2 = BreitWignerKstar( M, M0, MB, gamma, 2, 2);
std::complex<float> BrWig3 = BreitWignerKstar( M, M0, MB, gamma, 3, 2);

if(BrWig1 != BrWig1 || BrWig2 != BrWig2 || BrWig3 != BrWig3){
printf("BrWig123 Kstar2 is nan");
printf("%10.2e %10.2e %10.2e %10.2e", M, M0, MB, gamma);
getchar();
}

std::complex<float>	Hs1 = (float)(sqrt(2.0/5.0))*Bw1*BrWig1 - (float)(sqrt(3.0/5.0))*Bw3*BrWig3;
std::complex<float>	Hs2 = -(float)(sqrt(3.0/10.0))*Bw1*BrWig1 + (float)(sqrt(1.0/2.0))*Bw2*BrWig2 - (float)(sqrt(1.0/5.0))*Bw3*BrWig3;
std::complex<float>	Hs3 = -(float)(sqrt(3.0/10.0))*Bw1*BrWig1 - (float)(sqrt(1.0/2.0))*Bw2*BrWig2 - (float)(sqrt(1.0/5.0))*Bw3*BrWig3;


std::complex<float> Ampl1 = 0.0;
Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 0.0, thetaKstar);
Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 1.0, 0.0, thetaKstar) * emu1 * eK1;
Ampl1 += Hs3 * (float)Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, -1.0, 0.0, thetaKstar) * emu2 * eK2;

if(Ampl1 != Ampl1){ 
	printf("BWL1 %10.2e\n", abs(BreitWignerKstar( M, M0, MB, gamma, 1, 2)) ); 
	printf("BWL3 %10.2e\n", abs(BreitWignerKstar( M, M0, MB, gamma, 3, 2)) ); getchar(); }

return Ampl1;
}


std::complex<float> HamplitudeKstar3(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);
std::complex<float> eK1 = cos(phiK) + ix*sin(phiK);//exp(ix*phiK);
std::complex<float> eK2 = cos(phiK) - ix*sin(phiK);

std::complex<float> BrWig1 = BreitWignerKstar( M, M0, MB, gamma, 2, 3);
std::complex<float> BrWig2 = BreitWignerKstar( M, M0, MB, gamma, 3, 3);
std::complex<float> BrWig3 = BreitWignerKstar( M, M0, MB, gamma, 4, 3);

if(BrWig1 != BrWig1 || BrWig2 != BrWig2 || BrWig3 != BrWig3){
printf("BrWig123 Kstar3 is nan");
printf("%10.2e %10.2e %10.2e %10.2e", M, M0, MB, gamma);
getchar();
}

std::complex<float>	Hs1 = -(float)(sqrt(3.0/7.0))*Bw1*BrWig1 + (float)(sqrt(4.0/7.0))*Bw3*BrWig3;
std::complex<float>	Hs2 = (float)(sqrt(2.0/7.0))*Bw1*BrWig1 - (float)(sqrt(1.0/2.0))*Bw2*BrWig2 + (float)(sqrt(3.0/14.0))*Bw3*BrWig3;
std::complex<float>	Hs3 = (float)(sqrt(2.0/7.0))*Bw1*BrWig1 + (float)(sqrt(1.0/2.0))*Bw2*BrWig2 + (float)(sqrt(3.0/14.0))*Bw3*BrWig3;


std::complex<float> Ampl1 = 0.0;
Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(3.0, 0.0, 0.0, thetaKstar);
Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(3.0, 1.0, 0.0, thetaKstar) * emu1 * eK1;
Ampl1 += Hs3 * (float)Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * (float)Wigner_matrix(3.0, -1.0, 0.0, thetaKstar) * emu2 * eK2;


return Ampl1;
}




std::complex<float> HamplitudeKstar4(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);
std::complex<float> eK1 = cos(phiK) + ix*sin(phiK);//exp(ix*phiK);
std::complex<float> eK2 = cos(phiK) - ix*sin(phiK);


std::complex<float> BrWig1 = BreitWignerKstar( M, M0, MB, gamma, 3, 4);
std::complex<float> BrWig2 = BreitWignerKstar( M, M0, MB, gamma, 4, 4);
std::complex<float> BrWig3 = BreitWignerKstar( M, M0, MB, gamma, 5, 4);

if(BrWig1 != BrWig1 || BrWig2 != BrWig2 || BrWig3 != BrWig3){
printf("BrWig123 Kstar4 is nan");
printf("%10.2e %10.2e %10.2e %10.2e", M, M0, MB, gamma);
getchar();
}

std::complex<float>	Hs1 = (float)(sqrt(4.0/9.0))*Bw1*BrWig1 - (float)(sqrt(5.0/9.0))*Bw3*BrWig3;
std::complex<float>	Hs2 = -(float)(1.0/3.0*sqrt(5.0/2.0))*Bw1*BrWig1 + (float)(sqrt(1.0/2.0))*Bw2*BrWig2 - (float)(sqrt(2.0/9.0))*Bw3*BrWig3;
std::complex<float>	Hs3 = -(float)(1.0/3.0*sqrt(5.0/2.0))*Bw1*BrWig1 - (float)(sqrt(1.0/2.0))*Bw2*BrWig2 - (float)(sqrt(2.0/9.0))*Bw3*BrWig3;



std::complex<float> Ampl1 = 0.0;
Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(4.0, 0.0, 0.0, thetaKstar);
Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(4.0, 1.0, 0.0, thetaKstar) * emu1 * eK1;
Ampl1 += Hs3 * (float)Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * (float)Wigner_matrix(4.0, -1.0, 0.0, thetaKstar) * emu2 * eK2;


return Ampl1;
}





float HamplitudeXKstar_total(float MX, float MK, float MB, float M01, float M02, float gamma1, float gamma2,
						std::complex<float> X1Bs1, std::complex<float> X1Bs2,
						std::complex<float> X2Bs1, std::complex<float> X2Bs2,
						std::complex<float> K1410_1_Bw1, std::complex<float> K1410_1_Bw2, std::complex<float> K1410_1_Bw3,
						std::complex<float> K1430_0_Bw1, std::complex<float> K1430_0_Bw2, std::complex<float> K1430_0_Bw3,
						std::complex<float> K1430_2_Bw1, std::complex<float> K1430_2_Bw2, std::complex<float> K1430_2_Bw3,
						std::complex<float> K1680_1_Bw1, std::complex<float> K1680_1_Bw2, std::complex<float> K1680_1_Bw3,
						std::complex<float> K1780_3_Bw1, std::complex<float> K1780_3_Bw2, std::complex<float> K1780_3_Bw3,
						std::complex<float> K1950_0_Bw1, std::complex<float> K1950_0_Bw2, std::complex<float> K1950_0_Bw3,
						std::complex<float> K1980_2_Bw1, std::complex<float> K1980_2_Bw2, std::complex<float> K1980_2_Bw3,
						std::complex<float> K2045_4_Bw1, std::complex<float> K2045_4_Bw2, std::complex<float> K2045_4_Bw3,
						std::complex<float> KNR_0_Bw,
						float thetaX, float thetaKstar, float thetaPsiX, float thetaPsiKstar, float phimuX, float phimuKstar, 
						float phiK, float alphamu)
{
	
	float MeL = 0;
	
	std::complex<float> ix(0,1);

	for(int i3 = 0; i3 < 2; i3++)
	{

	float hmu = 2*i3 - 1.0;

	///std::complex<float> eamu = cos(hmu*phimuX) + ix*sin(hmu*phimuX);//exp(ix*phimu);
	std::complex<float> eamu = 1.0;
	std::complex<float> A1X = eamu*HamplitudeX1plus( MX, MB, M01, gamma1, X1Bs1, X1Bs2, hmu, thetaX, thetaPsiX, phimuX, alphamu );
	
	std::complex<float> A2X = eamu*HamplitudeX1plus( MX, MB, M02, gamma2, X2Bs1, X2Bs2, hmu, thetaX, thetaPsiX, phimuX, alphamu );
	
	
	
	std::complex<float> AK1410_1 = HamplitudeKstar1(MK, MB, 1.414, 0.232, K1410_1_Bw1, K1410_1_Bw2, K1410_1_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	std::complex<float> AK1430_0 = HamplitudeKstar0(MK, MB, 1.425, 0.270, K1430_0_Bw1, K1430_0_Bw2, K1430_0_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	//	std::complex<float> AK1430_2 = HamplitudeKstar2(MK, MB, 1.426, 0.099, K1430_2_Bw1, K1430_2_Bw2, K1430_2_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	// PDG 2018, neutral only
	std::complex<float> AK1430_2 = HamplitudeKstar2(MK, MB, 1.4324, 0.109, K1430_2_Bw1, K1430_2_Bw2, K1430_2_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	//	std::complex<float> AK1680_1 = HamplitudeKstar1(MK, MB, 1.717, 0.322, K1680_1_Bw1, K1680_1_Bw2, K1680_1_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	// PDG 2018
	std::complex<float> AK1680_1 = HamplitudeKstar1(MK, MB, 1.718, 0.322, K1680_1_Bw1, K1680_1_Bw2, K1680_1_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	std::complex<float> AK1780_3 = HamplitudeKstar3(MK, MB, 1.776, 0.159, K1780_3_Bw1, K1780_3_Bw2, K1780_3_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	std::complex<float> AK1950_0 = HamplitudeKstar0(MK, MB, 1.945, 0.201, K1950_0_Bw1, K1950_0_Bw2, K1950_0_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );

	std::complex<float> AK1980_2 = HamplitudeKstar2(MK, MB, 1.974, 0.376, K1980_2_Bw1, K1980_2_Bw2, K1980_2_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );

	std::complex<float> AK2045_4 = HamplitudeKstar4(MK, MB, 2.045, 0.198, K2045_4_Bw1, K2045_4_Bw2, K2045_4_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	///std::complex<float> AK1430_2 = HamplitudeKstar2(MK, MB, 1.4324, 0.109, K1430_2_Bw1, K1430_2_Bw2, K1430_2_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	std::complex<float> AKNR_0 = 0.0;
	if(includeKsNR == 1) AKNR_0 = HamplitudeKstarNR0(MK, MB, 1.425, 0.270, KNR_0_Bw, 0.0, 0.0, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK ); //commented


	
	//float ad = pow( abs(A1X + A2X + AK1410_1 + AK1430_0 + AK1430_2 + AK1680_1 + AK1780_3 + AK1950_0 + AK2045_4), 2.0 );
	float ad = pow( abs(A1X + A2X + AK1410_1 + AK1430_0 + AK1430_2 + AK1680_1 + AK1780_3 + AK1950_0 + AK1980_2 + AK2045_4 + AKNR_0), 2.0 );
	MeL += ad;
	if(ad != ad){
		printf("MX=%10.2e\n", MX);
		printf("MB=%10.2e\n", MB);
		printf("MK=%10.2e\n", MK);
		printf("A1X=%10.2e\n", abs(A1X));
		printf("A2X=%10.2e\n", abs(A2X));
		printf("AK1410_1=%10.2e\n", abs(AK1410_1));
		printf("AK1430_0=%10.2e\n", abs(AK1430_0));
		printf("AK1430_2=%10.2e\n", abs(AK1430_2));
		printf("AK1680_1=%10.2e\n", abs(AK1680_1));
		printf("AK1780_3=%10.2e\n", abs(AK1780_3));
		printf("AK1950_0=%10.2e\n", abs(AK1950_0));
		printf("AK1980_0=%10.2e\n", abs(AK1980_2));
		printf("AK2045_4=%10.2e\n", abs(AK2045_4));
		
		getchar();
	}


	}

	return MeL/3.0;


}

std::complex<float> HamplitudeKstar0_accelerated(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK, std::complex<float> BWK1_0, std::complex<float> BWK1_1)
{

std::complex<float> Ampl1;
if (hmu < 0.)
{
    Ampl1 = Bw1 * BWK1_0;
}
else 
{
    Ampl1 = Bw1 * BWK1_1;
}

return Ampl1;
}

// -BrWig1* (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi)


std::complex<float> HamplitudeKstar1_accelerated(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK, std::complex<float> BWK1_0, std::complex<float> BWK2_0, std::complex<float> BWK3_0, std::complex<float> BWK1_1, std::complex<float> BWK2_1, std::complex<float> BWK3_1)
{


std::complex<float> Ampl1;
if (hmu < 0.)
{
    Ampl1 = Bw1 * BWK1_0 + Bw2 * BWK2_0 + Bw3 * BWK3_0;
}
else 
{
    Ampl1 = Bw1 * BWK1_1 + Bw2 * BWK2_1 + Bw3 * BWK3_1;
}


return Ampl1;
}

std::complex<float> HamplitudeKstar2_accelerated(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK, std::complex<float> BWK1_0, std::complex<float> BWK2_0, std::complex<float> BWK3_0, std::complex<float> BWK1_1, std::complex<float> BWK2_1, std::complex<float> BWK3_1)
{

std::complex<float> Ampl1;
if (hmu < 0.)
{
    Ampl1 = Bw1 * BWK1_0 + Bw2 * BWK2_0 + Bw3 * BWK3_0;
}
else 
{
    Ampl1 = Bw1 * BWK1_1 + Bw2 * BWK2_1 + Bw3 * BWK3_1;
}

return Ampl1;
}

std::complex<float> HamplitudeKstar3_accelerated(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK, std::complex<float> BWK1_0, std::complex<float> BWK2_0, std::complex<float> BWK3_0, std::complex<float> BWK1_1, std::complex<float> BWK2_1, std::complex<float> BWK3_1)
{

std::complex<float> Ampl1;
if (hmu < 0.)
{
    Ampl1 = Bw1 * BWK1_0 + Bw2 * BWK2_0 + Bw3 * BWK3_0;
}
else 
{
    Ampl1 = Bw1 * BWK1_1 + Bw2 * BWK2_1 + Bw3 * BWK3_1;
}

return Ampl1;
}

std::complex<float> HamplitudeKstar4_accelerated(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK, std::complex<float> BWK1_0, std::complex<float> BWK2_0, std::complex<float> BWK3_0, std::complex<float> BWK1_1, std::complex<float> BWK2_1, std::complex<float> BWK3_1)
{

std::complex<float> Ampl1;
if (hmu < 0.)
{
    Ampl1 = Bw1 * BWK1_0 + Bw2 * BWK2_0 + Bw3 * BWK3_0;
}
else 
{
    Ampl1 = Bw1 * BWK1_1 + Bw2 * BWK2_1 + Bw3 * BWK3_1;
}

return Ampl1;
}





float HamplitudeXKstar_total_accelerated(float MX, float MK, float MB, float M01, float M02, float gamma1, float gamma2,
						std::complex<float> X1Bs1, std::complex<float> X1Bs2,
						std::complex<float> X2Bs1, std::complex<float> X2Bs2,
						std::complex<float> K1410_1_Bw1, std::complex<float> K1410_1_Bw2, std::complex<float> K1410_1_Bw3,
						std::complex<float> K1430_0_Bw1, std::complex<float> K1430_0_Bw2, std::complex<float> K1430_0_Bw3,
						std::complex<float> K1430_2_Bw1, std::complex<float> K1430_2_Bw2, std::complex<float> K1430_2_Bw3,
						std::complex<float> K1680_1_Bw1, std::complex<float> K1680_1_Bw2, std::complex<float> K1680_1_Bw3,
						std::complex<float> K1780_3_Bw1, std::complex<float> K1780_3_Bw2, std::complex<float> K1780_3_Bw3,
						std::complex<float> K1950_0_Bw1, std::complex<float> K1950_0_Bw2, std::complex<float> K1950_0_Bw3,
						std::complex<float> K1980_2_Bw1, std::complex<float> K1980_2_Bw2, std::complex<float> K1980_2_Bw3,
						std::complex<float> K2045_4_Bw1, std::complex<float> K2045_4_Bw2, std::complex<float> K2045_4_Bw3,
						std::complex<float> KNR_0_Bw,
						float thetaX, float thetaKstar, float thetaPsiX, float thetaPsiKstar, float phimuX, float phimuKstar, 
						float phiK, float alphamu,
                        std::complex<float> aBWK1410_1_1_minus, std::complex<float> aBWK1410_1_2_minus, std::complex<float> aBWK1410_1_3_minus, 
                        std::complex<float> aBWK1430_0_1_minus, 
                        std::complex<float> aBWK1430_2_1_minus, std::complex<float> aBWK1430_2_2_minus, std::complex<float> aBWK1430_2_3_minus, 
                        std::complex<float> aBWK1680_1_1_minus, std::complex<float> aBWK1680_1_2_minus, std::complex<float> aBWK1680_1_3_minus, 
                        std::complex<float> aBWK1780_3_1_minus, std::complex<float> aBWK1780_3_2_minus, std::complex<float> aBWK1780_3_3_minus, 
                        std::complex<float> aBWK1950_0_1_minus, 
                        std::complex<float> aBWK1980_2_1_minus, std::complex<float> aBWK1980_2_2_minus, std::complex<float> aBWK1980_2_3_minus, 
                        std::complex<float> aBWK2045_4_1_minus, std::complex<float> aBWK2045_4_2_minus, std::complex<float> aBWK2045_4_3_minus,
                        std::complex<float> aBWK1410_1_1_plus, std::complex<float> aBWK1410_1_2_plus, std::complex<float> aBWK1410_1_3_plus, 
                        std::complex<float> aBWK1430_0_1_plus, 
                        std::complex<float> aBWK1430_2_1_plus, std::complex<float> aBWK1430_2_2_plus, std::complex<float> aBWK1430_2_3_plus, 
                        std::complex<float> aBWK1680_1_1_plus, std::complex<float> aBWK1680_1_2_plus, std::complex<float> aBWK1680_1_3_plus, 
                        std::complex<float> aBWK1780_3_1_plus, std::complex<float> aBWK1780_3_2_plus, std::complex<float> aBWK1780_3_3_plus, 
                        std::complex<float> aBWK1950_0_1_plus, 
                        std::complex<float> aBWK1980_2_1_plus, std::complex<float> aBWK1980_2_2_plus, std::complex<float> aBWK1980_2_3_plus, 
                        std::complex<float> aBWK2045_4_1_plus, std::complex<float> aBWK2045_4_2_plus, std::complex<float> aBWK2045_4_3_plus)
{
	
	float MeL = 0;
	
	std::complex<float> ix(0,1);

	for(int i3 = 0; i3 < 2; i3++)
	{

	float hmu = 2*i3 - 1.0;

	///std::complex<float> eamu = cos(hmu*phimuX) + ix*sin(hmu*phimuX);//exp(ix*phimu);
	std::complex<float> eamu = 1.0;
    
    std::complex<float> A1X = std::complex<float>(0., 0.);
    std::complex<float> A2X = std::complex<float>(0., 0.);
    
    if (MODEL == 0)
    {
        A1X = eamu*HamplitudeX1plus( MX, MB, M01, gamma1, X1Bs1, X1Bs2, hmu, thetaX, thetaPsiX, phimuX, alphamu );
        A2X = eamu*HamplitudeX1plus( MX, MB, M02, gamma2, X2Bs1, X2Bs2, hmu, thetaX, thetaPsiX, phimuX, alphamu );
    }
    
    if (MODEL == 2)
    {
        A1X = eamu*HamplitudeX1minus( MX, MB, M01, gamma1, X1Bs1, X1Bs2, hmu, thetaX, thetaPsiX, phimuX, alphamu );
        A2X = eamu*HamplitudeX1minus( MX, MB, M02, gamma2, X2Bs1, X2Bs2, hmu, thetaX, thetaPsiX, phimuX, alphamu );
    }
    
    if (MODEL == 3)
    {
        A1X = eamu*HamplitudeX0minus( MX, MB, M01, gamma1, X1Bs1, X1Bs2, hmu, thetaX, thetaPsiX, phimuX, alphamu );
        A2X = eamu*HamplitudeX0minus( MX, MB, M02, gamma2, X2Bs1, X2Bs2, hmu, thetaX, thetaPsiX, phimuX, alphamu );
    }
    
    if (MODEL == 4)
    {
        A1X = eamu*HamplitudeX2minus( MX, MB, M01, gamma1, X1Bs1, X1Bs2, hmu, thetaX, thetaPsiX, phimuX, alphamu );
        A2X = eamu*HamplitudeX2minus( MX, MB, M02, gamma2, X2Bs1, X2Bs2, hmu, thetaX, thetaPsiX, phimuX, alphamu );
    }
    
    if (MODEL == 5)
    {
        A1X = eamu*HamplitudeX2plus( MX, MB, M01, gamma1, X1Bs1, X1Bs2, hmu, thetaX, thetaPsiX, phimuX, alphamu );
        A2X = eamu*HamplitudeX2plus( MX, MB, M02, gamma2, X2Bs1, X2Bs2, hmu, thetaX, thetaPsiX, phimuX, alphamu );
    }
	
	std::complex<float> AK1410_1 = HamplitudeKstar1_accelerated(MK, MB, 1.414, 0.232, K1410_1_Bw1, K1410_1_Bw2, K1410_1_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK, aBWK1410_1_1_minus, aBWK1410_1_2_minus, aBWK1410_1_3_minus, aBWK1410_1_1_plus, aBWK1410_1_2_plus, aBWK1410_1_3_plus);
	std::complex<float> AK1430_0 = HamplitudeKstar0_accelerated(MK, MB, 1.425, 0.270, K1430_0_Bw1, K1430_0_Bw2, K1430_0_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK, aBWK1430_0_1_minus, aBWK1430_0_1_plus);
	//	std::complex<float> AK1430_2 = HamplitudeKstar2(MK, MB, 1.426, 0.099, K1430_2_Bw1, K1430_2_Bw2, K1430_2_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	// PDG 2018, neutral only
	std::complex<float> AK1430_2 = HamplitudeKstar2_accelerated(MK, MB, 1.4324, 0.109, K1430_2_Bw1, K1430_2_Bw2, K1430_2_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK, aBWK1430_2_1_minus, aBWK1430_2_2_minus, aBWK1430_2_3_minus, aBWK1430_2_1_plus, aBWK1430_2_2_plus, aBWK1430_2_3_plus);
	//	std::complex<float> AK1680_1 = HamplitudeKstar1(MK, MB, 1.717, 0.322, K1680_1_Bw1, K1680_1_Bw2, K1680_1_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	// PDG 2018
	std::complex<float> AK1680_1 = HamplitudeKstar1_accelerated(MK, MB, 1.718, 0.322, K1680_1_Bw1, K1680_1_Bw2, K1680_1_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK, aBWK1680_1_1_minus, aBWK1680_1_2_minus, aBWK1680_1_3_minus, aBWK1680_1_1_plus, aBWK1680_1_2_plus, aBWK1680_1_3_plus);
	std::complex<float> AK1780_3 = HamplitudeKstar3_accelerated(MK, MB, 1.776, 0.159, K1780_3_Bw1, K1780_3_Bw2, K1780_3_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK, aBWK1780_3_1_minus, aBWK1780_3_2_minus, aBWK1780_3_3_minus, aBWK1780_3_1_plus, aBWK1780_3_2_plus, aBWK1780_3_3_plus);
	std::complex<float> AK1950_0 = HamplitudeKstar0_accelerated(MK, MB, 1.945, 0.201, K1950_0_Bw1, K1950_0_Bw2, K1950_0_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK, aBWK1950_0_1_minus, aBWK1950_0_1_plus);

	std::complex<float> AK1980_2 = HamplitudeKstar2_accelerated(MK, MB, 1.974, 0.376, K1980_2_Bw1, K1980_2_Bw2, K1980_2_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK, aBWK1980_2_1_minus, aBWK1980_2_2_minus, aBWK1980_2_3_minus, aBWK1980_2_1_plus, aBWK1980_2_2_plus, aBWK1980_2_3_plus);

	std::complex<float> AK2045_4 = HamplitudeKstar4_accelerated(MK, MB, 2.045, 0.198, K2045_4_Bw1, K2045_4_Bw2, K2045_4_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK, aBWK2045_4_1_minus, aBWK2045_4_2_minus, aBWK2045_4_3_minus, aBWK2045_4_1_plus, aBWK2045_4_2_plus, aBWK2045_4_3_plus);
	///std::complex<float> AK1430_2 = HamplitudeKstar2(MK, MB, 1.4324, 0.109, K1430_2_Bw1, K1430_2_Bw2, K1430_2_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	std::complex<float> AKNR_0 = 0.0;
	if(includeKsNR == 1) AKNR_0 = HamplitudeKstarNR0(MK, MB, 1.975, 0.270, KNR_0_Bw, 0.0, 0.0, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK ); //commented


	float ad = pow( abs(A1X + A2X + AK1410_1 + AK1430_0 + AK1430_2 + AK1680_1 + AK1780_3 + AK1950_0 + AK2045_4 + AK1980_2 + AKNR_0), 2.0 );
	//float ad = pow( abs(AK1410_1 + AK1430_0 + AK1430_2 + AK1680_1 + AK1780_3 + AK1950_0 + AK1980_2 + AK2045_4), 2.0 );
	MeL += ad;
	if(ad != ad){
		printf("MX=%10.2e\n", MX);
		printf("MB=%10.2e\n", MB);
		printf("MK=%10.2e\n", MK);
		//printf("A1X=%10.2e\n", abs(A1X));
		//printf("A2X=%10.2e\n", abs(A2X));
		printf("AK1410_1=%10.2e\n", abs(AK1410_1));
		printf("AK1430_0=%10.2e\n", abs(AK1430_0));
		printf("AK1430_2=%10.2e\n", abs(AK1430_2));
		printf("AK1680_1=%10.2e\n", abs(AK1680_1));
		printf("AK1780_3=%10.2e\n", abs(AK1780_3));
		printf("AK1950_0=%10.2e\n", abs(AK1950_0));
		printf("AK1980_0=%10.2e\n", abs(AK1980_2));
		printf("AK2045_4=%10.2e\n", abs(AK2045_4));
		
		getchar();
	}


	}

	return MeL/3.0;


}

//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
//                                                                  For Bs -> Jpsi K K                                                                              //
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//

std::complex<float> BreitWignerf( float MK, float M0K, float massB, float gamma0, int LK, int LB )
{
std::complex<float> ix(0,1);
const float massProton = MASS_PROTON;
const float massJpsi = MASS_JPSI;
const float massKaon = MASS_KAON;
const float massPion = MASS_PION;
const float massLambdaB = MASS_LB;
//const float massB = MASS_BD;
const float mplusK = massKaon + massKaon; /// mass JPsi + mass proton

massB = MASS_BS;


float argq0 = pow( M0K*M0K - massKaon*massKaon - massKaon*massKaon, 2.0 ) - 4.0 * massKaon*massKaon*massKaon*massKaon;
float argq1 = pow( MK*MK - massKaon*massKaon - massKaon*massKaon, 2.0 ) - 4.0 * massKaon*massKaon*massKaon*massKaon;
if(argq0 < 0.0 || argq1 < 0.0) return 0.0;
float q0 = sqrt(argq0)/2.0/M0K;
float q1 = sqrt(argq1)/2.0/MK;


float argp0 = pow( massB*massB - M0K*M0K - massJpsi*massJpsi, 2.0 ) - 4.0 * M0K*M0K*massJpsi*massJpsi;
float argp1 = pow( massB*massB - MK*MK - massJpsi*massJpsi, 2.0 ) - 4.0 * MK*MK*massJpsi*massJpsi;
if(argp0 < 0.0 || argp1 < 0.0) return 0.0;
float p0 = sqrt(argp0)/2.0/massB;
float p1 = sqrt(argp1)/2.0/massB;


float gamma = gamma0 * pow( q1/q0, 2*LK + 1) * M0K / MK * BlattWeisskopf(q0, q1, 3.0, LK);

if(gamma != gamma ){printf("in BWkstar gamma is nan"); getchar();}

float nom = 1.0;
//if(BW_nom_f==1) nom = MK*sqrt(M0K*gamma);
std::complex<float> denom = nom/( M0K*M0K - MK*MK - ix*M0K*gamma );

float BlWsLB = BlattWeisskopf(p0, p1, 3.0, LB);
float BlWsLK = BlattWeisskopf(q0, q1, 3.0, LK);

if(BlWsLB != BlWsLB) { printf("blatt LB is NaN, p0=%10.2e\tp1=%10.2e\tLB=%d", p0, p1, LB); getchar(); }
if(BlWsLK != BlWsLK) { printf("blatt LK is NaN, q0=%10.2e\tq1=%10.2e\tLK=%d", q0, q1, LK); getchar(); }

std::complex<float> Rm = (float)(BlWsLB*pow( p1/massB, LB ))*denom*(float)(BlWsLK*pow( q1/M0K, LK ));

/*
if(Rm != Rm){
printf("\nBW: %10.2e ", abs(denom));
printf("%10.1e ", BlattWeisskopf(p0, p1, 3.0, LB));
printf("%10.1e ", BlattWeisskopf(q0, q1, 3.0, LK));
printf("%10.1e ", pow( p1/massB, LB ) );
printf("%10.1e ", pow( q1/M0K, LK ) );
printf("%10.4e ", massB );
printf("%10.4e ", mplusB1 );
printf("%10.4e ", MK );
getchar();
}*/

return Rm;
}

std::complex<float> BreitWignerfNR( float MK, float M0K, float massB, float gamma0, int LK, int LB )
{
std::complex<float> ix(0,1);
const float massProton = MASS_PROTON;
const float massJpsi = MASS_JPSI;
const float massKaon = MASS_KAON;
const float massPion = MASS_PION;
const float massLambdaB = MASS_LB;
//const float massB = MASS_BD;
const float mplusK = massKaon + massKaon; /// mass JPsi + mass proton

massB = MASS_BS;


float argq0 = pow( M0K*M0K - massKaon*massKaon - massKaon*massKaon, 2.0 ) - 4.0 * massKaon*massKaon*massKaon*massKaon;
float argq1 = pow( MK*MK - massKaon*massKaon - massKaon*massKaon, 2.0 ) - 4.0 * massKaon*massKaon*massKaon*massKaon;
if(argq0 < 0.0 || argq1 < 0.0) return 0.0;
float q0 = sqrt(argq0)/2.0/M0K;
float q1 = sqrt(argq1)/2.0/MK;


float argp0 = pow( massB*massB - M0K*M0K - massJpsi*massJpsi, 2.0 ) - 4.0 * M0K*M0K*massJpsi*massJpsi;
float argp1 = pow( massB*massB - MK*MK - massJpsi*massJpsi, 2.0 ) - 4.0 * MK*MK*massJpsi*massJpsi;
if(argp0 < 0.0 || argp1 < 0.0) return 0.0;
float p0 = sqrt(argp0)/2.0/massB;
float p1 = sqrt(argp1)/2.0/massB;


float gamma = gamma0 * pow( q1/q0, 2*LK + 1) * M0K / MK * BlattWeisskopf(q0, q1, 3.0, LK);

if(gamma != gamma ){printf("in BWkstar gamma is nan"); getchar();}

float BlWsLB = BlattWeisskopf(p0, p1, 3.0, LB);
float BlWsLK = BlattWeisskopf(q0, q1, 3.0, LK);

if(BlWsLB != BlWsLB) { printf("blatt LB is NaN, p0=%10.2e\tp1=%10.2e\tLB=%d", p0, p1, LB); getchar(); }
if(BlWsLK != BlWsLK) { printf("blatt LK is NaN, q0=%10.2e\tq1=%10.2e\tLK=%d", q0, q1, LK); getchar(); }

std::complex<float> Rm = (float)(BlWsLB*pow( p1/massB, LB ))*(float)(BlWsLK*pow( q1/M0K, LK ));

return Rm;
}

int spinZ; 

std::complex<float> BreitWignerZ( float MX, float massB, float M0X, float gamma0, int LX )
{
std::complex<float> ix(0,1);
const float massProton = MASS_PROTON;
const float massJpsi = MASS_JPSI;
const float massKaon = MASS_KAON;
const float massPion = MASS_PION;
const float massLambdaB = MASS_LB;
massB = MASS_BS;


float argq0 = pow( M0X*M0X - massJpsi*massJpsi - massKaon*massKaon, 2.0 ) - 4.0 * massJpsi*massJpsi*massKaon*massKaon;
float argq1 = pow( MX*MX - massJpsi*massJpsi - massKaon*massKaon, 2.0 ) - 4.0 * massJpsi*massJpsi*massKaon*massKaon;
if(argq0 < 0.0 || argq1 < 0.0) return 0.0;
float q0 = sqrt(argq0)/2.0/M0X;
float q1 = sqrt(argq1)/2.0/MX;


float argp0 = pow( massB*massB - M0X*M0X - massKaon*massKaon, 2.0 ) - 4.0 * M0X*M0X*massKaon*massKaon;
float argp1 = pow( massB*massB - MX*MX - massKaon*massKaon, 2.0 ) - 4.0 * MX*MX*massKaon*massKaon;
if(argp0 < 0.0 || argp1 < 0.0) return 0.0;
float p0 = sqrt(argp0)/2.0/massB;
float p1 = sqrt(argp1)/2.0/massB;

float gamma = gamma0 * pow( q1/q0, 2*LX + 1) * M0X / MX * powf(BlattWeisskopf(q0, q1, 3.0, LX), 2.0);

float nom = MX*sqrt(M0X*gamma);
std::complex<float> denom = nom/( M0X*M0X - MX*MX - ix*M0X*gamma );
///std::complex<float> Rm = denom * BlattWeisskopf(q0, q1, 3.0, LX) * pow( q1/M0X, LX );

const int LB = spinZ;
std::complex<float> Rm = (float)(BlattWeisskopf(p0, p1, 3.0, LB) * pow( p1/massB, LB )) * denom * (float)(BlattWeisskopf(q0, q1, 3.0, LX) * pow( q1/M0X, LX ));

return Rm;
}

std::complex<float> HamplitudefNR0(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);

std::complex<float> BrWig1 = BreitWignerfNR( M, M0, MB, gamma, 1, 0);

std::complex<float>	Hs1 = -Bw1*BrWig1;

std::complex<float> Ampl1 = 0.0;

Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi);

return Ampl1;
}



std::complex<float> Hamplitudephi1(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);
std::complex<float> eK1 = cos(phiK) + ix*sin(phiK);//exp(ix*phiK);
std::complex<float> eK2 = cos(phiK) - ix*sin(phiK);

std::complex<float> BrWig1 = BreitWignerf( M, M0, MB, gamma, 0, 1);
std::complex<float> BrWig2 = BreitWignerf( M, M0, MB, gamma, 1, 1);
std::complex<float> BrWig3 = BreitWignerf( M, M0, MB, gamma, 2, 1);

std::complex<float>	Hs1 = -(float)(sqrt(1.0/3.0))*Bw1*BrWig1 + (float)(sqrt(2.0/3.0))*Bw3*BrWig3;
std::complex<float>	Hs2 = (float)(sqrt(1.0/3.0))*Bw1*BrWig1 - (float)(sqrt(1.0/2.0))*Bw2*BrWig2 + (float)(sqrt(1.0/6.0))*Bw3*BrWig3;
std::complex<float>	Hs3 = (float)(sqrt(1.0/3.0))*Bw1*BrWig1 + (float)(sqrt(1.0/2.0))*Bw2*BrWig2 + (float)(sqrt(1.0/6.0))*Bw3*BrWig3;


float wmpsi1 = (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi);
float wmpsi2 = (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi);
float wmpsi3 = (float)Wigner_matrix(1.0, -1.0, hmu, thetaPsi);
float wmkst1 = (float)Wigner_matrix(1.0, 0.0, 0.0, thetaKstar);
float wmkst2 = (float)Wigner_matrix(1.0, 1.0, 0.0, thetaKstar);
float wmkst3 = (float)Wigner_matrix(1.0, -1.0, 0.0, thetaKstar);

if(wmpsi1 != wmpsi1 || wmpsi2 != wmpsi2 || wmpsi3 != wmpsi3)
{printf("wmpsi is nan"); getchar(); }
if(wmkst1 != wmkst1 || wmkst2 != wmkst2 || wmkst3 != wmkst3)
{printf("wmkst is nan"); getchar(); }


std::complex<float> Ampl1 = 0.0;
Ampl1 += Hs1 * wmpsi1 * wmkst1;
Ampl1 += Hs2 * wmpsi2 * wmkst2 * emu1 * eK1;
Ampl1 += Hs3 * wmpsi3 * wmkst3 * emu2 * eK2;


return Ampl1;
}



std::complex<float> Hamplitudef2(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);
std::complex<float> eK1 = cos(phiK) + ix*sin(phiK);//exp(ix*phiK);
std::complex<float> eK2 = cos(phiK) - ix*sin(phiK);

std::complex<float> BrWig1 = BreitWignerf( M, M0, MB, gamma, 1, 2);
std::complex<float> BrWig2 = BreitWignerf( M, M0, MB, gamma, 2, 2);
std::complex<float> BrWig3 = BreitWignerf( M, M0, MB, gamma, 3, 2);

if(BrWig1 != BrWig1 || BrWig2 != BrWig2 || BrWig3 != BrWig3){
printf("BrWig123 Kstar2 is nan");
printf("%10.2e %10.2e %10.2e %10.2e", M, M0, MB, gamma);
getchar();
}

std::complex<float>	Hs1 = (float)(sqrt(2.0/5.0))*Bw1*BrWig1 - (float)(sqrt(3.0/5.0))*Bw3*BrWig3;
std::complex<float>	Hs2 = -(float)(sqrt(3.0/10.0))*Bw1*BrWig1 + (float)(sqrt(1.0/2.0))*Bw2*BrWig2 - (float)(sqrt(1.0/5.0))*Bw3*BrWig3;
std::complex<float>	Hs3 = -(float)(sqrt(3.0/10.0))*Bw1*BrWig1 - (float)(sqrt(1.0/2.0))*Bw2*BrWig2 - (float)(sqrt(1.0/5.0))*Bw3*BrWig3;


std::complex<float> Ampl1 = 0.0;
Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 0.0, thetaKstar);
Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 1.0, 0.0, thetaKstar) * emu1 * eK1;
Ampl1 += Hs3 * (float)Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, -1.0, 0.0, thetaKstar) * emu2 * eK2;

if(Ampl1 != Ampl1){ 
	printf("BWL1 %10.2e\n", abs(BreitWignerKstar( M, M0, MB, gamma, 1, 2)) ); 
	printf("BWL3 %10.2e\n", abs(BreitWignerKstar( M, M0, MB, gamma, 3, 2)) ); getchar(); }

return Ampl1;
}

std::complex<float> HamplitudeZ1plus(float M, float massB, float M0, float gamma, std::complex<float> Bs1, std::complex<float> Bs2, float hmu, float thetaX, float thetaPsi, float phimu )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);
std::complex<float> Hs1, Hs2, Hs3;

std::complex<float> Ampl1;
Ampl1 = 0.0;

int spinZ = 1;
int parityZ = 1;

/////spinX 2
	if(spinZ == 0 && parityZ == -1){
	Hs1 = -Bs1*BreitWignerZ( M, massB, M0, gamma, 1);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi);		

	}

	if(spinZ == 1 && parityZ == 1){
	Hs1 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerZ( M, massB, M0, gamma, 0) - (float)(sqrt(2.0/3.0))*Bs2*BreitWignerZ( M, massB, M0, gamma, 2);
	Hs2 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerZ( M, massB, M0, gamma, 0) + (float)(sqrt(1.0/6.0))*Bs2*BreitWignerZ( M, massB, M0, gamma, 2);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0,-1.0, thetaX) * emu2;

	}
	if(spinZ == 2 && parityZ == -1){
	Hs1 = (float)(sqrt(2.0/5.0))*Bs1*BreitWignerZ( M, massB, M0, gamma, 1) - (float)(sqrt(3.0/5.0))*Bs2*BreitWignerZ( M, massB, M0, gamma, 3);
	Hs2 = (float)(sqrt(3.0/10.0))*Bs1*BreitWignerZ( M, massB, M0, gamma, 1) + (float)(sqrt(1.0/5.0))*Bs2*BreitWignerZ( M, massB, M0, gamma, 3);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}
	
	if(spinZ == 2 && parityZ == 1){
	Hs2 = (float)(-sqrt(0.5))*Bs1*BreitWignerZ( M, massB, M0, gamma, 2);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 -= Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}

	/////Ampl1 *= (float)BsFactor( M, M0, spinZ );


return Ampl1;
}

float Hamplitudef_total(float MX, float MX1, float MK, float MB, float M01, float M02, float gamma1, float gamma2,
						std::complex<float> X1Bs1, std::complex<float> X1Bs2,
						std::complex<float> phi1680_1_Bw1, std::complex<float> phi1680_1_Bw2, std::complex<float> phi1680_1_Bw3,
						std::complex<float> f1525_2_Bw1, std::complex<float> f1525_2_Bw2, std::complex<float> f1525_2_Bw3,
						std::complex<float> f1640_2_Bw1, std::complex<float> f1640_2_Bw2, std::complex<float> f1640_2_Bw3,
						std::complex<float> f1750_2_Bw1, std::complex<float> f1750_2_Bw2, std::complex<float> f1750_2_Bw3,
						std::complex<float> f1950_2_Bw1, std::complex<float> f1950_2_Bw2, std::complex<float> f1950_2_Bw3,
						std::complex<float> NR_Bw1,
						float thetaXs, float thetaF, float thetaPsiXs, float thetaPsiF, float phimuXs, float phimuF, float phiKF, float thetaXs1, float thetaPsiF1, float phimuXs1)
{
	float MeL = 0;
	
	std::complex<float> ix(0,1);

	for(int i3 = 0; i3 < 2; i3++)
	{

	float hmu = 2*i3 - 1.0;

	//std::complex<float> A1X = HamplitudeX1plus( MX, MB, M01, gamma1, X1Bs1, X1Bs2, hmu, thetaXs, thetaPsiXs, phimuXs );
	//std::complex<float> A2X = HamplitudeX2plus( MX, MB, M02, gamma2, X2Bs1, X2Bs2, hmu, thetaXs, thetaPsiXs, phimuXs );
	
	
	//std::complex<float> A1X = HamplitudeZ1plus( MX, MB, M01, gamma1, X1Bs1, X1Bs2, hmu, thetaXs, thetaPsiF, phimuXs );
	//std::complex<float> A1X1 = HamplitudeZ1plus( MX1, MB, M01, gamma1, X1Bs1, X1Bs2, hmu, thetaXs1, thetaPsiF1, phimuXs1 );
	std::complex<float> Aphi1680_1 = Hamplitudephi1(MK, MB, 1.680, 0.150, phi1680_1_Bw1, phi1680_1_Bw2, phi1680_1_Bw3, hmu, thetaF, thetaPsiF, phimuF, phiKF );
	std::complex<float> Af1525_2 = Hamplitudef2(MK, MB, 1.525, 0.073, f1525_2_Bw1, f1525_2_Bw2, f1525_2_Bw3, hmu, thetaF, thetaPsiF, phimuF, phiKF );
	std::complex<float> Af1640_2 = Hamplitudef2(MK, MB, 1.639, 0.099, f1640_2_Bw1, f1640_2_Bw2, f1640_2_Bw3, hmu, thetaF, thetaPsiF, phimuF, phiKF );
	std::complex<float> Af1750_2 = Hamplitudef2(MK, MB, 1.750, 0.099, f1750_2_Bw1, f1750_2_Bw2, f1750_2_Bw3, hmu, thetaF, thetaPsiF, phimuF, phiKF );
	std::complex<float> Af1950_2 = Hamplitudef2(MK, MB, 1.944, 0.472, f1950_2_Bw1, f1950_2_Bw2, f1950_2_Bw3, hmu, thetaF, thetaPsiF, phimuF, phiKF );
	std::complex<float> ANR = 0.0;
	if(includeFNRsimple == 1) ANR = NR_Bw1;
	//if(includeFNR == 1) ANR = HamplitudefNR0(MK, MB, 1.9, 0.3, NR_Bw1, 0.0, 0.0, hmu, thetaF, thetaPsiF, phimuF, phiKF );
	//float ad = pow( abs(A1X + A2X + AK1410_1 + AK1430_0 + AK1430_2 + AK1680_1 + AK1780_3 + AK1950_0 + AK2045_4), 2.0 );
	float ad = pow( abs(Aphi1680_1 + Af1525_2 + Af1640_2 + Af1750_2 + Af1950_2 + ANR ), 2.0 ); // add "A1X + A1X1 + " for exotics
	MeL += ad;
	/*if(ad != ad){
		printf("MX=%10.2e\n", MX);
		printf("MB=%10.2e\n", MB);
		printf("MK=%10.2e\n", MK);
		printf("A1X=%10.2e\n", abs(A1X));
		printf("A2X=%10.2e\n", abs(A2X));
		printf("AK1410_1=%10.2e\n", abs(AK1410_1));
		printf("AK1430_0=%10.2e\n", abs(AK1430_0));
		printf("AK1430_2=%10.2e\n", abs(AK1430_2));
		printf("AK1680_1=%10.2e\n", abs(AK1680_1));
		printf("AK1780_3=%10.2e\n", abs(AK1780_3));
		printf("AK1950_0=%10.2e\n", abs(AK1950_0));
		printf("AK2045_4=%10.2e\n", abs(AK2045_4));
		
		getchar();
	}*/


	}

	return MeL/3.0;


}



/*

double hadronmass1[CHANNELS] = { MASS_PROTON * MEV, MASS_KAON * MEV, MASS_KAON * MEV, MASS_PION * MEV, MASS_KAON * MEV, MASS_PION * MEV, MASS_PROTON * MEV, MASS_KAON * MEV, MASS_PROTON * MEV, MASS_KAON * MEV, MASS_PROTON * MEV, MASS_PION * MEV };
double hadronmass2[CHANNELS] = { MASS_KAON * MEV, MASS_PROTON * MEV, MASS_KAON * MEV, MASS_PION * MEV, MASS_PION * MEV, MASS_KAON * MEV, MASS_KAON * MEV, MASS_PROTON * MEV, MASS_KAON * MEV, MASS_PROTON * MEV, MASS_PION * MEV, MASS_PROTON * MEV };


////// kinematic variables:
	if (j==4)
			{
			v_p1.SetXYZM( GEV*mc_momentum->at(1).Px(), GEV*mc_momentum->at(1).Py(), GEV*mc_momentum->at(1).Pz(), hadronmass1[j] );
			v_K2.SetXYZM( GEV*mc_momentum->at(2).Px(), GEV*mc_momentum->at(2).Py(), GEV*mc_momentum->at(2).Pz(), hadronmass2[j] );
			    vLambda_pK = v_Jpsi1 + v_p1 + v_K2;
				vJpsip = v_Jpsi1 + v_p1;
				vJpsiK = v_Jpsi1 + v_K2;
				v_pK = v_p1 + v_K2;

				BPhS_mc_mass_Kpi[evcounter] = GeV*v_pK.Mag();
				BPhS_mc_mass_X[evcounter] = GeV*vJpsiK.Mag();
				BPhS_mc_theta_X[evcounter] = theta_Pc(vLambda_pK, vJpsip, v_Jpsi1);
				BPhS_mc_theta_psi[evcounter] = theta_psi(vJpsip, v_Jpsi1, v_mu2);
				BPhS_mc_theta_psiKstar[evcounter] = theta_psi(vLambda_pK, v_Jpsi1, v_mu2);
				BPhS_mc_theta_Kstar[evcounter] = theta_Lstar( vLambda_pK, v_pK, v_K2 );
				BPhS_mc_phi_mu[evcounter] = phi_mu_Pc(vJpsip, v_K2, v_Jpsi1, v_mu2);
				BPhS_mc_phi_muKstar[evcounter] = phi_mu_Lstar(vLambda_pK, v_p1+v_K2, v_Jpsi1, v_mu2);
				BPhS_mc_phi_K[evcounter] = phi_K(vLambda_pK, v_pK, v_K2, v_Jpsi1);
				BPhS_mc_alpha_mu[evcounter] = alpha_mu(v_mu2, v_Jpsi1, vJpsip, vLambda_pK);
	}
*/



/////////////////////////////////////// Переделанные углы:

Double_t theta_B0(TLorentzVector vBd, TLorentzVector vKstar)
{
    TVector3 vBd3 = vBd.BoostVector();
	TLorentzVector vKstar_b = vKstar;
	vKstar_b.Boost(-vBd3);
return vBd.Vect().Angle( vKstar_b.Vect() );
}

Double_t phi_psi(TLorentzVector vB0, TLorentzVector vZc, TLorentzVector vpsi)
{
	TLorentzVector vZc_b = vZc;
	TLorentzVector vpsi_b = vpsi;
	TVector3 vZc3 = vZc.BoostVector();
    TVector3 vB03 = vB0.BoostVector();
	vZc_b.Boost(-vB03);
	vpsi_b.Boost(-vZc3);

	TVector3 x0 = -vB0.Vect().Unit() + ( vB0.Vect().Unit() * vZc_b.Vect().Unit() ) * vZc_b.Vect().Unit();
	
	double argy = ( vZc_b.Vect().Unit().Cross(x0.Unit()) ) * vpsi_b.Vect().Unit();
	double argx = vpsi_b.Vect().Unit() * x0.Unit();

	return atan2( argy, argx );
}

Double_t phi_K(TLorentzVector vBd, TLorentzVector vKstar, TLorentzVector vK, TLorentzVector vpsi)
{
	TLorentzVector vK_b = vK;
	TLorentzVector vKstarb = vKstar;
	TLorentzVector vpsib = vpsi;
	TLorentzVector vKb = vK;
	TVector3 vBd3 = vBd.BoostVector();
	TVector3 vKstar3 = vKstar.BoostVector();
	

	vKstarb.Boost(-vBd3);
	vpsib.Boost(-vKstar3);
	vKb.Boost(-vKstar3);

	TVector3 x0 = - vBd.Vect().Unit() + ( vBd.Vect().Unit() * vKstarb.Vect().Unit() ) * vKstarb.Vect().Unit();
	
	
	double argy = ( -vpsib.Vect().Unit().Cross(x0.Unit()) ) * vKb.Vect().Unit();
	double argx = x0.Unit() * vKb.Vect().Unit();

	return atan2( argy, argx );
}

Double_t alpha_mu(TLorentzVector vmu_2, TLorentzVector vpsi, TLorentzVector vZc, TLorentzVector vBd)
{
	TLorentzVector vmu2_b = vmu_2;	
	TLorentzVector vpsi_b = vpsi;
	TLorentzVector vpsi_b2 = vpsi;
	//TLorentzVector vLambda_b = vLambda;
	TVector3 vBd3 = vBd.BoostVector();
	TVector3 vZc3 = vZc.BoostVector();
	TVector3 vpsi3 = vpsi.BoostVector();
	

	vmu2_b.Boost(-vpsi3);
	vpsi_b.Boost(-vBd3);
	vpsi_b2.Boost(-vZc3);


	TVector3 x1 = - vpsi_b2.Vect().Unit() + ( vpsi_b2.Vect().Unit() * vmu2_b.Vect().Unit() ) * vmu2_b.Vect().Unit();
	TVector3 x2 = - vpsi_b.Vect().Unit() + ( vpsi_b.Vect().Unit() * vmu2_b.Vect().Unit() ) * vmu2_b.Vect().Unit();
	
	
	double argy = ( vmu2_b.Vect().Unit().Cross(x1.Unit()) ) * x2.Unit();
	double argx = x1.Unit() * x2.Unit();

	return atan2( argy, argx );
}

Double_t phi_mu_Kstar(TLorentzVector vBd, TLorentzVector vKstar, TLorentzVector vpsi, TLorentzVector v_mu2)
{
	TLorentzVector vKstar_b = vKstar;
	TLorentzVector vpsi_b = vpsi;
	TVector3 vBd3 = vBd.BoostVector();
	
	vpsi_b.Boost(-vBd3);
	vKstar_b.Boost(-vBd3);

	TVector3 x0 = -vBd.Vect().Unit() + ( vBd.Vect().Unit() * vKstar_b.Vect().Unit() ) * vKstar_b.Vect().Unit();
	
	TLorentzVector v_mu2_b = v_mu2;
	TVector3 vpsi3 = vpsi.BoostVector();
	
	v_mu2_b.Boost(-vpsi3);
	
	double argy = ( vpsi_b.Vect().Unit().Cross(x0.Unit()) ) * v_mu2_b.Vect().Unit();
	double argx = v_mu2_b.Vect().Unit() * x0.Unit();

	return atan2( argy, argx );
}

Double_t phi_mu_X(TLorentzVector vZc, TLorentzVector vB0, TLorentzVector vpsi, TLorentzVector v_mu2)
{
	TLorentzVector vZc_b = vZc;
	TLorentzVector vpsi_b = vpsi;
	TVector3 vZc3 = vZc.BoostVector();
    TVector3 vB03 = vB0.BoostVector();
	vZc_b.Boost(-vB03);
	vpsi_b.Boost(-vZc3);

	TVector3 x0 = -vZc_b.Vect().Unit() + ( vZc_b.Vect().Unit() * vpsi_b.Vect().Unit() ) * vpsi_b.Vect().Unit();
	
	
	TLorentzVector v_mu2_b = v_mu2;
	TVector3 vpsi3 = vpsi.BoostVector();
	v_mu2_b.Boost(-vpsi3);
	
	double argy = ( vpsi_b.Vect().Unit().Cross(x0.Unit()) ) * v_mu2_b.Vect().Unit();
	double argx = v_mu2_b.Vect().Unit() * x0.Unit();

	return atan2( argy, argx );
}

///// angle between Jpsi and muon in Jpsi rest frame, Jpsi momentum is taken in Pc rest frame;
Double_t theta_psi_X(TLorentzVector vZc, TLorentzVector vpsi, TLorentzVector v_mu2)
{

	TVector3 vZc3 = vZc.BoostVector();
	TVector3 vpsi3 = vpsi.BoostVector();
	TLorentzVector v_mu2_b = v_mu2; 
	v_mu2_b.Boost(-vpsi3);
	TLorentzVector vpsi_b = vpsi;
	vpsi_b.Boost(-vZc3);

return v_mu2_b.Vect().Angle(vpsi_b.Vect());
}

//////////////////////////////////////////////////////////////////////////////////////////////
///// angle between Jpsi and Pc in Pc rest frame, Pc momentum is taken in Lambda_b rest frame;
Double_t theta_X(TLorentzVector vBd, TLorentzVector vZc, TLorentzVector vpsi)
{
	TVector3 vBd3 = vBd.BoostVector();
	TVector3 vZc3 = vZc.BoostVector();
	TLorentzVector vpsi_b = vpsi;
	vpsi_b.Boost(-vZc3);
	TLorentzVector vZc_b2 = vZc;
	vZc_b2.Boost(-vBd3);

return vpsi_b.Vect().Angle( vZc_b2.Vect() );
}

//////////////////////////////////////////////////////////////////////////////////////////////
///// angle between Kaon and Lstar candidate in Lstar rest frame, Lstar momentum is taken in Lambda_b rest frame;
Double_t theta_Kstar( TLorentzVector vBd, TLorentzVector vKstar, TLorentzVector vK )
{
	
	TVector3 vBd3 = vBd.BoostVector();
	TVector3 vKstar3 = vKstar.BoostVector();

	TLorentzVector vK_b = vK;
	vK_b.Boost(-vKstar3);
	
	TLorentzVector vKstarb = vKstar;
	vKstarb.Boost(-vBd3);
	return vK_b.Vect().Angle(vKstarb.Vect()) ;

}

///////////////////////////////////////////////////////////////////////////////////////////////
///// angle between between Jpsi and muon in Jpsi rest frame, Jpsi momentum is taken in Bd rest frame;
Double_t theta_psi_Kstar(TLorentzVector vBd, TLorentzVector vpsi, TLorentzVector v_mu2)
{

	TVector3 vBd3 = vBd.BoostVector();
	TVector3 vpsi3 = vpsi.BoostVector();
	TLorentzVector v_mu2_b = v_mu2; 
	v_mu2_b.Boost(-vpsi3);
	TLorentzVector vpsi_b = vpsi;
	vpsi_b.Boost(-vBd3);

return v_mu2_b.Vect().Angle(vpsi_b.Vect());
}

Double_t s_calculation(TLorentzVector vjpsi, TLorentzVector vK, TLorentzVector vpi)
{
	float t = 0.61803, a, b, t1, t2;
    float m, M, M0 = 4800., M1 = 4900., m0 = 5250., m1 = 5310.;
    TLorentzVector vjpsi1, vK1, vpi1, vjpsi2, vK2, vpi2;
    M = (vjpsi + vK + vpi).M();
    m = m0 + (m1 - m0) / (M1 - M0) * (M - M0);
    a = 1.;
    b = 2.;
    t1 = a + (b - a) * (1 - t);
    t2 = a + (b - a) * t;
    while (b - a > 0.0001)
    {
        vjpsi1.SetXYZM(t1 * vjpsi.Px(), t1 * vjpsi.Py(), t1 * vjpsi.Pz(), 3096.916);
        vK1.SetXYZM(t1 * vK.Px(), t1 * vK.Py(), t1 * vK.Pz(), 493.677);
        vpi1.SetXYZM(t1 * vpi.Px(), t1 * vpi.Py(), t1 * vpi.Pz(), 139.57);
        vjpsi2.SetXYZM(t2 * vjpsi.Px(), t2 * vjpsi.Py(), t2 * vjpsi.Pz(), 3096.916);
        vK2.SetXYZM(t2 * vK.Px(), t2 * vK.Py(), t2 * vK.Pz(), 493.677);
        vpi2.SetXYZM(t2 * vpi.Px(), t2 * vpi.Py(), t2 * vpi.Pz(), 139.57);
        if (abs(m - (vjpsi1 + vK1 + vpi1).M()) > abs((vjpsi2 + vK2 + vpi2).M() - m))
        {
            a = t1;
            t1 = t2;
            t2 = a + (b - a) * t;
        }
        else
        {
            b = t2;
            t2 = t1;
            t1 = a + (b - a) * (1 - t);
        }
    }
    vjpsi1.SetXYZM(t1 * vjpsi.Px(), t1 * vjpsi.Py(), t1 * vjpsi.Pz(), 3096.916);
    vK1.SetXYZM(t1 * vK.Px(), t1 * vK.Py(), t1 * vK.Pz(), 493.677);
    vpi1.SetXYZM(t1 * vpi.Px(), t1 * vpi.Py(), t1 * vpi.Pz(), 139.57);
    vjpsi2.SetXYZM(t2 * vjpsi.Px(), t2 * vjpsi.Py(), t2 * vjpsi.Pz(), 3096.916);
    vK2.SetXYZM(t2 * vK.Px(), t2 * vK.Py(), t2 * vK.Pz(), 493.677);
    vpi2.SetXYZM(t2 * vpi.Px(), t2 * vpi.Py(), t2 * vpi.Pz(), 139.57);
    if (abs(m - (vjpsi1 + vK1 + vpi1).M()) > abs((vjpsi2 + vK2 + vpi2).M() - m)) 
    {
        //cout << (vjpsi2 + vK2 + vpi2).M()  << "    " << m << endl;
        return t2;
    }
    else
    {
        //cout << (vjpsi1 + vK1 + vpi1).M() << "    " << m << endl;
        return t1;
    }
}

Double_t s_mc_calculation(TLorentzVector vjpsi, TLorentzVector vK, TLorentzVector vpi)
{
	float t = 0.61803, a, b, t1, t2;
    float m, M, M0 = 4800., M1 = 4900., m0 = 5250., m1 = 5310.;
    TLorentzVector vjpsi1, vK1, vpi1, vjpsi2, vK2, vpi2;
    M = (vjpsi + vK + vpi).M();
    m = 5279.64;
    a = 1.;
    b = 2.;
    t1 = a + (b - a) * (1 - t);
    t2 = a + (b - a) * t;
    while (b - a > 0.0001)
    {
        vjpsi1.SetXYZM(t1 * vjpsi.Px(), t1 * vjpsi.Py(), t1 * vjpsi.Pz(), 3096.916);
        vK1.SetXYZM(t1 * vK.Px(), t1 * vK.Py(), t1 * vK.Pz(), 493.677);
        vpi1.SetXYZM(t1 * vpi.Px(), t1 * vpi.Py(), t1 * vpi.Pz(), 139.57);
        vjpsi2.SetXYZM(t2 * vjpsi.Px(), t2 * vjpsi.Py(), t2 * vjpsi.Pz(), 3096.916);
        vK2.SetXYZM(t2 * vK.Px(), t2 * vK.Py(), t2 * vK.Pz(), 493.677);
        vpi2.SetXYZM(t2 * vpi.Px(), t2 * vpi.Py(), t2 * vpi.Pz(), 139.57);
        if (abs(m - (vjpsi1 + vK1 + vpi1).M()) > abs((vjpsi2 + vK2 + vpi2).M() - m))
        {
            a = t1;
            t1 = t2;
            t2 = a + (b - a) * t;
        }
        else
        {
            b = t2;
            t2 = t1;
            t1 = a + (b - a) * (1 - t);
        }
    }
    vjpsi1.SetXYZM(t1 * vjpsi.Px(), t1 * vjpsi.Py(), t1 * vjpsi.Pz(), 3096.916);
    vK1.SetXYZM(t1 * vK.Px(), t1 * vK.Py(), t1 * vK.Pz(), 493.677);
    vpi1.SetXYZM(t1 * vpi.Px(), t1 * vpi.Py(), t1 * vpi.Pz(), 139.57);
    vjpsi2.SetXYZM(t2 * vjpsi.Px(), t2 * vjpsi.Py(), t2 * vjpsi.Pz(), 3096.916);
    vK2.SetXYZM(t2 * vK.Px(), t2 * vK.Py(), t2 * vK.Pz(), 493.677);
    vpi2.SetXYZM(t2 * vpi.Px(), t2 * vpi.Py(), t2 * vpi.Pz(), 139.57);
    if (abs(m - (vjpsi1 + vK1 + vpi1).M()) > abs((vjpsi2 + vK2 + vpi2).M() - m)) 
    {
        //cout << (vjpsi2 + vK2 + vpi2).M()  << "    " << m << endl;
        return t2;
    }
    else
    {
        //cout << (vjpsi1 + vK1 + vpi1).M() << "    " << m << endl;
        return t1;
    }
}

//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__||   Matrix element Lb   ||__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//

std::complex<float> BreitWignerLstar( float ML, float M0L, float massLb, float gamma0, int LLstar, int LLb )
{
std::complex<float> ix(0,1);
const float massProton = MASS_PROTON;
const float massJpsi = MASS_JPSI;
const float massKaon = MASS_KAON;
const float massPion = MASS_PION;
const float massLambdaB = MASS_LB;
//const float massB = MASS_BD;
const float mplusL = massProton + massKaon; /// mass JPsi + mass proton

massLb = MASS_LB;



float argq0 = pow( M0L*M0L - massProton*massProton - massKaon*massKaon, 2.0 ) - 4.0 * massProton*massProton*massKaon*massKaon;
float argq1 = pow( ML*ML - massProton*massProton - massKaon*massKaon, 2.0 ) - 4.0 * massProton*massProton*massKaon*massKaon;
if(argq0 < 0.0 || argq1 < 0.0) return 0.0;
float q0 = sqrt(argq0)/2.0/M0L;
float q1 = sqrt(argq1)/2.0/ML;


float argp0 = pow( massLb*massLb - M0L*M0L - massJpsi*massJpsi, 2.0 ) - 4.0 * M0L*M0L*massJpsi*massJpsi;
float argp1 = pow( massLb*massLb - ML*ML - massJpsi*massJpsi, 2.0 ) - 4.0 * ML*ML*massJpsi*massJpsi;
if(argp0 < 0.0 || argp1 < 0.0) return 0.0;
float p0 = sqrt(argp0)/2.0/massLb;
float p1 = sqrt(argp1)/2.0/massLb;

float gamma = gamma0 * pow( q1/q0, 2*LLstar + 1) * M0L / ML * powf(BlattWeisskopf(q0, q1, 3.0, LLstar), 2.0);

///float gamma = gamma0;

if(gamma != gamma ){printf("in BWkstar gamma is nan"); getchar();}



float nom = 1.0;
if( BW_nom_Lstar == 1) nom = ML*sqrt(M0L*gamma);
std::complex<float> denom = nom/( M0L*M0L - ML*ML - ix*M0L*gamma );

float BlWsLB = BlattWeisskopf(p0, p1, 3.0, LLb);
float BlWsLK = BlattWeisskopf(q0, q1, 3.0, LLstar);

if(BlWsLB != BlWsLB) { printf("blatt LLb is NaN, p0=%10.2e\tp1=%10.2e\tLB=%d", p0, p1, LLb); getchar(); }
if(BlWsLK != BlWsLK) { printf("blatt LLstar is NaN, q0=%10.2e\tq1=%10.2e\tLK=%d", q0, q1, LLstar); getchar(); }

std::complex<float> Rm = (float)(BlWsLB*pow( p1/massLb, LLb ))*denom*(float)(BlWsLK*pow( q1/M0L, LLstar ));

//std::complex<float> Rm = (float)(BlWsLB*pow( p1/massLb, LLb ))*denom;
//std::complex<float> Rm = denom*(float)(BlWsLK*pow( q1/M0L, LLstar ));
//std::complex<float> Rm = denom;

/*
if(Rm != Rm){
printf("\nBW: %10.2e ", abs(denom));
printf("%10.1e ", BlattWeisskopf(p0, p1, 3.0, LB));
printf("%10.1e ", BlattWeisskopf(q0, q1, 3.0, LK));
printf("%10.1e ", pow( p1/massB, LB ) );
printf("%10.1e ", pow( q1/M0K, LK ) );
printf("%10.4e ", massB );
printf("%10.4e ", mplusB1 );
printf("%10.4e ", MK );
getchar();
}*/

return Rm;
}




std::complex<float> BreitWignerLstarNR( float ML, float M0L, float massLb, float gamma0, int LLstar, int LLb )
{
std::complex<float> ix(0,1);
const float massProton = MASS_PROTON;
const float massJpsi = MASS_JPSI;
const float massKaon = MASS_KAON;
const float massPion = MASS_PION;
const float massLambdaB = MASS_LB;
//const float massB = MASS_BD;
const float mplusL = massProton + massKaon; /// mass JPsi + mass proton

massLb = MASS_LB;



float argq0 = pow( M0L*M0L - massProton*massProton - massKaon*massKaon, 2.0 ) - 4.0 * massProton*massProton*massKaon*massKaon;
float argq1 = pow( ML*ML - massProton*massProton - massKaon*massKaon, 2.0 ) - 4.0 * massProton*massProton*massKaon*massKaon;
if(argq0 < 0.0 || argq1 < 0.0) return 0.0;
float q0 = sqrt(argq0)/2.0/M0L;
float q1 = sqrt(argq1)/2.0/ML;


float argp0 = pow( massLb*massLb - M0L*M0L - massJpsi*massJpsi, 2.0 ) - 4.0 * M0L*M0L*massJpsi*massJpsi;
float argp1 = pow( massLb*massLb - ML*ML - massJpsi*massJpsi, 2.0 ) - 4.0 * ML*ML*massJpsi*massJpsi;
if(argp0 < 0.0 || argp1 < 0.0) return 0.0;
float p0 = sqrt(argp0)/2.0/massLb;
float p1 = sqrt(argp1)/2.0/massLb;

float gamma = gamma0 * pow( q1/q0, 2*LLstar + 1) * M0L / ML * powf(BlattWeisskopf(q0, q1, 3.0, LLstar), 2.0);

///float gamma = gamma0;

if(gamma != gamma ){printf("in BWkstar gamma is nan"); getchar();}


float nom = 1.0;
std::complex<float> denom = nom;

float BlWsLB = BlattWeisskopf(p0, p1, 3.0, LLb);
float BlWsLK = BlattWeisskopf(q0, q1, 3.0, LLstar);

if(BlWsLB != BlWsLB) { printf("blatt LLb is NaN, p0=%10.2e\tp1=%10.2e\tLB=%d", p0, p1, LLb); getchar(); }
if(BlWsLK != BlWsLK) { printf("blatt LLstar is NaN, q0=%10.2e\tq1=%10.2e\tLK=%d", q0, q1, LLstar); getchar(); }

std::complex<float> Rm = (float)(BlWsLB*pow( p1/massLb, LLb ))*denom*(float)(BlWsLK*pow( q1/M0L, LLstar ));

return Rm;
}

double LambdabFactor( double Mpc, double M0pc, int LL )
{
return 1.0;
//std::complex<double> ix(0,1);
const double massProton = MASS_PROTON;
const double massJpsi = MASS_JPSI;
const double massKaon = MASS_KAON;
const double massLambdaB = MASS_LB;
double mplusPc = massProton + massJpsi; /// mass JPsi + mass proton

double mplusLambda0 = M0pc + massKaon;
double mplusLambda1 = Mpc + massKaon;


double arg0 = pow( massLambdaB*massLambdaB -  M0pc*M0pc - massJpsi*massJpsi, 2.0) - 4.0*M0pc*M0pc*massJpsi*massJpsi;
double arg1 = pow( massLambdaB*massLambdaB -  Mpc*Mpc - massJpsi*massJpsi, 2.0) - 4.0*Mpc*Mpc*massJpsi*massJpsi;

if(arg0 <= 0.0 || arg1 <= 0.0) return 0.0;

//double p0 = sqrt(massLambdaB*massLambdaB - mplusLambda0*mplusLambda0 ); 
//double p1 = sqrt(massLambdaB*massLambdaB - mplusLambda1*mplusLambda1 );

double p0 = sqrt(arg0)/2.0/massLambdaB;
double p1  = sqrt(arg1)/2.0/massLambdaB;

double factor = BlattWeisskopf(p0, p1, 3.0, LL) * pow( p1/massLambdaB, LL );

return factor;
}

std::complex<float> BreitWignerPc( float Mpc, float M0pc, float gamma0, int Lpc )
{
std::complex<float> ix(0,1);
const float massProton = MASS_PROTON;
const float massJpsi = MASS_JPSI;
const float massKaon = MASS_KAON;
const float massLambdaB = MASS_LB;
float mplusPc = massProton + massJpsi; /// mass JPsi + mass proton

//float q0 = sqrt(M0pc*M0pc - mplusPc*mplusPc); 
//float q1 = sqrt(Mpc*Mpc - mplusPc*mplusPc);

float argq0 = pow( M0pc*M0pc - massJpsi*massJpsi - massProton*massProton, 2.0 ) - 4.0 * massJpsi*massJpsi*massProton*massProton;
float argq1 = pow( Mpc*Mpc - massJpsi*massJpsi - massProton*massProton, 2.0 ) - 4.0 * massJpsi*massJpsi*massProton*massProton;
if(argq0 < 0.0 || argq1 < 0.0) return 0.0;
float q0 = sqrt(argq0)/2.0/M0pc;
float q1 = sqrt(argq1)/2.0/Mpc;


//float argp0 = pow( massLambdaB*massLambdaB - M0pc*M0pc - massKaon*massKaon, 2.0 ) - 4.0 * M0pc*M0pc*massKaon*massKaon;
//float argp1 = pow( massLambdaB*massLambdaB - Mpc*Mpc - massKaon*massKaon, 2.0 ) - 4.0 * Mpc*Mpc*massKaon*massKaon;
//if(argp0 < 0.0 || argp1 < 0.0) return 0.0;
//float p0 = sqrt(argp0)/2.0/massLambdaB;
//float p1 = sqrt(argp1)/2.0/massLambdaB;


float gamma = gamma0 * pow( q1/q0, 2*Lpc + 1) * M0pc / Mpc * powf(BlattWeisskopf(q0, q1, 3.0, Lpc) , 2.0);
float nom = 1.0;
if( BW_nom_Pc == 1) nom = Mpc*sqrt(M0pc*gamma);

std::complex<float> denom = nom/( M0pc*M0pc - Mpc*Mpc - ix*M0pc*gamma );
std::complex<float> Rm = denom * (float)BlattWeisskopf(q0, q1, 3.0, Lpc) * (float)pow( q1/M0pc, Lpc );
//std::complex<float> Rm = BlattWeisskopf(p0, p1, 3.0, LL) * pow( p1/massLambdaB, LL ) * denom * BlattWeisskopf(q0, q1, 3.0, Lpc) * pow( q1/M0pc, Lpc );
return Rm;
}

std::complex<float> Hamplitude12minus(float M, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bs1, std::complex<float> Bs2, std::complex<float> Bs3,  float hL, float hp, float hmu, float thetaPc, float thetaL, float thetaPsi, float phimu, float phiPc, float phiPsi, float alpha_mu, float thetap )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = cosf(hL*phiPc) + ix*sinf(hL*phiPc); //exp(ix*hL*phiPc);

std::complex<float> ePsi1 = cosf(0.5*phiPsi) + ix*sinf(0.5*phiPsi); //exp(ix*phiPsi);
std::complex<float> ePsi2 = cosf(0.5*phiPsi) - ix*sinf(0.5*phiPsi); //exp(-ix*phiPsi);

std::complex<float> ealpha = cosf(hmu*alpha_mu) + ix*sinf(hmu*alpha_mu); //exp(-ix*phiPsi);

std::complex<float> Hs1, Hs2, Hs3, Hw1, Hw2;


Hw1 = sqrtf(0.5)*Bw1 * (float)LambdabFactor( M, M0, 0 ) - sqrtf(0.5)*Bw2 * (float)LambdabFactor( M, M0, 1 );  //lPc = 1/2
Hw2 = sqrtf(0.5)*Bw1 * (float)LambdabFactor( M, M0, 0 ) + sqrtf(0.5)*Bw2 * (float)LambdabFactor( M, M0, 1 );  //lPc = -1/2



std::complex<float> Ampl1, Ampl2, Ampl3, Ampl4;

float hppc = 0.5;
	Hs1 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerPc( M, M0, gamma, 0) + (float)(sqrt(1.0/6.0)) * Bs2 * BreitWignerPc( M, M0, gamma, 2);   //H_1_1/2
	Hs2 = (float)(1.0/sqrt(6.0))*Bs1*BreitWignerPc( M, M0, gamma, 0) - (float)(sqrt(1.0/3.0)) * Bs2 * BreitWignerPc( M, M0, gamma, 2);   //H_0_1/2
Ampl1 = 0.0;
Ampl1 += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(0.5, 0.5, 1.0 - hppc, thetaPc);
Ampl1 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(0.5, 0.5, - hppc, thetaPc);
////Ampl1 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, -1.0 - hppc, thetaPc);
Ampl1 *= Hw1 * ePsi1 * eL * Wigner_matrix(0.5, hL, 0.5, thetaL);

Ampl2 = 0.0;
Ampl2  += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(0.5, -0.5, 1.0 - hppc, thetaPc);
Ampl2 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(0.5, -0.5, - hppc, thetaPc);
///Ampl2 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, -1.0 - hppc, thetaPc);
Ampl2 *= Hw2 * ePsi2 * eL * Wigner_matrix(0.5, hL, -0.5, thetaL);

Ampl1 += Ampl2;
Ampl1 *= Wigner_matrix(0.5, hppc, hp, thetap);

hppc = -0.5;
	Hs3 = -(float)(1.0/sqrt(3.0))*Bs1*BreitWignerPc( M, M0, gamma, 0) - (float)(sqrt(1.0/6.0)) * Bs2 * BreitWignerPc( M, M0, gamma, 2);   //H_-1_-1/2
	Hs2 = -(float)(1.0/sqrt(6.0))*Bs1*BreitWignerPc( M, M0, gamma, 0) + (float)(sqrt(1.0/3.0)) * Bs2 * BreitWignerPc( M, M0, gamma, 2);   //H_0_-1/2
Ampl3 = 0.0;
//Ampl3 += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, 1.0 - hppc, thetaPc);
Ampl3 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(0.5, 0.5, - hppc, thetaPc);
Ampl3 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(0.5, 0.5, -1.0 - hppc, thetaPc);
Ampl3 *= Hw1 * ePsi1 * eL * Wigner_matrix(0.5, hL, 0.5, thetaL);

Ampl4 = 0.0;
//Ampl4  += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, 1.0 - hppc, thetaPc);
Ampl4 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(0.5, -0.5, - hppc, thetaPc);
Ampl4 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(0.5, -0.5, -1.0 - hppc, thetaPc);
Ampl4 *= Hw2 * ePsi2 * eL * Wigner_matrix(0.5, hL, -0.5, thetaL);

Ampl3 += Ampl4;
Ampl3 *= Wigner_matrix(0.5, hppc, hp, thetap);

Ampl1 += Ampl3;

Ampl1 *= ealpha; 

return Ampl1;
}



std::complex<float> Hamplitude12plus(float M, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bs1, std::complex<float> Bs2, std::complex<float> Bs3,  float hL, float hp, float hmu, float thetaPc, float thetaL, float thetaPsi, float phimu, float phiPc, float phiPsi, float alpha_mu, float thetap )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = cosf(hL*phiPc) + ix*sinf(hL*phiPc); //exp(ix*hL*phiPc);

std::complex<float> ePsi1 = cosf(0.5*phiPsi) + ix*sinf(0.5*phiPsi); //exp(ix*phiPsi);
std::complex<float> ePsi2 = cosf(0.5*phiPsi) - ix*sinf(0.5*phiPsi); //exp(-ix*phiPsi);

std::complex<float> ealpha = cosf(hmu*alpha_mu) + ix*sinf(hmu*alpha_mu); //exp(-ix*phiPsi);

std::complex<float> Hs1, Hs2, Hs3, Hw1, Hw2;


Hw1 = sqrtf(0.5)*Bw1 * (float)LambdabFactor( M, M0, 0 ) - sqrtf(0.5)*Bw2 * (float)LambdabFactor( M, M0, 1 );  //lPc = 1/2
Hw2 = sqrtf(0.5)*Bw1 * (float)LambdabFactor( M, M0, 0 ) + sqrtf(0.5)*Bw2 * (float)LambdabFactor( M, M0, 1 );  //lPc = -1/2



std::complex<float> Ampl1, Ampl2, Ampl3, Ampl4;

float hppc = 0.5;
	Hs1 = -(float)(1.0/sqrt(3.0))*Bs1*BreitWignerPc( M, M0, gamma, 1) - (float)(sqrt(1.0/6.0)) * Bs2 * BreitWignerPc( M, M0, gamma, 1);   //H_1_1/2
	Hs2 = (float)(1.0/sqrt(6.0))*Bs1*BreitWignerPc( M, M0, gamma, 1) - (float)(sqrt(1.0/3.0)) * Bs2 * BreitWignerPc( M, M0, gamma, 1);   //H_0_1/2
Ampl1 = 0.0;
Ampl1 += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(0.5, 0.5, 1.0 - hppc, thetaPc);
Ampl1 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(0.5, 0.5, - hppc, thetaPc);
////Ampl1 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, -1.0 - hppc, thetaPc);
Ampl1 *= Hw1 * ePsi1 * eL * Wigner_matrix(0.5, hL, 0.5, thetaL);

Ampl2 = 0.0;
Ampl2  += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(0.5, -0.5, 1.0 - hppc, thetaPc);
Ampl2 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(0.5, -0.5, - hppc, thetaPc);
///Ampl2 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, -1.0 - hppc, thetaPc);
Ampl2 *= Hw2 * ePsi2 * eL * Wigner_matrix(0.5, hL, -0.5, thetaL);

Ampl1 += Ampl2;
Ampl1 *= Wigner_matrix(0.5, hppc, hp, thetap);

hppc = -0.5;
	Hs3 = -(float)(1.0/sqrt(3.0))*Bs1*BreitWignerPc( M, M0, gamma, 1) - (float)(sqrt(1.0/6.0)) * Bs2 * BreitWignerPc( M, M0, gamma, 1);   //H_-1_-1/2
	Hs2 = (float)(1.0/sqrt(6.0))*Bs1*BreitWignerPc( M, M0, gamma, 1) - (float)(sqrt(1.0/3.0)) * Bs2 * BreitWignerPc( M, M0, gamma, 1);   //H_0_-1/2
Ampl3 = 0.0;
//Ampl3 += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, 1.0 - hppc, thetaPc);
Ampl3 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(0.5, 0.5, - hppc, thetaPc);
Ampl3 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(0.5, 0.5, -1.0 - hppc, thetaPc);
Ampl3 *= Hw1 * ePsi1 * eL * Wigner_matrix(0.5, hL, 0.5, thetaL);

Ampl4 = 0.0;
//Ampl4  += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, 1.0 - hppc, thetaPc);
Ampl4 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(0.5, -0.5, - hppc, thetaPc);
Ampl4 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(0.5, -0.5, -1.0 - hppc, thetaPc);
Ampl4 *= Hw2 * ePsi2 * eL * Wigner_matrix(0.5, hL, -0.5, thetaL);

Ampl3 += Ampl4;
Ampl3 *= Wigner_matrix(0.5, hppc, hp, thetap);

Ampl1 += Ampl3;

Ampl1 *= ealpha; 

return Ampl1;
}




std::complex<float> Hamplitude32minus(float M, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bs1, std::complex<float> Bs2, std::complex<float> Bs3,  float hL, float hp, float hmu, float thetaPc, float thetaL, float thetaPsi, float phimu, float phiPc, float phiPsi, float alpha_mu, float thetap )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = cosf(hL*phiPc) + ix*sinf(hL*phiPc); //exp(ix*hL*phiPc);

std::complex<float> ePsi1 = cosf(0.5*phiPsi) + ix*sinf(0.5*phiPsi); //exp(ix*phiPsi);
std::complex<float> ePsi2 = cosf(0.5*phiPsi) - ix*sinf(0.5*phiPsi); //exp(-ix*phiPsi);

std::complex<float> ealpha = cosf(hmu*alpha_mu) + ix*sinf(hmu*alpha_mu); //exp(-ix*phiPsi);

std::complex<float> Hs1, Hs2, Hs3, Hw1, Hw2;

if(hp == 0.5) {
	
}
if(hp == -0.5) {
	
}

Hw1 = -sqrtf(0.5)*Bw1 * (float)LambdabFactor( M, M0, 1 ) + sqrtf(0.5)*Bw2 * (float)LambdabFactor( M, M0, 2 );
Hw2 = -sqrtf(0.5)*Bw1 * (float)LambdabFactor( M, M0, 1 ) - sqrtf(0.5)*Bw2 * (float)LambdabFactor( M, M0, 2 );



std::complex<float> Ampl1, Ampl2, Ampl3, Ampl4;

float hppc = 0.5;
    Hs1 = (float)(1.0/2.0/sqrt(3.0))*Bs1*BreitWignerPc( M, M0, gamma, 0) - ( (float)(sqrt(1.0/3.0))*Bs2 + (float)(1.0/2.0/sqrt(3.0))*Bs3 )* BreitWignerPc( M, M0, gamma, 2);
	Hs2 = (float)(sqrt(1.0/6.0))*Bs1 * BreitWignerPc( M, M0, gamma, 0) + ( (float)(sqrt(1.0/6.0))*Bs2 - (float)(sqrt(1.0/6.0))*Bs3 ) * BreitWignerPc( M, M0, gamma, 2);
	Hs3 = (float)(1.0/2.0)*Bs1 * BreitWignerPc( M, M0, gamma, 0) + (float)(1.0/2.0)*Bs3 * BreitWignerPc( M, M0, gamma, 2);
Ampl1 = 0.0;
Ampl1 += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, 1.0 - hppc, thetaPc);
Ampl1 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, - hppc, thetaPc);
Ampl1 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, -1.0 - hppc, thetaPc);
Ampl1 *= Hw1 * ePsi1 * eL * Wigner_matrix(0.5, hL, 0.5, thetaL);

Ampl2 = 0.0;
Ampl2  += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, 1.0 - hppc, thetaPc);
Ampl2 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, - hppc, thetaPc);
Ampl2 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, -1.0 - hppc, thetaPc);
Ampl2 *= Hw2 * ePsi2 * eL * Wigner_matrix(0.5, hL, -0.5, thetaL);

Ampl1 += Ampl2;
Ampl1 *= Wigner_matrix(0.5, hppc, hp, thetap);

hppc = -0.5;
    Hs3 = (float)(1.0/2.0/sqrt(3.0))*Bs1*BreitWignerPc( M, M0, gamma, 0) - ( (float)(sqrt(1.0/3.0))*Bs2 + (float)(1.0/2.0/sqrt(3.0))*Bs3) * BreitWignerPc( M, M0, gamma, 2);
	Hs2 = (float)(sqrt(1.0/6.0))*Bs1 * BreitWignerPc( M, M0, gamma, 0) + ( (float)(sqrt(1.0/6.0))*Bs2 - (float)(sqrt(1.0/6.0))*Bs3 ) * BreitWignerPc( M, M0, gamma, 2);
	Hs1 = (float)(1.0/2.0)*Bs1 * BreitWignerPc( M, M0, gamma, 0) + (float)(1.0/2.0)*Bs3 * BreitWignerPc( M, M0, gamma, 2);
Ampl3 = 0.0;
Ampl3 += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, 1.0 - hppc, thetaPc);
Ampl3 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, - hppc, thetaPc);
Ampl3 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, -1.0 - hppc, thetaPc);
Ampl3 *= Hw1 * ePsi1 * eL * Wigner_matrix(0.5, hL, 0.5, thetaL);

Ampl4 = 0.0;
Ampl4  += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, 1.0 - hppc, thetaPc);
Ampl4 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, - hppc, thetaPc);
Ampl4 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, -1.0 - hppc, thetaPc);
Ampl4 *= Hw2 * ePsi2 * eL * Wigner_matrix(0.5, hL, -0.5, thetaL);

Ampl3 += Ampl4;
Ampl3 *= Wigner_matrix(0.5, hppc, hp, thetap);

Ampl1 += Ampl3;

Ampl1 *= ealpha; 

return Ampl1;
}



std::complex<float> Hamplitude32plus(float M, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bs1, std::complex<float> Bs2, std::complex<float> Bs3,  float hL, float hp, float hmu, float thetaPc, float thetaL, float thetaPsi, float phimu, float phiPc, float phiPsi, float alpha_mu, float thetap )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = cosf(hL*phiPc) + ix*sinf(hL*phiPc); //exp(ix*hL*phiPc);

std::complex<float> ePsi1 = cosf(0.5*phiPsi) + ix*sinf(0.5*phiPsi); //exp(ix*phiPsi);
std::complex<float> ePsi2 = cosf(0.5*phiPsi) - ix*sinf(0.5*phiPsi); //exp(-ix*phiPsi);

std::complex<float> ealpha = cosf(hmu*alpha_mu) + ix*sinf(hmu*alpha_mu); //exp(-ix*phiPsi);

std::complex<float> Hs1, Hs2, Hs3, Hw1, Hw2;

if(hp == 0.5) {
		
}
if(hp == -0.5) {
	
}

Hw1 = -sqrtf(0.5)*Bw1 * (float)LambdabFactor( M, M0, 1 ) + sqrtf(0.5)*Bw2 * (float)LambdabFactor( M, M0, 2 );
Hw2 = -sqrtf(0.5)*Bw1 * (float)LambdabFactor( M, M0, 1 ) - sqrtf(0.5)*Bw2 * (float)LambdabFactor( M, M0, 2 );



std::complex<float> Ampl1, Ampl2, Ampl3, Ampl4;

float hppc = 0.5;
	Hs1 = (-(float)(1.0/sqrt(3.0))*Bs1 + (float)(sqrt(1.0/60.0))*Bs2 )*BreitWignerPc( M, M0, gamma, 1) - (float)(3.0/sqrt(60.0))*Bs3*BreitWignerPc( M, M0, gamma, 3);
	Hs2 = (-(float)(sqrt(1.0/6.0))*Bs1 -(float)(sqrt(1.0/30.0))*Bs2 )* BreitWignerPc( M, M0, gamma, 1) + (float)(sqrt(3.0/10.0))*Bs3*BreitWignerPc( M, M0, gamma, 3);
	Hs3 = (float)(3.0/2.0/sqrt(5.0))*Bs1 * BreitWignerPc( M, M0, gamma, 1) + (float)(1.0/2.0/sqrt(5.0))*Bs3 * BreitWignerPc( M, M0, gamma, 3);

Ampl1 = 0.0;
Ampl1 += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, 1.0 - hppc, thetaPc);
Ampl1 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, - hppc, thetaPc);
Ampl1 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, -1.0 - hppc, thetaPc);
Ampl1 *= Hw1 * ePsi1 * eL * Wigner_matrix(0.5, hL, 0.5, thetaL);

Ampl2 = 0.0;
Ampl2  += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, 1.0 - hppc, thetaPc);
Ampl2 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, - hppc, thetaPc);
Ampl2 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, -1.0 - hppc, thetaPc);
Ampl2 *= Hw2 * ePsi2 * eL * Wigner_matrix(0.5, hL, -0.5, thetaL);

Ampl1 += Ampl2;
Ampl1 *= Wigner_matrix(0.5, hppc, hp, thetap);

hppc = -0.5;
	Hs3 = -(-(float)(1.0/sqrt(3.0))*Bs1 + (float)(sqrt(1.0/60.0))*Bs2 )*BreitWignerPc( M, M0, gamma, 1) + (float)(3.0/sqrt(60.0))*Bs3*BreitWignerPc( M, M0, gamma, 3);
	Hs2 = -(-(float)(sqrt(1.0/6.0))*Bs1 -(float)(sqrt(1.0/30.0))*Bs2 )* BreitWignerPc( M, M0, gamma, 1) - (float)(sqrt(3.0/10.0))*Bs3*BreitWignerPc( M, M0, gamma, 3);
	Hs1 = -(float)(3.0/2.0/sqrt(5.0))*Bs1 * BreitWignerPc( M, M0, gamma, 1) - (float)(1.0/2.0/sqrt(5.0))*Bs3 * BreitWignerPc( M, M0, gamma, 3);

Ampl3 = 0.0;
Ampl3 += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, 1.0 - hppc, thetaPc);
Ampl3 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, - hppc, thetaPc);
Ampl3 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(1.5, 0.5, -1.0 - hppc, thetaPc);
Ampl3 *= Hw1 * ePsi1 * eL * Wigner_matrix(0.5, hL, 0.5, thetaL);

Ampl4 = 0.0;
Ampl4  += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, 1.0 - hppc, thetaPc);
Ampl4 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, - hppc, thetaPc);
Ampl4 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(1.5, -0.5, -1.0 - hppc, thetaPc);
Ampl4 *= Hw2 * ePsi2 * eL * Wigner_matrix(0.5, hL, -0.5, thetaL);

Ampl3 += Ampl4;
Ampl3 *= Wigner_matrix(0.5, hppc, hp, thetap);

Ampl1 += Ampl3;

Ampl1 *= ealpha; 

return Ampl1;
}



std::complex<float> Hamplitude52plus(float M, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bs1, std::complex<float> Bs2, std::complex<float> Bs3,  float hL, float hp, float hmu, float thetaPc, float thetaL, float thetaPsi, float phimu, float phiPc, float phiPsi, float alpha_mu, float thetap  )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = cosf(hL*phiPc) + ix*sinf(hL*phiPc); //exp(ix*hL*phiPc);

std::complex<float> ePsi1 = cosf(0.5*phiPsi) + ix*sinf(0.5*phiPsi); //exp(ix*phiPsi);
std::complex<float> ePsi2 = cosf(0.5*phiPsi) - ix*sinf(0.5*phiPsi); //exp(-ix*phiPsi);

std::complex<float> ealpha = cosf(hmu*alpha_mu) + ix*sinf(hmu*alpha_mu); //exp(-ix*phiPsi);

std::complex<float> Hs1, Hs2, Hs3, Hw1, Hw2;

if(hp == 0.5) {
	
}
if(hp == -0.5) {
	
}

Hw1 = sqrtf(0.5)*Bw1 * (float)LambdabFactor( M, M0, 2 ) - sqrtf(0.5)*Bw2 * (float)LambdabFactor( M, M0, 3 );
Hw2 = sqrtf(0.5)*Bw1 * (float)LambdabFactor( M, M0, 2 ) + sqrtf(0.5)*Bw2 * (float)LambdabFactor( M, M0, 3 );


std::complex<float> Ampl1, Ampl2, Ampl3, Ampl4;

float hppc = 0.5;
	Hs1 = (float)(sqrt(1.0/10.0))*Bs1 * BreitWignerPc( M, M0, gamma, 1) - ((float)(sqrt(1.0/3.0))*Bs2 + (float)(sqrt(1.0/15.0))*Bs3) * BreitWignerPc( M, M0, gamma, 3);
	Hs2 = (float)(sqrt(1.0/5.0))*Bs1 * BreitWignerPc( M, M0, gamma, 1) + ( (float)(sqrt(1.0/6.0))*Bs2 - (float)(sqrt(2.0/15.0))*Bs3) * BreitWignerPc( M, M0, gamma, 3);
	Hs3 = (float)(sqrt(1.0/5.0))*Bs1 * BreitWignerPc( M, M0, gamma, 1) + (float)(sqrt(3.0/10.0))*Bs3 * BreitWignerPc( M, M0, gamma, 3);

Ampl1 = 0.0;
Ampl1 += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(2.5, 0.5, 1.0 - hppc, thetaPc);
Ampl1 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(2.5, 0.5, - hppc, thetaPc);
Ampl1 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(2.5, 0.5, -1.0 - hppc, thetaPc);
Ampl1 *= Hw1 * ePsi1 * eL * Wigner_matrix(0.5, hL, 0.5, thetaL);

Ampl2 = 0.0;
Ampl2  = Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(2.5, -0.5, 1.0 - hppc, thetaPc);
Ampl2 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(2.5, -0.5, - hppc, thetaPc);
Ampl2 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(2.5, -0.5, -1.0 - hppc, thetaPc);
Ampl2 *= Hw2 * ePsi2 * eL * Wigner_matrix(0.5, hL, -0.5, thetaL);

Ampl1 += Ampl2;
Ampl1 *= Wigner_matrix(0.5, hppc, hp, thetap);

hppc = -0.5;
	Hs3 = (float)(sqrt(1.0/10.0))*Bs1 * BreitWignerPc( M, M0, gamma, 1) - ((float)(sqrt(1.0/3.0))*Bs2 + (float)(sqrt(1.0/15.0))*Bs3) * BreitWignerPc( M, M0, gamma, 3);
	Hs2 = (float)(sqrt(1.0/5.0))*Bs1 * BreitWignerPc( M, M0, gamma, 1) + ( (float)(sqrt(1.0/6.0))*Bs2 - (float)(sqrt(2.0/15.0))*Bs3) * BreitWignerPc( M, M0, gamma, 3);
	Hs1 = (float)(sqrt(1.0/5.0))*Bs1 * BreitWignerPc( M, M0, gamma, 1) + (float)(sqrt(3.0/10.0))*Bs3 * BreitWignerPc( M, M0, gamma, 3);
Ampl3 = 0.0;
Ampl3 += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(2.5, 0.5, 1.0 - hppc, thetaPc);
Ampl3 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(2.5, 0.5, - hppc, thetaPc);
Ampl3 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(2.5, 0.5, -1.0 - hppc, thetaPc);
Ampl3 *= Hw1 * ePsi1 * eL * Wigner_matrix(0.5, hL, 0.5, thetaL);


Ampl4 = 0.0;
Ampl4  = Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(2.5, -0.5, 1.0 - hppc, thetaPc);
Ampl4 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(2.5, -0.5, - hppc, thetaPc);
Ampl4 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(2.5, -0.5, -1.0 - hppc, thetaPc);
Ampl4 *= Hw2 * ePsi2 * eL * Wigner_matrix(0.5, hL, -0.5, thetaL);

Ampl3 += Ampl4;
Ampl3 *= Wigner_matrix(0.5, hppc, hp, thetap);

Ampl1 += Ampl3;
Ampl1 *= ealpha;


//Ampl1 =  BreitWignerPc( M, M0, gamma, 1);
//std::complex<float> denom = nom/( M0pc*M0pc - Mpc*Mpc - ix*M0pc*gamma );
//Ampl1 = 0.03*abs( (float)(1.0) / ( M0*M0 - M*M - ix*M0*gamma ) );
return Ampl1;
}



std::complex<float> Hamplitude52minus(float M, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bs1, std::complex<float> Bs2, std::complex<float> Bs3,  float hL, float hp, float hmu, float thetaPc, float thetaL, float thetaPsi, float phimu, float phiPc, float phiPsi, float alpha_mu, float thetap  )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = cosf(hL*phiPc) + ix*sinf(hL*phiPc); //exp(ix*hL*phiPc);

std::complex<float> ePsi1 = cosf(0.5*phiPsi) + ix*sinf(0.5*phiPsi); //exp(ix*phiPsi);
std::complex<float> ePsi2 = cosf(0.5*phiPsi) - ix*sinf(0.5*phiPsi); //exp(-ix*phiPsi);

std::complex<float> ealpha = cosf(hmu*alpha_mu) + ix*sinf(hmu*alpha_mu); //exp(-ix*phiPsi);

std::complex<float> Hs1, Hs2, Hs3, Hw1, Hw2;

if(hp == 0.5) {
	
}
if(hp == -0.5) {
	
}

Hw1 = sqrtf(0.5)*Bw1 * (float)LambdabFactor( M, M0, 2 ) - sqrtf(0.5)*Bw2 * (float)LambdabFactor( M, M0, 3 );
Hw2 = sqrtf(0.5)*Bw1 * (float)LambdabFactor( M, M0, 2 ) + sqrtf(0.5)*Bw2 * (float)LambdabFactor( M, M0, 3 );


std::complex<float> Ampl1, Ampl2, Ampl3, Ampl4;

float hppc = 0.5;
	Hs1 = ( -(float)(sqrt(1.0/3.0))*Bs1 + (float)(sqrt(1.0/42.0))*Bs2 )*BreitWignerPc( M, M0, gamma, 2) - (float)(sqrt(1.0/7.0))*Bs3 * BreitWignerPc( M, M0, gamma, 4);
	Hs2 = (-(float)(sqrt(1.0/6.0))*Bs1 - (float)(sqrt(1.0/21.0))*Bs2 )* BreitWignerPc( M, M0, gamma, 2) + (float)(sqrt(2.0/14.0))*Bs3 * BreitWignerPc( M, M0, gamma, 4);
	Hs3 = (float)(sqrt(3.0/7.0))*Bs1 * BreitWignerPc( M, M0, gamma, 2) + (float)(sqrt(1.0/14.0))*Bs3 * BreitWignerPc( M, M0, gamma, 4);
Ampl1 = 0.0;
Ampl1 += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(2.5, 0.5, 1.0 - hppc, thetaPc);
Ampl1 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(2.5, 0.5, - hppc, thetaPc);
Ampl1 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(2.5, 0.5, -1.0 - hppc, thetaPc);
Ampl1 *= Hw1 * ePsi1 * eL * Wigner_matrix(0.5, hL, 0.5, thetaL);

Ampl2 = 0.0;
Ampl2  = Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(2.5, -0.5, 1.0 - hppc, thetaPc);
Ampl2 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(2.5, -0.5, - hppc, thetaPc);
Ampl2 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(2.5, -0.5, -1.0 - hppc, thetaPc);
Ampl2 *= Hw2 * ePsi2 * eL * Wigner_matrix(0.5, hL, -0.5, thetaL);

Ampl1 += Ampl2;
Ampl1 *= Wigner_matrix(0.5, hppc, hp, thetap);

hppc = -0.5;
	Hs3 = -( -(float)(sqrt(1.0/3.0))*Bs1 + (float)(sqrt(1.0/42.0))*Bs2 )*BreitWignerPc( M, M0, gamma, 2) + (float)(sqrt(1.0/7.0))*Bs3 * BreitWignerPc( M, M0, gamma, 4);
	Hs2 = -(-(float)(sqrt(1.0/6.0))*Bs1 - (float)(sqrt(1.0/21.0))*Bs2 )* BreitWignerPc( M, M0, gamma, 2) - (float)(sqrt(2.0/14.0))*Bs3 * BreitWignerPc( M, M0, gamma, 4);
	Hs1 = -(float)(sqrt(3.0/7.0))*Bs1 * BreitWignerPc( M, M0, gamma, 2) - (float)(sqrt(1.0/14.0))*Bs3 * BreitWignerPc( M, M0, gamma, 4);
Ampl3 = 0.0;
Ampl3 += Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(2.5, 0.5, 1.0 - hppc, thetaPc);
Ampl3 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(2.5, 0.5, - hppc, thetaPc);
Ampl3 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(2.5, 0.5, -1.0 - hppc, thetaPc);
Ampl3 *= Hw1 * ePsi1 * eL * Wigner_matrix(0.5, hL, 0.5, thetaL);


Ampl4 = 0.0;
Ampl4  = Hs1 * emu1 * Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * Wigner_matrix(2.5, -0.5, 1.0 - hppc, thetaPc);
Ampl4 += Hs2 * Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * Wigner_matrix(2.5, -0.5, - hppc, thetaPc);
Ampl4 += Hs3 * emu2 * Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * Wigner_matrix(2.5, -0.5, -1.0 - hppc, thetaPc);
Ampl4 *= Hw2 * ePsi2 * eL * Wigner_matrix(0.5, hL, -0.5, thetaL);

Ampl3 += Ampl4;
Ampl3 *= Wigner_matrix(0.5, hppc, hp, thetap);

Ampl1 += Ampl3;
Ampl1 *= ealpha;

return Ampl1;
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////
//minimal Lstar12 //////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
std::complex<float> HamplitudeLstar12(int Lstarparity, float M, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hL, float hp, float hmu, float thetaLstar, float thetaL, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = 1.0; //cos(hL*phiLstar) + ix*sin(hL*phiLstar); //exp(ix*hL*phiPc);

//std::complex<float> eK0 = cosf(1.5*phiK) + ix*sinf(1.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK1 = cosf(0.5*phiK) + ix*sinf(0.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK2 = cosf(0.5*phiK) - ix*sinf(0.5*phiK); //exp(-ix*phiPsi);
//std::complex<float> eK3 = cosf(1.5*phiK) - ix*sinf(1.5*phiK); //exp(-ix*phiPsi);


std::complex<float> Ampl1 = 0.0;
std::complex<float> Hs1, Hw1, Hw2, Hw3, Hw4;

Hs1 = 1.0;
if(Lstarparity > 0 && hp == -0.5) Hs1 = -1.0;   /// do not do it when Lstar parity is negative

//int LLb = 0;
//if(Lstarparity > 0) LLb = 1;

int LLstar = 0;
if(Lstarparity > 0) LLstar = 1;

Hw1 = sqrtf(1.0/6.0)*Bw1;
Hw2 = sqrtf(1.0/3.0)*Bw1;
Hw3 = -sqrtf(1.0/3.0)*Bw1;
Hw4 = -sqrtf(1.0/6.0)*Bw1;


//BreitWignerLstar( float ML, float M0L, float massLb, float gamma0, int LLstar, int LLb )

Ampl1 += Hs1*Hw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK1 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 0.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw2*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK1 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 0.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) * emu2 * eL * eK2 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 0.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw4*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK2 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 0.5, -0.5, hp, thetaLstar );


//printf( "BW=%10.5e\t%10.5e\n", BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ).real(), BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ).imag() );
//printf( "WM_thetapsi=%10.5e\n", Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) );
//printf( "WM_thetaL=%10.5e\n", Wigner_matrix( 0.5, hL, 0.5, thetaL ) );
//printf( "WM_thetaLstar=%10.5e\n", Wigner_matrix( 0.5, 0.5, hp, thetaLstar ) );
//printf( "eK1=%10.5e\t%10.5e\n", eK1.real(), eK1.imag() );
//printf( "Hw1=%10.5e\t%10.5e\n", Hw1.real(), Hw1.imag() );

//printf( "ampl1=%10.5e\t%10.5e\n", (Hs1*Hw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK1 
//	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 0.5, 0.5, hp, thetaLstar )).real(), 
//		(Hs1*Hw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK1 
//	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 0.5, 0.5, hp, thetaLstar )).imag() );



return Ampl1;
}



//////////////////////////////////////////////////////////////////////////////////////////
//reloaded full Lstar12 //////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
std::complex<float> HamplitudeLstar12(int Lstarparity, float M, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, std::complex<float> Bw4, float hL, float hp, float hmu, float thetaLstar, float thetaL, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = 1.0; //cos(hL*phiLstar) + ix*sin(hL*phiLstar); //exp(ix*hL*phiPc);

//std::complex<float> eK0 = cosf(1.5*phiK) + ix*sinf(1.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK1 = cosf(0.5*phiK) + ix*sinf(0.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK2 = cosf(0.5*phiK) - ix*sinf(0.5*phiK); //exp(-ix*phiPsi);
//std::complex<float> eK3 = cosf(1.5*phiK) - ix*sinf(1.5*phiK); //exp(-ix*phiPsi);


std::complex<float> Ampl1 = 0.0;
std::complex<float> Hs1, Hw1, Hw2, Hw3, Hw4;

Hs1 = 1.0;
if(Lstarparity > 0 && hp == -0.5) Hs1 = -1.0;   
int LLb = 0;
if(Lstarparity > 0) LLb = 1;

int LLstar = 0;
if(Lstarparity > 0) LLstar = 1;

// lambda lstar = 0.5, lambdapsi = 0
Hw1 = sqrtf(1.0/6.0)*Bw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) - sqrtf(1.0/6.0)*Bw2*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 )
	- sqrtf(1.0/3.0)*Bw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 ) + sqrtf(1.0/3.0)*Bw4*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 2 );

// lambda lstar = 0.5, lambdapsi = 1
Hw2 = sqrtf(1.0/3.0)*Bw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) + sqrtf(1.0/3.0)*Bw2*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 )
	- sqrtf(1.0/6.0)*Bw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 ) - sqrtf(1.0/6.0)*Bw4*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 2 );

// lambda lstar = -0.5, lambdapsi = -1
Hw3 = -sqrtf(1.0/3.0)*Bw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) + sqrtf(1.0/3.0)*Bw2*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 )
	- sqrtf(1.0/6.0)*Bw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 ) + sqrtf(1.0/6.0)*Bw4*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 2 );

// lambda lstar = -0.5, lambdapsi = 0
Hw4 = -sqrtf(1.0/6.0)*Bw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) - sqrtf(1.0/6.0)*Bw2*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 )
	- sqrtf(1.0/3.0)*Bw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 ) - sqrtf(1.0/3.0)*Bw4*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 2 );



//BreitWignerLstar( float ML, float M0L, float massLb, float gamma0, int LLstar, int LLb )

Ampl1 += Hs1*Hw1* Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK1 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 0.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw2 * Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK1 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 0.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw3 * Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) * emu2 * eL * eK2 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 0.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw4 * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK2 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 0.5, -0.5, hp, thetaLstar );


return Ampl1;
}



//////////////////////////////////////////////////////////////////////////////////////////
//minimal Lstar32 //////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
std::complex<float> HamplitudeLstar32(int Lstarparity, float M, float M0, float gamma, std::complex<float> Bw1, float hL, float hp, float hmu, float thetaLstar, float thetaL, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = 1.0; //cos(hL*phiLstar) + ix*sin(hL*phiLstar); //exp(ix*hL*phiPc);

std::complex<float> eK0 = cosf(1.5*phiK) + ix*sinf(1.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK1 = cosf(0.5*phiK) + ix*sinf(0.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK2 = cosf(0.5*phiK) - ix*sinf(0.5*phiK); //exp(-ix*phiPsi);
std::complex<float> eK3 = cosf(1.5*phiK) - ix*sinf(1.5*phiK); //exp(-ix*phiPsi);

std::complex<float> Ampl1 = 0.0;
std::complex<float> Hs1;
std::complex<float> Hw1, Hw2, Hw3, Hw4, Hw5, Hw6;

Hs1 = 1.0;
if(Lstarparity < 0 && hp == -0.5) Hs1 = -1.0;   /// do not do it when Lstar parity is negative

//int LLb = 1;
//if(Lstarparity > 0) LLb = 2;

int LLstar = 1;
if(Lstarparity > 0) LLstar = 2;



Hw1 = -Bw1/(float)(2.0);
Hw2 = sqrtf(1.0/6.0)*Bw1;
Hw3 = -sqrtf(1.0/12.0)*Bw1;
Hw4 = -sqrtf(1.0/12.0)*Bw1;
Hw5 = sqrtf(1.0/6.0)*Bw1;
Hw6 = -Bw1/(float)(2.0);


//BreitWignerLstar( float ML, float M0L, float massLb, float gamma0, int LLstar, int LLb )
Ampl1 +=  Hs1*Hw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK0 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 1.5, 1.5, hp, thetaLstar ) ;

Ampl1 += Hs1*Hw2*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK1 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 1.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) *emu2 * eL * eK2 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 1.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw4*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK1 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 1.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw5*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK2 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 1.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw6*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) * emu2 * eL * eK3 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 1.5, -1.5, hp, thetaLstar );


return Ampl1;
}


//////////////////////////////////////////////////////////////////////////////////////////
//reloaded extended Lstar32 //////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
std::complex<float> HamplitudeLstar32(int Lstarparity, float M, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hL, float hp, float hmu, float thetaLstar, float thetaL, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = 1.0; //cos(hL*phiLstar) + ix*sin(hL*phiLstar); //exp(ix*hL*phiPc);

std::complex<float> eK0 = cosf(1.5*phiK) + ix*sinf(1.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK1 = cosf(0.5*phiK) + ix*sinf(0.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK2 = cosf(0.5*phiK) - ix*sinf(0.5*phiK); //exp(-ix*phiPsi);
std::complex<float> eK3 = cosf(1.5*phiK) - ix*sinf(1.5*phiK); //exp(-ix*phiPsi);

std::complex<float> Ampl1 = 0.0;
std::complex<float> Hs1;
std::complex<float> Hw1, Hw2, Hw3, Hw4, Hw5, Hw6;

Hs1 = 1.0;
if(Lstarparity < 0 && hp == -0.5) Hs1 = -1.0;   

int LLb = 1;
if(Lstarparity > 0) LLb = 2;

int LLstar = 1;
if(Lstarparity > 0) LLstar = 2;

Hw1 = Bw1/(float)(2.0)*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) - Bw2/(float)(2.0)*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 )
	-sqrtf(1.0/5.0)*Bw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 );

Hw2 = -sqrtf(1.0/6.0)*Bw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) + sqrtf(1.0/6.0)*Bw2*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 )
	-sqrtf(1.0/30.0)*Bw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 );

Hw3 = sqrtf(1.0/12.0)*Bw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) -sqrtf(1.0/12.0)*Bw2*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 ) 
	+ sqrtf(4.0/15.0)*Bw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 );

Hw4 = sqrtf(1.0/12.0)*Bw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) -sqrtf(1.0/12.0)*Bw2*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 ) 
	+ sqrtf(4.0/15.0)*Bw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 );

Hw5 = -sqrtf(1.0/6.0)*Bw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) - sqrtf(1.0/6.0)*Bw2*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 )
	+sqrtf(1.0/30.0)*Bw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 );

Hw6 = Bw1/(float)(2.0)*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 0 ) + Bw2/(float)(2.0)*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 )
	+sqrtf(1.0/5.0)*Bw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 );


//BreitWignerLstar( float ML, float M0L, float massLb, float gamma0, int LLstar, int LLb )
Ampl1 +=  Hs1*Hw1* Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK0 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 1.5, 1.5, hp, thetaLstar ) ;

Ampl1 += Hs1*Hw2*Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK1 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 1.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw3* Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) *emu2 * eL * eK2 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 1.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw4* Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK1 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 1.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw5* Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK2 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 1.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw6* Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) * emu2 * eL * eK3 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 1.5, -1.5, hp, thetaLstar );


return Ampl1;
}


bool test_flag = 1;

std::complex<float> HamplitudeLstar52(int Lstarparity, float M, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hL, float hp, float hmu, float thetaLstar, float thetaL, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = 1.0; ///cos(hL*phiLstar) + ix*sin(hL*phiLstar); //exp(ix*hL*phiPc);

std::complex<float> eK0 = cosf(1.5*phiK) + ix*sinf(1.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK1 = cosf(0.5*phiK) + ix*sinf(0.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK2 = cosf(0.5*phiK) - ix*sinf(0.5*phiK); //exp(-ix*phiPsi);
std::complex<float> eK3 = cosf(1.5*phiK) - ix*sinf(1.5*phiK); //exp(-ix*phiPsi);

std::complex<float> Ampl1 = 0.0;
std::complex<float> Hs1;
std::complex<float> Hw1, Hw2, Hw3, Hw4, Hw5, Hw6;

Hs1 = 1.0;
if(Lstarparity > 0 && hp == -0.5) Hs1 = -1.0;   /// do not do it when Lstar parity is negative
//int LLb = 2;
//if(Lstarparity > 0) LLb = 3;

int LLstar = 2;
if(Lstarparity > 0) LLstar = 3;

Hw1 = -sqrtf(0.2)*Bw1;
Hw2 = sqrtf(0.2)*Bw1;
Hw3 = -sqrtf(0.1)*Bw1;
Hw4 = -sqrtf(0.1)*Bw1;
Hw5 = sqrtf(0.2)*Bw1;
Hw6 = -sqrtf(0.2)*Bw1;


//BreitWignerLstar( float ML, float M0L, float massLb, float gamma0, int LLstar, int LLb )
Ampl1 +=  Hs1*Hw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 ) * Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK0 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 2.5, 1.5, hp, thetaLstar ) ;

Ampl1 += Hs1*Hw2*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK1 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 2.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 ) * Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) *emu2 * eL * eK2 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 2.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw4*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 ) * Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK1 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 2.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw5*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK2 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 2.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw6*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 ) * Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) * emu2 * eL * eK3 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 2.5, -1.5, hp, thetaLstar );
    
    /*if (((M0 - 2.110) < 0.01) && test_flag)
    {
        cout << "Amp52:\t" << abs(BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 )) << "\t" << abs(BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 1 ) * Bw1) << "\t" << abs(Ampl1) << endl;
    }*/

return Ampl1;
}


std::complex<float> HamplitudeLstar72(int Lstarparity, float M, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hL, float hp, float hmu, float thetaLstar, float thetaL, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = 1.0; ///cos(hL*phiLstar) + ix*sin(hL*phiLstar); //exp(ix*hL*phiPc);

std::complex<float> eK0 = cosf(1.5*phiK) + ix*sinf(1.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK1 = cosf(0.5*phiK) + ix*sinf(0.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK2 = cosf(0.5*phiK) - ix*sinf(0.5*phiK); //exp(-ix*phiPsi);
std::complex<float> eK3 = cosf(1.5*phiK) - ix*sinf(1.5*phiK); //exp(-ix*phiPsi);

std::complex<float> Ampl1 = 0.0;
std::complex<float> Hs1;
std::complex<float> Hw1, Hw2, Hw3, Hw4, Hw5, Hw6;

Hs1 = 1.0;
if(Lstarparity < 0 && hp == -0.5) Hs1 = -1.0;   /// do not do it when Lstar parity is negative
//int LLb = 3;
//if(Lstarparity > 0) LLb = 4;

int LLstar = 3;
if(Lstarparity > 0) LLstar = 4;

Hw1 = sqrtf(5.0/28.0)*Bw1;
Hw2 = -sqrtf(3.0/14.0)*Bw1;
Hw3 = sqrtf(3.0/28.0)*Bw1;
Hw4 = sqrtf(3.0/28.0)*Bw1;
Hw5 = -sqrtf(3.0/14.0)*Bw1;
Hw6 = sqrtf(5.0/28.0)*Bw1;


//BreitWignerLstar( float ML, float M0L, float massLb, float gamma0, int LLstar, int LLb )
Ampl1 +=  Hs1*Hw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 2 ) * Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK0 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 3.5, 1.5, hp, thetaLstar ) ;

Ampl1 += Hs1*Hw2*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 2 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK1 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 3.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 2 ) * Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) *emu2 * eL * eK2 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 3.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw4*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 2 ) * Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK1 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 3.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw5*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 2 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK2 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 3.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw6*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 2 ) * Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) * emu2 * eL * eK3 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 3.5, -1.5, hp, thetaLstar );

    /*if (((M0 - 2.100) < 0.01) && test_flag)
    {
        cout << "Amp72:\t" << abs(BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 2 )) << "\t" << abs(BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 2 ) * Bw1) << "\t" << abs(Ampl1) << endl;
    }*/
    
return Ampl1;
}




std::complex<float> HamplitudeLstar92(int Lstarparity, float M, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hL, float hp, float hmu, float thetaLstar, float thetaL, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = 1.0; ///cos(hL*phiLstar) + ix*sin(hL*phiLstar); //exp(ix*hL*phiPc);

std::complex<float> eK0 = cosf(1.5*phiK) + ix*sinf(1.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK1 = cosf(0.5*phiK) + ix*sinf(0.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK2 = cosf(0.5*phiK) - ix*sinf(0.5*phiK); //exp(-ix*phiPsi);
std::complex<float> eK3 = cosf(1.5*phiK) - ix*sinf(1.5*phiK); //exp(-ix*phiPsi);

std::complex<float> Ampl1 = 0.0;
std::complex<float> Hs1;
std::complex<float> Hw1, Hw2, Hw3, Hw4, Hw5, Hw6;

Hs1 = 1.0;
if(Lstarparity > 0 && hp == -0.5) Hs1 = -1.0;   /// do not do it when Lstar parity is negative
int LLb = 4;
if(Lstarparity > 0) LLb = 5;

int LLstar = 4;
if(Lstarparity > 0) LLstar = 5;

Hw1 = -sqrtf(1.0/6.0)*Bw1;
Hw2 = sqrtf(2.0/9.0)*Bw1;
Hw3 = -(float)(0.3333333)*Bw1;
Hw4 = -(float)(0.3333333)*Bw1;
Hw5 = sqrtf(2.0/9.0)*Bw1;
Hw6 = -sqrtf(1.0/6.0)*Bw1;


//BreitWignerLstar( float ML, float M0L, float massLb, float gamma0, int LLstar, int LLb )
Ampl1 +=  Hs1*Hw1*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 3 ) * Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK0 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 4.5, 1.5, hp, thetaLstar ) ;

Ampl1 += Hs1*Hw2*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 3 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK1 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 4.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw3*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 3 ) * Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) *emu2 * eL * eK2 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 4.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw4*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 3 ) * Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK1 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 4.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw5*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 3 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK2 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 4.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw6*BreitWignerLstar( M, M0, MASS_LB, gamma, LLstar, 3 ) * Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) * emu2 * eL * eK3 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 4.5, -1.5, hp, thetaLstar );


return Ampl1;
}


//////////////////////////////////////////////////////////////////////////////////////////
//nonresonant minimal Lstar12 //////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
std::complex<float> HamplitudeLstar12NR(int Lstarparity, float M, float M0, float gamma, std::complex<float> Bw1, float hL, float hp, float hmu, float thetaLstar, float thetaL, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = 1.0; //cos(hL*phiLstar) + ix*sin(hL*phiLstar); //exp(ix*hL*phiPc);

//std::complex<float> eK0 = cosf(1.5*phiK) + ix*sinf(1.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK1 = cosf(0.5*phiK) + ix*sinf(0.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK2 = cosf(0.5*phiK) - ix*sinf(0.5*phiK); //exp(-ix*phiPsi);
//std::complex<float> eK3 = cosf(1.5*phiK) - ix*sinf(1.5*phiK); //exp(-ix*phiPsi);


std::complex<float> Ampl1 = 0.0;
std::complex<float> Hs1, Hw1, Hw2, Hw3, Hw4;

Hs1 = 1.0;
if(Lstarparity > 0 && hp == -0.5) Hs1 = -1.0;   /// do not do it when Lstar parity is negative

//int LLb = 0;
//if(Lstarparity > 0) LLb = 1;

int LLstar = 0;
if(Lstarparity > 0) LLstar = 1;

Hw1 = sqrtf(1.0/6.0)*Bw1;
Hw2 = sqrtf(1.0/3.0)*Bw1;
Hw3 = -sqrtf(1.0/3.0)*Bw1;
Hw4 = -sqrtf(1.0/6.0)*Bw1;


//BreitWignerLstar( float ML, float M0L, float massLb, float gamma0, int LLstar, int LLb )

Ampl1 += Hs1*Hw1*BreitWignerLstarNR( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK1 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 0.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw2*BreitWignerLstarNR( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK1 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 0.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw3*BreitWignerLstarNR( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) * emu2 * eL * eK2 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 0.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw4*BreitWignerLstarNR( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK2 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 0.5, -0.5, hp, thetaLstar );



return Ampl1;
}


//////////////////////////////////////////////////////////////////////////////////////////
//nonresonant minimal Lstar32 //////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
std::complex<float> HamplitudeLstar32NR(int Lstarparity, float M, float M0, float gamma, std::complex<float> Bw1, float hL, float hp, float hmu, float thetaLstar, float thetaL, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cosf(phimu) + ix*sinf(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cosf(phimu) - ix*sinf(phimu);
std::complex<float> eL = 1.0; //cos(hL*phiLstar) + ix*sin(hL*phiLstar); //exp(ix*hL*phiPc);

std::complex<float> eK0 = cosf(1.5*phiK) + ix*sinf(1.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK1 = cosf(0.5*phiK) + ix*sinf(0.5*phiK); //exp(ix*phiPsi);
std::complex<float> eK2 = cosf(0.5*phiK) - ix*sinf(0.5*phiK); //exp(-ix*phiPsi);
std::complex<float> eK3 = cosf(1.5*phiK) - ix*sinf(1.5*phiK); //exp(-ix*phiPsi);

std::complex<float> Ampl1 = 0.0;
std::complex<float> Hs1;
std::complex<float> Hw1, Hw2, Hw3, Hw4, Hw5, Hw6;

Hs1 = 1.0;
if(Lstarparity < 0 && hp == -0.5) Hs1 = -1.0;   /// do not do it when Lstar parity is negative

//int LLb = 1;
//if(Lstarparity > 0) LLb = 2;

int LLstar = 1;
if(Lstarparity > 0) LLstar = 2;



Hw1 = -Bw1/(float)(2.0);
Hw2 = sqrtf(1.0/6.0)*Bw1;
Hw3 = -sqrtf(1.0/12.0)*Bw1;
Hw4 = -sqrtf(1.0/12.0)*Bw1;
Hw5 = sqrtf(1.0/6.0)*Bw1;
Hw6 = -Bw1/(float)(2.0);


//BreitWignerLstar( float ML, float M0L, float massLb, float gamma0, int LLstar, int LLb )
Ampl1 +=  Hs1*Hw1*BreitWignerLstarNR( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK0 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 1.5, 1.5, hp, thetaLstar ) ;

Ampl1 += Hs1*Hw2*BreitWignerLstarNR( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK1 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 1.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw3*BreitWignerLstarNR( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) *emu2 * eL * eK2 
	* Wigner_matrix( 0.5, hL, 0.5, thetaL ) * Wigner_matrix( 1.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw4*BreitWignerLstarNR( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 1.0, hmu, thetaPsi ) * emu1 * eL * eK1 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 1.5, 0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw5*BreitWignerLstarNR( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, 0.0, hmu, thetaPsi ) * eL * eK2 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 1.5, -0.5, hp, thetaLstar );

Ampl1 += Hs1*Hw6*BreitWignerLstarNR( M, M0, MASS_LB, gamma, LLstar, 0 ) * Wigner_matrix( 1.0, -1.0, hmu, thetaPsi ) * emu2 * eL * eK3 
	* Wigner_matrix( 0.5, hL, -0.5, thetaL ) * Wigner_matrix( 1.5, -1.5, hp, thetaLstar );


return Ampl1;
}





float HamplitudePcLstar_total(float MPc, float MLstar, float MLb, float M01, float M02, float M03, float M04, float gamma1, float gamma2, float gamma3, float gamma4,
						std::complex<float> Pc1Bw1, std::complex<float> Pc1Bw2, std::complex<float> Pc1Bs1, std::complex<float> Pc1Bs2, std::complex<float> Pc1Bs3,
						std::complex<float> Pc2Bw1, std::complex<float> Pc2Bw2, std::complex<float> Pc2Bs1, std::complex<float> Pc2Bs2, std::complex<float> Pc2Bs3,
						std::complex<float> Pc3Bw1, std::complex<float> Pc3Bw2, std::complex<float> Pc3Bs1, std::complex<float> Pc3Bs2, std::complex<float> Pc3Bs3,
						std::complex<float> Pc4Bw1, std::complex<float> Pc4Bw2, std::complex<float> Pc4Bs1, std::complex<float> Pc4Bs2, std::complex<float> Pc4Bs3,
						std::complex<float> Ls1800_12_Bw1, std::complex<float> Ls1800_12_Bw2, std::complex<float> Ls1800_12_Bw3, std::complex<float> Ls1800_12_Bw4,
						std::complex<float> Ls1810_12_Bw1, std::complex<float> Ls1810_12_Bw2, std::complex<float> Ls1810_12_Bw3,
						std::complex<float> Ls1820_52_Bw1, std::complex<float> Ls1820_52_Bw2, std::complex<float> Ls1820_52_Bw3,
						std::complex<float> Ls1830_52_Bw1, std::complex<float> Ls1830_52_Bw2, std::complex<float> Ls1830_52_Bw3,
						std::complex<float> Ls1890_32_Bw1, std::complex<float> Ls1890_32_Bw2, std::complex<float> Ls1890_32_Bw3,
						std::complex<float> Ls2020_72_Bw1, std::complex<float> Ls2020_72_Bw2, std::complex<float> Ls2020_72_Bw3,
						std::complex<float> Ls2050_32_Bw1, std::complex<float> Ls2050_32_Bw2, std::complex<float> Ls2050_32_Bw3,
						std::complex<float> Ls2100_72_Bw1, std::complex<float> Ls2100_72_Bw2, std::complex<float> Ls2100_72_Bw3,
						std::complex<float> Ls2110_52_Bw1, std::complex<float> Ls2110_52_Bw2, std::complex<float> Ls2110_52_Bw3,
						std::complex<float> Ls2325_32_Bw1, std::complex<float> Ls2325_32_Bw2, std::complex<float> Ls2325_32_Bw3,
						std::complex<float> Ls2350_92_Bw1, std::complex<float> Ls2350_92_Bw2, std::complex<float> LsNR_12_Bw,
						float thetaLstar, float thetaLLstar, float thetaPsiLstar, float phimuLstar, float phiK,
						float thetaPc, float thetaLPc, float thetaPsiPc, float phimuPc, float phiPc, float phiPsi, 
						float alpha_mu, float thetap )
{
	
	float MeL = 0;
	
	std::complex<float> ix(0,1);

	for(int i1 = 0; i1 < 2; i1++)
	for(int i2 = 0; i2 < 2; i2++)
	for(int i3 = 0; i3 < 2; i3++)
	{
	
	float hL = i1 - 0.5;
	float hp = i2 - 0.5;
	float hmu = 2*i3 - 1.0;
	std::complex<float> A1Pc = 0.0;
	std::complex<float> A2Pc = 0.0;
	std::complex<float> A3Pc = 0.0;
	std::complex<float> A4Pc = 0.0;

	if( Pc_32minus_52plus == 1 ){
	 A1Pc = Hamplitude32minus(MPc, M01, gamma1, Pc1Bw1, Pc1Bw2, Pc1Bs1, Pc1Bs2, Pc1Bs3,  hL, hp, hmu, thetaPc, thetaLPc, thetaPsiPc, phimuPc, phiPc, phiPsi, alpha_mu, thetap );
	 A2Pc = Hamplitude52plus(MPc, M02, gamma2, Pc2Bw1, Pc2Bw2, Pc2Bs1, Pc2Bs2, Pc2Bs3,  hL, hp, hmu, thetaPc, thetaLPc, thetaPsiPc, phimuPc, phiPc, phiPsi, alpha_mu, thetap );
	}
	if( Pc_32plus_52minus == 1 ){
	 A1Pc = Hamplitude32plus(MPc, M01, gamma1, Pc1Bw1, Pc1Bw2, Pc1Bs1, Pc1Bs2, Pc1Bs3,  hL, hp, hmu, thetaPc, thetaLPc, thetaPsiPc, phimuPc, phiPc, phiPsi, alpha_mu, thetap );
	 A2Pc = Hamplitude52minus(MPc, M02, gamma2, Pc2Bw1, Pc2Bw2, Pc2Bs1, Pc2Bs2, Pc2Bs3,  hL, hp, hmu, thetaPc, thetaLPc, thetaPsiPc, phimuPc, phiPc, phiPsi, alpha_mu, thetap );
	}
	if( Pc4_12minus_32minus_12minus_32minus == 1 ){
	 A1Pc = Hamplitude32minus(MPc, M01, gamma1, Pc1Bw1, Pc1Bw2, Pc1Bs1, Pc1Bs2, Pc1Bs3,  hL, hp, hmu, thetaPc, thetaLPc, thetaPsiPc, phimuPc, phiPc, phiPsi, alpha_mu, thetap );
	 A2Pc = Hamplitude12minus(MPc, M02, gamma2, Pc2Bw1, Pc2Bw2, Pc2Bs1, Pc2Bs2, Pc2Bs3,  hL, hp, hmu, thetaPc, thetaLPc, thetaPsiPc, phimuPc, phiPc, phiPsi, alpha_mu, thetap );
	 A3Pc = Hamplitude12minus(MPc, M03, gamma3, Pc3Bw1, Pc3Bw2, Pc3Bs1, Pc3Bs2, Pc3Bs3,  hL, hp, hmu, thetaPc, thetaLPc, thetaPsiPc, phimuPc, phiPc, phiPsi, alpha_mu, thetap );
	 A4Pc = Hamplitude32minus(MPc, M04, gamma4, Pc4Bw1, Pc4Bw2, Pc4Bs1, Pc4Bs2, Pc4Bs3,  hL, hp, hmu, thetaPc, thetaLPc, thetaPsiPc, phimuPc, phiPc, phiPsi, alpha_mu, thetap );
	
	}

	//printf("APc32minus_real = %10.5e \t APc32minus_imag = %10.5e\n", A1Pc.real(), A1Pc.imag() );
	//printf("Pc1Bw1_real = %10.5e \t Pc1Bw1_imag = %10.5e\n", Pc1Bw1.real(), Pc1Bw1.imag() );

	//getchar();

	std::complex<float> ALstar1800_12 = HamplitudeLstar12( -1, MLstar, 1.80, 0.300, Ls1800_12_Bw1, Ls1800_12_Bw2, Ls1800_12_Bw3, Ls1800_12_Bw4, hL, hp, hmu, thetaLstar, thetaLLstar, thetaPsiLstar, phimuLstar, phiK );
	std::complex<float> ALstar1810_12 = HamplitudeLstar12( 1, MLstar, 1.81, 0.150, Ls1810_12_Bw1, Ls1810_12_Bw2, Ls1810_12_Bw3, 0.0, hL, hp, hmu, thetaLstar, thetaLLstar, thetaPsiLstar, phimuLstar, phiK );
	std::complex<float> ALstar1820_52 = HamplitudeLstar52( 1, MLstar, 1.82, 0.080, Ls1820_52_Bw1, Ls1820_52_Bw2, Ls1820_52_Bw3, hL, hp, hmu, thetaLstar, thetaLLstar, thetaPsiLstar, phimuLstar, phiK );
	std::complex<float> ALstar1830_52 = HamplitudeLstar52( -1, MLstar, 1.83, 0.095, Ls1830_52_Bw1, Ls1830_52_Bw2, Ls1830_52_Bw3, hL, hp, hmu, thetaLstar, thetaLLstar, thetaPsiLstar, phimuLstar, phiK );
	std::complex<float> ALstar1890_32 = HamplitudeLstar32( 1, MLstar, 1.89, 0.100, Ls1890_32_Bw1, Ls1890_32_Bw2, Ls1890_32_Bw3, hL, hp, hmu, thetaLstar, thetaLLstar, thetaPsiLstar, phimuLstar, phiK );
	/// need amplitude72
	std::complex<float> ALstar2020_72 = HamplitudeLstar72( 1, MLstar, 2.020, 0.100, Ls2020_72_Bw1, Ls2020_72_Bw2, Ls2020_72_Bw3, hL, hp, hmu, thetaLstar, thetaLLstar, thetaPsiLstar, phimuLstar, phiK );
	std::complex<float> ALstar2050_32 = HamplitudeLstar32( -1, MLstar, 2.056, 0.493, Ls2050_32_Bw1, Ls2050_32_Bw2, Ls2050_32_Bw3, hL, hp, hmu, thetaLstar, thetaLLstar, thetaPsiLstar, phimuLstar, phiK );
	std::complex<float> ALstar2100_72 = HamplitudeLstar72( -1, MLstar, 2.100, 0.200, Ls2100_72_Bw1, Ls2100_72_Bw2, Ls2100_72_Bw3, hL, hp, hmu, thetaLstar, thetaLLstar, thetaPsiLstar, phimuLstar, phiK );
	std::complex<float> ALstar2110_52 = HamplitudeLstar52( 1, MLstar, 2.110, 0.200, Ls2110_52_Bw1, Ls2110_52_Bw2, Ls2110_52_Bw3, hL, hp, hmu, thetaLstar, thetaLLstar, thetaPsiLstar, phimuLstar, phiK );
	std::complex<float> ALstar2325_32 = HamplitudeLstar32( -1, MLstar, 2.325, 0.100, Ls2325_32_Bw1, Ls2325_32_Bw2, Ls2325_32_Bw3, hL, hp, hmu, thetaLstar, thetaLLstar, thetaPsiLstar, phimuLstar, phiK );
	std::complex<float> ALstar2350_92 = HamplitudeLstar92( 1, MLstar, 2.350, 0.150, Ls2350_92_Bw1, 0.0, 0.0, hL, hp, hmu, thetaLstar, thetaLLstar, thetaPsiLstar, phimuLstar, phiK );

	std::complex<float> ALstarNR_12 =  0.0;
	if(includeLsNRsimple == 1) ALstarNR_12 = LsNR_12_Bw;
	if(includeLsNR == 1) ALstarNR_12 = HamplitudeLstar12NR( 1, MLstar, 1.80, 0.300, LsNR_12_Bw, hL, hp, hmu, thetaLstar, thetaLLstar, thetaPsiLstar, phimuLstar, phiK );	

	float ad = 0.0;

	std::complex<float> Ampl_int = ALstar1800_12 + ALstar1810_12 + ALstar1820_52 + ALstar1830_52 + ALstar1890_32
			 + ALstar2020_72 + ALstar2050_32 + ALstar2100_72 + ALstar2110_52 + ALstar2325_32 + ALstar2350_92 + ALstarNR_12;

	if(Pc1_interference_off == 0) Ampl_int += A1Pc;
		else ad += pow( abs(A1Pc), 2.0 );
	if(Pc2_interference_off == 0) Ampl_int += A2Pc;
		else ad += pow( abs(A2Pc), 2.0 );
	if(Pc3_interference_off == 0) Ampl_int += A3Pc;
		else ad += pow( abs(A3Pc), 2.0 );
	if(Pc4_interference_off == 0) Ampl_int += A4Pc;
		else ad += pow( abs(A4Pc), 2.0 );
	
	ad += pow( abs(Ampl_int), 2.0 );

	MeL += ad;
	 if(ad != ad){
		printf("A1800=%10.2e\n", abs(ALstar1800_12));
		printf("A1810=%10.2e\n", abs(ALstar1810_12));
		printf("A1820=%10.2e\n", abs(ALstar1820_52));
		printf("A1830=%10.2e\n", abs(ALstar1830_52));
		printf("A1890=%10.2e\n", abs(ALstar1890_32));
		printf("A2020=%10.2e\n", abs(ALstar2020_72));
		printf("A2050=%10.2e\n", abs(ALstar2050_32));
		printf("A2100=%10.2e\n", abs(ALstar2100_72));
		printf("A2110=%10.2e\n", abs(ALstar2110_52));
		printf("A2325=%10.2e\n", abs(ALstar2325_32));
		printf("A2350=%10.2e\n", abs(ALstar2350_92));
		printf("ANR=%10.2e\n", abs(ALstarNR_12));
		printf("APc1=%10.2e\n", abs(A1Pc));
		printf("APc2=%10.2e\n", abs(A2Pc));
		printf("APc3=%10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e\n", abs(A3Pc), M03, gamma3, abs(Pc3Bw1), abs(Pc3Bw2), abs(Pc3Bs1), abs(Pc3Bs2), abs(Pc3Bs3) );
		printf("APc4=%10.2e\n", abs(A4Pc));

		
		getchar();
	}


	}

	//std::complex<float> BLS1_test (0.5, 0.1 );
	//std::complex<float> BLS2_test (0.4, 0.9 );
	//std::complex<float> BLS3_test (0.0, 1.2 );

	//std::complex<float> APc32minus = Hamplitude32minus( 4.42, 4.40, 0.1, 1.0, BLS1_test, 1.0, 1.0, 0.0,  0.5, 0.5, -1.0, 0.2, -0.5, 1.5, -1.2, 0.0, 2.3, 0.4, 0.6 );
	//std::complex<float> APc32plus = Hamplitude32plus( 4.45, 4.39, 0.1, BLS2_test, 0.0, BLS3_test, 1.0, 0.0,  0.5, -0.5, 1.0, 0.2, -0.5, 1.5, -1.2, 0.0, 2.3, -0.4, 0.1 );
	//std::complex<float> APc52plus = Hamplitude52plus(4.30, 4.45, 0.03, BLS1_test, 0.0, BLS3_test, 1.0, 0.0,  0.5, -0.5, 1.0, 0.2, -0.5, 1.5, -1.2, 0.0, 2.3, -0.4, 0.1 );
	//std::complex<float> APc52minus = Hamplitude52minus(4.30, 4.45, 0.03, BLS2_test, 0.0, BLS1_test, 1.0, 0.0,  -0.5, 0.5, 1.0, 0.2, -0.5, 1.5, -1.2, 0.0, 2.3, -0.4, -0.1 );

	//printf("APc32minus_real = %10.5e \t APc32minus_imag = %10.5e\n", APc32minus.real(), APc32minus.imag() );
	//printf("APc32plus_real = %10.5e \t APc32plus_imag = %10.5e\n", APc32plus.real(), APc32plus.imag() );
	//printf("APc52plus_real = %10.5e \t APc52plus_imag = %10.5e\n", APc52plus.real(), APc52plus.imag() );
	//printf("APc52minus_real = %10.5e \t APc52minus_imag = %10.5e\n", APc52minus.real(), APc52minus.imag() );

	///getchar();
	
	if(MeL != MeL) { printf("in Pc total Amplitude MeL is Nan"); getchar();}
	return MeL/3.0;
}

///// angle between Pc and Lambda_b in Lambda_b rest frame, Lambda_b momentum is taken in lab system;
Double_t theta_lambda_b(TLorentzVector vLambda, TLorentzVector vPc)
{

	TVector3 vLambda3 = vLambda.BoostVector();
	TLorentzVector vPc_b = vPc;
	vPc_b.Boost(-vLambda3);

return vPc_b.Vect().Angle( vLambda.Vect() );
}


Double_t theta_lambda_b_ls(TLorentzVector vLambda, TLorentzVector vp, TLorentzVector vK )
{

	TVector3 vLambda3 = vLambda.BoostVector();
	TLorentzVector vLstar_b = vp + vK;
	vLstar_b.Boost(-vLambda3);

return vLstar_b.Vect().Angle( vLambda.Vect() );
}

//////////////////////////////////////////////////////////////////////////////////////////////
///// angle between Jpsi and Pc in Pc rest frame, Pc momentum is taken in Lambda_b rest frame;
Double_t theta_Pc(TLorentzVector vLambda, TLorentzVector vPc, TLorentzVector vpsi)
{
	TVector3 vLambda3 = vLambda.BoostVector();
	TVector3 vPc3 = vPc.BoostVector();
	TLorentzVector vpsi_b = vpsi;
	vpsi_b.Boost(-vPc3);
	TLorentzVector vPc_b2 = vPc;
	vPc_b2.Boost(-vLambda3);

return vpsi_b.Vect().Angle( vPc_b2.Vect() );
}

//////////////////////////////////////////////////////////////////////////////////////////////
///// angle between Kaon and Lstar candidate in Lstar rest frame, Lstar momentum is taken in Lambda_b rest frame;
Double_t theta_Lstar( TLorentzVector vLambda, TLorentzVector vLstar, TLorentzVector vK )
{
	
	TVector3 vLambda3 = vLambda.BoostVector();
	TVector3 vLstar3 = vLstar.BoostVector();

	TLorentzVector vK_b = vK;
	vK_b.Boost(-vLstar3);
	
	TLorentzVector vLstarb = vLstar;
	vLstarb.Boost(-vLambda3);
	return vK_b.Vect().Angle(vLstarb.Vect()) ;

}


///// angle between Jpsi and muon in Jpsi rest frame, Jpsi momentum is taken in Pc rest frame;
Double_t theta_psi(TLorentzVector vPc, TLorentzVector vpsi, TLorentzVector v_mu2)
{

	TVector3 vPc3 = vPc.BoostVector();
	TVector3 vpsi3 = vpsi.BoostVector();
	TLorentzVector v_mu2_b = v_mu2; 
	v_mu2_b.Boost(-vpsi3);
	TLorentzVector vpsi_b = vpsi;
	vpsi_b.Boost(-vPc3);

return v_mu2_b.Vect().Angle(vpsi_b.Vect());
}


///// angle between Jpsi and muon in Jpsi rest frame, Jpsi momentum is taken in Pc rest frame;
Double_t phi_mu(TLorentzVector vPc, TLorentzVector vK, TLorentzVector vpsi, TLorentzVector vp, TLorentzVector v_mu2)
{
	TLorentzVector vK_b = vK;
	TLorentzVector vpsi_b = vpsi;
	TVector3 vPc3 = vPc.BoostVector();
	vK_b.Boost(-vPc3);
	vpsi_b.Boost(-vPc3);

	TVector3 x0 = vK_b.Vect().Unit() - ( vK_b.Vect().Unit() * vpsi_b.Vect().Unit() ) * vpsi_b.Vect().Unit();
	

	TLorentzVector vp_b = vp;
	TLorentzVector v_mu2_b = v_mu2;
	TVector3 vpsi3 = vpsi.BoostVector();
	vp_b.Boost(-vpsi3);
	v_mu2_b.Boost(-vpsi3);
	
	double argy = ( -vp_b.Vect().Unit().Cross(x0.Unit()) ) * v_mu2_b.Vect().Unit();
	double argx = v_mu2_b.Vect().Unit() * x0.Unit();

	return atan2( argy, argx );

}


Double_t phi_mu_Pc(TLorentzVector vPc, TLorentzVector vK, TLorentzVector vpsi, TLorentzVector v_mu2)
{
	TLorentzVector vK_b = vK;
	TLorentzVector vpsi_b = vpsi;
	TVector3 vPc3 = vPc.BoostVector();
	vK_b.Boost(-vPc3);
	vpsi_b.Boost(-vPc3);

	TVector3 x0 = vK_b.Vect().Unit() - ( vK_b.Vect().Unit() * vpsi_b.Vect().Unit() ) * vpsi_b.Vect().Unit();
	
	
	TLorentzVector v_mu2_b = v_mu2;
	TVector3 vpsi3 = vpsi.BoostVector();
	v_mu2_b.Boost(-vpsi3);
	
	double argy = ( vpsi_b.Vect().Unit().Cross(x0.Unit()) ) * v_mu2_b.Vect().Unit();
	double argx = v_mu2_b.Vect().Unit() * x0.Unit();

	return atan2( argy, argx );

}



Double_t phi_mu_Lstar(TLorentzVector vLambda, TLorentzVector vLstar, TLorentzVector vpsi, TLorentzVector v_mu2)
{
	TLorentzVector vLstar_b = vLstar;
	TLorentzVector vpsi_b = vpsi;
	TVector3 vLambda3 = vLambda.BoostVector();
	
	vpsi_b.Boost(-vLambda3);
	vLstar_b.Boost(-vLambda3);

	TVector3 x0 = -vLambda.Vect().Unit() + ( vLambda.Vect().Unit() * vLstar_b.Vect().Unit() ) * vLstar_b.Vect().Unit();
	
	TLorentzVector v_mu2_b = v_mu2;
	TVector3 vpsi3 = vpsi.BoostVector();
	
	v_mu2_b.Boost(-vpsi3);
	
	double argy = ( vpsi_b.Vect().Unit().Cross(x0.Unit()) ) * v_mu2_b.Vect().Unit();
	double argx = v_mu2_b.Vect().Unit() * x0.Unit();

	return atan2( argy, argx );

}

Double_t phi_K_Lstar(TLorentzVector vLambda, TLorentzVector vLstar, TLorentzVector vK, TLorentzVector vpsi)
{
	
	TLorentzVector vK_b = vK;
	TLorentzVector vLstarb = vLstar;
	TLorentzVector vpsib = vpsi;
	TLorentzVector vKb = vK;
	TVector3 vLambda3 = vLambda.BoostVector();
	TVector3 vLstar3 = vLstar.BoostVector();
	

	vLstarb.Boost(-vLambda3);
	vpsib.Boost(-vLstar3);
	vKb.Boost(-vLstar3);

	TVector3 x0 = - vLambda.Vect().Unit() + ( vLambda.Vect().Unit() * vLstarb.Vect().Unit() ) * vLstarb.Vect().Unit();
	
	
	double argy = ( vLstarb.Vect().Unit().Cross(x0.Unit()) ) * vKb.Vect().Unit();
	double argx = x0.Unit() * vKb.Vect().Unit();

	return atan2( argy, argx );

}




Double_t phi_psi(TLorentzVector vLambda, TLorentzVector vPc, TLorentzVector vpsi, TLorentzVector vK )
{
	TVector3 vLambda3 = vLambda.BoostVector();
	TLorentzVector vPc_b = vPc;
	vPc_b.Boost(-vLambda3);
	
	TVector3 x0 = -vLambda.Vect().Unit() + ( vLambda.Vect().Unit() * vPc_b.Vect().Unit() ) * vPc_b.Vect().Unit();
//	printf("%10.5e\t", x0.Mag() ); getchar();

	TLorentzVector vK_b = vK;
	TLorentzVector vpsi_b = vpsi;
	TVector3 vPc3 = vPc.BoostVector();
	vK_b.Boost(-vPc3);
	vpsi_b.Boost(-vPc3);

	double argy = ( -vK_b.Vect().Unit().Cross( x0.Unit() ) ) * vpsi_b.Vect().Unit();   //x0.Unit();
	//double argy = ( vK_b.Vect().Unit().Cross(vpsi_b.Vect().Unit()) ) * x0.Unit();
	double argx = vpsi_b.Vect().Unit() * x0.Unit();

	return atan2(  argy,  argx );
}



Double_t phi_Pc(TLorentzVector vLambda, TLorentzVector vPc, TLorentzVector vLstar)
{
	
	TLorentzVector vPc_b = vPc;
	TLorentzVector vLstar_b = vLstar;
	TVector3 vLambda3 = vLambda.BoostVector();
	

	vPc_b.Boost(-vLambda3);
	vLstar_b.Boost(-vLambda3);

	TVector3 x0 = vLstar_b.Vect().Unit() - ( vLstar_b.Vect().Unit() * vLambda.Vect().Unit() ) * vLambda.Vect().Unit();
	
	
	double argy = ( vLambda.Vect().Unit().Cross(x0.Unit()) ) * vPc_b.Vect().Unit();
	double argx = x0.Unit() * vPc_b.Vect().Unit();

	return atan2( argy, argx );

}

Double_t theta_p(TLorentzVector vpsi, TLorentzVector vp, TLorentzVector vK)
{


	TLorentzVector vpsi_b = vpsi;
	TLorentzVector vK_b = vK;
	//TLorentzVector vLambda_b = vLambda;

	TVector3 vp3 = vp.BoostVector();
	

	vpsi_b.Boost(-vp3);
	vK_b.Boost(-vp3);


	return vK_b.Vect().Angle(vpsi_b.Vect()) ;
}

//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__|| arrays and hisograms  ||__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//

float Jpsipi1_mass[phs_Bd], Jpsipi2_mass[phs_Bd], JpsiK1_mass[phs_Bd], JpsiK2_mass[phs_Bd], K1pi2_mass[phs_Bd], K2pi1_mass[phs_Bd], p1K2_mass[phs_Bd], p2K1_mass[phs_Bd], Jpsip1_mass[phs_Bd], Jpsip2_mass[phs_Bd], Kpi_truth[phs_Bd], Jpsipi_truth[phs_Bd], p_pi[phs_Bd], p_K[phs_Bd], phi_K_a[phs_Bd], alpha_mu_a[phs_Bd], phi_mu_Kstar_a[phs_Bd], theta_psi_X_a[phs_Bd], phi_mu_X_a[phs_Bd], theta_X_a[phs_Bd], theta_Kstar_a[phs_Bd], theta_psi_Kstar_a[phs_Bd], weight[phs_Bd], w_dimuon_a[phs_Bd], KK_mass[phs_Bd], JpsiKpi_mass[phs_Bd], JpsipiK_mass[phs_Bd], JpsiKK_mass[phs_Bd], Jpsipipi_mass[phs_Bd], JpsipK_mass[phs_Bd], JpsiKp_mass[phs_Bd], cos_theta_Zc_Kpi_Bd[phs_Bd], cos_theta_Zc_piK_Bd[phs_Bd],
phi_K_Kpi_Bd[phs_Bd], phi_K_piK_Bd[phs_Bd],
alpha_mu_Kpi_Bd[phs_Bd], alpha_mu_piK_Bd[phs_Bd],
phi_mu_Kstar_Kpi_Bd[phs_Bd], phi_mu_Kstar_piK_Bd[phs_Bd],
phi_mu_X_Kpi_Bd[phs_Bd], phi_mu_X_piK_Bd[phs_Bd],
cos_theta_psi_X_Kpi_Bd[phs_Bd], cos_theta_psi_X_piK_Bd[phs_Bd],
cos_theta_Kstar_Kpi_Bd[phs_Bd], cos_theta_Kstar_piK_Bd[phs_Bd],
cos_theta_psi_Kstar_Kpi_Bd[phs_Bd], cos_theta_psi_Kstar_piK_Bd[phs_Bd],
cos_theta_B0_Kpi_Bd[phs_Bd], cos_theta_B0_piK_Bd[phs_Bd],
phi_psi_Kpi_Bd[phs_Bd], phi_psi_piK_Bd[phs_Bd];

int signal_a[phs_Bd];

std::complex<float> BWK1410_1_1_minus[phs_Bd], BWK1410_1_2_minus[phs_Bd], BWK1410_1_3_minus[phs_Bd], BWK1430_0_1_minus[phs_Bd], BWK1430_2_1_minus[phs_Bd], BWK1430_2_2_minus[phs_Bd], BWK1430_2_3_minus[phs_Bd], BWK1680_1_1_minus[phs_Bd], BWK1680_1_2_minus[phs_Bd], BWK1680_1_3_minus[phs_Bd], BWK1780_3_1_minus[phs_Bd], BWK1780_3_2_minus[phs_Bd], BWK1780_3_3_minus[phs_Bd], BWK1950_0_1_minus[phs_Bd], BWK1980_2_1_minus[phs_Bd], BWK1980_2_2_minus[phs_Bd], BWK1980_2_3_minus[phs_Bd], BWK2045_4_1_minus[phs_Bd], BWK2045_4_2_minus[phs_Bd], BWK2045_4_3_minus[phs_Bd];

std::complex<float> BWK1410_1_1_plus[phs_Bd], BWK1410_1_2_plus[phs_Bd], BWK1410_1_3_plus[phs_Bd], BWK1430_0_1_plus[phs_Bd], BWK1430_2_1_plus[phs_Bd], BWK1430_2_2_plus[phs_Bd], BWK1430_2_3_plus[phs_Bd], BWK1680_1_1_plus[phs_Bd], BWK1680_1_2_plus[phs_Bd], BWK1680_1_3_plus[phs_Bd], BWK1780_3_1_plus[phs_Bd], BWK1780_3_2_plus[phs_Bd], BWK1780_3_3_plus[phs_Bd], BWK1950_0_1_plus[phs_Bd], BWK1980_2_1_plus[phs_Bd], BWK1980_2_2_plus[phs_Bd], BWK1980_2_3_plus[phs_Bd], BWK2045_4_1_plus[phs_Bd], BWK2045_4_2_plus[phs_Bd], BWK2045_4_3_plus[phs_Bd];

float JpsiK1_mass_BsKK[phs_Bs], JpsiK2_mass_BsKK[phs_Bs], KK_mass_BsKK[phs_Bs], KK_truth_BsKK[phs_Bs], thetaF_a[phs_Bs], phi_mu_F_a[phs_Bs], phi_KF_a[phs_Bs], theta_psi_F_a[phs_Bs], weight_BsKK[phs_Bs], Jpsipi1_mass_BsKK[phs_Bs], Jpsipi2_mass_BsKK[phs_Bs], K1pi2_mass_BsKK[phs_Bs], K2pi1_mass_BsKK[phs_Bs], p1K2_mass_BsKK[phs_Bs], p2K1_mass_BsKK[phs_Bs], Jpsip1_mass_BsKK[phs_Bs], Jpsip2_mass_BsKK[phs_Bs], w_dimuon_a_BsKK[phs_Bs], JpsiKpi_mass_BsKK[phs_Bs], JpsipiK_mass_BsKK[phs_Bs], JpsiKK_mass_BsKK[phs_Bs], Jpsipipi_mass_BsKK[phs_Bs], JpsipK_mass_BsKK[phs_Bs], JpsiKp_mass_BsKK[phs_Bs], cos_theta_Zc_Kpi_Bs[phs_Bs], cos_theta_Zc_piK_Bs[phs_Bs],
phi_K_Kpi_Bs[phs_Bs], phi_K_piK_Bs[phs_Bs],
alpha_mu_Kpi_Bs[phs_Bs], alpha_mu_piK_Bs[phs_Bs],
phi_mu_Kstar_Kpi_Bs[phs_Bs], phi_mu_Kstar_piK_Bs[phs_Bs],
phi_mu_X_Kpi_Bs[phs_Bs], phi_mu_X_piK_Bs[phs_Bs],
cos_theta_psi_X_Kpi_Bs[phs_Bs], cos_theta_psi_X_piK_Bs[phs_Bs],
cos_theta_Kstar_Kpi_Bs[phs_Bs], cos_theta_Kstar_piK_Bs[phs_Bs],
cos_theta_psi_Kstar_Kpi_Bs[phs_Bs], cos_theta_psi_Kstar_piK_Bs[phs_Bs],
cos_theta_B0_Kpi_Bs[phs_Bs], cos_theta_B0_piK_Bs[phs_Bs],
phi_psi_Kpi_Bs[phs_Bs], phi_psi_piK_Bs[phs_Bs];

int signal_a_BsKK[phs_Bs];

float JpsiK1_mass_Lb[phs_Lb], JpsiK2_mass_Lb[phs_Lb], KK_mass_Lb[phs_Lb], pK_truth_Lb[phs_Lb], Jpsip_truth_Lb[phs_Lb], weight_Lb[phs_Lb], Jpsipi1_mass_Lb[phs_Lb], Jpsipi2_mass_Lb[phs_Lb], K1pi2_mass_Lb[phs_Lb], K2pi1_mass_Lb[phs_Lb], p1K2_mass_Lb[phs_Lb], p2K1_mass_Lb[phs_Lb], Jpsip1_mass_Lb[phs_Lb], Jpsip2_mass_Lb[phs_Lb], w_dimuon_a_Lb[phs_Lb], JpsiKpi_mass_Lb[phs_Lb], JpsipiK_mass_Lb[phs_Lb], JpsiKK_mass_Lb[phs_Lb], Jpsipipi_mass_Lb[phs_Lb], JpsipK_mass_Lb[phs_Lb], JpsiKp_mass_Lb[phs_Lb], theta_lambda_b_a[phs_Lb], theta_lambda_b_ls_a[phs_Lb], theta_Pc_a[phs_Lb], theta_Lstar_a[phs_Lb], theta_psi_a[phs_Lb], phi_mu_a[phs_Lb], phi_mu_Pc_a[phs_Lb], phi_mu_Lstar_a[phs_Lb], phi_K_Lstar_a[phs_Lb], phi_psi_a[phs_Lb], phi_Pc_a[phs_Lb], theta_p_a[phs_Lb], alpha_mu_Lb_a[phs_Lb], thete_psi_Lstar_a[phs_Lb], cos_theta_Zc_Kpi_Lb[phs_Lb], cos_theta_Zc_piK_Lb[phs_Lb],
phi_K_Kpi_Lb[phs_Lb], phi_K_piK_Lb[phs_Lb],
alpha_mu_Kpi_Lb[phs_Lb], alpha_mu_piK_Lb[phs_Lb],
phi_mu_Kstar_Kpi_Lb[phs_Lb], phi_mu_Kstar_piK_Lb[phs_Lb],
phi_mu_X_Kpi_Lb[phs_Lb], phi_mu_X_piK_Lb[phs_Lb],
cos_theta_psi_X_Kpi_Lb[phs_Lb], cos_theta_psi_X_piK_Lb[phs_Lb],
cos_theta_Kstar_Kpi_Lb[phs_Lb], cos_theta_Kstar_piK_Lb[phs_Lb],
cos_theta_psi_Kstar_Kpi_Lb[phs_Lb], cos_theta_psi_Kstar_piK_Lb[phs_Lb],
cos_theta_B0_Kpi_Lb[phs_Lb], cos_theta_B0_piK_Lb[phs_Lb],
phi_psi_Kpi_Lb[phs_Lb], phi_psi_piK_Lb[phs_Lb];

int signal_a_Lb[phs_Lb];

int n_fit_step = 0;

TH2D *Jpsipi_2d_data = 0;
TH2D *Jpsipi_2d_LAMBDA0B_to_JPSI_P_K = 0;
TH2D *Jpsipi_2d_BS_to_JPSI_K_K = 0;
TH2D *Jpsipi_2d_BS_to_JPSI_PI_PI = 0;
TH2D *Jpsipi_2d_BD_to_JPSI_K_PI = 0;// new TH2D("Jpsipi_2d_BD_to_JPSI_K_PI", "Jpsipi_2d_BD_to_JPSI_K_PI", 50, 3.3, 5., 50, 3.3, 5.);
TH2D *Jpsipi_2d_BD_to_JPSI_PI_PI = 0;
TH2D *Jpsipi_2d_BS_to_JPSI_K_PI = 0;
TH2D *Jpsipi_2d_BD_to_JPSI_K_K = 0;
TH2D *Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI = 0;

TH2D *Jpsipi_2d_bg = 0;

TH1D *hist_buffer = 0;

//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__||4 track fit||__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//

TH2D *Kpi_piK_2d_data = 0;
TH2D *Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K = 0;
TH2D *Kpi_piK_2d_BS_to_JPSI_PI_PI = 0;
TH2D *Kpi_piK_2d_BD_to_JPSI_PI_PI = 0;
TH2D *Kpi_piK_2d_BD_to_JPSI_K_PI = 0;
TH2D *Kpi_piK_2d_BS_to_JPSI_K_K = 0;
TH2D *Kpi_piK_2d_BD_to_JPSI_K_K = 0;
TH2D *Kpi_piK_2d_BS_to_JPSI_K_PI = 0;
TH2D *Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI = 0;

TH2 *Kpi_piK_2d_full = new TH2D("Kpi_piK_2d_full", "Kpi_piK_2d_full", 40, 4900. * GEV, 5700. * GEV, 62, 4650. * GEV, 5900. * GEV);
TH2 *Kpi_piK_2d_bg = new TH2D("Kpi_piK_2d_bg", "Kpi_piK_2d_bg", 40, 4900. * GEV, 5700. * GEV, 62, 4650. * GEV, 5900. * GEV);

Float_t coef_4_to_s_BD_to_JPSI_K_PI, coef_4_to_c_BD_to_JPSI_K_PI, coef_4_to_clb_BD_to_JPSI_K_PI,
        coef_4_to_s_BS_to_JPSI_K_K, coef_4_to_c_BS_to_JPSI_K_K, coef_4_to_clb_BS_to_JPSI_K_K,
        coef_4_to_s_BD_to_JPSI_PI_PI, coef_4_to_c_BD_to_JPSI_PI_PI, coef_4_to_clb_BD_to_JPSI_PI_PI,
        coef_4_to_s_BS_to_JPSI_PI_PI, coef_4_to_c_BS_to_JPSI_PI_PI, coef_4_to_clb_BS_to_JPSI_PI_PI,
        coef_4_to_s_LAMBDA0B_to_JPSI_P_K, coef_4_to_c_LAMBDA0B_to_JPSI_P_K, coef_4_to_clb_LAMBDA0B_to_JPSI_P_K,
        coef_4_to_s_LAMBDA0B_to_JPSI_P_PI, coef_4_to_c_LAMBDA0B_to_JPSI_P_PI, coef_4_to_clb_LAMBDA0B_to_JPSI_P_PI,
        coef_4_to_s_BD_to_JPSI_K_K, coef_4_to_c_BD_to_JPSI_K_K, coef_4_to_clb_BD_to_JPSI_K_K,
        coef_4_to_s_BS_to_JPSI_K_PI, coef_4_to_c_BS_to_JPSI_K_PI, coef_4_to_clb_BS_to_JPSI_K_PI,
        coef_4_to_s_bg, coef_4_to_c_bg, coef_4_to_clb_bg;

//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//

float n_LAMBDA0B_to_JPSI_P_K,
        n_BD_to_JPSI_K_PI,
        n_BD_to_JPSI_PI_PI,
        n_BS_to_JPSI_PI_PI,
        n_BS_to_JPSI_K_K,
        n_LAMBDA0B_to_JPSI_P_PI,
        n_BD_to_JPSI_K_K,
        n_BS_to_JPSI_K_PI,
        n_comb_bg;
    
Double_t n_global_BD_to_JPSI_K_PI, 
        n_global_BS_to_JPSI_K_K,
        n_global_BS_to_JPSI_K_K_ini,
        n_global_BS_to_JPSI_PI_PI,
        n_global_BD_to_JPSI_PI_PI,
        n_global_LAMBDA0B_to_JPSI_P_K,
        n_global_BD_to_JPSI_K_K,
        n_global_BS_to_JPSI_K_PI,
        n_global_LAMBDA0B_to_JPSI_P_PI;
    
int sa_a[40][62];

Int_t Kpi_piK_2d_nbinsX;
Int_t Kpi_piK_2d_nbinsY;
Int_t KK_1d_nbins;
Int_t pipi_1d_nbins;
Int_t pK_Kp_2d_nbinsX;
Int_t pK_Kp_2d_nbinsY;
Int_t Jpsipi_2d_nbinsX;
Int_t Jpsipi_2d_nbinsY;
Int_t Kpi_2d_nbinsX;
Int_t Kpi_2d_nbinsY;
Int_t JpsiK_2d_nbinsX;
Int_t JpsiK_2d_nbinsY;
Int_t cos_theta_Zc_nbinsX;
Int_t cos_theta_Zc_nbinsY;
Int_t cos_theta_psi_X_nbinsX;
Int_t cos_theta_psi_X_nbinsY;
Int_t phi_mu_X_nbinsX;
Int_t phi_mu_X_nbinsY;
Int_t JpsiK_2d_control_nbinsX;
Int_t JpsiK_2d_control_nbinsY;
Int_t KK_1d_control_nbins;
Int_t Jpsip_2d_controlLb_nbinsX;
Int_t Jpsip_2d_controlLb_nbinsY;
Int_t pK_2d_controlLb_nbinsX;
Int_t pK_2d_controlLb_nbinsY;
Int_t JpsiK_2d_controlLb_nbinsX;
Int_t JpsiK_2d_controlLb_nbinsY;

TH2D *area_hist = 0;

TH2D *Kpi_piK_2d_total_bg = 0;
TH2D *Jpsipi_2d_total_bg = 0;
TH2D *Jpsipi_2d_total_model = 0;
TH2D *Jpsipi_2d_pseudo_data = 0;

void prepare(std::string par_in_name)
{
	TFile *f_in_par = new TFile(par_in_name.c_str(), "open");
    TVectorD *vpar = (TVectorD*)f_in_par->Get("parameters");
    
    for(int j=0; j<214; j++){
	
	if( (j >= 0 && j <= 43)   ){
	  if( j % 2 == 1){  
		
		int n = (int)((*vpar)(j)/2/PI);   (*vpar)(j) =  (*vpar)(j) - n*2*PI;  // force phase to 0..2PI range
		if((*vpar)(j-1) < 0){(*vpar)(j-1) *= -1.0; (*vpar)(j) += PI; }        // force amplitude to be positive, phase gets additional PI
		if(j > 1) (*vpar)(j) = (*vpar)(j) - (*vpar)(1);                       //subtraction of the K1410 phase
		}//ifj%2

	}//if 0..43



	if( (j >= 47 && j <= 78)   ){
	  if( j % 2 == 0){  
		int n = (int)((*vpar)(j)/2/PI);   (*vpar)(j) =  (*vpar)(j) - n*2*PI;  // force phase to 0..2PI range
		if((*vpar)(j-1) < 0){(*vpar)(j-1) *= -1.0; (*vpar)(j) += PI; }  // force amplitude to be positive, phase gets additional PI 
		if(j > 48) (*vpar)(j) = (*vpar)(j) - (*vpar)(48);
		}
	}// if 47..78

    } //for
    (*vpar)(1) = 0.0;
    (*vpar)(48) = 0.0;
    
    Double_t buf_amp, buf_phi;
    
    if ((*vpar).GetNrows() < 222 )
    {
        buf_amp = 0.;
        buf_phi = 0.;
    }
    else 
    {   
        buf_amp = (*vpar)(217);
        buf_phi = (*vpar)(218);
    }
	
	n_global_BD_to_JPSI_K_PI = (*vpar)(83), 
    n_global_BS_to_JPSI_K_K = (*vpar)(84),
    n_global_BS_to_JPSI_K_K_ini = (*vpar)(84),
    n_global_BS_to_JPSI_PI_PI = (*vpar)(81),
    n_global_BD_to_JPSI_PI_PI = (*vpar)(82),
    n_global_LAMBDA0B_to_JPSI_P_K = (*vpar)(80);
    n_global_LAMBDA0B_to_JPSI_P_PI = (*vpar)(214);
    n_global_BD_to_JPSI_K_K = (*vpar)(215);
    n_global_BS_to_JPSI_K_PI = (*vpar)(216);
	
	TFile *f_sas = new TFile("ROOT_files/signal_area_shape.root", "open");
	area_hist = (TH2D*)f_sas->Get("area_hist");
	
	for(int i = 0; i < 40; i++)
    {
        for(int k = 0; k < 62; k++)
        {
            sa_a[i][k] = 0;
            if (area_hist->GetBinContent(i + 1, k + 1) > 0.)
            {
                sa_a[i][k] = 1;
            }
        }
    }
	
	TFile *f_4tr = new TFile("ROOT_files/fitting_Zc1plus.root");
    
    Kpi_piK_2d_data = (TH2D*)f_4tr->Get("Kpi_piK_2d_data");
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K = (TH2D*)f_4tr->Get("Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K");
    Kpi_piK_2d_BS_to_JPSI_PI_PI = (TH2D*)f_4tr->Get("Kpi_piK_2d_BS_to_JPSI_PI_PI");
    Kpi_piK_2d_BD_to_JPSI_PI_PI = (TH2D*)f_4tr->Get("Kpi_piK_2d_BD_to_JPSI_PI_PI");
    Kpi_piK_2d_BD_to_JPSI_K_PI = (TH2D*)f_4tr->Get("Kpi_piK_2d_BD_to_JPSI_K_PI");
    Kpi_piK_2d_BS_to_JPSI_K_K = (TH2D*)f_4tr->Get("Kpi_piK_2d_BS_to_JPSI_K_K");
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI = (TH2D*)f_4tr->Get("Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI");
    Kpi_piK_2d_BD_to_JPSI_K_K = (TH2D*)f_4tr->Get("Kpi_piK_2d_BD_to_JPSI_K_K");
    Kpi_piK_2d_BS_to_JPSI_K_PI = (TH2D*)f_4tr->Get("Kpi_piK_2d_BS_to_JPSI_K_PI");
	
	TFile *f_in = new TFile("ROOT_files/signal_area_Zc1plus.root");
    
    f_in->GetObject("Jpsipi_2d_data", Jpsipi_2d_data);
    f_in->GetObject("Jpsipi_2d_LAMBDA0B_to_JPSI_P_K", Jpsipi_2d_LAMBDA0B_to_JPSI_P_K);
    f_in->GetObject("Jpsipi_2d_BS_to_JPSI_K_K", Jpsipi_2d_BS_to_JPSI_K_K);
    f_in->GetObject("Jpsipi_2d_BS_to_JPSI_PI_PI", Jpsipi_2d_BS_to_JPSI_PI_PI);
    f_in->GetObject("Jpsipi_2d_BD_to_JPSI_K_PI", Jpsipi_2d_BD_to_JPSI_K_PI);
    f_in->GetObject("Jpsipi_2d_BD_to_JPSI_PI_PI", Jpsipi_2d_BD_to_JPSI_PI_PI);
    f_in->GetObject("Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI", Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI);
    f_in->GetObject("Jpsipi_2d_BD_to_JPSI_K_K", Jpsipi_2d_BD_to_JPSI_K_K);
    f_in->GetObject("Jpsipi_2d_BS_to_JPSI_K_PI", Jpsipi_2d_BS_to_JPSI_K_PI);
	
	Jpsipi_2d_BS_to_JPSI_K_K->Reset();
	Kpi_piK_2d_BS_to_JPSI_K_K->Reset();
	
	Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Reset();
	Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->Reset();
	
	Kpi_piK_2d_BD_to_JPSI_K_PI->Reset();
	Jpsipi_2d_BD_to_JPSI_K_PI->Reset();
	
	TFile *f_phs = new TFile("ROOT_files/phase_space.root");
    
    TVectorD *vcoef = (TVectorD*)f_phs->Get("counters");
    
    coef_4_to_s_LAMBDA0B_to_JPSI_P_K = (*vcoef)(0);
    coef_4_to_s_BD_to_JPSI_PI_PI = (*vcoef)(1);
    coef_4_to_s_BS_to_JPSI_PI_PI = (*vcoef)(2);
    coef_4_to_s_LAMBDA0B_to_JPSI_P_PI = (*vcoef)(3);
    coef_4_to_s_BD_to_JPSI_K_K = (*vcoef)(4);
    coef_4_to_s_BS_to_JPSI_K_PI = (*vcoef)(5);
    coef_4_to_c_LAMBDA0B_to_JPSI_P_K = (*vcoef)(6);
    coef_4_to_c_BD_to_JPSI_PI_PI = (*vcoef)(7);
    coef_4_to_c_BS_to_JPSI_PI_PI = (*vcoef)(8);
    coef_4_to_c_LAMBDA0B_to_JPSI_P_PI = (*vcoef)(9);
    coef_4_to_c_BD_to_JPSI_K_K = (*vcoef)(10);
    coef_4_to_c_BS_to_JPSI_K_PI = (*vcoef)(11);
    coef_4_to_clb_LAMBDA0B_to_JPSI_P_K = (*vcoef)(12);
    coef_4_to_clb_BD_to_JPSI_PI_PI = (*vcoef)(13);
    coef_4_to_clb_BS_to_JPSI_PI_PI = (*vcoef)(14);
    coef_4_to_clb_LAMBDA0B_to_JPSI_P_PI = (*vcoef)(15);
    coef_4_to_clb_BD_to_JPSI_K_K = (*vcoef)(16);
    coef_4_to_clb_BS_to_JPSI_K_PI = (*vcoef)(17);
	
	coef_4_to_s_LAMBDA0B_to_JPSI_P_K = 0.;
	coef_4_to_s_BS_to_JPSI_K_K = 0.;
    
    float muon1_px, muon1_py, muon1_pz, muon2_px, muon2_py, muon2_pz, kaon_px, kaon_py, kaon_pz, pion_px, pion_py, pion_pz, 
        jpsipi1, jpsipi2, jpsiK1, jpsiK2, K1pi2, K2pi1, w_dimuon, K1K2, jpsiKpi, jpsipiK, jpsiKK, jpsipipi, jpsipK, jpsiKp, p1K2, p2K1, jpsip1, jpsip2, 
        cos_theta_Zc_Kpi, cos_theta_Zc_piK,
        phi_K_Kpi, phi_K_piK,
        alpha_mu_Kpi, alpha_mu_piK,
        phi_mu_Kstar_Kpi, phi_mu_Kstar_piK,
        phi_mu_X_Kpi, phi_mu_X_piK,
        cos_theta_psi_X_Kpi, cos_theta_psi_X_piK,
        cos_theta_Kstar_Kpi, cos_theta_Kstar_piK,
        cos_theta_psi_Kstar_Kpi, cos_theta_psi_Kstar_piK,
        cos_theta_B0_Kpi, cos_theta_B0_piK,
        phi_psi_Kpi, phi_psi_piK;
    int signal, control, controlLb;
    TTree *tree = (TTree*)f_phs->Get("phs_BD_to_JPSI_K_PI");
    Long64_t nentries = tree->GetEntries();
    
    tree->SetBranchAddress("mu1_px", &muon1_px);
    tree->SetBranchAddress("mu1_py", &muon1_py);
    tree->SetBranchAddress("mu1_pz", &muon1_pz);
    tree->SetBranchAddress("mu2_px", &muon2_px);
    tree->SetBranchAddress("mu2_py", &muon2_py);
    tree->SetBranchAddress("mu2_pz", &muon2_pz);
    tree->SetBranchAddress("K_px", &kaon_px);
    tree->SetBranchAddress("K_py", &kaon_py);
    tree->SetBranchAddress("K_pz", &kaon_pz);
    tree->SetBranchAddress("pi_px", &pion_px);
    tree->SetBranchAddress("pi_py", &pion_py);
    tree->SetBranchAddress("pi_pz", &pion_pz);
    tree->SetBranchAddress("jpsipi1", &jpsipi1);
    tree->SetBranchAddress("jpsipi2", &jpsipi2);
    tree->SetBranchAddress("jpsiK1", &jpsiK1);
    tree->SetBranchAddress("jpsiK2", &jpsiK2);
    tree->SetBranchAddress("K1pi2", &K1pi2);
    tree->SetBranchAddress("K2pi1", &K2pi1);
    tree->SetBranchAddress("w_dimuon", &w_dimuon);
    tree->SetBranchAddress("signal", &signal);
    tree->SetBranchAddress("control", &control);
    tree->SetBranchAddress("K1K2", &K1K2);
    tree->SetBranchAddress("jpsiKpi", &jpsiKpi);
    tree->SetBranchAddress("jpsipiK", &jpsipiK);
    tree->SetBranchAddress("jpsiKK", &jpsiKK);
    tree->SetBranchAddress("jpsipipi", &jpsipipi);
    tree->SetBranchAddress("jpsipK", &jpsipK);
    tree->SetBranchAddress("jpsiKp", &jpsiKp);
    tree->SetBranchAddress("controlLb", &controlLb);
    tree->SetBranchAddress("jpsip1", &jpsip1);
    tree->SetBranchAddress("jpsip2", &jpsip2);
    tree->SetBranchAddress("p1K2", &p1K2);
    tree->SetBranchAddress("p2K1", &p2K1);
    tree->SetBranchAddress("cos_theta_Zc_Kpi", &cos_theta_Zc_Kpi);
    tree->SetBranchAddress("cos_theta_Zc_piK", &cos_theta_Zc_piK);
    tree->SetBranchAddress("phi_K_Kpi", &phi_K_Kpi);
    tree->SetBranchAddress("phi_K_piK", &phi_K_piK);
    tree->SetBranchAddress("alpha_mu_Kpi", &alpha_mu_Kpi);
    tree->SetBranchAddress("alpha_mu_piK", &alpha_mu_piK);
    tree->SetBranchAddress("phi_mu_Kstar_Kpi", &phi_mu_Kstar_Kpi);
    tree->SetBranchAddress("phi_mu_Kstar_piK", &phi_mu_Kstar_piK);
    tree->SetBranchAddress("phi_mu_X_Kpi", &phi_mu_X_Kpi);
    tree->SetBranchAddress("phi_mu_X_piK", &phi_mu_X_piK);
    tree->SetBranchAddress("cos_theta_psi_X_Kpi", &cos_theta_psi_X_Kpi);
    tree->SetBranchAddress("cos_theta_psi_X_piK", &cos_theta_psi_X_piK);
    tree->SetBranchAddress("cos_theta_Kstar_Kpi", &cos_theta_Kstar_Kpi);
    tree->SetBranchAddress("cos_theta_Kstar_piK", &cos_theta_Kstar_piK);
    tree->SetBranchAddress("cos_theta_psi_Kstar_Kpi", &cos_theta_psi_Kstar_Kpi);
    tree->SetBranchAddress("cos_theta_psi_Kstar_piK", &cos_theta_psi_Kstar_piK);
    tree->SetBranchAddress("cos_theta_B0_Kpi", &cos_theta_B0_Kpi);
    tree->SetBranchAddress("cos_theta_B0_piK", &cos_theta_B0_piK);
    tree->SetBranchAddress("phi_psi_Kpi", &phi_psi_Kpi);
    tree->SetBranchAddress("phi_psi_piK", &phi_psi_piK);
    
    TLorentzVector mu1, mu2, pion, kaon, psi, Zc, Bd, Kstar, kaon1, kaon2, Bs, f, proton, Pc, Lstar, Lb;
    
    std::complex<float> ix(0,1);
    
    for(int i = 1; i <= nentries; i++)
    {
        tree->GetEntry(i);
        
        mu1.SetXYZM(muon1_px, muon1_py, muon1_pz, 105.65837);
        mu2.SetXYZM(muon2_px, muon2_py, muon2_pz, 105.65837);
        pion.SetXYZM(pion_px, pion_py, pion_pz, 139.57);
        kaon.SetXYZM(kaon_px, kaon_py, kaon_pz, 493.677);
        
        psi = mu1 + mu2;
        Zc = psi + pion;
        Bd = psi + pion + kaon;
        Kstar = pion + kaon;
        
        Jpsipi1_mass[i-1] = jpsipi1;
        Jpsipi2_mass[i-1] = jpsipi2;
        JpsiK1_mass[i-1] = jpsiK1;
        JpsiK2_mass[i-1] = jpsiK2;
        K1pi2_mass[i-1] = K1pi2;
        K2pi1_mass[i-1] = K2pi1;
        Kpi_truth[i-1] = (kaon + pion).M();
        Jpsipi_truth[i-1] = (mu1 + mu2 + pion).M();
        p_pi[i-1] = (pion.Vect()).Mag();
        p_K[i-1] = (kaon.Vect()).Mag();
        phi_K_a[i-1] = phi_K(Bd, Kstar, kaon, psi);
        alpha_mu_a[i-1] = alpha_mu(mu2, psi, Zc, Bd);
        phi_mu_Kstar_a[i-1] = phi_mu_Kstar(Bd, Kstar, psi, mu2);
        theta_psi_X_a[i-1] = theta_psi_X(Zc, psi, mu2);
        phi_mu_X_a[i-1] = phi_mu_X(Zc, Bd, psi, mu2);
        theta_X_a[i-1] = theta_X(Bd, Zc, psi);
        theta_Kstar_a[i-1] = theta_Kstar( Bd, Kstar, kaon );
        theta_psi_Kstar_a[i-1] = theta_psi_Kstar(Bd, psi, mu2);
        w_dimuon_a[i - 1] = w_dimuon;
        JpsiKpi_mass[i - 1] = jpsiKpi;
        signal_a[i - 1] = signal;
        KK_mass[i - 1] = K1K2;
        JpsiKpi_mass[i - 1] = jpsiKpi;
        JpsipiK_mass[i - 1] = jpsipiK;
        JpsiKK_mass[i - 1] = jpsiKK;
        Jpsipipi_mass[i - 1] = jpsipipi;
        JpsipK_mass[i - 1] = jpsipK;
        JpsiKp_mass[i - 1] = jpsiKp;
        Jpsip1_mass[i - 1] = jpsip1;
        Jpsip2_mass[i - 1] = jpsip2;
        p1K2_mass[i - 1] = p1K2;
        p2K1_mass[i - 1] = p2K1;
        cos_theta_Zc_Kpi_Bd[i - 1] = cos_theta_Zc_Kpi;
        cos_theta_Zc_piK_Bd[i - 1] = cos_theta_Zc_piK;
        phi_K_Kpi_Bd[i - 1] = phi_K_Kpi;
        phi_K_piK_Bd[i - 1] = phi_K_piK;
        alpha_mu_Kpi_Bd[i - 1] = alpha_mu_Kpi;
        alpha_mu_piK_Bd[i - 1] = alpha_mu_piK;
        phi_mu_Kstar_Kpi_Bd[i - 1] = phi_mu_Kstar_Kpi;
        phi_mu_Kstar_piK_Bd[i - 1] = phi_mu_Kstar_piK;
        phi_mu_X_Kpi_Bd[i - 1] = phi_mu_X_Kpi;
        phi_mu_X_piK_Bd[i - 1] = phi_mu_X_piK;
        cos_theta_psi_X_Kpi_Bd[i - 1] = cos_theta_psi_X_Kpi;
        cos_theta_psi_X_piK_Bd[i - 1] = cos_theta_psi_X_piK;
        cos_theta_Kstar_Kpi_Bd[i - 1] = cos_theta_Kstar_Kpi;
        cos_theta_Kstar_piK_Bd[i - 1] = cos_theta_Kstar_piK;
        cos_theta_psi_Kstar_Kpi_Bd[i - 1] = cos_theta_psi_Kstar_Kpi;
        cos_theta_psi_Kstar_piK_Bd[i - 1] = cos_theta_psi_Kstar_piK;
        cos_theta_B0_Kpi_Bd[i - 1] = cos_theta_B0_Kpi;
        cos_theta_B0_piK_Bd[i - 1] = cos_theta_B0_piK;
        phi_psi_Kpi_Bd[i - 1] = phi_psi_Kpi;
        phi_psi_piK_Bd[i - 1] = phi_psi_piK;
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1410_1_1_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.421, 5.27964, 0.236, 0, 1) * ((float)(-1.) *(float)(sqrt(1.0/3.0))* (float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 0.0, 0.0, theta_Kstar_a[i-1]) + (float)(sqrt(1.0/3.0))* (float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 1.0, 0.0, theta_Kstar_a[i-1]) * ((float)cos(phi_mu_Kstar_a[i-1]) + ix*(float)sin(phi_mu_Kstar_a[i-1])) * ((float)cos(phi_K_a[i-1]) + ix*(float)sin(phi_K_a[i-1])) + (float)(sqrt(1.0/3.0))*(float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1])*(float)Wigner_matrix(1.0, -1.0, 0.0, theta_Kstar_a[i-1])*((float)cos(phi_mu_Kstar_a[i-1]) - ix*(float)sin(phi_mu_Kstar_a[i-1]))*((float)cos(phi_K_a[i-1]) - ix*(float)sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1410_1_1_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.421, 5.27964, 0.236, 0, 1) * ((float)(-1.) *(float)(sqrt(1.0/3.0))* (float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 0.0, 0.0, theta_Kstar_a[i-1]) + (float)(sqrt(1.0/3.0))* (float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(1.0/3.0))*(float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1])*(float)Wigner_matrix(1.0, -1.0, 0.0, theta_Kstar_a[i-1])*(cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1]))*(cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1410_1_2_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.421, 5.27964, 0.236, 1, 1) * ((float)(-1.)*(float)(sqrt(1.0/2.0))*(float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(1.0/2.0))*(float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1])*(float)Wigner_matrix(1.0, -1.0, 0.0, theta_Kstar_a[i-1])*(cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1]))*(cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1410_1_2_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.421, 5.27964, 0.236, 1, 1) * ((float)(-1.)*(float)(sqrt(1.0/2.0))*(float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(1.0/2.0))*(float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1])*(float)Wigner_matrix(1.0, -1.0, 0.0, theta_Kstar_a[i-1])*(cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1]))*(cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1410_1_3_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.421, 5.27964, 0.236, 2, 1) * ((float)(sqrt(2.0/3.0))*(float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 0.0, 0.0, theta_Kstar_a[i-1]) + (float)(sqrt(1.0/6.0))*(float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(1.0/6.0))*(float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1])*(float)Wigner_matrix(1.0, -1.0, 0.0, theta_Kstar_a[i-1])*(cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1]))*(cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1410_1_3_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.421, 5.27964, 0.236, 2, 1) * ((float)(sqrt(2.0/3.0))*(float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 0.0, 0.0, theta_Kstar_a[i-1]) + (float)(sqrt(1.0/6.0))*(float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(1.0/6.0))*(float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1])*(float)Wigner_matrix(1.0, -1.0, 0.0, theta_Kstar_a[i-1])*(cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1]))*(cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1430_0_1_minus[i-1] = (float)(-1.) * BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.425, 5.27964, 0.270, 1, 0) * (float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]);
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1430_0_1_plus[i-1] = (float)(-1.) * BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.425, 5.27964, 0.270, 1, 0) * (float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]);
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1430_2_1_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.4324, 5.27964, 0.109, 1, 2) * ((float)(sqrt(2.0/5.0)) * (float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 0.0, 0.0, theta_Kstar_a[i-1]) -(float)(sqrt(3.0/10.0))* (float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) -(float)(sqrt(3.0/10.0))*(float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1430_2_1_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.4324, 5.27964, 0.109, 1, 2) * ((float)(sqrt(2.0/5.0)) * (float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 0.0, 0.0, theta_Kstar_a[i-1]) -(float)(sqrt(3.0/10.0))* (float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) -(float)(sqrt(3.0/10.0))*(float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1430_2_2_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.4324, 5.27964, 0.109, 2, 2) * ((float)(sqrt(1.0/2.0))* (float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(sqrt(1.0/2.0))* (float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1430_2_2_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.4324, 5.27964, 0.109, 2, 2) * ((float)(sqrt(1.0/2.0))* (float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(sqrt(1.0/2.0))* (float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1430_2_3_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.4324, 5.27964, 0.109, 3, 2) * ((float)(-1.) * (float)(sqrt(3.0/5.0))*(float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 0.0, 0.0, theta_Kstar_a[i-1]) - (float)(sqrt(1.0/5.0))*(float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(sqrt(1.0/5.0))*(float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1430_2_3_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.4324, 5.27964, 0.109, 3, 2) * ((float)(-1.) * (float)(sqrt(3.0/5.0))*(float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 0.0, 0.0, theta_Kstar_a[i-1]) - (float)(sqrt(1.0/5.0))*(float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(sqrt(1.0/5.0))*(float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1680_1_1_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.718, 5.27964, 0.322, 0, 1) * ((float)(-1.) *(float)(sqrt(1.0/3.0))* (float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 0.0, 0.0, theta_Kstar_a[i-1]) + (float)(sqrt(1.0/3.0))* (float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(1.0/3.0))*(float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1])*(float)Wigner_matrix(1.0, -1.0, 0.0, theta_Kstar_a[i-1])*(cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1]))*(cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1680_1_1_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.718, 5.27964, 0.322, 0, 1) * ((float)(-1.) *(float)(sqrt(1.0/3.0))* (float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 0.0, 0.0, theta_Kstar_a[i-1]) + (float)(sqrt(1.0/3.0))* (float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(1.0/3.0))*(float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1])*(float)Wigner_matrix(1.0, -1.0, 0.0, theta_Kstar_a[i-1])*(cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1]))*(cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1680_1_2_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.718, 5.27964, 0.322, 1, 1) * ((float)(-1.)*(float)(sqrt(1.0/2.0))*(float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(1.0/2.0))*(float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1])*(float)Wigner_matrix(1.0, -1.0, 0.0, theta_Kstar_a[i-1])*(cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1]))*(cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1680_1_2_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.718, 5.27964, 0.322, 1, 1) * ((float)(-1.)*(float)(sqrt(1.0/2.0))*(float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(1.0/2.0))*(float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1])*(float)Wigner_matrix(1.0, -1.0, 0.0, theta_Kstar_a[i-1])*(cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1]))*(cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1680_1_3_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.718, 5.27964, 0.322, 2, 1) * ((float)(sqrt(2.0/3.0))*(float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 0.0, 0.0, theta_Kstar_a[i-1]) + (float)(sqrt(1.0/6.0))*(float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(1.0/6.0))*(float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1])*(float)Wigner_matrix(1.0, -1.0, 0.0, theta_Kstar_a[i-1])*(cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1]))*(cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1680_1_3_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.718, 5.27964, 0.322, 2, 1) * ((float)(sqrt(2.0/3.0))*(float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 0.0, 0.0, theta_Kstar_a[i-1]) + (float)(sqrt(1.0/6.0))*(float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(1.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(1.0/6.0))*(float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1])*(float)Wigner_matrix(1.0, -1.0, 0.0, theta_Kstar_a[i-1])*(cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1]))*(cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1780_3_1_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.776, 5.27964, 0.159, 2, 3) * ((float)(-1.) * (float)(sqrt(3.0/7.0))*(float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, 0.0, 0.0, theta_Kstar_a[i-1]) + (float)(sqrt(2.0/7.0))* (float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(2.0/7.0))*(float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1780_3_1_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.776, 5.27964, 0.159, 2, 3) * ((float)(-1.) * (float)(sqrt(3.0/7.0))*(float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, 0.0, 0.0, theta_Kstar_a[i-1]) + (float)(sqrt(2.0/7.0))* (float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(2.0/7.0))*(float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1780_3_2_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.776, 5.27964, 0.159, 3, 3) * ((float)(-1.) * (float)(sqrt(1.0/2.0))*(float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(1.0/2.0))*(float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1780_3_2_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.776, 5.27964, 0.159, 3, 3) * ((float)(-1.) * (float)(sqrt(1.0/2.0))*(float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(1.0/2.0))*(float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1780_3_3_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.776, 5.27964, 0.159, 4, 3) * ((float)(sqrt(4.0/7.0))*(float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, 0.0, 0.0, theta_Kstar_a[i-1]) + (float)(sqrt(3.0/14.0))* (float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(3.0/14.0))* (float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1780_3_3_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.776, 5.27964, 0.159, 4, 3) * ((float)(sqrt(4.0/7.0))*(float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, 0.0, 0.0, theta_Kstar_a[i-1]) + (float)(sqrt(3.0/14.0))* (float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) + (float)(sqrt(3.0/14.0))* (float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(3.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1950_0_1_minus[i-1] = (float)(-1.) * BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.945, 5.27964, 0.201, 1, 0) * (float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]);
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1950_0_1_plus[i-1] = (float)(-1.) * BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.945, 5.27964, 0.201, 1, 0) * (float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]);
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1980_2_1_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.974, 5.27964, 0.376, 1, 2) * ((float)(sqrt(2.0/5.0)) * (float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 0.0, 0.0, theta_Kstar_a[i-1]) -(float)(sqrt(3.0/10.0))* (float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) -(float)(sqrt(3.0/10.0))*(float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1980_2_1_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.974, 5.27964, 0.376, 1, 2) * ((float)(sqrt(2.0/5.0)) * (float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 0.0, 0.0, theta_Kstar_a[i-1]) -(float)(sqrt(3.0/10.0))* (float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) -(float)(sqrt(3.0/10.0))*(float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1980_2_2_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.974, 5.27964, 0.376, 2, 2) * ((float)(sqrt(1.0/2.0))* (float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(sqrt(1.0/2.0))* (float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1980_2_2_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.974, 5.27964, 0.376, 2, 2) * ((float)(sqrt(1.0/2.0))* (float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(sqrt(1.0/2.0))* (float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1980_2_3_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.974, 5.27964, 0.376, 3, 2) * ((float)(-1.) * (float)(sqrt(3.0/5.0))*(float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 0.0, 0.0, theta_Kstar_a[i-1]) - (float)(sqrt(1.0/5.0))*(float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(sqrt(1.0/5.0))*(float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK1980_2_3_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 1.974, 5.27964, 0.376, 3, 2) * ((float)(-1.) * (float)(sqrt(3.0/5.0))*(float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 0.0, 0.0, theta_Kstar_a[i-1]) - (float)(sqrt(1.0/5.0))*(float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(sqrt(1.0/5.0))*(float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(2.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK2045_4_1_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 2.045, 5.27964, 0.198, 3, 4) * ((float)(sqrt(4.0/9.0))*(float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, 0.0, 0.0, theta_Kstar_a[i-1]) - (float)(1.0/3.0*sqrt(5.0/2.0))* (float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(1.0/3.0*sqrt(5.0/2.0))* (float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK2045_4_1_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 2.045, 5.27964, 0.198, 3, 4) * ((float)(sqrt(4.0/9.0))*(float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, 0.0, 0.0, theta_Kstar_a[i-1]) - (float)(1.0/3.0*sqrt(5.0/2.0))* (float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(1.0/3.0*sqrt(5.0/2.0))* (float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK2045_4_2_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 2.045, 5.27964, 0.198, 4, 4) * ((float)(sqrt(1.0/2.0)) * (float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(sqrt(1.0/2.0)) * (float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK2045_4_2_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 2.045, 5.27964, 0.198, 4, 4) * ((float)(sqrt(1.0/2.0)) * (float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(sqrt(1.0/2.0)) * (float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK2045_4_3_minus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 2.045, 5.27964, 0.198, 5, 4) * ((float)(-1.) * (float)(sqrt(5.0/9.0))* (float)Wigner_matrix(1.0, 0.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, 0.0, 0.0, theta_Kstar_a[i-1]) - (float)(sqrt(2.0/9.0))* (float)Wigner_matrix(1.0, 1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(sqrt(2.0/9.0))*(float)Wigner_matrix(1.0, -1.0, -1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        BWK2045_4_3_plus[i-1] = BreitWignerKstar(Kpi_truth[i-1] / 1000., 2.045, 5.27964, 0.198, 5, 4) * ((float)(-1.) * (float)(sqrt(5.0/9.0))* (float)Wigner_matrix(1.0, 0.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, 0.0, 0.0, theta_Kstar_a[i-1]) - (float)(sqrt(2.0/9.0))* (float)Wigner_matrix(1.0, 1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, 1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) + ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) + ix*sin(phi_K_a[i-1])) - (float)(sqrt(2.0/9.0))*(float)Wigner_matrix(1.0, -1.0, 1., theta_psi_Kstar_a[i-1]) * (float)Wigner_matrix(4.0, -1.0, 0.0, theta_Kstar_a[i-1]) * (cos(phi_mu_Kstar_a[i-1]) - ix*sin(phi_mu_Kstar_a[i-1])) * (cos(phi_K_a[i-1]) - ix*sin(phi_K_a[i-1])));
        
        //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
        
        weight[i-1] = HamplitudeXKstar_total_accelerated(Jpsipi_truth[i-1] / 1000., Kpi_truth[i-1] / 1000., 5.27964, (*vpar)(45), 4.43, (*vpar)(44), 0.181,
                                    std::complex<float>((*vpar)(40) * cos((*vpar)(41)), (*vpar)(40) * sin((*vpar)(41))), 
                                    std::complex<float>((*vpar)(42) * cos((*vpar)(43)), (*vpar)(42) * sin((*vpar)(43))),
                                    std::complex<float>(0., 0.), 
                                    std::complex<float>(0., 0.),
                                    std::complex<float>((*vpar)(0) * cos((*vpar)(1)), (*vpar)(0) * sin((*vpar)(1))), 
                                    std::complex<float>((*vpar)(16) * cos((*vpar)(17)), (*vpar)(16) * sin((*vpar)(17))), 
                                    std::complex<float>((*vpar)(18) * cos((*vpar)(19)), (*vpar)(18) * sin((*vpar)(19))),
                                    std::complex<float>((*vpar)(2) * cos((*vpar)(3)), (*vpar)(2) * sin((*vpar)(3))), 
                                    std::complex<float>(0., 0.),
                                    std::complex<float>(0., 0.),
                                    std::complex<float>((*vpar)(4) * cos((*vpar)(5)), (*vpar)(4) * sin((*vpar)(5))), 
                                    std::complex<float>((*vpar)(20) * cos((*vpar)(21)), (*vpar)(20) * sin((*vpar)(21))), 
                                    std::complex<float>((*vpar)(22) * cos((*vpar)(23)), (*vpar)(22) * sin((*vpar)(23))),
                                    std::complex<float>((*vpar)(6) * cos((*vpar)(7)), (*vpar)(6) * sin((*vpar)(7))), 
                                    std::complex<float>((*vpar)(24) * cos((*vpar)(25)), (*vpar)(24) * sin((*vpar)(25))), 
                                    std::complex<float>((*vpar)(26) * cos((*vpar)(27)), (*vpar)(26) * sin((*vpar)(27))),
                                    std::complex<float>((*vpar)(8) * cos((*vpar)(9)), (*vpar)(9) * sin((*vpar)(9))), 
                                    std::complex<float>((*vpar)(28) * cos((*vpar)(29)), (*vpar)(28) * sin((*vpar)(29))), 
                                    std::complex<float>((*vpar)(30) * cos((*vpar)(31)), (*vpar)(30) * sin((*vpar)(31))),
                                    std::complex<float>((*vpar)(10) * cos((*vpar)(11)), (*vpar)(10) * sin((*vpar)(11))), 
                                    std::complex<float>(0., 0.), 
                                    std::complex<float>(0., 0.),
                                    std::complex<float>((*vpar)(14) * cos((*vpar)(15)), (*vpar)(14) * sin((*vpar)(15))),
                                    std::complex<float>((*vpar)(32) * cos((*vpar)(33)), (*vpar)(32) * sin((*vpar)(33))),
                                    std::complex<float>((*vpar)(34) * cos((*vpar)(35)), (*vpar)(34) * sin((*vpar)(35))),
                                    std::complex<float>((*vpar)(12) * cos((*vpar)(13)), (*vpar)(12) * sin((*vpar)(13))),
                                    std::complex<float>((*vpar)(36) * cos((*vpar)(37)), (*vpar)(36) * sin((*vpar)(37))),
                                    std::complex<float>((*vpar)(38) * cos((*vpar)(39)), (*vpar)(38) * sin((*vpar)(39))),
                                    std::complex<float>(buf_amp * cos(buf_phi), buf_amp * sin(buf_phi)),
                                    theta_X_a[i-1], theta_Kstar_a[i-1], theta_psi_X_a[i-1], theta_psi_Kstar_a[i-1], phi_mu_X_a[i-1], phi_mu_Kstar_a[i-1], 
                                    phi_K_a[i-1], alpha_mu_a[i-1],
                                    BWK1410_1_1_minus[i-1], BWK1410_1_2_minus[i-1], BWK1410_1_3_minus[i-1], 
                                    BWK1430_0_1_minus[i-1], 
                                    BWK1430_2_1_minus[i-1], BWK1430_2_2_minus[i-1], BWK1430_2_3_minus[i-1], 
                                    BWK1680_1_1_minus[i-1], BWK1680_1_2_minus[i-1], BWK1680_1_3_minus[i-1], 
                                    BWK1780_3_1_minus[i-1], BWK1780_3_2_minus[i-1], BWK1780_3_3_minus[i-1], 
                                    BWK1950_0_1_minus[i-1], 
                                    BWK1980_2_1_minus[i-1], BWK1980_2_2_minus[i-1], BWK1980_2_3_minus[i-1], 
                                    BWK2045_4_1_minus[i-1], BWK2045_4_2_minus[i-1], BWK2045_4_3_minus[i-1],
                                    BWK1410_1_1_plus[i-1], BWK1410_1_2_plus[i-1], BWK1410_1_3_plus[i-1], 
                                    BWK1430_0_1_plus[i-1], 
                                    BWK1430_2_1_plus[i-1], BWK1430_2_2_plus[i-1], BWK1430_2_3_plus[i-1], 
                                    BWK1680_1_1_plus[i-1], BWK1680_1_2_plus[i-1], BWK1680_1_3_plus[i-1], 
                                    BWK1780_3_1_plus[i-1], BWK1780_3_2_plus[i-1], BWK1780_3_3_plus[i-1], 
                                    BWK1950_0_1_plus[i-1], 
                                    BWK1980_2_1_plus[i-1], BWK1980_2_2_plus[i-1], BWK1980_2_3_plus[i-1], 
                                    BWK2045_4_1_plus[i-1], BWK2045_4_2_plus[i-1], BWK2045_4_3_plus[i-1]) * w_dimuon_a[i - 1];
		if (signal_a[i - 1])
        {
            Jpsipi_2d_BD_to_JPSI_K_PI->Fill(Jpsipi1_mass[i - 1], Jpsipi2_mass[i - 1], weight[i - 1]);
            coef_4_to_s_BD_to_JPSI_K_PI += weight[i - 1];
        }
		Kpi_piK_2d_BD_to_JPSI_K_PI->Fill(JpsiKpi_mass[i - 1], JpsipiK_mass[i - 1], weight[i - 1]);
    }
    
    TTree *tree_BsKK = (TTree*)f_phs->Get("phs_BS_to_JPSI_K_K");
    Long64_t nentries_BsKK = tree_BsKK->GetEntries();
    
//     cout << nentries_BsKK << endl;
    
    float kaon1_px, kaon1_py, kaon1_pz, kaon2_px, kaon2_py, kaon2_pz; 
    
    tree_BsKK->SetBranchAddress("mu1_px", &muon1_px);
    tree_BsKK->SetBranchAddress("mu1_py", &muon1_py);
    tree_BsKK->SetBranchAddress("mu1_pz", &muon1_pz);
    tree_BsKK->SetBranchAddress("mu2_px", &muon2_px);
    tree_BsKK->SetBranchAddress("mu2_py", &muon2_py);
    tree_BsKK->SetBranchAddress("mu2_pz", &muon2_pz);
    tree_BsKK->SetBranchAddress("K1_px", &kaon1_px);
    tree_BsKK->SetBranchAddress("K1_py", &kaon1_py);
    tree_BsKK->SetBranchAddress("K1_pz", &kaon1_pz);
    tree_BsKK->SetBranchAddress("K2_px", &kaon2_px);
    tree_BsKK->SetBranchAddress("K2_py", &kaon2_py);
    tree_BsKK->SetBranchAddress("K2_pz", &kaon2_pz);
    tree_BsKK->SetBranchAddress("jpsiK1", &jpsiK1);
    tree_BsKK->SetBranchAddress("jpsiK2", &jpsiK2);
    tree_BsKK->SetBranchAddress("jpsipi1", &jpsipi1);
    tree_BsKK->SetBranchAddress("jpsipi2", &jpsipi2);
    tree_BsKK->SetBranchAddress("K1pi2", &K1pi2);
    tree_BsKK->SetBranchAddress("K2pi1", &K2pi1);
    tree_BsKK->SetBranchAddress("w_dimuon", &w_dimuon);
    tree_BsKK->SetBranchAddress("signal", &signal);
    tree_BsKK->SetBranchAddress("control", &control);
    tree_BsKK->SetBranchAddress("K1K2", &K1K2);
    tree_BsKK->SetBranchAddress("jpsiKpi", &jpsiKpi);
    tree_BsKK->SetBranchAddress("jpsipiK", &jpsipiK);
    tree_BsKK->SetBranchAddress("jpsiKK", &jpsiKK);
    tree_BsKK->SetBranchAddress("jpsipipi", &jpsipipi);
    tree_BsKK->SetBranchAddress("jpsipK", &jpsipK);
    tree_BsKK->SetBranchAddress("jpsiKp", &jpsiKp);
    tree_BsKK->SetBranchAddress("controlLb", &controlLb);
    tree_BsKK->SetBranchAddress("jpsip1", &jpsip1);
    tree_BsKK->SetBranchAddress("jpsip2", &jpsip2);
    tree_BsKK->SetBranchAddress("p1K2", &p1K2);
    tree_BsKK->SetBranchAddress("p2K1", &p2K1);
    tree_BsKK->SetBranchAddress("cos_theta_Zc_Kpi", &cos_theta_Zc_Kpi);
    tree_BsKK->SetBranchAddress("cos_theta_Zc_piK", &cos_theta_Zc_piK);
    tree_BsKK->SetBranchAddress("phi_K_Kpi", &phi_K_Kpi);
    tree_BsKK->SetBranchAddress("phi_K_piK", &phi_K_piK);
    tree_BsKK->SetBranchAddress("alpha_mu_Kpi", &alpha_mu_Kpi);
    tree_BsKK->SetBranchAddress("alpha_mu_piK", &alpha_mu_piK);
    tree_BsKK->SetBranchAddress("phi_mu_Kstar_Kpi", &phi_mu_Kstar_Kpi);
    tree_BsKK->SetBranchAddress("phi_mu_Kstar_piK", &phi_mu_Kstar_piK);
    tree_BsKK->SetBranchAddress("phi_mu_X_Kpi", &phi_mu_X_Kpi);
    tree_BsKK->SetBranchAddress("phi_mu_X_piK", &phi_mu_X_piK);
    tree_BsKK->SetBranchAddress("cos_theta_psi_X_Kpi", &cos_theta_psi_X_Kpi);
    tree_BsKK->SetBranchAddress("cos_theta_psi_X_piK", &cos_theta_psi_X_piK);
    tree_BsKK->SetBranchAddress("cos_theta_Kstar_Kpi", &cos_theta_Kstar_Kpi);
    tree_BsKK->SetBranchAddress("cos_theta_Kstar_piK", &cos_theta_Kstar_piK);
    tree_BsKK->SetBranchAddress("cos_theta_psi_Kstar_Kpi", &cos_theta_psi_Kstar_Kpi);
    tree_BsKK->SetBranchAddress("cos_theta_psi_Kstar_piK", &cos_theta_psi_Kstar_piK);
    tree_BsKK->SetBranchAddress("cos_theta_B0_Kpi", &cos_theta_B0_Kpi);
    tree_BsKK->SetBranchAddress("cos_theta_B0_piK", &cos_theta_B0_piK);
    tree_BsKK->SetBranchAddress("phi_psi_Kpi", &phi_psi_Kpi);
    tree_BsKK->SetBranchAddress("phi_psi_piK", &phi_psi_piK);
    
    for(int i = 1; i <= nentries_BsKK; i++)
    {
        tree_BsKK->GetEntry(i);
        
        mu1.SetXYZM(muon1_px, muon1_py, muon1_pz, 105.65837);
        mu2.SetXYZM(muon2_px, muon2_py, muon2_pz, 105.65837);
        kaon1.SetXYZM(kaon1_px, kaon1_py, kaon1_pz, 493.677);
        kaon2.SetXYZM(kaon2_px, kaon2_py, kaon2_pz, 493.677);
        
        psi = mu1 + mu2;
        f = kaon1 + kaon2;
        Bs = psi + kaon1 + kaon2;
        
        JpsiK1_mass_BsKK[i-1] = jpsiK1;
        JpsiK2_mass_BsKK[i-1] = jpsiK2;
        Jpsipi1_mass_BsKK[i-1] = jpsipi1;
        Jpsipi2_mass_BsKK[i-1] = jpsipi2;
        K1pi2_mass_BsKK[i-1] = K1pi2;
        K2pi1_mass_BsKK[i-1] = K2pi1;
        KK_truth_BsKK[i - 1] = (kaon1 + kaon2).M();
        thetaF_a[i - 1] = theta_Kstar( Bs, f, kaon1 );
        phi_mu_F_a[i - 1] = phi_mu_Kstar(Bs, f, psi, mu2); 
        phi_KF_a[i - 1] = phi_K(Bs, f, kaon1, psi);
        theta_psi_F_a[i - 1] = theta_psi_Kstar(Bs, psi, mu2);
        w_dimuon_a_BsKK[i - 1] = w_dimuon;
        signal_a_BsKK[i - 1] = signal;
        KK_mass_BsKK[i - 1] = K1K2;
        JpsiKpi_mass_BsKK[i - 1] = jpsiKpi;
        JpsipiK_mass_BsKK[i - 1] = jpsipiK;
        JpsiKK_mass_BsKK[i - 1] = jpsiKK;
        Jpsipipi_mass_BsKK[i - 1] = jpsipipi;
        JpsipK_mass_BsKK[i - 1] = jpsipK;
        JpsiKp_mass_BsKK[i - 1] = jpsiKp;
        Jpsip1_mass_BsKK[i - 1] = jpsip1;
        Jpsip2_mass_BsKK[i - 1] = jpsip2;
        p1K2_mass_BsKK[i - 1] = p1K2;
        p2K1_mass_BsKK[i - 1] = p2K1;
        cos_theta_Zc_Kpi_Bs[i - 1] = cos_theta_Zc_Kpi;
        cos_theta_Zc_piK_Bs[i - 1] = cos_theta_Zc_piK;
        phi_K_Kpi_Bs[i - 1] = phi_K_Kpi;
        phi_K_piK_Bs[i - 1] = phi_K_piK;
        alpha_mu_Kpi_Bs[i - 1] = alpha_mu_Kpi;
        alpha_mu_piK_Bs[i - 1] = alpha_mu_piK;
        phi_mu_Kstar_Kpi_Bs[i - 1] = phi_mu_Kstar_Kpi;
        phi_mu_Kstar_piK_Bs[i - 1] = phi_mu_Kstar_piK;
        phi_mu_X_Kpi_Bs[i - 1] = phi_mu_X_Kpi;
        phi_mu_X_piK_Bs[i - 1] = phi_mu_X_piK;
        cos_theta_psi_X_Kpi_Bs[i - 1] = cos_theta_psi_X_Kpi;
        cos_theta_psi_X_piK_Bs[i - 1] = cos_theta_psi_X_piK;
        cos_theta_Kstar_Kpi_Bs[i - 1] = cos_theta_Kstar_Kpi;
        cos_theta_Kstar_piK_Bs[i - 1] = cos_theta_Kstar_piK;
        cos_theta_psi_Kstar_Kpi_Bs[i - 1] = cos_theta_psi_Kstar_Kpi;
        cos_theta_psi_Kstar_piK_Bs[i - 1] = cos_theta_psi_Kstar_piK;
        cos_theta_B0_Kpi_Bs[i - 1] = cos_theta_B0_Kpi;
        cos_theta_B0_piK_Bs[i - 1] = cos_theta_B0_piK;
        phi_psi_Kpi_Bs[i - 1] = phi_psi_Kpi;
        phi_psi_piK_Bs[i - 1] = phi_psi_piK;
        
        weight_BsKK[i - 1] = Hamplitudef_total(0., 0., KK_truth_BsKK[i - 1] / 1000., 5.36688, 0., 0., 0., 0.,
						std::complex<float>(0., 0.), std::complex<float>(0., 0.),
						std::complex<float> ((*vpar)(47) * cos((*vpar)(48)), (*vpar)(47) * sin((*vpar)(48))), 
                        std::complex<float> ((*vpar)(49) * cos((*vpar)(50)), (*vpar)(49) * sin((*vpar)(50))), 
                        std::complex<float> ((*vpar)(51) * cos((*vpar)(52)), (*vpar)(51) * sin((*vpar)(52))),
						std::complex<float> ((*vpar)(53) * cos((*vpar)(54)), (*vpar)(53) * sin((*vpar)(54))), 
                        std::complex<float> ((*vpar)(55) * cos((*vpar)(56)), (*vpar)(55) * sin((*vpar)(56))), 
                        std::complex<float> ((*vpar)(57) * cos((*vpar)(58)), (*vpar)(57) * sin((*vpar)(58))),
						std::complex<float> ((*vpar)(59) * cos((*vpar)(60)), (*vpar)(59) * sin((*vpar)(60))), 
                        std::complex<float> ((*vpar)(61) * cos((*vpar)(62)), (*vpar)(61) * sin((*vpar)(62))), 
                        std::complex<float> ((*vpar)(63) * cos((*vpar)(64)), (*vpar)(63) * sin((*vpar)(64))),
						std::complex<float> ((*vpar)(65) * cos((*vpar)(66)), (*vpar)(65) * sin((*vpar)(66))), 
                        std::complex<float> ((*vpar)(67) * cos((*vpar)(68)), (*vpar)(67) * sin((*vpar)(68))),
                        std::complex<float> ((*vpar)(69) * cos((*vpar)(70)), (*vpar)(69) * sin((*vpar)(70))),
						std::complex<float> ((*vpar)(71) * cos((*vpar)(72)), (*vpar)(71) * sin((*vpar)(72))), 
                        std::complex<float> ((*vpar)(73) * cos((*vpar)(74)), (*vpar)(73) * sin((*vpar)(74))), 
                        std::complex<float> ((*vpar)(75) * cos((*vpar)(76)), (*vpar)(75) * sin((*vpar)(76))),
						std::complex<float> ((*vpar)(77) * cos((*vpar)(78)), (*vpar)(77) * sin((*vpar)(78))),
						0., thetaF_a[i - 1], 0., theta_psi_F_a[i - 1], 0., phi_mu_F_a[i - 1], phi_KF_a[i - 1], 0., 0., 0.) * w_dimuon_a_BsKK[i - 1];
        if (signal_a_BsKK[i - 1])
        {
            Jpsipi_2d_BS_to_JPSI_K_K->Fill(Jpsipi1_mass_BsKK[i - 1], Jpsipi2_mass_BsKK[i - 1], weight_BsKK[i - 1]);
            coef_4_to_s_BS_to_JPSI_K_K += weight_BsKK[i - 1];
        }
		Kpi_piK_2d_BS_to_JPSI_K_K->Fill(JpsiKpi_mass_BsKK[i - 1], JpsipiK_mass_BsKK[i - 1], weight_BsKK[i - 1]);
    }
    
    TFile *f_in1 = new TFile("ROOT_files/signal_area_bg.root");
    
    f_in1->GetObject("Jpsipi_2d_bg", Jpsipi_2d_bg);
    
    TTree *tree_Lb = (TTree*)f_phs->Get("phs_LAMBDA0B_to_JPSI_P_K");
    Long64_t nentries_Lb = tree_Lb->GetEntries();
    
    float proton_px, proton_py, proton_pz; 
    
    tree_Lb->SetBranchAddress("mu1_px", &muon1_px);
    tree_Lb->SetBranchAddress("mu1_py", &muon1_py);
    tree_Lb->SetBranchAddress("mu1_pz", &muon1_pz);
    tree_Lb->SetBranchAddress("mu2_px", &muon2_px);
    tree_Lb->SetBranchAddress("mu2_py", &muon2_py);
    tree_Lb->SetBranchAddress("mu2_pz", &muon2_pz);
    tree_Lb->SetBranchAddress("p_px", &proton_px);
    tree_Lb->SetBranchAddress("p_py", &proton_py);
    tree_Lb->SetBranchAddress("p_pz", &proton_pz);
    tree_Lb->SetBranchAddress("K_px", &kaon_px);
    tree_Lb->SetBranchAddress("K_py", &kaon_py);
    tree_Lb->SetBranchAddress("K_pz", &kaon_pz);
    tree_Lb->SetBranchAddress("jpsiK1", &jpsiK1);
    tree_Lb->SetBranchAddress("jpsiK2", &jpsiK2);
    tree_Lb->SetBranchAddress("jpsipi1", &jpsipi1);
    tree_Lb->SetBranchAddress("jpsipi2", &jpsipi2);
    tree_Lb->SetBranchAddress("K1pi2", &K1pi2);
    tree_Lb->SetBranchAddress("K2pi1", &K2pi1);
    tree_Lb->SetBranchAddress("w_dimuon", &w_dimuon);
    tree_Lb->SetBranchAddress("signal", &signal);
    tree_Lb->SetBranchAddress("control", &control);
    tree_Lb->SetBranchAddress("K1K2", &K1K2);
    tree_Lb->SetBranchAddress("jpsiKpi", &jpsiKpi);
    tree_Lb->SetBranchAddress("jpsipiK", &jpsipiK);
    tree_Lb->SetBranchAddress("jpsiKK", &jpsiKK);
    tree_Lb->SetBranchAddress("jpsipipi", &jpsipipi);
    tree_Lb->SetBranchAddress("jpsipK", &jpsipK);
    tree_Lb->SetBranchAddress("jpsiKp", &jpsiKp);
    tree_Lb->SetBranchAddress("controlLb", &controlLb);
    tree_Lb->SetBranchAddress("jpsip1", &jpsip1);
    tree_Lb->SetBranchAddress("jpsip2", &jpsip2);
    tree_Lb->SetBranchAddress("p1K2", &p1K2);
    tree_Lb->SetBranchAddress("p2K1", &p2K1);
    tree_Lb->SetBranchAddress("cos_theta_Zc_Kpi", &cos_theta_Zc_Kpi);
    tree_Lb->SetBranchAddress("cos_theta_Zc_piK", &cos_theta_Zc_piK);
    tree_Lb->SetBranchAddress("phi_K_Kpi", &phi_K_Kpi);
    tree_Lb->SetBranchAddress("phi_K_piK", &phi_K_piK);
    tree_Lb->SetBranchAddress("alpha_mu_Kpi", &alpha_mu_Kpi);
    tree_Lb->SetBranchAddress("alpha_mu_piK", &alpha_mu_piK);
    tree_Lb->SetBranchAddress("phi_mu_Kstar_Kpi", &phi_mu_Kstar_Kpi);
    tree_Lb->SetBranchAddress("phi_mu_Kstar_piK", &phi_mu_Kstar_piK);
    tree_Lb->SetBranchAddress("phi_mu_X_Kpi", &phi_mu_X_Kpi);
    tree_Lb->SetBranchAddress("phi_mu_X_piK", &phi_mu_X_piK);
    tree_Lb->SetBranchAddress("cos_theta_psi_X_Kpi", &cos_theta_psi_X_Kpi);
    tree_Lb->SetBranchAddress("cos_theta_psi_X_piK", &cos_theta_psi_X_piK);
    tree_Lb->SetBranchAddress("cos_theta_Kstar_Kpi", &cos_theta_Kstar_Kpi);
    tree_Lb->SetBranchAddress("cos_theta_Kstar_piK", &cos_theta_Kstar_piK);
    tree_Lb->SetBranchAddress("cos_theta_psi_Kstar_Kpi", &cos_theta_psi_Kstar_Kpi);
    tree_Lb->SetBranchAddress("cos_theta_psi_Kstar_piK", &cos_theta_psi_Kstar_piK);
    tree_Lb->SetBranchAddress("cos_theta_B0_Kpi", &cos_theta_B0_Kpi);
    tree_Lb->SetBranchAddress("cos_theta_B0_piK", &cos_theta_B0_piK);
    tree_Lb->SetBranchAddress("phi_psi_Kpi", &phi_psi_Kpi);
    tree_Lb->SetBranchAddress("phi_psi_piK", &phi_psi_piK);
    
    for(int i = 1; i <= nentries_Lb; i++)
    {
        tree_Lb->GetEntry(i);
        mu1.SetXYZM(muon1_px, muon1_py, muon1_pz, 105.65837);
        mu2.SetXYZM(muon2_px, muon2_py, muon2_pz, 105.65837);
        proton.SetXYZM(proton_px, proton_py, proton_pz, 938.272081);
        kaon.SetXYZM(kaon_px, kaon_py, kaon_pz, 493.677);
        
        psi = mu1 + mu2;
        Lstar = proton + kaon;
        Lb = psi + proton + kaon;
        Pc = psi + proton;
        
        JpsiK1_mass_Lb[i-1] = jpsiK1;
        JpsiK2_mass_Lb[i-1] = jpsiK2;
        Jpsipi1_mass_Lb[i-1] = jpsipi1;
        Jpsipi2_mass_Lb[i-1] = jpsipi2;
        K1pi2_mass_Lb[i-1] = K1pi2;
        K2pi1_mass_Lb[i-1] = K2pi1;
        pK_truth_Lb[i - 1] = (proton + kaon).M();
        Jpsip_truth_Lb[i - 1] = (psi + proton).M();
        w_dimuon_a_Lb[i - 1] = w_dimuon;
        signal_a_Lb[i - 1] = signal;
        KK_mass_Lb[i - 1] = K1K2;
        JpsiKpi_mass_Lb[i - 1] = jpsiKpi;
        JpsipiK_mass_Lb[i - 1] = jpsipiK;
        JpsiKK_mass_Lb[i - 1] = jpsiKK;
        Jpsipipi_mass_Lb[i - 1] = jpsipipi;
        JpsipK_mass_Lb[i - 1] = jpsipK;
        JpsiKp_mass_Lb[i - 1] = jpsiKp;
        
        /*psi = kaon; // for test
        kaon = proton;
        proton = psi;
        
        psi = mu1 + mu2;
        Lstar = proton + kaon;
        Lb = psi + proton + kaon;
        Pc = psi + proton;*/
        
        theta_lambda_b_a[i - 1] = theta_lambda_b(Lb, Pc);
        theta_lambda_b_ls_a[i - 1] = theta_lambda_b_ls(Lb, proton, kaon); 
        theta_Pc_a[i - 1] = theta_Pc(Lb, Pc, psi); 
        theta_Lstar_a[i - 1] = theta_Lstar(Lb, Lstar, kaon); 
        theta_psi_a[i - 1] = theta_psi(Pc, psi, mu2); 
        phi_mu_a[i - 1] = phi_mu(Pc, kaon, psi, proton, mu2); 
        phi_mu_Pc_a[i - 1] = phi_mu_Pc(Pc, kaon, psi, mu2); 
        phi_mu_Lstar_a[i - 1] = phi_mu_Lstar(Lb, Lstar, psi, mu2); 
        phi_K_Lstar_a[i - 1] = phi_K_Lstar(Lb, Lstar, kaon, psi); 
        phi_psi_a[i - 1] = phi_psi(Lb, Pc, psi, kaon); 
        phi_Pc_a[i - 1] = phi_Pc(Lb, Pc, Lstar); 
        theta_p_a[i - 1] = theta_p(psi, proton, kaon);
        alpha_mu_Lb_a[i - 1] = alpha_mu(mu2, psi, Pc, Lb);
        thete_psi_Lstar_a[i - 1] = theta_psi(Lstar, psi, mu2);
        Jpsip1_mass_Lb[i - 1] = jpsip1;
        Jpsip2_mass_Lb[i - 1] = jpsip2;
        p1K2_mass_Lb[i - 1] = p1K2;
        p2K1_mass_Lb[i - 1] = p2K1;
        cos_theta_Zc_Kpi_Lb[i - 1] = cos_theta_Zc_Kpi;
        cos_theta_Zc_piK_Lb[i - 1] = cos_theta_Zc_piK;
        phi_K_Kpi_Lb[i - 1] = phi_K_Kpi;
        phi_K_piK_Lb[i - 1] = phi_K_piK;
        alpha_mu_Kpi_Lb[i - 1] = alpha_mu_Kpi;
        alpha_mu_piK_Lb[i - 1] = alpha_mu_piK;
        phi_mu_Kstar_Kpi_Lb[i - 1] = phi_mu_Kstar_Kpi;
        phi_mu_Kstar_piK_Lb[i - 1] = phi_mu_Kstar_piK;
        phi_mu_X_Kpi_Lb[i - 1] = phi_mu_X_Kpi;
        phi_mu_X_piK_Lb[i - 1] = phi_mu_X_piK;
        cos_theta_psi_X_Kpi_Lb[i - 1] = cos_theta_psi_X_Kpi;
        cos_theta_psi_X_piK_Lb[i - 1] = cos_theta_psi_X_piK;
        cos_theta_Kstar_Kpi_Lb[i - 1] = cos_theta_Kstar_Kpi;
        cos_theta_Kstar_piK_Lb[i - 1] = cos_theta_Kstar_piK;
        cos_theta_psi_Kstar_Kpi_Lb[i - 1] = cos_theta_psi_Kstar_Kpi;
        cos_theta_psi_Kstar_piK_Lb[i - 1] = cos_theta_psi_Kstar_piK;
        cos_theta_B0_Kpi_Lb[i - 1] = cos_theta_B0_Kpi;
        cos_theta_B0_piK_Lb[i - 1] = cos_theta_B0_piK;
        phi_psi_Kpi_Lb[i - 1] = phi_psi_Kpi;
        phi_psi_piK_Lb[i - 1] = phi_psi_piK;
        
        weight_Lb[i - 1] = HamplitudePcLstar_total(Jpsip_truth_Lb[i - 1] / 1000., pK_truth_Lb[i - 1] / 1000., 5.6196, (*vpar)(209), (*vpar)(210), 0., 0., (*vpar)(211), (*vpar)(212), 0., 0.,
						std::complex<float> ((*vpar)(189) * cos((*vpar)(190)), (*vpar)(189) * cos((*vpar)(190))), 
                        std::complex<float> ((*vpar)(191) * cos((*vpar)(192)), (*vpar)(191) * cos((*vpar)(192))), 
                        std::complex<float> ((*vpar)(193) * cos((*vpar)(194)), (*vpar)(193) * cos((*vpar)(194))), 
                        std::complex<float> ((*vpar)(195) * cos((*vpar)(196)), (*vpar)(195) * cos((*vpar)(196))), 
                        std::complex<float> ((*vpar)(197) * cos((*vpar)(198)), (*vpar)(197) * cos((*vpar)(198))),
						std::complex<float> ((*vpar)(199) * cos((*vpar)(200)), (*vpar)(199) * cos((*vpar)(200))), 
                        std::complex<float> ((*vpar)(201) * cos((*vpar)(202)), (*vpar)(201) * cos((*vpar)(202))), 
                        std::complex<float> ((*vpar)(203) * cos((*vpar)(204)), (*vpar)(203) * cos((*vpar)(204))), 
                        std::complex<float> ((*vpar)(205) * cos((*vpar)(206)), (*vpar)(205) * cos((*vpar)(206))), 
                        std::complex<float> ((*vpar)(207) * cos((*vpar)(208)), (*vpar)(207) * cos((*vpar)(208))),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.), std::complex<float> (0., 0.),
						std::complex<float> ((*vpar)(121) * cos((*vpar)(122)), (*vpar)(121) * cos((*vpar)(122))), 
                        std::complex<float> ((*vpar)(123) * cos((*vpar)(124)), (*vpar)(123) * cos((*vpar)(124))), 
                        std::complex<float> ((*vpar)(125) * cos((*vpar)(126)), (*vpar)(125) * cos((*vpar)(126))), 
                        std::complex<float> ((*vpar)(127) * cos((*vpar)(128)), (*vpar)(127) * cos((*vpar)(128))),
						std::complex<float> ((*vpar)(129) * cos((*vpar)(130)), (*vpar)(129) * cos((*vpar)(130))), 
                        std::complex<float> ((*vpar)(131) * cos((*vpar)(132)), (*vpar)(131) * cos((*vpar)(132))), 
                        std::complex<float> ((*vpar)(133) * cos((*vpar)(134)), (*vpar)(133) * cos((*vpar)(134))),
						std::complex<float> ((*vpar)(135) * cos((*vpar)(136)), (*vpar)(135) * cos((*vpar)(136))), 
                        std::complex<float> ((*vpar)(137) * cos((*vpar)(138)), (*vpar)(137) * cos((*vpar)(138))), 
                        std::complex<float> ((*vpar)(139) * cos((*vpar)(140)), (*vpar)(139) * cos((*vpar)(140))),
						std::complex<float> ((*vpar)(141) * cos((*vpar)(142)), (*vpar)(141) * cos((*vpar)(142))), 
                        std::complex<float> ((*vpar)(143) * cos((*vpar)(144)), (*vpar)(143) * cos((*vpar)(144))), 
                        std::complex<float> ((*vpar)(145) * cos((*vpar)(146)), (*vpar)(145) * cos((*vpar)(146))),
						std::complex<float> ((*vpar)(147) * cos((*vpar)(148)), (*vpar)(147) * cos((*vpar)(148))), 
                        std::complex<float> ((*vpar)(149) * cos((*vpar)(150)), (*vpar)(149) * cos((*vpar)(150))), 
                        std::complex<float> ((*vpar)(151) * cos((*vpar)(152)), (*vpar)(151) * cos((*vpar)(152))),
						std::complex<float> ((*vpar)(153) * cos((*vpar)(154)), (*vpar)(153) * cos((*vpar)(154))), 
                        std::complex<float> ((*vpar)(155) * cos((*vpar)(156)), (*vpar)(155) * cos((*vpar)(156))), 
                        std::complex<float> ((*vpar)(157) * cos((*vpar)(158)), (*vpar)(157) * cos((*vpar)(158))),
						std::complex<float> ((*vpar)(159) * cos((*vpar)(160)), (*vpar)(159) * cos((*vpar)(160))), 
                        std::complex<float> ((*vpar)(161) * cos((*vpar)(162)), (*vpar)(161) * cos((*vpar)(162))),
                        std::complex<float> ((*vpar)(163) * cos((*vpar)(164)), (*vpar)(163) * cos((*vpar)(164))),
						std::complex<float> ((*vpar)(165) * cos((*vpar)(166)), (*vpar)(165) * cos((*vpar)(166))), 
                        std::complex<float> ((*vpar)(167) * cos((*vpar)(168)), (*vpar)(167) * cos((*vpar)(168))), 
                        std::complex<float> ((*vpar)(169) * cos((*vpar)(170)), (*vpar)(169) * cos((*vpar)(170))),
						std::complex<float> ((*vpar)(171) * cos((*vpar)(172)), (*vpar)(171) * cos((*vpar)(172))), 
                        std::complex<float> ((*vpar)(173) * cos((*vpar)(174)), (*vpar)(173) * cos((*vpar)(174))), 
                        std::complex<float> ((*vpar)(175) * cos((*vpar)(176)), (*vpar)(175) * cos((*vpar)(176))),
						std::complex<float> ((*vpar)(177) * cos((*vpar)(178)), (*vpar)(177) * cos((*vpar)(178))), 
                        std::complex<float> ((*vpar)(179) * cos((*vpar)(180)), (*vpar)(179) * cos((*vpar)(180))), 
                        std::complex<float> ((*vpar)(181) * cos((*vpar)(182)), (*vpar)(181) * cos((*vpar)(182))),
						std::complex<float> ((*vpar)(183) * cos((*vpar)(184)), (*vpar)(183) * cos((*vpar)(184))), 
                        std::complex<float> ((*vpar)(185) * cos((*vpar)(186)), (*vpar)(185) * cos((*vpar)(186))), 
                        std::complex<float> ((*vpar)(187) * cos((*vpar)(188)), (*vpar)(187) * cos((*vpar)(188))),
						theta_Lstar_a[i - 1], theta_lambda_b_ls_a[i - 1], thete_psi_Lstar_a[i - 1], phi_mu_Lstar_a[i - 1], phi_K_Lstar_a[i - 1],
						theta_Pc_a[i - 1], theta_lambda_b_a[i - 1], theta_psi_a[i - 1], phi_mu_Pc_a[i - 1], phi_Pc_a[i - 1], phi_psi_a[i - 1], 
						alpha_mu_Lb_a[i - 1], theta_p_a[i - 1]) * w_dimuon_a_Lb[i - 1]; // for_syst
        if (signal_a_Lb[i - 1])
        {
            Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->Fill(Jpsipi1_mass_Lb[i - 1], Jpsipi2_mass_Lb[i - 1], weight_Lb[i - 1]);
            coef_4_to_s_LAMBDA0B_to_JPSI_P_K += weight_Lb[i - 1];
        } 
		Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Fill(JpsiKpi_mass_Lb[i - 1], JpsipiK_mass_Lb[i - 1], weight_Lb[i - 1]);
        //cout << JpsiKpi_mass_Lb[i] << "\t" << JpsipiK_mass_Lb[i] << "\t" << weight_Lb[i - 1] << endl; 
    }
	
    ov_uf_bins_Clear(Kpi_piK_2d_data);
    ov_uf_bins_Clear(Kpi_piK_2d_BD_to_JPSI_K_PI);
    ov_uf_bins_Clear(Kpi_piK_2d_BD_to_JPSI_PI_PI);
    ov_uf_bins_Clear(Kpi_piK_2d_BS_to_JPSI_K_K);
    ov_uf_bins_Clear(Kpi_piK_2d_BS_to_JPSI_PI_PI);
    ov_uf_bins_Clear(Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K);
    ov_uf_bins_Clear(Kpi_piK_2d_bg);
    ov_uf_bins_Clear(Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI);
    ov_uf_bins_Clear(Kpi_piK_2d_BD_to_JPSI_K_K);
    ov_uf_bins_Clear(Kpi_piK_2d_BS_to_JPSI_K_PI);
    
	ov_uf_bins_Clear(Jpsipi_2d_data);
    ov_uf_bins_Clear(Jpsipi_2d_BD_to_JPSI_K_PI);
    ov_uf_bins_Clear(Jpsipi_2d_BD_to_JPSI_PI_PI);
    ov_uf_bins_Clear(Jpsipi_2d_BS_to_JPSI_K_K);
    ov_uf_bins_Clear(Jpsipi_2d_BS_to_JPSI_PI_PI);
    ov_uf_bins_Clear(Jpsipi_2d_LAMBDA0B_to_JPSI_P_K);
    ov_uf_bins_Clear(Jpsipi_2d_bg);
    ov_uf_bins_Clear(Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI);
    ov_uf_bins_Clear(Jpsipi_2d_BD_to_JPSI_K_K);
    ov_uf_bins_Clear(Jpsipi_2d_BS_to_JPSI_K_PI);
    
    Kpi_piK_2d_nbinsX = Kpi_piK_2d_BD_to_JPSI_K_PI->ProjectionX()->GetNbinsX();
    Kpi_piK_2d_nbinsY = Kpi_piK_2d_BD_to_JPSI_K_PI->ProjectionY()->GetNbinsX();
    
    Jpsipi_2d_nbinsX = Jpsipi_2d_BD_to_JPSI_K_PI->ProjectionX()->GetNbinsX();
    Jpsipi_2d_nbinsY = Jpsipi_2d_BD_to_JPSI_K_PI->ProjectionY()->GetNbinsX();
    
	Double_t st = 1. /sqrt(2.);
	
	coef_4_to_s_BS_to_JPSI_K_K = coef_4_to_s_BS_to_JPSI_K_K / Kpi_piK_2d_BS_to_JPSI_K_K->Integral();
    
    coef_4_to_s_LAMBDA0B_to_JPSI_P_K = coef_4_to_s_LAMBDA0B_to_JPSI_P_K / Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Integral();
	
	coef_4_to_s_BD_to_JPSI_K_PI = coef_4_to_s_BD_to_JPSI_K_PI / Kpi_piK_2d_BD_to_JPSI_K_PI->Integral();
	
    n_BS_to_JPSI_K_K = coef_4_to_s_BS_to_JPSI_K_K * n_global_BS_to_JPSI_K_K;
    n_BS_to_JPSI_PI_PI = coef_4_to_s_BS_to_JPSI_PI_PI * n_global_BS_to_JPSI_PI_PI;
    n_BD_to_JPSI_PI_PI = coef_4_to_s_BD_to_JPSI_PI_PI * n_global_BD_to_JPSI_PI_PI;
    n_LAMBDA0B_to_JPSI_P_K = coef_4_to_s_LAMBDA0B_to_JPSI_P_K * n_global_LAMBDA0B_to_JPSI_P_K;
    n_LAMBDA0B_to_JPSI_P_PI = coef_4_to_s_LAMBDA0B_to_JPSI_P_PI * n_global_LAMBDA0B_to_JPSI_P_PI;
    n_BD_to_JPSI_K_K = coef_4_to_s_BD_to_JPSI_K_K * n_global_BD_to_JPSI_K_K;
    n_BS_to_JPSI_K_PI = coef_4_to_s_BS_to_JPSI_K_PI * n_global_BS_to_JPSI_K_PI;
	n_BD_to_JPSI_K_PI = coef_4_to_s_BD_to_JPSI_K_PI * n_global_BD_to_JPSI_K_PI;
	
	n_comb_bg = 0.;
	
	for(int i = 1; i <= Kpi_piK_2d_bg->ProjectionX()->GetNbinsX(); i++)
    {
        for(int j = 1; j <= Kpi_piK_2d_bg->ProjectionY()->GetNbinsX(); j++)
        {
            
            double xx0 = Kpi_piK_2d_data->ProjectionX()->GetBinCenter(i); 
            double yy0 = Kpi_piK_2d_data->ProjectionY()->GetBinCenter(j);
            double x = st * xx0 + st * yy0 - 6.6;
            double y = -st * xx0 + st * yy0;
            Kpi_piK_2d_bg->SetBinContent(i, j, exp((*vpar)(85) + x * (*vpar)(86) + pow(x, 2.) * (*vpar)(87) + pow(x, 3.) * (*vpar)(88)) * exp(-1. * pow( (y + (*vpar)(89)) / ((*vpar)(90) + x * (*vpar)(91) + pow(x, 2.) * (*vpar)(92)), 2.)));            
            if (sa_a[i-1][j-1] == 1)
            {
                n_comb_bg += Kpi_piK_2d_bg->GetBinContent(i, j);
            }
        }
    }
	Double_t Kpi_piK_norm_bg = (*vpar)(213) / Kpi_piK_2d_bg->Integral();
    n_comb_bg = n_comb_bg * Kpi_piK_norm_bg;
    Kpi_piK_2d_bg->Scale(Kpi_piK_norm_bg);
	
	Jpsipi_2d_BD_to_JPSI_K_PI->Scale(1. / Jpsipi_2d_BD_to_JPSI_K_PI->Integral(1, Jpsipi_2d_nbinsX, 1, Jpsipi_2d_nbinsY) * n_BD_to_JPSI_K_PI);
	Jpsipi_2d_BS_to_JPSI_PI_PI->Scale(1. / Jpsipi_2d_BS_to_JPSI_PI_PI->Integral(1, Jpsipi_2d_nbinsX, 1, Jpsipi_2d_nbinsY) * n_BS_to_JPSI_PI_PI);
	Jpsipi_2d_BD_to_JPSI_PI_PI->Scale(1. / Jpsipi_2d_BD_to_JPSI_PI_PI->Integral() * n_BD_to_JPSI_PI_PI);
	Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->Scale(1. / Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->Integral(1, Jpsipi_2d_nbinsX, 1, Jpsipi_2d_nbinsY) * n_LAMBDA0B_to_JPSI_P_K);
	Jpsipi_2d_BS_to_JPSI_K_K->Scale(1. / Jpsipi_2d_BS_to_JPSI_K_K->Integral(1, Jpsipi_2d_nbinsX, 1, Jpsipi_2d_nbinsY) * n_BS_to_JPSI_K_K);
	Jpsipi_2d_BD_to_JPSI_K_K->Scale(1. / Jpsipi_2d_BD_to_JPSI_K_K->Integral(1, Jpsipi_2d_nbinsX, 1, Jpsipi_2d_nbinsY) * n_BD_to_JPSI_K_K);
	Jpsipi_2d_BS_to_JPSI_K_PI->Scale(1. / Jpsipi_2d_BS_to_JPSI_K_PI->Integral(1, Jpsipi_2d_nbinsX, 1, Jpsipi_2d_nbinsY) * n_BS_to_JPSI_K_PI);
	Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->Scale(1. / Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->Integral(1, Jpsipi_2d_nbinsX, 1, Jpsipi_2d_nbinsY) * n_LAMBDA0B_to_JPSI_P_PI);
	Jpsipi_2d_bg->Scale(1. / Jpsipi_2d_bg->Integral(1, Jpsipi_2d_nbinsX, 1, Jpsipi_2d_nbinsY) * n_comb_bg);
	
	TCanvas *c_Min2_X_KpipiK = new TCanvas("c_Min2_X_KpipiK","fit #2_X",200,10,700,500);
	
	Kpi_piK_2d_bg->SetFillColor(42);
    Kpi_piK_2d_BS_to_JPSI_K_K->SetFillColor(30);
    Kpi_piK_2d_BD_to_JPSI_K_PI->SetFillColor(7);
    Kpi_piK_2d_BD_to_JPSI_PI_PI->SetFillColor(8);
    Kpi_piK_2d_BS_to_JPSI_PI_PI->SetFillColor(46);
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->SetFillColor(95);
    Kpi_piK_2d_BD_to_JPSI_K_K->SetFillColor(12);
    Kpi_piK_2d_BS_to_JPSI_K_PI->SetFillColor(6);
    
    Kpi_piK_2d_data->SetMarkerStyle(kFullCircle);
    Kpi_piK_2d_data->SetMarkerSize(1.5);

    Kpi_piK_2d_BS_to_JPSI_K_K->Scale(n_global_BS_to_JPSI_K_K / Kpi_piK_2d_BS_to_JPSI_K_K->Integral());
    Kpi_piK_2d_BD_to_JPSI_K_PI->Scale(n_global_BD_to_JPSI_K_PI / Kpi_piK_2d_BD_to_JPSI_K_PI->Integral());
    Kpi_piK_2d_BD_to_JPSI_PI_PI->Scale(n_global_BD_to_JPSI_PI_PI / Kpi_piK_2d_BD_to_JPSI_PI_PI->Integral());
    Kpi_piK_2d_BS_to_JPSI_PI_PI->Scale(n_global_BS_to_JPSI_PI_PI / Kpi_piK_2d_BS_to_JPSI_PI_PI->Integral());
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Scale(n_global_LAMBDA0B_to_JPSI_P_K / Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Integral());
    Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->Scale(n_global_LAMBDA0B_to_JPSI_P_PI / Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->Integral());
    Kpi_piK_2d_BD_to_JPSI_K_K->Scale(n_global_BD_to_JPSI_K_K / Kpi_piK_2d_BD_to_JPSI_K_K->Integral());
    Kpi_piK_2d_BS_to_JPSI_K_PI->Scale(n_global_BS_to_JPSI_K_PI / Kpi_piK_2d_BS_to_JPSI_K_PI->Integral());
    
    THStack hs_Kpi("","");
    hs_Kpi.Add(Kpi_piK_2d_bg->ProjectionX());
    hs_Kpi.Add(Kpi_piK_2d_BS_to_JPSI_PI_PI->ProjectionX());
    hs_Kpi.Add(Kpi_piK_2d_BD_to_JPSI_PI_PI->ProjectionX());
    hs_Kpi.Add(Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->ProjectionX());
    hs_Kpi.Add(Kpi_piK_2d_BS_to_JPSI_K_K->ProjectionX());
    hs_Kpi.Add(Kpi_piK_2d_BD_to_JPSI_K_PI->ProjectionX());
    if (RARE_DECAYS)
    {   
        hs_Kpi.Add(Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionX());
        hs_Kpi.Add(Kpi_piK_2d_BD_to_JPSI_K_K->ProjectionX());
        hs_Kpi.Add(Kpi_piK_2d_BS_to_JPSI_K_PI->ProjectionX());
    }

    TLegend *l_Kpi = new TLegend(.60,.60,.90,.90);
    l_Kpi->AddEntry(Kpi_piK_2d_data, "data");
    l_Kpi->AddEntry(Kpi_piK_2d_BD_to_JPSI_K_PI, "B_{d} -> J/#psi K #pi");
    l_Kpi->AddEntry(Kpi_piK_2d_BS_to_JPSI_K_K, "B_{s} -> J/#psi K K");
    l_Kpi->AddEntry(Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K, "#Lambda_{b} -> J/#psi p K");
    l_Kpi->AddEntry(Kpi_piK_2d_BD_to_JPSI_PI_PI, "B_{d} -> J/#psi #pi #pi");
    l_Kpi->AddEntry(Kpi_piK_2d_BS_to_JPSI_PI_PI, "B_{s} -> J/#psi #pi #pi");
    if (RARE_DECAYS)
    {   
        l_Kpi->AddEntry(Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI, "#Lambda_{b} -> J/#psi p #pi");
        l_Kpi->AddEntry(Kpi_piK_2d_BD_to_JPSI_K_K, "B_{d} -> J/#psi K K");
        l_Kpi->AddEntry(Kpi_piK_2d_BS_to_JPSI_K_PI, "B_{s} -> J/#psi K #pi");
    }
    l_Kpi->AddEntry(Kpi_piK_2d_bg, "comb background");
    
    hist_buffer = (TH1D*)Kpi_piK_2d_data->ProjectionX()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("m(J/#psi h_{1}=K h_{2}=#pi), GeV");
    hist_buffer->GetYaxis()->SetTitleOffset(1.35);
    hist_buffer->GetYaxis()->SetTitle("Events / 20 MeV");
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    hist_buffer->Draw("");
    hs_Kpi.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    l_Kpi->Draw("same");
    
    c_Min2_X_KpipiK->SaveAs("Kpi_piK_prepare_test.png");
	
	TCanvas * c_JpsipiX = new TCanvas("jpsipi_mass_ProjectionX");
	
	Jpsipi_2d_bg->SetLineColor(1);
    Jpsipi_2d_bg->SetFillColor(42);
    Jpsipi_2d_BS_to_JPSI_K_K->SetFillColor(30);
    Jpsipi_2d_BD_to_JPSI_K_PI->SetFillColor(7);
    Jpsipi_2d_BD_to_JPSI_PI_PI->SetFillColor(8);
    Jpsipi_2d_BS_to_JPSI_PI_PI->SetFillColor(46);
    Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->SetFillColor(3);
    Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->SetFillColor(95);
    Jpsipi_2d_BD_to_JPSI_K_K->SetFillColor(12);
    Jpsipi_2d_BS_to_JPSI_K_PI->SetFillColor(6);
    Jpsipi_2d_data->SetMarkerStyle(kFullCircle);
    Jpsipi_2d_data->SetMarkerSize(0.7);
	
	THStack hs_JpsipiX("hs_JpsipiX","Jpsipi_signalX");
    
    hs_JpsipiX.Add(Jpsipi_2d_bg->ProjectionX());
    hs_JpsipiX.Add(Jpsipi_2d_BD_to_JPSI_PI_PI->ProjectionX());
    hs_JpsipiX.Add(Jpsipi_2d_BS_to_JPSI_PI_PI->ProjectionX());
    hs_JpsipiX.Add(Jpsipi_2d_BS_to_JPSI_K_K->ProjectionX());
    hs_JpsipiX.Add(Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->ProjectionX());
    hs_JpsipiX.Add(Jpsipi_2d_BD_to_JPSI_K_PI->ProjectionX());
    if (RARE_DECAYS)
    {
        hs_JpsipiX.Add(Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionX());
        hs_JpsipiX.Add(Jpsipi_2d_BD_to_JPSI_K_K->ProjectionX());
        hs_JpsipiX.Add(Jpsipi_2d_BS_to_JPSI_K_PI->ProjectionX());
    }
    
    hist_buffer = (TH1D*)Jpsipi_2d_data->ProjectionX()->Clone("hist_buffer");
    
    hist_buffer->SetStats(kFALSE);
    hist_buffer->SetTitle("");
    hist_buffer->GetXaxis()->SetTitle("m(J/#psi, h_{1}=#pi), GeV");
    hist_buffer->GetYaxis()->SetTitle("Events / 34 MeV");
    hist_buffer->GetYaxis()->SetRangeUser(0., hist_buffer->GetMaximum() * 1.1);
    
    hist_buffer->Draw("");
    hs_JpsipiX.Draw("hist same");
    hist_buffer->Draw("same");
    hist_buffer->Draw("same axis");
    l_Kpi->Draw();
    
    c_JpsipiX->SaveAs("Jpsipi_2d_prepare_test.png");
	
	Double_t model_integral, model_integral_X, model_integral_Y;
 
	cout << "\nKpi_piK_2d" << endl; 
    printf("%-10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s\n", "name", "data", "model", "BD_KPI", "BS_KK", "BD_PIPI", "BS_PIPI", "LB_PK", "LB_PPI", "BD_KK", "BS_KPI", "comb_bg");
    model_integral = Kpi_piK_2d_bg->Integral() + Kpi_piK_2d_BD_to_JPSI_K_PI->Integral() + Kpi_piK_2d_BS_to_JPSI_K_K->Integral() + Kpi_piK_2d_BS_to_JPSI_PI_PI->Integral() + Kpi_piK_2d_BD_to_JPSI_PI_PI->Integral() + Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Integral() + Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->Integral() + Kpi_piK_2d_BD_to_JPSI_K_K->Integral() + Kpi_piK_2d_BS_to_JPSI_K_PI->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "2d", Kpi_piK_2d_data->Integral(), model_integral, Kpi_piK_2d_BD_to_JPSI_K_PI->Integral(), Kpi_piK_2d_BS_to_JPSI_K_K->Integral(), Kpi_piK_2d_BD_to_JPSI_PI_PI->Integral(), Kpi_piK_2d_BS_to_JPSI_PI_PI->Integral(), Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->Integral(), Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->Integral(), Kpi_piK_2d_BD_to_JPSI_K_K->Integral(), Kpi_piK_2d_BS_to_JPSI_K_PI->Integral(), Kpi_piK_2d_bg->Integral());
    model_integral_X = Kpi_piK_2d_bg->ProjectionX()->Integral() + Kpi_piK_2d_BD_to_JPSI_K_PI->ProjectionX()->Integral() + Kpi_piK_2d_BS_to_JPSI_K_K->ProjectionX()->Integral() + Kpi_piK_2d_BS_to_JPSI_PI_PI->ProjectionX()->Integral() + Kpi_piK_2d_BD_to_JPSI_PI_PI->ProjectionX()->Integral() + Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral() + Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral() + Kpi_piK_2d_BD_to_JPSI_K_K->ProjectionX()->Integral() + Kpi_piK_2d_BS_to_JPSI_K_PI->ProjectionX()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "X", Kpi_piK_2d_data->ProjectionX()->Integral(), model_integral_X, Kpi_piK_2d_BD_to_JPSI_K_PI->ProjectionX()->Integral(), Kpi_piK_2d_BS_to_JPSI_K_K->ProjectionX()->Integral(), Kpi_piK_2d_BD_to_JPSI_PI_PI->ProjectionX()->Integral(), Kpi_piK_2d_BS_to_JPSI_PI_PI->ProjectionX()->Integral(), Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral(), Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral(), Kpi_piK_2d_BD_to_JPSI_K_K->ProjectionX()->Integral(), Kpi_piK_2d_BS_to_JPSI_K_PI->ProjectionX()->Integral(), Kpi_piK_2d_bg->ProjectionX()->Integral());
    model_integral_Y = Kpi_piK_2d_bg->ProjectionY()->Integral() + Kpi_piK_2d_BD_to_JPSI_K_PI->ProjectionY()->Integral() + Kpi_piK_2d_BS_to_JPSI_K_K->ProjectionY()->Integral() + Kpi_piK_2d_BS_to_JPSI_PI_PI->ProjectionY()->Integral() + Kpi_piK_2d_BD_to_JPSI_PI_PI->ProjectionY()->Integral() + Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral() + Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral() + Kpi_piK_2d_BD_to_JPSI_K_K->ProjectionY()->Integral() + Kpi_piK_2d_BS_to_JPSI_K_PI->ProjectionY()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "Y", Kpi_piK_2d_data->ProjectionY()->Integral(), model_integral_Y, Kpi_piK_2d_BD_to_JPSI_K_PI->ProjectionY()->Integral(), Kpi_piK_2d_BS_to_JPSI_K_K->ProjectionY()->Integral(), Kpi_piK_2d_BD_to_JPSI_PI_PI->ProjectionY()->Integral(), Kpi_piK_2d_BS_to_JPSI_PI_PI->ProjectionY()->Integral(), Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral(), Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral(), Kpi_piK_2d_BD_to_JPSI_K_K->ProjectionY()->Integral(), Kpi_piK_2d_BS_to_JPSI_K_PI->ProjectionY()->Integral(), Kpi_piK_2d_bg->ProjectionY()->Integral());
    
	 cout << "\nJpsipi_2d" << endl;
    printf("%-10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s\n", "name", "data", "model", "BD_KPI", "BS_KK", "BD_PIPI", "BS_PIPI", "LB_PK", "LB_PPI", "BD_KK", "BS_KPI", "comb_bg");
    model_integral = Jpsipi_2d_bg->Integral() + Jpsipi_2d_BD_to_JPSI_K_PI->Integral() + Jpsipi_2d_BS_to_JPSI_K_K->Integral() + Jpsipi_2d_BS_to_JPSI_PI_PI->Integral() + Jpsipi_2d_BD_to_JPSI_PI_PI->Integral() + Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->Integral() + Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->Integral() + Jpsipi_2d_BD_to_JPSI_K_K->Integral() + Jpsipi_2d_BS_to_JPSI_K_PI->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "2d", Jpsipi_2d_data->Integral(), model_integral, Jpsipi_2d_BD_to_JPSI_K_PI->Integral(), Jpsipi_2d_BS_to_JPSI_K_K->Integral(), Jpsipi_2d_BD_to_JPSI_PI_PI->Integral(), Jpsipi_2d_BS_to_JPSI_PI_PI->Integral(), Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->Integral(), Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->Integral(), Jpsipi_2d_BD_to_JPSI_K_K->Integral(), Jpsipi_2d_BS_to_JPSI_K_PI->Integral(), Jpsipi_2d_bg->Integral());
    model_integral_X = Jpsipi_2d_bg->ProjectionX()->Integral() + Jpsipi_2d_BD_to_JPSI_K_PI->ProjectionX()->Integral() + Jpsipi_2d_BS_to_JPSI_K_K->ProjectionX()->Integral() + Jpsipi_2d_BS_to_JPSI_PI_PI->ProjectionX()->Integral() + Jpsipi_2d_BD_to_JPSI_PI_PI->ProjectionX()->Integral() + Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral() + Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral() + Jpsipi_2d_BD_to_JPSI_K_K->ProjectionX()->Integral() + Jpsipi_2d_BS_to_JPSI_K_PI->ProjectionX()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "X", Jpsipi_2d_data->ProjectionX()->Integral(), model_integral_X, Jpsipi_2d_BD_to_JPSI_K_PI->ProjectionX()->Integral(), Jpsipi_2d_BS_to_JPSI_K_K->ProjectionX()->Integral(), Jpsipi_2d_BD_to_JPSI_PI_PI->ProjectionX()->Integral(), Jpsipi_2d_BS_to_JPSI_PI_PI->ProjectionX()->Integral(), Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->ProjectionX()->Integral(), Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionX()->Integral(), Jpsipi_2d_BD_to_JPSI_K_K->ProjectionX()->Integral(), Jpsipi_2d_BS_to_JPSI_K_PI->ProjectionX()->Integral(), Jpsipi_2d_bg->ProjectionX()->Integral());
    model_integral_Y = Jpsipi_2d_bg->ProjectionY()->Integral() + Jpsipi_2d_BD_to_JPSI_K_PI->ProjectionY()->Integral() + Jpsipi_2d_BS_to_JPSI_K_K->ProjectionY()->Integral() + Jpsipi_2d_BS_to_JPSI_PI_PI->ProjectionY()->Integral() + Jpsipi_2d_BD_to_JPSI_PI_PI->ProjectionY()->Integral() + Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral() + Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral() + Jpsipi_2d_BD_to_JPSI_K_K->ProjectionY()->Integral() + Jpsipi_2d_BS_to_JPSI_K_PI->ProjectionY()->Integral();
    printf("%-10s%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f\n", "Y", Jpsipi_2d_data->ProjectionY()->Integral(), model_integral_Y, Jpsipi_2d_BD_to_JPSI_K_PI->ProjectionY()->Integral(), Jpsipi_2d_BS_to_JPSI_K_K->ProjectionY()->Integral(), Jpsipi_2d_BD_to_JPSI_PI_PI->ProjectionY()->Integral(), Jpsipi_2d_BS_to_JPSI_PI_PI->ProjectionY()->Integral(), Jpsipi_2d_LAMBDA0B_to_JPSI_P_K->ProjectionY()->Integral(), Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI->ProjectionY()->Integral(), Jpsipi_2d_BD_to_JPSI_K_K->ProjectionY()->Integral(), Jpsipi_2d_BS_to_JPSI_K_PI->ProjectionY()->Integral(), Jpsipi_2d_bg->ProjectionY()->Integral());
    
	Kpi_piK_2d_total_bg = (TH2D*)Kpi_piK_2d_bg->Clone("Kpi_piK_2d_total_bg");
	Kpi_piK_2d_total_bg->SetTitle("Kpi_piK_2d_total_bg");
	Kpi_piK_2d_total_bg->Add(Kpi_piK_2d_BD_to_JPSI_K_K);
	Kpi_piK_2d_total_bg->Add(Kpi_piK_2d_BD_to_JPSI_PI_PI);
	Kpi_piK_2d_total_bg->Add(Kpi_piK_2d_BS_to_JPSI_PI_PI);
	Kpi_piK_2d_total_bg->Add(Kpi_piK_2d_BS_to_JPSI_K_K);
	Kpi_piK_2d_total_bg->Add(Kpi_piK_2d_BS_to_JPSI_K_PI);
	Kpi_piK_2d_total_bg->Add(Kpi_piK_2d_LAMBDA0B_to_JPSI_P_K);
	Kpi_piK_2d_total_bg->Add(Kpi_piK_2d_LAMBDA0B_to_JPSI_P_PI);
	
	Jpsipi_2d_total_bg = (TH2D*)Jpsipi_2d_bg->Clone("Jpsipi_2d_total_bg");
	Jpsipi_2d_total_bg->SetTitle("Jpsipi_2d_total_bg");
	Jpsipi_2d_total_bg->Add(Jpsipi_2d_BD_to_JPSI_K_K);
	Jpsipi_2d_total_bg->Add(Jpsipi_2d_BD_to_JPSI_PI_PI);
	Jpsipi_2d_total_bg->Add(Jpsipi_2d_BS_to_JPSI_PI_PI);
	Jpsipi_2d_total_bg->Add(Jpsipi_2d_BS_to_JPSI_K_K);
	Jpsipi_2d_total_bg->Add(Jpsipi_2d_BS_to_JPSI_K_PI);
	Jpsipi_2d_total_bg->Add(Jpsipi_2d_LAMBDA0B_to_JPSI_P_K);
	Jpsipi_2d_total_bg->Add(Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI);
	
	Jpsipi_2d_total_model = (TH2D*)Jpsipi_2d_bg->Clone("Jpsipi_2d_total_model");
	Jpsipi_2d_total_model->SetTitle("Jpsipi_2d_total_model");
	Jpsipi_2d_total_model->Add(Jpsipi_2d_BD_to_JPSI_K_K);
	Jpsipi_2d_total_model->Add(Jpsipi_2d_BD_to_JPSI_PI_PI);
	Jpsipi_2d_total_model->Add(Jpsipi_2d_BS_to_JPSI_PI_PI);
	Jpsipi_2d_total_model->Add(Jpsipi_2d_BS_to_JPSI_K_K);
	Jpsipi_2d_total_model->Add(Jpsipi_2d_BS_to_JPSI_K_PI);
	Jpsipi_2d_total_model->Add(Jpsipi_2d_LAMBDA0B_to_JPSI_P_K);
	Jpsipi_2d_total_model->Add(Jpsipi_2d_LAMBDA0B_to_JPSI_P_PI);
	Jpsipi_2d_total_model->Add(Jpsipi_2d_BD_to_JPSI_K_PI);
}

void pseudo_data_generation()
{
	RooRandom::randomGenerator()->SetSeed(rand());
    
    RooRealVar Jpsipi1("Jpsipi1","Jpsipi1", 3.3, 5.) ;
	Jpsipi1.setBins(50);
	RooRealVar Jpsipi2("Jpsipi2","Jpsipi2", 3.3, 5.) ;
	Jpsipi2.setBins(50);
	RooDataHist model_hist("model", "", RooArgList(Jpsipi1, Jpsipi2), Jpsipi_2d_total_model);
	
	RooHistPdf model_pdg("model_pdg", "", RooArgList(Jpsipi1, Jpsipi2), model_hist);
	
	RooDataSet *pseudo_data = model_pdg.generate(RooArgSet(Jpsipi1, Jpsipi2), 53548);
	
	Jpsipi_2d_pseudo_data = (TH2D*)pseudo_data->createHistogram(Jpsipi1, Jpsipi2, 50, 50, "", "Jpsipi_2d_pseudo_data");
	
	TCanvas * c_pseudo_dataX = new TCanvas("pseudo_dataX");
	
	
	Jpsipi_2d_total_model->ProjectionX()->Draw("hist");
    Jpsipi_2d_pseudo_data->ProjectionX()->Draw("same hist");
	cout << Jpsipi_2d_pseudo_data->Integral() << endl;
	cout << Jpsipi_2d_total_model->Integral() << endl;
	c_pseudo_dataX->SaveAs("testX.png");
    
    TCanvas * c_pseudo_dataY = new TCanvas("pseudo_dataY");
	
	
	Jpsipi_2d_total_model->ProjectionY()->Draw("hist");
    Jpsipi_2d_pseudo_data->ProjectionY()->Draw("same hist");
	
	c_pseudo_dataY->SaveAs("testY.png");
}

void signal_area_fit_func(Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag)
{
	Double_t chisq = 0.0;
    float w;
	
	Jpsipi_2d_BD_to_JPSI_K_PI->Reset();
	Kpi_piK_2d_BD_to_JPSI_K_PI->Reset();
	
	std::thread thr[THREADS];
    int trd;
	
	for(trd=0; trd < THREADS; trd++)
    {
        thr[trd] = (std::thread([trd, par](){
        for(int ent0 = 0; ent0 < phs_Bd/THREADS; ent0++)
            {
                int ent = ent0 + trd*phs_Bd/THREADS;
                weight[ent] = HamplitudeXKstar_total_accelerated(Jpsipi_truth[ent] / 1000., Kpi_truth[ent] / 1000., 5.27964, 0, 4.43, 0., 0.181,
                                    std::complex<float>(0., 0.), 
                                    std::complex<float>(0., 0.),
                                    std::complex<float>(0., 0.), 
                                    std::complex<float>(0., 0.),
                                    std::complex<float>(par[0] * cos(par[1]), par[0] * sin(par[1])), 
                                    std::complex<float>(par[16] * cos(par[17]), par[16] * sin(par[17])), 
                                    std::complex<float>(par[18] * cos(par[19]), par[18] * sin(par[19])),
                                    std::complex<float>(par[2] * cos(par[3]), par[2] * sin(par[3])), 
                                    std::complex<float>(0., 0.),
                                    std::complex<float>(0., 0.),
                                    std::complex<float>(par[4] * cos(par[5]), par[4] * sin(par[5])), 
                                    std::complex<float>(par[20] * cos(par[21]), par[20] * sin(par[21])), 
                                    std::complex<float>(par[22] * cos(par[23]), par[22] * sin(par[23])),
                                    std::complex<float>(par[6] * cos(par[7]), par[6] * sin(par[7])), 
                                    std::complex<float>(par[24] * cos(par[25]), par[24] * sin(par[25])), 
                                    std::complex<float>(par[26] * cos(par[27]), par[26] * sin(par[27])),
                                    std::complex<float>(par[8] * cos(par[9]), par[8] * sin(par[9])), 
                                    std::complex<float>(par[28] * cos(par[29]), par[28] * sin(par[29])), 
                                    std::complex<float>(par[30] * cos(par[31]), par[30] * sin(par[31])),
                                    std::complex<float>(par[10] * cos(par[11]), par[10] * sin(par[11])), 
                                    std::complex<float>(0., 0.), 
                                    std::complex<float>(0., 0.),
                                    std::complex<float>(par[14] * cos(par[15]), par[14] * sin(par[15])),
                                    std::complex<float>(par[32] * cos(par[33]), par[32] * sin(par[33])),
                                    std::complex<float>(par[34] * cos(par[35]), par[34] * sin(par[35])),
                                    std::complex<float>(par[12] * cos(par[13]), par[12] * sin(par[13])),
                                    std::complex<float>(par[36] * cos(par[37]), par[36] * sin(par[37])),
                                    std::complex<float>(par[38] * cos(par[39]), par[38] * sin(par[39])),
                                    std::complex<float>(par[40] * cos(par[41]), par[40] * sin(par[42])),
                                    theta_X_a[ent], theta_Kstar_a[ent], theta_psi_X_a[ent], theta_psi_Kstar_a[ent], phi_mu_X_a[ent], phi_mu_Kstar_a[ent], 
                                    phi_K_a[ent], alpha_mu_a[ent],
                                    BWK1410_1_1_minus[ent], BWK1410_1_2_minus[ent], BWK1410_1_3_minus[ent], 
                                    BWK1430_0_1_minus[ent], 
                                    BWK1430_2_1_minus[ent], BWK1430_2_2_minus[ent], BWK1430_2_3_minus[ent], 
                                    BWK1680_1_1_minus[ent], BWK1680_1_2_minus[ent], BWK1680_1_3_minus[ent], 
                                    BWK1780_3_1_minus[ent], BWK1780_3_2_minus[ent], BWK1780_3_3_minus[ent], 
                                    BWK1950_0_1_minus[ent], 
                                    BWK1980_2_1_minus[ent], BWK1980_2_2_minus[ent], BWK1980_2_3_minus[ent], 
                                    BWK2045_4_1_minus[ent], BWK2045_4_2_minus[ent], BWK2045_4_3_minus[ent],
                                    BWK1410_1_1_plus[ent], BWK1410_1_2_plus[ent], BWK1410_1_3_plus[ent], 
                                    BWK1430_0_1_plus[ent], 
                                    BWK1430_2_1_plus[ent], BWK1430_2_2_plus[ent], BWK1430_2_3_plus[ent], 
                                    BWK1680_1_1_plus[ent], BWK1680_1_2_plus[ent], BWK1680_1_3_plus[ent], 
                                    BWK1780_3_1_plus[ent], BWK1780_3_2_plus[ent], BWK1780_3_3_plus[ent], 
                                    BWK1950_0_1_plus[ent], 
                                    BWK1980_2_1_plus[ent], BWK1980_2_2_plus[ent], BWK1980_2_3_plus[ent], 
                                    BWK2045_4_1_plus[ent], BWK2045_4_2_plus[ent], BWK2045_4_3_plus[ent]) * w_dimuon_a[ent];
            }
           
    }));
    }
    
    for(trd=0; trd < THREADS; trd++)
    {
        ////printf("joining %d thread...\n", trd);
        thr[trd].join();    
    } 

	coef_4_to_s_BD_to_JPSI_K_PI = 0.;
	
	for(int i = 0; i < phs_Bd - phs_Bd % THREADS; i++)
    {
        if (signal_a[i]) {
            Jpsipi_2d_BD_to_JPSI_K_PI->Fill(Jpsipi1_mass[i], Jpsipi2_mass[i], weight[i]);
            coef_4_to_s_BD_to_JPSI_K_PI += weight[i];
		}
		Kpi_piK_2d_BD_to_JPSI_K_PI->Fill(JpsiKpi_mass[i], JpsipiK_mass[i], weight[i]);
	}
	
	coef_4_to_s_BD_to_JPSI_K_PI = coef_4_to_s_BD_to_JPSI_K_PI / Kpi_piK_2d_BD_to_JPSI_K_PI->Integral();
	n_BD_to_JPSI_K_PI = coef_4_to_s_BD_to_JPSI_K_PI * n_global_BD_to_JPSI_K_PI;
	
	Jpsipi_2d_BD_to_JPSI_K_PI->Scale(1. / Jpsipi_2d_BD_to_JPSI_K_PI->Integral(1, Jpsipi_2d_nbinsX, 1, Jpsipi_2d_nbinsY) * n_BD_to_JPSI_K_PI);
	
	for(int i = 1; i <= Jpsipi_2d_data->ProjectionX()->GetNbinsX(); i++)
    {
        for(int k = 1; k <= Jpsipi_2d_data->ProjectionY()->GetNbinsX(); k++)
        {
                //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
                //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
                double nb = 0.0; double nberr = 0.0; double fb = 0.0;
                fb = Jpsipi_2d_BD_to_JPSI_K_PI->GetBinContent(i, k);
                fb += Jpsipi_2d_total_bg->GetBinContent(i, k);
                nb = Jpsipi_2d_pseudo_data->GetBinContent(i, k);
                if( fb > 0.0 ) chisq += -nb*log(fb) + fb;
                //if ((-nb*log(fb) + fb) > 0.) {cout << i << "\t" << k << endl;}
        }
    }
	f = chisq;
    //cout << f << endl; //<< "\t" << n_BD_to_JPSI_K_PI << endl;
}

void fit_iteration(std::string par_in_name)
{
	Double_t likelyhood_Jpsipi = 0.;
    
    for(int i = 1; i <= Jpsipi_2d_pseudo_data->ProjectionX()->GetNbinsX(); i++)
    {
        for(int k = 1; k <= Jpsipi_2d_pseudo_data->ProjectionY()->GetNbinsX(); k++)
        {
            //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
            //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            fb = Jpsipi_2d_total_model->GetBinContent(i, k);
            //cout << bglevel0_psipi << endl;
            nb = Jpsipi_2d_pseudo_data->GetBinContent(i, k);
            if( fb > 0.0  && nb > 0.0) likelyhood_Jpsipi += -nb*log(fb) + fb - nb + nb*log(nb);
            //if ((-nb*log(fb) + fb) > 0.) {cout << i << "\t" << k << endl;}
        }
    }
    
    cout << "\n\nLikelyhood Jpsipi:\n" << likelyhood_Jpsipi << endl;
    
    Double_t likelyhood_Jpsipi_old = 0.;
    
    for(int i = 1; i <= Jpsipi_2d_data->ProjectionX()->GetNbinsX(); i++)
    {
        for(int k = 1; k <= Jpsipi_2d_data->ProjectionY()->GetNbinsX(); k++)
        {
            //xx0 = bg_signal_jpsipi->GetBinCenter(i); // may be benefit
            //if(xx0 < xmin_psipi || xx0 > xmax_psipi) continue; // may be benefit
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            fb = Jpsipi_2d_total_model->GetBinContent(i, k);
            //cout << bglevel0_psipi << endl;
            nb = Jpsipi_2d_data->GetBinContent(i, k);
            if( fb > 0.0 && nb > 0.0) likelyhood_Jpsipi_old += -nb*log(fb) + fb - nb + nb*log(nb);
            //if ((-nb*log(fb) + fb) > 0.) {cout << i << "\t" << k << endl;}
        }
    }
    
    cout << "\n\nLikelyhood Jpsipi_old:\n" << likelyhood_Jpsipi_old << endl;
    
    TH1D *JpsipiX_data = (TH1D*)Jpsipi_2d_data->ProjectionX()->Clone("JpsipiX_data");
    JpsipiX_data->SetTitle("JpsipiX_data");
    
    TH1D *JpsipiX_pseudo_data = (TH1D*)Jpsipi_2d_pseudo_data->ProjectionX()->Clone("JpsipiX_pseudo_data");
    JpsipiX_pseudo_data->SetTitle("JpsipiX_pseudo_data");
    
    TH1D *JpsipiX_total_model = (TH1D*)Jpsipi_2d_total_model->ProjectionX()->Clone("JpsipiX_total_model");
    JpsipiX_total_model->SetTitle("JpsipiX_total_model");
    
    TH1D *likelyhood = (TH1D*)Jpsipi_2d_total_model->ProjectionX()->Clone("likelyhood");
    likelyhood->SetTitle("likelyhood");
    TH1D *likelyhood_old = (TH1D*)Jpsipi_2d_total_model->ProjectionX()->Clone("likelyhood_old");
    likelyhood_old->SetTitle("likelyhood_old");
    
    likelyhood->Reset();
    likelyhood_old->Reset();
    
    Double_t likelyhood_Jpsipi_oldX = 0.;
    
    for(int i = 1; i <= JpsipiX_data->GetNbinsX(); i++)
    {
        
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            fb = JpsipiX_total_model->GetBinContent(i);
            //cout << bglevel0_psipi << endl;
            nb = JpsipiX_data->GetBinContent(i);
            if( fb > 0.0 ) likelyhood_Jpsipi_oldX += -nb*log(fb) + fb;
            if( fb > 0.0 ) {likelyhood_old->SetBinContent(i, nb*log(fb) - fb);}
            else {likelyhood_old->SetBinContent(i, 0.);}
    }
    
    cout << "\n\nLikelyhood Jpsipi_oldX:\n" << likelyhood_Jpsipi_oldX << endl;
    
    Double_t likelyhood_JpsipiX = 0.;
    
    for(int i = 1; i <= JpsipiX_pseudo_data->GetNbinsX(); i++)
    {
        
            double nb = 0.0; double nberr = 0.0; double fb = 0.0;
            fb = JpsipiX_total_model->GetBinContent(i);
            //cout << bglevel0_psipi << endl;
            nb = JpsipiX_pseudo_data->GetBinContent(i);
            if( fb > 0.0 ) likelyhood_JpsipiX += -nb*log(fb) + fb;
            if( fb > 0.0 ) {likelyhood->SetBinContent(i, nb*log(fb) - fb);}
            else {likelyhood->SetBinContent(i, 0.);}
    }
    
    cout << "\n\nLikelyhood JpsipiX:\n" << likelyhood_JpsipiX << endl;
    
    TCanvas * c_likelyhood = new TCanvas("likelyhood");
	likelyhood->Draw("hist");
	c_likelyhood->SaveAs("likelyhood.png");
    
    TCanvas * c_likelyhood_old = new TCanvas("likelyhood_old");
	likelyhood_old->Draw("hist");
	c_likelyhood_old->SaveAs("likelyhood_old.png");
    
    TFile *f_in_par = new TFile(par_in_name.c_str(), "open");
    TVectorD *vpar = (TVectorD*)f_in_par->Get("parameters");
    
    THREADS = 8;
    
	TMinuit *Min = new TMinuit(42);
    Min->SetFCN(signal_area_fit_func);
    
    Double_t vstart[42];
    Double_t step[42];
    
    for(int i = 0; i < 40; i++)
    {
        vstart[i] = (*vpar)(i);
    }
    vstart[40] = (*vpar)(217);
	vstart[41] = (*vpar)(218);
	
	for(int j=0; j<42; j++)
	{
		if( j % 2 == 1)
		{  
			int n = (int)(vstart[j]/2/PI);   vstart[j] =  vstart[j] - n*2*PI;  // force phase to 0..2PI range
			if(vstart[j-1] < 0){vstart[j-1] *= -1.0; vstart[j] += PI; }        // force amplitude to be positive, phase gets additional PI
		}//ifj%2

	}//if 0..43
    
	for(int i = 0; i < 42; i++)
    {
        if( i % 2 == 1)
		{
			step[i] = 0.1;
		}
		else
		{
			step[i] = 1.;
		}
    }
	
	Int_t ierflg;
	
	Min->mnparm(0, "K1410_1_Bw1_amp", vstart[0], step[0], 0., 1.0e12, ierflg);
    Min->mnparm(1, "K1410_1_Bw1_phi", vstart[1], step[1], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(2, "K1430_0_Bw1_amp", vstart[2], step[2], 0., 1.0e12, ierflg);
    Min->mnparm(3, "K1430_0_Bw1_phi", vstart[3], step[3], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(4, "K1430_2_Bw1_amp", vstart[4], step[4], 0., 1.0e12, ierflg);
    Min->mnparm(5, "K1430_2_Bw1_phi", vstart[5], step[5], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(6, "K1680_1_Bw1_amp", vstart[6], step[6], 0., 1.0e12, ierflg);
    Min->mnparm(7, "K1680_1_Bw1_phi", vstart[7], step[7], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(8, "K1780_3_Bw1_amp", vstart[8], step[8], 0., 1.0e12, ierflg);
    Min->mnparm(9, "K1780_3_Bw1_phi", vstart[9], step[9], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(10, "K1950_0_Bw1_amp", vstart[10], step[10], 0., 1.0e12, ierflg);
    Min->mnparm(11, "K1950_0_Bw1_phi", vstart[11], step[11], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(12, "K2045_4_Bw1_amp", vstart[12], step[12], 0., 1.0e12, ierflg);
    Min->mnparm(13, "K2045_4_Bw1_phi", vstart[13], step[13], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(14, "K1980_2_Bw1_amp", vstart[14], step[14], 0., 1.0e12, ierflg);
    Min->mnparm(15, "K1980_2_Bw1_phi", vstart[15], step[15], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(16, "K1410_1_Bw2_amp", vstart[16], step[16], 0., 1.0e12, ierflg);
    Min->mnparm(17, "K1410_1_Bw2_phi", vstart[17], step[17], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(18, "K1410_1_Bw3_amp", vstart[18], step[18], 0., 1.0e12, ierflg);
    Min->mnparm(19, "K1410_1_Bw3_phi", vstart[19], step[19], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(20, "K1430_2_Bw2_amp", vstart[20], step[20], 0., 1.0e12, ierflg);
    Min->mnparm(21, "K1430_2_Bw2_phi", vstart[21], step[21], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(22, "K1430_2_Bw3_amp", vstart[22], step[22], 0., 1.0e12, ierflg);
    Min->mnparm(23, "K1430_2_Bw3_phi", vstart[23], step[23], -8.0*PI, 8.0*PI, ierflg);
	Min->mnparm(24, "K1680_1_Bw2_amp", vstart[24], step[24], 0., 1.0e12, ierflg);
    Min->mnparm(25, "K1680_1_Bw2_phi", vstart[25], step[25], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(26, "K1680_1_Bw3_amp", vstart[26], step[26], 0., 1.0e12, ierflg);
    Min->mnparm(27, "K1680_1_Bw3_phi", vstart[27], step[27], -8.0*PI, 8.0*PI, ierflg);
	Min->mnparm(28, "K1780_3_Bw2_amp", vstart[28], step[28], 0., 1.0e12, ierflg);
    Min->mnparm(29, "K1780_3_Bw2_phi", vstart[29], step[29], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(30, "K1780_3_Bw3_amp", vstart[30], step[30], 0., 1.0e12, ierflg);
    Min->mnparm(31, "K1780_3_Bw3_phi", vstart[31], step[31], -8.0*PI, 8.0*PI, ierflg);
	Min->mnparm(32, "K1980_2_Bw2_amp", vstart[32], step[32], 0., 1.0e12, ierflg);
    Min->mnparm(33, "K1980_2_Bw2_phi", vstart[33], step[33], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(34, "K1980_2_Bw3_amp", vstart[34], step[34], 0., 1.0e12, ierflg);
    Min->mnparm(35, "K1980_2_Bw3_phi", vstart[35], step[35], -8.0*PI, 8.0*PI, ierflg);
	Min->mnparm(36, "K2045_4_Bw2_amp", vstart[36], step[36], 0., 1.0e12, ierflg);
    Min->mnparm(37, "K2045_4_Bw2_phi", vstart[37], step[37], -8.0*PI, 8.0*PI, ierflg);
    Min->mnparm(38, "K2045_4_Bw3_amp", vstart[38], step[38], 0., 1.0e12, ierflg);
    Min->mnparm(39, "K2045_4_Bw3_phi", vstart[39], step[39], -8.0*PI, 8.0*PI, ierflg);
	Min->mnparm(40, "KNR_0_Bw_amp", vstart[40], step[40], 0., 1.0e12, ierflg);
    Min->mnparm(41, "KNR_0_Bw_phi", vstart[41], step[41], -8.0*PI, 8.0*PI, ierflg);
	
	Min->FixParameter(0); //should always be fixedd as p0 for comb bg
    Min->FixParameter(1);
	
	Double_t arglist[10];
    arglist[0] = 1;
    Min->SetErrorDef(0.5);
    //вызов статистики:
    Double_t amin = 0.,edm,errdef;
    Int_t nvpar,nparx,icstat;
    
    Min->mnexcm("SET ERR", arglist, 1, ierflg);
    
    arglist[0] = 100000; //число итераций
    arglist[1] = 0.01;   //целевая точность
    Min->mnexcm("MIGRAD", arglist , 2,ierflg);
    
    arglist[0] = 50000;
    arglist[1] = 0.01;

    Min->mnstat(amin,edm,errdef,nvpar,nparx,icstat);
    
    int cntr00 = 0;
    while( icstat < 3 && cntr00 < 5 )
    {
            arglist[0] = 500000;
            arglist[1] = 0.1;
            Min->mnexcm("MIGRAD", arglist ,2,ierflg);

            arglist[0] = 100000;
            arglist[1] = 0.1;
            Min->mnexcm( "HESse", arglist, 2, ierflg);

            Min->mnstat(amin,edm,errdef,nvpar,nparx,icstat);

            cntr00++;
    }
	
	cout << "\n\n Minimized value:\t" << amin << endl;
}
