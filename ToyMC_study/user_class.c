#include <stdio.h>
#include <time.h>
#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <TMath.h>
#include <vector>

#include <TROOT.h> 

#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TMinuit.h>
#include <TLatex.h>
#include <TRandom3.h> 
#include <thread>
#include <algorithm>
#include <THStack.h>
#include <TRandom.h>

#include <complex>
//#include "AtlasStyle.png"

#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TF1.h>

#include "TLorentzVector.h"
#include "TVector3.h"
#include "TVectorD.h"

#include "TMath.h"
//#include "Math/Vector3D.h"
//#include "Math/Vector4D.h"

void Fill_custom(Double_t a[], Double_t x_value, Double_t y_value, Double_t weight)
{
	a[(int)(((x_value - a[2]) / a[6]) + 1) * (int)a[1] + (int)((y_value - a[3]) / a[7])] += weight;
	/*for(int i = 0; i < (int)a[0]; i++)
	for(int j = 0; j < (int)a[1]; j++)
	{
		cout << a[(i + 1) * (int)a[1] + j] << endl;
	}*/
}

Double_t Integral_custom(Double_t a[])
{
	Double_t integral = 0.;
	for(int i = 0; i < (int)a[0]; i++)
	for(int j = 0; j < (int)a[1]; j++)
	{
		integral += a[(i + 1) * (int)a[1] + j];
	}
	return integral;
}

TH2 * hist = new TH2D("hist", "hist", 10, 0., 1., 10, 0., 1.);

double b[11][10];



using namespace std;


int main_TH2()
{
	Double_t x1, x2;
	b[0][0] = 10.; b[0][1] = 10.; b[0][2] = 0.; b[0][3] = 0.; b[0][4] = 10.; b[0][5] = 10.; b[0][6] = 0.1; b[0][7] = 0.1; 
	for(int i = 0; i < 10000000; i++)
	{
		x1 = (Double_t)rand() / RAND_MAX;
		x2 = (Double_t)rand() / RAND_MAX;
		//Fill_custom(a, x1, x2, 1.);
		//hist->Fill(x1, x2);
		//Fill_custom(&b[0][0], x1, x2, 1.);
		Integral_custom(&b[0][0]);
	}
	
	
	
	return 0;
}