root -b -l << EOF
.L Toy_mc_fit.c++
prepare("parameters_signal_noZc_v16_it2_n8.root");
pseudo_data_generation();	
fit_iteration("parameters_signal_noZc_v16_it2_n8.root");					
.q
EOF
