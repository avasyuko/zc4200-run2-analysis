That repository contains code for Zc(4200) Run2 analysys. There are ntuples in avasyuko cernbox.To get access you can send me email artem.vasyukov@cern.ch

The workflow of analysis:

First of all one should make slimming of ntuples:
run scripts:
stiching_data.c                    // for stiching_data.c one should choose data period (line 4)
stiching_BD_to_JPSI_K_PI.c         //  
stiching_BD_to_JPSI_PI_PI.c        // Don`t forget to write the proper paths to 
stiching_BS_to_JPSI_PI_PI.c        // ntuples inside that scripts (at the beginning of each program)
stiching_BS_to_JPSI_K_K.c          //  
stiching_LAMBDA0B_to_JPSI_P_K.c    //

The commands to run stiching of data:
[root 0] .x stiching_data.c
* you should use variable "selection_mode" to vary selection conditions.

Tthe commands to run stiching of mc:
[root 0] .L trigger_weight_mc.c
[root 1] .x stiching_<mc decay name>.c

To run fit code you should get necessary histograms and trees for calculating matrix elements. To do that, you should do following operations:

1) Run event_selector.c to generate following root files:
    - fitting_Zc1plus.root (4track global area distributions);
    - comb_bg_signal.root (muon same charge signal area events distributions);
    - signal_area_Zc1plus.root (events from signal area distributions);
    - control_area_BsKK_Zc1plus.root (events from Bs->J/psiKK control area distributions);
    - control_area_Lb.root (events from Lb->J/psipK control area distributions);
    - phase_space.root (kinematic variables for calculatin matrix elements);
    - cos_signal.root (angular variables distributions);
    
Instructions:
[root 0] .L event_selector.c
[root 1] main()

2) Run scripts to get combinatorial background for signal area and control area of LAMBDA0B_to_JPSI_P_K decays.
See detail information in README.txt in Comb_bg folder. Brief information:

To obtain comb bg in signal area you should use following instructions:

[0] cd Comb_bg
[1] root
[root 0] .L Bd_decay_amplitude.c
[root 1] event_selector_comb.c

To obtain comb bg in control area you should use following instructions:

[0] cd Comb_bg
[1] root
[root 0] .L event_selector_comb_control_BsKK.c
[root 1] main()

3) In the folder SignalAreaShapeSA there is code for obtaining signal area and Lb decays control area shapes. You should rerun EvoAlg after every iteration of fit proceduree (and copy signal_area_shape.root to ROOT_files folder). More instructions see in README.txt from SignalAreaShapeSA folder. 

Instructions to run EvoAlg:
[0] cd SignalAreaShapeSA
[1] root
[root 0] .L esab_run.c
[root 1] main()

4) Finaly you can run fitting code.

Instructions:
[0] cd Zc1plus_fit/input
[1] . run_test_v16_<version>.sh
