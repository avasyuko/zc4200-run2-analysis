#include <stdio.h>
#include <time.h>
#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <TMath.h>
#include <vector>

#include <TROOT.h> 

#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TMinuit.h>
#include <TLatex.h>
#include <TRandom3.h> 
#include <thread>
#include <algorithm>
#include <THStack.h>

#include <complex>
//#include "AtlasStyle.C"

#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TF1.h>

#include "TLorentzVector.h"
#include "TVector3.h"
#include "TVectorD.h"

#include "TMath.h"
#include "Math/Vector3D.h"
#include "Math/Vector4D.h"

#define THREADS 4

using namespace std;

#define MASS_JPSI   3.096900
#define MASS_MUON   0.1056583745
#define MASS_PION   0.13957061
#define MASS_KAON   0.493677
#define MASS_PROTON 0.938272081
#define MASS_LB     5.61960
#define MASS_BD     5.27963
#define MASS_BS     5.36689
#define MEV         1000.0

#define includeFNRsimple 1


#define pK_Kp_bins 64
#define pK_Kp_low 5.200
#define pK_Kp_high 6.800
#define pK_Kp_bins2 66
#define pK_Kp_low2 5.150
#define pK_Kp_high2 6.800

#define pK_Kp_bins3 50
#define pK_Kp_low3 5.200
#define pK_Kp_high3 7.000
#define pK_Kp_bins4 51
#define pK_Kp_low4 5.150
#define pK_Kp_high4 7.000

#define Kpi_piK_bins 40
#define Kpi_piK_low 4.900
#define Kpi_piK_high 5.700
#define Kpi_piK_bins2 62
#define Kpi_piK_low2 4.650
#define Kpi_piK_high2 5.900

#define cos_bins 10
#define cos_low 0.0
#define cos_high 1.0

#define phi_bins 10
#define phi_low -3.1416
#define phi_high 3.1416

#define Jpsipi_bins 50
#define Jpsipi_low 3.300
#define Jpsipi_high 5.000

#define JpsiK_bins 50
#define JpsiK_low 3.500
#define JpsiK_high 5.200

#define Kpi_bins 50
#define Kpi_low 1.550
#define Kpi_high 2.400

#define KK_bins 62
#define KK_low 1.680
#define KK_high 2.300

#define JpsiK_bins2 100
#define JpsiK_low2 3.575
#define JpsiK_high2 4.850

#define JpsiK_bins3 50
#define JpsiK_low3 3.575
#define JpsiK_high3 4.850

#define KK_pipi_bins 40
#define KK_pipi_low 4.970
#define KK_pipi_high 6.000

#define KK_pipi_bins2 40
#define KK_pipi_low2 4.550
#define KK_pipi_high2 5.650

#define JpsiK_bins4 50
#define JpsiK_low4 3.600
#define JpsiK_high4 4.850

#define Jpsip_bins 50
#define Jpsip_low 4.000
#define Jpsip_high 5.250

#define pK_bins 50
#define pK_low 2.000
#define pK_high 2.800

#define pK_Kp_bins5 50
#define pK_Kp_low5 7.400
#define pK_Kp_high5 9.500

#define pK_Kp_bins6 50
#define pK_Kp_low6 -1.000
#define pK_Kp_high6 0.900

#define KK_bins2 100
#define KK_low2 4.970
#define KK_high2 6.000

#define pipi_bins 100
#define pipi_low 4.550
#define pipi_high 5.650

#define pipi_bins2 440
#define pipi_low2 4.550
#define pipi_high2 5.650

Double_t s_calculation(TLorentzVector vjpsi, TLorentzVector vK1, TLorentzVector vK2)
{
	float t = 0.61803, a, b, t1, t2;
    float m, M, M0 = 5000., M1 = 5100., m0 = 5336., m1 = 5396.;
    TLorentzVector vjpsi1, vK1_1, vK2_1, vjpsi2, vK1_2, vK2_2;
    M = (vjpsi + vK1 + vK2).M();
    m = m0 + (m1 - m0) / (M1 - M0) * (M - M0);
    a = 1.;
    b = 3.;
    t1 = a + (b - a) * (1 - t);
    t2 = a + (b - a) * t;
    while (b - a > 0.0001)
    {
        vjpsi1.SetXYZM(t1 * vjpsi.Px(), t1 * vjpsi.Py(), t1 * vjpsi.Pz(), 3096.916);
        vK1_1.SetXYZM(t1 * vK1.Px(), t1 * vK1.Py(), t1 * vK1.Pz(), 493.677);
        vK2_1.SetXYZM(t1 * vK2.Px(), t1 * vK2.Py(), t1 * vK2.Pz(), 493.677);
        vjpsi2.SetXYZM(t2 * vjpsi.Px(), t2 * vjpsi.Py(), t2 * vjpsi.Pz(), 3096.916);
        vK1_2.SetXYZM(t2 * vK1.Px(), t2 * vK1.Py(), t2 * vK1.Pz(), 493.677);
        vK2_2.SetXYZM(t2 * vK2.Px(), t2 * vK2.Py(), t2 * vK2.Pz(), 493.677);
        if (abs(m - (vjpsi + vK1_1 + vK2_1).M()) > abs((vjpsi + vK1_2 + vK2_2).M() - m))
        {
            a = t1;
            t1 = t2;
            t2 = a + (b - a) * t;
        }
        else
        {
            b = t2;
            t2 = t1;
            t1 = a + (b - a) * (1 - t);
        }
    }
    vjpsi1.SetXYZM(t1 * vjpsi.Px(), t1 * vjpsi.Py(), t1 * vjpsi.Pz(), 3096.916);
    vK1_1.SetXYZM(t1 * vK1.Px(), t1 * vK1.Py(), t1 * vK1.Pz(), 493.677);
    vK2_1.SetXYZM(t1 * vK2.Px(), t1 * vK2.Py(), t1 * vK2.Pz(), 493.677);
    vjpsi2.SetXYZM(t2 * vjpsi.Px(), t2 * vjpsi.Py(), t2 * vjpsi.Pz(), 3096.916);
    vK1_2.SetXYZM(t2 * vK1.Px(), t2 * vK1.Py(), t2 * vK1.Pz(), 493.677);
    vK2_2.SetXYZM(t2 * vK2.Px(), t2 * vK2.Py(), t2 * vK2.Pz(), 493.677);
    if (abs(m - (vjpsi + vK1_1 + vK2_1).M()) > abs((vjpsi + vK1_2 + vK2_2).M() - m)) 
    {
        //cout << (vjpsi + vK1_2 + vK2_2).M()  << "    " << m << endl;
        return t2;
    }
    else
    {
        //cout << (vjpsi + vK1_1 + vK2_1).M() << "    " << m << endl;
        return t1;
    }
}

Double_t s_mc_calculation(TLorentzVector vjpsi, TLorentzVector vK1, TLorentzVector vK2)
{
	float t = 0.61803, a, b, t1, t2;
    float m, M, M0 = 4650., M1 = 4750., m0 = 5336., m1 = 5396.;
    TLorentzVector vjpsi1, vK1_1, vK2_1, vjpsi2, vK1_2, vK2_2;
    M = (vjpsi + vK1 + vK2).M();
    m = 5366.88;
    a = 1.;
    b = 3.;
    t1 = a + (b - a) * (1 - t);
    t2 = a + (b - a) * t;
    while (b - a > 0.0001)
    {
        vjpsi1.SetXYZM(t1 * vjpsi.Px(), t1 * vjpsi.Py(), t1 * vjpsi.Pz(), 3096.916);
        vK1_1.SetXYZM(t1 * vK1.Px(), t1 * vK1.Py(), t1 * vK1.Pz(), 493.677);
        vK2_1.SetXYZM(t1 * vK2.Px(), t1 * vK2.Py(), t1 * vK2.Pz(), 493.677);
        vjpsi2.SetXYZM(t2 * vjpsi.Px(), t2 * vjpsi.Py(), t2 * vjpsi.Pz(), 3096.916);
        vK1_2.SetXYZM(t2 * vK1.Px(), t2 * vK1.Py(), t2 * vK1.Pz(), 493.677);
        vK2_2.SetXYZM(t2 * vK2.Px(), t2 * vK2.Py(), t2 * vK2.Pz(), 493.677);
        if (abs(m - (vjpsi + vK1_1 + vK2_1).M()) > abs((vjpsi + vK1_2 + vK2_2).M() - m))
        {
            a = t1;
            t1 = t2;
            t2 = a + (b - a) * t;
        }
        else
        {
            b = t2;
            t2 = t1;
            t1 = a + (b - a) * (1 - t);
        }
    }
    vjpsi1.SetXYZM(t1 * vjpsi.Px(), t1 * vjpsi.Py(), t1 * vjpsi.Pz(), 3096.916);
    vK1_1.SetXYZM(t1 * vK1.Px(), t1 * vK1.Py(), t1 * vK1.Pz(), 493.677);
    vK2_1.SetXYZM(t1 * vK2.Px(), t1 * vK2.Py(), t1 * vK2.Pz(), 493.677);
    vjpsi2.SetXYZM(t2 * vjpsi.Px(), t2 * vjpsi.Py(), t2 * vjpsi.Pz(), 3096.916);
    vK1_2.SetXYZM(t2 * vK1.Px(), t2 * vK1.Py(), t2 * vK1.Pz(), 493.677);
    vK2_2.SetXYZM(t2 * vK2.Px(), t2 * vK2.Py(), t2 * vK2.Pz(), 493.677);
    if (abs(m - (vjpsi + vK1_1 + vK2_1).M()) > abs((vjpsi + vK1_2 + vK2_2).M() - m)) 
    {
        //cout << t2 << "    " << (vjpsi + vK1_2 + vK2_2).M()  << "    " << m << endl;
        return t2;
    }
    else
    {
        //cout << t1 << "    " << (vjpsi + vK1_1 + vK2_1).M() << "    " << m << endl;
        return t1;
    }
}



int main()
{        
    float GEV = 0.001;
    TH1 *KK_1d_data_11 = new TH1D("KK_1d_data_11", "KK_1d_data_11", KK_bins, KK_low, KK_high);
    TH1 *KK_1d_data_12 = new TH1D("KK_1d_data_12", "KK_1d_data_12", KK_bins, KK_low, KK_high);
    TH1 *KK_1d_data_13 = new TH1D("KK_1d_data_13", "KK_1d_data_13", KK_bins, KK_low, KK_high);

    TH1 *KK_1d_control_bg = new TH1D("KK_1d_control_bg", "KK_1d_control_bg", KK_bins, KK_low, KK_high);
    TH1 *JpsiK1_1d_control_bg = new TH1D("JpsiK1_1d_control_bg", "JpsiK1_1d_control_bg", 100, 3575. * GEV, 4850. * GEV);
    TH1 *JpsiK2_1d_control_bg = new TH1D("JpsiK2_1d_control_bg", "JpsiK2_1d_control_bg", 100, 3575. * GEV, 4850. * GEV);
    TH2 *JpsiK_2d_control_bg = new TH2D("JpsiK_2d_control_bg", "JpsiK_2d_control_bg", JpsiK_bins2, JpsiK_low2, JpsiK_high2, JpsiK_bins2, JpsiK_low2, JpsiK_high2);

    TChain stree("stree");
    stree.AddFile("../datasets/data15_for_comb.root");
    stree.AddFile("../datasets/data16Main_for_comb.root");
    stree.AddFile("../datasets/data16delayed_for_comb.root");
    stree.AddFile("../datasets/data17_for_comb.root");
    stree.AddFile("../datasets/data18_for_comb.root");
    Long64_t nentries16 = stree.GetEntries();
    cout << "Number of runs data16\t" << nentries16 << endl;

    float sB_mu1_px, sB_mu1_py, sB_mu1_pz, sB_mu2_px, sB_mu2_py, sB_mu2_pz, sB_trk1_px, sB_trk1_py, sB_trk1_pz, sB_trk2_px, sB_trk2_py, sB_trk2_pz, sB_trk1_charge, sB_trk2_charge, sB_Lxy_MaxSumPt, sB_Bs_pt, sB_mu1_pt, sB_mu2_pt, sB_trk1_pt, sB_trk2_pt, sB_Bs_chi2_ndof, sB_MaxSumPt, sB_mu1_eta, sB_mu2_eta, sB_trk1_eta, sB_trk2_eta, sB_Jpsi_mass, sB_Jpsi_chi2;
    float w;
    
    stree.SetBranchAddress("sB_mu1_px", &sB_mu1_px);
    stree.SetBranchAddress("sB_mu1_py", &sB_mu1_py);
    stree.SetBranchAddress("sB_mu1_pz", &sB_mu1_pz);
    stree.SetBranchAddress("sB_mu2_px", &sB_mu2_px);
    stree.SetBranchAddress("sB_mu2_py", &sB_mu2_py);
    stree.SetBranchAddress("sB_mu2_pz", &sB_mu2_pz);
    stree.SetBranchAddress("sB_trk1_px", &sB_trk1_px);
    stree.SetBranchAddress("sB_trk1_py", &sB_trk1_py);
    stree.SetBranchAddress("sB_trk1_pz", &sB_trk1_pz);
    stree.SetBranchAddress("sB_trk2_px", &sB_trk2_px);
    stree.SetBranchAddress("sB_trk2_py", &sB_trk2_py);
    stree.SetBranchAddress("sB_trk2_pz", &sB_trk2_pz);
    stree.SetBranchAddress("sB_trk1_charge", &sB_trk1_charge);
    stree.SetBranchAddress("sB_trk2_charge", &sB_trk2_charge);
    stree.SetBranchAddress("sB_Lxy_MaxSumPt", &sB_Lxy_MaxSumPt);
    stree.SetBranchAddress("sB_Bs_pt", &sB_Bs_pt);
    stree.SetBranchAddress("sB_mu1_pt", &sB_mu1_pt);
    stree.SetBranchAddress("sB_mu2_pt", &sB_mu2_pt);
    stree.SetBranchAddress("sB_trk1_pt", &sB_trk1_pt);
    stree.SetBranchAddress("sB_trk2_pt", &sB_trk2_pt);
    stree.SetBranchAddress("sB_Bs_chi2_ndof", &sB_Bs_chi2_ndof);
    stree.SetBranchAddress("sB_MaxSumPt", &sB_MaxSumPt);
    stree.SetBranchAddress("sB_mu1_eta", &sB_mu1_eta);
    stree.SetBranchAddress("sB_mu2_eta", &sB_mu2_eta);
    stree.SetBranchAddress("sB_trk1_eta", &sB_trk1_eta);
    stree.SetBranchAddress("sB_trk2_eta", &sB_trk2_eta);
    stree.SetBranchAddress("sB_Jpsi_mass", &sB_Jpsi_mass);
    stree.SetBranchAddress("sB_Jpsi_chi2", &sB_Jpsi_chi2);
    
    float xmin = 5000.;
    float xmax = 5100.;
    float sm;
    
    int n_test = 0, nx, ny;

    for(int i = 1; i <= nentries16; i++)
    {
        if (i % 100000 == 0) {cout << i << endl;}        
        stree.GetEntry(i);
        TLorentzVector mu1, mu2, pi1, pi2, K1, K2, p1, p2, jpsi;

        TLorentzVector mu1_11, mu2_11, pi1_11, pi2_11, K1_11, K2_11, p1_11, p2_11, jpsi_11;
        TLorentzVector mu1_12, mu2_12, pi1_12, pi2_12, K1_12, K2_12, p1_12, p2_12, jpsi_12;
        TLorentzVector mu1_13, mu2_13, pi1_13, pi2_13, K1_13, K2_13, p1_13, p2_13, jpsi_13;

        w = 1.;
        mu1.SetXYZM(sB_mu1_px, sB_mu1_py, sB_mu1_pz, 105.65837);
        mu2.SetXYZM(sB_mu2_px, sB_mu2_py, sB_mu2_pz, 105.65837);
        pi1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 139.57);
        pi2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 139.57);
        K1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 493.677);
        K2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 493.677);
        p1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 938.272);
        p2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 938.272);
        jpsi.SetXYZM(sB_mu1_px + sB_mu2_px, sB_mu1_py + sB_mu2_py, sB_mu1_pz + sB_mu2_pz, 3096.);
        TLorentzVector bs = jpsi + pi1 + pi2;
        

        if (xmin < (jpsi + K1 + K2).M() && (jpsi + K1 + K2).M() < xmax && sB_Bs_pt > 0.25 * sB_MaxSumPt && sB_Lxy_MaxSumPt > 1. && sB_Bs_chi2_ndof < 1.7 && (jpsi + pi1).M() < 4900. && (jpsi + pi2).M() < 4900.) 
        {
            if (sB_trk1_charge * sB_trk2_charge > 0) {w = -1;}             
            n_test++; 
            sm = s_calculation(jpsi, K1, K2);
            if (sm < 2.){
            mu1_11.SetXYZM(sB_mu1_px * sm, sB_mu1_py * sm, sB_mu1_pz * sm, 105.65837);
            mu2_11.SetXYZM(sB_mu2_px * sm, sB_mu2_py * sm, sB_mu2_pz * sm, 105.65837);
            pi1_11.SetXYZM(sB_trk1_px * sm, sB_trk1_py * sm, sB_trk1_pz * sm, 139.57);
            pi2_11.SetXYZM(sB_trk2_px * sm, sB_trk2_py * sm, sB_trk2_pz * sm, 139.57);
            K1_11.SetXYZM(sB_trk1_px * sm, sB_trk1_py * sm, sB_trk1_pz * sm, 493.677);
            K2_11.SetXYZM(sB_trk2_px * sm, sB_trk2_py * sm, sB_trk2_pz * sm, 493.677);
            p1_11.SetXYZM(sB_trk1_px * sm, sB_trk1_py * sm, sB_trk1_pz * sm, 938.272);
            p2_11.SetXYZM(sB_trk2_px * sm, sB_trk2_py * sm, sB_trk2_pz * sm, 938.272);
            jpsi_11.SetXYZM(sB_mu1_px * sm + sB_mu2_px * sm, sB_mu1_py * sm + sB_mu2_py * sm, sB_mu1_pz * sm + sB_mu2_pz * sm, 3096.);
            if ((K1_11 + pi2_11).M() > 1550. && (pi1_11 + K2_11).M() > 1550. && sB_mu1_pt > 4000. && sB_mu2_pt > 4000. && sB_trk1_pt * sm > 2000. && sB_trk2_pt * sm > 2000. && 4900. < (jpsi + K1_11 + pi2_11).M() && (jpsi + K1_11 + pi2_11).M() < 5700. && (jpsi + pi1_11 + K2_11).M() < 5900.)
            {
                KK_1d_data_11->Fill((jpsi + K1_11 + K2_11).M() * GEV, w);               
            }
            }
        }
    }

    double n_lambda, n_bs, n_bd_pipi, n_bd_Kpi, n_bs_KK, x0_KK, d0_KK, p0_KK, p1_KK, p2_KK, p3_KK, p4_KK, p5_KK, comb_bg_norm;

    TFile *f_par = new TFile("../Zc1plus_fit/input/parameters/parameters_signal_Zc1plus_v16_n1_ini.root", "open");
    TVectorD *vpar = (TVectorD*)f_par->Get("parameters");
    
    n_lambda = (*vpar)(80);
    n_bs = (*vpar)(81);
    n_bd_pipi = (*vpar)(82);
    n_bd_Kpi = (*vpar)(83);
    n_bs_KK = (*vpar)(84);
    x0_KK = (*vpar)(93);
    d0_KK = (*vpar)(94);
    p0_KK = (*vpar)(95);
    p1_KK = (*vpar)(96);
    p2_KK = (*vpar)(97);
    p3_KK = (*vpar)(98);
    p4_KK = (*vpar)(99);
    p5_KK = (*vpar)(100);
    comb_bg_norm = (*vpar)(213);
    
    Double_t norn_coef;

    float xx, yy, x, y, xmin_KK = 4.970;
    
    TH1 *KK_1d_bg = new TH1D("KK_1d_bg", "KK_1d_bg", 100, 4.970, 6.000);
    
    for(int i = 1; i <= KK_1d_bg->GetNbinsX(); i++)
    {
        double xx0 = KK_1d_bg->GetBinCenter(i); 
        double x = xx0 - xmin_KK;
        double nb = 0.0; double nberr = 0.0; double fb = 0.0;
        KK_1d_bg->SetBinContent(i, pow(abs(xx0 - x0_KK), d0_KK) * exp(p0_KK + x * p1_KK + pow(x, 2.) * p2_KK + pow(x, 3.) * p3_KK + pow(x, 4.) * p4_KK + pow(x, 5.) * p5_KK));
        //cout << i << "\t" << pow(xx0 - x0_KK, d0_KK) * exp(p0_KK + x * p1_KK + pow(x, 2.) * p2_KK + pow(x, 3.) * p3_KK) << endl;
    }
    
    norn_coef = comb_bg_norm / KK_1d_bg->Integral();
    
    TF1 *bg_func = new TF1("bg_func", "pow(abs(x - [0]), [1]) * exp([2] + (x - [8]) * [3] + pow((x - [8]), 2.) * [4] + pow((x - [8]), 3.) * [5] + pow((x - [8]), 4.) * [6] + pow((x - [8]), 5.) * [7])", 4.970, 6.);
    bg_func->SetParameter(0, x0_KK);
    bg_func->SetParameter(1, d0_KK);
    bg_func->SetParameter(2, p0_KK);
    bg_func->SetParameter(3, p1_KK);
    bg_func->SetParameter(4, p2_KK);
    bg_func->SetParameter(5, p3_KK);
    bg_func->SetParameter(6, p4_KK);
    bg_func->SetParameter(7, p5_KK);
    bg_func->SetParameter(8, xmin_KK);
    
    float b_size = KK_1d_bg->GetBinCenter(11) - KK_1d_bg->GetBinCenter(10);
    
    int nl = KK_1d_bg->FindBin(5.336);
    int nr = KK_1d_bg->FindBin(5.396);
    
    cout << nl << endl;
    cout << nr << endl;
    
    xx = KK_1d_bg->GetBinCenter(20);
    x = xx - xmin_KK;
    float norm = norn_coef * pow(abs(xx - x0_KK), d0_KK) * exp(p0_KK + x * p1_KK + pow(x, 2.) * p2_KK + pow(x, 3.) * p3_KK + pow(x, 4.) * p4_KK + pow(x, 5.) * p5_KK) / bg_func->Integral(xx - b_size / 2., xx + b_size / 2.);
    cout << norm << endl;
    cout << norn_coef * pow(abs(xx - x0_KK), d0_KK) * exp(p0_KK + x * p1_KK + pow(x, 2.) * p2_KK + pow(x, 3.) * p3_KK + pow(x, 4.) * p4_KK + pow(x, 5.) * p5_KK) << endl;
    cout << bg_func->Integral(xx - b_size / 2., xx + b_size / 2.) << endl;

    KK_1d_bg->SetBinContent(nl, norm * bg_func->Integral(5.336, KK_1d_bg->GetBinCenter(nl) + b_size / 2.));
    KK_1d_bg->SetBinContent(nr, norm * bg_func->Integral(KK_1d_bg->GetBinCenter(nr) - b_size / 2., 5.396));
    
    
    for(int i = nl + 1; i < nr; i++)
    {
        xx = KK_1d_bg->GetBinCenter(i);
        x = xx - xmin_KK;
        if (xmin_KK < xx)
        {
            KK_1d_bg->SetBinContent(i, norn_coef * pow(abs(xx - x0_KK), d0_KK) * exp(p0_KK + x * p1_KK + pow(x, 2.) * p2_KK + pow(x, 3.) * p3_KK + pow(x, 4.) * p4_KK + pow(x, 5.) * p5_KK));
        }
        else 
        {
            KK_1d_bg->SetBinContent(i, 0.);
        }
    }
    
    cout << "BG is ready" << endl;
    //bg_func->Draw();
    KK_1d_bg->Draw("");
    KK_1d_data_11->SetLineColor(3);
    KK_1d_data_11->Draw("hist same");
    cout << KK_1d_bg->Integral() << endl;

    for(int i = 1; i <= nentries16; i++)
    {
        if (i % 100000 == 0) {cout << i << endl;}        
        stree.GetEntry(i);
        TLorentzVector mu1, mu2, pi1, pi2, K1, K2, p1, p2, jpsi;

        TLorentzVector mu1_11, mu2_11, pi1_11, pi2_11, K1_11, K2_11, p1_11, p2_11, jpsi_11;
        TLorentzVector mu1_12, mu2_12, pi1_12, pi2_12, K1_12, K2_12, p1_12, p2_12, jpsi_12;
        TLorentzVector mu1_13, mu2_13, pi1_13, pi2_13, K1_13, K2_13, p1_13, p2_13, jpsi_13;
        

        w = 1.;
        mu1.SetXYZM(sB_mu1_px, sB_mu1_py, sB_mu1_pz, 105.65837);
        mu2.SetXYZM(sB_mu2_px, sB_mu2_py, sB_mu2_pz, 105.65837);
        pi1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 139.57);
        pi2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 139.57);
        K1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 493.677);
        K2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 493.677);
        p1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 938.272);
        p2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 938.272);
        jpsi.SetXYZM(sB_mu1_px + sB_mu2_px, sB_mu1_py + sB_mu2_py, sB_mu1_pz + sB_mu2_pz, 3096.);
        TLorentzVector bs = jpsi + pi1 + pi2;

        if (xmin < (jpsi + K1 + K2).M() && (jpsi + K1 + K2).M() < xmax && sB_Bs_pt > 0.25 * sB_MaxSumPt && sB_Lxy_MaxSumPt > 1. && sB_Bs_chi2_ndof < 1.7 && (jpsi + pi1).M() < 4900. && (jpsi + pi2).M() < 4900.) 
        {
            if (sB_trk1_charge * sB_trk2_charge > 0) {w = -1;}             
            n_test++; 
            sm = s_calculation(jpsi, K1, K2);
            if (sm < 2.){
            mu1_11.SetXYZM(sB_mu1_px * sm, sB_mu1_py * sm, sB_mu1_pz * sm, 105.65837);
            mu2_11.SetXYZM(sB_mu2_px * sm, sB_mu2_py * sm, sB_mu2_pz * sm, 105.65837);
            pi1_11.SetXYZM(sB_trk1_px * sm, sB_trk1_py * sm, sB_trk1_pz * sm, 139.57);
            pi2_11.SetXYZM(sB_trk2_px * sm, sB_trk2_py * sm, sB_trk2_pz * sm, 139.57);
            K1_11.SetXYZM(sB_trk1_px * sm, sB_trk1_py * sm, sB_trk1_pz * sm, 493.677);
            K2_11.SetXYZM(sB_trk2_px * sm, sB_trk2_py * sm, sB_trk2_pz * sm, 493.677);
            p1_11.SetXYZM(sB_trk1_px * sm, sB_trk1_py * sm, sB_trk1_pz * sm, 938.272);
            p2_11.SetXYZM(sB_trk2_px * sm, sB_trk2_py * sm, sB_trk2_pz * sm, 938.272);
            jpsi_11.SetXYZM(sB_mu1_px * sm + sB_mu2_px * sm, sB_mu1_py * sm + sB_mu2_py * sm, sB_mu1_pz * sm + sB_mu2_pz * sm, 3096.);
            if ((K1_11 + pi2_11).M() > 1550. && (pi1_11 + K2_11).M() > 1550. && sB_mu1_pt > 4000. && sB_mu2_pt > 4000. && sB_trk1_pt * sm > 2000. && sB_trk2_pt * sm > 2000. && 4900. < (jpsi + K1_11 + pi2_11).M() && (jpsi + K1_11 + pi2_11).M() < 5700. && (jpsi + pi1_11 + K2_11).M() < 5900.)
            {
                nx = KK_1d_data_11->FindBin((jpsi + K1_11 + K2_11).M() * GEV);
                if (5336. < (jpsi + K1_11 + K2_11).M() && (jpsi + K1_11 + K2_11).M() < 5396.)
                {
                    if (KK_1d_data_11->GetBinContent(nx) != 0.) {                    
                    w = w / KK_1d_data_11->GetBinContent(nx) * KK_1d_bg->GetBinContent(nx);}      
                    else {cout << "fail" << endl;}
                    KK_1d_data_12->Fill((jpsi + K1_11 + K2_11).M() * GEV, w);
                    KK_1d_control_bg->Fill((K1_11 + K2_11).M() * GEV, w);
                    JpsiK1_1d_control_bg->Fill((jpsi + K1_11).M() * GEV, w);
                    JpsiK2_1d_control_bg->Fill((jpsi + K2_11).M() * GEV, w);
                    JpsiK_2d_control_bg->Fill((jpsi + K1_11).M() * GEV, (jpsi + K2_11).M() * GEV, w);
                }
            }
            }
        }
    }
    
    TFile *f_out = new TFile("control_area_BsKK_bg.root", "recreate");
    KK_1d_control_bg->Write();
    JpsiK1_1d_control_bg->Write();
    JpsiK2_1d_control_bg->Write();
    JpsiK_2d_control_bg->Write();
    
    //__oO0Oo____oO0Oo____oO0Oo____oO0Oo__|| pictures to test result ||__oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
    
    TCanvas * c_JpsiKX = new TCanvas("jpsiK_mass_ProjectionX");
    JpsiK_2d_control_bg->ProjectionX()->Draw();
    c_JpsiKX->SaveAs("JpsiK_X_CA_BsKK_bg.png");
    
    TCanvas * c_JpsiKY = new TCanvas("jpsiK_mass_ProjectionY");
    JpsiK_2d_control_bg->ProjectionY()->Draw();
    c_JpsiKY->SaveAs("JpsiK_Y_CA_BsKK_bg.png");
    
    TCanvas * c_KK = new TCanvas("KK_mass");
    KK_1d_control_bg->Draw();
    c_KK->SaveAs("KK_CA_BsKK_bg.png");
    
    //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
    
    /*KK_1d_data_12->SetLineColor(4);
    KK_1d_data_12->Draw("same");
    cout << KK_1d_control_bg->Integral() << endl;*/
    //cout << "n_test " << n_test << endl;
}
