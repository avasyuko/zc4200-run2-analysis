#define pK_Kp_bins 64
#define pK_Kp_low 5.200
#define pK_Kp_high 6.800
#define pK_Kp_bins2 66
#define pK_Kp_low2 5.150
#define pK_Kp_high2 6.800

#define pK_Kp_bins3 50
#define pK_Kp_low3 5.200
#define pK_Kp_high3 7.000
#define pK_Kp_bins4 51
#define pK_Kp_low4 5.150
#define pK_Kp_high4 7.000

#define Kpi_piK_bins 40
#define Kpi_piK_low 4.900
#define Kpi_piK_high 5.700
#define Kpi_piK_bins2 62
#define Kpi_piK_low2 4.650
#define Kpi_piK_high2 5.900

#define cos_bins 10
#define cos_low 0.0
#define cos_high 1.0

#define phi_bins 10
#define phi_low -3.1416
#define phi_high 3.1416

#define Jpsipi_bins 50
#define Jpsipi_low 3.300
#define Jpsipi_high 5.000

#define JpsiK_bins 50
#define JpsiK_low 3.500
#define JpsiK_high 5.200

#define Kpi_bins 50
#define Kpi_low 1.550
#define Kpi_high 2.400

#define KK_bins 62
#define KK_low 1.680
#define KK_high 2.300

#define JpsiK_bins2 100
#define JpsiK_low2 3.575
#define JpsiK_high2 4.850

#define JpsiK_bins3 50
#define JpsiK_low3 3.575
#define JpsiK_high3 4.850

#define KK_pipi_bins 40
#define KK_pipi_low 4.970
#define KK_pipi_high 6.000

#define KK_pipi_bins2 40
#define KK_pipi_low2 4.550
#define KK_pipi_high2 5.650

#define JpsiK_bins4 50
#define JpsiK_low4 3.600
#define JpsiK_high4 4.850

#define Jpsip_bins 50
#define Jpsip_low 4.000
#define Jpsip_high 5.250

#define pK_bins 50
#define pK_low 2.000
#define pK_high 2.800

#define pK_Kp_bins5 50
#define pK_Kp_low5 7.400
#define pK_Kp_high5 9.500

#define pK_Kp_bins6 50
#define pK_Kp_low6 -1.000
#define pK_Kp_high6 0.900

#define KK_bins2 100
#define KK_low2 4.970
#define KK_high2 6.000

#define pipi_bins 100
#define pipi_low 4.550
#define pipi_high 5.650

#define pipi_bins2 440
#define pipi_low2 4.550
#define pipi_high2 5.650

{        
    TFile *f_sas = new TFile("../Zc1plus_fit/input/ROOT_files/signal_area_shape.root", "open");
    TH2 *area_hist = (TH2D*)f_sas->Get("area_hist");
    
    TH2 *cos_theta_Zc_bg = new TH2D("cos_theta_Zc_bg", "cos_theta_Zc_bg", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_psi_X_bg = new TH2D("cos_theta_psi_X_bg", "cos_theta_psi_X_bg", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_Kstar_bg = new TH2D("cos_theta_Kstar_bg", "cos_theta_Kstar_bg", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *cos_theta_psi_Kstar_bg = new TH2D("cos_theta_psi_Kstar_bg", "cos_theta_psi_Kstar_bg", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *phi_K_bg = new TH2D("phi_K_bg", "phi_K_bg", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *alpha_mu_bg = new TH2D("alpha_mu_bg", "alpha_mu_bg", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_mu_Kstar_bg = new TH2D("phi_mu_Kstar_bg", "phi_mu_Kstar_bg", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *phi_mu_X_bg = new TH2D("phi_mu_X_bg", "phi_mu_X_bg", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    TH2 *cos_theta_B0_bg = new TH2D("cos_theta_B0_bg", "cos_theta_B0_bg", cos_bins, cos_low, cos_high, cos_bins, cos_low, cos_high);
    TH2 *phi_psi_bg = new TH2D("phi_psi_bg", "phi_psi_bg", phi_bins, phi_low, phi_high, phi_bins, phi_low, phi_high);
    
    float GEV = 0.001;
    TH2 *Kpi_piK_2d_data_11 = new TH2D("Kpi_piK_2d_data_11", "Kpi_piK_2d_data_11", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);
    TH2 *Kpi_piK_2d_data_12 = new TH2D("Kpi_piK_2d_data_12", "Kpi_piK_2d_data_12", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);
    TH2 *Kpi_piK_2d_data_13 = new TH2D("Kpi_piK_2d_data_13", "Kpi_piK_2d_data_13", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);
    TH2 *Kpi_piK_2d_data_14 = new TH2D("Kpi_piK_2d_data_14", "Kpi_piK_2d_data_14", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);
    TH2 *Kpi_piK_2d_data_15 = new TH2D("Kpi_piK_2d_data_15", "Kpi_piK_2d_data_15", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);
    TH2 *Kpi_piK_2d_data_16 = new TH2D("Kpi_piK_2d_data_16", "Kpi_piK_2d_data_16", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);
    TH2 *Kpi_piK_2d_data_17 = new TH2D("Kpi_piK_2d_data_17", "Kpi_piK_2d_data_17", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);
    TH2 *Kpi_piK_2d_data_18 = new TH2D("Kpi_piK_2d_data_18", "Kpi_piK_2d_data_18", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);
    TH2 *Kpi_piK_2d_data_19 = new TH2D("Kpi_piK_2d_data_19", "Kpi_piK_2d_data_19", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);

    TH2 *Kpi_piK_2d_bg = new TH2D("Kpi_piK_2d_bg", "Kpi_piK_2d_bg", Kpi_piK_bins, Kpi_piK_low, Kpi_piK_high, Kpi_piK_bins2, Kpi_piK_low2, Kpi_piK_high2);

    TH2 *Jpsipi_2d = new TH2D("Jpsipi_2d_bg", "Jpsipi_2d_bg", Jpsipi_bins, Jpsipi_low, Jpsipi_high, Jpsipi_bins, Jpsipi_low, Jpsipi_high);
    TH2 *JpsiK_2d = new TH2D("JpsiK_2d_bg", "JpsiK_2d_bg", JpsiK_bins, JpsiK_low, JpsiK_high, JpsiK_bins, JpsiK_low, JpsiK_high);
    TH2 *Kpi_2d = new TH2D("Kpi_2d_bg", "Kpi_2d_bg", Kpi_bins, Kpi_low, Kpi_high, Kpi_bins, Kpi_low, Kpi_high);
    
    TH2 *trk_pt_2d_bg = new TH2D("trk_pt_2d_bg", "trk_pt_2d_bg", 50, 2000. * GEV, 10000. * GEV, 50, 2000. * GEV, 10000. * GEV);
    
    Double_t scale_coef = 1.3;
    
    TChain stree("stree");
    stree.AddFile("../datasets/data15_for_comb.root");
    stree.AddFile("../datasets/data16Main_for_comb.root");
    stree.AddFile("../datasets/data16delayed_for_comb.root");
    stree.AddFile("../datasets/data17_for_comb.root");
    stree.AddFile("../datasets/data18_for_comb.root");
    
    Long64_t nentries16 = stree.GetEntries();
    cout << "Number of runs data\t" << nentries16 << endl;

    float sB_mu1_px, sB_mu1_py, sB_mu1_pz, sB_mu2_px, sB_mu2_py, sB_mu2_pz, sB_trk1_px, sB_trk1_py, sB_trk1_pz, sB_trk2_px, sB_trk2_py, sB_trk2_pz, sB_trk1_charge, sB_trk2_charge, sB_Lxy_MaxSumPt, sB_Bs_pt, sB_mu1_pt, sB_mu2_pt, sB_trk1_pt, sB_trk2_pt, sB_Bs_chi2_ndof, sB_MaxSumPt, sB_mu1_eta, sB_mu2_eta, sB_trk1_eta, sB_trk2_eta, sB_Jpsi_mass, sB_Jpsi_chi2;
    float w;
    
    stree.SetBranchAddress("sB_mu1_px", &sB_mu1_px);
    stree.SetBranchAddress("sB_mu1_py", &sB_mu1_py);
    stree.SetBranchAddress("sB_mu1_pz", &sB_mu1_pz);
    stree.SetBranchAddress("sB_mu2_px", &sB_mu2_px);
    stree.SetBranchAddress("sB_mu2_py", &sB_mu2_py);
    stree.SetBranchAddress("sB_mu2_pz", &sB_mu2_pz);
    stree.SetBranchAddress("sB_trk1_px", &sB_trk1_px);
    stree.SetBranchAddress("sB_trk1_py", &sB_trk1_py);
    stree.SetBranchAddress("sB_trk1_pz", &sB_trk1_pz);
    stree.SetBranchAddress("sB_trk2_px", &sB_trk2_px);
    stree.SetBranchAddress("sB_trk2_py", &sB_trk2_py);
    stree.SetBranchAddress("sB_trk2_pz", &sB_trk2_pz);
    stree.SetBranchAddress("sB_trk1_charge", &sB_trk1_charge);
    stree.SetBranchAddress("sB_trk2_charge", &sB_trk2_charge);
    stree.SetBranchAddress("sB_Lxy_MaxSumPt", &sB_Lxy_MaxSumPt);
    stree.SetBranchAddress("sB_Bs_pt", &sB_Bs_pt);
    stree.SetBranchAddress("sB_mu1_pt", &sB_mu1_pt);
    stree.SetBranchAddress("sB_mu2_pt", &sB_mu2_pt);
    stree.SetBranchAddress("sB_trk1_pt", &sB_trk1_pt);
    stree.SetBranchAddress("sB_trk2_pt", &sB_trk2_pt);
    stree.SetBranchAddress("sB_Bs_chi2_ndof", &sB_Bs_chi2_ndof);
    stree.SetBranchAddress("sB_MaxSumPt", &sB_MaxSumPt);
    stree.SetBranchAddress("sB_mu1_eta", &sB_mu1_eta);
    stree.SetBranchAddress("sB_mu2_eta", &sB_mu2_eta);
    stree.SetBranchAddress("sB_trk1_eta", &sB_trk1_eta);
    stree.SetBranchAddress("sB_trk2_eta", &sB_trk2_eta);
    stree.SetBranchAddress("sB_Jpsi_mass", &sB_Jpsi_mass);
    stree.SetBranchAddress("sB_Jpsi_chi2", &sB_Jpsi_chi2);

    float xmin = 4790.;
    float xmax = 4950.;
    float xmax2 = 5100.;
    float ymin = 4790.;
    float ymax = 4950.;
    float ymax2 = 5100.;
    int n_test = 0, nx, ny;

    for(int i = 1; i <= nentries16; i++)
    {
        if (i % 100000 == 0) {cout << i << endl;}        
        stree.GetEntry(i);
        TLorentzVector mu1, mu2, pi1, pi2, K1, K2, p1, p2, jpsi;

        TLorentzVector mu1_11, mu2_11, pi1_11, pi2_11, K1_11, K2_11, p1_11, p2_11, jpsi_11;
        TLorentzVector mu1_12, mu2_12, pi1_12, pi2_12, K1_12, K2_12, p1_12, p2_12, jpsi_12;
        TLorentzVector mu1_13, mu2_13, pi1_13, pi2_13, K1_13, K2_13, p1_13, p2_13, jpsi_13;
        TLorentzVector mu1_14, mu2_14, pi1_14, pi2_14, K1_14, K2_14, p1_14, p2_14, jpsi_14;
        TLorentzVector mu1_15, mu2_15, pi1_15, pi2_15, K1_15, K2_15, p1_15, p2_15, jpsi_15;
        TLorentzVector mu1_16, mu2_16, pi1_16, pi2_16, K1_16, K2_16, p1_16, p2_16, jpsi_16;
        TLorentzVector mu1_17, mu2_17, pi1_17, pi2_17, K1_17, K2_17, p1_17, p2_17, jpsi_17;
        TLorentzVector mu1_18, mu2_18, pi1_18, pi2_18, K1_18, K2_18, p1_18, p2_18, jpsi_18;
        TLorentzVector mu1_19, mu2_19, pi1_19, pi2_19, K1_19, K2_19, p1_19, p2_19, jpsi_19;

        w = 1.;
        mu1.SetXYZM(sB_mu1_px, sB_mu1_py, sB_mu1_pz, 105.65837);
        mu2.SetXYZM(sB_mu2_px, sB_mu2_py, sB_mu2_pz, 105.65837);
        pi1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 139.57);
        pi2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 139.57);
        K1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 493.677);
        K2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 493.677);
        p1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 938.272);
        p2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 938.272);
        jpsi.SetXYZM(sB_mu1_px + sB_mu2_px, sB_mu1_py + sB_mu2_py, sB_mu1_pz + sB_mu2_pz, 3096.);
        TLorentzVector bs = jpsi + pi1 + pi2;

        if (((xmin < (jpsi + K1 + pi2).M() && (jpsi + K1 + pi2).M() < xmax && ymin < (jpsi + K2 + pi1).M() && (jpsi + K2 + pi1).M() < ymax) || (xmin < (jpsi + K1 + pi2).M() && (jpsi + K1 + pi2).M() < xmax && ymax < (jpsi + K2 + pi1).M() && (jpsi + K2 + pi1).M() < ymax2) || (xmax < (jpsi + K1 + pi2).M() && (jpsi + K1 + pi2).M() < xmax2 && ymin < (jpsi + K2 + pi1).M() && (jpsi + K2 + pi1).M() < ymax)) && sB_Bs_pt > 0.25 * sB_MaxSumPt && sB_Lxy_MaxSumPt > 1. && sB_Bs_chi2_ndof < 1.7 && (jpsi + pi1).M() < 4900. && (jpsi + pi2).M() < 4900.) 
        {
            //if (sB_trk1_charge * sB_trk2_charge > 0) {w = -1;}             
            n_test++; 
            mu1_11.SetXYZM(sB_mu1_px * scale_coef, sB_mu1_py * scale_coef, sB_mu1_pz * scale_coef, 105.65837);
            mu2_11.SetXYZM(sB_mu2_px * scale_coef, sB_mu2_py * scale_coef, sB_mu2_pz * scale_coef, 105.65837);
            pi1_11.SetXYZM(sB_trk1_px * scale_coef, sB_trk1_py * scale_coef, sB_trk1_pz * scale_coef, 139.57);
            pi2_11.SetXYZM(sB_trk2_px * scale_coef, sB_trk2_py * scale_coef, sB_trk2_pz * scale_coef, 139.57);
            K1_11.SetXYZM(sB_trk1_px * scale_coef, sB_trk1_py * scale_coef, sB_trk1_pz * scale_coef, 493.677);
            K2_11.SetXYZM(sB_trk2_px * scale_coef, sB_trk2_py * scale_coef, sB_trk2_pz * scale_coef, 493.677);
            p1_11.SetXYZM(sB_trk1_px * scale_coef, sB_trk1_py * scale_coef, sB_trk1_pz * scale_coef, 938.272);
            p2_11.SetXYZM(sB_trk2_px * scale_coef, sB_trk2_py * scale_coef, sB_trk2_pz * scale_coef, 938.272);
            jpsi_11.SetXYZM(sB_mu1_px * scale_coef + sB_mu2_px * scale_coef, sB_mu1_py * scale_coef + sB_mu2_py * scale_coef, sB_mu1_pz * scale_coef + sB_mu2_pz * scale_coef, 3096.);
            if ((K1_11 + pi2_11).M() > 1550. && (pi1_11 + K2_11).M() > 1550. && sB_mu1_pt > 4000. && sB_mu2_pt > 4000. && sB_trk1_pt * scale_coef > 2000. && sB_trk2_pt * scale_coef > 2000. && 4900. < (jpsi + K1_11 + pi2_11).M() && (jpsi + K1_11 + pi2_11).M() < 5700. && (jpsi + pi1_11 + K2_11).M() < 5900.)
            {
                Kpi_piK_2d_data_11->Fill((jpsi + K1_11 + pi2_11).M() * GEV, (jpsi + K2_11 + pi1_11).M() * GEV, w); 
                Kpi_piK_2d_data_13->Fill((jpsi + K1_11 + pi2_11).M() * GEV, (jpsi + K2_11 + pi1_11).M() * GEV, w);
            }
        }
    }
    //f16->Close();
    
    TCanvas * c_Kpi_piK = new TCanvas("c_Kpi_piK");
    c_Kpi_piK->SetLogz();
    Kpi_piK_2d_data_13->SetStats(0);
    Kpi_piK_2d_data_13->Scale(10. / Kpi_piK_2d_data_13->Integral());
    Kpi_piK_2d_data_13->Draw("COLZ");    
    area_hist->Draw("same BOX");
    c_Kpi_piK->SaveAs("Kpi_piK_with_signal_area_shape.png");
    c_Kpi_piK->SaveAs("Kpi_piK_with_signal_area_shape.C");

    double n_lambda, n_bs, n_bd_pipi, n_bd_Kpi, n_bs_KK, p0, p1, p2, p3, a0, a1, a2, b0, b1, b2, d0_KKpipi, p0_KKpipi, p1_KKpipi, p2_KKpipi, p3_KKpipi, a0_KKpipi, a1_KKpipi, a2_KKpipi, b0_KKpipi, b1_KKpipi, b2_KKpipi, x0_KKpipi, comb_bg_norm;

    TFile *f_par = new TFile("../Zc1plus_fit/input/parameters/parameters_signal_Zc1plus_v16_it0_n9.root", "open");
    TVectorD *vpar = (TVectorD*)f_par->Get("parameters");
    
    n_lambda = (*vpar)(80);
    n_bs = (*vpar)(81);
    n_bd_pipi = (*vpar)(82);
    n_bd_Kpi = (*vpar)(83);
    n_bs_KK = (*vpar)(84);
    p0 = (*vpar)(85);
    p1 = (*vpar)(86);
    p2 = (*vpar)(87);
    p3 = (*vpar)(88);
    a0 = (*vpar)(89);
    b0 = (*vpar)(90);
    b1 = (*vpar)(91);
    b2 = (*vpar)(92);
    comb_bg_norm = (*vpar)(213);

    float xx, yy, x, y, xmin_Kpi = 4.9, xmin_piK = 4.65;
    
    float st = 1 / sqrt(2.);

    for(int i = 1; i <= Kpi_piK_2d_bg->ProjectionX()->GetNbinsX(); i++)
    {
        for(int j = 1; j <= Kpi_piK_2d_bg->ProjectionY()->GetNbinsX(); j++)
        {
            xx = Kpi_piK_2d_bg->ProjectionX()->GetBinCenter(i);
            yy = Kpi_piK_2d_bg->ProjectionY()->GetBinCenter(j);
            x = st * xx + st * yy - 6.6;
            y = -st * xx + st * yy;
            if (xmin_Kpi < xx)
            {
                Kpi_piK_2d_bg->SetBinContent(i, j, exp(p0 + x * p1 + pow(x, 2.) * p2 + pow(x, 3.) * p3) * exp(-1. * pow((y + a0) / (b0 + x * b1 + pow(x, 2.) * b2), 2.)));
            }
            else 
            {
                Kpi_piK_2d_bg->SetBinContent(i, j, 0.);
            }
        }
    }
    Kpi_piK_2d_bg->Scale(comb_bg_norm / Kpi_piK_2d_bg->Integral());
    
    cout << "BG is ready" << endl;
    Kpi_piK_2d_bg->Draw("COLZ");
    
    TH1D *B_pt_comb = new TH1D("B_pt_comb", "B_pt_comb", 50, 0., 100.);

    for(int i = 1; i <= nentries16; i++)
    {
        if (i % 100000 == 0) {cout << i << endl;}        
        stree.GetEntry(i);
        TLorentzVector mu1, mu2, pi1, pi2, K1, K2, p1, p2, jpsi;

        TLorentzVector mu1_11, mu2_11, pi1_11, pi2_11, K1_11, K2_11, p1_11, p2_11, jpsi_11, bs_mod;
        TLorentzVector mu1_12, mu2_12, pi1_12, pi2_12, K1_12, K2_12, p1_12, p2_12, jpsi_12;
        TLorentzVector mu1_13, mu2_13, pi1_13, pi2_13, K1_13, K2_13, p1_13, p2_13, jpsi_13;
        

        w = 1.;
        mu1.SetXYZM(sB_mu1_px, sB_mu1_py, sB_mu1_pz, 105.65837);
        mu2.SetXYZM(sB_mu2_px, sB_mu2_py, sB_mu2_pz, 105.65837);
        pi1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 139.57);
        pi2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 139.57);
        K1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 493.677);
        K2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 493.677);
        p1.SetXYZM(sB_trk1_px, sB_trk1_py, sB_trk1_pz, 938.272);
        p2.SetXYZM(sB_trk2_px, sB_trk2_py, sB_trk2_pz, 938.272);
        jpsi.SetXYZM(sB_mu1_px + sB_mu2_px, sB_mu1_py + sB_mu2_py, sB_mu1_pz + sB_mu2_pz, 3096.);
        TLorentzVector bs = jpsi + pi1 + pi2;

        if (((xmin < (jpsi + K1 + pi2).M() && (jpsi + K1 + pi2).M() < xmax && ymin < (jpsi + K2 + pi1).M() && (jpsi + K2 + pi1).M() < ymax) || (xmin < (jpsi + K1 + pi2).M() && (jpsi + K1 + pi2).M() < xmax && ymax < (jpsi + K2 + pi1).M() && (jpsi + K2 + pi1).M() < ymax2) || (xmax < (jpsi + K1 + pi2).M() && (jpsi + K1 + pi2).M() < xmax2 && ymin < (jpsi + K2 + pi1).M() && (jpsi + K2 + pi1).M() < ymax)) && sB_Bs_pt > 0.25 * sB_MaxSumPt && sB_Lxy_MaxSumPt > 1. && sB_Bs_chi2_ndof < 1.7 && (jpsi + pi1).M() < 4900. && (jpsi + pi2).M() < 4900.) 
        {
            //if (sB_trk1_charge * sB_trk2_charge > 0) {w = -1;}             
            n_test++; 
            mu1_11.SetXYZM(sB_mu1_px * scale_coef, sB_mu1_py * scale_coef, sB_mu1_pz * scale_coef, 105.65837);
            mu2_11.SetXYZM(sB_mu2_px * scale_coef, sB_mu2_py * scale_coef, sB_mu2_pz * scale_coef, 105.65837);
            pi1_11.SetXYZM(sB_trk1_px * scale_coef, sB_trk1_py * scale_coef, sB_trk1_pz * scale_coef, 139.57);
            pi2_11.SetXYZM(sB_trk2_px * scale_coef, sB_trk2_py * scale_coef, sB_trk2_pz * scale_coef, 139.57);
            K1_11.SetXYZM(sB_trk1_px * scale_coef, sB_trk1_py * scale_coef, sB_trk1_pz * scale_coef, 493.677);
            K2_11.SetXYZM(sB_trk2_px * scale_coef, sB_trk2_py * scale_coef, sB_trk2_pz * scale_coef, 493.677);
            p1_11.SetXYZM(sB_trk1_px * scale_coef, sB_trk1_py * scale_coef, sB_trk1_pz * scale_coef, 938.272);
            p2_11.SetXYZM(sB_trk2_px * scale_coef, sB_trk2_py * scale_coef, sB_trk2_pz * scale_coef, 938.272);
            TLorentzVector bs_mod = jpsi + K1_11 + pi2_11;
            
            jpsi_11.SetXYZM(sB_mu1_px * scale_coef + sB_mu2_px * scale_coef, sB_mu1_py * scale_coef + sB_mu2_py * scale_coef, sB_mu1_pz * scale_coef + sB_mu2_pz * scale_coef, 3096.);
            if ((K1_11 + pi2_11).M() > 1550. && (pi1_11 + K2_11).M() > 1550. && sB_mu1_pt > 4000. && sB_mu2_pt > 4000. && sB_trk1_pt * scale_coef > 2000. && sB_trk2_pt * scale_coef > 2000. && 4900. < (jpsi + K1_11 + pi2_11).M() && (jpsi + K1_11 + pi2_11).M() < 5700. && (jpsi + pi1_11 + K2_11).M() < 5900.)
            {
                nx = Kpi_piK_2d_data_11->ProjectionX()->FindBin((jpsi + K1_11 + pi2_11).M() * GEV); 
                ny = Kpi_piK_2d_data_11->ProjectionY()->FindBin((jpsi + K2_11 + pi1_11).M() * GEV);  
                if (area_hist->GetBinContent(nx, ny) > 0.)
                {  
                    if (Kpi_piK_2d_data_11->GetBinContent(nx, ny) != 0.) {                    
                    w = w / Kpi_piK_2d_data_11->GetBinContent(nx, ny) * Kpi_piK_2d_bg->GetBinContent(nx, ny);}      
                    else {cout << "fail" << endl;}
                    Kpi_piK_2d_data_12->Fill((jpsi + K1_11 + pi2_11).M() * GEV, (jpsi + K2_11 + pi1_11).M() * GEV, w);
                    Jpsipi_2d->Fill((jpsi + pi1_11).M() * GEV, (jpsi + pi2_11).M() * GEV, w);
                    JpsiK_2d->Fill((jpsi + K1_11).M() * GEV, (jpsi + K2_11).M() * GEV, w);
                    Kpi_2d->Fill((K1_11 + pi2_11).M() * GEV, (K2_11 + pi1_11).M() * GEV, w);
                    cos_theta_Zc_bg->Fill(abs(cos(theta_X(jpsi + K1_11 + pi2_11, jpsi + pi2_11, jpsi))), abs(cos(theta_X(jpsi + K2_11 + pi1_11, jpsi + pi1_11, jpsi))), w);
                    phi_K_bg->Fill(phi_K(jpsi + K1_11 + pi2_11, K1_11 + pi2_11, K1_11, jpsi), phi_K(jpsi + K2_11 + pi1_11, K2_11 + pi1_11, K2_11, jpsi), w);
                    alpha_mu_bg->Fill(alpha_mu(mu2, jpsi, jpsi + pi2_11, jpsi + K1_11 + pi2_11), alpha_mu(mu2, jpsi, jpsi + pi1_11, jpsi + K2_11 + pi1_11), w);
                    phi_mu_Kstar_bg->Fill(phi_mu_Kstar(jpsi + K1_11 + pi2_11, K1_11 + pi2_11, jpsi, mu2), phi_mu_Kstar(jpsi + K2_11 + pi1_11, K2_11 + pi1_11, jpsi, mu2), w);
                    phi_mu_X_bg->Fill(phi_mu_X(jpsi + pi2_11, jpsi + pi2_11 + K1_11, jpsi, mu2), phi_mu_X(jpsi + pi1_11, jpsi + pi1_11 + K2_11, jpsi, mu2), w);
                    cos_theta_psi_X_bg->Fill(abs(cos(theta_psi_X(jpsi + pi2_11, jpsi, mu2))), abs(cos(theta_psi_X(jpsi + pi1_11, jpsi, mu2))), w);
                    cos_theta_Kstar_bg->Fill(abs(cos(theta_Kstar(jpsi + K1_11 + pi2_11, K1_11 + pi2_11, K1_11))), abs(cos(theta_Kstar(jpsi + K2_11 + pi1_11, K2_11 + pi1_11, K2_11))), w);
                    cos_theta_psi_Kstar_bg->Fill(abs(cos(theta_psi_Kstar(jpsi + K1_11 + pi2_11, jpsi, mu2))), abs(cos(theta_psi_Kstar(jpsi + K2_11 + pi1_11, jpsi, mu2))), w);
                    cos_theta_B0_bg->Fill(abs(cos(theta_B0(jpsi + K1_11 + pi2_11, K1_11 + pi2_11))), abs(cos(theta_B0(jpsi + K2_11 + pi1, K2_11 + pi1_11))), w);
                    phi_psi_bg->Fill(phi_psi(jpsi + K1_11 + pi2_11, jpsi + pi2_11, jpsi), phi_psi(jpsi + K2_11 + pi1_11, jpsi + pi1_11, jpsi), w);
                    trk_pt_2d_bg->Fill(sB_trk1_pt * scale_coef * GEV, sB_trk2_pt * scale_coef * GEV, w);
                    B_pt_comb->Fill(bs_mod.Pt() * GEV, w);
                }
            }
       
        }
    }
    
    TFile *f_out = new TFile("signal_area_bg.root", "recreate");
    Jpsipi_2d->Write();
    JpsiK_2d->Write();
    Kpi_2d->Write();
    trk_pt_2d_bg->Write();
    f_out->Close();
    
    TFile *f_out_cos =new TFile("cos_signal_bg.root", "recreate");
    cos_theta_Zc_bg->Write();
    phi_K_bg->Write();
    alpha_mu_bg->Write();
    phi_mu_Kstar_bg->Write();
    phi_mu_X_bg->Write();
    cos_theta_psi_X_bg->Write();
    cos_theta_Kstar_bg->Write();
    cos_theta_psi_Kstar_bg->Write();
    cos_theta_B0_bg->Write();
    phi_psi_bg->Write();
    f_out_cos->Close();
    
    //__oO0Oo____oO0Oo____oO0Oo____oO0Oo__|| pictures to test result ||__oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
    
    TCanvas * c_JpsipiX = new TCanvas("jpsipi_mass_ProjectionX"); 
    Jpsipi_2d->ProjectionX()->Draw();
    c_JpsipiX->SaveAs("Jpsipi_X_SA_bg.png");
    
    TCanvas * c_JpsipiY = new TCanvas("jpsipi_mass_ProjectionY");
    Jpsipi_2d->ProjectionY()->Draw();
    c_JpsipiY->SaveAs("Jpsipi_Y_SA_bg.png");
    
    TCanvas * c_JpsiKX = new TCanvas("jpsiK_mass_ProjectionX");
    JpsiK_2d->ProjectionX()->Draw();
    c_JpsiKX->SaveAs("JpsiK_X_SA_bg.png");
    
    TCanvas * c_JpsiKY = new TCanvas("jpsiK_mass_ProjectionY");
    JpsiK_2d->ProjectionY()->Draw();
    c_JpsiKY->SaveAs("JpsiK_Y_SA_bg.png");
    
    TCanvas * c_KpiX = new TCanvas("Kpi_mass_ProjectionX");
    Kpi_2d->ProjectionX()->Draw();
    c_KpiX->SaveAs("Kpi_X_SA_bg.png");
    
    TCanvas * c_KpiY = new TCanvas("Kpi_mass_ProjectionY");
    Kpi_2d->ProjectionY()->Draw();
    c_KpiY->SaveAs("Kpi_Y_SA_bg.png");
    
    //__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
    
    TFile *f_out_B_pt =new TFile("../B_pt_studies/B_pt_comb.root", "recreate");
    B_pt_comb->Write();
    f_out_B_pt->Close();

    //cout << "n_test " << n_test << endl;
}
