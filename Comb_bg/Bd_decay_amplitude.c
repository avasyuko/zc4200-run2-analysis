/*std::complex<float> breit_wigner(float m0, float m, float g0, int l, float d, float p, float p0) //counts relativistic Breit-Wigner amplitude
{
	std::complex<float> z1(m * sqrt(m0 * G_width(p, p0, d, l, m0, m, g0)), 0);
	std::complex<float> z2(m0 * m0 - m * m, -1. * G_width(p, p0, d, l, m0, m, g0) * m0);
	std::complex<float> r;
	r = z1 / z2;
	//cout << "z2 = " << z2 << endl;
	return r;
}*/

float Wigner_matrix(float a, float  b, float c, double fi) //counts Wigner d-matrix(0.5; 1; 1.5; 2.5 (not full b = 1.5|0.5|-0.5|-1.5); 3.5(not full b = 1.5|0.5|-0.5|-1.5))
{
	int t;
	float f = 0.;
	switch (t = (int)nearbyint(a * 2))
	{
	case 0:
	{	
		switch (t = (int)nearbyint(2 * b))
			  {
			  case 0:
			  {
						switch (t = (int)nearbyint(c * 2))
						{
						case 0:
						{
							f = 1.;
								   break;
						}
						}
						break;
			  }
			  }
			break;	
	}
	case 1:
	{
			  switch (t = (int)nearbyint(2 * b))
			  {
			  case 1:
			  {
						switch (t = (int)nearbyint(c * 2))
						{
						case -1:
						{
							f = -sin(fi / 2.);
								   break;
						}
						case 1:
						{
							f = cos(fi / 2.);
								  break;
						}
						default:
						{
													
							if ((f == 0) && (b != c)) { f = (float)pow(-1, b - c) * Wigner_matrix(a, c, b, fi); }
							if (f == 0) { f = (float)pow(-1, b - c) * Wigner_matrix(a, -b, -c, fi); }
							if (f == 0) { f = Wigner_matrix(a, -c, -b, fi); }	
						}
						}
						break;
			  }
			  default:
			  {					
					if ((f == 0) && (b != c)) { f = (float)pow(-1, b - c) * Wigner_matrix(a, c, b, fi); }
					if (f == 0) { f = (float)pow(-1, b - c) * Wigner_matrix(a, -b, -c, fi); }
					if (f == 0) { f = Wigner_matrix(a, -c, -b, fi); }		
			  }
			  }
			  break;
	}
	case 2:
	{
			  switch (t = (int)nearbyint(b))
			  {
			  case -1:
			  {
						switch (t = (int)nearbyint(c * 2))
						{
						case -2:
						{
								  f = (1. + cos(fi)) / 2.;
								  break;
						}
						case 0:
						{
								  f = -sin(fi) / sqrt(2.);
								  break;
						}
						case 2:
						{
								  f = (1. - cos(fi)) / 2.;
								  break;
						}
						default:
						{
													
							if ((f == 0) && (b != c)) { f = (float)pow(-1, b - c) * Wigner_matrix(a, c, b, fi); }
							if (f == 0) { f = (float)pow(-1, b - c) * Wigner_matrix(a, -b, -c, fi); }
							if (f == 0) { f = Wigner_matrix(a, -c, -b, fi); }	
						}
						}
						break;

			  }
			  case 0:
			  {
						switch (t = (int)nearbyint(c * 2))
						{
						case -2:
						{
								  f = -sin(fi) / sqrt(2.);
								  break;
						}
						case 0:
						{
								  f = cos(fi);
								  break;
						}
						case 2:
						{
								  f = sin(fi) / sqrt(2.);
								  break;
						}
						default:
						{
													
							if ((f == 0) && (b != c)) { f = (float)pow(-1, b - c) * Wigner_matrix(a, c, b, fi); }
							if (f == 0) { f = (float)pow(-1, b - c) * Wigner_matrix(a, -b, -c, fi); }
							if (f == 0) { f = Wigner_matrix(a, -c, -b, fi); }	
						}
						}
						break;
			  }
			  case 1:
			  {
						switch (t = (int)nearbyint(c * 2))
						{
						case -2:
						{
								   f = (1. - cos(fi)) / 2.;
								   break;
						}
						case 0:
						{
								  f = -sin(fi) / sqrt(2.);
								  break;
						}
						case 2:
						{
								  f = (1. + cos(fi)) / 2.;
								  break;
						}
						default:
						{
													
							if ((f == 0) && (b != c)) { f = (float)pow(-1, b - c) * Wigner_matrix(a, c, b, fi); }
							if (f == 0) { f = (float)pow(-1, b - c) * Wigner_matrix(a, -b, -c, fi); }
							if (f == 0) { f = Wigner_matrix(a, -c, -b, fi); }	
						}
						}
						break;
			  }
				default:
						{
													
							if ((f == 0) && (b != c)) { f = (float)pow(-1, b - c) * Wigner_matrix(a, c, b, fi); }
							if (f == 0) { f = (float)pow(-1, b - c) * Wigner_matrix(a, -b, -c, fi); }
							if (f == 0) { f = Wigner_matrix(a, -c, -b, fi); }	
						}
			  }
			  break;
	}
	case 4:
	{
			switch (t = (int)nearbyint(b))
			  {
			  case 0:
			  {
						switch (t = (int)nearbyint(c * 2))
						{
						case -2:
						{
								  f = -sqrt(6.) / 3. * pow(cos(fi / 2.), 5.) * pow(sin(fi / 2.), 1.) + sqrt(6.) / 2. * pow(cos(fi / 2.), 3.) * pow(sin(fi / 2.), 3.);
								  break;
						}
						case 0:
						{
								  f = pow(cos(fi / 2.), 4.);
								  break;
						}
						case 2:
						{
								  f = sqrt(6.) / 3. * pow(cos(fi / 2.), 5.) * pow(sin(fi / 2.), 1.) - sqrt(6.) / 2. * pow(cos(fi / 2.), 3.) * pow(sin(fi / 2.), 3.);
								  break;
						}
						}
						break;
			  }
			}
			break;	
	}
	case 6:
	{
			switch (t = (int)nearbyint(b))
			  {
			  case 0:
			  {
						switch (t = (int)nearbyint(c * 2))
						{
						case -2:
						{
								  f = - 2. * sqrt(3.) * pow(cos(fi / 2.), 7.) * pow(sin(fi / 2.), 1.) + 6 * sqrt(3.) * pow(cos(fi / 2.), 5.) * pow(sin(fi / 2.), 3.) - 2. * sqrt(3.) * pow(cos(fi / 2.), 3.) * pow(sin(fi / 2.), 5.);
								  break;
						}
						case 0:
						{
								  f = pow(cos(fi / 2.), 6.);
								  break;
						}
						case 2:
						{
								  f = 2. * sqrt(3.) * pow(cos(fi / 2.), 7.) * pow(sin(fi / 2.), 1.) - 6 * sqrt(3.) * pow(cos(fi / 2.), 5.) * pow(sin(fi / 2.), 3.) + 2. * sqrt(3.) * pow(cos(fi / 2.), 3.) * pow(sin(fi / 2.), 5.);
								  break;
						}
						}
						break;
			  }
			}
			break;	
	}

	default:
	{
		//cout << "forbidden value a = " << a << endl;;
	}
	}
	//cout << "Wigner_matrix = " << f << endl;
	return f;
}

std::complex<float> make_complex(float a, float p) //this function construct complex type using amplitude and phase
{
    return std::complex<float>(a * cos(p), a * sin(p));
}

double BlattWeisskopf(double p0, double p1, double d, int L)
{

	if(L==0) return 1.0;

	float c1 = p0*d*p0*d;
	float c2 = p1*d*p1*d;
	
	if(L==1) return sqrt((1 + c1)/(1 + c2));
	
	if(L==2) return sqrt((9.0 + 3.0*c1 + c1*c1)/(9.0 + 3.0*c2 + c2*c2));
	
	if(L==3) return sqrt((225.0 + 45.0*c1 + 6.0*c1*c1 + c1*c1*c1)/(225.0 + 45.0*c2 + 6.0*c2*c2 + c2*c2*c2));
	
	float c14 = c1*c1*c1*c1;
	float c24 = c2*c2*c2*c2;
		
	if(L==4) return sqrt(   ( 11025.0 + 1575.0*c1 + 135.0*c1*c1 + 10.0*c1*c1*c1 + c14 )/
				( 11025.0 + 1575.0*c2 + 135.0*c2*c2 + 10.0*c1*c1*c1 + c24 ) );
	
	if(L==5) return sqrt(   (893025.0 + 99225.0*c1 + 6300.0*c1*c1 + 315.0*c1*c1*c1 + 15.0*c14 + c14*c1)/
			        (893025.0 + 99225.0*c2 + 6300.0*c2*c2 + 315.0*c2*c2*c2 + 15.0*c24 + c24*c2) );
	return 0.0;


}

std::complex<float> BreitWignerX( float MX, float massB, float M0X, float gamma0, int LX )
{
std::complex<float> ix(0,1);
const float massProton = 0.938272;
const float massJpsi = 3.096916;
const float massKaon = 0.493667;
const float massPion = 0.13957;
//const float massLambdaB = MASS_LB;
//massB = MASS_BD;
float mplusX = massPion + massJpsi; /// mass JPsi + mass proton
float spinX = 1.;
//if(M0X < mplusX) return 0.0;
//if(MX < mplusX)  return 0.0;
//float q0 = sqrt(M0X*M0X - mplusX*mplusX); 
//float q1 = sqrt(MX*MX - mplusX*mplusX);


float argq0 = pow( M0X*M0X - massJpsi*massJpsi - massPion*massPion, 2.0 ) - 4.0 * massJpsi*massJpsi*massPion*massPion;
float argq1 = pow( MX*MX - massJpsi*massJpsi - massPion*massPion, 2.0 ) - 4.0 * massJpsi*massJpsi*massPion*massPion;
if(argq0 < 0.0 || argq1 < 0.0) return 0.0;
float q0 = sqrt(argq0)/2.0/M0X;
float q1 = sqrt(argq1)/2.0/MX;


float argp0 = pow( massB*massB - M0X*M0X - massKaon*massKaon, 2.0 ) - 4.0 * M0X*M0X*massKaon*massKaon;
float argp1 = pow( massB*massB - MX*MX - massKaon*massKaon, 2.0 ) - 4.0 * MX*MX*massKaon*massKaon;
if(argp0 < 0.0 || argp1 < 0.0) return 0.0;
float p0 = sqrt(argp0)/2.0/massB;
float p1 = sqrt(argp1)/2.0/massB;

float gamma = gamma0 * pow( q1/q0, 2*LX + 1) * M0X / MX * powf(BlattWeisskopf(q0, q1, 3.0, LX), 2.0);

float nom = 1.0;
//if(BW_nom_X==1) nom = MX*sqrt(M0X*gamma); //commented
std::complex<float> denom = nom/( M0X*M0X - MX*MX - ix*M0X*gamma );
///std::complex<float> Rm = denom * BlattWeisskopf(q0, q1, 3.0, LX) * pow( q1/M0X, LX );

const int LB = spinX;
std::complex<float> Rm = (float)(BlattWeisskopf(p0, p1, 3.0, LB) * pow( p1/massB, LB )) * denom * (float)(BlattWeisskopf(q0, q1, 3.0, LX) * pow( q1/M0X, LX ));

return Rm;
}

std::complex<float> BreitWignerX1( float MX, float massB, float M0X, float gamma0, int LX )
{
std::complex<float> ix(0,1);
const float massProton = 0.938272;
const float massJpsi = 3.096916;
const float massKaon = 0.493667;
const float massPion = 0.13957;
//const float massLambdaB = MASS_LB;
//massB = MASS_BD;
float mplusX = massPion + massJpsi; /// mass JPsi + mass proton
float spinX = 1.;
//if(M0X < mplusX) return 0.0;
//if(MX < mplusX)  return 0.0;
//float q0 = sqrt(M0X*M0X - mplusX*mplusX); 
//float q1 = sqrt(MX*MX - mplusX*mplusX);


float argq0 = pow( M0X*M0X - massJpsi*massJpsi - massPion*massPion, 2.0 ) - 4.0 * massJpsi*massJpsi*massPion*massPion;
float argq1 = pow( MX*MX - massJpsi*massJpsi - massPion*massPion, 2.0 ) - 4.0 * massJpsi*massJpsi*massPion*massPion;
if(argq0 < 0.0 || argq1 < 0.0) return 0.0;
float q0 = sqrt(argq0)/2.0/M0X;
float q1 = sqrt(argq1)/2.0/MX;


float argp0 = pow( massB*massB - M0X*M0X - massKaon*massKaon, 2.0 ) - 4.0 * M0X*M0X*massKaon*massKaon;
float argp1 = pow( massB*massB - MX*MX - massKaon*massKaon, 2.0 ) - 4.0 * MX*MX*massKaon*massKaon;
if(argp0 < 0.0 || argp1 < 0.0) return 0.0;
float p0 = sqrt(argp0)/2.0/massB;
float p1 = sqrt(argp1)/2.0/massB;

float gamma = gamma0 * pow( q1/q0, 2*LX + 1) * M0X / MX * powf(BlattWeisskopf(q0, q1, 3.0, LX), 2.0);

float nom = 1.0;
//if(BW_nom_X==1) nom = MX*sqrt(M0X*gamma); //commented
std::complex<float> denom = nom/( M0X*M0X - MX*MX - ix*M0X*gamma );
///std::complex<float> Rm = denom * BlattWeisskopf(q0, q1, 3.0, LX) * pow( q1/M0X, LX );

const int LB = spinX;
std::complex<float> Rm = (float)(BlattWeisskopf(p0, p1, 3.0, LB) * pow( p1/massB, LB )) * denom * (float)(BlattWeisskopf(q0, q1, 3.0, LX) * pow( q1/M0X, LX ));

return Rm;
}

std::complex<float> BreitWignerX2( float MX, float massB, float M0X, float gamma0, int LX )
{
std::complex<float> ix(0,1);
const float massProton = 0.938272;
const float massJpsi = 3.096916;
const float massKaon = 0.493667;
const float massPion = 0.13957;
//const float massLambdaB = MASS_LB;
//massB = MASS_BD;
float mplusX = massPion + massJpsi; /// mass JPsi + mass proton
float spinX = 2.;
//if(M0X < mplusX) return 0.0;
//if(MX < mplusX)  return 0.0;
//float q0 = sqrt(M0X*M0X - mplusX*mplusX); 
//float q1 = sqrt(MX*MX - mplusX*mplusX);


float argq0 = pow( M0X*M0X - massJpsi*massJpsi - massPion*massPion, 2.0 ) - 4.0 * massJpsi*massJpsi*massPion*massPion;
float argq1 = pow( MX*MX - massJpsi*massJpsi - massPion*massPion, 2.0 ) - 4.0 * massJpsi*massJpsi*massPion*massPion;
if(argq0 < 0.0 || argq1 < 0.0) return 0.0;
float q0 = sqrt(argq0)/2.0/M0X;
float q1 = sqrt(argq1)/2.0/MX;


float argp0 = pow( massB*massB - M0X*M0X - massKaon*massKaon, 2.0 ) - 4.0 * M0X*M0X*massKaon*massKaon;
float argp1 = pow( massB*massB - MX*MX - massKaon*massKaon, 2.0 ) - 4.0 * MX*MX*massKaon*massKaon;
if(argp0 < 0.0 || argp1 < 0.0) return 0.0;
float p0 = sqrt(argp0)/2.0/massB;
float p1 = sqrt(argp1)/2.0/massB;

float gamma = gamma0 * pow( q1/q0, 2*LX + 1) * M0X / MX * powf(BlattWeisskopf(q0, q1, 3.0, LX), 2.0);

float nom = 1.0;
//if(BW_nom_X==1) nom = MX*sqrt(M0X*gamma); //commented
std::complex<float> denom = nom/( M0X*M0X - MX*MX - ix*M0X*gamma );
///std::complex<float> Rm = denom * BlattWeisskopf(q0, q1, 3.0, LX) * pow( q1/M0X, LX );

const int LB = spinX;
std::complex<float> Rm = (float)(BlattWeisskopf(p0, p1, 3.0, LB) * pow( p1/massB, LB )) * denom * (float)(BlattWeisskopf(q0, q1, 3.0, LX) * pow( q1/M0X, LX ));

return Rm;
}

std::complex<float> BreitWignerKstar( float MK, float M0K, float massB, float gamma0, int LK, int LB )
{
std::complex<float> ix(0,1);
const float massProton = 0.938272;
const float massJpsi = 3.096916;
const float massKaon = 0.493667;
const float massPion = 0.13957;
//const float massLambdaB = MASS_LB;
//const float massB = MASS_BD;
const float mplusK = massPion + massKaon; /// mass JPsi + mass proton

//massB = MASS_BD;

float argq0 = pow( M0K*M0K - massPion*massPion - massKaon*massKaon, 2.0 ) - 4.0 * massPion*massPion*massKaon*massKaon;
float argq1 = pow( MK*MK - massPion*massPion - massKaon*massKaon, 2.0 ) - 4.0 * massPion*massPion*massKaon*massKaon;
if(argq0 < 0.0 || argq1 < 0.0) return 0.0;
float q0 = sqrt(argq0)/2.0/M0K;
float q1 = sqrt(argq1)/2.0/MK;


float argp0 = pow( massB*massB - M0K*M0K - massJpsi*massJpsi, 2.0 ) - 4.0 * M0K*M0K*massJpsi*massJpsi;
float argp1 = pow( massB*massB - MK*MK - massJpsi*massJpsi, 2.0 ) - 4.0 * MK*MK*massJpsi*massJpsi;
if(argp0 < 0.0 || argp1 < 0.0) return 0.0;
float p0 = sqrt(argp0)/2.0/massB;
float p1 = sqrt(argp1)/2.0/massB;


float gamma = gamma0 * pow( q1/q0, 2*LK + 1) * M0K / MK * powf(BlattWeisskopf(q0, q1, 3.0, LK), 2.0);

if(gamma != gamma ){printf("in BWkstar gamma is nan"); getchar();}

float nom = 1.0;
//if(BW_nom_Kstar==1) nom = MK*sqrt(M0K*gamma); //commented
std::complex<float> denom = nom/( M0K*M0K - MK*MK - ix*M0K*gamma );

float BlWsLB = BlattWeisskopf(p0, p1, 3.0, LB);
float BlWsLK = BlattWeisskopf(q0, q1, 3.0, LK);

if(BlWsLB != BlWsLB) { printf("blatt LB is NaN, p0=%10.2e\tp1=%10.2e\tLB=%d", p0, p1, LB); getchar(); }
if(BlWsLK != BlWsLK) { printf("blatt LK is NaN, q0=%10.2e\tq1=%10.2e\tLK=%d", q0, q1, LK); getchar(); }

std::complex<float> Rm = (float)(BlWsLB*pow( p1/massB, LB ))*denom*(float)(BlWsLK*pow( q1/M0K, LK ));

/*
if(Rm != Rm){
printf("\nBW: %10.2e ", abs(denom));
printf("%10.1e ", BlattWeisskopf(p0, p1, 3.0, LB));
printf("%10.1e ", BlattWeisskopf(q0, q1, 3.0, LK));
printf("%10.1e ", pow( p1/massB, LB ) );
printf("%10.1e ", pow( q1/M0K, LK ) );
printf("%10.4e ", massB );
printf("%10.4e ", mplusB1 );
printf("%10.4e ", MK );
getchar();
}*/

return Rm;
}

std::complex<float> HamplitudeX1plus(float M, float massB, float M0, float gamma, std::complex<float> Bs1, std::complex<float> Bs2, float hmu, float thetaX, float thetaPsi, float phimu, float alpha_mu )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);

std::complex<float> ealpha = cosf(hmu*alpha_mu) + ix * sinf(hmu*alpha_mu);

std::complex<float> Hs1, Hs2, Hs3;

std::complex<float> Ampl1;
Ampl1 = 0.0;

float spinX = 1.;
int parityX = 1;

/////spinX 2
	if(spinX == 0 && parityX == -1){
	Hs1 = -Bs1*BreitWignerX( M, massB, M0, gamma, 1);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi);		

	}

	if(spinX == 1 && parityX == 1){
	Hs1 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerX1( M, massB, M0, gamma, 0) - (float)(sqrt(2.0/3.0))*Bs2*BreitWignerX1( M, massB, M0, gamma, 2);
	Hs2 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerX1( M, massB, M0, gamma, 0) + (float)(sqrt(1.0/6.0))*Bs2*BreitWignerX1( M, massB, M0, gamma, 2);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0,-1.0, thetaX) * emu2;

	}
	if(spinX == 2 && parityX == -1){
	Hs1 = (float)(sqrt(2.0/5.0))*Bs1*BreitWignerX( M, massB, M0, gamma, 1) - (float)(sqrt(3.0/5.0))*Bs2*BreitWignerX( M, massB, M0, gamma, 3);
	Hs2 = (float)(sqrt(3.0/10.0))*Bs1*BreitWignerX( M, massB, M0, gamma, 1) + (float)(sqrt(1.0/5.0))*Bs2*BreitWignerX( M, massB, M0, gamma, 3);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}
	
	if(spinX == 2 && parityX == 1){
	Hs2 = (float)(-sqrt(0.5))*Bs1*BreitWignerX( M, massB, M0, gamma, 2);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 -= Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}

	////Ampl1 *= (float)B0Factor( M, M0, spinX );


return Ampl1*ealpha;
}

std::complex<float> HamplitudeX2plus(float M, float massB, float M0, float gamma, std::complex<float> Bs1, std::complex<float> Bs2, float hmu, float thetaX, float thetaPsi, float phimu, float alpha_mu )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);

std::complex<float> ealpha = cosf(hmu*alpha_mu) + ix * sinf(hmu*alpha_mu);

std::complex<float> Hs1, Hs2, Hs3;

std::complex<float> Ampl1;
Ampl1 = 0.0;

float spinX = 2.;
int parityX = 1;

/////spinX 2
	if(spinX == 0 && parityX == -1){
	Hs1 = -Bs1*BreitWignerX( M, massB, M0, gamma, 1);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi);		

	}

	if(spinX == 1 && parityX == 1){
	Hs1 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerX1( M, massB, M0, gamma, 0) - (float)(sqrt(2.0/3.0))*Bs2*BreitWignerX1( M, massB, M0, gamma, 2);
	Hs2 = (float)(1.0/sqrt(3.0))*Bs1*BreitWignerX1( M, massB, M0, gamma, 0) + (float)(sqrt(1.0/6.0))*Bs2*BreitWignerX1( M, massB, M0, gamma, 2);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(1.0, 0.0,-1.0, thetaX) * emu2;

	}
	if(spinX == 2 && parityX == -1){
	Hs1 = (float)(sqrt(2.0/5.0))*Bs1*BreitWignerX2( M, massB, M0, gamma, 1) - (float)(sqrt(3.0/5.0))*Bs2*BreitWignerX2( M, massB, M0, gamma, 3);
	Hs2 = (float)(sqrt(3.0/10.0))*Bs1*BreitWignerX2( M, massB, M0, gamma, 1) + (float)(sqrt(1.0/5.0))*Bs2*BreitWignerX2( M, massB, M0, gamma, 3);
		Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 0.0, thetaX);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}
	
	if(spinX == 2 && parityX == 1){
	Hs2 = (float)(-sqrt(0.5))*Bs1*BreitWignerX2( M, massB, M0, gamma, 2);
		Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 1.0, thetaX) * emu1;
		Ampl1 -= Hs2 * (float)Wigner_matrix(1.0,-1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0,-1.0, thetaX) * emu2;
	}

	////Ampl1 *= (float)B0Factor( M, M0, spinX );


return Ampl1*ealpha;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::complex<float> HamplitudeKstar0(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);

std::complex<float> BrWig1 = BreitWignerKstar( M, M0, MB, gamma, 1, 0);

if(BrWig1 != BrWig1){
printf("BrWig1 Kstar0 is nan");
printf("%10.2e %10.2e %10.2e %10.2e", M, M0, MB, gamma);
getchar();
}

std::complex<float>	Hs1 = -Bw1*BrWig1;

std::complex<float> Ampl1 = 0.0;

Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi);

return Ampl1;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*std::complex<float> HamplitudeKstarNR0(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);

std::complex<float> BrWig1 = BreitWignerKstarNR( M, M0, MB, gamma, 1, 0);

if(BrWig1 != BrWig1){
printf("BrWig1 Kstar0 is nan");
printf("%10.2e %10.2e %10.2e %10.2e", M, M0, MB, gamma);
getchar();
}

std::complex<float>	Hs1 = -Bw1*BrWig1;

std::complex<float> Ampl1 = 0.0;

Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi);

return Ampl1;
}*/



std::complex<float> HamplitudeKstar1(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK )
{


if(hmu != hmu || thetaKstar != thetaKstar || thetaPsi != thetaPsi || phimu != phimu || phiK != phiK)
{printf("input is nan"); getchar(); }

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);
std::complex<float> eK1 = cos(phiK) + ix*sin(phiK);//exp(ix*phiK);
std::complex<float> eK2 = cos(phiK) - ix*sin(phiK);

std::complex<float> BrWig1 = BreitWignerKstar( M, M0, MB, gamma, 0, 1);
std::complex<float> BrWig2 = BreitWignerKstar( M, M0, MB, gamma, 1, 1);
std::complex<float> BrWig3 = BreitWignerKstar( M, M0, MB, gamma, 2, 1);

if(BrWig1 != BrWig1 || BrWig2 != BrWig2 || BrWig3 != BrWig3){
printf("BrWig123 Kstar1 is nan");
printf("%10.2e %10.2e %10.2e %10.2e", M, M0, MB, gamma);
getchar();
}

std::complex<float>	Hs1 = -(float)(sqrt(1.0/3.0))*Bw1*BrWig1 + (float)(sqrt(2.0/3.0))*Bw3*BrWig3;
std::complex<float>	Hs2 = (float)(sqrt(1.0/3.0))*Bw1*BrWig1 - (float)(sqrt(1.0/2.0))*Bw2*BrWig2 + (float)(sqrt(1.0/6.0))*Bw3*BrWig3;
std::complex<float>	Hs3 = (float)(sqrt(1.0/3.0))*Bw1*BrWig1 + (float)(sqrt(1.0/2.0))*Bw2*BrWig2 + (float)(sqrt(1.0/6.0))*Bw3*BrWig3;


if(Bw1 != Bw1 || Bw2 != Bw2 || Bw3 != Bw3)
{
printf("Bw is nan"); getchar();
}

if(Hs1 != Hs1 || Hs2 != Hs2 || Hs3 != Hs3)
{
printf("Hs is nan"); getchar();
}

float wmpsi1 = (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi);
float wmpsi2 = (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi);
float wmpsi3 = (float)Wigner_matrix(1.0, -1.0, hmu, thetaPsi);
float wmkst1 = (float)Wigner_matrix(1.0, 0.0, 0.0, thetaKstar);
float wmkst2 = (float)Wigner_matrix(1.0, 1.0, 0.0, thetaKstar);
float wmkst3 = (float)Wigner_matrix(1.0, -1.0, 0.0, thetaKstar);

if(wmpsi1 != wmpsi1 || wmpsi2 != wmpsi2 || wmpsi3 != wmpsi3)
{printf("wmpsi is nan"); getchar(); }
if(wmkst1 != wmkst1 || wmkst2 != wmkst2 || wmkst3 != wmkst3)
{printf("wmkst is nan"); getchar(); }


std::complex<float> Ampl1 = 0.0;
Ampl1 += Hs1 * wmpsi1 * wmkst1;
Ampl1 += Hs2 * wmpsi2 * wmkst2 * emu1 * eK1;
Ampl1 += Hs3 * wmpsi3 * wmkst3 * emu2 * eK2;


return Ampl1;
}

std::complex<float> HamplitudeKstar2(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);
std::complex<float> eK1 = cos(phiK) + ix*sin(phiK);//exp(ix*phiK);
std::complex<float> eK2 = cos(phiK) - ix*sin(phiK);

std::complex<float> BrWig1 = BreitWignerKstar( M, M0, MB, gamma, 1, 2);
std::complex<float> BrWig2 = BreitWignerKstar( M, M0, MB, gamma, 2, 2);
std::complex<float> BrWig3 = BreitWignerKstar( M, M0, MB, gamma, 3, 2);

if(BrWig1 != BrWig1 || BrWig2 != BrWig2 || BrWig3 != BrWig3){
printf("BrWig123 Kstar2 is nan");
printf("%10.2e %10.2e %10.2e %10.2e", M, M0, MB, gamma);
getchar();
}

std::complex<float>	Hs1 = (float)(sqrt(2.0/5.0))*Bw1*BrWig1 - (float)(sqrt(3.0/5.0))*Bw3*BrWig3;
std::complex<float>	Hs2 = -(float)(sqrt(3.0/10.0))*Bw1*BrWig1 + (float)(sqrt(1.0/2.0))*Bw2*BrWig2 - (float)(sqrt(1.0/5.0))*Bw3*BrWig3;
std::complex<float>	Hs3 = -(float)(sqrt(3.0/10.0))*Bw1*BrWig1 - (float)(sqrt(1.0/2.0))*Bw2*BrWig2 - (float)(sqrt(1.0/5.0))*Bw3*BrWig3;


std::complex<float> Ampl1 = 0.0;
Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 0.0, 0.0, thetaKstar);
Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, 1.0, 0.0, thetaKstar) * emu1 * eK1;
Ampl1 += Hs3 * (float)Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * (float)Wigner_matrix(2.0, -1.0, 0.0, thetaKstar) * emu2 * eK2;

if(Ampl1 != Ampl1){ 
	printf("BWL1 %10.2e\n", abs(BreitWignerKstar( M, M0, MB, gamma, 1, 2)) ); 
	printf("BWL3 %10.2e\n", abs(BreitWignerKstar( M, M0, MB, gamma, 3, 2)) ); getchar(); }

return Ampl1;
}


std::complex<float> HamplitudeKstar3(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);
std::complex<float> eK1 = cos(phiK) + ix*sin(phiK);//exp(ix*phiK);
std::complex<float> eK2 = cos(phiK) - ix*sin(phiK);

std::complex<float> BrWig1 = BreitWignerKstar( M, M0, MB, gamma, 2, 3);
std::complex<float> BrWig2 = BreitWignerKstar( M, M0, MB, gamma, 3, 3);
std::complex<float> BrWig3 = BreitWignerKstar( M, M0, MB, gamma, 4, 3);

if(BrWig1 != BrWig1 || BrWig2 != BrWig2 || BrWig3 != BrWig3){
printf("BrWig123 Kstar3 is nan");
printf("%10.2e %10.2e %10.2e %10.2e", M, M0, MB, gamma);
getchar();
}

std::complex<float>	Hs1 = -(float)(sqrt(3.0/7.0))*Bw1*BrWig1 + (float)(sqrt(4.0/7.0))*Bw3*BrWig3;
std::complex<float>	Hs2 = (float)(sqrt(2.0/7.0))*Bw1*BrWig1 - (float)(sqrt(1.0/2.0))*Bw2*BrWig2 + (float)(sqrt(3.0/14.0))*Bw3*BrWig3;
std::complex<float>	Hs3 = (float)(sqrt(2.0/7.0))*Bw1*BrWig1 + (float)(sqrt(1.0/2.0))*Bw2*BrWig2 + (float)(sqrt(3.0/14.0))*Bw3*BrWig3;


std::complex<float> Ampl1 = 0.0;
Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(3.0, 0.0, 0.0, thetaKstar);
Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(3.0, 1.0, 0.0, thetaKstar) * emu1 * eK1;
Ampl1 += Hs3 * (float)Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * (float)Wigner_matrix(3.0, -1.0, 0.0, thetaKstar) * emu2 * eK2;


return Ampl1;
}




std::complex<float> HamplitudeKstar4(float M, float MB, float M0, float gamma, std::complex<float> Bw1, std::complex<float> Bw2, std::complex<float> Bw3, float hmu, float thetaKstar, float thetaPsi, float phimu, float phiK )
{

std::complex<float> ix(0,1);
//printf("%10.5e\n", phimu);
std::complex<float> emu1 = cos(phimu) + ix*sin(phimu);//exp(ix*phimu);
std::complex<float> emu2 = cos(phimu) - ix*sin(phimu);
std::complex<float> eK1 = cos(phiK) + ix*sin(phiK);//exp(ix*phiK);
std::complex<float> eK2 = cos(phiK) - ix*sin(phiK);


std::complex<float> BrWig1 = BreitWignerKstar( M, M0, MB, gamma, 3, 4);
std::complex<float> BrWig2 = BreitWignerKstar( M, M0, MB, gamma, 4, 4);
std::complex<float> BrWig3 = BreitWignerKstar( M, M0, MB, gamma, 5, 4);

if(BrWig1 != BrWig1 || BrWig2 != BrWig2 || BrWig3 != BrWig3){
printf("BrWig123 Kstar4 is nan");
printf("%10.2e %10.2e %10.2e %10.2e", M, M0, MB, gamma);
getchar();
}

std::complex<float>	Hs1 = (float)(sqrt(4.0/9.0))*Bw1*BrWig1 - (float)(sqrt(5.0/9.0))*Bw3*BrWig3;
std::complex<float>	Hs2 = -(float)(1.0/3.0*sqrt(5.0/2.0))*Bw1*BrWig1 + (float)(sqrt(1.0/2.0))*Bw2*BrWig2 - (float)(sqrt(2.0/9.0))*Bw3*BrWig3;
std::complex<float>	Hs3 = -(float)(1.0/3.0*sqrt(5.0/2.0))*Bw1*BrWig1 - (float)(sqrt(1.0/2.0))*Bw2*BrWig2 - (float)(sqrt(2.0/9.0))*Bw3*BrWig3;



std::complex<float> Ampl1 = 0.0;
Ampl1 += Hs1 * (float)Wigner_matrix(1.0, 0.0, hmu, thetaPsi) * (float)Wigner_matrix(4.0, 0.0, 0.0, thetaKstar);
Ampl1 += Hs2 * (float)Wigner_matrix(1.0, 1.0, hmu, thetaPsi) * (float)Wigner_matrix(4.0, 1.0, 0.0, thetaKstar) * emu1 * eK1;
Ampl1 += Hs3 * (float)Wigner_matrix(1.0, -1.0, hmu, thetaPsi) * (float)Wigner_matrix(4.0, -1.0, 0.0, thetaKstar) * emu2 * eK2;


return Ampl1;
}





float HamplitudeXKstar_total(float MX, float MK, float MB, float M01, float M02, float gamma1, float gamma2,
						std::complex<float> X1Bs1, std::complex<float> X1Bs2,
						std::complex<float> X2Bs1, std::complex<float> X2Bs2,
						std::complex<float> K1410_1_Bw1, std::complex<float> K1410_1_Bw2, std::complex<float> K1410_1_Bw3,
						std::complex<float> K1430_0_Bw1, std::complex<float> K1430_0_Bw2, std::complex<float> K1430_0_Bw3,
						std::complex<float> K1430_2_Bw1, std::complex<float> K1430_2_Bw2, std::complex<float> K1430_2_Bw3,
						std::complex<float> K1680_1_Bw1, std::complex<float> K1680_1_Bw2, std::complex<float> K1680_1_Bw3,
						std::complex<float> K1780_3_Bw1, std::complex<float> K1780_3_Bw2, std::complex<float> K1780_3_Bw3,
						std::complex<float> K1950_0_Bw1, std::complex<float> K1950_0_Bw2, std::complex<float> K1950_0_Bw3,
						std::complex<float> K1980_2_Bw1, std::complex<float> K1980_2_Bw2, std::complex<float> K1980_2_Bw3,
						std::complex<float> K2045_4_Bw1, std::complex<float> K2045_4_Bw2, std::complex<float> K2045_4_Bw3,
						std::complex<float> KNR_0_Bw,
						float thetaX, float thetaKstar, float thetaPsiX, float thetaPsiKstar, float phimuX, float phimuKstar, 
						float phiK, float alphamu)
{
	
	float MeL = 0;
	
	std::complex<float> ix(0,1);

	for(int i3 = 0; i3 < 2; i3++)
	{

	float hmu = 2*i3 - 1.0;

	///std::complex<float> eamu = cos(hmu*phimuX) + ix*sin(hmu*phimuX);//exp(ix*phimu);
	std::complex<float> eamu = 1.0;
	std::complex<float> A1X = eamu*HamplitudeX1plus( MX, MB, M01, gamma1, X1Bs1, X1Bs2, hmu, thetaX, thetaPsiX, phimuX, alphamu );
	
	std::complex<float> A2X = eamu*HamplitudeX1plus( MX, MB, M02, gamma2, X2Bs1, X2Bs2, hmu, thetaX, thetaPsiX, phimuX, alphamu );
	
	
	
	std::complex<float> AK1410_1 = HamplitudeKstar1(MK, MB, 1.421, 0.236, K1410_1_Bw1, K1410_1_Bw2, K1410_1_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	std::complex<float> AK1430_0 = HamplitudeKstar0(MK, MB, 1.425, 0.270, K1430_0_Bw1, K1430_0_Bw2, K1430_0_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	//	std::complex<float> AK1430_2 = HamplitudeKstar2(MK, MB, 1.426, 0.099, K1430_2_Bw1, K1430_2_Bw2, K1430_2_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	// PDG 2018, neutral only
	std::complex<float> AK1430_2 = HamplitudeKstar2(MK, MB, 1.4324, 0.109, K1430_2_Bw1, K1430_2_Bw2, K1430_2_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	//	std::complex<float> AK1680_1 = HamplitudeKstar1(MK, MB, 1.717, 0.322, K1680_1_Bw1, K1680_1_Bw2, K1680_1_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	// PDG 2018
	std::complex<float> AK1680_1 = HamplitudeKstar1(MK, MB, 1.718, 0.322, K1680_1_Bw1, K1680_1_Bw2, K1680_1_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	std::complex<float> AK1780_3 = HamplitudeKstar3(MK, MB, 1.776, 0.159, K1780_3_Bw1, K1780_3_Bw2, K1780_3_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	std::complex<float> AK1950_0 = HamplitudeKstar0(MK, MB, 1.945, 0.201, K1950_0_Bw1, K1950_0_Bw2, K1950_0_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );

	std::complex<float> AK1980_2 = HamplitudeKstar2(MK, MB, 1.974, 0.376, K1980_2_Bw1, K1980_2_Bw2, K1980_2_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );

	std::complex<float> AK2045_4 = HamplitudeKstar4(MK, MB, 2.045, 0.198, K2045_4_Bw1, K2045_4_Bw2, K2045_4_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	///std::complex<float> AK1430_2 = HamplitudeKstar2(MK, MB, 1.4324, 0.109, K1430_2_Bw1, K1430_2_Bw2, K1430_2_Bw3, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK );
	std::complex<float> AKNR_0 = 0.0;
	//if(includeKsNR == 1) AKNR_0 = HamplitudeKstarNR0(MK, MB, 1.425, 0.270, KNR_0_Bw, 0.0, 0.0, hmu, thetaKstar, thetaPsiKstar, phimuKstar, phiK ); //commented


	
	//float ad = pow( abs(A1X + A2X + AK1410_1 + AK1430_0 + AK1430_2 + AK1680_1 + AK1780_3 + AK1950_0 + AK2045_4), 2.0 );
	float ad = pow( abs(A1X + A2X + AK1410_1 + AK1430_0 + AK1430_2 + AK1680_1 + AK1780_3 + AK1950_0 + AK1980_2 + AK2045_4), 2.0 );
	MeL += ad;
	if(ad != ad){
		printf("MX=%10.2e\n", MX);
		printf("MB=%10.2e\n", MB);
		printf("MK=%10.2e\n", MK);
		printf("A1X=%10.2e\n", abs(A1X));
		printf("A2X=%10.2e\n", abs(A2X));
		printf("AK1410_1=%10.2e\n", abs(AK1410_1));
		printf("AK1430_0=%10.2e\n", abs(AK1430_0));
		printf("AK1430_2=%10.2e\n", abs(AK1430_2));
		printf("AK1680_1=%10.2e\n", abs(AK1680_1));
		printf("AK1780_3=%10.2e\n", abs(AK1780_3));
		printf("AK1950_0=%10.2e\n", abs(AK1950_0));
		printf("AK1980_0=%10.2e\n", abs(AK1980_2));
		printf("AK2045_4=%10.2e\n", abs(AK2045_4));
		
		getchar();
	}


	}

	return MeL/3.0;


}








/*

double hadronmass1[CHANNELS] = { MASS_PROTON * MEV, MASS_KAON * MEV, MASS_KAON * MEV, MASS_PION * MEV, MASS_KAON * MEV, MASS_PION * MEV, MASS_PROTON * MEV, MASS_KAON * MEV, MASS_PROTON * MEV, MASS_KAON * MEV, MASS_PROTON * MEV, MASS_PION * MEV };
double hadronmass2[CHANNELS] = { MASS_KAON * MEV, MASS_PROTON * MEV, MASS_KAON * MEV, MASS_PION * MEV, MASS_PION * MEV, MASS_KAON * MEV, MASS_KAON * MEV, MASS_PROTON * MEV, MASS_KAON * MEV, MASS_PROTON * MEV, MASS_PION * MEV, MASS_PROTON * MEV };


////// kinematic variables:
	if (j==4)
			{
			v_p1.SetXYZM( GEV*mc_momentum->at(1).Px(), GEV*mc_momentum->at(1).Py(), GEV*mc_momentum->at(1).Pz(), hadronmass1[j] );
			v_K2.SetXYZM( GEV*mc_momentum->at(2).Px(), GEV*mc_momentum->at(2).Py(), GEV*mc_momentum->at(2).Pz(), hadronmass2[j] );
			    vLambda_pK = v_Jpsi1 + v_p1 + v_K2;
				vJpsip = v_Jpsi1 + v_p1;
				vJpsiK = v_Jpsi1 + v_K2;
				v_pK = v_p1 + v_K2;

				BPhS_mc_mass_Kpi[evcounter] = GeV*v_pK.Mag();
				BPhS_mc_mass_X[evcounter] = GeV*vJpsiK.Mag();
				BPhS_mc_theta_X[evcounter] = theta_Pc(vLambda_pK, vJpsip, v_Jpsi1);
				BPhS_mc_theta_psi[evcounter] = theta_psi(vJpsip, v_Jpsi1, v_mu2);
				BPhS_mc_theta_psiKstar[evcounter] = theta_psi(vLambda_pK, v_Jpsi1, v_mu2);
				BPhS_mc_theta_Kstar[evcounter] = theta_Lstar( vLambda_pK, v_pK, v_K2 );
				BPhS_mc_phi_mu[evcounter] = phi_mu_Pc(vJpsip, v_K2, v_Jpsi1, v_mu2);
				BPhS_mc_phi_muKstar[evcounter] = phi_mu_Lstar(vLambda_pK, v_p1+v_K2, v_Jpsi1, v_mu2);
				BPhS_mc_phi_K[evcounter] = phi_K(vLambda_pK, v_pK, v_K2, v_Jpsi1);
				BPhS_mc_alpha_mu[evcounter] = alpha_mu(v_mu2, v_Jpsi1, vJpsip, vLambda_pK);
	}
*/



/////////////////////////////////////// Переделанные углы:

Double_t theta_B0(TLorentzVector vBd, TLorentzVector vKstar)
{
    TVector3 vBd3 = vBd.BoostVector();
	TLorentzVector vKstar_b = vKstar;
	vKstar_b.Boost(-vBd3);
return vBd.Vect().Angle( vKstar_b.Vect() );
}

Double_t phi_psi(TLorentzVector vB0, TLorentzVector vZc, TLorentzVector vpsi)
{
	TLorentzVector vZc_b = vZc;
	TLorentzVector vpsi_b = vpsi;
	TVector3 vZc3 = vZc.BoostVector();
    TVector3 vB03 = vB0.BoostVector();
	vZc_b.Boost(-vB03);
	vpsi_b.Boost(-vZc3);

	TVector3 x0 = -vB0.Vect().Unit() + ( vB0.Vect().Unit() * vZc_b.Vect().Unit() ) * vZc_b.Vect().Unit();
	
	double argy = ( vZc_b.Vect().Unit().Cross(x0.Unit()) ) * vpsi_b.Vect().Unit();
	double argx = vpsi_b.Vect().Unit() * x0.Unit();

	return atan2( argy, argx );
}

Double_t phi_K(TLorentzVector vBd, TLorentzVector vKstar, TLorentzVector vK, TLorentzVector vpsi)
{
	TLorentzVector vK_b = vK;
	TLorentzVector vKstarb = vKstar;
	TLorentzVector vpsib = vpsi;
	TLorentzVector vKb = vK;
	TVector3 vBd3 = vBd.BoostVector();
	TVector3 vKstar3 = vKstar.BoostVector();
	

	vKstarb.Boost(-vBd3);
	vpsib.Boost(-vKstar3);
	vKb.Boost(-vKstar3);

	TVector3 x0 = - vBd.Vect().Unit() + ( vBd.Vect().Unit() * vKstarb.Vect().Unit() ) * vKstarb.Vect().Unit();
	
	
	double argy = ( -vpsib.Vect().Unit().Cross(x0.Unit()) ) * vKb.Vect().Unit();
	double argx = x0.Unit() * vKb.Vect().Unit();

	return atan2( argy, argx );
}

Double_t alpha_mu(TLorentzVector vmu_2, TLorentzVector vpsi, TLorentzVector vZc, TLorentzVector vBd)
{
	TLorentzVector vmu2_b = vmu_2;	
	TLorentzVector vpsi_b = vpsi;
	TLorentzVector vpsi_b2 = vpsi;
	//TLorentzVector vLambda_b = vLambda;
	TVector3 vBd3 = vBd.BoostVector();
	TVector3 vZc3 = vZc.BoostVector();
	TVector3 vpsi3 = vpsi.BoostVector();
	

	vmu2_b.Boost(-vpsi3);
	vpsi_b.Boost(-vBd3);
	vpsi_b2.Boost(-vZc3);


	TVector3 x1 = - vpsi_b2.Vect().Unit() + ( vpsi_b2.Vect().Unit() * vmu2_b.Vect().Unit() ) * vmu2_b.Vect().Unit();
	TVector3 x2 = - vpsi_b.Vect().Unit() + ( vpsi_b.Vect().Unit() * vmu2_b.Vect().Unit() ) * vmu2_b.Vect().Unit();
	
	
	double argy = ( vmu2_b.Vect().Unit().Cross(x1.Unit()) ) * x2.Unit();
	double argx = x1.Unit() * x2.Unit();

	return atan2( argy, argx );
}

Double_t phi_mu_Kstar(TLorentzVector vBd, TLorentzVector vKstar, TLorentzVector vpsi, TLorentzVector v_mu2)
{
	TLorentzVector vKstar_b = vKstar;
	TLorentzVector vpsi_b = vpsi;
	TVector3 vBd3 = vBd.BoostVector();
	
	vpsi_b.Boost(-vBd3);
	vKstar_b.Boost(-vBd3);

	TVector3 x0 = -vBd.Vect().Unit() + ( vBd.Vect().Unit() * vKstar_b.Vect().Unit() ) * vKstar_b.Vect().Unit();
	
	TLorentzVector v_mu2_b = v_mu2;
	TVector3 vpsi3 = vpsi.BoostVector();
	
	v_mu2_b.Boost(-vpsi3);
	
	double argy = ( vpsi_b.Vect().Unit().Cross(x0.Unit()) ) * v_mu2_b.Vect().Unit();
	double argx = v_mu2_b.Vect().Unit() * x0.Unit();

	return atan2( argy, argx );
}

Double_t phi_mu_X(TLorentzVector vZc, TLorentzVector vB0, TLorentzVector vpsi, TLorentzVector v_mu2)
{
	TLorentzVector vZc_b = vZc;
	TLorentzVector vpsi_b = vpsi;
	TVector3 vZc3 = vZc.BoostVector();
    TVector3 vB03 = vB0.BoostVector();
	vZc_b.Boost(-vB03);
	vpsi_b.Boost(-vZc3);

	TVector3 x0 = -vZc_b.Vect().Unit() + ( vZc_b.Vect().Unit() * vpsi_b.Vect().Unit() ) * vpsi_b.Vect().Unit();
	
	
	TLorentzVector v_mu2_b = v_mu2;
	TVector3 vpsi3 = vpsi.BoostVector();
	v_mu2_b.Boost(-vpsi3);
	
	double argy = ( vpsi_b.Vect().Unit().Cross(x0.Unit()) ) * v_mu2_b.Vect().Unit();
	double argx = v_mu2_b.Vect().Unit() * x0.Unit();

	return atan2( argy, argx );
}

///// angle between Jpsi and muon in Jpsi rest frame, Jpsi momentum is taken in Pc rest frame;
Double_t theta_psi_X(TLorentzVector vZc, TLorentzVector vpsi, TLorentzVector v_mu2)
{

	TVector3 vZc3 = vZc.BoostVector();
	TVector3 vpsi3 = vpsi.BoostVector();
	TLorentzVector v_mu2_b = v_mu2; 
	v_mu2_b.Boost(-vpsi3);
	TLorentzVector vpsi_b = vpsi;
	vpsi_b.Boost(-vZc3);

return v_mu2_b.Vect().Angle(vpsi_b.Vect());
}

//////////////////////////////////////////////////////////////////////////////////////////////
///// angle between Jpsi and Pc in Pc rest frame, Pc momentum is taken in Lambda_b rest frame;
Double_t theta_X(TLorentzVector vBd, TLorentzVector vZc, TLorentzVector vpsi)
{
	TVector3 vBd3 = vBd.BoostVector();
	TVector3 vZc3 = vZc.BoostVector();
	TLorentzVector vpsi_b = vpsi;
	vpsi_b.Boost(-vZc3);
	TLorentzVector vZc_b2 = vZc;
	vZc_b2.Boost(-vBd3);

return vpsi_b.Vect().Angle( vZc_b2.Vect() );
}

//////////////////////////////////////////////////////////////////////////////////////////////
///// angle between Kaon and Lstar candidate in Lstar rest frame, Lstar momentum is taken in Lambda_b rest frame;
Double_t theta_Kstar( TLorentzVector vBd, TLorentzVector vKstar, TLorentzVector vK )
{
	
	TVector3 vBd3 = vBd.BoostVector();
	TVector3 vKstar3 = vKstar.BoostVector();

	TLorentzVector vK_b = vK;
	vK_b.Boost(-vKstar3);
	
	TLorentzVector vKstarb = vKstar;
	vKstarb.Boost(-vBd3);
	return vK_b.Vect().Angle(vKstarb.Vect()) ;

}

///////////////////////////////////////////////////////////////////////////////////////////////
///// angle between between Jpsi and muon in Jpsi rest frame, Jpsi momentum is taken in Bd rest frame;
Double_t theta_psi_Kstar(TLorentzVector vBd, TLorentzVector vpsi, TLorentzVector v_mu2)
{

	TVector3 vBd3 = vBd.BoostVector();
	TVector3 vpsi3 = vpsi.BoostVector();
	TLorentzVector v_mu2_b = v_mu2; 
	v_mu2_b.Boost(-vpsi3);
	TLorentzVector vpsi_b = vpsi;
	vpsi_b.Boost(-vBd3);

return v_mu2_b.Vect().Angle(vpsi_b.Vect());
}

Double_t s_calculation(TLorentzVector vjpsi, TLorentzVector vK, TLorentzVector vpi)
{
	float t = 0.61803, a, b, t1, t2;
    float m, M, M0 = 4800., M1 = 4900., m0 = 5250., m1 = 5310.;
    TLorentzVector vjpsi1, vK1, vpi1, vjpsi2, vK2, vpi2;
    M = (vjpsi + vK + vpi).M();
    m = m0 + (m1 - m0) / (M1 - M0) * (M - M0);
    a = 1.;
    b = 2.;
    t1 = a + (b - a) * (1 - t);
    t2 = a + (b - a) * t;
    while (b - a > 0.0001)
    {
        vjpsi1.SetXYZM(t1 * vjpsi.Px(), t1 * vjpsi.Py(), t1 * vjpsi.Pz(), 3096.916);
        vK1.SetXYZM(t1 * vK.Px(), t1 * vK.Py(), t1 * vK.Pz(), 493.677);
        vpi1.SetXYZM(t1 * vpi.Px(), t1 * vpi.Py(), t1 * vpi.Pz(), 139.57);
        vjpsi2.SetXYZM(t2 * vjpsi.Px(), t2 * vjpsi.Py(), t2 * vjpsi.Pz(), 3096.916);
        vK2.SetXYZM(t2 * vK.Px(), t2 * vK.Py(), t2 * vK.Pz(), 493.677);
        vpi2.SetXYZM(t2 * vpi.Px(), t2 * vpi.Py(), t2 * vpi.Pz(), 139.57);
        if (abs(m - (vjpsi1 + vK1 + vpi1).M()) > abs((vjpsi2 + vK2 + vpi2).M() - m))
        {
            a = t1;
            t1 = t2;
            t2 = a + (b - a) * t;
        }
        else
        {
            b = t2;
            t2 = t1;
            t1 = a + (b - a) * (1 - t);
        }
    }
    vjpsi1.SetXYZM(t1 * vjpsi.Px(), t1 * vjpsi.Py(), t1 * vjpsi.Pz(), 3096.916);
    vK1.SetXYZM(t1 * vK.Px(), t1 * vK.Py(), t1 * vK.Pz(), 493.677);
    vpi1.SetXYZM(t1 * vpi.Px(), t1 * vpi.Py(), t1 * vpi.Pz(), 139.57);
    vjpsi2.SetXYZM(t2 * vjpsi.Px(), t2 * vjpsi.Py(), t2 * vjpsi.Pz(), 3096.916);
    vK2.SetXYZM(t2 * vK.Px(), t2 * vK.Py(), t2 * vK.Pz(), 493.677);
    vpi2.SetXYZM(t2 * vpi.Px(), t2 * vpi.Py(), t2 * vpi.Pz(), 139.57);
    if (abs(m - (vjpsi1 + vK1 + vpi1).M()) > abs((vjpsi2 + vK2 + vpi2).M() - m)) 
    {
        //cout << (vjpsi2 + vK2 + vpi2).M()  << "    " << m << endl;
        return t2;
    }
    else
    {
        //cout << (vjpsi1 + vK1 + vpi1).M() << "    " << m << endl;
        return t1;
    }
}

Double_t s_mc_calculation(TLorentzVector vjpsi, TLorentzVector vK, TLorentzVector vpi)
{
	float t = 0.61803, a, b, t1, t2;
    float m, M, M0 = 4800., M1 = 4900., m0 = 5250., m1 = 5310.;
    TLorentzVector vjpsi1, vK1, vpi1, vjpsi2, vK2, vpi2;
    M = (vjpsi + vK + vpi).M();
    m = 5279.64;
    a = 1.;
    b = 2.;
    t1 = a + (b - a) * (1 - t);
    t2 = a + (b - a) * t;
    while (b - a > 0.0001)
    {
        vjpsi1.SetXYZM(t1 * vjpsi.Px(), t1 * vjpsi.Py(), t1 * vjpsi.Pz(), 3096.916);
        vK1.SetXYZM(t1 * vK.Px(), t1 * vK.Py(), t1 * vK.Pz(), 493.677);
        vpi1.SetXYZM(t1 * vpi.Px(), t1 * vpi.Py(), t1 * vpi.Pz(), 139.57);
        vjpsi2.SetXYZM(t2 * vjpsi.Px(), t2 * vjpsi.Py(), t2 * vjpsi.Pz(), 3096.916);
        vK2.SetXYZM(t2 * vK.Px(), t2 * vK.Py(), t2 * vK.Pz(), 493.677);
        vpi2.SetXYZM(t2 * vpi.Px(), t2 * vpi.Py(), t2 * vpi.Pz(), 139.57);
        if (abs(m - (vjpsi1 + vK1 + vpi1).M()) > abs((vjpsi2 + vK2 + vpi2).M() - m))
        {
            a = t1;
            t1 = t2;
            t2 = a + (b - a) * t;
        }
        else
        {
            b = t2;
            t2 = t1;
            t1 = a + (b - a) * (1 - t);
        }
    }
    vjpsi1.SetXYZM(t1 * vjpsi.Px(), t1 * vjpsi.Py(), t1 * vjpsi.Pz(), 3096.916);
    vK1.SetXYZM(t1 * vK.Px(), t1 * vK.Py(), t1 * vK.Pz(), 493.677);
    vpi1.SetXYZM(t1 * vpi.Px(), t1 * vpi.Py(), t1 * vpi.Pz(), 139.57);
    vjpsi2.SetXYZM(t2 * vjpsi.Px(), t2 * vjpsi.Py(), t2 * vjpsi.Pz(), 3096.916);
    vK2.SetXYZM(t2 * vK.Px(), t2 * vK.Py(), t2 * vK.Pz(), 493.677);
    vpi2.SetXYZM(t2 * vpi.Px(), t2 * vpi.Py(), t2 * vpi.Pz(), 139.57);
    if (abs(m - (vjpsi1 + vK1 + vpi1).M()) > abs((vjpsi2 + vK2 + vpi2).M() - m)) 
    {
        //cout << (vjpsi2 + vK2 + vpi2).M()  << "    " << m << endl;
        return t2;
    }
    else
    {
        //cout << (vjpsi1 + vK1 + vpi1).M() << "    " << m << endl;
        return t1;
    }
}

float matrix_element(TLorentzVector muon1, TLorentzVector muon2, TLorentzVector kaon, TLorentzVector pion)
{ 
    TLorentzVector jpsi = muon1 + muon2, Bd = muon1 + muon2 + kaon + pion, Zc = muon1 + muon2 + pion, Kstar = kaon + pion;
     Double_t vstart[46] = {3.5746, -7.93451, 1.31998, 1.84057, 20.0575, 16.5005, -1.24044, -18.9953, -418.7, -16.0238, 0.279479, 11.2291, 496.636, 0.00180622, 26.9619, 26.9619, 1.88133, 11.6268, 6.36857, -18.83, 0.00960553, 2.52659, 53.4462, -6.44662, 1.45676, -0.922654, 0.354629, 5.19828, 400.057, 93.6205, -320.021, 26.3613, 0.00956211, 0.00350352, 133.805, -15.3857, 0.00827755, -1.98554, -15675, 0, 11.2659, 19.0313, 84.1563, 0.586158, 0.306206, 4.196};
    double K1410_1_Bw2_amp, K1410_1_Bw2_phi, K1410_1_Bw3_amp, K1410_1_Bw3_phi, K1430_2_Bw2_amp, K1430_2_Bw2_phi, K1430_2_Bw3_amp, K1430_2_Bw3_phi, K1680_1_Bw2_amp, K1680_1_Bw2_phi, K1680_1_Bw3_amp, K1680_1_Bw3_phi, K1780_3_Bw2_amp, K1780_3_Bw2_phi, K1780_3_Bw3_amp, K1780_3_Bw3_phi, K1980_2_Bw2_amp, K1980_2_Bw2_phi, K1980_2_Bw3_amp, K1980_2_Bw3_phi, K2045_4_Bw2_amp, K2045_4_Bw2_phi, K2045_4_Bw3_amp, K2045_4_Bw3_phi, X1Bs1_amp, X1Bs1_phi, X1Bs2_amp, X1Bs2_phi, gamma_Zc4200, m_Zc4200;
    double K1410_1_Bw1_amp, K1410_1_Bw1_phi, K1430_0_Bw1_amp, K1430_0_Bw1_phi, K1430_2_Bw1_amp, K1430_2_Bw1_phi, K1680_1_Bw1_amp, K1680_1_Bw1_phi, K1780_3_Bw1_amp, K1780_3_Bw1_phi, K1950_0_Bw1_amp, K1950_0_Bw1_phi, K2045_4_Bw1_amp, K2045_4_Bw1_phi, par1_Kpi, par2_Kpi, par1_jpsipi, par2_jpsipi, par1_jpsiK, par2_jpsiK, K1980_2_Bw1_amp, K1980_2_Bw1_phi;


    K1410_1_Bw1_amp = vstart[0];
    K1410_1_Bw1_phi = vstart[1];
    K1430_0_Bw1_amp = vstart[2];
    K1430_0_Bw1_phi = vstart[3];
    K1430_2_Bw1_amp = vstart[4];
    K1430_2_Bw1_phi = vstart[5];
    K1680_1_Bw1_amp = vstart[6];
    K1680_1_Bw1_phi = vstart[7];
    K1780_3_Bw1_amp = vstart[8];
    K1780_3_Bw1_phi = vstart[9];
    K1950_0_Bw1_amp = vstart[10];
    K1950_0_Bw1_phi = vstart[11];
    K2045_4_Bw1_amp = vstart[12];
    K2045_4_Bw1_phi = vstart[13];
    K1980_2_Bw1_amp = vstart[14];
    K1980_2_Bw1_phi = vstart[15];
    K1410_1_Bw2_amp = vstart[16];
    K1410_1_Bw2_phi = vstart[17];
    K1410_1_Bw3_amp = vstart[18];
    K1410_1_Bw3_phi = vstart[19];
    K1430_2_Bw2_amp = vstart[20];
    K1430_2_Bw2_phi = vstart[21];
    K1430_2_Bw3_amp = vstart[22];
    K1430_2_Bw3_phi = vstart[23];
	K1680_1_Bw2_amp = vstart[24];
    K1680_1_Bw2_phi = vstart[25];
    K1680_1_Bw3_amp = vstart[26];
    K1680_1_Bw3_phi = vstart[27];
	K1780_3_Bw2_amp = vstart[28];
    K1780_3_Bw2_phi = vstart[29];
    K1780_3_Bw3_amp = vstart[30];
    K1780_3_Bw3_phi = vstart[31];
	K1980_2_Bw2_amp = vstart[32];
    K1980_2_Bw2_phi = vstart[33];
    K1980_2_Bw3_amp = vstart[34];
    K1980_2_Bw3_phi = vstart[35];
	K2045_4_Bw2_amp = vstart[36];
    K2045_4_Bw2_phi = vstart[37];
    K2045_4_Bw3_amp = vstart[38];
    K2045_4_Bw3_phi = vstart[39];
    X1Bs1_amp = vstart[40];
    X1Bs1_phi = vstart[41];
	X1Bs2_amp = vstart[42];
    X1Bs2_phi = vstart[43];
    

    return HamplitudeXKstar_total((jpsi + pion).M() / 1000., (kaon + pion).M() / 1000., 5.27964, 4.2, 4.43, 0.3, 0.181,
						std::complex<float>(X1Bs1_amp * cos(X1Bs1_phi), X1Bs1_amp * sin(X1Bs1_phi)), 
                        std::complex<float>(X1Bs2_amp * cos(X1Bs2_phi), X1Bs2_amp * sin(X1Bs2_phi)),
						std::complex<float>(0., 0.), std::complex<float>(0., 0.),
						std::complex<float>(K1410_1_Bw1_amp * cos(K1410_1_Bw1_phi), K1410_1_Bw1_amp * sin(K1410_1_Bw1_phi)), 
                        std::complex<float>(K1410_1_Bw2_amp * cos(K1410_1_Bw2_phi), K1410_1_Bw2_amp * sin(K1410_1_Bw2_phi)), 
                        std::complex<float>(K1410_1_Bw3_amp * cos(K1410_1_Bw3_phi), K1410_1_Bw3_amp * sin(K1410_1_Bw3_phi)),
						std::complex<float>(K1430_0_Bw1_amp * cos(K1430_0_Bw1_phi), K1430_0_Bw1_amp * sin(K1430_0_Bw1_phi)), 
                        std::complex<float>(0., 0.), 
                        std::complex<float>(0., 0.),
						std::complex<float>(K1430_2_Bw1_amp * cos(K1430_2_Bw1_phi), K1430_2_Bw1_amp * sin(K1430_2_Bw1_phi)), 
                        std::complex<float>(K1430_2_Bw2_amp * cos(K1430_2_Bw2_phi), K1430_2_Bw2_amp * sin(K1430_2_Bw2_phi)), 
                        std::complex<float>(K1430_2_Bw3_amp * cos(K1430_2_Bw3_phi), K1430_2_Bw3_amp * sin(K1430_2_Bw3_phi)),
						std::complex<float>(K1680_1_Bw1_amp * cos(K1680_1_Bw1_phi), K1680_1_Bw1_amp * sin(K1680_1_Bw1_phi)), 
                        std::complex<float>(K1680_1_Bw2_amp * cos(K1680_1_Bw2_phi), K1680_1_Bw2_amp * sin(K1680_1_Bw2_phi)), 
                        std::complex<float>(K1680_1_Bw3_amp * cos(K1680_1_Bw3_phi), K1680_1_Bw3_amp * sin(K1680_1_Bw3_phi)),
						std::complex<float>(K1780_3_Bw1_amp * cos(K1780_3_Bw1_phi), K1780_3_Bw1_amp * sin(K1780_3_Bw1_phi)), 
                        std::complex<float>(K1780_3_Bw2_amp * cos(K1780_3_Bw2_phi), K1780_3_Bw2_amp * sin(K1780_3_Bw2_phi)), 
                        std::complex<float>(K1780_3_Bw3_amp * cos(K1780_3_Bw3_phi), K1780_3_Bw3_amp * sin(K1780_3_Bw3_phi)),
						std::complex<float>(K1950_0_Bw1_amp * cos(K1950_0_Bw1_phi), K1950_0_Bw1_amp * sin(K1950_0_Bw1_phi)), 
                        std::complex<float>(0., 0.), 
                        std::complex<float>(0., 0.),
						std::complex<float>(K1980_2_Bw1_amp * cos(K1980_2_Bw1_phi), K1980_2_Bw1_amp * sin(K1980_2_Bw1_phi)),
                        std::complex<float>(K1980_2_Bw2_amp * cos(K1980_2_Bw2_phi), K1980_2_Bw2_amp * sin(K1980_2_Bw2_phi)), 
                        std::complex<float>(K1980_2_Bw3_amp * cos(K1980_2_Bw3_phi), K1980_2_Bw3_amp * sin(K1980_2_Bw3_phi)),
						std::complex<float>(K2045_4_Bw1_amp * cos(K2045_4_Bw1_phi), K2045_4_Bw1_amp * sin(K2045_4_Bw1_phi)),
                        std::complex<float>(K2045_4_Bw2_amp * cos(K2045_4_Bw2_phi), K2045_4_Bw2_amp * sin(K2045_4_Bw2_phi)), 
                        std::complex<float>(K2045_4_Bw3_amp * cos(K2045_4_Bw3_phi), K2045_4_Bw3_amp * sin(K2045_4_Bw3_phi)),
						std::complex<float>(0., 0.),
						theta_X(Bd, Zc, jpsi), theta_Kstar( Bd, Kstar, kaon ),  theta_psi_X(Zc, jpsi, muon2), theta_psi_Kstar(Bd, jpsi, muon2), phi_mu_X(Zc, kaon, jpsi, muon2), phi_mu_Kstar(Bd, Kstar, jpsi, muon2), 
						phi_K(Bd, Kstar, kaon, jpsi), alpha_mu(muon2, jpsi, Zc, Bd));
}
/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////APPENDIX


///// angle between Pc and Lambda_b in Lambda_b rest frame, Lambda_b momentum is taken in lab system;
Double_t theta_lambda_b(TLorentzVector vLambda, TLorentzVector vPc)
{

	TVector3 vLambda3 = vLambda.BoostVector();
	TLorentzVector vPc_b = vPc;
	vPc_b.Boost(-vLambda3);

return vPc_b.Vect().Angle( vLambda.Vect() );
}


Double_t theta_lambda_b_ls(TLorentzVector vLambda, TLorentzVector vp, TLorentzVector vK )
{

	TVector3 vLambda3 = vLambda.BoostVector();
	TLorentzVector vLstar_b = vp + vK;
	vLstar_b.Boost(-vLambda3);

return vLstar_b.Vect().Angle( vLambda.Vect() );
}

///// angle between Jpsi and muon in Jpsi rest frame, Jpsi momentum is taken in Pc rest frame;
Double_t phi_mu(TLorentzVector vPc, TLorentzVector vK, TLorentzVector vpsi, TLorentzVector vp, TLorentzVector v_mu2)
{
	TLorentzVector vK_b = vK;
	TLorentzVector vpsi_b = vpsi;
	TVector3 vPc3 = vPc.BoostVector();
	vK_b.Boost(-vPc3);
	vpsi_b.Boost(-vPc3);

	TVector3 x0 = vK_b.Vect().Unit() - ( vK_b.Vect().Unit() * vpsi_b.Vect().Unit() ) * vpsi_b.Vect().Unit();
	

	TLorentzVector vp_b = vp;
	TLorentzVector v_mu2_b = v_mu2;
	TVector3 vpsi3 = vpsi.BoostVector();
	vp_b.Boost(-vpsi3);
	v_mu2_b.Boost(-vpsi3);
	
	double argy = ( -vp_b.Vect().Unit().Cross(x0.Unit()) ) * v_mu2_b.Vect().Unit();
	double argx = v_mu2_b.Vect().Unit() * x0.Unit();

	return atan2( argy, argx );

}

Double_t phi_K_Lstar(TLorentzVector vLambda, TLorentzVector vLstar, TLorentzVector vK, TLorentzVector vpsi)
{
	
	TLorentzVector vK_b = vK;
	TLorentzVector vLstarb = vLstar;
	TLorentzVector vpsib = vpsi;
	TLorentzVector vKb = vK;
	TVector3 vLambda3 = vLambda.BoostVector();
	TVector3 vLstar3 = vLstar.BoostVector();
	

	vLstarb.Boost(-vLambda3);
	vpsib.Boost(-vLstar3);
	vKb.Boost(-vLstar3);

	TVector3 x0 = - vLambda.Vect().Unit() + ( vLambda.Vect().Unit() * vLstarb.Vect().Unit() ) * vLstarb.Vect().Unit();
	
	
	double argy = ( vLstarb.Vect().Unit().Cross(x0.Unit()) ) * vKb.Vect().Unit();
	double argx = x0.Unit() * vKb.Vect().Unit();

	return atan2( argy, argx );

}




Double_t phi_psi(TLorentzVector vLambda, TLorentzVector vPc, TLorentzVector vpsi, TLorentzVector vK )
{
	TVector3 vLambda3 = vLambda.BoostVector();
	TLorentzVector vPc_b = vPc;
	vPc_b.Boost(-vLambda3);
	
	TVector3 x0 = -vLambda.Vect().Unit() + ( vLambda.Vect().Unit() * vPc_b.Vect().Unit() ) * vPc_b.Vect().Unit();
//	printf("%10.5e\t", x0.Mag() ); getchar();

	TLorentzVector vK_b = vK;
	TLorentzVector vpsi_b = vpsi;
	TVector3 vPc3 = vPc.BoostVector();
	vK_b.Boost(-vPc3);
	vpsi_b.Boost(-vPc3);

	double argy = ( -vK_b.Vect().Unit().Cross( x0.Unit() ) ) * vpsi_b.Vect().Unit();   //x0.Unit();
	//double argy = ( vK_b.Vect().Unit().Cross(vpsi_b.Vect().Unit()) ) * x0.Unit();
	double argx = vpsi_b.Vect().Unit() * x0.Unit();

	return atan2(  argy,  argx );
}



Double_t phi_Pc(TLorentzVector vLambda, TLorentzVector vPc, TLorentzVector vLstar)
{
	
	TLorentzVector vPc_b = vPc;
	TLorentzVector vLstar_b = vLstar;
	TVector3 vLambda3 = vLambda.BoostVector();
	

	vPc_b.Boost(-vLambda3);
	vLstar_b.Boost(-vLambda3);

	TVector3 x0 = vLstar_b.Vect().Unit() - ( vLstar_b.Vect().Unit() * vLambda.Vect().Unit() ) * vLambda.Vect().Unit();
	
	
	double argy = ( vLambda.Vect().Unit().Cross(x0.Unit()) ) * vPc_b.Vect().Unit();
	double argx = x0.Unit() * vPc_b.Vect().Unit();

	return atan2( argy, argx );

}

Double_t theta_p(TLorentzVector vpsi, TLorentzVector vp, TLorentzVector vK)
{


	TLorentzVector vpsi_b = vpsi;
	TLorentzVector vK_b = vK;
	//TLorentzVector vLambda_b = vLambda;

	TVector3 vp3 = vp.BoostVector();
	

	vpsi_b.Boost(-vp3);
	vK_b.Boost(-vp3);


	return vK_b.Vect().Angle(vpsi_b.Vect()) ;
}*/
	
/*

BPhS_mass_Lambda[evcounter] = GeV*vLambda_pK.Mag();
					BPhS_theta_Pc[evcounter] = theta_Pc(vLambda_pK, vJpsip, v_Jpsi1);
					///thetap_B0Kpi00 = theta_p(v_Jpsi1, v_p1, v_K2);
					BPhS_theta_Lambda[evcounter] = theta_lambda_b(vLambda_pK, vJpsip);
					//PhS_theta_LambdaLs[evcounter] = theta_lambda_b_ls(vLambda_pK, v_p1, v_K2);
					//ptLambdab_B0Kpi00 = vLambda_pK.Pt();
					//ptJpsi_B0Kpi00 = v_Jpsi1.Pt();
					//ptLstar_B0Kpi00 = (v_p1+v_K2).Pt();
					//ptmu_B0Kpi00 = v_mu1.Pt();
					//ptmu_B0Kpi01 = v_mu2.Pt();
					//pth_B0Kpi00 = v_p1.Pt();
					//pth_B0Kpi01 = v_K2.Pt();
					//etamu_B0Kpi00 = v_mu1.Eta();
					//etamu_B0Kpi01 = v_mu2.Eta();
					//etah_B0Kpi00 = v_p1.Eta();
					//etah_B0Kpi01 = v_K2.Eta();
					//etaLambdab_B0Kpi00 = vLambda_pK.Eta();
					BPhS_thetaLb_psi[evcounter] = theta_psi(vJpsip, v_Jpsi1, v_mu2);
					BPhS_theta_psi[evcounter] = theta_psi(vJpsip, v_Jpsi1, v_mu2);
					//PhS_theta_psiLstar[evcounter] = theta_psi(vLambda_pK, v_Jpsi1, v_mu2);
					//PhS_phi_Pc[evcounter] = phi_Pc(vLambda_pK, vJpsip, v_p1+v_K2);
					//PhS_phi_psi[evcounter] = phi_psi(vLambda_pK, vJpsip, v_Jpsi1, v_K2 );
					//PhS_phi_mu[evcounter] = phi_mu_Pc(vJpsip, v_K2, v_Jpsi1, v_mu2);
					//alphamu_B0Kpi00 = alpha_mu(v_mu2, v_Jpsi1, vJpsip, vLambda_pK);
*/

				

/*					
double matrix_el = HamplitudeXKstar_total(BPhS_mc_mass_X[ent], BPhS_mc_mass_Kpi[ent], BPhS_mc_mass_B0[ent], M1, M2, G1, G2,
						X1Bs1, X1Bs2,
						X2Bs1, X2Bs2,
						K1410_1_Bw1, K1410_1_Bw2, K1410_1_Bw3,
						K1430_0_Bw1, K1430_0_Bw2, K1430_0_Bw3,
						K1430_2_Bw1, K1430_2_Bw2, K1430_2_Bw3,
						K1680_1_Bw1, K1680_1_Bw2, K1680_1_Bw3,
						K1780_3_Bw1, K1780_3_Bw2, K1780_3_Bw3,
						K1950_0_Bw1, K1950_0_Bw2, K1950_0_Bw3,
						K1980_2_Bw1, K1980_2_Bw2, K1980_2_Bw3,
						K2045_4_Bw1, K2045_4_Bw2, K2045_4_Bw3,
						KNR_0_Bw,
						BPhS_mc_theta_X[ent], BPhS_mc_theta_Kstar[ent], BPhS_mc_theta_psi[ent], BPhS_theta_psiKstar[ent], BPhS_mc_phi_mu[ent], BPhS_mc_phi_muKstar[ent], 
						BPhS_mc_phi_K[ent], BPhS_mc_alpha_mu[ent]);
*/					
