{
    TFile *f = new TFile("signal_area_bg.root", "open");
    TH2D* Jpsipi_comb = (TH2D*)f->Get("Jpsipi_2d_bg");
    Jpsipi_comb->SetTitle("Jpsipi_comb");
    TH2D* JpsiK_comb = (TH2D*)f->Get("JpsiK_2d_bg");
    JpsiK_comb->SetTitle("JpsiK_comb");
    TH2D* Kpi_comb = (TH2D*)f->Get("Kpi_2d_bg");
    Kpi_comb->SetTitle("Kpi_comb");
    TH2D* trk_pt_comb = (TH2D*)f->Get("trk_pt_2d_bg");
    
    TFile *f_old = new TFile("signal_area_bg_it0.root", "open");
    TH2D* Jpsipi_comb_old = (TH2D*)f_old->Get("Jpsipi_2d_bg");
    TH2D* JpsiK_comb_old = (TH2D*)f_old->Get("JpsiK_2d_bg");
    TH2D* Kpi_comb_old = (TH2D*)f_old->Get("Kpi_2d_bg");
    
    TFile *f_sc = new TFile("comb_bg_signal.root", "open");
    TH2D* Jpsipi_2d_bg_comb = (TH2D*)f_sc->Get("Jpsipi_2d_bg_comb");
    TH2D* JpsiK_2d_bg_comb = (TH2D*)f_sc->Get("JpsiK_2d_bg_comb");
    TH2D* Kpi_2d_bg_comb = (TH2D*)f_sc->Get("Kpi_2d_bg_comb");
    TH2D* trk_pt_bg_comb = (TH2D*)f_sc->Get("trk_pt_bg_comb");
    
    Jpsipi_comb->SetLineColor(2);
    JpsiK_comb->SetLineColor(2);
    Kpi_comb->SetLineColor(2);
    trk_pt_comb->SetLineColor(2);
    
    Jpsipi_comb_old->SetLineColor(3);
    JpsiK_comb_old->SetLineColor(3);
    Kpi_comb_old->SetLineColor(3);
    
    Jpsipi_2d_bg_comb->SetLineColor(4);
    JpsiK_2d_bg_comb->SetLineColor(4);
    Kpi_2d_bg_comb->SetLineColor(4);
    trk_pt_bg_comb->SetLineColor(4); 
    
    TLegend *legend = new TLegend(.60,.60,.90,.90);
    legend->AddEntry(Jpsipi_comb,"new");
	legend->AddEntry(Jpsipi_comb_old,"old");
    legend->AddEntry(Jpsipi_2d_bg_comb,"SC");
    
    TCanvas * c_JpsipiX = new TCanvas("jpsipi_mass_ProjectionX");
    Jpsipi_comb->GetXaxis()->SetRangeUser(0., 0.1);
    Jpsipi_comb->ProjectionX()->DrawNormalized("hist");
    Jpsipi_comb_old->ProjectionX()->DrawNormalized("same hist");
    
    legend->Draw();
    c_JpsipiX->SaveAs("compare_Jpsipi_X_SA_bg.png");
    
    TCanvas * c_JpsiKX = new TCanvas("JpsiK_mass_ProjectionX");
    JpsiK_comb->ProjectionX()->DrawNormalized("hist");
    JpsiK_comb_old->ProjectionX()->DrawNormalized("same hist");
    legend->Draw();
    c_JpsiKX->SaveAs("compare_JpsiK_X_SA_bg.png");
    
    TCanvas * c_KpiX = new TCanvas("Kpi_mass_ProjectionX");
    Kpi_2d_bg_comb->ProjectionX()->DrawNormalized("same hist");
    Kpi_comb->ProjectionX()->DrawNormalized("same hist");
    Kpi_comb_old->ProjectionX()->DrawNormalized("same hist");
    
    legend->Draw();
    c_KpiX->SaveAs("compare_Kpi_X_SA_bg.png");
    
    TCanvas * c_JpsipiY = new TCanvas("jpsipi_mass_ProjectionY");
    Jpsipi_comb->GetYaxis()->SetRangeUser(0., 0.1);
    Jpsipi_comb->ProjectionY()->DrawNormalized("hist");
    Jpsipi_comb_old->ProjectionY()->DrawNormalized("same hist");
    legend->Draw();
    c_JpsipiY->SaveAs("compare_Jpsipi_Y_SA_bg.png");
    
    TCanvas * c_JpsiKY = new TCanvas("JpsiK_mass_ProjectionY");
    JpsiK_comb->ProjectionY()->DrawNormalized("hist");
    JpsiK_comb_old->ProjectionY()->DrawNormalized("same hist");
    legend->Draw();
    c_JpsiKY->SaveAs("compare_JpsiK_Y_SA_bg.png");
    
    TCanvas * c_KpiY = new TCanvas("Kpi_mass_ProjectionY");
    Kpi_2d_bg_comb->ProjectionY()->DrawNormalized("same hist");
    Kpi_comb->ProjectionY()->DrawNormalized("same hist");
    Kpi_comb_old->ProjectionY()->DrawNormalized("same hist");
    legend->Draw();
    c_KpiY->SaveAs("compare_Kpi_Y_SA_bg.png");
    
    TCanvas * c_trk_pt_X = new TCanvas("trk_pt_ProjectionX");
    trk_pt_bg_comb->ProjectionX()->DrawNormalized("same hist");
    trk_pt_comb->ProjectionX()->DrawNormalized("same hist");
    legend->Draw();
    c_KpiX->SaveAs("compare_trk_pt_X_SA_bg.png");
    
    TCanvas * c_trk_pt_Y = new TCanvas("trk_pt_ProjectionY");
    trk_pt_bg_comb->ProjectionY()->DrawNormalized("same hist");
    trk_pt_comb->ProjectionY()->DrawNormalized("same hist");
    legend->Draw();
    c_KpiY->SaveAs("compare_trk_pt_Y_SA_bg.png");
}
