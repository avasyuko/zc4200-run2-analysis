In that folder you can find scripts for obtaining combinatorial background in signal and control areas.

To run scripts you should use commands:

signal area:
[root 0] .L Bd_decay_amplitude.c
[root 1] event_selector_comb.c

control area BsKK decays:
[root 0] .L event_selector_comb_control_BsKK.c
[root 1] main()

After success running of that programs don`t forget to put files cos_signal_bg.root, signal_area_bg.root and control_area_BsKK_bg.root to ROOT_files folder.

