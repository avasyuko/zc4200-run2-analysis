{
    TChain BsAllCandidates("BsAllCandidates");
    
    // Possible modes:
    // - "comb_bg"
    // That is selection for obtaining
    // comb_bg in signal and control areas
    // - "default"
    // That is default selection for slimming
    // ntuples (if you want to vary selection later)
    // - "slim"
    // 
    // - "full"
    // That is final selection for events
    // from "global area". One should use it
    // if it is no necessarity of selection variation
    
    std::string selection_mode = "slim";
    
    std::string dataset = "data16Main";
    // One shold run that scripts for different values of dataset variable.
    // Possible values:
    // 1) "data15"
    // 2) "data16Main"
    // 3) "data16delayed"
    // 4) "data17"
    // 5) "data18"
    
    Double_t GEV = 0.001;
    
    int year;
    
    std::string sf_name = std::string("datasets/") + dataset.c_str() + std::string(".root");
    
    if (selection_mode == "comb_bg")
    {
        sf_name = std::string("datasets/") + dataset.c_str() + std::string("_for_comb.root");
    }
    
    if (selection_mode == "slim")
    {
        sf_name = std::string("datasets/") + dataset.c_str() + std::string("_slim.root");
    }
    
    cout << sf_name << endl;
    TFile *sf = new TFile(sf_name.c_str(), "recreate");
    
    if (dataset == "data15")
    {
        // here your should put the output of command
        //  . data15.sh 
        // from Data15 ntuples folder 
        // Don't forget to change <ntuples_path> in data15_dataset.py
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579041._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579066._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579073._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579078._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579082._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579085._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579089._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579094._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579096._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579096._000002.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579096._000003.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579096._000004.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579101._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579110._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579113._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579116._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579119._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579120._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579129._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579135._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579145._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579152._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579160._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579169._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579177._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579184._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579192._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579204._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579212._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579220._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579227._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579234._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579241._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579248._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579254._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579261._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579284._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579298._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579304._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579313._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579318._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579321._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579323._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579329._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579337._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579347._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579368._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579376._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579384._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579391._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579400._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579417._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579425._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579427._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579429._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579431._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579432._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579433._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579443._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579463._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579484._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579504._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579510._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579511._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579512._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579520._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579527._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579529._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579530._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579531._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579532._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579533._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579535._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579536._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579539._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579543._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579544._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579545._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579546._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579547._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579556._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579558._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579559._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579561._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579562._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579564._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579566._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579567._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579568._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579569._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579571._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579573._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579574._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579576._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579583._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579600._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579607._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579614._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579622._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579629._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579643._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579650._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579658._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579664._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579675._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579685._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579692._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22579698._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data15_WP/user.avasyuko.22592621._000001.DefaultOutput.root");
        year = 15;
    }
    
    if (dataset == "data16Main")
    {
        // here your should put the output of command
        //  . data16Main.sh 
        // from Data16Main_WP ntuples folder 
        // Don't forget to change <ntuples_path> in data16Main_dataset.py 
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22579783._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22579794._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22579809._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22579824._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22579835._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22579846._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22579856._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22579865._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22579865._000002.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22579876._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22579886._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22579896._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22579906._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22579916._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580117._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580126._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580134._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580146._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580159._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580159._000002.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580166._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580176._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580176._000002.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580186._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580193._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580204._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580222._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580222._000002.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580235._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580265._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580282._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580298._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580316._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580336._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580352._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580368._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580390._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580408._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580417._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580427._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580443._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580466._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580466._000002.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580466._000003.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580477._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580490._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580514._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580543._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580567._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580589._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580628._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580655._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580662._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580672._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580694._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580705._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580715._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580715._000002.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580724._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580735._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580756._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580763._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580764._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580765._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580771._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580775._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580776._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580777._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580777._000002.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580780._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580789._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580791._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580792._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580793._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580794._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580795._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580796._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580797._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580798._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580799._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580800._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580805._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580820._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580833._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580835._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580841._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580847._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580874._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580874._000002.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580902._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580932._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580960._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22580985._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581006._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581007._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581011._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581017._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581022._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581029._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581032._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581043._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581070._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581083._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581098._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581115._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581149._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581162._000003.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581165._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581169._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581177._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581199._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581206._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581207._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581208._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581209._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581223._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581224._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581225._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581226._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581227._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581228._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581230._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581233._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581239._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581241._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581242._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581243._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581244._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581245._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581246._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581249._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581251._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581252._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581253._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581253._000002.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581254._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581255._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581255._000002.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581256._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581257._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581258._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581259._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581260._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581265._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581273._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581280._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581282._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581286._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581290._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581292._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581293._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581294._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581295._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581296._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581297._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581299._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581300._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581301._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581302._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581304._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581306._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581308._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581310._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581311._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581312._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22581316._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16Main_WP/user.avasyuko.22593202._000001.DefaultOutput.root");      
        year = 16;
    }
    
    if (dataset == "data16delayed")
    {
        // here your should put the output of command
        //  . data16delayed.sh 
        // from Data16delayed_WP ntuples folder 
        // Don't forget to change <ntuples_path> in data16delayed_dataset.py 
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581429._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581430._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581431._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581432._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581433._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581434._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581435._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581438._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581440._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581441._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581442._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581446._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581450._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581451._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581453._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581454._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581456._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581458._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581463._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581472._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581477._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581483._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581491._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581495._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581495._000002.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581511._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581516._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581517._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581518._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581518._000002.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581518._000003.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581518._000004.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581529._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581530._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581532._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581543._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581545._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581546._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581554._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581558._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581560._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581567._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581568._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581568._000002.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581568._000003.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581568._000004.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581577._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581597._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581631._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581664._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581690._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581715._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581726._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581728._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581730._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581731._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581733._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581735._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581742._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581750._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581750._000002.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581751._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581758._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581785._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581794._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581795._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581796._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581797._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581800._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581801._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581802._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581803._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581804._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581805._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581807._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581808._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581810._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581811._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581814._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581818._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581820._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581822._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581824._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581826._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581828._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581830._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581832._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581834._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581834._000002.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581842._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581852._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581861._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581872._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581879._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581881._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581881._000002.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581881._000003.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581884._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581889._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581899._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581908._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581917._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581945._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581971._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22581996._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22582018._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22582020._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22582027._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22582038._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22582048._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22582057._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22582068._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22582076._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22582079._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22582081._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22582090._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22582110._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22582149._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22582174._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22582184._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22582192._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22582194._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22582195._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22582197._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22582200._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22582202._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22582204._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data16delayed_WP/user.avasyuko.22582206._000001.DefaultOutput.root");
        year = 16;
    }
    
    if (dataset == "data17")
    {
        // here your should put the output of command
        //  . data17.sh 
        // from Data17BPhysLS_WP ntuples folder 
        // Don't forget to change <ntuples_path> in data17_dataset.py 
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587636._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587637._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587638._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587639._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587641._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587642._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587643._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587644._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587645._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587646._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587647._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587648._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587649._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587650._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587652._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587658._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587662._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587666._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587670._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587674._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587677._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587681._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587684._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587685._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587686._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587688._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587689._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587690._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587691._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587692._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587713._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587738._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587751._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587755._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587757._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587758._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587759._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587762._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587767._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587771._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587772._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587773._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587774._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587775._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587777._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587783._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587790._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587796._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587801._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587802._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587803._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587804._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587805._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587808._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587825._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587848._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587869._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587870._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587871._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587872._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587873._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587874._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587875._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587876._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587877._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587878._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587879._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587880._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587881._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587882._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587883._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587885._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587890._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587896._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587906._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587914._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587918._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587922._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587927._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587932._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587936._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587939._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587940._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587941._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587943._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587944._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587948._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587953._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587957._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587961._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587966._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587970._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587976._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587981._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587984._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587991._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587997._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22587999._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588000._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588001._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588006._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588012._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588019._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588024._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588028._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588031._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588035._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588036._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588044._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588063._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588080._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588101._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588102._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588103._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588104._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588105._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588106._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588107._000004.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588108._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588109._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588110._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588111._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588112._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588113._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588114._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588115._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588116._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588117._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588118._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588119._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588120._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588121._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588122._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588123._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588124._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588125._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588127._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588130._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588133._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588136._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588139._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588142._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588145._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588148._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588150._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588153._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588157._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588160._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588163._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588165._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588168._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588175._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588180._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588185._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588188._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588192._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588194._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588207._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588211._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588214._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588217._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588220._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588223._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588225._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588229._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588231._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588234._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588238._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588242._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588245._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588247._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588256._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588285._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588309._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588317._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588318._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588319._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588326._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588327._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588328._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588329._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588330._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588331._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588332._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588333._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588335._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588336._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588337._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588338._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588339._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588340._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588347._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588352._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588356._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588357._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588360._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588363._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588364._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22588365._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data17delayed_WP/user.avasyuko.22594003._000001.DefaultOutput.root");
        year = 17;
    }
    
    if (dataset == "data18")
    {
        // here your should put the output of command
        //  . data18.sh 
        // from Data18BPhysLS_WP ntuples folder 
        // Don't forget to change <ntuples_path> in data18_dataset.py 
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584613._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584632._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584648._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584665._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584666._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584667._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584668._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584669._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584670._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584671._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584672._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584673._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584674._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584676._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584677._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584678._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584679._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584680._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584681._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584682._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584683._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584684._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584686._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584687._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584689._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584690._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584691._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584692._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584693._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584694._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584695._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584696._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584697._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584698._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584699._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584700._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584701._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584702._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584703._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584704._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584706._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584707._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584708._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584709._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584710._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584711._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584712._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584715._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584716._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584717._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584718._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584719._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584719._000002.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584719._000003.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584719._000004.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584720._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584721._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584723._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584724._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584725._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584726._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584727._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584728._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584730._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584732._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584734._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584745._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584762._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584780._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584801._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584821._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584822._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584824._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584825._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584826._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584827._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584829._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584830._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584831._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584832._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584835._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584836._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584837._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584838._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584839._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584842._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584843._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584844._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584845._000002.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584846._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584847._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584849._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584850._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584852._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584853._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584855._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584856._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584858._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584861._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584862._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584863._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584864._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584865._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584866._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584867._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584868._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584869._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584871._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584871._000002.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584871._000003.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584871._000004.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584871._000005.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584871._000006.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584873._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584874._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584875._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584876._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584877._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584880._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584882._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584884._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584885._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584886._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584887._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584888._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584889._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584890._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584892._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584893._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584894._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584896._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584897._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584898._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584899._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584903._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584913._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584929._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584950._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584969._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584985._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584990._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584992._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584994._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584995._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584996._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584997._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22584998._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585000._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585002._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585003._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585005._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585006._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585007._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585008._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585008._000002.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585008._000003.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585008._000004.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585008._000005.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585009._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585011._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585012._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585013._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585014._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585015._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585016._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585018._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585019._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585020._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585021._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585022._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585023._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585024._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585025._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585027._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585028._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585029._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585030._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585031._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585032._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585033._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585035._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585036._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585037._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585038._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585039._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585040._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585041._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585043._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585044._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585045._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585046._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585047._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585048._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585049._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585051._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585053._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585054._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585055._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585056._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585057._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585058._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585061._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585067._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585075._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585083._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585090._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585095._000001.DefaultOutput.root");
        BsAllCandidates.AddFile("~/BPHY11_15/datasets/data18BPhysLS_WP/user.avasyuko.22585113._000001.DefaultOutput.root");      
        year = 18;
    }
        
    TTree *stree = new TTree("stree", "stree");
    
    float mu1_pt, mu2_pt, trk1_pt, trk2_pt, mu1_px, mu1_py, mu1_pz, mu2_px, mu2_py, mu2_pz, trk1_px, trk1_py, trk1_pz, trk2_px, trk2_py, trk2_pz, trk1_charge, trk2_charge, Lxy_MaxSumPt, Bs_pt, Bs_chi2_ndof, MaxSumPt, mu1_eta, mu2_eta, trk1_eta, trk2_eta, Jpsi_mass, Old_Jpsi_chi2, mu1_px_ReFit_pK, mu1_py_ReFit_pK, mu1_pz_ReFit_pK, mu2_px_ReFit_pK, mu2_py_ReFit_pK, mu2_pz_ReFit_pK, trk1_px_ReFit_pK, trk1_py_ReFit_pK, trk1_pz_ReFit_pK, trk2_px_ReFit_pK, trk2_py_ReFit_pK, trk2_pz_ReFit_pK, mu1_px_ReFit_Kp, mu1_py_ReFit_Kp, mu1_pz_ReFit_Kp, mu2_px_ReFit_Kp, mu2_py_ReFit_Kp, mu2_pz_ReFit_Kp, trk1_px_ReFit_Kp, trk1_py_ReFit_Kp, trk1_pz_ReFit_Kp, trk2_px_ReFit_Kp, trk2_py_ReFit_Kp, trk2_pz_ReFit_Kp, mu1_charge, mu2_charge, MinA0SumPt, Lxy_MinA0;
    int ReFit_result;
        
    TBranch *sB_mu1_pt = stree->Branch("sB_mu1_pt", &mu1_pt);
    TBranch *sB_mu2_pt = stree->Branch("sB_mu2_pt", &mu2_pt);
    TBranch *sB_trk1_pt = stree->Branch("sB_trk1_pt", &trk1_pt);
    TBranch *sB_trk2_pt = stree->Branch("sB_trk2_pt", &trk2_pt);
    TBranch *sB_mu1_px = stree->Branch("sB_mu1_px", &mu1_px);
    TBranch *sB_mu1_py = stree->Branch("sB_mu1_py", &mu1_py);
    TBranch *sB_mu1_pz = stree->Branch("sB_mu1_pz", &mu1_pz);
    TBranch *sB_mu2_px = stree->Branch("sB_mu2_px", &mu2_px);
    TBranch *sB_mu2_py = stree->Branch("sB_mu2_py", &mu2_py);
    TBranch *sB_mu2_pz = stree->Branch("sB_mu2_pz", &mu2_pz);
    TBranch *sB_trk1_px = stree->Branch("sB_trk1_px", &trk1_px);
    TBranch *sB_trk1_py = stree->Branch("sB_trk1_py", &trk1_py);
    TBranch *sB_trk1_pz = stree->Branch("sB_trk1_pz", &trk1_pz);
    TBranch *sB_trk2_px = stree->Branch("sB_trk2_px", &trk2_px);
    TBranch *sB_trk2_py = stree->Branch("sB_trk2_py", &trk2_py);
    TBranch *sB_trk2_pz = stree->Branch("sB_trk2_pz", &trk2_pz);
    TBranch *sB_trk1_charge = stree->Branch("sB_trk1_charge", &trk1_charge);
    TBranch *sB_trk2_charge = stree->Branch("sB_trk2_charge", &trk2_charge);
    TBranch *sB_MaxSumPt = stree->Branch("sB_MaxSumPt", &MaxSumPt);  
    TBranch *sB_MinA0SumPt = stree->Branch("sB_MinA0SumPt", &MinA0SumPt);
    TBranch *sB_mu1_eta = stree->Branch("sB_mu1_eta", &mu1_eta);
    TBranch *sB_mu2_eta = stree->Branch("sB_mu2_eta", &mu2_eta);
    TBranch *sB_trk1_eta = stree->Branch("sB_trk1_eta", &trk1_eta);
    TBranch *sB_trk2_eta = stree->Branch("sB_trk2_eta", &trk2_eta);
    TBranch *sB_Bs_pt = stree->Branch("sB_Bs_pt", &Bs_pt);
    TBranch *sB_Bs_chi2_ndof = stree->Branch("sB_Bs_chi2_ndof", &Bs_chi2_ndof);
    TBranch *sB_Lxy_MaxSumPt = stree->Branch("sB_Lxy_MaxSumPt", &Lxy_MaxSumPt);
    TBranch *sB_Lxy_MinA0 = stree->Branch("sB_Lxy_MinA0", &Lxy_MinA0);
    TBranch *sB_Jpsi_mass = stree->Branch("sB_Jpsi_mass", &Jpsi_mass);
    TBranch *sB_Jpsi_chi2 = stree->Branch("sB_Jpsi_chi2", &Old_Jpsi_chi2);
    TBranch *sB_mu1_px_ReFit_pK = stree->Branch("sB_mu1_px_ReFit_pK", &mu1_px_ReFit_pK);
    TBranch *sB_mu1_py_ReFit_pK = stree->Branch("sB_mu1_py_ReFit_pK", &mu1_py_ReFit_pK);
    TBranch *sB_mu1_pz_ReFit_pK = stree->Branch("sB_mu1_pz_ReFit_pK", &mu1_pz_ReFit_pK);
    TBranch *sB_mu2_px_ReFit_pK = stree->Branch("sB_mu2_px_ReFit_pK", &mu2_px_ReFit_pK);
    TBranch *sB_mu2_py_ReFit_pK = stree->Branch("sB_mu2_py_ReFit_pK", &mu2_py_ReFit_pK);
    TBranch *sB_mu2_pz_ReFit_pK = stree->Branch("sB_mu2_pz_ReFit_pK", &mu2_pz_ReFit_pK);
    TBranch *sB_trk1_px_ReFit_pK = stree->Branch("sB_trk1_px_ReFit_pK", &trk1_px_ReFit_pK);
    TBranch *sB_trk1_py_ReFit_pK = stree->Branch("sB_trk1_py_ReFit_pK", &trk1_py_ReFit_pK);
    TBranch *sB_trk1_pz_ReFit_pK = stree->Branch("sB_trk1_pz_ReFit_pK", &trk1_pz_ReFit_pK);
    TBranch *sB_trk2_px_ReFit_pK = stree->Branch("sB_trk2_px_ReFit_pK", &trk2_px_ReFit_pK);
    TBranch *sB_trk2_py_ReFit_pK = stree->Branch("sB_trk2_py_ReFit_pK", &trk2_py_ReFit_pK);
    TBranch *sB_trk2_pz_ReFit_pK = stree->Branch("sB_trk2_pz_ReFit_pK", &trk2_pz_ReFit_pK);
    TBranch *sB_mu1_px_ReFit_Kp = stree->Branch("sB_mu1_px_ReFit_Kp", &mu1_px_ReFit_Kp);
    TBranch *sB_mu1_py_ReFit_Kp = stree->Branch("sB_mu1_py_ReFit_Kp", &mu1_py_ReFit_Kp);
    TBranch *sB_mu1_pz_ReFit_Kp = stree->Branch("sB_mu1_pz_ReFit_Kp", &mu1_pz_ReFit_Kp);
    TBranch *sB_mu2_px_ReFit_Kp = stree->Branch("sB_mu2_px_ReFit_Kp", &mu2_px_ReFit_Kp);
    TBranch *sB_mu2_py_ReFit_Kp = stree->Branch("sB_mu2_py_ReFit_Kp", &mu2_py_ReFit_Kp);
    TBranch *sB_mu2_pz_ReFit_Kp = stree->Branch("sB_mu2_pz_ReFit_Kp", &mu2_pz_ReFit_Kp);
    TBranch *sB_trk1_px_ReFit_Kp = stree->Branch("sB_trk1_px_ReFit_Kp", &trk1_px_ReFit_Kp);
    TBranch *sB_trk1_py_ReFit_Kp = stree->Branch("sB_trk1_py_ReFit_Kp", &trk1_py_ReFit_Kp);
    TBranch *sB_trk1_pz_ReFit_Kp = stree->Branch("sB_trk1_pz_ReFit_Kp", &trk1_pz_ReFit_Kp);
    TBranch *sB_trk2_px_ReFit_Kp = stree->Branch("sB_trk2_px_ReFit_Kp", &trk2_px_ReFit_Kp);
    TBranch *sB_trk2_py_ReFit_Kp = stree->Branch("sB_trk2_py_ReFit_Kp", &trk2_py_ReFit_Kp);
    TBranch *sB_trk2_pz_ReFit_Kp = stree->Branch("sB_trk2_pz_ReFit_Kp", &trk2_pz_ReFit_Kp);
    TBranch *sB_ReFit_result = stree->Branch("sB_ReFit_result", &ReFit_result);
    TBranch *sB_mu1_charge = stree->Branch("sB_mu1_charge", &mu1_charge);
    TBranch *sB_mu2_charge = stree->Branch("sB_mu2_charge", &mu2_charge);
    TBranch *sB_year = stree->Branch("sB_year", &year);
    
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo__  STITCHING PART  __oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
//__oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo____oO0Oo__//
  
    Long64_t nentries = BsAllCandidates.GetEntries();
    
    vector<float> *B_mu1_px, *B_mu1_py, *B_mu1_pz, *B_mu2_px, *B_mu2_py, *B_mu2_pz, *B_trk1_px, *B_trk1_py, *B_trk1_pz, *B_trk2_px, *B_trk2_py, *B_trk2_pz, *B_trk1_charge, *B_trk2_charge, *B_Lxy_MaxSumPt, *B_pT, *B_mu1_pT, *B_mu2_pT, *B_trk1_pT, *B_trk2_pT, *B_chi2_ndof, *MaxSumPt_SumPt, *B_mu1_eta, *B_mu2_eta, *B_trk1_eta, *B_trk2_eta, *B_Jpsi_mass, *B_Jpsi_chi2, *B_mu1_px_ReFit_pK, *B_mu1_py_ReFit_pK, *B_mu1_pz_ReFit_pK, *B_mu2_px_ReFit_pK, *B_mu2_py_ReFit_pK, *B_mu2_pz_ReFit_pK, *B_trk1_px_ReFit_pK, *B_trk1_py_ReFit_pK, *B_trk1_pz_ReFit_pK, *B_trk2_px_ReFit_pK, *B_trk2_py_ReFit_pK, *B_trk2_pz_ReFit_pK, *B_mu1_px_ReFit_Kp, *B_mu1_py_ReFit_Kp, *B_mu1_pz_ReFit_Kp, *B_mu2_px_ReFit_Kp, *B_mu2_py_ReFit_Kp, *B_mu2_pz_ReFit_Kp, *B_trk1_px_ReFit_Kp, *B_trk1_py_ReFit_Kp, *B_trk1_pz_ReFit_Kp, *B_trk2_px_ReFit_Kp, *B_trk2_py_ReFit_Kp, *B_trk2_pz_ReFit_Kp, *B_mu1_charge, *B_mu2_charge, *MinA0_SumPt, *B_Lxy_MinA0;
    vector<int> *B_ReFit_result;
    UInt_t B_RunNumber;
    vector<bool> *HLT_mu6_mu4_bJpsimumu_noL2_isPassed, *HLT_2mu4_bJpsimumu_noL2_isPassed,
    *HLT_mu6_mu4_bJpsimumu_isPassed, 
    *HLT_mu10_mu6_bJpsimumu_isPassed,
    *HLT_2mu6_bJpsimumu_isPassed, 
    *HLT_mu20_2mu0noL1_JpsimumuFS_isPassed,
    *HLT_mu6_2mu4_bJpsi_isPassed,
    *HLT_mu20_nomucomb_mu6noL1_nscan03_isPassed,
    *HLT_2mu6_bJpsimumu_delayed_isPassed,
    *HLT_mu6_mu4_bJpsimumu_Lxy0_delayed_isPassed,
    *HLT_mu6_nomucomb_2mu4_nomucomb_delayed_L1MU6_3MU4_isPassed,
    *HLT_3mu4_bJpsi_delayed_isPassed,
    *HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6_isPassed,
    *HLT_mu6_mu4_bJpsimumu_Lxy0_L1BPH_2M9_MU6MU4_BPH_0DR15_MU6MU4_isPassed,
    *HLT_mu11_mu6_bDimu_isPassed,
    *HLT_3mu4_bJpsi_isPassed,
    *HLT_mu11_2mu4noL1_bNocut_L1MU11_2MU6_isPassed,
    *HLT_2mu4_bJpsimumu_Lxy0_L1BPH_2M9_2MU4_BPH_0DR15_2MU4_isPassed;
    
    vector<Int_t> *quality_mu1, *quality_mu2, *Pixelhits_had1, *Pixelhits_had2, *PixelShared_had1, *PixelShared_had2, *PixelDeadSensors_had1, *PixelDeadSensors_had2, *SCThitshad1, *SCThitshad2, *SCTShared_had1, *SCTShared_had2, *SCTDeadSensors_had1, *SCTDeadSensors_had2, *SCTHoles_had1, *SCTHoles_had2, *PixelHoles_had1, *PixelHoles_had2;
    
    int n;
    
    BsAllCandidates.SetBranchAddress("B_mu1_px", &B_mu1_px);
    BsAllCandidates.SetBranchAddress("B_mu1_py", &B_mu1_py);
    BsAllCandidates.SetBranchAddress("B_mu1_pz", &B_mu1_pz);
    BsAllCandidates.SetBranchAddress("B_mu2_px", &B_mu2_px);
    BsAllCandidates.SetBranchAddress("B_mu2_py", &B_mu2_py);
    BsAllCandidates.SetBranchAddress("B_mu2_pz", &B_mu2_pz);
    BsAllCandidates.SetBranchAddress("B_trk1_px", &B_trk1_px);
    BsAllCandidates.SetBranchAddress("B_trk1_py", &B_trk1_py);
    BsAllCandidates.SetBranchAddress("B_trk1_pz", &B_trk1_pz);
    BsAllCandidates.SetBranchAddress("B_trk2_px", &B_trk2_px);
    BsAllCandidates.SetBranchAddress("B_trk2_py", &B_trk2_py);
    BsAllCandidates.SetBranchAddress("B_trk2_pz", &B_trk2_pz);
    BsAllCandidates.SetBranchAddress("B_trk1_charge", &B_trk1_charge);
    BsAllCandidates.SetBranchAddress("B_trk2_charge", &B_trk2_charge);
    BsAllCandidates.SetBranchAddress("B_Lxy_MaxSumPt", &B_Lxy_MaxSumPt);
    BsAllCandidates.SetBranchAddress("B_Lxy_MinA0", &B_Lxy_MinA0);
    BsAllCandidates.SetBranchAddress("B_pT", &B_pT);
    BsAllCandidates.SetBranchAddress("B_mu1_pT", &B_mu1_pT);
    BsAllCandidates.SetBranchAddress("B_mu2_pT", &B_mu2_pT);
    BsAllCandidates.SetBranchAddress("B_trk1_pT", &B_trk1_pT);
    BsAllCandidates.SetBranchAddress("B_trk2_pT", &B_trk2_pT);
    BsAllCandidates.SetBranchAddress("B_chi2_ndof", &B_chi2_ndof);
    BsAllCandidates.SetBranchAddress("MaxSumPt_SumPt", &MaxSumPt_SumPt);
    BsAllCandidates.SetBranchAddress("MinA0_SumPt", &MinA0_SumPt);
    BsAllCandidates.SetBranchAddress("B_mu1_eta", &B_mu1_eta);
    BsAllCandidates.SetBranchAddress("B_mu2_eta", &B_mu2_eta);
    BsAllCandidates.SetBranchAddress("B_trk1_eta", &B_trk1_eta);
    BsAllCandidates.SetBranchAddress("B_trk2_eta", &B_trk2_eta);
    BsAllCandidates.SetBranchAddress("B_Jpsi_mass", &B_Jpsi_mass);
    BsAllCandidates.SetBranchAddress("Jpsi_chi2", &B_Jpsi_chi2);
    BsAllCandidates.SetBranchAddress("B_mu1_px_ReFit_pK", &B_mu1_px_ReFit_pK);
    BsAllCandidates.SetBranchAddress("B_mu1_py_ReFit_pK", &B_mu1_py_ReFit_pK);
    BsAllCandidates.SetBranchAddress("B_mu1_pz_ReFit_pK", &B_mu1_pz_ReFit_pK);
    BsAllCandidates.SetBranchAddress("B_mu2_px_ReFit_pK", &B_mu2_px_ReFit_pK);
    BsAllCandidates.SetBranchAddress("B_mu2_py_ReFit_pK", &B_mu2_py_ReFit_pK);
    BsAllCandidates.SetBranchAddress("B_mu2_pz_ReFit_pK", &B_mu2_pz_ReFit_pK);
    BsAllCandidates.SetBranchAddress("B_trk1_px_ReFit_pK", &B_trk1_px_ReFit_pK);
    BsAllCandidates.SetBranchAddress("B_trk1_py_ReFit_pK", &B_trk1_py_ReFit_pK);
    BsAllCandidates.SetBranchAddress("B_trk1_pz_ReFit_pK", &B_trk1_pz_ReFit_pK);
    BsAllCandidates.SetBranchAddress("B_trk2_px_ReFit_pK", &B_trk2_px_ReFit_pK);
    BsAllCandidates.SetBranchAddress("B_trk2_py_ReFit_pK", &B_trk2_py_ReFit_pK);
    BsAllCandidates.SetBranchAddress("B_trk2_pz_ReFit_pK", &B_trk2_pz_ReFit_pK);
    BsAllCandidates.SetBranchAddress("B_mu1_px_ReFit_Kp", &B_mu1_px_ReFit_Kp);
    BsAllCandidates.SetBranchAddress("B_mu1_py_ReFit_Kp", &B_mu1_py_ReFit_Kp);
    BsAllCandidates.SetBranchAddress("B_mu1_pz_ReFit_Kp", &B_mu1_pz_ReFit_Kp);
    BsAllCandidates.SetBranchAddress("B_mu2_px_ReFit_Kp", &B_mu2_px_ReFit_Kp);
    BsAllCandidates.SetBranchAddress("B_mu2_py_ReFit_Kp", &B_mu2_py_ReFit_Kp);
    BsAllCandidates.SetBranchAddress("B_mu2_pz_ReFit_Kp", &B_mu2_pz_ReFit_Kp);
    BsAllCandidates.SetBranchAddress("B_trk1_px_ReFit_Kp", &B_trk1_px_ReFit_Kp);
    BsAllCandidates.SetBranchAddress("B_trk1_py_ReFit_Kp", &B_trk1_py_ReFit_Kp);
    BsAllCandidates.SetBranchAddress("B_trk1_pz_ReFit_Kp", &B_trk1_pz_ReFit_Kp);
    BsAllCandidates.SetBranchAddress("B_trk2_px_ReFit_Kp", &B_trk2_px_ReFit_Kp);
    BsAllCandidates.SetBranchAddress("B_trk2_py_ReFit_Kp", &B_trk2_py_ReFit_Kp);
    BsAllCandidates.SetBranchAddress("B_trk2_pz_ReFit_Kp", &B_trk2_pz_ReFit_Kp);
    BsAllCandidates.SetBranchAddress("B_ReFit_result", &B_ReFit_result);
    BsAllCandidates.SetBranchAddress("B_mu1_charge", &B_mu1_charge);
    BsAllCandidates.SetBranchAddress("B_mu2_charge", &B_mu2_charge);
    BsAllCandidates.SetBranchAddress("run_number", &B_RunNumber);
    BsAllCandidates.SetBranchAddress("quality_mu1", &quality_mu1);
    BsAllCandidates.SetBranchAddress("quality_mu2", &quality_mu2);
    BsAllCandidates.SetBranchAddress("Pixelhits_had1", &Pixelhits_had1);
    BsAllCandidates.SetBranchAddress("Pixelhits_had2", &Pixelhits_had2);
    BsAllCandidates.SetBranchAddress("PixelShared_had1", &PixelShared_had1);
    BsAllCandidates.SetBranchAddress("PixelShared_had2", &PixelShared_had2);
    BsAllCandidates.SetBranchAddress("PixelDeadSensors_had1", &PixelDeadSensors_had1);
    BsAllCandidates.SetBranchAddress("PixelDeadSensors_had2", &PixelDeadSensors_had2);
    BsAllCandidates.SetBranchAddress("SCThitshad1", &SCThitshad1);
    BsAllCandidates.SetBranchAddress("SCThitshad2", &SCThitshad2);
    BsAllCandidates.SetBranchAddress("SCTShared_had1", &SCTShared_had1);
    BsAllCandidates.SetBranchAddress("SCTShared_had2", &SCTShared_had2);
    BsAllCandidates.SetBranchAddress("SCTDeadSensors_had1", &SCTDeadSensors_had1);
    BsAllCandidates.SetBranchAddress("SCTDeadSensors_had2", &SCTDeadSensors_had2);
    BsAllCandidates.SetBranchAddress("SCTHoles_had1", &SCTHoles_had1);
    BsAllCandidates.SetBranchAddress("SCTHoles_had2", &SCTHoles_had2);
    BsAllCandidates.SetBranchAddress("PixelHoles_had1", &PixelHoles_had1);
    BsAllCandidates.SetBranchAddress("PixelHoles_had2", &PixelHoles_had2); 
    
    if (dataset == "data15")
    {
        BsAllCandidates.SetBranchAddress("HLT_mu6_mu4_bJpsimumu_noL2_isPassed", &HLT_mu6_mu4_bJpsimumu_noL2_isPassed);
        BsAllCandidates.SetBranchAddress("HLT_2mu4_bJpsimumu_noL2_isPassed", &HLT_2mu4_bJpsimumu_noL2_isPassed);
    }
    
    if (dataset == "data16Main")
    {
        BsAllCandidates.SetBranchAddress("HLT_mu10_mu6_bJpsimumu_isPassed", &HLT_mu10_mu6_bJpsimumu_isPassed);
        BsAllCandidates.SetBranchAddress("HLT_mu6_mu4_bJpsimumu_isPassed", &HLT_mu6_mu4_bJpsimumu_isPassed);
        BsAllCandidates.SetBranchAddress("HLT_2mu6_bJpsimumu_isPassed", &HLT_2mu6_bJpsimumu_isPassed);
        BsAllCandidates.SetBranchAddress("HLT_mu6_2mu4_bJpsi_isPassed", &HLT_mu6_2mu4_bJpsi_isPassed);
    }
    
    if (dataset == "data16delayed")
    {
        BsAllCandidates.SetBranchAddress("HLT_2mu6_bJpsimumu_delayed_isPassed", &HLT_2mu6_bJpsimumu_delayed_isPassed);
        BsAllCandidates.SetBranchAddress("HLT_mu6_mu4_bJpsimumu_Lxy0_delayed_isPassed", &HLT_mu6_mu4_bJpsimumu_Lxy0_delayed_isPassed);
        BsAllCandidates.SetBranchAddress("HLT_mu6_nomucomb_2mu4_nomucomb_delayed_L1MU6_3MU4_isPassed", &HLT_mu6_nomucomb_2mu4_nomucomb_delayed_L1MU6_3MU4_isPassed);
        BsAllCandidates.SetBranchAddress("HLT_3mu4_bJpsi_delayed_isPassed", &HLT_3mu4_bJpsi_delayed_isPassed);
    }
    
    if (dataset == "data17")
    {
        BsAllCandidates.SetBranchAddress("HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6_isPassed", &HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6_isPassed);
        BsAllCandidates.SetBranchAddress("HLT_mu11_mu6_bDimu_isPassed", &HLT_mu11_mu6_bDimu_isPassed);
        BsAllCandidates.SetBranchAddress("HLT_3mu4_bJpsi_isPassed", &HLT_3mu4_bJpsi_isPassed);
        BsAllCandidates.SetBranchAddress("HLT_mu6_mu4_bJpsimumu_Lxy0_L1BPH_2M9_MU6MU4_BPH_0DR15_MU6MU4_isPassed", &HLT_mu6_mu4_bJpsimumu_Lxy0_L1BPH_2M9_MU6MU4_BPH_0DR15_MU6MU4_isPassed);
    }
    
    if (dataset == "data18")
    {   
        BsAllCandidates.SetBranchAddress("HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6_isPassed", &HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6_isPassed);
        BsAllCandidates.SetBranchAddress("HLT_mu11_mu6_bDimu_isPassed", &HLT_mu11_mu6_bDimu_isPassed);
        BsAllCandidates.SetBranchAddress("HLT_mu6_mu4_bJpsimumu_Lxy0_L1BPH_2M9_MU6MU4_BPH_0DR15_MU6MU4_isPassed", &HLT_mu6_mu4_bJpsimumu_Lxy0_L1BPH_2M9_MU6MU4_BPH_0DR15_MU6MU4_isPassed);
        BsAllCandidates.SetBranchAddress("HLT_3mu4_bJpsi_isPassed", &HLT_3mu4_bJpsi_isPassed);
        BsAllCandidates.SetBranchAddress("HLT_2mu4_bJpsimumu_Lxy0_L1BPH_2M9_2MU4_BPH_0DR15_2MU4_isPassed", &HLT_2mu4_bJpsimumu_Lxy0_L1BPH_2M9_2MU4_BPH_0DR15_2MU4_isPassed);
    }
    
    cout << "Processing data datasets -----> number of \"dirty\" entries " << nentries << endl;
    
    for(int i = 1; i <= nentries; i++)
    {
            BsAllCandidates.GetEntry(i);
            if (i % 100000 == 0) {cout << "events passed " << i << endl;}
            if (dataset == "data16Main")
            {
                if (B_RunNumber > 302925) {continue;}
            }
            for(int k = 0; k < B_mu1_px->size(); k++)
            {
                
                if (dataset == "data15")
                {
                    if (!(HLT_mu6_mu4_bJpsimumu_noL2_isPassed->at(k) || HLT_2mu4_bJpsimumu_noL2_isPassed->at(k))) {continue; cout << "no" << endl; } // triggerL data15
                }
                if (dataset == "data16Main")
                {
                    if (!(HLT_mu10_mu6_bJpsimumu_isPassed->at(k) || HLT_mu6_mu4_bJpsimumu_isPassed->at(k) || HLT_2mu6_bJpsimumu_isPassed->at(k) || HLT_mu6_2mu4_bJpsi_isPassed->at(k))) {continue;}
                }
                if (dataset == "data16dalayed")
                {
                    if (!(HLT_2mu6_bJpsimumu_delayed_isPassed->at(k) || HLT_mu6_mu4_bJpsimumu_Lxy0_delayed_isPassed->at(k) || HLT_mu6_nomucomb_2mu4_nomucomb_delayed_L1MU6_3MU4_isPassed->at(k) || HLT_3mu4_bJpsi_delayed_isPassed->at(k))) {continue;}//selected
                }
                if (dataset == "data17")
                {
                    if (!(HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6_isPassed->at(k) || HLT_mu11_mu6_bDimu_isPassed->at(k) || HLT_3mu4_bJpsi_isPassed->at(k) || HLT_mu6_mu4_bJpsimumu_Lxy0_L1BPH_2M9_MU6MU4_BPH_0DR15_MU6MU4_isPassed->at(k))) {continue;} // selected
                }
                if (dataset == "data18")
                {    
                    if (!(HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6_isPassed->at(k) || HLT_mu6_mu4_bJpsimumu_Lxy0_L1BPH_2M9_MU6MU4_BPH_0DR15_MU6MU4_isPassed->at(k) || HLT_mu11_mu6_bDimu_isPassed->at(k) || HLT_3mu4_bJpsi_isPassed->at(k) || HLT_2mu4_bJpsimumu_Lxy0_L1BPH_2M9_2MU4_BPH_0DR15_2MU4_isPassed->at(k))) {continue;}
                }
                    
                TLorentzVector mu1, mu2, pi1, pi2, K1, K2, p1, p2, jpsi;
                mu1.SetXYZM(B_mu1_px->at(k), B_mu1_py->at(k), B_mu1_pz->at(k), 105.65837);
                mu2.SetXYZM(B_mu2_px->at(k), B_mu2_py->at(k), B_mu2_pz->at(k), 105.65837);
                pi1.SetXYZM(B_trk1_px->at(k), B_trk1_py->at(k), B_trk1_pz->at(k), 139.57);
                pi2.SetXYZM(B_trk2_px->at(k), B_trk2_py->at(k), B_trk2_pz->at(k), 139.57);
                K1.SetXYZM(B_trk1_px->at(k), B_trk1_py->at(k), B_trk1_pz->at(k), 493.677);
                K2.SetXYZM(B_trk2_px->at(k), B_trk2_py->at(k), B_trk2_pz->at(k), 493.677);
                p1.SetXYZM(B_trk1_px->at(k), B_trk1_py->at(k), B_trk1_pz->at(k), 938.272);
                p2.SetXYZM(B_trk2_px->at(k), B_trk2_py->at(k), B_trk2_pz->at(k), 938.272);
                jpsi.SetXYZM(B_mu1_px->at(k) + B_mu2_px->at(k), B_mu1_py->at(k) + B_mu2_py->at(k), B_mu1_pz->at(k) + B_mu2_pz->at(k), 3096.);
                
                bool had1_WP = ((B_trk1_eta->at(k) < 2.5) && (B_trk1_eta->at(k) > -2.5) && (B_trk1_pT->at(k) > 500.) && ((Pixelhits_had1->at(k) + SCThitshad1->at(k) + SCTDeadSensors_had1->at(k) + PixelDeadSensors_had1->at(k)) >= 7) && (((float)PixelShared_had1->at(k) + SCTShared_had1->at(k) / 2.) <= 1.) && ((SCTHoles_had1->at(k) + PixelHoles_had1->at(k)) <= 2) && (PixelHoles_had1->at(k) <= 1));
                bool had2_WP = ((B_trk2_eta->at(k) < 2.5) && (B_trk2_eta->at(k) > -2.5) && (B_trk2_pT->at(k) > 500.) && ((Pixelhits_had2->at(k) + SCThitshad2->at(k) + SCTDeadSensors_had2->at(k) + PixelDeadSensors_had2->at(k)) >= 7) && (((float)PixelShared_had2->at(k) + SCTShared_had2->at(k) / 2.) <= 1.) && ((SCTHoles_had2->at(k) + PixelHoles_had2->at(k)) <= 2) && (PixelHoles_had2->at(k) <= 1));
                
              
                TLorentzVector bs = jpsi + pi1 + pi2;
                bool selection_test;
                
                if (selection_mode == "comb_bg") 
                {
                     selection_test = (B_Lxy_MaxSumPt->at(k) > 0.7 && B_chi2_ndof->at(k) < 2. && B_pT->at(k) > 0.15 * MaxSumPt_SumPt->at(k) && quality_mu1->at(k) == 0 && quality_mu2->at(k) == 0 && had1_WP && had2_WP);
                }
                    
                if (selection_mode == "default") 
                {
                     selection_test = ((K1 + pi2).M() > 1500. && (pi1 + K2).M() > 1500. && B_Lxy_MaxSumPt->at(k) > 0.7 && B_mu1_pT->at(k) > 4000. && B_mu2_pT->at(k) > 4000. && B_trk1_pT->at(k) > 1500. && B_trk2_pT->at(k) > 1500. && B_chi2_ndof->at(k) < 2. && B_pT->at(k) > 0.15 * MaxSumPt_SumPt->at(k) && quality_mu1->at(k) == 0 && quality_mu2->at(k) == 0 && had1_WP && had2_WP); 
                }
                  
                if (selection_mode == "full") 
                {
                    selection_test = ((K1 + pi2).M() > 1550. && (pi1 + K2).M() > 1550. && B_Lxy_MaxSumPt->at(k) > 1. && (jpsi + pi1).M() < 4900.  && (jpsi + pi2).M() < 4900. && B_mu1_pT->at(k) > 4000. && B_mu2_pT->at(k) > 4000. && B_trk1_pT->at(k) > 2000. && B_trk2_pT->at(k) > 2000.  && B_chi2_ndof->at(k) < 1.7 && B_pT->at(k) > 0.25 * MaxSumPt_SumPt->at(k) && 4900. < (jpsi + K1 + pi2).M() && (jpsi + K1 + pi2).M() < 5700. && (jpsi + pi1 + K2).M() < 5900. && 2950. < B_Jpsi_mass->at(k) && B_Jpsi_mass->at(k) < 3250.);
                }
                
                if (selection_mode == "slim") 
                {
                    selection_test = (quality_mu1->at(k) == 0 && quality_mu2->at(k) == 0 && had1_WP && had2_WP);
                }
                
                if (selection_test)
                {
                    n++;
                    
                    mu1_pt = B_mu1_pT->at(k);
                    mu2_pt = B_mu2_pT->at(k);
                    trk1_pt = B_trk1_pT->at(k);
                    trk2_pt = B_trk2_pT->at(k);
                    mu1_px = B_mu1_px->at(k);
                    mu1_py = B_mu1_py->at(k);
                    mu1_pz = B_mu1_pz->at(k);
                    mu2_px = B_mu2_px->at(k);
                    mu2_py = B_mu2_py->at(k);
                    mu2_pz = B_mu2_pz->at(k);
                    trk1_px = B_trk1_px->at(k);
                    trk1_py = B_trk1_py->at(k);
                    trk1_pz = B_trk1_pz->at(k);
                    trk2_px = B_trk2_px->at(k);
                    trk2_py = B_trk2_py->at(k);
                    trk2_pz = B_trk2_pz->at(k);
                    mu1_px_ReFit_pK = B_mu1_px_ReFit_pK->at(k);
                    mu1_py_ReFit_pK = B_mu1_py_ReFit_pK->at(k);
                    mu1_pz_ReFit_pK = B_mu1_pz_ReFit_pK->at(k);
                    mu2_px_ReFit_pK = B_mu2_px_ReFit_pK->at(k);
                    mu2_py_ReFit_pK = B_mu2_py_ReFit_pK->at(k);
                    mu2_pz_ReFit_pK = B_mu2_pz_ReFit_pK->at(k);
                    trk1_px_ReFit_pK = B_trk1_px_ReFit_pK->at(k);
                    trk1_py_ReFit_pK = B_trk1_py_ReFit_pK->at(k);
                    trk1_pz_ReFit_pK = B_trk1_pz_ReFit_pK->at(k);
                    trk2_px_ReFit_pK = B_trk2_px_ReFit_pK->at(k);
                    trk2_py_ReFit_pK = B_trk2_py_ReFit_pK->at(k);
                    trk2_pz_ReFit_pK = B_trk2_pz_ReFit_pK->at(k);
                    mu1_px_ReFit_Kp = B_mu1_px_ReFit_Kp->at(k);
                    mu1_py_ReFit_Kp = B_mu1_py_ReFit_Kp->at(k);
                    mu1_pz_ReFit_Kp = B_mu1_pz_ReFit_Kp->at(k);
                    mu2_px_ReFit_Kp = B_mu2_px_ReFit_Kp->at(k);
                    mu2_py_ReFit_Kp = B_mu2_py_ReFit_Kp->at(k);
                    mu2_pz_ReFit_Kp = B_mu2_pz_ReFit_Kp->at(k);
                    trk1_px_ReFit_Kp = B_trk1_px_ReFit_Kp->at(k);
                    trk1_py_ReFit_Kp = B_trk1_py_ReFit_Kp->at(k);
                    trk1_pz_ReFit_Kp = B_trk1_pz_ReFit_Kp->at(k);
                    trk2_px_ReFit_Kp = B_trk2_px_ReFit_Kp->at(k);
                    trk2_py_ReFit_Kp = B_trk2_py_ReFit_Kp->at(k);
                    trk2_pz_ReFit_Kp = B_trk2_pz_ReFit_Kp->at(k);
                    ReFit_result = B_ReFit_result->at(k);
                    trk1_charge = B_trk1_charge->at(k);
                    trk2_charge = B_trk2_charge->at(k);
                    MaxSumPt = MaxSumPt_SumPt->at(k);
                    MinA0SumPt = MinA0_SumPt->at(k);
                    mu1_eta = B_mu1_eta->at(k);
                    mu2_eta = B_mu2_eta->at(k);
                    trk1_eta = B_trk1_eta->at(k);
                    trk2_eta = B_trk2_eta->at(k);
                    Bs_pt = B_pT->at(k);
                    Bs_chi2_ndof = B_chi2_ndof->at(k);
                    Lxy_MaxSumPt = B_Lxy_MaxSumPt->at(k);
                    Lxy_MinA0 = B_Lxy_MinA0->at(k);
                    Jpsi_mass = B_Jpsi_mass->at(k);
                    Old_Jpsi_chi2 = B_Jpsi_chi2->at(0);
                    mu1_charge = B_mu1_charge->at(k);
                    mu2_charge = B_mu2_charge->at(k);
                    stree->Fill();
                    
                }
            }
    }
    
    cout << "_____________________________________________________________________" << endl;
    cout << "Total number of events:  " << n << endl;
    sf->cd();
    stree->Write();
    sf->Close();
}
